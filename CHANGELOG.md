## Alpha 0.20 (CURRENT)
* TODO

## Alpha 0.19
* Split Database into GameDatabase & AuthDatabase
* Implement deferred shutdown to shutdown server after an amount of time
* TCPClient/Server improvements
* Move some client settings to a JSON format
* Various performance improvements using new C++11 STL containers
* SQLite 3.11.0
* Various game stop fixes
* Move many attributes outside of Player, bind them to PlayerSAO to reduce memory usage
* Import lz4 library

## Alpha 0.18
* Implement a basic epixel TCP protocol
* Interserver chatserver improvements
* Import libpng 1.6.21
* Import zlib 1.2.8
* Upgrade to Lua 5.3.2
* Improve DB strength by adding constraints
* Android build fixes (WIP)
* Texture rework (pillow, doors, bronze, steel, farming_desert, xpanes, furnaces, fire)
* SQLite update (3.10.2)
* Add coordinates under minimap
* PostgreSQL 9.5 is mandatory as the PostgreSQL API use UPSERTs
* Implement a maintenance mode settable by administrator to permit lock connections

## Alpha 0.17
* Add a cmake target to create a Debian package
* Lua updated from 5.1.3 to 5.1.5
* Import SQLite into the project

## Alpha 0.16
* Add teleporters with commands to add it dynamicly
* Import irrlicht 1.8.4 & backport many Irrlicht 1.9 fixes
* Cotton & wheat can rotten after ~3 days ingame
* Embed jsoncpp in our package
* MapDB connector use a basic postgresql database pool
* Node drops: improve API
* Migrate many moreblocks, dye, 3d_armor crafts to database
* Improve performance in various way
* Don't spawn monsters when a player is too close
* /server status: add player count
* Implement interserver chatserver
* Add a passive mapblock generator when no player is connected
* Re-texture beds, farming, wool and TNT
* Epixel mapblock v2: lz4 compression

## Alpha 0.15
* Import moreblocks mod into epixel_game
* Epixel mapblock v1: no compression
* Split mapblock storage into three fields (block, active objects, timers)
* Drop LUA call: emerge_area
* Server step set to 0.025
* Queue movement packets in a special queue to handle them faster
* Networking performance improvements
* Add ballistic API (example: bows)
* Notify the right player when modifying an area
* Fixes on explosion API
* Fixes on teleport command
* C++11 Random number generation

## Alpha 0.14
* Fix inventory problems when bones cannot be dropped
* Add log4cpp logging system to have a powerful logging system for server owners (log4j like)
* Fix on /privs list
* Optimize many string copy/comparison
* Drop /pulverize command
* Migrate /give command to core and remove /giveme
* Fix xpane texture problems
* Drop some minetest 0.3 compat things
* Drop INTERNET_SIMULATOR in connection
* Support ore registration from database
* CastSpell is now a UnitSAO feature, not only Creature
* Moretrees & trees textures
* Drop add_owner from areas mod
* Rename and migrate /area set_owner to native /area add command
* More logs into areas commands
* Wool textures

## Alpha 0.13 and lower
* Starting project with many features (@TODO)