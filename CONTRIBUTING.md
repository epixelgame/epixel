# How to contribute

## Merge request format
Merge requests are composed with a single commit to have a cleaner git master branch history.
Reviews are done by developpers before merge.
Merge requests should be rebased before a merge and contributor should maintain the merge request branch.

## License
Contributions on core are accepted if you agree that the license will remain same as the sources you modify.