Epixel
========

An InfiniMiner/Minecraft inspired game forked from Minetest

Epixel : Copyright (c) 2015-2016 <http://www.epixel-game.net>
and contributors (see source file comments and the version control log)

Minetest : Copyright (c) 2010-2013 Perttu Ahola <celeron55@gmail.com>
and contributors (see source file comments and the version control log)


In case you downloaded the source code:
---------------------------------------
You can download Epixel source at :
  https://gitlab.com/epixelgame/epixel/

Differences between Minetest and Epixel
----------------------------------------

Epixel server uses PostgreSQL (9.5 or greater) to make the best performance possible
without consuming too much memory.

Epixel client & singleplayer only use SQLite

Epixel database use incremental database model to permit users to have
the best backend experience and plug a website on it.

Epixel uses C++11, don't try to compile it on a very old machine.

Epixel provide OpenMP support in some areas

Epixel embedded and up-to-date lua, log4cpp, jsoncpp, png, sqlite3, zlib and irrlicht engine to provide the better user experience

Logging in epixel is managed by log4cpp.properties.
- epixel.log: contains all logs
- epixel-chat.log: contains only chat & whispers between players

Epixel embed many mods functions coreside (with enhancements & performance optimizations) like:
- generic commands
- achievements
- areas
- armor
- bans (player & IP)
- creatures
- falling nodes
- guest filtering
- hunger
- item dropping on ground
- worldedit (partially)

Epixel add some database API permitting to populate content only by adding entries to database.
This includes:
- crafts
- nodes
- items
- creatures
- spells
- achievements
- tips
- ores

Epixel save the following datas into the SQL database whereas minetest
doesn't (except mapblocks):
- accounts
- players & inventories
- game time

Epixel choose LZ4 instead of zlib for compressing mapblocks into database,
increasing fetching/saving performance

Epixel provide migration scripts from minetest to epixel

Epixel adds exclusive embedded functions:
- ballistic/projectiles API (used by our embedded bows & arrows)
- cobble transform to mossycobble when there is water
- creatures burn into lava
- explosion API
- experience
- improved logging using log4cpp library
- items burn into lava
- lightweight console
- mana
- player message length limit
- player message flooding limit
- money
- player channels permitting discussion in different broadcast areas
- rules acceptance (using magic sentence)
- server maintenance mode permitting only to superadmin to connect
- spell API to create your own spells and add it to creatures
- teleporters
- InterServer Chat using our lightweight emt-chatserver (https://github.com/epixelgame/emt-chatserver)

Epixel client has some differences:
- coordinates are shown under minimap
- main menu music (from OpenBSD team)
- X11 fixes for non qwerty keymaps
- Irrlicht 1.9 performance optimizations
- Lua 5.3.2 instead of Lua 5.1.4

Further documentation
----------------------
- Website: http://www.epixel-game.net
- Website(fr) : http://www.epixel.fr
- Gitlab: https://gitlab.com/epixelgame/epixel/
- doc/ directory of source distribution and Doxygen

This game is not finished
--------------------------
- Don't expect it to work as well as a finished game will.
- Please report any bugs. When doing that, debug.txt is useful.

Default Controls
-----------------
- WASD: move
- Space: jump/climb
- Shift: sneak/go down
- Q: drop itemstack (+ SHIFT for single item)
- I: inventory
- Mouse: turn/look
- Mouse left: dig/punch
- Mouse right: place/use
- Mouse wheel: select item
- T: chat
- 1-8: select item

- Esc: pause menu (pauses only singleplayer game)
- R: Enable/Disable full range view
- +: Increase view range
- -: Decrease view range
- K: Enable/Disable fly (needs fly privilege)
- J: Enable/Disable fast (needs fast privilege)
- H: Enable/Disable noclip (needs noclip privilege)

- F1:  Hide/Show HUD
- F2:  Hide/Show Chat
- F3:  Disable/Enable Fog
- F4:  Disable/Enable Camera update (Mapblocks are not updated anymore when disabled)
- F5:  Toogle through debug info screens
- F6:  Toogle through output data
- F7:  Toggle through camera modes
- F10: Show/Hide console
- F12: Take screenshot

- Settable in the configuration file, see the section below.

Paths
------
```
$bin   - Compiled binaries
$share - Distributed read-only data
$user  - User-created modifiable data

Windows .zip / RUN_IN_PLACE source:
$bin   = bin
$share = .
$user  = .

Linux installed:
$bin   = /usr/bin
$share = /usr/share/minetest
$user  = ~/.minetest

OS X:
$bin   = Contents/MacOS
$share = Contents/Resources
$user  = Contents/User OR ~/Library/Application Support/minetest
```

World directory
----------------
- Worlds can be found as separate folders in:
    $user/worlds/

Running dedicated server
------------------------

Dedicated server needs postgresql (>= 9.5) to run.

On Debian/Ubuntu:
```
$ apt-get install postgresql-9.5
```

On FreeBSD:
```
$ pkg install postgresql95-server
```

Configuring PostgreSQL:
```
$ service postgresql initdb
$ service postgresql start
```

Next edit pg_hba.conf. It's located here:
- Debian/Ubuntu: /etc/postgresql/<version>/pg_hba.conf
- FreeBSD: /usr/local/pgsql/data/pg_hba.conf

And add this line:
host	all	all	0.0.0.0/0	md5

Next, connect as a postgresql user

On Debian/Ubuntu:
```
$ su - postgres
```

On FreeBSD:
```
$ su - pgsql
```

Next create a user, a database and give him rights:
```
$ createdb epixel
$ createuser epixel -P
$ psql epixel
epixel=# GRANT ALL ON DATABASE epixel to epixel;
GRANT
epixel=# \q
$ exit
```

Then restart postgresql service
```
$ service postgresql restart
```

Configuration file:
-------------------
- Default location:
	$user/epixel.conf
- It is created by Epixel when it is ran the first time.
- A specific file can be specified on the command line:
	--config <path-to-file\>

Command-line options:
---------------------
- Use --help

Recommandations for development:
--------------------------------

We recommend you to use QTCreator which is very well integrated with
Epixel environment, supporting C++11, GDB, Valgrind, CMake and more.

Compiling on GNU/Linux:
-----------------------

Ubuntu 15.04 and lesser users need to add GCC 5.1 additionnal repository or switch to clang.
```
$ sudo add-apt-repository ppa:ubuntu-toolchain-r/test && sudo apt-get install gcc-5 g++-5
```

Install dependencies. Here's an example for Debian/Ubuntu:
```
$ sudo apt-get install build-essential cmake libbz2-dev libjpeg-dev libxxf86vm-dev libgl1-mesa-dev libopenal-dev libfreetype6-dev libgmp-dev libpq-dev
```

On Fedora:
```
$ sudo dnf install make automake gcc gcc-c++ kernel-devel cmake openal* libXxf86vm-devel freetype-devel mesa-libGL-devel bzip2-libs gmp-devel bzip2-dev
```

On FreeBSD:
```
$ sudo pkg install cmake clang37 postgresql95-client freetype libiconv
```

You can install git for easily keeping your copy up to date.
If you dont want git, read below on how to get the source without git.
This is an example for installing git on Debian/Ubuntu:
```
$ sudo apt-get install git-core
```

On Fedora:
```
$ sudo dns install git-core
```

On FreeBSD:
```
$ sudo pkg install git
```

Download source (this is the URL to the latest of source repository, which might not work at all times) using git:
```
$ git clone --depth 1 https://gitlab.com/epixelgame/epixel.git
$ cd epixel
```

Build a version that runs directly from the source directory:
```
$ cmake . -DRUN_IN_PLACE=TRUE
$ make -j <number of processors\>
```

On FreeBSD:
```
$ cmake . -DENABLE_GETTEXT=0 -DRUN_IN_PLACE=1 -DCMAKE_CXX_COMPILER=clang++37 -DCMAKE_C_COMPILER=clang37
$ make -j <number of processors\>
```

Run it:
```
$ ./bin/epixel
```

- Use cmake . -LH to see all CMake options and their current state
- If you want to install it system-wide (or are making a distribution package),
  you will want to use -DRUN_IN_PLACE=FALSE
- You can build a bare server by specifying -DBUILD_SERVER=TRUE
- You can disable the client build by specifying -DBUILD_CLIENT=FALSE
- You can select between Release and Debug build by -DCMAKE_BUILD_TYPE=<Debug or Release>
  - Debug build is slower, but gives much more useful output in a debugger
- If you build a bare server, you don't need to have Irrlicht installed.
  In that case use -DIRRLICHT_SOURCE_DIR=/the/irrlicht/source

CMake options
-------------

```
General options:

BUILD_CLIENT        - Build Minetest client
BUILD_SERVER        - Build Minetest server
CMAKE_BUILD_TYPE    - Type of build (Release vs. Debug)
	Release         - Release build
	Debug           - Debug build
	RelWithDebInfo  - Release build with Debug information
	MinSizeRel      - Release build with -Os passed to compiler to make executable as small as possible
ENABLE_GETTEXT      - Build with Gettext; Allows using translations
ENABLE_GLES         - Search for Open GLES headers & libraries and use them
ENABLE_SPATIAL      - Build with LibSpatial; Speeds up AreaStores
ENABLE_SOUND        - Build with OpenAL, libogg & libvorbis; in-game Sounds
ENABLE_SYSTEM_GMP   - Use GMP from system (much faster than bundled mini-gmp)
RUN_IN_PLACE        - Create a portable install (worlds, settings etc. in current directory)
USE_GPROF           - Enable profiling using GProf
VERSION_EXTRA       - Text to append to version (e.g. VERSION_EXTRA=foobar -> Minetest 0.4.9-foobar)

Library specific options:

BZIP2_INCLUDE_DIR               - Linux only; directory where bzlib.h is located
BZIP2_LIBRARY                   - Linux only; path to libbz2.a/libbz2.so
EGL_INCLUDE_DIR                 - Only if building with GLES; directory that contains egl.h
EGL_LIBRARY                     - Only if building with GLES; path to libEGL.a/libEGL.so
FREETYPE_INCLUDE_DIR_freetype2  - Only if building with Freetype2; directory that contains an freetype directory with files such as ftimage.h in it
FREETYPE_INCLUDE_DIR_ft2build   - Only if building with Freetype2; directory that contains ft2build.h
FREETYPE_LIBRARY                - Only if building with Freetype2; path to libfreetype.a/libfreetype.so/freetype.lib
FREETYPE_DLL                    - Only if building with Freetype2 on Windows; path to libfreetype.dll
GETTEXT_DLL                     - Only when building with Gettext on Windows; path to libintl3.dll
GETTEXT_ICONV_DLL               - Only when building with Gettext on Windows; path to libiconv2.dll
GETTEXT_INCLUDE_DIR             - Only when building with Gettext; directory that contains iconv.h
GETTEXT_LIBRARY                 - Only when building with Gettext on Windows; path to libintl.dll.a
GETTEXT_MSGFMT                  - Only when building with Gettext; path to msgfmt/msgfmt.exe
IRRLICHT_DLL                    - Only on Windows; path to Irrlicht.dll
IRRLICHT_INCLUDE_DIR            - Directory that contains IrrCompileConfig.h
IRRLICHT_LIBRARY                - Path to libIrrlicht.a/libIrrlicht.so/libIrrlicht.dll.a/Irrlicht.lib
SPATIAL_INCLUDE_DIR             - Only when building with LibSpatial; directory that contains spatialindex/SpatialIndex.h
SPATIAL_LIBRARY                 - Only when building with LibSpatial; path to libspatialindex_c.so/spatialindex-32.lib
MINGWM10_DLL                    - Only if compiling with MinGW; path to mingwm10.dll
OPENAL_DLL                      - Only if building with sound on Windows; path to OpenAL32.dll
OPENAL_INCLUDE_DIR              - Only if building with sound; directory where al.h is located
OPENAL_LIBRARY                  - Only if building with sound; path to libopenal.a/libopenal.so/OpenAL32.lib
OPENGLES2_INCLUDE_DIR           - Only if building with GLES; directory that contains gl2.h
OPENGLES2_LIBRARY               - Only if building with GLES; path to libGLESv2.a/libGLESv2.so
XXF86VM_LIBRARY                 - Only on Linux; path to libXXf86vm.a/libXXf86vm.so
```

Embedded library versions:
---------------------
- SQLite 3.13.0 (https://www.sqlite.org/2016/sqlite-amalgamation-3130000.zip)
- Irrlicht 1.8.4 synced with some irrlicht 1.9-trunk
- log4cpp 1.1.1 (http://sourceforge.net/projects/log4cpp/files/log4cpp-1.1.x%20%28new%29/log4cpp-1.1/log4cpp-1.1.1.tar.gz/download)
- jsoncpp 1.6.5 (https://github.com/open-source-parsers/jsoncpp/archive/1.6.5.zip)
- LUA 5.3.2 (http://www.lua.org/ftp/lua-5.3.2.tar.gz)
- zlib 1.2.8 (http://zlib.net/zlib-1.2.8.tar.gz)
- PNG 1.6.21 (http://heanet.dl.sourceforge.net/project/libpng/libpng16/1.6.21/libpng-1.6.21.tar.xz)
- LZ4 r131 (https://github.com/Cyan4973/lz4/commits/r131)
- CURL 7.48.0 (https://curl.haxx.se/download/curl-7.48.0.tar.gz)
- OGG 1.3.2 (http://downloads.xiph.org/releases/ogg/libogg-1.3.2.tar.gz)
- Vorbis 1.3.5 (http://downloads.xiph.org/releases/vorbis/libvorbis-1.3.5.tar.xz)
- LibreSSL 2.3.3 (https://github.com/libressl-portable/portable/archive/v2.3.3.zip)

Compiling on Windows:
---------------------

- This section is outdated. In addition to what is described here:
  - If you wish to have sound support, you need libvorbis and libopenal

- You need:
	* CMake:
		http://www.cmake.org/cmake/resources/software.html
	* Visual Studio 2015
		http://msdn.microsoft.com/en-us/vstudio/default
	* For Windows 10, Windows 10 SDK
		https://dev.windows.com/en-us/downloads/windows-10-sdk
	* Freetype 2.6 or greater (http://download.savannah.gnu.org/releases/freetype/)
	* jpeg 6b-4 or greater
	* Optional: gettext library and tools:
		http://gnuwin32.sourceforge.net/downlinks/gettext.php
		- This is used for other UI languages. Feel free to leave it out.
	* And, of course, Epixel:
		http://www.epixel-game.net/download
- Steps:
	- Copy epixel/lib/zlib/zconf.h.included to epixel/lib/zlib/zconf.h
	- Configure with cmake & compile Freetype and JPEG

```
	BUILD_CLIENT             [X]
	BUILD_SERVER             [ ]
	CMAKE_BUILD_TYPE         Release
	CMAKE_INSTALL_PREFIX     DIR/minetest-install
	RUN_IN_PLACE             [X]
	WARN_ALL                 [ ]
	GETTEXT_BIN_DIR          DIR/gettext/bin
	GETTEXT_INCLUDE_DIR      DIR/gettext/include
	GETTEXT_LIBRARIES        DIR/gettext/lib/intl.lib
	GETTEXT_MSGFMT           DIR/gettext/bin/msgfmt
```

- Hit "Configure"
	- Hit "Configure" once again 8)
	- If something is still coloured red, you have a problem.
	- Hit "Generate"
	If using MSVC:
		- Open the generated epixel.sln
		- The project defaults to the "Debug" configuration. Make very sure to
		  select "Release", unless you want to debug some stuff (it's slower
		  and might not even work at all)
		- Build the ALL_BUILD project
		- Build the INSTALL project
		- You should now have a working game with the executable in
			DIR/epixel-install/bin/epixel.exe
		- Additionally you may create a zip package by building the PACKAGE
		  project.
