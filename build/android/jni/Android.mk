LOCAL_PATH := $(call my-dir)/..

#LOCAL_ADDRESS_SANITIZER:=true

include $(CLEAR_VARS)
LOCAL_MODULE := Irrlicht
LOCAL_SRC_FILES := deps/irrlicht/lib/Android/libIrrlicht.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := curl
LOCAL_SRC_FILES := ../../lib/curl/lib/.libs/libcurl.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := freetype
LOCAL_SRC_FILES := deps/freetype2-android/Android/obj/local/$(TARGET_ARCH_ABI)/libfreetype2-static.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := iconv
LOCAL_SRC_FILES := deps/libiconv/lib/.libs/libiconv.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := openal
LOCAL_SRC_FILES := deps/openal-soft/libs/$(TARGET_LIBDIR)/libopenal.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := ogg
LOCAL_SRC_FILES := deps/libvorbis-libogg-android/libs/$(TARGET_LIBDIR)/libogg.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := vorbis
LOCAL_SRC_FILES := deps/libvorbis-libogg-android/libs/$(TARGET_LIBDIR)/libvorbis.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := gmp
LOCAL_SRC_FILES := deps/gmp/usr/lib/libgmp.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := ssl
LOCAL_SRC_FILES := deps/openssl/libssl.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := crypto
LOCAL_SRC_FILES := deps/openssl/libcrypto.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := lz4
LOCAL_SRC_FILES := deps/lz4/liblz4.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := enet
LOCAL_SRC_FILES := deps/enet/libenet.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_CFLAGS := -Wall -O3 -pipe -std=c++11
LOCAL_MODULE := log4cpp
LOCAL_C_INCLUDES :=                               \
		../../lib/log4cpp/include
LOCAL_SRC_FILES := \
../../lib/log4cpp/src/AbortAppender.cpp \
../../lib/log4cpp/src/Appender.cpp \
../../lib/log4cpp/src/AppendersFactory.cpp \
../../lib/log4cpp/src/AppenderSkeleton.cpp \
../../lib/log4cpp/src/BasicConfigurator.cpp \
../../lib/log4cpp/src/BasicLayout.cpp \
../../lib/log4cpp/src/BufferingAppender.cpp \
../../lib/log4cpp/src/Category.cpp \
../../lib/log4cpp/src/CategoryStream.cpp \
../../lib/log4cpp/src/Configurator.cpp \
../../lib/log4cpp/src/DllMain.cpp \
../../lib/log4cpp/src/DummyThreads.cpp \
../../lib/log4cpp/src/FactoryParams.cpp \
../../lib/log4cpp/src/FileAppender.cpp \
../../lib/log4cpp/src/Filter.cpp \
../../lib/log4cpp/src/FixedContextCategory.cpp \
../../lib/log4cpp/src/HierarchyMaintainer.cpp \
../../lib/log4cpp/src/IdsaAppender.cpp \
../../lib/log4cpp/src/LayoutAppender.cpp \
../../lib/log4cpp/src/LayoutsFactory.cpp \
../../lib/log4cpp/src/LevelEvaluator.cpp \
../../lib/log4cpp/src/Localtime.cpp \
../../lib/log4cpp/src/LoggingEvent.cpp \
../../lib/log4cpp/src/Manipulator.cpp \
../../lib/log4cpp/src/MSThreads.cpp \
../../lib/log4cpp/src/NDC.cpp \
../../lib/log4cpp/src/NTEventLogAppender.cpp \
../../lib/log4cpp/src/OmniThreads.cpp \
../../lib/log4cpp/src/OstreamAppender.cpp \
../../lib/log4cpp/src/PassThroughLayout.cpp \
../../lib/log4cpp/src/PatternLayout.cpp \
../../lib/log4cpp/src/PortabilityImpl.cpp \
../../lib/log4cpp/src/Priority.cpp \
../../lib/log4cpp/src/Properties.cpp \
../../lib/log4cpp/src/PropertyConfigurator.cpp \
../../lib/log4cpp/src/PropertyConfiguratorImpl.cpp \
../../lib/log4cpp/src/PThreads.cpp \
../../lib/log4cpp/src/RemoteSyslogAppender.cpp \
../../lib/log4cpp/src/RollingFileAppender.cpp \
../../lib/log4cpp/src/SimpleConfigurator.cpp \
../../lib/log4cpp/src/SimpleLayout.cpp \
../../lib/log4cpp/src/SmtpAppender.cpp \
../../lib/log4cpp/src/snprintf.c \
../../lib/log4cpp/src/StringQueueAppender.cpp \
../../lib/log4cpp/src/StringUtil.cpp \
../../lib/log4cpp/src/SyslogAppender.cpp \
../../lib/log4cpp/src/TimeStamp.cpp \
../../lib/log4cpp/src/TriggeringEventEvaluatorFactory.cpp
include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_CFLAGS := -Wall -O3 -pipe -std=c++11
LOCAL_MODULE := jsoncpp
LOCAL_C_INCLUDES :=                               \
		../../lib/jsoncpp/include
LOCAL_SRC_FILES := \
../../lib/jsoncpp/jsoncpp.cpp
include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_CFLAGS := -Wall -O3 -pipe
LOCAL_MODULE := sqlite3
LOCAL_C_INCLUDES :=                               \
		../../lib/sqlite3/src
LOCAL_SRC_FILES := \
../../lib/sqlite3/src/sqlite3.c
include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := lua
LOCAL_SRC_FILES := deps/lua/build/liblua.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := epixel

LOCAL_CPP_FEATURES += exceptions

ifdef GPROF
GPROF_DEF=-DGPROF
endif

LOCAL_CFLAGS := -D_IRR_ANDROID_PLATFORM_      \
		-DHAVE_TOUCHSCREENGUI         \
		-DUSE_CURL=1                  \
		-DUSE_SOUND=1                 \
		-DUSE_FREETYPE=1              \
		$(GPROF_DEF)                  \
		-pipe -fstrict-aliasing -std=c++11

ifndef NDEBUG
LOCAL_CFLAGS += -g -D_DEBUG -O0 -fno-omit-frame-pointer
else
LOCAL_CFLAGS += -O3
endif

ifdef GPROF
PROFILER_LIBS := android-ndk-profiler
LOCAL_CFLAGS += -pg
endif

# LOCAL_CFLAGS += -fsanitize=address
# LOCAL_LDFLAGS += -fsanitize=address

ifeq ($(TARGET_ARCH_ABI),x86)
LOCAL_CFLAGS += -fno-stack-protector
endif

LOCAL_C_INCLUDES := \
		jni/src                                   \
		jni/src/script                            \
		jni/src/cguittfont                        \
		deps/irrlicht/include                     \
		deps/libiconv/include                     \
		deps/freetype2-android/include            \
		../../lib/curl/include                    \
		deps/openal-soft/jni/OpenAL/include       \
		deps/openssl/include		          \
		deps/libvorbis-libogg-android/jni/include \
		deps/gmp/usr/include                      \
		../../lib/sqlite3/src			  \
		../../lib/lua/src	    		  \
		../../lib/lz4   	    		  \
		../../lib/log4cpp/include		  \
		../../lib/enet/include			  \
		../../lib/jsoncpp/include

# We use this command to generate list:
# find src -name "*.cpp" | grep -v "\/src\/server\/" | grep -v "\/cguittfont\/CGUITTFont.cpp" | \
# grep -v "settings_translation_file.cpp" |  sed 's/^/\tjni\//' | sed 's/$/ \\/' | sort
LOCAL_SRC_FILES :=                                \
	    jni/src/areastore.cpp \
	    jni/src/camera.cpp \
	    jni/src/cavegen.cpp \
	    jni/src/cguittfont/xCGUITTFont.cpp \
	    jni/src/client/chat.cpp \
	    jni/src/client/clientlauncher.cpp \
	    jni/src/client/clientmedia.cpp \
	    jni/src/client/clientsettings.cpp \
	    jni/src/client.cpp \
	    jni/src/client/database-sqlite3.cpp \
	    jni/src/client/filecache.cpp \
	    jni/src/client/guiChatConsole.cpp \
	    jni/src/client/guiEngine.cpp \
	    jni/src/client/guiFileSelectMenu.cpp \
	    jni/src/client/guiFormSpecMenu.cpp \
	    jni/src/client/guiKeyChangeMenu.cpp \
	    jni/src/client/guiPasswordChange.cpp \
	    jni/src/client/guiscalingfilter.cpp \
	    jni/src/client/guiTable.cpp \
	    jni/src/client/guiVolumeChange.cpp \
	    jni/src/clientiface.cpp \
	    jni/src/client/keycode.cpp \
	    jni/src/client/localplayer.cpp \
	    jni/src/clientmap.cpp \
	    jni/src/clientobject.cpp \
	    jni/src/client/tile.cpp \
	    jni/src/clouds.cpp \
	    jni/src/collision.cpp \
	    jni/src/content_cao.cpp \
	    jni/src/content_cso.cpp \
	    jni/src/content_mapblock.cpp \
	    jni/src/content_mapnode.cpp \
	    jni/src/content_nodemeta.cpp \
	    jni/src/content_sao.cpp \
	    jni/src/contrib/abm.cpp \
	    jni/src/contrib/abms/fireabm.cpp \
	    jni/src/contrib/abms/treeabm.cpp \
	    jni/src/contrib/areas.cpp \
	    jni/src/contrib/ban.cpp \
	    jni/src/contrib/channelhandler.cpp \
	    jni/src/contrib/chathandler.cpp \
	    jni/src/contrib/creature.cpp \
	    jni/src/contrib/creatures/cow.cpp \
	    jni/src/contrib/creatures/sheep.cpp \
	    jni/src/contrib/creaturestore.cpp \
	    jni/src/contrib/environment.cpp \
	    jni/src/contrib/fallingsao.cpp \
	    jni/src/contrib/itemsao.cpp \
	    jni/src/contrib/l_env.cpp \
	    jni/src/contrib/l_object.cpp \
	    jni/src/contrib/l_server.cpp \
	    jni/src/contrib/network/networkclient.cpp \
	    jni/src/contrib/network/networkserver.cpp \
	    jni/src/contrib/nodetimers.cpp \
	    jni/src/contrib/player.cpp \
	    jni/src/contrib/playersao.cpp \
	    jni/src/contrib/projectilesao.cpp \
	    jni/src/contrib/server.cpp \
	    jni/src/contrib/spell.cpp \
	    jni/src/contrib/stats.cpp \
	    jni/src/contrib/teleport.cpp \
	    jni/src/contrib/unitsao.cpp \
	    jni/src/contrib/util/base64.cpp \
	    jni/src/contrib/util/optparse.cpp \
	    jni/src/contrib/util/socket.cpp \
	    jni/src/convert_json.cpp \
	    jni/src/craftdef.cpp \
	    jni/src/debug.cpp \
	    jni/src/defaultsettings.cpp \
	    jni/src/drawscene.cpp \
	    jni/src/dungeongen.cpp \
	    jni/src/emerge.cpp \
	    jni/src/environment.cpp \
	    jni/src/fontengine.cpp \
	    jni/src/game.cpp \
	    jni/src/genericobject.cpp \
	    jni/src/gettext.cpp \
	    jni/src/httpfetch.cpp \
	    jni/src/hud.cpp \
	    jni/src/imagefilters.cpp \
	    jni/src/inventory.cpp \
	    jni/src/inventorymanager.cpp \
	    jni/src/itemdef.cpp \
	    jni/src/light.cpp \
	    jni/src/main.cpp \
	    jni/src/mapblock.cpp \
	    jni/src/mapblock_mesh.cpp \
	    jni/src/map.cpp \
	    jni/src/mapgen.cpp \
	    jni/src/mapgen_singlenode.cpp \
	    jni/src/mapgen_v6.cpp \
	    jni/src/mapgen_v7.cpp \
	    jni/src/mapgen_valleys.cpp \
	    jni/src/mapnode.cpp \
	    jni/src/mapsector.cpp \
	    jni/src/mesh.cpp \
	    jni/src/mg_biome.cpp \
	    jni/src/mg_decoration.cpp \
	    jni/src/mg_ore.cpp \
	    jni/src/mg_schematic.cpp \
	    jni/src/minimap.cpp \
	    jni/src/mods.cpp \
	    jni/src/nameidmapping.cpp \
	    jni/src/network/clientopcodes.cpp \
	    jni/src/network/clientpackethandler.cpp \
	    jni/src/network/connection.cpp \
	    jni/src/network/networkpacket.cpp \
	    jni/src/network/serveropcodes.cpp \
	    jni/src/network/serverpackethandler.cpp \
	    jni/src/nodedef.cpp \
	    jni/src/nodemetadata.cpp \
	    jni/src/nodetimer.cpp \
	    jni/src/noise.cpp \
	    jni/src/objdef.cpp \
	    jni/src/object_properties.cpp \
	    jni/src/particles.cpp \
	    jni/src/player.cpp \
	    jni/src/porting_android.cpp \
	    jni/src/porting.cpp \
	    jni/src/script/common/c_content.cpp \
	    jni/src/script/common/c_converter.cpp \
	    jni/src/script/common/c_internal.cpp \
	    jni/src/script/common/c_types.cpp \
	    jni/src/script/cpp_api/s_async.cpp \
	    jni/src/script/cpp_api/s_base.cpp \
	    jni/src/script/cpp_api/s_entity.cpp \
	    jni/src/script/cpp_api/s_env.cpp \
	    jni/src/script/cpp_api/s_inventory.cpp \
	    jni/src/script/cpp_api/s_item.cpp \
	    jni/src/script/cpp_api/s_mainmenu.cpp \
	    jni/src/script/cpp_api/s_node.cpp \
	    jni/src/script/cpp_api/s_nodemeta.cpp \
	    jni/src/script/cpp_api/s_player.cpp \
	    jni/src/script/cpp_api/s_security.cpp \
	    jni/src/script/cpp_api/s_server.cpp \
	    jni/src/script/lua_api/l_areastore.cpp \
	    jni/src/script/lua_api/l_base.cpp \
	    jni/src/script/lua_api/l_craft.cpp \
	    jni/src/script/lua_api/l_env.cpp \
	    jni/src/script/lua_api/l_http.cpp \
	    jni/src/script/lua_api/l_inventory.cpp \
	    jni/src/script/lua_api/l_item.cpp \
	    jni/src/script/lua_api/l_mainmenu.cpp \
	    jni/src/script/lua_api/l_mapgen.cpp \
	    jni/src/script/lua_api/l_nodemeta.cpp \
	    jni/src/script/lua_api/l_nodetimer.cpp \
	    jni/src/script/lua_api/l_noise.cpp \
	    jni/src/script/lua_api/l_object.cpp \
	    jni/src/script/lua_api/l_particles.cpp \
	    jni/src/script/lua_api/l_server.cpp \
	    jni/src/script/lua_api/l_settings.cpp \
	    jni/src/script/lua_api/l_util.cpp \
	    jni/src/script/lua_api/l_vmanip.cpp \
	    jni/src/script/scripting_game.cpp \
	    jni/src/script/scripting_mainmenu.cpp \
	    jni/src/serialization.cpp \
	    jni/src/server.cpp \
	    jni/src/serverlist.cpp \
	    jni/src/serverobject.cpp \
	    jni/src/settings.cpp \
	    jni/src/shader.cpp \
	    jni/src/sky.cpp \
	    jni/src/socket.cpp \
	    jni/src/sound.cpp \
	    jni/src/sound_openal.cpp \
	    jni/src/staticobject.cpp \
	    jni/src/subgame.cpp \
	    jni/src/threads/event.cpp \
	    jni/src/threads/semaphore.cpp \
	    jni/src/threads/thread.cpp \
	    jni/src/tool.cpp \
	    jni/src/touchscreengui.cpp \
	    jni/src/treegen.cpp \
	    jni/src/util/auth.cpp \
	    jni/src/util/bytebuffer.cpp \
	    jni/src/util/directiontables.cpp \
	    jni/src/util/filesys.cpp \
	    jni/src/util/numeric.cpp \
	    jni/src/util/pointedthing.cpp \
	    jni/src/util/serialize.cpp \
	    jni/src/util/srp.cpp \
	    jni/src/util/string.cpp \
	    jni/src/version.cpp \
	    jni/src/voxel.cpp \
	    jni/src/wieldmesh.cpp

LOCAL_CFLAGS := -std=c++11

LOCAL_SHARED_LIBRARIES := iconv openal ogg vorbis gmp
LOCAL_STATIC_LIBRARIES := Irrlicht freetype curl ssl crypto android_native_app_glue sqlite3 log4cpp jsoncpp lz4 lua enet $(PROFILER_LIBS)

LOCAL_LDLIBS := -lEGL -llog -lGLESv1_CM -lGLESv2 -lz -landroid

include $(BUILD_SHARED_LIBRARY)

# at the end of Android.mk
ifdef GPROF
$(call import-module,android-ndk-profiler)
endif
$(call import-module,android/native_app_glue)

