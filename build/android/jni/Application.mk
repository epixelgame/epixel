# NDK_TOOLCHAIN_VERSION := clang3.8

APP_PLATFORM := android-21
APP_MODULES := epixel
APP_STL := c++_shared
APP_GNUSTL_CPP_FEATURES        := exceptions rtti

APP_CPPFLAGS += -fexceptions -frtti
APP_GNUSTL_FORCE_CPP_FEATURES := rtti

