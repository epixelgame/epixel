-- Minetest: builtin/item.lua

--
-- Falling stuff
--

function check_attached_node(p, n)
	local def = core.registered_nodes[n.name]
	local d = {x=0, y=0, z=0}
	if def.paramtype2 == "wallmounted" then
		if n.param2 == 0 then
			d.y = 1
		elseif n.param2 == 1 then
			d.y = -1
		elseif n.param2 == 2 then
			d.x = 1
		elseif n.param2 == 3 then
			d.x = -1
		elseif n.param2 == 4 then
			d.z = 1
		elseif n.param2 == 5 then
			d.z = -1
		end
	else
		d.y = -1
	end
	local p2 = {x=p.x+d.x, y=p.y+d.y, z=p.z+d.z}
	local nn = core.get_node(p2).name
	local def2 = core.registered_nodes[nn]
	if def2 and not def2.walkable then
		return false
	end
	return true
end

--
-- Some common functions
--

function nodeupdate(p, delay)
	core.nodeupdate(p)
end

--
-- Global callbacks
--

function on_placenode(p, node)
	nodeupdate(p)
end
core.register_on_placenode(on_placenode)

function on_dignode(p, node)
	nodeupdate(p)
end
core.register_on_dignode(on_dignode)

function on_punchnode(p, node)
	nodeupdate(p)
end
core.register_on_punchnode(on_punchnode)
