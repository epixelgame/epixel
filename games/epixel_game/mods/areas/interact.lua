function minetest.is_protected(pos, name)
	if not minetest.area_caninteract(pos, name) then
		return true
	end
	return false
end

minetest.register_on_protection_violation(function(pos, name)
	if not minetest.area_caninteract(pos, name) then
		local owners = minetest.area_getnodeowners(pos)
		minetest.chat_send_player(name,
			("%s is protected by %s."):format(
				minetest.pos_to_string(pos),
				table.concat(owners, ", ")))
	end
end)


