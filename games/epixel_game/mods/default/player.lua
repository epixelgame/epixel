-- Minetest 0.4 mod: player
-- See README.txt for licensing and other information.

-- Player animation blending
-- Note: This is currently broken due to a bug in Irrlicht, leave at 0
local animation_blend = 0

default.registered_player_models = { }

-- Local for speed.
local models = default.registered_player_models

function default.player_register_model(name, def)
	models[name] = def
end

-- Player stats and animations
local player_model = {}
local player_textures = {}
local player_anim = {}
local player_sneak = {}
default.player_attached = {}

function default.player_set_textures(player, textures)
	local name = player:get_player_name()
	player_textures[name] = textures
	player:set_properties({textures = textures,})
end

-- Update appearance when the player joins
minetest.register_on_joinplayer(function(player)
	default.player_attached[player:get_player_name()] = false
	
	-- set GUI
	if not minetest.setting_getbool("creative_mode") then
		player:set_inventory_formspec(default.gui_survival_form)
	end
	player:hud_set_hotbar_image("gui_hotbar.png")
	player:hud_set_hotbar_selected_image("gui_hotbar_selected.png")
end)

minetest.register_on_leaveplayer(function(player)
	local name = player:get_player_name()
	player_anim[name] = nil
	player_textures[name] = nil
end)

-- Localize for better performance.
local player_attached = default.player_attached

-- Check each player and apply animations
