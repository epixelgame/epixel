minetest.override_item("default:dirt_with_grass", {
	groups = {crumbly=3,soil=1},
	soil = {
		base = "default:dirt_with_grass",
		dry = "farming:soil",
		wet = "farming:soil_wet"
	}
})

minetest.override_item("default:grass_1", {drop = {
	max_items = 1,
	items = {
		{items = {'farming:seed_wheat'},rarity = 5},
		{items = {'default:grass_1'}},
	}
}})
