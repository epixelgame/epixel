hud = {}

-- HUD statbar values
hud.health = {}

-- HUD item ids
local health_hud = {}

-- default settings

HUD_SCALEABLE = false
HUD_SIZE = ""

HUD_TICK = 0.1

HUD_ENABLE_HUNGER = minetest.setting_getbool("hud_hunger_enable")
if HUD_ENABLE_HUNGER == nil then
	HUD_ENABLE_HUNGER = minetest.setting_getbool("enable_damage")
end

--load custom settings
local set = io.open(minetest.get_modpath("hud").."/hud.conf", "r")
if set then 
	dofile(minetest.get_modpath("hud").."/hud.conf")
	set:close()
else
	if not HUD_ENABLE_HUNGER then
		HUD_AIR_OFFSET = HUD_HUNGER_OFFSET
	end
end

local function hide_builtin(player)
	 player:hud_set_flags({crosshair = true, hotbar = true, healthbar = false, wielditem = true, breathbar = false})
end


local function custom_hud(player)
 local name = player:get_player_name()

-- fancy hotbar (only when no crafting mod present)
 if minetest.get_modpath("crafting") == nil then
	player:hud_set_hotbar_image("gui_hotbar.png")
	player:hud_set_hotbar_selected_image("gui_hotbar_selected.png")
 end
end

if HUD_ENABLE_HUNGER then dofile(minetest.get_modpath("hud").."/hunger.lua") end

-- update hud elemtens if value has changed
local function update_hud(player)
	local name = player:get_player_name()
 --health
	local hp = tonumber(hud.health[name])
	if player:get_hp() ~= hp then
		hp = player:get_hp()
		hud.health[name] = hp
		player:hud_change(1, "number", hp)
	end
end

minetest.register_on_joinplayer(function(player)
	local name = player:get_player_name()
	local inv = player:get_inventory()
	inv:set_size("hunger",1)
	hud.health[name] = player:get_hp()
	minetest.after(0.5, function()
		hide_builtin(player)
		custom_hud(player)
	end)
end)

local main_timer = 0
local timer = 0
minetest.after(2.5, function()
	minetest.register_globalstep(function(dtime)
	 main_timer = main_timer + dtime
	 timer = timer + dtime
		if main_timer > HUD_TICK or timer > 4 then
		 if main_timer > HUD_TICK then main_timer = 0 end
		 for _,player in ipairs(minetest.get_connected_players()) do
			local name = player:get_player_name()
			 -- update all hud elements
			 update_hud(player)
		 end
		
		end
		if timer > 4 then timer = 0 end
	end)
end)
