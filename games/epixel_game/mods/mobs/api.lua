-- Mobs Api (26th July 2015)
mobs = {}
mobs.mod = "redo"

-- Initial settings check
local damage_enabled = minetest.setting_getbool("enable_damage")
local peaceful_only = minetest.setting_getbool("only_peaceful_mobs")
local enable_blood = minetest.setting_getbool("mobs_enable_blood") or true
mobs.protected = tonumber(minetest.setting_get("mobs_spawn_protected")) or 0

function mobs:register_mob(name, def)
	minetest.register_entity(name, {
		name = name,
		fly = def.fly,
		fly_in = def.fly_in or "air",
		owner = def.owner or "",
		order = def.order or "",
		on_die = def.on_die,
		do_custom = def.do_custom,
		--rotate = def.rotate or 0, -- 0=front, 1.5=side, 3.0=back, 4.5=side2
		physical = true,
		collisionbox = def.collisionbox,
		visual = def.visual,
		visual_size = def.visual_size or {x = 1, y = 1},
		mesh = def.mesh,
		makes_footstep_sound = def.makes_footstep_sound or false,
		damage = def.damage,
		water_damage = def.water_damage or 0,
		lava_damage = def.lava_damage or 0,
		fall_damage = def.fall_damage or 1,
		fall_speed = def.fall_speed or -10, -- must be lower than -2 (default: -10)
		on_rightclick = def.on_rightclick,
		type = def.type,
		attack_type = def.attack_type,
		arrow = def.arrow,
		shoot_interval = def.shoot_interval,
		sounds = def.sounds or {},
		attacks_monsters = def.attacks_monsters or false,
		group_attack = def.group_attack or false,
		--fov = def.fov or 120,
		passive = def.passive or false,
		recovery_time = def.recovery_time or 0.5,
		shoot_offset = def.shoot_offset or 0,
		timer = 0,
		attack = {player=nil, dist=nil},
		state = "stand",
		pause_timer = 0,
		health = 0,

		set_velocity = function(self, v)
			v = (v or 0)
			if def.drawtype
			and def.drawtype == "side" then
				self.rotate = math.rad(90)
			end
			local yaw = self.object:getyaw() + self.rotate
			local x = math.sin(yaw) * -v
			local z = math.cos(yaw) * v
			self.object:setvelocity({x = x, y = self.object:getvelocity().y, z = z})
		end,

		get_velocity = function(self)
			local v = self.object:getvelocity()
			return (v.x ^ 2 + v.z ^ 2) ^ (0.5)
		end,

		set_animation = function(self, type)
		end,

		get_staticdata = function(self)
			local tmp = {}
			for _,stat in pairs(self) do
				local t = type(stat)
				if  t ~= 'function'
				and t ~= 'nil'
				and t ~= 'userdata' then
					tmp[_] = self[_]
				end
			end
			-- print('===== '..self.name..'\n'.. dump(tmp)..'\n=====\n')
			return minetest.serialize(tmp)
		end,
	})
end

-- Spawn Egg
function mobs:register_egg(mob, desc, background, addegg)
	local invimg = background
	if addegg == 1 then
		invimg = invimg.."^mobs_chicken_egg.png"
	end
	minetest.register_craftitem(mob, {
		description = desc,
		inventory_image = invimg,
		on_place = function(itemstack, placer, pointed_thing)
			local pos = pointed_thing.above
			if pointed_thing.above
			and not minetest.is_protected(pos, placer:get_player_name()) then
				pos.y = pos.y + 0.5
				local mob = minetest.add_creature(pos, mob)
				local ent = mob:get_luaentity()
				if ent.type ~= "monster" then
					-- set owner
					ent.owner = placer:get_player_name()
					ent.tamed = true
				end
				itemstack:take_item()
			end
			return itemstack
		end,
	})
end
