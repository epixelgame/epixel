
-- Bee by KrupnoPavel

mobs:register_mob("mobs:bee", {
	type = "animal",
	passive = true,
	collisionbox = {-0.2, -0.01, -0.2, 0.2, 0.2, 0.2},
	visual = "mesh",
	makes_footstep_sound = false,
})

mobs:register_egg("mobs:bee", "Bee", "mobs_bee_inv.png", 0)

-- beehive (when placed spawns bee)
minetest.register_node("mobs:beehive", {
	description = "Beehive",
	drawtype = "plantlike",
	visual_scale = 1.0,
	tiles = {"mobs_beehive.png"},
	inventory_image = "mobs_beehive.png",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = true,
	groups = {fleshy = 3, dig_immediate = 3},
	sounds = default.node_sound_defaults(),
	after_place_node = function(pos, placer, itemstack)
		if placer:is_player() then
			minetest.set_node(pos, {name = "mobs:beehive", param2 = 1})
			minetest.add_creature(pos, "mobs:bee")
		end
	end,

})
