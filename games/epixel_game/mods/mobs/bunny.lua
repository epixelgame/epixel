
-- Bunny by ExeterDad

mobs:register_mob("mobs:bunny", {
	type = "animal",
	passive = true,
	visual = "mesh",
	drawtype = "front",
	makes_footstep_sound = false,
	run_velocity = 2,
	follow = "farming:carrot",
	damage = 5,
})

mobs:register_egg("mobs:bunny", "Bunny", "mobs_bunny_inv.png", 0)
