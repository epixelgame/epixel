
-- Dungeon Master by PilzAdam

mobs:register_mob("mobs:dungeon_master", {
	type = "monster",
	passive = false,
	shoot_interval = 2.5,
	arrow = "mobs:fireball",
	shoot_offset = 1,
	visual = "mesh",
	makes_footstep_sound = true,
	run_velocity = 3,
})

mobs:register_egg("mobs:dungeon_master", "Dungeon Master", "fire_basic_flame.png", 1)
