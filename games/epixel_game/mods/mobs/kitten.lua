
-- Kitten by Jordach / BFD

mobs:register_mob("mobs:kitten", {
	type = "animal",
	passive = true,
	visual = "mesh",
	visual_size = {x = 0.5, y = 0.5},
	makes_footstep_sound = false,
	follow = "mobs:rat",
})

mobs:register_egg("mobs:kitten", "Kitten", "mobs_kitten_inv.png", 0)
