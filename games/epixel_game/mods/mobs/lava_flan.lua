
-- Lava Flan by Zeg9

mobs:register_mob("mobs:lava_flan", {
	type = "monster",
	passive = false,
	visual = "mesh",
	blood_texture = "fire_basic_flame.png",
	makes_footstep_sound = false,
	run_velocity = 2,
	on_die = function(self, pos)
		minetest.set_node(pos, {name = "fire:basic_flame"})
	end,
})

mobs:register_egg("mobs:lava_flan", "Lava Flan", "default_lava.png", 1)

minetest.register_alias("zmobs:lava_orb", "mobs:lava_orb")
