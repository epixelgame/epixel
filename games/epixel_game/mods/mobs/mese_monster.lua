
-- Mese Monster by Zeg9

mobs:register_mob("mobs:mese_monster", {
	type = "monster",
	passive = false,
	shoot_interval = .5,
	arrow = "mobs:mese_arrow",
	shoot_offset = 2,
	visual = "mesh",
	blood_texture = "default_mese_crystal_fragment.png",
	makes_footstep_sound = false,
	run_velocity = 2,
})

mobs:register_egg("mobs:mese_monster", "Mese Monster", "default_mese_block.png", 1)
