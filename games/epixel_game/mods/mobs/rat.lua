
-- Rat by PilzAdam

mobs:register_mob("mobs:rat", {
	type = "animal",
	passive = true,
	visual = "mesh",
	makes_footstep_sound = false,
})

mobs:register_egg("mobs:rat", "Rat", "mobs_rat_inventory.png", 0)
