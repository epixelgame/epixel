
-- Spider by AspireMint (fishyWET (CC-BY-SA 3.0 license for texture)

mobs:register_mob("mobs:spider", {
	type = "monster",
	passive = false,
	attack_type = "dogfight",
	visual = "mesh",
	visual_size = {x = 7, y = 7},
	makes_footstep_sound = false,
	run_velocity = 3,
})

mobs:register_egg("mobs:spider", "Spider", "mobs_cobweb.png", 1)

-- ethereal crystal spike compatibility
if not minetest.get_modpath("ethereal") then
	minetest.register_alias("ethereal:crystal_spike", "default:sandstone")
end
