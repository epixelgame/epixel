
-- Warthog by KrupnoPavel

mobs:register_mob("mobs:pumba", {
	type = "animal",
	passive = false,
	visual = "mesh",
	makes_footstep_sound = true,
	run_velocity = 3,
	follow = "default:apple",
})

mobs:register_egg("mobs:pumba", "Warthog", "wool_pink.png", 1)
