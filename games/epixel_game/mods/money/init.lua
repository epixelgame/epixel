--[[
	Mod by Kotolegokot and Xiong (2012-2013)
	Rev. kilbith and nerzhul (2015)
]]

money = {}

dofile(minetest.get_modpath("money") .. "/settings.txt") -- Loading settings.

function money.exist(name)
	return minetest.get_money(name) ~= nil
end

local save_accounts = money.save_accounts
local exist = money.exist

minetest.register_privilege("money", "Can use /money [pay <account> <amount>] command")
minetest.register_privilege("money_admin", {
	description = "Can use /money <account> | take/set/inc/dec <account> <amount>",
	give_to_singleplayer = false,
})

minetest.register_chatcommand("money", {
	privs = {money=true},
	params = "[<account> | pay/take/set/inc/dec <account> <amount>]",
	description = "Operations with money",
	func = function(name, param)
		if param == "" then --/money
			minetest.chat_send_player(name, "My money account : " .. CURRENCY_PREFIX .. minetest.get_money(name) .. CURRENCY_POSTFIX)
			return true
		end
		local m = string.split(param, " ")
		local param1, param2, param3 = m[1], m[2], m[3]
		if param1 and not param2 then --/money <account>
			if minetest.get_player_privs(name)["money_admin"] then
				if exist(param1) then
					minetest.chat_send_player(name, "Account of player '" .. param1 .. "' : " .. CURRENCY_PREFIX .. minetest.get_money(param1) .. CURRENCY_POSTFIX)
				else
					minetest.chat_send_player(name, "\"" .. param1 .. "\" account don't exist.")
				end
			else
				minetest.chat_send_player(name, "You don't have permission to run this command (missing privilege: money_admin)")
			end
			return true
		end
		if param1 and param2 and param3 then --/money pay/take/set/inc/dec <account> <amount>
			if param1 == "pay" or param1 == "take" or param1 == "set" or param1 == "inc" or param1 == "dec" then
				if exist(param2) then
					if tonumber(param3) then
						if tonumber(param3) >= 0 then
							param3 = tonumber(param3)
							if param1 == "pay" then
								if minetest.get_money(name) >= param3 then
									minetest.set_money(param2, minetest.get_money(param2) + param3)
									minetest.set_money(name, minetest.get_money(name) - param3)
									minetest.chat_send_player(param2, name .. " sent you " .. CURRENCY_PREFIX .. param3 .. CURRENCY_POSTFIX .. ".")
									minetest.chat_send_player(name, param2 .. " took your " .. CURRENCY_PREFIX .. param3 .. CURRENCY_POSTFIX .. ".")
								else
									minetest.chat_send_player(name, "You don't have " .. CURRENCY_PREFIX .. param3 - minetest.get_money(name) .. CURRENCY_POSTFIX .. ".")
								end
								return true
							end
							if minetest.get_player_privs(name)["money_admin"] then
								if param1 == "take" then
									if minetest.get_money(param2) >= param3 then
										minetest.set_money(param2, minetest.get_money(param2) - param3)
										minetest.set_money(name, minetest.get_money(name) + param3)
										minetest.chat_send_player(param2, name .. " took your " .. CURRENCY_PREFIX .. param3 .. CURRENCY_POSTFIX .. ".")
										minetest.chat_send_player(name, "You took " .. param2 .. "'s " .. CURRENCY_PREFIX .. param3 .. CURRENCY_POSTFIX .. ".")
									else
										minetest.chat_send_player(name, "Player named \""..param2.."\" do not have enough " .. CURRENCY_PREFIX .. param3 - minetest.get_money(player) .. CURRENCY_POSTFIX .. ".")
									end
								elseif param1 == "set" then
									minetest.set_money(param2, param3)
									minetest.chat_send_player(name, param2 .. " " .. CURRENCY_PREFIX .. param3 .. CURRENCY_POSTFIX)
								elseif param1 == "inc" then
									minetest.set_money(param2, minetest.get_money(param2) + param3)
									minetest.chat_send_player(name, param2 .. " " .. CURRENCY_PREFIX .. minetest.get_money(param2) .. CURRENCY_POSTFIX)
								elseif param1 == "dec" then
									if minetest.get_money(param2) >= param3 then
										minetest.set_money(param2, minetest.get_money(param2) - param3)
										minetest.chat_send_player(name, param2 .. " " .. CURRENCY_PREFIX .. minetest.get_money(param2) .. CURRENCY_POSTFIX)
									else
										minetest.chat_send_player(name, "Player named \""..param2.."\" don't have enough " .. CURRENCY_PREFIX .. param3 - minetest.get_money(player) .. CURRENCY_POSTFIX .. ".")
									end
								end
							else
								minetest.chat_send_player(name, "You don't have permission to run this command (missing privilege: money_admin)")
							end
						else
							minetest.chat_send_player(name, "You must specify a positive amount.")
						end
					else
						minetest.chat_send_player(name, "The amount must be a number.")
					end
				else
					minetest.chat_send_player(name, "\"" .. param2 .. "\" account don't exist.")
				end
				return true
			end
		end
		minetest.chat_send_player(name, "Invalid parameters (see /help money)")
	end,
})

local function has_shop_privilege(meta, player)
	return player:get_player_name() == meta:get_string("owner") or minetest.get_player_privs(player:get_player_name())["money_admin"]
end
