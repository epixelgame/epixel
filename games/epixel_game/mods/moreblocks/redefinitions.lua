--[[
More Blocks: redefinitions of default stuff

Copyright (c) 2011-2015 Calinou and contributors.
Licensed under the zlib license. See LICENSE.md for more information.
--]]

-- Redefinitions of some default crafting recipes:

minetest.register_craft({
	type = "toolrepair",
	additional_wear = -0.15, -- Tool repair buff (15% bonus instead of 2%).
})

-- Redefinitions of some default nodes
-- ===================================

-- Let there be light. This makes some nodes let light pass through:
minetest.override_item("default:papyrus", {
	paramtype = "light",
	sunlight_propagates = true,
})

minetest.override_item("default:grass_1", {
	inventory_image = "default_grass_3.png", -- Use a bigger inventory image.
	wield_image = "default_grass_3.png",
	paramtype = "light",
	sunlight_propagates = true,
})
