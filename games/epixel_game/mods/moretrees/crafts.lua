local S = moretrees.intllib

-- Food recipes!

for i in ipairs(moretrees.cutting_tools) do
	local tool = moretrees.cutting_tools[i]
	minetest.register_craft({
		type = "shapeless",
		output = "moretrees:coconut_milk",
		recipe = {
			"moretrees:coconut",
			"vessels:drinking_glass",
			tool
		},
		replacements = {
			{ "moretrees:coconut", "moretrees:raw_coconut" },
			{ tool, tool }
		}
	})
end

minetest.register_craft({
	type = "shapeless",
	output = "moretrees:acorn_muffin_batter",
	recipe = {
		"moretrees:acorn",
		"moretrees:acorn",
		"moretrees:acorn",
		"moretrees:acorn",
		"moretrees:coconut_milk",
	},
	replacements = {
		{ "moretrees:coconut_milk", "vessels:drinking_glass" }
	}
})
