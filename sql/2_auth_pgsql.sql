CREATE TABLE IF NOT EXISTS users (
    id SERIAL,
    login TEXT UNIQUE,
    password TEXT,
    lastlogin INT DEFAULT 0,
    account_lock BOOLEAN NOT NULL DEFAULT 'f',
    CONSTRAINT uid UNIQUE (id)
);

UPDATE dbversion set version = 55 WHERE dbname = 'auth';