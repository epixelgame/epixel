CREATE TABLE users (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	login VARCHAR(64) UNIQUE,
	password VARCHAR(64),
	lastlogin INT NOT NULL DEFAULT 0,
	account_lock INT NOT NULL DEFAULT 0
);

UPDATE dbversion set version = 55 WHERE dbname = 'auth';