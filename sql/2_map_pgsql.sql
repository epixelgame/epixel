CREATE TABLE IF NOT EXISTS blocks (
    posX INT NOT NULL,
    posY INT NOT NULL,
    posZ INT NOT NULL,
    block_data BYTEA,
    b_version INT NOT NULL DEFAULT 26,
    eb_version INT NOT NULL DEFAULT 0,
    b_flags SMALLINT NOT NULL DEFAULT 0,
    b_content_width SMALLINT NOT NULL DEFAULT 2,
    b_params_width SMALLINT NOT NULL DEFAULT 2,
    b_nodes BYTEA NOT NULL DEFAULT '',
    b_nodes_metas BYTEA NOT NULL DEFAULT '',
    b_time TIMESTAMP NOT NULL DEFAULT NOW(),
    PRIMARY KEY (posX,posY,posZ)
);

CREATE TABLE env_meta (
    game_time BIGINT NOT NULL DEFAULT '0',
    time_of_day BIGINT NOT NULL DEFAULT '0',
    automapgen_offset INT NOT NULL DEFAULT 0,
    lbm_introduction_times_version BIGINT NOT NULL DEFAULT '1',
    lbm_introduction_times TEXT NOT NULL DEFAULT '',
    day_count INT NOT NULL DEFAULT 0
);

INSERT INTO env_meta (game_time, time_of_day) VALUES (0, 0);

UPDATE dbversion set version = 55 WHERE dbname = 'map';