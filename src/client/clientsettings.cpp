/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "clientsettings.h"
#include <json/json.h>
#include <fstream>
#include "porting.h"
#include "filesys.h"
#include "log.h"

namespace epixel
{

// Define static resources here
std::atomic_bool ClientSettings::m_bool_settings[BOOLCSETTING_COUNT];

struct CBDSetting
{
	const char* setting_name;
	bool value;
};

static const CBDSetting cbd_opts[BOOLCSETTING_COUNT] =
{
	{"enable_always_fly_fast", true},
	{"doubletap_jump", false},
	{"enable_minimap", true},
	{"fast_move", false},
	{"free_move", false},
	{"minimap_shape_round", true},
	{"enable_noclip", false},
	{"enable_cinematic_mode", false},
	{"enable_vbo", false},
	{"enable_aux1_descends", false},
};

bool ClientSettings::load()
{
	load_default();

	std::string config_file = porting::path_user + DIR_DELIM + "epixel-client.json";
	if (fs::PathExists(config_file)) {
		Json::Value root;
		std::ifstream config_f(config_file, std::ifstream::binary);
		try {
			config_f >> root;

			for (u32 i=0; i < BOOLCSETTING_COUNT; i++) {
				if (root.isMember(cbd_opts[i].setting_name) && root[cbd_opts[i].setting_name].isBool()) {
					m_bool_settings[i] = root[cbd_opts[i].setting_name].asBool();
				}
			}

			config_f.close();
		}
		catch (std::exception &e) {
			logger.warn("Unable to read settings from %s, use default settings. Error was %s",
						config_file.c_str(), e.what());
		}
	}

	return true;
}

bool ClientSettings::save()
{
	Json::Value root;

	// Write settings to JSON
	for (u32 i=0; i < BOOLCSETTING_COUNT; i++) {
		root[cbd_opts[i].setting_name] = (bool) m_bool_settings[i];
	}

	std::string config_file = porting::path_user + DIR_DELIM + "epixel-client.json";
	try {
		std::ofstream config_f(config_file, std::ofstream::binary);
		config_f << root;
		config_f.close();
	}
	catch (std::exception &e) {
		logger.error("Failed to write client config to %s. Error: %s", config_file.c_str(), e.what());
	}

	return true;
}

void ClientSettings::load_default()
{
	for (u32 i=0; i < BOOLCSETTING_COUNT; i++) {
		m_bool_settings[i] = cbd_opts[i].value;
	}
}

}
