/*
Minetest
Copyright (C) 2013 celeron55, Perttu Ahola <celeron55@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

/*
SQLite format specification:
	blocks:
		(PK) INT id
		BLOB data
*/

#include <fstream>
#include <codecvt>
#include <string>
#include <locale>
#include "database-sqlite3.h"
#include "contrib/abms/treeabm.h"
#include "contrib/areas.h"
#include "contrib/awards.h"
#include "contrib/ban.h"
#include "contrib/creaturestore.h"
#include "contrib/playersao.h"
#include "contrib/spell.h"
#include "contrib/teleport.h"
#include "craftdef.h"
#include "log.h"
#include "mapblock.h"
#include "mg_biome.h"
#include "mg_ore.h"
#include "mg_schematic.h"
#include "nodedef.h"
#include "filesys.h"
#include "content_sao.h"
#include "settings.h"
#include "environment.h"
#include "porting.h"
#include "scripting_game.h"
#include "tool.h"
#include "util/string.h"

#include <cassert>

// When to print messages when the database is being held locked by another process
// Note: I've seen occasional delays of over 250ms while running minetestmapper.
#define BUSY_INFO_TRESHOLD	100	// Print first informational message after 100ms.
#define BUSY_WARNING_TRESHOLD	250	// Print warning message after 250ms. Lag is increased.
#define BUSY_ERROR_TRESHOLD	1000	// Print error message after 1000ms. Significant lag.
#define BUSY_FATAL_TRESHOLD	3000	// Allow SQLITE_BUSY to be returned, which will cause a minetest crash.
#define BUSY_ERROR_INTERVAL	10000	// Safety net: report again every 10 seconds

#define PREPARE_STATEMENT(name, query) \
	sqlite3_vrfy(sqlite3_prepare_v2(m_database, query, -1, &m_stmt_##name, NULL),\
		"Failed to prepare query '" query "'");

#define FINALIZE_STATEMENT(statement) \
	sqlite3_vrfy(sqlite3_finalize(statement), "Failed to finalize " #statement);

static const char* stmt_list[SQLITE3STMT_COUNT] = {
	"SELECT `data`, `b_version`, `eb_version`, `b_flags`, `b_content_width`, `b_params_width`, `b_nodes`, `b_nodes_metas`, `b_time` FROM `blocks` "
			"WHERE `pos` = ? LIMIT 1",
	"REPLACE INTO `blocks` (`pos`, `data`, `b_version`, `eb_version`, `b_flags`, `b_content_width`, `b_params_width`, `b_nodes`, `b_nodes_metas`,"
			"`b_time`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
	"SELECT `pos` FROM `blocks`",
	"DELETE FROM `blocks` WHERE `pos` = ?",
	"DELETE FROM `env_meta`",
	"INSERT INTO `env_meta` (`game_time`, `time_of_day`, `automapgen_offset`,`lbm_introduction_times_version`, `lbm_introduction_times`, `day_count`) VALUES (?, ?, ?, ?, ?, ?)",
	"SELECT `game_time`, `time_of_day`, `automapgen_offset`, `lbm_introduction_times_version`, `lbm_introduction_times`, `day_count` FROM `env_meta`",
	"", // MAP END / AUTH START
	"INSERT INTO `users` (`login`, `password`) VALUES (?, ?)",
	"SELECT `id`, `password`, `lastlogin`, `account_lock` FROM `users` WHERE `login` = ?",
	"SELECT `login`, `password`, `lastlogin`, `account_lock` FROM `users` WHERE `id` = ?",
	"DELETE FROM `users` WHERE `login` = ?",
	"SELECT `login` FROM `users` WHERE lower(`login`) = lower(?)",
	"UPDATE `users` SET `password` = ? WHERE `login` = ?",
	"SELECT `lastlogin` FROM `users` WHERE `id` = ?",
	"UPDATE `users` SET `lastlogin` = strftime('%s','now') WHERE `login` = ?",
	"SELECT `login` FROM `users` WHERE `id` = ?",
	"", // AUTH END / GAME START
	"REPLACE INTO `areas` (`id`, `name`, `parent_id`, `pos1`, `pos2`, `isopen`, `pvpflag`) VALUES (?, ?, ?, ?, ?, ?, ?)",
	"INSERT INTO `area_owners` (`area_id`, `user_id`) VALUES (?, ?)",
	"REPLACE INTO `bans` (`playername`, `bantime`, `source`, `reason`) VALUES (?, ?, ?, ?)",
	"REPLACE INTO `bans_ip` (`ip`, `bantime`, `source`, `reason`) VALUES (?, ?, ?, ?)",
	"SELECT `id`, `name`, `parent_id`, `pos1`, `pos2`, `isopen`, `pvpflag` FROM `areas`",
	"SELECT `user_id` FROM `area_owners` WHERE `area_id` = ?",
	"DELETE FROM `areas` WHERE `id` = ?",
	"DELETE FROM `area_owners` WHERE `area_id` = ?",
	"SELECT `playername`, `bantime`, `source`, `reason` FROM `bans`",
	"SELECT `ip`, `bantime`, `source`, `reason` FROM `bans_ip`",
	"DELETE FROM `bans` WHERE `playername` = ?",
	"DELETE FROM `bans_ip` WHERE `ip` = ?",
	"SELECT `tip_en` FROM `tips`",
	"SELECT `creaturedefs`.`name`, `creaturetype`, `attacktype`, `canfloat`, `canjump`, `viewrange`, `replacerate`, `replaceoffset`, "
			"`collisionbox1`, `collisionbox2`, `collisionbox3`, `collisionbox4`, `collisionbox5`, `collisionbox6`, "
			"can_take_fall_damage, fall_speed, jump_height, walk_chance, walk_velocity, damage_min, `loot_table`, `nodereplace_table`, "
			"`min_hp`, `max_hp`, `armor`, `visualsize_x`, `visualsize_y`, `xp_min`, `xp_max`,"
			"speed_normal, stand_start, stand_end, walk_start, walk_end, run_start, run_end, speed_run, punch_start, punch_end, "
			"sound_warcry, sound_random, sound_attack, sound_death, sound_damage, sound_jump, "
			"damage_light, damage_water, damage_lava, damage_fall "
			"FROM `creaturedefs` "
			"LEFT OUTER JOIN creaturedefs_animationinfos ON (creaturedefs.name = creaturedefs_animationinfos.name) "
			"LEFT OUTER JOIN creaturedefs_sounds ON (creaturedefs.name = creaturedefs_sounds.name) "
			"LEFT OUTER JOIN creaturedefs_damages ON (creaturedefs.name = creaturedefs_damages.name)",
	"SELECT `id`, `item`, `chance`, `mindrops`, `maxdrops` FROM `creaturedefs_loot`",
	"SELECT `id`, `sourcenode`, `destnode` FROM `creaturedefs_nodereplace`",
	"SELECT `mesh` FROM creaturedefs_meshes WHERE `name` = ?",
	"SELECT `texture` FROM creaturedefs_textures WHERE `name` = ?",
	"SELECT `spell_name` FROM creaturedefs_spells WHERE `creature_name` = ?",
	"SELECT `name`, `interval`, `chance`, `min_light`, `max_light`, `max_count_per_area`, `min_height`, `max_height` "
			"FROM `creaturedefs_spawning_abms` WHERE `name` in (SELECT `name` FROM `creaturedefs`)",
	"SELECT `nodename` FROM `creaturedefs_spawning_nodes` WHERE `name` = ?",
	"SELECT `priv_name` FROM `user_privs` WHERE `user_id` = ?",
	"INSERT INTO `user_privs` (`user_id`, `priv_name`) VALUES (?, ?)",
	"DELETE FROM `user_privs` WHERE `user_id` = ? AND `priv_name` = ?",
	"DELETE FROM `user_privs` WHERE `user_id` = ?",
	"INSERT INTO `user_channels` (`user_id`, `channel`) VALUES (?, ?)",
	"DELETE FROM `user_channels` WHERE `user_id` = ? AND `channel` = ?",
	"SELECT `channel` FROM `user_channels` WHERE `user_id` = ?",
	"SELECT `craftoutput`, `craftwidth`, `recipe1`, `recipe2`, `recipe3`, `recipe4`, `recipe5`, `recipe6`, `recipe7`, `recipe8`, `recipe9` FROM `craft_shaped`",
	"SELECT `craftoutput`, `recipe`, `cooktime` FROM `craft_cooking`",
	"SELECT `recipe`, `burntime` FROM `craft_fuel`",
	"SELECT `craftoutput`, `recipe1`, `recipe2`, `recipe3`, `recipe4`, `recipe5`, `recipe6`, `recipe7`, `recipe8`, `recipe9` FROM `craft_shapeless`",
	"SELECT `attr`, `value` FROM `player_ext_attributes` WHERE `user_id` = ?",
	"INSERT INTO `player_ext_attributes` (`user_id`, `attr`, `value`) VALUES (?, ?, ?)",
	"DELETE FROM `player_ext_attributes` WHERE `user_id` = ?",
	"INSERT INTO `player` (`user_id`, `pitch`, `yaw`, `posX`, `posY`, `posZ`, `hp`, `breath`, `money`, `hunger`, `mana`, `xp`, `rules_accepted`) VALUES "
		"(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
	"UPDATE `player` SET `pitch` = ?, `yaw` = ?, `posX` = ?, `posY` = ?, `posZ` = ?, `hp` = ?, `breath` = ?, `money` = ?, `hunger` = ?, `mana` = ?, `xp` = ? "
		"WHERE user_id = ?",
	"DELETE FROM `player_inventories` WHERE `user_id` = ?",
	"DELETE FROM `player_inventory_items` WHERE `user_id` = ?",
	"INSERT INTO `player_inventories` (`user_id`, `inv_id`, `inv_width`, `inv_name`, `inv_size`) VALUES (?, ?, ?, ?, ?)",
	"INSERT INTO `player_inventory_items` (`user_id`, `inv_id`, `slot_id`, `item`) VALUES (?, ?, ?, ?)",
	"SELECT `inv_id`, `inv_width`, `inv_name`, `inv_size` FROM `player_inventories` WHERE `user_id` = ? ORDER BY inv_id",
	"SELECT `slot_id`, `item` FROM `player_inventory_items` WHERE `user_id` = ? AND `inv_id` = ?",
	"SELECT `pitch`, `yaw`, `posX`, `posY`, `posZ`, `hp`, `breath`, `money`, `hunger`, `homeX`, `homeY`, `homeZ`, `mana`, `xp`, `rules_accepted` "
		"FROM `player` WHERE `user_id` = ?",
	"UPDATE `player` SET `homeX` = ?, `homeY` = ?, `homeZ` = ? WHERE `user_id` = ?",
	"SELECT `id`,`axiom`,`rules_a`,`rules_b`,`rules_c`,`rules_d`,`trunk`,`leaves`,`angle`,`iterations`,`random_level`,`trunk_type`,`thin_branches`,`fruit`,`fruit_chance`,`abm_interval`,`abm_chance` FROM `abm_tree_scheme`",
	"SELECT `sapling` FROM abm_tree_sapling WHERE `id` = ?",
	"SELECT `node_name`, `node_replace_name`, `interval`, `chance` FROM `abm_nodereplace`",
	"SELECT `oredefs`.`ore_id`, `ore_name`, `ore_type`, `ore_flags`, `clust_scarcity`, `clust_num_ores`, `clust_size`, `noise_threshhold`, `noise_params`, `y_min`, `y_max`, "
			"`noise_offset`, `noise_scale`, `noise_spread_x`, `noise_spread_y`, `noise_spread_z`, `noise_persist`, `noise_lacunarity`, `noise_seed`, `noise_octaves`, `noise_flags` "
			"FROM `oredefs` "
			"LEFT OUTER JOIN `oredefs_noise` ON (`oredefs`.`ore_id` = `oredefs_noise`.`ore_id`)",
	"SELECT `into_name` FROM `oredefs_pop_into` WHERE `ore_id` = ?",
	"SELECT `itemdefs`.`name`, `description`, `type`, `inventory_image`, `wield_image`, `wield_scale_x`, `wield_scale_y`, `wield_scale_z`, "
			"`stack_max`, `item_range`, `usable`, `liquids_pointable`, `node_placement_prediction`, `eat_amount`, `replace_with`,"
			"`type_extended` FROM `itemdefs` "
			"LEFT OUTER JOIN `itemdefs_eat` ON (`itemdefs`.`name` = `itemdefs_eat`.`name`)",
	"SELECT `group_name`, `group_value` FROM `itemdefs_groups` WHERE `item_name` = ?",
	"SELECT `sound_name`, `sound_file`, `sound_gain` FROM `itemdefs_sounds` WHERE `item_name` = ?",
	"SELECT `full_punch_interval`, `max_drop_level` FROM `itemdefs_tool_capabilities` WHERE `item_name` = ?",
	"SELECT `group_name`, `group_value` FROM `itemdefs_tool_capabilities_damage_groups` WHERE `item_name` = ?",
	"SELECT `groupcap_name`, `maxlevel`, `uses` FROM `itemdefs_tool_capabilities_groupcap` WHERE `item_name` = ?",
	"SELECT `time_id`, `time_value` FROM `itemdefs_tool_capabilities_groupcap_times` WHERE `item_name` = ? AND `groupcap_name` = ?",
	"SELECT `drawtype`,`visual_scale`,`mesh`,`alpha`,`param_type`,`param_type2`,`is_ground_content`,`sunlight_propagates`,"
			"`walkable`,`pointable`,`diggable`,`climbable`,`buildable_to`,`rightclickable`,`liquid_type`,`liquid_alternative_flowing`,"
			"`liquid_alternative_source`,`liquid_viscosity`,`liquid_range`,`leveled`,`drowning`,`light_source`,`damage_per_second`,"
			"`waving`,`legacy_facedir_simple`,`legacy_wallmounted`,`tile1`,`tile2`,`tile3`,`tile4`,`tile5`,`tile6`,`base`,`dry`,`wet`,"
			"`max_drops`,`droppable`,`nodebox_type`,`collisionbox_type`,`selectionbox_type`,`rightclick_formspec` "
			"FROM `itemdefs_nodes` "
			"LEFT OUTER JOIN `itemdefs_nodes_tiles` ON (`itemdefs_nodes`.`item_name` = `itemdefs_nodes_tiles`.`item_name`) "
			"LEFT OUTER JOIN `itemdefs_nodes_soil` ON (`itemdefs_nodes`.`item_name` = `itemdefs_nodes_soil`.`item_name`) "
			"LEFT OUTER JOIN `itemdefs_nodeboxes` ON (`itemdefs_nodes`.`item_name` = `itemdefs_nodeboxes`.`item_name`) "
			"WHERE `itemdefs_nodes`.`item_name` = ?",
	"SELECT `drop_name`, `drop_rarity`, `min_items`, `max_items` FROM `itemdefs_nodes_drops` WHERE `item_name` = ?",
	"SELECT `nodebox_id`, `minedge_x`,`minedge_y`,`minedge_z`,`maxedge_x`,`maxedge_y`,`maxedge_z` FROM `itemdefs_nodeboxes_components` "
			"WHERE `item_name` = ? AND `nodebox_id` = ?",
	"SELECT `nodebox_id`, `minedge_x`,`minedge_y`,`minedge_z`,`maxedge_x`,`maxedge_y`,`maxedge_z` FROM `itemdefs_nodeboxes_components` "
			"WHERE `item_name` = ? AND `nodebox_id` IN (?,?,?)",
	"SELECT `name`,`visual`,`visual_size_x`,`visual_size_y`,`texture`,`velocity`,`damages_player`,`damages_npc`,`damagegroup`,"
			"`damages_nodes_radius`,`max_range`,`timer`, `spell_target_type` FROM `spelldefs`",
	"SELECT `id`, `name`, `image`, `description`, `achievement_type`, `achievement_target`, `achievement_target_what` FROM `achievement`",
	"SELECT `id_achievement`, `achievement_progress`, `unlocked` FROM `user_awards` WHERE `id_player` = ?",
	"INSERT INTO `user_awards` (`id_player`, `id_achievement`, `achievement_progress`, `unlocked`) VALUES (?, ?, ?, ?)",
	"DELETE FROM `user_awards` WHERE `id_player` = ? AND `id_achievement` = ?",
	"INSERT INTO `teleporter_location` (`loc_x`, `loc_y`, `loc_z`, `description`) VALUES (?, ?, ?, ?)",
	"DELETE FROM `teleporter_location` WHERE `loc_x` = ? AND `loc_y` = ? AND `loc_z` = ?",
	"SELECT `loc_x`, `loc_y`, `loc_z`, `description` FROM `teleporter_location`",
	"SELECT `name`, `node_dust`, `node_top`, `depth_top`, `node_filler`, `depth_filler`, `node_stone`, `node_water_top`, `depth_water_top`, `node_water`, `y_min`, `y_max`, `heat_point`, `humidity_point` FROM `biomes` WHERE `name` IN (SELECT `biome_name` FROM `biomes_mapgen` WHERE `mapgen` = ?)",
	"SELECT `id`, `name`, `type`, `place_on`, `sidelen`, `fill_ratio`, `np_offset`, `np_scale`, `np_spread_x`, `np_spread_y`, `np_spread_z`, `np_seed`, `np_octaves`, `np_persist`, `y_min`, `y_max`, `schematic`, `flags`, `rotation`, `decoration`, `height`, `height_max`, `spawn_by`, `num_spawn_by` FROM `decorations`",
	"SELECT `decoration_id`, `biome_name` FROM `decoration_biomes` WHERE `decoration_id` = ?",
	"SELECT `ignored_player` FROM `player_ignored_players` WHERE `user_id` = ?",
	"INSERT INTO `player_ignored_players`(`user_id`,`ignored_player`) VALUES (?, ?)",
	"DELETE FROM `player_ignored_players` WHERE `user_id` = ? AND `ignored_player` = ?",
	"", // GAME END
	"BEGIN",
	"COMMIT",
};

int Database_SQLite3::busyHandler(void *data, int count)
{
	s64 &first_time = reinterpret_cast<s64 *>(data)[0];
	s64 &prev_time = reinterpret_cast<s64 *>(data)[1];
	s64 cur_time = getTimeMs();

	if (count == 0) {
		first_time = cur_time;
		prev_time = first_time;
	} else {
		while (cur_time < prev_time)
			cur_time += s64(1)<<32;
	}

	if (cur_time - first_time < BUSY_INFO_TRESHOLD) {
		; // do nothing
	} else if (cur_time - first_time >= BUSY_INFO_TRESHOLD &&
			prev_time - first_time < BUSY_INFO_TRESHOLD) {
		logger.info("SQLite3 database has been locked for %d ms.", cur_time - first_time);
	} else if (cur_time - first_time >= BUSY_WARNING_TRESHOLD &&
			prev_time - first_time < BUSY_WARNING_TRESHOLD) {
		logger.warnStream() << "SQLite3 database has been locked for "
			<< cur_time - first_time << " ms.\n";
	} else if (cur_time - first_time >= BUSY_ERROR_TRESHOLD &&
			prev_time - first_time < BUSY_ERROR_TRESHOLD) {
		logger.errorStream() << "SQLite3 database has been locked for "
			<< cur_time - first_time << " ms; this causes lag.\n";
	} else if (cur_time - first_time >= BUSY_FATAL_TRESHOLD &&
			prev_time - first_time < BUSY_FATAL_TRESHOLD) {
		logger.errorStream() << "SQLite3 database has been locked for "
			<< cur_time - first_time << " ms - giving up!\n";
	} else if ((cur_time - first_time) / BUSY_ERROR_INTERVAL !=
			(prev_time - first_time) / BUSY_ERROR_INTERVAL) {
		// Safety net: keep reporting every BUSY_ERROR_INTERVAL
		logger.errorStream() << "SQLite3 database has been locked for "
			<< (cur_time - first_time) / 1000 << " seconds!\n";
	}

	prev_time = cur_time;

	// Make sqlite transaction fail if delay exceeds BUSY_FATAL_TRESHOLD
	return cur_time - first_time < BUSY_FATAL_TRESHOLD;
}

Database_SQLite3::Database_SQLite3(const std::string &savedir, const std::string &name):
	m_name(name),
	m_initialized(false),
	m_savedir(savedir),
	m_database(NULL)
{
	for (u16 i = 0; i < SQLITE3STMT_COUNT; i++) {
		m_stmt[i] = nullptr;
	}
}

void Database_SQLite3::beginSave() {
	verifyDatabase();
	sqlite3_vrfy(stmt_step(SQLITE3STMT_BEGIN), SQLITE_DONE, "Failed to start SQLite3 transaction");
	reset_stmt();
}

void Database_SQLite3::endSave() {
	verifyDatabase();
	sqlite3_vrfy(stmt_step(SQLITE3STMT_END), SQLITE_DONE, "Failed to commit SQLite3 transaction");
	reset_stmt();
}

void Database_SQLite3::openDatabase()
{
	if (m_database) return;

	const std::string dbp = m_savedir + DIR_DELIM + m_name + ".sqlite";

	// Open the database connection

	if (!fs::CreateAllDirs(m_savedir)) {
		logger.info("Database_SQLite3: Failed to create directory '%s'", m_savedir.c_str());
		throw FileNotGoodException("Failed to create database "
				"save directory");
	}

	bool needs_create = !fs::PathExists(dbp);

	sqlite3_vrfy(sqlite3_open_v2(dbp.c_str(), &m_database,
			SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL),
		std::string("Failed to open SQLite3 database file ") + dbp);

	sqlite3_vrfy(sqlite3_busy_handler(m_database, Database_SQLite3::busyHandler,
		m_busy_handler_data), "Failed to set SQLite3 busy handler");

	if (needs_create) {
		createDatabase();
	}

	std::string query_str = std::string("PRAGMA synchronous = ")
			 + itos(g_settings->get(U16SETTING_SQLITE_SYNCHRONOUS));
	sqlite3_vrfy(sqlite3_exec(m_database, query_str.c_str(), NULL, NULL, NULL),
		"Failed to modify sqlite3 synchronous mode");
}

void Database_SQLite3::verifyDatabase()
{
	if (m_initialized) return;

	openDatabase();

	incrementDatabase();

	u16 offset_start = 0, offset_end = 0;
	if (m_name.compare("map") == 0) {
		offset_start = SQLITE3STMT_READ_BLOCK;
		offset_end = SQLITE3STMT_MAP_MAX;
	}
	else if (m_name.compare("game") == 0) {
		offset_start = SQLITE3STMT_WRITE_AREA;
		offset_end = SQLITE3STMT_GAME_MAX;
	}
	else if (m_name.compare("auth") == 0) {
		offset_start = SQLITE3STMT_WRITE_USER;
		offset_end = SQLITE3STMT_AUTH_MAX;
	}

	// DB specific statements
	for (u16 i = offset_start; i < offset_end; i++) {
		logger.info("Loading statement %d %s", i, stmt_list[i]);
		sqlite3_vrfy(sqlite3_prepare_v2(m_database, stmt_list[i], -1, &m_stmt[i], NULL),
				m_name + ": Could not prepare statement " + itos(i));
	}

	// Common statements
	for (u16 i = SQLITE3STMT_BEGIN; i < SQLITE3STMT_COUNT; i++) {
		logger.info("Loading statement %d %s", i, stmt_list[i]);
		sqlite3_vrfy(sqlite3_prepare_v2(m_database, stmt_list[i], -1, &m_stmt[i], NULL),
				m_name + ": Could not prepare statement " + itos(i));
	}

	m_initialized = true;

	logger.debug("Server: SQLite3 database opened: %s.", m_name.c_str());
}

inline void Database_SQLite3::bindPos(sqlite3_stmt *stmt, const v3s16 &pos, int index)
{
	sqlite3_vrfy(sqlite3_bind_int64(stmt, index, getBlockAsInteger(pos)),
		"Internal error: failed to bind query at " __FILE__ ":" TOSTRING(__LINE__));
}

bool Database_SQLite3::saveBlock(const v3s16 &pos, const std::string &data, const epixel::MapBlockDB &blockdb_data)
{
	verifyDatabase();

	bindPos(m_stmt[SQLITE3STMT_WRITE_BLOCK], pos);
	sqlite3_vrfy(sqlite3_bind_blob(m_stmt[SQLITE3STMT_WRITE_BLOCK], 2, data.data(), data.size(), NULL));
	int_to_sqlite(SQLITE3STMT_WRITE_BLOCK, 3, blockdb_data.version);
	int_to_sqlite(SQLITE3STMT_WRITE_BLOCK, 4, blockdb_data.epixel_version);
	int_to_sqlite(SQLITE3STMT_WRITE_BLOCK, 5, blockdb_data.flags);
	int_to_sqlite(SQLITE3STMT_WRITE_BLOCK, 6, blockdb_data.content_width);
	int_to_sqlite(SQLITE3STMT_WRITE_BLOCK, 7, blockdb_data.params_width);
	sqlite3_vrfy(sqlite3_bind_blob(m_stmt[SQLITE3STMT_WRITE_BLOCK], 8, blockdb_data.raw_nodes.data(), blockdb_data.raw_nodes.size(), NULL));
	sqlite3_vrfy(sqlite3_bind_blob(m_stmt[SQLITE3STMT_WRITE_BLOCK], 9, blockdb_data.raw_nodes_metas.data(), blockdb_data.raw_nodes_metas.size(), NULL));
	int_to_sqlite(SQLITE3STMT_WRITE_BLOCK, 10, blockdb_data.timestamp);

	sqlite3_vrfy(stmt_step(SQLITE3STMT_WRITE_BLOCK), SQLITE_DONE);
	reset_stmt();

	return true;
}

void Database_SQLite3::loadBlock(const v3s16 &pos, std::string &data, epixel::MapBlockDB& blockdb_data)
{
	verifyDatabase();

	bindPos(m_stmt[SQLITE3STMT_READ_BLOCK], pos);

	if (stmt_step(SQLITE3STMT_READ_BLOCK) != SQLITE_ROW) {
		reset_stmt();
		return;
	}

	const char *c_data = (const char *) sqlite3_column_blob(m_stmt[SQLITE3STMT_READ_BLOCK], 0);
	size_t len = sqlite3_column_bytes(m_stmt[SQLITE3STMT_READ_BLOCK], 0);
	const char *n_data = (const char *) sqlite3_column_blob(m_stmt[SQLITE3STMT_READ_BLOCK], 6);
	size_t nlen = sqlite3_column_bytes(m_stmt[SQLITE3STMT_READ_BLOCK], 6);
	const char *nm_data = (const char *) sqlite3_column_blob(m_stmt[SQLITE3STMT_READ_BLOCK], 7);
	size_t nmlen = sqlite3_column_bytes(m_stmt[SQLITE3STMT_READ_BLOCK], 7);

	if (c_data)
		data = std::string(c_data, len);

	blockdb_data.version = sqlite_to_int(SQLITE3STMT_READ_BLOCK, 1);
	blockdb_data.epixel_version = sqlite_to_int(SQLITE3STMT_READ_BLOCK, 2);
	blockdb_data.flags = sqlite_to_int(SQLITE3STMT_READ_BLOCK, 3);
	blockdb_data.content_width = sqlite_to_int(SQLITE3STMT_READ_BLOCK, 4);
	blockdb_data.params_width = sqlite_to_int(SQLITE3STMT_READ_BLOCK, 5);
	if (n_data)
		blockdb_data.raw_nodes = std::string(n_data, nlen);
	if (nm_data)
		blockdb_data.raw_nodes_metas = std::string(nm_data, nmlen);
	blockdb_data.timestamp = sqlite_to_int(SQLITE3STMT_READ_BLOCK, 8);

	reset_stmt();
}

bool Database_SQLite3::saveArea(epixel::Area* area)
{
	verifyDatabase();
	beginSave();

	//`id`, `name`, `owner`, `parent_id`, `pos1`, `pos2`, `isopen`
	int_to_sqlite(SQLITE3STMT_WRITE_AREA, 1, area->id);
	str_to_sqlite(SQLITE3STMT_WRITE_AREA, 2, area->name);
	int_to_sqlite(SQLITE3STMT_WRITE_AREA, 3, area->parent_id);

	sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_WRITE_AREA], 4, getBlockAsInteger(area->pos1)));
	sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_WRITE_AREA], 5, getBlockAsInteger(area->pos2)));
	int_to_sqlite(SQLITE3STMT_WRITE_AREA, 6, area->open ? 1 : 0);
	int_to_sqlite(SQLITE3STMT_WRITE_AREA, 7, area->pvpflag);

	sqlite3_vrfy(stmt_step(SQLITE3STMT_WRITE_AREA), SQLITE_DONE);
	reset_stmt(SQLITE3STMT_WRITE_AREA);

	int_to_sqlite(SQLITE3STMT_REMOVE_AREA_OWNERS, 1, area->id);
	sqlite3_vrfy(stmt_step(SQLITE3STMT_REMOVE_AREA_OWNERS), SQLITE_DONE);
	reset_stmt(SQLITE3STMT_REMOVE_AREA_OWNERS);
	for (const auto &pid: area->owners) {
		int_to_sqlite(SQLITE3STMT_WRITE_AREA_OWNER, 1, area->id);
		int_to_sqlite(SQLITE3STMT_WRITE_AREA_OWNER, 2, pid);

		sqlite3_vrfy(stmt_step(SQLITE3STMT_WRITE_AREA_OWNER), SQLITE_DONE);
		reset_stmt(SQLITE3STMT_WRITE_AREA_OWNER);
	}

	endSave();
	return true;
}

bool Database_SQLite3::deleteArea(u32 areaId)
{
	int_to_sqlite(SQLITE3STMT_REMOVE_AREA, 1, areaId);

	sqlite3_vrfy(stmt_step(SQLITE3STMT_REMOVE_AREA), SQLITE_DONE);
	reset_stmt();
	return true;
}

bool Database_SQLite3::loadAreas(std::unordered_map<u32, epixel::Area*> &areas)
{
	verifyDatabase();

	while (stmt_step(SQLITE3STMT_READ_AREAS) == SQLITE_ROW) {
		u32 id = sqlite_to_int(SQLITE3STMT_READ_AREAS, 0);
		std::string name = sqlite_to_string(SQLITE3STMT_READ_AREAS, 1);
		u32 parent_id = sqlite_to_int(SQLITE3STMT_READ_AREAS, 2);
		v3f pos1 = getIntegerAsFloatBlock(sqlite3_column_int64(m_stmt[SQLITE3STMT_READ_AREAS], 3));
		v3f pos2 = getIntegerAsFloatBlock(sqlite3_column_int64(m_stmt[SQLITE3STMT_READ_AREAS], 4));
		bool isopen = sqlite_to_bool(SQLITE3STMT_READ_AREAS, 5);
		epixel::AreaPvpFlag pvpflag = (epixel::AreaPvpFlag) sqlite_to_int(SQLITE3STMT_READ_AREAS, 6);

		std::vector<u32> owners;

		int_to_sqlite(SQLITE3STMT_READ_AREA_OWNERS, 1, id);
		while (stmt_step(SQLITE3STMT_READ_AREA_OWNERS) == SQLITE_ROW) {
			owners.push_back(sqlite_to_int(SQLITE3STMT_READ_AREA_OWNERS, 0));
		}
		reset_stmt(SQLITE3STMT_READ_AREA_OWNERS);

		// Now store the loaded value
		areas[id] = new epixel::Area(owners, name, pos1,
				pos2, parent_id, isopen, pvpflag);
		areas[id]->id = id;
		areas[id]->init();
	}
	reset_stmt(SQLITE3STMT_READ_AREAS);

	return true;
}

bool Database_SQLite3::savePlayerBan(const std::string &playername, epixel::Ban* ban)
{
	verifyDatabase();

	// `playername`, `bantime`, `source`, `reason`
	str_to_sqlite(SQLITE3STMT_WRITE_BAN_PLAYER, 1, playername);
	sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_WRITE_BAN_PLAYER], 2, ban->bantime));
	str_to_sqlite(SQLITE3STMT_WRITE_BAN_PLAYER, 3, ban->source);
	str_to_sqlite(SQLITE3STMT_WRITE_BAN_PLAYER, 4, ban->reason);

	sqlite3_vrfy(stmt_step(SQLITE3STMT_WRITE_BAN_PLAYER), SQLITE_DONE);
	reset_stmt();

	return true;
}

bool Database_SQLite3::saveIPBan(const std::string &ip, epixel::Ban* ban)
{
	verifyDatabase();

	// `playername`, `bantime`, `source`, `reason`
	str_to_sqlite(SQLITE3STMT_WRITE_BAN_IP, 1, ip);
	sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_WRITE_BAN_IP], 2, ban->bantime));
	str_to_sqlite(SQLITE3STMT_WRITE_BAN_IP, 3, ban->source);
	str_to_sqlite(SQLITE3STMT_WRITE_BAN_IP, 4, ban->reason);

	sqlite3_vrfy(stmt_step(SQLITE3STMT_WRITE_BAN_IP), SQLITE_DONE);
	reset_stmt();

	return true;
}

bool Database_SQLite3::deletePlayerBan(const std::string &playername)
{
	str_to_sqlite(SQLITE3STMT_REMOVE_BAN_PLAYER, 1, playername);

	sqlite3_vrfy(stmt_step(SQLITE3STMT_REMOVE_BAN_PLAYER), SQLITE_DONE);
	reset_stmt();
	return true;
}

bool Database_SQLite3::deleteIPBan(const std::string &ip)
{
	str_to_sqlite(SQLITE3STMT_REMOVE_BAN_IP, 1, ip);

	sqlite3_vrfy(stmt_step(SQLITE3STMT_REMOVE_BAN_IP), SQLITE_DONE);
	reset_stmt();
	return true;
}

bool Database_SQLite3::loadPlayerBans(std::unordered_map<std::string, epixel::Ban*> &bans)
{
	verifyDatabase();

	while (stmt_step(SQLITE3STMT_LOAD_BANS_PLAYER) == SQLITE_ROW) {
		std::string playername = sqlite_to_string(SQLITE3STMT_LOAD_BANS_PLAYER, 0);
		u32 ban_time = sqlite3_column_int64(m_stmt[SQLITE3STMT_LOAD_BANS_PLAYER], 1);
		std::string ban_source = sqlite_to_string(SQLITE3STMT_LOAD_BANS_PLAYER, 2);
		std::string ban_reason = sqlite_to_string(SQLITE3STMT_LOAD_BANS_PLAYER, 3);

		// Now store the loaded value
		bans[playername] = new epixel::Ban(ban_time, ban_source, ban_reason);
	}
	reset_stmt();

	return true;
}

bool Database_SQLite3::loadIPBans(std::unordered_map<std::string, epixel::Ban*> &bans)
{
	verifyDatabase();

	while (stmt_step(SQLITE3STMT_LOAD_BANS_IP) == SQLITE_ROW) {
		std::string ip = sqlite_to_string(SQLITE3STMT_LOAD_BANS_IP, 0);
		u32 ban_time = sqlite3_column_int64(m_stmt[SQLITE3STMT_LOAD_BANS_IP], 1);
		std::string ban_source = sqlite_to_string(SQLITE3STMT_LOAD_BANS_IP, 2);
		std::string ban_reason = sqlite_to_string(SQLITE3STMT_LOAD_BANS_IP, 3);

		// Now store the loaded value
		bans[ip] = new epixel::Ban(ban_time, ban_source, ban_reason);
	}
	reset_stmt();

	return true;
}

bool Database_SQLite3::loadCrafts(std::vector<CraftDefinition *> &crafts)
{
	verifyDatabase();

	// Load shaped crafts
	while (stmt_step(SQLITE3STMT_READ_CRAFT_SHAPED) == SQLITE_ROW) {
		CraftReplacements replacements;
		std::vector<std::string> recipe;

		std::string output = sqlite_to_string(SQLITE3STMT_READ_CRAFT_SHAPED, 0);
		u32 width = sqlite3_column_int64(m_stmt[SQLITE3STMT_READ_CRAFT_SHAPED], 1);
		recipe.push_back(sqlite_to_string(SQLITE3STMT_READ_CRAFT_SHAPED, 2));
		recipe.push_back(sqlite_to_string(SQLITE3STMT_READ_CRAFT_SHAPED, 3));
		recipe.push_back(sqlite_to_string(SQLITE3STMT_READ_CRAFT_SHAPED, 4));
		recipe.push_back(sqlite_to_string(SQLITE3STMT_READ_CRAFT_SHAPED, 5));
		recipe.push_back(sqlite_to_string(SQLITE3STMT_READ_CRAFT_SHAPED, 6));
		recipe.push_back(sqlite_to_string(SQLITE3STMT_READ_CRAFT_SHAPED, 7));
		recipe.push_back(sqlite_to_string(SQLITE3STMT_READ_CRAFT_SHAPED, 8));
		recipe.push_back(sqlite_to_string(SQLITE3STMT_READ_CRAFT_SHAPED, 9));
		recipe.push_back(sqlite_to_string(SQLITE3STMT_READ_CRAFT_SHAPED, 10));

		CraftDefinition *def = new CraftDefinitionShaped(
			output, width, recipe, replacements);

		crafts.push_back(def);
	}
	reset_stmt();

	// Load cooking crafts
	while (stmt_step(SQLITE3STMT_READ_CRAFT_COOKING) == SQLITE_ROW) {
		CraftReplacements replacements;

		std::string output = sqlite_to_string(SQLITE3STMT_READ_CRAFT_COOKING, 0);
		std::string recipe = sqlite_to_string(SQLITE3STMT_READ_CRAFT_COOKING, 1);
		float cooktime = sqlite_to_float(SQLITE3STMT_READ_CRAFT_COOKING, 2);

		CraftDefinition *def = new CraftDefinitionCooking(
				output, recipe, cooktime, replacements);

		crafts.push_back(def);
	}
	reset_stmt();

	// Load fuel crafts
	while (stmt_step(SQLITE3STMT_READ_CRAFT_FUEL) == SQLITE_ROW) {
		CraftReplacements replacements;

		std::string recipe = sqlite_to_string(SQLITE3STMT_READ_CRAFT_FUEL, 0);
		float burntime = sqlite_to_float(SQLITE3STMT_READ_CRAFT_FUEL, 1);

		CraftDefinition *def = new CraftDefinitionFuel(
			recipe, burntime, replacements);

		crafts.push_back(def);
	}
	reset_stmt();

	// Load shapeless crafts
	while (stmt_step(SQLITE3STMT_READ_CRAFT_SHAPELESS) == SQLITE_ROW) {
		CraftReplacements replacements;
		std::vector<std::string> recipe;

		std::string output = sqlite_to_string(SQLITE3STMT_READ_CRAFT_SHAPELESS, 0);
		for (u8 i = 1; i < 10; i++) {
			const std::string component = sqlite_to_string(SQLITE3STMT_READ_CRAFT_SHAPELESS, i);
			if (component.length() > 0)
				recipe.push_back(component);
		}

		CraftDefinition *def = new CraftDefinitionShapeless(
			output, recipe, replacements);

		crafts.push_back(def);
	}
	reset_stmt();

	return true;
}

bool Database_SQLite3::loadTips(std::vector<std::wstring> &tips)
{
	verifyDatabase();

	while (stmt_step(SQLITE3STMT_READ_TIPS) == SQLITE_ROW) {
		std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
		std::wstring ws = converter.from_bytes(std::string(reinterpret_cast<const char*>(sqlite3_column_text(m_stmt[SQLITE3STMT_READ_TIPS], 0))));
		tips.push_back(ws);
	}
	reset_stmt();
	return true;
}

bool Database_SQLite3::loadItemDefinitions(IWritableItemDefManager *idef, IWritableNodeDefManager *ndef, GameScripting *script)
{
	verifyDatabase();

	u32 counter = 0;

	while (stmt_step(SQLITE3STMT_LOAD_ITEMDEFS) == SQLITE_ROW) {
		ItemDefinition itemdef;
		itemdef.coreside = true;
		ContentFeatures f;
		itemdef.name = sqlite_to_string(SQLITE3STMT_LOAD_ITEMDEFS, 0);
		itemdef.description = sqlite_to_string(SQLITE3STMT_LOAD_ITEMDEFS, 1);
		itemdef.type = (ItemType)sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS, 2);
		itemdef.inventory_image = sqlite_to_string(SQLITE3STMT_LOAD_ITEMDEFS, 3);
		itemdef.wield_image = sqlite_to_string(SQLITE3STMT_LOAD_ITEMDEFS, 4);
		itemdef.wield_scale = sqlite_to_v3f(SQLITE3STMT_LOAD_ITEMDEFS, 5);
		itemdef.stack_max = sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS, 8);
		itemdef.range = sqlite_to_float(SQLITE3STMT_LOAD_ITEMDEFS, 9);
		itemdef.usable = sqlite_to_bool(SQLITE3STMT_LOAD_ITEMDEFS, 10);
		itemdef.liquids_pointable = sqlite_to_bool(SQLITE3STMT_LOAD_ITEMDEFS, 11);
		itemdef.node_placement_prediction = sqlite_to_string(SQLITE3STMT_LOAD_ITEMDEFS, 12);
		if (itemdef.node_placement_prediction == "" && itemdef.type == ITEM_NODE) {
			itemdef.node_placement_prediction = itemdef.name;
		}
		itemdef.eat_amount = sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS, 13);
		itemdef.on_use_replace_with = sqlite_to_string(SQLITE3STMT_LOAD_ITEMDEFS, 14);
		itemdef.extended_type = (epixel::ItemExtendedType)sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS, 15);

		// Load item groups
		str_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_GROUPS, 1, itemdef.name);
		// Clear groups from default values
		itemdef.groups.clear();
		f.groups.clear();
		while (stmt_step(SQLITE3STMT_LOAD_ITEMDEFS_GROUPS) == SQLITE_ROW) {
			std::string groupname = sqlite_to_string(SQLITE3STMT_LOAD_ITEMDEFS_GROUPS, 0);
			itemdef.groups[groupname] =
					sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_GROUPS, 1);
			if (itemdef.type == ITEM_NODE) {
				f.groups[groupname] =
						sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_GROUPS, 1);

				if (groupname.compare("unbreakable") == 0 && f.groups[groupname] > 0) {
					f.unbreakable = true;
				}
				else if (groupname.compare("explode_in_chain") == 0 && f.groups[groupname] > 0) {
					f.explodeInChain = true;
				}
				else if (groupname.compare("explosion_radius") == 0 && f.groups[groupname] > 0) {
					f.explosionRadius = f.groups[groupname];
				}
			}
		}
		reset_stmt();

		str_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_SOUNDS, 1, itemdef.name);
		while (stmt_step(SQLITE3STMT_LOAD_ITEMDEFS_SOUNDS) == SQLITE_ROW) {
			std::string sound_name = sqlite_to_string(SQLITE3STMT_LOAD_ITEMDEFS_SOUNDS, 0);
			if (sound_name.compare("place") == 0) {
				itemdef.sound_place.name = sqlite_to_string(SQLITE3STMT_LOAD_ITEMDEFS_SOUNDS, 1);
				itemdef.sound_place.gain = sqlite_to_float(SQLITE3STMT_LOAD_ITEMDEFS_SOUNDS, 2);
			}
			else if (itemdef.type == ITEM_NODE) {
				if (sound_name.compare("dig") == 0) {
					f.sound_dig = SimpleSoundSpec(
						sqlite_to_string(SQLITE3STMT_LOAD_ITEMDEFS_SOUNDS, 1),
						sqlite_to_float(SQLITE3STMT_LOAD_ITEMDEFS_SOUNDS, 2)
					);
				}
				else if (sound_name.compare("dug") == 0) {
					f.sound_dug = SimpleSoundSpec(
						sqlite_to_string(SQLITE3STMT_LOAD_ITEMDEFS_SOUNDS, 1),
						sqlite_to_float(SQLITE3STMT_LOAD_ITEMDEFS_SOUNDS, 2)
					);
				}
				else if (sound_name.compare("footstep") == 0) {
					f.sound_footstep = SimpleSoundSpec(
						sqlite_to_string(SQLITE3STMT_LOAD_ITEMDEFS_SOUNDS, 1),
						sqlite_to_float(SQLITE3STMT_LOAD_ITEMDEFS_SOUNDS, 2)
					);
				}
			}
		}
		reset_stmt();

		// Special handling for tools
		if (itemdef.type == ITEM_TOOL) {
			// Read tool capabilities
			float full_punch_interval = 1.4f;
			int max_drop_level = 1;

			str_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_TOOL_CAPABILITIES, 1, itemdef.name);
			if (stmt_step(SQLITE3STMT_LOAD_ITEMDEFS_TOOL_CAPABILITIES) == SQLITE_ROW) {
				full_punch_interval = sqlite_to_float(SQLITE3STMT_LOAD_ITEMDEFS_TOOL_CAPABILITIES, 0);
				max_drop_level = sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_TOOL_CAPABILITIES, 1);
			}
			reset_stmt();
			itemdef.tool_capabilities = new ToolCapabilities(full_punch_interval, max_drop_level);

			// Tool capabilities damage groups
			str_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_TOOL_CAPABILITIES_DAMAGE_GROUPS, 1, itemdef.name);
			while (stmt_step(SQLITE3STMT_LOAD_ITEMDEFS_TOOL_CAPABILITIES_DAMAGE_GROUPS) == SQLITE_ROW) {
				itemdef.tool_capabilities->damageGroups[sqlite_to_string(SQLITE3STMT_LOAD_ITEMDEFS_TOOL_CAPABILITIES_DAMAGE_GROUPS, 0)] =
						sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_TOOL_CAPABILITIES_DAMAGE_GROUPS, 1);
			}
			reset_stmt();

			// Tool capabilities groupcap
			str_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_TOOL_CAPABILITIES_GROUPCAP, 1, itemdef.name);
			while (stmt_step(SQLITE3STMT_LOAD_ITEMDEFS_TOOL_CAPABILITIES_GROUPCAP) == SQLITE_ROW) {
				// item_name, groupcap_name
				const std::string groupcap_name = sqlite_to_string(SQLITE3STMT_LOAD_ITEMDEFS_TOOL_CAPABILITIES_GROUPCAP, 0);
				ToolGroupCap tgc;
				tgc.maxlevel = sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_TOOL_CAPABILITIES_GROUPCAP, 1);
				tgc.uses = sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_TOOL_CAPABILITIES_GROUPCAP, 2);

				// ToolGroupCap times
				str_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_TOOL_CAPABILITIES_GROUPCAP_TIMES, 1, itemdef.name);
				str_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_TOOL_CAPABILITIES_GROUPCAP_TIMES, 2, groupcap_name);
				while (stmt_step(SQLITE3STMT_LOAD_ITEMDEFS_TOOL_CAPABILITIES_GROUPCAP_TIMES) == SQLITE_ROW) {
					tgc.times[sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_TOOL_CAPABILITIES_GROUPCAP_TIMES, 0)] =
							sqlite_to_float(SQLITE3STMT_LOAD_ITEMDEFS_TOOL_CAPABILITIES_GROUPCAP_TIMES, 1);
				}
				reset_stmt();

				itemdef.tool_capabilities->groupcaps[groupcap_name] = tgc;
			}
			reset_stmt(SQLITE3STMT_LOAD_ITEMDEFS_TOOL_CAPABILITIES_GROUPCAP);
		}
		else if (itemdef.type == ITEM_NODE) {
			// Nodes
			str_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 1, itemdef.name);
			if (stmt_step(SQLITE3STMT_LOAD_ITEMDEFS_NODES) == SQLITE_ROW) {
				f.name = itemdef.name;
				f.drawtype = (NodeDrawType)sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 0);
				f.visual_scale = sqlite_to_float(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 1);
				f.mesh = sqlite_to_string(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 2);
				f.alpha = sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 3);
				f.param_type = (ContentParamType)sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 4);
				f.param_type_2 = (ContentParamType2)sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 5);
				f.is_ground_content = sqlite_to_bool(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 6);
				f.sunlight_propagates = sqlite_to_bool(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 7);
				f.walkable = sqlite_to_bool(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 8);
				f.pointable = sqlite_to_bool(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 9);
				f.diggable = sqlite_to_bool(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 10);
				f.climbable = sqlite_to_bool(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 11);
				f.buildable_to = sqlite_to_bool(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 12);
				f.rightclickable = sqlite_to_bool(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 13);
				f.liquid_type = (LiquidType)sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 14);
				f.liquid_alternative_flowing = sqlite_to_string(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 15);
				f.liquid_alternative_source = sqlite_to_string(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 16);
				f.liquid_viscosity = sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 17);
				f.liquid_range = sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 18);
				f.leveled = sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 19);
				f.drowning = sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 20);
				f.light_source = sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 21);
				f.damage_per_second = sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 22);
				f.waving = sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 23);
				f.legacy_facedir_simple = sqlite_to_bool(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 24);
				f.legacy_wallmounted = sqlite_to_bool(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 25);

				f.light_propagates = (f.param_type == CPT_LIGHT);

				// Read tiles
				bool default_tiling = !(f.drawtype == NDT_PLANTLIKE || f.drawtype == NDT_FIRELIKE);
				f.tiledef[0].name = sqlite_to_string(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 26);
				f.tiledef[0].tileable_horizontal = default_tiling;
				f.tiledef[0].tileable_vertical = default_tiling;

				TileDef lasttile = f.tiledef[0];

				for (u8 i = 1; i < 6; i++) {
					std::string tilename = sqlite_to_string(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 26 + i);
					if (tilename == "") {
						f.tiledef[i] = lasttile;
					}
					else {
						f.tiledef[i].name = tilename;
						f.tiledef[i].tileable_horizontal = default_tiling;
						f.tiledef[i].tileable_vertical = default_tiling;
					}
				}

				// farming
				f.soil.base = sqlite_to_string(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 32);
				f.soil.dry = sqlite_to_string(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 33);
				f.soil.wet = sqlite_to_string(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 34);
				f.drops.max_items = sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 35);
				f.droppable = sqlite_to_bool(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 36);
				f.node_box.type = (NodeBoxType)sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 37);
				f.collision_box.type = (NodeBoxType)sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 38);
				f.selection_box.type = (NodeBoxType)sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 39);
				if (sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 40) < epixel::CFRC_MAX) {
					f.rc_formspec_type = (epixel::ContentFeaturesRightClickFormspecType)sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_NODES, 40);
				}
				else {
					logger.error("Wrong rightclick_formspec for item %s, defaulting to NONE", f.name.c_str());
				}

				// Load nodebox
				if (f.node_box.type == NODEBOX_FIXED) {
					str_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES, 1, itemdef.name);
					int_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES, 2, 1);
					while (stmt_step(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES) == SQLITE_ROW) {
						f.node_box.fixed.push_back(sqlite_to_aabb3f(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES, 1));
					}

					reset_stmt();
				}
				else if (f.node_box.type == NODEBOX_WALLMOUNTED) {
					str_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI, 1, itemdef.name);
					int_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI, 2, 11);
					int_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI, 3, 12);
					int_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI, 4, 13);
					while (stmt_step(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI) == SQLITE_ROW) {
						switch (sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI, 0)) {
							case 11: f.node_box.wall_bottom =
									sqlite_to_aabb3f(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI, 1); break;
							case 12: f.node_box.wall_side =
									sqlite_to_aabb3f(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI, 1); break;
							case 13: f.node_box.wall_top =
									sqlite_to_aabb3f(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI, 1); break;
						}
					}

					reset_stmt();
				}

				if (f.collision_box.type == NODEBOX_FIXED) {
					str_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES, 1, itemdef.name);
					int_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES, 2, 2);
					while (stmt_step(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES) == SQLITE_ROW) {
						f.node_box.fixed.push_back(sqlite_to_aabb3f(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES, 1));
					}

					reset_stmt();
				}
				else if (f.collision_box.type == NODEBOX_WALLMOUNTED) {
					str_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI, 1, itemdef.name);
					int_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI, 2, 21);
					int_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI, 3, 22);
					int_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI, 4, 23);
					while (stmt_step(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI) == SQLITE_ROW) {
						switch (sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI, 0)) {
							case 21: f.collision_box.wall_bottom =
									sqlite_to_aabb3f(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI, 1); break;
							case 22: f.collision_box.wall_side =
									sqlite_to_aabb3f(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI, 1); break;
							case 23: f.collision_box.wall_top =
									sqlite_to_aabb3f(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI, 1); break;
						}
					}

					reset_stmt();
				}

				if (f.selection_box.type == NODEBOX_FIXED) {
					str_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES, 1, itemdef.name);
					int_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES, 2, 3);
					while (stmt_step(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES) == SQLITE_ROW) {
						f.node_box.fixed.push_back(sqlite_to_aabb3f(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES, 1));
					}

					reset_stmt();
				}
				else if (f.selection_box.type == NODEBOX_WALLMOUNTED) {
					str_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI, 1, itemdef.name);
					int_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI, 2, 31);
					int_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI, 3, 32);
					int_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI, 4, 33);
					while (stmt_step(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI) == SQLITE_ROW) {
						switch (sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI, 0)) {
							case 31: f.collision_box.wall_bottom =
									sqlite_to_aabb3f(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI, 1); break;
							case 32: f.collision_box.wall_side =
									sqlite_to_aabb3f(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI, 1); break;
							case 33: f.collision_box.wall_top =
									sqlite_to_aabb3f(SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI, 1); break;
						}
					}

					reset_stmt();
				}

				if (f.drawtype == NDT_NODEBOX) {
					f.selection_box = f.node_box;
				}
				else if (f.drawtype == NDT_FENCELIKE) {
					f.selection_box.type = NODEBOX_FIXED;
					f.selection_box.fixed = {aabb3f(10/8,10/2,10/8, 10/8,10/2,10/8)};
				}

				str_to_sqlite(SQLITE3STMT_LOAD_ITEMDEFS_NODES_DROPS, 1, itemdef.name);
				while (stmt_step(SQLITE3STMT_LOAD_ITEMDEFS_NODES_DROPS) == SQLITE_ROW) {
					std::string drop_name = sqlite_to_string(SQLITE3STMT_LOAD_ITEMDEFS_NODES_DROPS, 0);
					u16 drop_rarity = sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_NODES_DROPS, 1);
					u16 min_items = sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_NODES_DROPS, 2);
					u16 max_items = sqlite_to_int(SQLITE3STMT_LOAD_ITEMDEFS_NODES_DROPS, 3);

					// Fix wrong min_items
					if (min_items < 1) {
						min_items = 1;
						logger.warn("min_items for drop '%s' lower than 1, fixing to 1. Please fix your database",
								drop_name.c_str());
					}

					// Fix wrong max_items
					if (max_items < min_items) {
						max_items = min_items;
						logger.warn("max_items < min_items for drop '%s', fixing to min_items. Please fix your database",
								drop_name.c_str());
					}

					f.drops.items.push_back({drop_name, drop_rarity, min_items, max_items});
				}

				reset_stmt();

				content_t id = ndef->set(f.name, f);
				if (id > MAX_REGISTERED_CONTENT) {
					throw DatabaseException("Number of registerable nodes ("
							+ itos(MAX_REGISTERED_CONTENT + 1)
							+ ") exceeded (" + f.name + ")");
				}
			}

			reset_stmt(SQLITE3STMT_LOAD_ITEMDEFS_NODES);
		}

		counter++;
		idef->registerItem(itemdef);
		script->addItemDefinition(&itemdef, f);
	}

	logger.notice("%d items loaded.", counter);
	reset_stmt(SQLITE3STMT_LOAD_ITEMDEFS);

	return true;
}

bool Database_SQLite3::loadTreeABM(INodeDefManager* ndef, std::vector<epixel::abm::DBTreeSaplingABM*> &abms)
{
	verifyDatabase();

	while (stmt_step(SQLITE3STMT_LOAD_TREE_ABMS_SCHEMES) == SQLITE_ROW) {
		u32 id = sqlite_to_int(SQLITE3STMT_LOAD_TREE_ABMS_SCHEMES, 0),
			angle = sqlite_to_int(SQLITE3STMT_LOAD_TREE_ABMS_SCHEMES, 8),
			iterations = sqlite_to_int(SQLITE3STMT_LOAD_TREE_ABMS_SCHEMES, 9),
			random_level = sqlite_to_int(SQLITE3STMT_LOAD_TREE_ABMS_SCHEMES, 10),
			fruit_chance = sqlite_to_int(SQLITE3STMT_LOAD_TREE_ABMS_SCHEMES, 14),
			abm_chance = sqlite_to_int(SQLITE3STMT_LOAD_TREE_ABMS_SCHEMES, 16);
		std::string axiom = sqlite_to_string(SQLITE3STMT_LOAD_TREE_ABMS_SCHEMES, 1),
			rules_a = sqlite_to_string(SQLITE3STMT_LOAD_TREE_ABMS_SCHEMES, 2),
			rules_b = sqlite_to_string(SQLITE3STMT_LOAD_TREE_ABMS_SCHEMES, 3),
			rules_c = sqlite_to_string(SQLITE3STMT_LOAD_TREE_ABMS_SCHEMES, 4),
			rules_d = sqlite_to_string(SQLITE3STMT_LOAD_TREE_ABMS_SCHEMES, 5),
			trunk = sqlite_to_string(SQLITE3STMT_LOAD_TREE_ABMS_SCHEMES, 6),
			leaves = sqlite_to_string(SQLITE3STMT_LOAD_TREE_ABMS_SCHEMES, 7),
			trunk_type = sqlite_to_string(SQLITE3STMT_LOAD_TREE_ABMS_SCHEMES, 11),
			fruit = sqlite_to_string(SQLITE3STMT_LOAD_TREE_ABMS_SCHEMES, 13);
		float abm_interval = sqlite_to_float(SQLITE3STMT_LOAD_TREE_ABMS_SCHEMES, 15);
		bool thin_branches = sqlite_to_bool(SQLITE3STMT_LOAD_TREE_ABMS_SCHEMES, 12);

		int_to_sqlite(SQLITE3STMT_LOAD_TREE_ABMS_SAPLINGS, 1, id);

		std::vector<std::string> saplings;
		while (stmt_step(SQLITE3STMT_LOAD_TREE_ABMS_SAPLINGS) == SQLITE_ROW) {
			saplings.push_back(sqlite_to_string(SQLITE3STMT_LOAD_TREE_ABMS_SAPLINGS, 0));
		}
		reset_stmt();

		epixel::abm::DBTreeSaplingABM* abm = new epixel::abm::DBTreeSaplingABM(ndef, saplings, abm_interval, abm_chance);
		treegen::TreeDef* def = abm->def();
		def->angle = angle;
		def->iterations = iterations;
		def->iterations_random_level = random_level;
		if (fruit.length() > 0)
			def->fruit_chance = fruit_chance;
		def->initial_axiom = axiom;
		def->rules_a = rules_a;
		def->rules_b = rules_b;
		def->rules_c = rules_c;
		def->rules_d = rules_d;
		def->trunk_type = trunk_type;
		def->thin_branches = thin_branches;
		abm->setTrunkLeavesFruit(trunk, leaves, fruit);
		abms.push_back(abm);
	}
	reset_stmt(SQLITE3STMT_LOAD_TREE_ABMS_SCHEMES);
	return true;
}

bool Database_SQLite3::loadNodeReplaceABM(INodeDefManager* ndef, std::vector<epixel::abm::NodeReplaceABM*> &abms)
{
	verifyDatabase();

	while (stmt_step(SQLITE3STMT_LOAD_NODEREPLACE_ABMS) == SQLITE_ROW) {
		const std::string node_name = sqlite_to_string(SQLITE3STMT_LOAD_NODEREPLACE_ABMS, 0),
				node_replace_name = sqlite_to_string(SQLITE3STMT_LOAD_NODEREPLACE_ABMS, 1);
		const float interval = sqlite_to_float(SQLITE3STMT_LOAD_NODEREPLACE_ABMS, 2);
		const s32 chance = sqlite_to_int(SQLITE3STMT_LOAD_NODEREPLACE_ABMS, 3);

		epixel::abm::NodeReplaceABM* abm = new epixel::abm::NodeReplaceABM(ndef, interval, chance, node_name, node_replace_name);
		abms.push_back(abm);
	}

	reset_stmt(SQLITE3STMT_LOAD_NODEREPLACE_ABMS);
	return true;
}

bool Database_SQLite3::loadOres(std::vector<Ore*> &ores, OreManager* oremgr)
{
	verifyDatabase();

	while (stmt_step(SQLITE3STMT_LOAD_ORES) == SQLITE_ROW) {
		u32 id = sqlite_to_int(SQLITE3STMT_LOAD_ORES, 0);
		std::string name = sqlite_to_string(SQLITE3STMT_LOAD_ORES, 1);
		u16 oretype = sqlite_to_int(SQLITE3STMT_LOAD_ORES, 2);
		if (oretype > ORE_VEIN) {
			logger.warn("Invalid ore %s with ID %d with type %d", name.c_str(), id, oretype);
			continue;
		}

		Ore *ore = oremgr->create((OreType)oretype);
		if (!ore) {
			logger.warn("Ore creation failed, unknown type ?");
			continue;
		}

		ore->name = "";
		ore->flags = sqlite_to_int(SQLITE3STMT_LOAD_ORES, 3);
		ore->clust_scarcity = sqlite_to_int(SQLITE3STMT_LOAD_ORES, 4);
		ore->clust_num_ores = sqlite_to_int(SQLITE3STMT_LOAD_ORES, 5);
		ore->clust_size = sqlite_to_int(SQLITE3STMT_LOAD_ORES, 6);
		ore->nthresh = sqlite_to_float(SQLITE3STMT_LOAD_ORES, 7);
		ore->y_min = sqlite_to_int(SQLITE3STMT_LOAD_ORES, 9);
		ore->y_max = sqlite_to_int(SQLITE3STMT_LOAD_ORES, 10);
		ore->noise = NULL;

		// If it's noisy, load
		if (sqlite_to_bool(SQLITE3STMT_LOAD_ORES, 8)) {
			ore->np.offset = sqlite_to_float(SQLITE3STMT_LOAD_ORES, 11);
			ore->np.scale = sqlite_to_float(SQLITE3STMT_LOAD_ORES, 12);
			ore->np.spread = sqlite_to_v3f(SQLITE3STMT_LOAD_ORES, 13);
			ore->np.persist = sqlite_to_float(SQLITE3STMT_LOAD_ORES, 16);
			ore->np.lacunarity = sqlite_to_float(SQLITE3STMT_LOAD_ORES, 17);
			ore->np.seed = sqlite_to_int(SQLITE3STMT_LOAD_ORES, 18);
			ore->np.octaves = sqlite_to_int(SQLITE3STMT_LOAD_ORES, 19);
			ore->np.flags = sqlite_to_int(SQLITE3STMT_LOAD_ORES, 20);
		}

		if (oretype == ORE_VEIN) {
			OreVein *orevein = (OreVein *)ore;
			orevein->random_factor = 1.0f;
		}

		ore->m_nodenames.push_back(name);

		int_to_sqlite(SQLITE3STMT_LOAD_ORE_INTO, 1, id);
		while (stmt_step(SQLITE3STMT_LOAD_ORE_INTO) == SQLITE_ROW) {
			ore->m_nodenames.push_back(sqlite_to_string(SQLITE3STMT_LOAD_ORE_INTO, 0));
		}
		ore->m_nnlistsizes.push_back(ore->m_nodenames.size() - 1);

		reset_stmt(SQLITE3STMT_LOAD_ORE_INTO);
		ores.push_back(ore);
	}

	reset_stmt(SQLITE3STMT_LOAD_ORES);
	return true;
}

bool Database_SQLite3::loadLoots(epixel::CreatureDropMap &creaturedrops, u32 &loot_nb)
{
	verifyDatabase();

	// Load creature loots
	while (stmt_step(SQLITE3STMT_READ_CREATUREDEFS_LOOT) == SQLITE_ROW) {
		u32 id = sqlite3_column_int64(m_stmt[SQLITE3STMT_READ_CREATUREDEFS_LOOT], 0);
		if (creaturedrops.find(id) == creaturedrops.end()) {
			creaturedrops[id] = new epixel::CreatureDropList();
		}

		std::string item = sqlite_to_string(SQLITE3STMT_READ_CREATUREDEFS_LOOT, 1);
		u8 chance = sqlite3_column_int64(m_stmt[SQLITE3STMT_READ_CREATUREDEFS_LOOT], 2);
		u8 min = sqlite3_column_int64(m_stmt[SQLITE3STMT_READ_CREATUREDEFS_LOOT], 3);
		u8 max = sqlite3_column_int64(m_stmt[SQLITE3STMT_READ_CREATUREDEFS_LOOT], 4);
		epixel::CreatureDrop drop(item, chance, min, max);
		creaturedrops[id]->push_back(drop);
		loot_nb++;
	}

	reset_stmt();
	return true;
}

bool Database_SQLite3::loadCreatureNodeReplace(epixel::CreatureNodeReplaceMap &nodereplace, u32 &replace_nb)
{
	verifyDatabase();

	// Load creature nodes to replace
	while (stmt_step(SQLITE3STMT_READ_CREATUREDEFS_NODE_REPLACE) == SQLITE_ROW) {
		u32 id = sqlite3_column_int64(m_stmt[SQLITE3STMT_READ_CREATUREDEFS_NODE_REPLACE], 0);
		if (nodereplace.find(id) == nodereplace.end()) {
			nodereplace[id] = new epixel::CreatureNodeReplace();
		}

		std::string node_src = sqlite_to_string(SQLITE3STMT_READ_CREATUREDEFS_NODE_REPLACE, 1);
		std::string node_dst = sqlite_to_string(SQLITE3STMT_READ_CREATUREDEFS_NODE_REPLACE, 2);

		if (node_src.compare("") != 0 && node_dst.compare("") != 0) {
			(*nodereplace[id])[node_src] = node_dst;
			replace_nb++;
		}
	}

	reset_stmt();
	return true;
}

bool Database_SQLite3::loadCreatureDefinitions(epixel::CreatureDefMap &creaturedefs,
		epixel::CreatureStore *cstore,
		u32 &meshes_nb, u32 &texture_nb, const epixel::SpellMap &spells)
{
	verifyDatabase();

	// Load creature definitions
	while (stmt_step(SQLITE3STMT_READ_CREATUREDEFS) == SQLITE_ROW) {
		std::string name = sqlite_to_string(SQLITE3STMT_READ_CREATUREDEFS, 0);
		if (creaturedefs.find(name) != creaturedefs.end()) {
			continue;
		}
		epixel::CreatureDef* def = new epixel::CreatureDef();

		def->type = (epixel::CreatureType)sqlite3_column_int64(m_stmt[SQLITE3STMT_READ_CREATUREDEFS], 1);
		def->attacktype = (epixel::CreatureAttackType)sqlite3_column_int64(m_stmt[SQLITE3STMT_READ_CREATUREDEFS], 2);
		def->can_float = sqlite_to_bool(SQLITE3STMT_READ_CREATUREDEFS, 3);
		def->can_jump = sqlite_to_bool(SQLITE3STMT_READ_CREATUREDEFS, 4);
		def->viewrange = sqlite_to_float(SQLITE3STMT_READ_CREATUREDEFS, 5);
		def->replacerate = sqlite3_column_int64(m_stmt[SQLITE3STMT_READ_CREATUREDEFS], 6);
		def->replaceoffset = sqlite_to_float(SQLITE3STMT_READ_CREATUREDEFS, 7);
		def->collisionbox.push_back(sqlite_to_float(SQLITE3STMT_READ_CREATUREDEFS, 8));
		def->collisionbox.push_back(sqlite_to_float(SQLITE3STMT_READ_CREATUREDEFS, 9));
		def->collisionbox.push_back(sqlite_to_float(SQLITE3STMT_READ_CREATUREDEFS, 10));
		def->collisionbox.push_back(sqlite_to_float(SQLITE3STMT_READ_CREATUREDEFS, 11));
		def->collisionbox.push_back(sqlite_to_float(SQLITE3STMT_READ_CREATUREDEFS, 12));
		def->collisionbox.push_back(sqlite_to_float(SQLITE3STMT_READ_CREATUREDEFS, 13));

		def->can_take_fall_damage = sqlite_to_bool(SQLITE3STMT_READ_CREATUREDEFS, 14);
		def->fall_speed = sqlite_to_float(SQLITE3STMT_READ_CREATUREDEFS, 15);
		def->jump_height = sqlite_to_float(SQLITE3STMT_READ_CREATUREDEFS, 16);
		def->walk_chance = sqlite_to_float(SQLITE3STMT_READ_CREATUREDEFS, 17);
		def->walk_velocity = sqlite_to_float(SQLITE3STMT_READ_CREATUREDEFS, 18);
		def->damage_min = sqlite3_column_int64(m_stmt[SQLITE3STMT_READ_CREATUREDEFS], 19);
		u32 loot_table = sqlite3_column_int64(m_stmt[SQLITE3STMT_READ_CREATUREDEFS], 20);
		u32 nr_table = sqlite3_column_int64(m_stmt[SQLITE3STMT_READ_CREATUREDEFS], 21);
		def->hp_min = sqlite3_column_int64(m_stmt[SQLITE3STMT_READ_CREATUREDEFS], 22);
		def->hp_max = sqlite3_column_int64(m_stmt[SQLITE3STMT_READ_CREATUREDEFS], 23);
		def->armor = sqlite3_column_int64(m_stmt[SQLITE3STMT_READ_CREATUREDEFS], 24);
		def->visualsize = sqlite_to_v2f(SQLITE3STMT_READ_CREATUREDEFS, 25);
		def->xp_min = sqlite_to_int(SQLITE3STMT_READ_CREATUREDEFS, 27);
		def->xp_max = sqlite_to_int(SQLITE3STMT_READ_CREATUREDEFS, 28);

		def->drops = cstore->getDropList(loot_table);
		def->replacelist = cstore->getNodeReplaceList(nr_table);

		for (u8 i = 0; i < epixel::CREATUREANIMATIONINFOS_MAX; i++) {
			def->animation_infos[i] = sqlite3_column_int64(m_stmt[SQLITE3STMT_READ_CREATUREDEFS], 29 + i);
		}

		for (u8 i = 0; i < epixel::CREATURESOUND_MAX; i++) {
			std::string sound = sqlite_to_string(SQLITE3STMT_READ_CREATUREDEFS,
					29 + epixel::CREATUREANIMATIONINFOS_MAX + i);
			if (sound.compare("") != 0) {
				def->sounds[(epixel::CreatureSound)i] = sound;
			}
		}

		for (u8 i = 0; i < epixel::CREATUREDAMAGE_MAX; i++) {
			u16 dmg = sqlite3_column_int64(m_stmt[SQLITE3STMT_READ_CREATUREDEFS], 29 +
					epixel::CREATUREANIMATIONINFOS_MAX + epixel::CREATURESOUND_MAX + i);

			if (dmg != 0) {
				def->damages[(epixel::CreatureDamageType)i] = dmg;
			}
		}

		// Load creature meshes
		str_to_sqlite(SQLITE3STMT_READ_CREATUREDEFS_MESHES, 1, name);
		while (stmt_step(SQLITE3STMT_READ_CREATUREDEFS_MESHES) == SQLITE_ROW) {
			def->meshlist.push_back(sqlite_to_string(SQLITE3STMT_READ_CREATUREDEFS_MESHES, 0));
			meshes_nb++;
		}
		reset_stmt();

		// Load creature textures
		str_to_sqlite(SQLITE3STMT_READ_CREATUREDEFS_TEXTURES, 1, name);
		while (stmt_step(SQLITE3STMT_READ_CREATUREDEFS_TEXTURES) == SQLITE_ROW) {
			def->texturelist.push_back(sqlite_to_string(SQLITE3STMT_READ_CREATUREDEFS_TEXTURES, 0));
			texture_nb++;
		}
		reset_stmt();

		str_to_sqlite(SQLITE3STMT_READ_CREATUREDEFS_SPELLS, 1, name);
		def->spells.clear();
		while (stmt_step(SQLITE3STMT_READ_CREATUREDEFS_SPELLS) == SQLITE_ROW) {
			std::string spellname = sqlite_to_string(SQLITE3STMT_READ_CREATUREDEFS_SPELLS, 0);
			for (const auto &spell: spells) {
				if (spellname.compare(spell.second->name) == 0) {
					def->spells[spell.second->id] = spell.second;
					break;
				}
			}
		}
		reset_stmt();

		creaturedefs[name] = def;
	}

	reset_stmt(SQLITE3STMT_READ_CREATUREDEFS);
	return true;
}

bool Database_SQLite3::loadCreatureSpawningABM(INodeDefManager* ndef, std::vector<epixel::abm::CreatureSpawningABM*> &abms)
{
	verifyDatabase();
	while (stmt_step(SQLITE3STMT_READ_CREATUREDEFS_ABMS) == SQLITE_ROW) {
		std::string name = sqlite_to_string(SQLITE3STMT_READ_CREATUREDEFS_ABMS, 0);

		std::vector<std::string> nodes;
		str_to_sqlite(SQLITE3STMT_READ_CREATUREDEFS_ABMS_NODES, 1, name);
		while (stmt_step(SQLITE3STMT_READ_CREATUREDEFS_ABMS_NODES) == SQLITE_ROW) {
			nodes.push_back(sqlite_to_string(SQLITE3STMT_READ_CREATUREDEFS_ABMS_NODES, 0));
		}
		reset_stmt();

		if (nodes.size() == 0) {
			logger.error("Warning: Creature spawning ABM for %s doesn't have linked nodes. Ignoring.", name.c_str());
			continue;
		}

		float interval = sqlite_to_float(SQLITE3STMT_READ_CREATUREDEFS_ABMS, 1);
		u32 chance = sqlite_to_int(SQLITE3STMT_READ_CREATUREDEFS_ABMS, 2);
		u32 min_light = sqlite_to_int(SQLITE3STMT_READ_CREATUREDEFS_ABMS, 3);
		u32 max_light = sqlite_to_int(SQLITE3STMT_READ_CREATUREDEFS_ABMS, 4);
		u32 max_count_per_area = sqlite_to_int(SQLITE3STMT_READ_CREATUREDEFS_ABMS, 5);
		u32 min_height = sqlite_to_int(SQLITE3STMT_READ_CREATUREDEFS_ABMS, 6);
		u32 max_height = sqlite_to_int(SQLITE3STMT_READ_CREATUREDEFS_ABMS, 7);
		abms.push_back(new epixel::abm::CreatureSpawningABM(ndef, nodes, name, interval,
				chance, min_light, max_light, max_count_per_area, min_height, max_height));
	}

	reset_stmt(SQLITE3STMT_READ_CREATUREDEFS_ABMS);
	return true;
}

bool Database_SQLite3::loadSpells(epixel::SpellMap &spells)
{
	verifyDatabase();

	u32 spellId = 0;

	while (stmt_step(SQLITE3STMT_LOAD_SPELLS) == SQLITE_ROW) {
		epixel::Spell* spell = new epixel::Spell();
		spell->name = sqlite_to_string(SQLITE3STMT_LOAD_SPELLS, 0);
		spell->visual = sqlite_to_string(SQLITE3STMT_LOAD_SPELLS, 1);
		spell->visual_size = sqlite_to_v2f(SQLITE3STMT_LOAD_SPELLS, 2);
		spell->texture = sqlite_to_string(SQLITE3STMT_LOAD_SPELLS, 4);
		spell->velocity = sqlite_to_float(SQLITE3STMT_LOAD_SPELLS, 5);
		spell->damages_player = sqlite_to_int(SQLITE3STMT_LOAD_SPELLS, 6);
		spell->damages_npc = sqlite_to_int(SQLITE3STMT_LOAD_SPELLS, 7);

		if (sqlite_to_int(SQLITE3STMT_LOAD_SPELLS, 8) >= epixel::SPELLDAMAGEGROUP_MAX) {
			std::stringstream ss;
			ss << "Invalid spell damagegroup ID " << sqlite_to_int(SQLITE3STMT_LOAD_SPELLS, 8)
					<< "for spell " << spell->name << ", aborting." << std::endl;
			delete spell;
			throw DatabaseException(ss.str());
		}
		spell->damagegroup = (epixel::SpellDamageGroup)sqlite_to_int(SQLITE3STMT_LOAD_SPELLS, 8);
		if (spell->damagegroup >= epixel::SPELLDAMAGEGROUP_MAX) {
			logger.crit("Invalid spell damagegroup for spell %s: %d >= SPELLDAMAGEGROUP_MAX", spell->name.c_str(), spell->target_type);
			delete spell;
			continue;
		}
		const std::string dmgGroup = epixel::Spell::DamageGroupMapper[spell->damagegroup];
		spell->toolcap_npc.damageGroups[dmgGroup] = spell->damages_npc;
		spell->toolcap_player.damageGroups[dmgGroup] = spell->damages_player;

		spell->damages_nodes_radius = sqlite_to_int(SQLITE3STMT_LOAD_SPELLS, 9);
		spell->max_range = sqlite_to_float(SQLITE3STMT_LOAD_SPELLS, 10);
		spell->timer = sqlite_to_float(SQLITE3STMT_LOAD_SPELLS, 11);
		spell->target_type = (epixel::SpellTargetType)sqlite_to_int(SQLITE3STMT_LOAD_SPELLS, 12);
		if (spell->target_type >= epixel::SPELL_TARGETTYPE_MAX) {
			logger.crit("Invalid spell_target type for spell %s: %d >= SPELL_TARGETTYPE_MAX", spell->name.c_str(), spell->target_type);
			delete spell;
			continue;
		}
		spell->id = spellId;
		spells[spellId] = spell;
		spellId++;
	}

	reset_stmt();
	return true;
}

bool Database_SQLite3::createUser(const std::string &login, const std::string &pwdhash)
{
	verifyDatabase();

	// Add user in database
	// `login`, `password(hash)`, `last_login`, `account_lock`
	str_to_sqlite(SQLITE3STMT_WRITE_USER, 1, login);
	str_to_sqlite(SQLITE3STMT_WRITE_USER, 2, pwdhash);
	sqlite3_vrfy(stmt_step(SQLITE3STMT_WRITE_USER), SQLITE_DONE);
	reset_stmt();

	return true;
}

bool Database_SQLite3::loadUserAndVerifyPassword(const std::string &login, const std::string &pwdhash)
{
	verifyDatabase();

	// Load user info and privileges
	str_to_sqlite(SQLITE3STMT_READ_USER, 1, login);
	if (stmt_step(SQLITE3STMT_READ_USER) != SQLITE_ROW) {
		reset_stmt();
		return false;
	}

	std::string pwdhashbdd = std::string(reinterpret_cast<const char*>(sqlite3_column_text(m_stmt[SQLITE3STMT_READ_USER], 2)));
	reset_stmt();

	return pwdhash.compare(pwdhashbdd) == 0;
}

bool Database_SQLite3::loadUser(const std::string &login, std::string &dstpassword)
{
	verifyDatabase();

	// Load user info and privileges
	str_to_sqlite(SQLITE3STMT_READ_USER, 1, login);
	if (stmt_step(SQLITE3STMT_READ_USER) != SQLITE_ROW) {
		reset_stmt();
		return false;
	}

	dstpassword = sqlite_to_string(SQLITE3STMT_READ_USER, 1);
	reset_stmt();
	return true;
}

bool Database_SQLite3::loadUserPrivs(const u32 user_id, std::set<std::string> &privs)
{
	privs.clear();
	sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_READ_USER_PRIVS], 1, user_id));
	while (stmt_step(SQLITE3STMT_READ_USER_PRIVS) == SQLITE_ROW) {
		privs.insert(std::string(reinterpret_cast<const char*>(sqlite3_column_text(m_stmt[SQLITE3STMT_READ_USER_PRIVS], 0))));
	}
	reset_stmt(SQLITE3STMT_READ_USER_PRIVS);
	return true;
}

bool Database_SQLite3::getUsernamesWithoutCase(const std::string &login, std::vector<std::string> &names)
{
	verifyDatabase();

	// Load user info and privileges
	str_to_sqlite(SQLITE3STMT_READ_USER_WITHOUTCASE, 1, login);
	while (stmt_step(SQLITE3STMT_READ_USER_WITHOUTCASE) == SQLITE_ROW) {
		names.push_back(sqlite_to_string(SQLITE3STMT_READ_USER_WITHOUTCASE, 0));
	}
	reset_stmt();
	return true;
}

u32 Database_SQLite3::userExists(const std::string &login)
{
	verifyDatabase();

	str_to_sqlite(SQLITE3STMT_READ_USER, 1, login);
	if (stmt_step(SQLITE3STMT_READ_USER) == SQLITE_ROW) {
		u32 id = sqlite3_column_int(m_stmt[SQLITE3STMT_READ_USER], 0);
		reset_stmt();
		return id;
	}

	reset_stmt();
	return 0;
}

bool Database_SQLite3::userExists(u32 id)
{
	verifyDatabase();

	sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_READ_USER_ID], 1, id));
	if (stmt_step(SQLITE3STMT_READ_USER_ID) == SQLITE_ROW) {
		reset_stmt();
		return true;
	}

	reset_stmt();
	return false;
}

bool Database_SQLite3::getUserListByIds(const std::vector<u32> &user_ids, std::vector<std::string> &res)
{
	for (const auto &uid: user_ids) {
		verifyDatabase();

		sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_READ_USER_LOGIN], 1, uid));
		if (stmt_step(SQLITE3STMT_READ_USER_LOGIN) == SQLITE_ROW) {
			res.push_back(sqlite_to_string(SQLITE3STMT_READ_USER_LOGIN, 0));
		}
		reset_stmt(SQLITE3STMT_READ_USER_LOGIN);
	}
	return true;
}

u32 Database_SQLite3::getLastLogin(const u32 id)
{
	verifyDatabase();

	// Load user info and privileges
	sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_READ_USER_LASTLOGIN], 1, id));

	u32 res = 0;
	if (stmt_step(SQLITE3STMT_READ_USER_LASTLOGIN) == SQLITE_ROW) {
		res = sqlite3_column_int(m_stmt[SQLITE3STMT_READ_USER_LASTLOGIN], 0);
	}

	reset_stmt();
	return res;
}

bool Database_SQLite3::setLastLogin(const std::string &login)
{
	verifyDatabase();

	str_to_sqlite(SQLITE3STMT_UPDATE_USER_LASTLOGIN, 1, login);
	sqlite3_vrfy(stmt_step(SQLITE3STMT_UPDATE_USER_LASTLOGIN), SQLITE_DONE);
	reset_stmt();
	return true;
}

bool Database_SQLite3::setPassword(const std::string &login, const std::string &pwdhash)
{
	verifyDatabase();

	if (userExists(login)) {
		str_to_sqlite(SQLITE3STMT_UPDATE_PASSWORD, 1, pwdhash);
		str_to_sqlite(SQLITE3STMT_UPDATE_PASSWORD, 2, login);
		sqlite3_vrfy(stmt_step(SQLITE3STMT_UPDATE_PASSWORD), SQLITE_DONE);
		reset_stmt();
		return true;
	}

	return false;
}

bool Database_SQLite3::removeUser(u32 id)
{
	verifyDatabase();

	if (userExists(id)) {
		sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_REMOVE_USER], 1, (sqlite3_uint64)id));
		sqlite3_vrfy(stmt_step(SQLITE3STMT_REMOVE_USER), SQLITE_DONE);
		reset_stmt();

		removeAllPrivs(id);
		return true;
	}

	return false;
}

bool Database_SQLite3::addPrivs(u32 id, const std::vector<std::string> &privs)
{
	verifyDatabase();

	// Add priv(s) in database for user
	for (auto &priv: privs) {
		sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_WRITE_USER_PRIV], 1, (sqlite3_uint64)id));
		str_to_sqlite(SQLITE3STMT_WRITE_USER_PRIV, 2, priv);
		sqlite3_vrfy(stmt_step(SQLITE3STMT_WRITE_USER_PRIV), SQLITE_DONE);
		reset_stmt();
	}

	return true;
}

bool Database_SQLite3::removePrivs(u32 id, const std::vector<std::string> &privs)
{
	verifyDatabase();

	for (auto &priv: privs) {
		sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_REMOVE_SINGLE_USER_PRIV], 1, (sqlite3_uint64)id));
		str_to_sqlite(SQLITE3STMT_REMOVE_SINGLE_USER_PRIV, 2, priv);
		sqlite3_vrfy(stmt_step(SQLITE3STMT_REMOVE_SINGLE_USER_PRIV), SQLITE_DONE);
		reset_stmt();
	}

	return true;
}

bool Database_SQLite3::removeAllPrivs(u32 id)
{
	verifyDatabase();

	sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_REMOVE_ALL_USER_PRIVS], 1, (sqlite3_uint64)id));
	sqlite3_vrfy(stmt_step(SQLITE3STMT_REMOVE_ALL_USER_PRIVS), SQLITE_DONE);
	reset_stmt();

	return true;
}

bool Database_SQLite3::loadPlayerIgnores(PlayerSAO* sao)
{
	verifyDatabase();
	sao->clearExtendedAttributes();

	int_to_sqlite(SQLITE3STMT_LOAD_PLAYER_IGNOREDLIST, 1, sao->getPlayer()->getDBId());
	while (stmt_step(SQLITE3STMT_LOAD_PLAYER_IGNOREDLIST) == SQLITE_ROW) {
		std::string ignorename = sqlite_to_string(SQLITE3STMT_LOAD_PLAYER_IGNOREDLIST, 0);
		sao->ignorePlayer(ignorename);
	}
	reset_stmt();
	return true;
}

bool Database_SQLite3::addPlayerIgnore(const u32 user_id, const std::string &ignorename)
{
	verifyDatabase();

	int64_to_sqlite(SQLITE3STMT_ADD_PLAYER_IGNORED_TOLIST, 1, user_id);
	str_to_sqlite(SQLITE3STMT_ADD_PLAYER_IGNORED_TOLIST, 2, ignorename);
	sqlite3_vrfy(stmt_step(SQLITE3STMT_ADD_PLAYER_IGNORED_TOLIST), SQLITE_DONE);
	reset_stmt();

	return true;
}

bool Database_SQLite3::removePlayerIgnore(const u32 user_id, const std::string &ignorename)
{
	verifyDatabase();

	int64_to_sqlite(SQLITE3STMT_REMOVE_PLAYER_IGNORED_FROMLIST, 1, user_id);
	str_to_sqlite(SQLITE3STMT_REMOVE_PLAYER_IGNORED_FROMLIST, 2, ignorename);
	sqlite3_vrfy(stmt_step(SQLITE3STMT_REMOVE_PLAYER_IGNORED_FROMLIST), SQLITE_DONE);
	reset_stmt();

	return true;
}

bool Database_SQLite3::savePlayerExtendedAttributes(const u32 user_id, const std::unordered_map<std::string, std::string> &attributes)
{
	beginSave();

	int_to_sqlite(SQLITE3STMT_REMOVE_PLAYER_EXT_ATTRIBUTES, 1, user_id);
	sqlite3_vrfy(stmt_step(SQLITE3STMT_REMOVE_PLAYER_EXT_ATTRIBUTES), SQLITE_DONE);
	reset_stmt();

	for (const auto &attr: attributes) {
		int_to_sqlite(SQLITE3STMT_SAVE_PLAYER_EXT_ATTRIBUTE, 1, user_id);
		str_to_sqlite(SQLITE3STMT_SAVE_PLAYER_EXT_ATTRIBUTE, 2, attr.first);
		str_to_sqlite(SQLITE3STMT_SAVE_PLAYER_EXT_ATTRIBUTE, 3, attr.second);
		sqlite3_vrfy(stmt_step(SQLITE3STMT_SAVE_PLAYER_EXT_ATTRIBUTE), SQLITE_DONE);
		reset_stmt();
	}

	endSave();
	return true;
}

bool Database_SQLite3::loadPlayerExtendedAttributes(PlayerSAO *sao)
{
	verifyDatabase();
	sao->clearExtendedAttributes();

	int_to_sqlite(SQLITE3STMT_LOAD_PLAYER_EXT_ATTRIBUTES, 1, sao->getPlayer()->getDBId());
	while (stmt_step(SQLITE3STMT_LOAD_PLAYER_EXT_ATTRIBUTES) == SQLITE_ROW) {
		std::string attr = sqlite_to_string(SQLITE3STMT_LOAD_PLAYER_EXT_ATTRIBUTES, 0);
		std::string value = sqlite_to_string(SQLITE3STMT_LOAD_PLAYER_EXT_ATTRIBUTES, 1);

		sao->setExtendedAttribute(attr, value);
	}
	reset_stmt();
	return true;
}

/**
 * @brief Database_SQLite3::savePlayer
 * @param player
 * @return false if failed, true if success
 *
 * Save player data to sqlite3
 *
 */
bool Database_SQLite3::savePlayer(RemotePlayer *player)
{
	PlayerSAO* sao = player->getPlayerSAO();
	if (!sao)
		return false;

	// Begin save in brace is mandatory
	if (!playerDataExists(player->getDBId())) {
		beginSave();
		int_to_sqlite(SQLITE3STMT_WRITE_PLAYER, 1, player->getDBId());
		double_to_sqlite(SQLITE3STMT_WRITE_PLAYER, 2, sao->getPitch());
		double_to_sqlite(SQLITE3STMT_WRITE_PLAYER, 3, sao->getYaw());
		double_to_sqlite(SQLITE3STMT_WRITE_PLAYER, 4, player->getPosition().X);
		double_to_sqlite(SQLITE3STMT_WRITE_PLAYER, 5, player->getPosition().Y);
		double_to_sqlite(SQLITE3STMT_WRITE_PLAYER, 6, player->getPosition().Z);
		sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_WRITE_PLAYER], 7, sao->getHP()));
		sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_WRITE_PLAYER], 8, sao->getBreath()));
		sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_WRITE_PLAYER], 9, sao->getMoney()));
		sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_WRITE_PLAYER], 10, sao->getHunger()));
		sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_WRITE_PLAYER], 11, sao->getMana()));
		sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_WRITE_PLAYER], 12, sao->getXP()));
		int_to_sqlite(SQLITE3STMT_WRITE_PLAYER, 13, sao->hasAcceptedRules() ? 1 : 0);

		sqlite3_vrfy(stmt_step(SQLITE3STMT_WRITE_PLAYER), SQLITE_DONE);
		reset_stmt();
	}
	else {
		beginSave();
		double_to_sqlite(SQLITE3STMT_UPDATE_PLAYER, 1, sao->getPitch());
		double_to_sqlite(SQLITE3STMT_UPDATE_PLAYER, 2, sao->getYaw());
		double_to_sqlite(SQLITE3STMT_UPDATE_PLAYER, 3, player->getPosition().X);
		double_to_sqlite(SQLITE3STMT_UPDATE_PLAYER, 4, player->getPosition().Y);
		double_to_sqlite(SQLITE3STMT_UPDATE_PLAYER, 5, player->getPosition().Z);
		sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_UPDATE_PLAYER], 6, sao->getHP()));
		sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_UPDATE_PLAYER], 7, sao->getBreath()));
		sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_UPDATE_PLAYER], 8, sao->getMoney()));
		sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_UPDATE_PLAYER], 9, sao->getHunger()));
		sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_UPDATE_PLAYER], 10, sao->getMana()));
		sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_UPDATE_PLAYER], 11, sao->getXP()));
		int_to_sqlite(SQLITE3STMT_UPDATE_PLAYER, 12, player->getDBId());

		sqlite3_vrfy(stmt_step(SQLITE3STMT_UPDATE_PLAYER), SQLITE_DONE);
		reset_stmt();
	}

	if (player->checkModified()) {
		// Write player inventories
		int_to_sqlite(SQLITE3STMT_REMOVE_PLAYER_INVENTORIES, 1, player->getDBId());
		sqlite3_vrfy(stmt_step(SQLITE3STMT_REMOVE_PLAYER_INVENTORIES), SQLITE_DONE);
		reset_stmt();

		int_to_sqlite(SQLITE3STMT_REMOVE_PLAYER_INVENTORY_ITEMS, 1, player->getDBId());
		sqlite3_vrfy(stmt_step(SQLITE3STMT_REMOVE_PLAYER_INVENTORY_ITEMS), SQLITE_DONE);
		reset_stmt();

		u16 i=0;
		for (const auto &list: sao->getInventory()->getLists()) {
			int_to_sqlite(SQLITE3STMT_ADD_PLAYER_INVENTORY, 1, player->getDBId());
			int_to_sqlite(SQLITE3STMT_ADD_PLAYER_INVENTORY, 2, i);
			int_to_sqlite(SQLITE3STMT_ADD_PLAYER_INVENTORY, 3, list->getWidth());
			str_to_sqlite(SQLITE3STMT_ADD_PLAYER_INVENTORY, 4, list->getName());
			int_to_sqlite(SQLITE3STMT_ADD_PLAYER_INVENTORY, 5, list->getSize());
			sqlite3_vrfy(stmt_step(SQLITE3STMT_ADD_PLAYER_INVENTORY), SQLITE_DONE);
			reset_stmt();

			for(u32 j = 0; j < list->getSize(); j++) {
				std::ostringstream os;
				list->getItem(j).serialize(os);
				std::string itemStr = os.str();

				int_to_sqlite(SQLITE3STMT_ADD_PLAYER_INVENTORY_ITEMS, 1, player->getDBId());
				int_to_sqlite(SQLITE3STMT_ADD_PLAYER_INVENTORY_ITEMS, 2, i);
				int_to_sqlite(SQLITE3STMT_ADD_PLAYER_INVENTORY_ITEMS, 3, j);
				str_to_sqlite(SQLITE3STMT_ADD_PLAYER_INVENTORY_ITEMS, 4, itemStr);
				sqlite3_vrfy(stmt_step(SQLITE3STMT_ADD_PLAYER_INVENTORY_ITEMS), SQLITE_DONE);
				reset_stmt();
			}

			i++;
		}
	}

	for (const auto &achievement: sao->getAchievementProgress()) {
		epixel::AchievementProgress ap = achievement.second;

		int_to_sqlite(SQLITE3STMT_DELETE_USER_AWARDS, 1, player->getDBId());
		int_to_sqlite(SQLITE3STMT_DELETE_USER_AWARDS, 2, ap.id_achievement);
		sqlite3_vrfy(stmt_step(SQLITE3STMT_DELETE_USER_AWARDS), SQLITE_DONE);
		reset_stmt(SQLITE3STMT_DELETE_USER_AWARDS);

		int_to_sqlite(SQLITE3STMT_SAVE_USER_AWARDS, 1, player->getDBId());
		int_to_sqlite(SQLITE3STMT_SAVE_USER_AWARDS, 2, ap.id_achievement);
		int_to_sqlite(SQLITE3STMT_SAVE_USER_AWARDS, 3, ap.progress);
		int_to_sqlite(SQLITE3STMT_SAVE_USER_AWARDS, 4, ap.unlocked ? 1 : 0);
		sqlite3_vrfy(stmt_step(SQLITE3STMT_SAVE_USER_AWARDS), SQLITE_DONE);
		reset_stmt(SQLITE3STMT_SAVE_USER_AWARDS);
	}

	endSave();

	return true;
}

/**
 * @brief Database_SQLite3::loadPlayer
 * @param player
 * @param sao
 * @return false if failed, true if success
 *
 * Load player data from sqlite3
 *
 */
bool Database_SQLite3::loadPlayer(RemotePlayer *player, PlayerSAO *sao)
{
	if (!sao)
		return false;
	verifyDatabase();

	int_to_sqlite(SQLITE3STMT_LOAD_PLAYER, 1, player->getDBId());
	if (stmt_step(SQLITE3STMT_LOAD_PLAYER) == SQLITE_ROW) {
		sao->setPitch(sqlite_to_float(SQLITE3STMT_LOAD_PLAYER, 0));
		sao->setYaw(sqlite_to_float(SQLITE3STMT_LOAD_PLAYER, 1));
		player->setPosition(sqlite_to_v3f(SQLITE3STMT_LOAD_PLAYER, 2));
		sao->setHP(sqlite_to_int(SQLITE3STMT_LOAD_PLAYER, 5));
		sao->setBreath(sqlite_to_int(SQLITE3STMT_LOAD_PLAYER, 6));
		sao->setMoney(sqlite_to_int(SQLITE3STMT_LOAD_PLAYER, 7));
		sao->setHunger(sqlite_to_int(SQLITE3STMT_LOAD_PLAYER, 8));
		sao->setHome(sqlite_to_v3f(SQLITE3STMT_LOAD_PLAYER, 9), false);
		sao->setMana(sqlite_to_int(SQLITE3STMT_LOAD_PLAYER, 12));
		sao->setXP((s64)sqlite_to_u64(SQLITE3STMT_LOAD_PLAYER, 13));
		if (sqlite_to_bool(SQLITE3STMT_LOAD_PLAYER, 14)) {
			sao->acceptRules();
		}
	}
	reset_stmt(SQLITE3STMT_LOAD_PLAYER);

	// Load inventory
	int_to_sqlite(SQLITE3STMT_LOAD_PLAYER_INVENTORIES, 1, player->getDBId());
	while (stmt_step(SQLITE3STMT_LOAD_PLAYER_INVENTORIES) == SQLITE_ROW) {
		InventoryList* invList = sao->getInventory()->addList(
				sqlite_to_string(SQLITE3STMT_LOAD_PLAYER_INVENTORIES, 2),
				sqlite_to_int(SQLITE3STMT_LOAD_PLAYER_INVENTORIES, 3));
		invList->setWidth(sqlite_to_int(SQLITE3STMT_LOAD_PLAYER_INVENTORIES, 1));

		u32 invId = sqlite_to_int(SQLITE3STMT_LOAD_PLAYER_INVENTORIES, 0);

		int_to_sqlite(SQLITE3STMT_LOAD_PLAYER_INVENTORY_ITEMS, 1, player->getDBId());
		int_to_sqlite(SQLITE3STMT_LOAD_PLAYER_INVENTORY_ITEMS, 2, invId);
		while (stmt_step(SQLITE3STMT_LOAD_PLAYER_INVENTORY_ITEMS) == SQLITE_ROW) {
			const std::string itemStr = sqlite_to_string(SQLITE3STMT_LOAD_PLAYER_INVENTORY_ITEMS, 1);
			if (itemStr.length() > 0) {
				ItemStack stack;
				stack.deSerialize(itemStr);
				invList->addItem(sqlite_to_int(SQLITE3STMT_LOAD_PLAYER_INVENTORY_ITEMS, 0), stack);
			}
		}
		reset_stmt(SQLITE3STMT_LOAD_PLAYER_INVENTORY_ITEMS);
	}

	reset_stmt(SQLITE3STMT_LOAD_PLAYER_INVENTORIES);

	// Load awards
	int_to_sqlite(SQLITE3STMT_LOAD_USER_AWARDS, 1, player->getDBId());
	while (stmt_step(SQLITE3STMT_LOAD_USER_AWARDS) == SQLITE_ROW) {
		sao->setAchievementProgress(
			sqlite_to_int(SQLITE3STMT_LOAD_USER_AWARDS, 0),
			sqlite_to_int(SQLITE3STMT_LOAD_USER_AWARDS, 1),
			sqlite_to_bool(SQLITE3STMT_LOAD_USER_AWARDS, 2));
	}
	reset_stmt(SQLITE3STMT_LOAD_USER_AWARDS);

	return true;
}

/**
 * @brief Database_SQLite3::playerDataExists
 * @param player
 * @return false if failed, true if success
 *
 * Verify if player data exists in sqlite3
 *
 */
bool Database_SQLite3::playerDataExists(const u32 user_id)
{
	verifyDatabase();

	int_to_sqlite(SQLITE3STMT_LOAD_PLAYER, 1, user_id);
	bool res = (stmt_step(SQLITE3STMT_LOAD_PLAYER) == SQLITE_ROW);
	reset_stmt();

	return res;
}

/**
 * @brief Database_SQLite3::setPlayerHome
 * @param player
 * @return false is failed, true is success
 *
 * Set player home position to pgsql
 *
 */
bool Database_SQLite3::setPlayerHome(RemotePlayer *player)
{
	if (!player->getPlayerSAO())
		return false;

	double_to_sqlite(SQLITE3STMT_PLAYER_SET_HOME, 1, player->getPosition().X);
	double_to_sqlite(SQLITE3STMT_PLAYER_SET_HOME, 2, player->getPosition().Y);
	double_to_sqlite(SQLITE3STMT_PLAYER_SET_HOME, 3, player->getPosition().Z);
	int_to_sqlite(SQLITE3STMT_PLAYER_SET_HOME, 4, player->getDBId());

	sqlite3_vrfy(stmt_step(SQLITE3STMT_PLAYER_SET_HOME), SQLITE_DONE);
	reset_stmt();

	return true;
}

bool Database_SQLite3::addPlayerToChannel(const u32 user_id, const std::string &channel)
{
	removePlayerFromChannel(user_id, channel);

	verifyDatabase();

	int_to_sqlite(SQLITE3STMT_ADD_PLAYER_TO_CHANNEL, 1, user_id);
	str_to_sqlite(SQLITE3STMT_ADD_PLAYER_TO_CHANNEL, 2, channel);
	sqlite3_vrfy(stmt_step(SQLITE3STMT_ADD_PLAYER_TO_CHANNEL), SQLITE_DONE);
	reset_stmt();

	return true;
}

bool Database_SQLite3::removePlayerFromChannel(const u32 user_id, const std::string &channel)
{
	verifyDatabase();

	int_to_sqlite(SQLITE3STMT_REMOVE_PLAYER_FROM_CHANNEL, 1, user_id);
	str_to_sqlite(SQLITE3STMT_REMOVE_PLAYER_FROM_CHANNEL, 2, channel);
	sqlite3_vrfy(stmt_step(SQLITE3STMT_REMOVE_PLAYER_FROM_CHANNEL), SQLITE_DONE);
	reset_stmt();

	return true;
}

bool Database_SQLite3::loadPlayerChannels(const u32 user_id, std::vector<std::string> &channels)
{
	verifyDatabase();
	channels.clear();

	int_to_sqlite(SQLITE3STMT_LOAD_PLAYER_CHANNELS, 1, user_id);
	while (stmt_step(SQLITE3STMT_REMOVE_PLAYER_FROM_CHANNEL) == SQLITE_ROW) {
		channels.push_back(sqlite_to_string(SQLITE3STMT_LOAD_PLAYER_CHANNELS, 0));
	}
	reset_stmt();

	return true;
}

/**
 * @brief Database_SQLite3::saveEnvMeta
 * @param g_time
 * @param time_of_day
 * @return
 *
 * Save environement meta to sqlite
 */
bool Database_SQLite3::saveEnvMeta(u32 g_time, u32 time_of_day, s16 automapgen_offset,
		const u64 &lbm_version, const std::string &lbm_introtimes_str, const u32 day_count)
{

	beginSave();
	sqlite3_vrfy(stmt_step(SQLITE3STMT_DELETE_ENV_META), SQLITE_DONE);
	reset_stmt();

	sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_SAVE_ENV_META], 1, g_time));
	sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_SAVE_ENV_META], 2, time_of_day));
	sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_SAVE_ENV_META], 3, automapgen_offset));
	sqlite3_vrfy(sqlite3_bind_int64(m_stmt[SQLITE3STMT_SAVE_ENV_META], 4, lbm_version));
	str_to_sqlite(SQLITE3STMT_SAVE_ENV_META, 5, lbm_introtimes_str.c_str());
	sqlite3_vrfy(sqlite3_bind_int(m_stmt[SQLITE3STMT_SAVE_ENV_META], 6, day_count));
	sqlite3_vrfy(stmt_step(SQLITE3STMT_SAVE_ENV_META), SQLITE_DONE);
	reset_stmt();
	endSave();

	return true;
}

/**
 * @brief Database_SQLite3::loadEnvMeta
 * @param env
 * @return
 *
 * Load environement meta from sqlite
 */
bool Database_SQLite3::loadEnvMeta(ServerEnvironment* env)
{
	verifyDatabase();

	if (stmt_step(SQLITE3STMT_LOAD_ENV_META) == SQLITE_ROW) {
		env->setGameTime(sqlite_to_int(SQLITE3STMT_LOAD_ENV_META, 0));
		env->setTimeOfDay(sqlite_to_int(SQLITE3STMT_LOAD_ENV_META, 1));
		env->setCurrentAutoMapgenOffset(sqlite_to_int(SQLITE3STMT_LOAD_ENV_META, 2));
		env->getLBMMgr()->loadIntroductionTimes(sqlite_to_string(SQLITE3STMT_LOAD_ENV_META, 4),
				env->getGameDef(), env->getGameTime());
		env->setDayCount(sqlite_to_int(SQLITE3STMT_LOAD_ENV_META, 5));
	}
	else {
		env->getLBMMgr()->loadIntroductionTimes("", env->getGameDef(), env->getGameTime());
	}
	reset_stmt();

	return true;
}

/**
 * @brief Database_SQLite3::loadAchievements
 * @param achievements
 * @return
 *
 * Load achievements from sqlite
 */
bool Database_SQLite3::loadAchievements(std::unordered_map<u32, epixel::Achievement> &achievements)
{
	verifyDatabase();

	while (stmt_step(SQLITE3STMT_LOAD_ACHIEVEMENTS) == SQLITE_ROW) {
		epixel::Achievement achievement;
		achievement.id = sqlite_to_int(SQLITE3STMT_LOAD_ACHIEVEMENTS, 0);
		achievement.name = sqlite_to_string(SQLITE3STMT_LOAD_ACHIEVEMENTS, 1);
		achievement.image = sqlite_to_string(SQLITE3STMT_LOAD_ACHIEVEMENTS, 2);
		achievement.description = sqlite_to_string(SQLITE3STMT_LOAD_ACHIEVEMENTS, 3);
		achievement.at = (epixel::AchievementType)sqlite_to_int(SQLITE3STMT_LOAD_ACHIEVEMENTS, 4);
		achievement.target_count = sqlite_to_int(SQLITE3STMT_LOAD_ACHIEVEMENTS, 5);
		achievement.target_what = sqlite_to_string(SQLITE3STMT_LOAD_ACHIEVEMENTS, 6);
		achievements[achievement.id] = achievement;
	}
	reset_stmt(SQLITE3STMT_LOAD_ACHIEVEMENTS);

	return true;
}


/**
 * @brief Database_SQLite3::addTeleportLocation
 * @param tl
 * @return
 */
bool Database_SQLite3::addTeleportLocation(const epixel::TeleportLocation &tl)
{
	verifyDatabase();

	beginSave();
	double_to_sqlite(SQLITE3STMT_DELETE_TELEPORTLOC, 1, tl.x);
	double_to_sqlite(SQLITE3STMT_DELETE_TELEPORTLOC, 2, tl.y);
	double_to_sqlite(SQLITE3STMT_DELETE_TELEPORTLOC, 3, tl.z);
	sqlite3_vrfy(stmt_step(SQLITE3STMT_DELETE_TELEPORTLOC), SQLITE_DONE);
	reset_stmt();

	double_to_sqlite(SQLITE3STMT_ADD_TELEPORTLOC, 1, tl.x);
	double_to_sqlite(SQLITE3STMT_ADD_TELEPORTLOC, 2, tl.y);
	double_to_sqlite(SQLITE3STMT_ADD_TELEPORTLOC, 3, tl.z);
	str_to_sqlite(SQLITE3STMT_ADD_TELEPORTLOC, 4, tl.label);
	sqlite3_vrfy(stmt_step(SQLITE3STMT_ADD_TELEPORTLOC), SQLITE_DONE);
	reset_stmt();
	endSave();

	return true;
}

/**
 * @brief Database_SQLite3::removeTeleportLocation
 * @param tl
 * @return
 */
bool Database_SQLite3::removeTeleportLocation(const epixel::TeleportLocation &tl)
{
	verifyDatabase();

	beginSave();
	double_to_sqlite(SQLITE3STMT_DELETE_TELEPORTLOC, 1, tl.x);
	double_to_sqlite(SQLITE3STMT_DELETE_TELEPORTLOC, 2, tl.y);
	double_to_sqlite(SQLITE3STMT_DELETE_TELEPORTLOC, 3, tl.z);
	sqlite3_vrfy(stmt_step(SQLITE3STMT_DELETE_TELEPORTLOC), SQLITE_DONE);
	reset_stmt();
	return true;
}

/**
 * @brief Database_SQLite3::loadTeleportLocations
 * @param tmgr
 * @return
 */
bool Database_SQLite3::loadTeleportLocations(epixel::TeleportMgr *tmgr)
{
	verifyDatabase();

	while (stmt_step(SQLITE3STMT_LOAD_TELEPORTLOC) == SQLITE_ROW) {
		tmgr->addTeleportLocation({
			sqlite_to_string(SQLITE3STMT_LOAD_TELEPORTLOC, 3),
			sqlite_to_float(SQLITE3STMT_LOAD_TELEPORTLOC, 0),
			sqlite_to_float(SQLITE3STMT_LOAD_TELEPORTLOC, 1),
			sqlite_to_float(SQLITE3STMT_LOAD_TELEPORTLOC, 2)
		}, true);
	}

	tmgr->regenFormspec();

	return true;
}

bool Database_SQLite3::loadBiomes(std::vector<Biome*> &biomes, BiomeManager* biomemgr)
{
	verifyDatabase();

	BiomeType biometype = BIOME_NORMAL;

	int_to_sqlite(SQLITE3STMT_LOAD_BIOMES, 1, g_settings->get(U16SETTING_MG_VERSION));
	while (stmt_step(SQLITE3STMT_LOAD_BIOMES) == SQLITE_ROW) {
		Biome *biome = biomemgr->create(biometype);
		if (!biome) {
			logger.warn("Biome creation failed, unknown type ?");
			continue;
		}

		biome->name	= sqlite_to_string(SQLITE3STMT_LOAD_BIOMES, 0);
		biome->depth_top = sqlite_to_int(SQLITE3STMT_LOAD_BIOMES, 3);
		biome->depth_filler = sqlite_to_int(SQLITE3STMT_LOAD_BIOMES, 5);
		biome->depth_water_top = sqlite_to_int(SQLITE3STMT_LOAD_BIOMES, 8);
		biome->y_min = sqlite_to_int(SQLITE3STMT_LOAD_BIOMES, 10);
		biome->y_max = sqlite_to_int(SQLITE3STMT_LOAD_BIOMES, 11);
		biome->heat_point = sqlite_to_int(SQLITE3STMT_LOAD_BIOMES, 12);
		biome->humidity_point = sqlite_to_int(SQLITE3STMT_LOAD_BIOMES, 13);
		biome->flags = g_settings->get(U16SETTING_MG_VERSION);

		biome->m_nodenames.push_back(sqlite_to_string(SQLITE3STMT_LOAD_BIOMES, 2));
		biome->m_nodenames.push_back(sqlite_to_string(SQLITE3STMT_LOAD_BIOMES, 4));
		biome->m_nodenames.push_back(sqlite_to_string(SQLITE3STMT_LOAD_BIOMES, 6));
		biome->m_nodenames.push_back(sqlite_to_string(SQLITE3STMT_LOAD_BIOMES, 7));
		biome->m_nodenames.push_back(sqlite_to_string(SQLITE3STMT_LOAD_BIOMES, 8));
		biome->m_nodenames.push_back(sqlite_to_string(SQLITE3STMT_LOAD_BIOMES, 9));
		biome->m_nodenames.push_back(sqlite_to_string(SQLITE3STMT_LOAD_BIOMES, 1));

		biome->m_nnlistsizes.push_back(biome->m_nodenames.size() - 1);

		biomes.push_back(biome);
	}
	reset_stmt(SQLITE3STMT_LOAD_BIOMES);
	return true;
}

bool Database_SQLite3::loadDecorations(std::vector<Decoration*> &decorations, DecorationManager* decomgr, SchematicManager* schmgr, BiomeManager* biomemgr)
{
	verifyDatabase();

	while (stmt_step(SQLITE3STMT_LOAD_DECORATIONS) == SQLITE_ROW) {
		enum DecorationType decotype = (DecorationType)sqlite_to_int(SQLITE3STMT_LOAD_DECORATIONS, 2);
		Decoration *deco = decomgr->create(decotype);
		if (!deco) {
			logger.crit("register_decoration: decoration placement type %d"
				" not implemented", decotype);
			continue;
		}

		deco->name       = sqlite_to_string(SQLITE3STMT_LOAD_DECORATIONS, 1);
		deco->fill_ratio = sqlite_to_float(SQLITE3STMT_LOAD_DECORATIONS, 5);
		deco->y_min      = sqlite_to_int(SQLITE3STMT_LOAD_DECORATIONS, 14);
		deco->y_max      = sqlite_to_int(SQLITE3STMT_LOAD_DECORATIONS, 15);
		deco->sidelen    = sqlite_to_int(SQLITE3STMT_LOAD_DECORATIONS, 4);
		if (deco->sidelen <= 0) {
			logger.error("register_decoration: sidelen must be greater than 0");
			continue;
		}
		std::string place_on = sqlite_to_string(SQLITE3STMT_LOAD_DECORATIONS, 3);
		str_remove_spaces(place_on);
		std::vector<std::string> v_place_on = str_split(place_on, ',');
		for (const auto &placeOn: v_place_on) {
			deco->m_nodenames.push_back(placeOn);
		}
		deco->m_nnlistsizes.push_back(deco->m_nodenames.size());
		if (deco->m_nnlistsizes.size() == 0) {
			logger.error("register_decoration: no place_on nodes defined");
			continue;
		}

		deco->flags = sqlite_to_int(SQLITE3STMT_LOAD_DECORATIONS, 17);

		// Noise Param
		deco->np.offset = sqlite_to_float(SQLITE3STMT_LOAD_DECORATIONS, 6);
		deco->np.scale = sqlite_to_float(SQLITE3STMT_LOAD_DECORATIONS, 7);
		deco->np.spread = sqlite_to_v3f(SQLITE3STMT_LOAD_DECORATIONS, 8);
		deco->np.seed = sqlite_to_int(SQLITE3STMT_LOAD_DECORATIONS, 11);
		deco->np.octaves = sqlite_to_int(SQLITE3STMT_LOAD_DECORATIONS, 12);
		deco->np.persist = sqlite_to_float(SQLITE3STMT_LOAD_DECORATIONS, 13);
		deco->np.lacunarity = 0;

		if (deco->np.seed != 0)
			deco->flags |= DECO_USE_NOISE;

		int_to_sqlite(SQLITE3STMT_LOAD_DECORATIONS_INTO, 1, sqlite_to_int(SQLITE3STMT_LOAD_DECORATIONS, 0));
		while (stmt_step(SQLITE3STMT_LOAD_DECORATIONS_INTO) == SQLITE_ROW) {
			deco->biomes.insert(biomemgr->getByName(sqlite_to_string(SQLITE3STMT_LOAD_DECORATIONS_INTO, 1))->index);
		}
		reset_stmt(SQLITE3STMT_LOAD_DECORATIONS_INTO);

		bool success = false;
		switch (decotype) {
			case DECO_SIMPLE:
			{
				DecoSimple *decoSimple = (DecoSimple*)deco;
				decoSimple->deco_height = sqlite_to_int(SQLITE3STMT_LOAD_DECORATIONS, 20);
				if (decoSimple->deco_height <= 0) {
					logger.error("register_decoration: simple decoration height"
						" must be greater than 0 for %s", decoSimple->name.c_str());
					break;
				}
				decoSimple->deco_height_max = sqlite_to_int(SQLITE3STMT_LOAD_DECORATIONS, 21);
				decoSimple->nspawnby = sqlite_to_int(SQLITE3STMT_LOAD_DECORATIONS, 23);

				std::string reg_deco = sqlite_to_string(SQLITE3STMT_LOAD_DECORATIONS, 19);
				if (reg_deco == "") {
					logger.error("register_decoration: no decoration nodes defined");
					break;
				}
				decoSimple->m_nodenames.push_back(reg_deco);
				decoSimple->m_nnlistsizes.push_back(1);

				std::string reg_spawn_by = sqlite_to_string(SQLITE3STMT_LOAD_DECORATIONS, 22);
				if (reg_spawn_by == "" && decoSimple->nspawnby != 0) {
					logger.error("register_decoration: no spawn_by nodes defined, but num_spawn_by specified");
					break;
				}
				decoSimple->m_nodenames.push_back(reg_spawn_by);
				decoSimple->m_nnlistsizes.push_back(1);

				success = true;
				break;
			}

			case DECO_SCHEMATIC:
			{
				DecoSchematic *decoSchem = (DecoSchematic*)deco;
				decoSchem->rotation = (Rotation)sqlite_to_int(SQLITE3STMT_LOAD_DECORATIONS, 18);

				std::string pathSchem = porting::path_share + "/games/epixel_game/mods/default/schematics/";
				Schematic *schem = SchematicManager::create(SCHEMATIC_NORMAL);
				if (!schem->loadSchematicFromFile( pathSchem + sqlite_to_string(SQLITE3STMT_LOAD_DECORATIONS, 16),
						decomgr->getNodeDef())) {
					logger.error("Schematic not found in : %s for %s", pathSchem.c_str(), decoSchem->name.c_str());
					continue;
				}
				schmgr->add(schem);
				decoSchem->schematic = schem;

				success = true;
				break;
			}

			case DECO_LSYSTEM:
				break;
		}
		if (!success) {
			delete deco;
			continue;
		}

		decorations.push_back(deco);
	}

	reset_stmt(SQLITE3STMT_LOAD_DECORATIONS);
	return true;
}

void Database_SQLite3::createDatabase()
{
	assert(m_database); // Pre-condition

	const std::string dbcreate_sql = "CREATE TABLE IF NOT EXISTS `dbversion` ("
	"	dbname VARCHAR(32) NOT NULL,"
	"	version BIGINT NOT NULL,"
	"	PRIMARY KEY(dbname)"
	");"
	"INSERT INTO `dbversion` (`dbname`,`version`) VALUES ('" + m_name + "',1);";

	sqlite3_vrfy(sqlite3_exec(m_database, dbcreate_sql.c_str(), NULL, NULL, NULL));

	logger.debug("SQLite3: Game Database was updated");
}

bool Database_SQLite3::incrementDatabase()
{
	u32 version;
	sqlite3_stmt *m_stmt_load_version;
	u32 current_db_version = 0;
	if (m_name.compare("map") == 0) {
		current_db_version = CURRENT_MAPDB_VERSION;
	}
	else if (m_name.compare("game") == 0) {
		current_db_version = CURRENT_GAMEDB_VERSION;
	}
	else if (m_name.compare("auth") == 0) {
		current_db_version = CURRENT_AUTHDB_VERSION;
	}
	else {
		logger.fatal("Unknown database with name %s", m_name.c_str());
		abort();
	}

	PREPARE_STATEMENT(load_version, "SELECT `version` FROM `dbversion` WHERE dbname = ?")
	sqlite3_vrfy(sqlite3_bind_text(m_stmt_load_version, 1, m_name.c_str(), m_name.size(), NULL));
	if (sqlite3_step(m_stmt_load_version) == SQLITE_ROW) {
		version = sqlite3_column_int64(m_stmt_load_version, 0);
	}
	else {
		throw DatabaseException(std::string(
				"SQLite database error: cannot recognize dbversion for database " + m_name + ", "
				"please check your database !"));
	}

	sqlite3_reset(m_stmt_load_version);

	logger.info("SQLite3 %s database version %d found.", m_name.c_str(), version);

	if (version == current_db_version) {
		FINALIZE_STATEMENT(m_stmt_load_version);
		logger.notice("%s database is already up-to-date.", m_name.c_str());
		return false;
	}

	// We need to update, read files by offset over DB_VERSION
	u16 loopbreak = 0;
	while (version < current_db_version && loopbreak < 1000) {
		loopbreak++;
		u32 i = version + 1;
		std::ifstream ifs;
		ifs.open(porting::path_share + "/sql/" + itos(i) + "_" + m_name + "_sqlite3.sql");
		if (!ifs.good()) {
			std::stringstream ss;
			ss << std::string("SQLite upgrade error: file ")
					<< porting::path_share << "/sql/" << i << std::string("_") << m_name
					<< std::string("_sqlite3.sql not found");
			throw DatabaseException(ss.str());
		}
		std::stringstream sqlStream;
		sqlStream << ifs.rdbuf();
		ifs.close();

		logger.notice("Apply SQL %d_%s_sqlite3.sql...", i, m_name.c_str());
		sqlite3_vrfy(sqlite3_exec(m_database,
			sqlStream.str().c_str(),
			NULL, NULL, NULL));

		sqlite3_vrfy(sqlite3_bind_text(m_stmt_load_version, 1, m_name.c_str(), m_name.size(), NULL));
		if (sqlite3_step(m_stmt_load_version) == SQLITE_ROW) {
			version = sqlite3_column_int64(m_stmt_load_version, 0);
		}
		sqlite3_reset(m_stmt_load_version);
	}

	FINALIZE_STATEMENT(m_stmt_load_version);

	if (loopbreak == 1000) {
		throw DatabaseException("SQLITE UPGRADE FAILED. Version was not updated to current version in some SQL file");
	}

	logger.notice("Database %s updated to version %d.", m_name.c_str(), current_db_version);

	return true;
}

void Database_SQLite3::listAllLoadableBlocks(std::vector<v3s16> &dst)
{
	verifyDatabase();

	while (stmt_step(SQLITE3STMT_LIST_BLOCKS) == SQLITE_ROW) {
		dst.push_back(getIntegerAsBlock(sqlite3_column_int64(m_stmt[SQLITE3STMT_LIST_BLOCKS], 0)));
	}
	reset_stmt();
}

/****************
 * Black magic! *
 ****************
 * The position hashing is very messed up.
 * It's a lot more complicated than it looks.
 */

static inline s16 unsigned_to_signed(u16 i, u16 max_positive)
{
	if (i < max_positive) {
		return i;
	} else {
		return i - (max_positive * 2);
	}
}


// Modulo of a negative number does not work consistently in C
static inline s64 pythonmodulo(s64 i, s16 mod)
{
	if (i >= 0) {
		return i % mod;
	}
	return mod - ((-i) % mod);
}


s64 Database_SQLite3::getBlockAsInteger(const v3s16 &pos)
{
	return (u64) pos.Z * 0x1000000 +
		(u64) pos.Y * 0x1000 +
		(u64) pos.X;
}

v3s16 Database_SQLite3::getIntegerAsBlock(s64 i)
{
	v3s16 pos;
	pos.X = unsigned_to_signed(pythonmodulo(i, 4096), 2048);
	i = (i - pos.X) / 4096;
	pos.Y = unsigned_to_signed(pythonmodulo(i, 4096), 2048);
	i = (i - pos.Y) / 4096;
	pos.Z = unsigned_to_signed(pythonmodulo(i, 4096), 2048);
	return pos;
}

v3f Database_SQLite3::getIntegerAsFloatBlock(s64 i)
{
	v3f pos;
	pos.X = unsigned_to_signed(pythonmodulo(i, 4096), 2048);
	i = (i - pos.X) / 4096;
	pos.Y = unsigned_to_signed(pythonmodulo(i, 4096), 2048);
	i = (i - pos.Y) / 4096;
	pos.Z = unsigned_to_signed(pythonmodulo(i, 4096), 2048);
	return pos;
}

s64 Database_SQLite3::getBlockAsInteger(const v3f &pos)
{
	return (u64) pos.Z * 0x1000000 +
		(u64) pos.Y * 0x1000 +
		(u64) pos.X;
}


Database_SQLite3::~Database_SQLite3()
{
	for (u16 i = 0; i < SQLITE3STMT_COUNT; i++) {
		FINALIZE_STATEMENT(m_stmt[i])
	}

	sqlite3_vrfy(sqlite3_close(m_database), "Failed to close database");
}

