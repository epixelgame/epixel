/*
Minetest
Copyright (C) 2013 celeron55, Perttu Ahola <celeron55@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef DATABASE_SQLITE3_HEADER
#define DATABASE_SQLITE3_HEADER

#include "database.h"
#include "assert.h"
#include <string>
#include "exceptions.h"
#include "emerge.h"
#include "contrib/awards.h"
#include "mg_decoration.h"
#include "util/string.h"

extern "C" {
	#include "sqlite3.h"
}

enum Sqlite3Statement
{
	SQLITE3STMT_READ_BLOCK,
	SQLITE3STMT_WRITE_BLOCK,
	SQLITE3STMT_LIST_BLOCKS,
	SQLITE3STMT_DELETE_BLOCK,
	SQLITE3STMT_DELETE_ENV_META,
	SQLITE3STMT_SAVE_ENV_META,
	SQLITE3STMT_LOAD_ENV_META,
	SQLITE3STMT_MAP_MAX, // MAP END
	SQLITE3STMT_WRITE_USER,
	SQLITE3STMT_READ_USER,
	SQLITE3STMT_READ_USER_ID,
	SQLITE3STMT_REMOVE_USER,
	SQLITE3STMT_READ_USER_WITHOUTCASE,
	SQLITE3STMT_UPDATE_PASSWORD,
	SQLITE3STMT_READ_USER_LASTLOGIN,
	SQLITE3STMT_UPDATE_USER_LASTLOGIN,
	SQLITE3STMT_READ_USER_LOGIN,
	SQLITE3STMT_AUTH_MAX, // AUTH END
	SQLITE3STMT_WRITE_AREA,
	SQLITE3STMT_WRITE_AREA_OWNER,
	SQLITE3STMT_WRITE_BAN_PLAYER,
	SQLITE3STMT_WRITE_BAN_IP,
	SQLITE3STMT_READ_AREAS,
	SQLITE3STMT_READ_AREA_OWNERS,
	SQLITE3STMT_REMOVE_AREA,
	SQLITE3STMT_REMOVE_AREA_OWNERS,
	SQLITE3STMT_LOAD_BANS_PLAYER,
	SQLITE3STMT_LOAD_BANS_IP,
	SQLITE3STMT_REMOVE_BAN_PLAYER,
	SQLITE3STMT_REMOVE_BAN_IP,
	SQLITE3STMT_READ_TIPS,
	SQLITE3STMT_READ_CREATUREDEFS,
	SQLITE3STMT_READ_CREATUREDEFS_LOOT,
	SQLITE3STMT_READ_CREATUREDEFS_NODE_REPLACE,
	SQLITE3STMT_READ_CREATUREDEFS_MESHES,
	SQLITE3STMT_READ_CREATUREDEFS_TEXTURES,
	SQLITE3STMT_READ_CREATUREDEFS_SPELLS,
	SQLITE3STMT_READ_CREATUREDEFS_ABMS,
	SQLITE3STMT_READ_CREATUREDEFS_ABMS_NODES,
	SQLITE3STMT_READ_USER_PRIVS,
	SQLITE3STMT_WRITE_USER_PRIV,
	SQLITE3STMT_REMOVE_SINGLE_USER_PRIV,
	SQLITE3STMT_REMOVE_ALL_USER_PRIVS,
	SQLITE3STMT_ADD_PLAYER_TO_CHANNEL,
	SQLITE3STMT_REMOVE_PLAYER_FROM_CHANNEL,
	SQLITE3STMT_LOAD_PLAYER_CHANNELS,
	SQLITE3STMT_READ_CRAFT_SHAPED,
	SQLITE3STMT_READ_CRAFT_COOKING,
	SQLITE3STMT_READ_CRAFT_FUEL,
	SQLITE3STMT_READ_CRAFT_SHAPELESS,
	SQLITE3STMT_LOAD_PLAYER_EXT_ATTRIBUTES,
	SQLITE3STMT_SAVE_PLAYER_EXT_ATTRIBUTE,
	SQLITE3STMT_REMOVE_PLAYER_EXT_ATTRIBUTES,
	SQLITE3STMT_WRITE_PLAYER,
	SQLITE3STMT_UPDATE_PLAYER,
	SQLITE3STMT_REMOVE_PLAYER_INVENTORIES,
	SQLITE3STMT_REMOVE_PLAYER_INVENTORY_ITEMS,
	SQLITE3STMT_ADD_PLAYER_INVENTORY,
	SQLITE3STMT_ADD_PLAYER_INVENTORY_ITEMS,
	SQLITE3STMT_LOAD_PLAYER_INVENTORIES,
	SQLITE3STMT_LOAD_PLAYER_INVENTORY_ITEMS,
	SQLITE3STMT_LOAD_PLAYER,
	SQLITE3STMT_PLAYER_SET_HOME,
	SQLITE3STMT_LOAD_TREE_ABMS_SCHEMES,
	SQLITE3STMT_LOAD_TREE_ABMS_SAPLINGS,
	SQLITE3STMT_LOAD_NODEREPLACE_ABMS,
	SQLITE3STMT_LOAD_ORES,
	SQLITE3STMT_LOAD_ORE_INTO,
	SQLITE3STMT_LOAD_ITEMDEFS,
	SQLITE3STMT_LOAD_ITEMDEFS_GROUPS,
	SQLITE3STMT_LOAD_ITEMDEFS_SOUNDS,
	SQLITE3STMT_LOAD_ITEMDEFS_TOOL_CAPABILITIES,
	SQLITE3STMT_LOAD_ITEMDEFS_TOOL_CAPABILITIES_DAMAGE_GROUPS,
	SQLITE3STMT_LOAD_ITEMDEFS_TOOL_CAPABILITIES_GROUPCAP,
	SQLITE3STMT_LOAD_ITEMDEFS_TOOL_CAPABILITIES_GROUPCAP_TIMES,
	SQLITE3STMT_LOAD_ITEMDEFS_NODES,
	SQLITE3STMT_LOAD_ITEMDEFS_NODES_DROPS,
	SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES,
	SQLITE3STMT_LOAD_ITEMDEFS_NODES_NODEBOXES_MULTI,
	SQLITE3STMT_LOAD_SPELLS,
	SQLITE3STMT_LOAD_ACHIEVEMENTS,
	SQLITE3STMT_LOAD_USER_AWARDS,
	SQLITE3STMT_SAVE_USER_AWARDS,
	SQLITE3STMT_DELETE_USER_AWARDS,
	SQLITE3STMT_ADD_TELEPORTLOC,
	SQLITE3STMT_DELETE_TELEPORTLOC,
	SQLITE3STMT_LOAD_TELEPORTLOC,
	SQLITE3STMT_LOAD_BIOMES,
	SQLITE3STMT_LOAD_DECORATIONS,
	SQLITE3STMT_LOAD_DECORATIONS_INTO,
	SQLITE3STMT_LOAD_PLAYER_IGNOREDLIST,
	SQLITE3STMT_ADD_PLAYER_IGNORED_TOLIST,
	SQLITE3STMT_REMOVE_PLAYER_IGNORED_FROMLIST,
	SQLITE3STMT_GAME_MAX,

	SQLITE3STMT_BEGIN,
	SQLITE3STMT_END,

	SQLITE3STMT_COUNT,
};

class Database_SQLite3 : public MapDatabase, public GameDatabase, public AuthDatabase
{
public:
	Database_SQLite3(const std::string &savedir, const std::string &name);

	void precheck() { verifyDatabase(); }
	void init_worker() {}

	static s64 getBlockAsInteger(const v3s16 &pos);
	static s64 getBlockAsInteger(const v3f &pos);
	static v3s16 getIntegerAsBlock(s64 i);
	static v3f getIntegerAsFloatBlock(s64 i);

	void beginSave();
	void endSave();

	bool saveBlock(const v3s16 &pos, const std::string &data, const epixel::MapBlockDB &blockdb_data);
	void loadBlock(const v3s16 &pos, std::string &data, epixel::MapBlockDB &blockdb_data);
	void listAllLoadableBlocks(std::vector<v3s16> &dst);

	bool saveArea(epixel::Area* area);
	bool deleteArea(u32 areaId);
	bool loadAreas(std::unordered_map<u32, epixel::Area*>& areas);

	bool savePlayerBan(const std::string &playername, epixel::Ban* ban);
	bool saveIPBan(const std::string &ip, epixel::Ban* ban);
	bool deletePlayerBan(const std::string &playername);
	bool deleteIPBan(const std::string &ip);
	bool loadPlayerBans(std::unordered_map<std::string, epixel::Ban*> &bans);
	bool loadIPBans(std::unordered_map<std::string, epixel::Ban*> &bans);

	bool loadCrafts(std::vector<CraftDefinition *> &crafts);

	bool loadTips(std::vector<std::wstring> &tips);
	bool loadTreeABM(INodeDefManager *ndef, std::vector<epixel::abm::DBTreeSaplingABM*> &abms);
	bool loadNodeReplaceABM(INodeDefManager* ndef, std::vector<epixel::abm::NodeReplaceABM*> &abms);
	bool loadOres(std::vector<Ore*> &ores, OreManager* oremgr);

	bool loadBiomes(std::vector<Biome *> &biomes, BiomeManager *biomemgr);
	bool loadDecorations(std::vector<Decoration*> &decorations, DecorationManager* decomgr, SchematicManager *schmgr, BiomeManager *biomemgr);

	/*
	 * Items
	 */
	bool loadItemDefinitions(IWritableItemDefManager* idef, IWritableNodeDefManager *ndef, GameScripting* script);

	/*
	 * Creatures
	 */
	bool loadLoots(epixel::CreatureDropMap &creaturedrops, u32 &loot_nb);
	bool loadCreatureNodeReplace(epixel::CreatureNodeReplaceMap &nodereplace, u32 &replace_nb);
	bool loadCreatureDefinitions(epixel::CreatureDefMap &creaturedefs, epixel::CreatureStore* cstore,
			u32 &meshes_nb, u32 &texture_nb, const epixel::SpellMap &spells);
	bool loadCreatureSpawningABM(INodeDefManager* ndef, std::vector<epixel::abm::CreatureSpawningABM*> &abms);
	bool loadSpells(epixel::SpellMap &spells);

	/*
	 * Auth
	 */
	bool createUser(const std::string &login, const std::string &pwdhash);
	bool loadUserAndVerifyPassword(const std::string &login, const std::string &pwdhash);
	bool loadUser(const std::string &login, std::string &dstpassword);
	bool loadUserPrivs(const u32 user_id, std::set<std::string> &privs);
	bool getUsernamesWithoutCase(const std::string &login, std::vector<std::string> &names);
	bool removeUser(u32 id);
	u32 userExists(const std::string &login);
	bool userExists(u32 id);
	bool getUserListByIds(const std::vector<u32> &user_ids, std::vector<std::string> &res);
	u32 getLastLogin(const u32 id);
	bool setLastLogin(const std::string &login);
	bool setPassword(const std::string &login, const std::string &pwdhash);

	// Ingame player
	bool loadPlayerExtendedAttributes(PlayerSAO* sao);
	bool savePlayerExtendedAttributes(const u32 user_id, const std::unordered_map<std::string, std::string> &attributes);
	bool loadPlayerIgnores(PlayerSAO* sao);
	bool addPlayerIgnore(const u32 user_id, const std::string &ignorename);
	bool removePlayerIgnore(const u32 user_id, const std::string &ignorename);

	// Channels
	bool addPlayerToChannel(const u32 user_id, const std::string &channel);
	bool removePlayerFromChannel(const u32 user_id, const std::string &channel);
	bool loadPlayerChannels(const u32 user_id, std::vector<std::string> &channels);

	// Privileges
	bool addPrivs(u32 id, const std::vector<std::string> &privs);
	bool removePrivs(u32 id, const std::vector<std::string> &privs);
	bool removeAllPrivs(u32 id);

	// Player
	bool savePlayer(RemotePlayer* player);
	bool loadPlayer(RemotePlayer* player, PlayerSAO* sao);
	bool playerDataExists(const u32 user_id);
	bool setPlayerHome(RemotePlayer* player);

	// Teleporters
	bool addTeleportLocation(const epixel::TeleportLocation& tl);
	bool removeTeleportLocation(const epixel::TeleportLocation& tl);
	bool loadTeleportLocations(epixel::TeleportMgr* tmgr);

	// Env Meta
	bool saveEnvMeta(u32 g_time, u32 time_of_day, s16 automapgen_offset, const u64 &lbm_version,
			const std::string &lbm_introtimes_str, const u32 day_count);
	bool loadEnvMeta(ServerEnvironment* env);

	// Awards
	bool loadAchievements(std::unordered_map<u32, epixel::Achievement> &achievements);

	bool initialized() const { return m_initialized; }

	~Database_SQLite3();

private:
	// Open the database
	void openDatabase();
	// Create the database structure
	void createDatabase();
	// Increment database if needed
	bool incrementDatabase();
	// Open and initialize the database if needed
	void verifyDatabase();

	void bindPos(sqlite3_stmt *stmt, const v3s16 &pos, int index=1);

	std::string m_name = "";
	bool m_initialized;

	inline const bool sqlite_to_bool(const Sqlite3Statement s, int iCol)
	{
		assert(s < SQLITE3STMT_COUNT);
		return (sqlite3_column_int(m_stmt[s], iCol) > 0);
	}

	inline const std::string sqlite_to_string(const Sqlite3Statement s, int iCol)
	{
		assert(s < SQLITE3STMT_COUNT);
		const char* text = reinterpret_cast<const char*>(sqlite3_column_text(m_stmt[s], iCol));
		return std::string(text ? text : "");
	}

	inline const u32 sqlite_to_int(const Sqlite3Statement s, int iCol)
	{
		assert(s < SQLITE3STMT_COUNT);
		return sqlite3_column_int(m_stmt[s], iCol);
	}

	inline const u64 sqlite_to_u64(const Sqlite3Statement s, int iCol)
	{
		assert(s < SQLITE3STMT_COUNT);
		return (u64)sqlite3_column_int64(m_stmt[s], iCol);
	}

	inline const float sqlite_to_float(const Sqlite3Statement s, int iCol)
	{
		assert(s < SQLITE3STMT_COUNT);
		return sqlite3_column_double(m_stmt[s], iCol);
	}

	inline const v2f sqlite_to_v2f(const Sqlite3Statement s, int iCol)
	{
		assert(s < SQLITE3STMT_COUNT);
		return v2f(sqlite3_column_double(m_stmt[s], iCol),
				sqlite3_column_double(m_stmt[s], iCol + 1));
	}

	inline const v3f sqlite_to_v3f(const Sqlite3Statement s, int iCol)
	{
		assert(s < SQLITE3STMT_COUNT);
		return v3f(sqlite3_column_double(m_stmt[s], iCol),
				sqlite3_column_double(m_stmt[s], iCol + 1),
				sqlite3_column_double(m_stmt[s], iCol + 2));
	}

	inline const aabb3f sqlite_to_aabb3f(const Sqlite3Statement s, const int iCol)
	{
		assert(s < SQLITE3STMT_COUNT);
		return aabb3f(
			sqlite_to_float(s, iCol),
			sqlite_to_float(s, iCol + 1),
			sqlite_to_float(s, iCol + 2),
			sqlite_to_float(s, iCol + 3),
			sqlite_to_float(s, iCol + 4),
			sqlite_to_float(s, iCol + 5)
		);
	}

	inline void str_to_sqlite(const Sqlite3Statement s, const int iCol, const std::string &str) const
	{
		assert(s < SQLITE3STMT_COUNT);
		sqlite3_vrfy(sqlite3_bind_text(m_stmt[s], iCol, str.c_str(), str.size(), NULL));
	}

	inline void int_to_sqlite(const Sqlite3Statement s, const int iCol, const int val) const
	{
		assert(s < SQLITE3STMT_COUNT);
		sqlite3_vrfy(sqlite3_bind_int(m_stmt[s], iCol, val));
	}

	inline void int64_to_sqlite(const Sqlite3Statement s, const int iCol, const s64 val) const
	{
		assert(s < SQLITE3STMT_COUNT);
		sqlite3_vrfy(sqlite3_bind_int64(m_stmt[s], iCol, (sqlite3_uint64)val));
	}

	inline void double_to_sqlite(const Sqlite3Statement s, const int iCol, const double val) const
	{
		assert(s < SQLITE3STMT_COUNT);
		sqlite3_vrfy(sqlite3_bind_double(m_stmt[s], iCol, val));
	}

	inline int stmt_step(const Sqlite3Statement s)
	{
		assert(s < SQLITE3STMT_COUNT);
		m_last_called_stmt = s;
		return sqlite3_step(m_stmt[s]);
	}

	inline int reset_stmt(Sqlite3Statement s = SQLITE3STMT_COUNT)
	{
		if (s == SQLITE3STMT_COUNT) {
			return sqlite3_reset(m_stmt[m_last_called_stmt]);
		}
		else {
			return sqlite3_reset(m_stmt[s]);
		}
	}

	inline void sqlite3_vrfy(const int s, const std::string &m = "", const int r = SQLITE_OK) const
	{
		if ((s) != (r)) { \
			throw DatabaseException(std::string(m) + ": " + sqlite3_errmsg(m_database));
		}
	}

	inline void sqlite3_vrfy(const int s, const int r, const std::string &m = "") const
	{
		sqlite3_vrfy(s, m, r);
	}

	std::string m_savedir;

	sqlite3 *m_database;
	sqlite3_stmt *m_stmt[SQLITE3STMT_COUNT];
	Sqlite3Statement m_last_called_stmt;

	s64 m_busy_handler_data[2];

	static int busyHandler(void *data, int count);
};

#endif

