/*
Minetest
Copyright (C) 2013 celeron55, Perttu Ahola <celeron55@gmail.com>
Copyright (C) 2013 Jonathan Neuschäfer <j.neuschaefer@gmx.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "filecache.h"

#include "network/networkprotocol.h"
#include "log.h"
#include "filesys.h"
#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>

bool FileCache::loadByPath(const std::string &path, std::ostream &os) const
{
	std::ifstream fis(path.c_str(), std::ios_base::binary);

	if(!fis.good()){
		logger.debug("FileCache: File not found in cache: %s", path.c_str());
		return false;
	}

	bool bad = false;
	for(;;){
		char buf[1024];
		fis.read(buf, 1024);
		std::streamsize len = fis.gcount();
		os.write(buf, len);
		if(fis.eof())
			break;
		if(!fis.good()){
			bad = true;
			break;
		}
	}
	if(bad){
		logger.error("FileCache: Failed to read file from cache: '%s'", path.c_str());
	}

	return !bad;
}

bool FileCache::updateByPath(const std::string &path, const std::string &data) const
{
	std::ofstream file(path.c_str(), std::ios_base::binary |
			std::ios_base::trunc);

	if(!file.good()) {
		logger.error("FileCache: Can't write to file at %s", path.c_str());
		return false;
	}

	file.write(data.c_str(), data.length());
	file.close();

	return !file.fail();
}

