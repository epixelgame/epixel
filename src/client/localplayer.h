/*
Minetest
Copyright (C) 2010-2013 celeron55, Perttu Ahola <celeron55@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef LOCALPLAYER_HEADER
#define LOCALPLAYER_HEADER

#include "player.h"
#include <list>

class Environment;
class GenericCAO;
class ClientActiveObject;

enum LocalPlayerAnimations {NO_ANIM, WALK_ANIM, DIG_ANIM, WD_ANIM};  // no local animation, walking, digging, both

class LocalPlayer : public Player
{
public:
	LocalPlayer(IGameDef *gamedef, const char *name);
	virtual ~LocalPlayer();

	bool isLocal() const
	{
		return true;
	}

	ClientActiveObject *parent;

	bool isAttached;

	v3f overridePosition;

	void move(f32 dtime, Environment *env, f32 pos_max_d);
	void move(f32 dtime, Environment *env, f32 pos_max_d,
			std::vector<CollisionInfo> *collision_info);

	void applyControl(float dtime);

	v3s16 getStandingNodePos();

	// Used to check if anything changed and prevent sending packets if not
	v3f last_position;
	v3f last_speed;
	float last_pitch;
	float last_yaw;
	unsigned int last_keyPressed;

	float camera_impact;

	int last_animation;
	float last_animation_speed;

	std::string hotbar_image;
	std::string hotbar_selected_image;

	video::SColor light_color;

	GenericCAO* getCAO() const {
		return m_cao;
	}

	void setCAO(GenericCAO* toset) {
		assert( m_cao == NULL ); // Pre-condition
		m_cao = toset;
	}

	void setLocalAnimations(v2s32 frames[4], float frame_speed)
	{
		for (int i = 0; i < 4; i++)
			local_animations[i] = frames[i];
		local_animation_speed = frame_speed;
	}

	void getLocalAnimations(v2s32 *frames, float *frame_speed)
	{
		for (int i = 0; i < 4; i++)
			frames[i] = local_animations[i];
		*frame_speed = local_animation_speed;
	}

	float getLocalAnimationSpeed() { return local_animation_speed; }
	void setLocalAnimationSpeed(float s) { local_animation_speed = s; }
	v2s32 getLocalAnimation(u8 idx)
	{
		assert (idx < 4);
		return local_animations[idx];
	}
	void setLocalAnimation(u8 idx, v2s32 val)
	{
		assert (idx < 4);
		local_animations[idx] = val;
	}
	bool touchGround() { return touching_ground; }
	bool swimVertical() { return swimming_vertical; }
	bool inLiquid() { return in_liquid; }
	bool inLiquidStable() { return in_liquid_stable; }
	u8 getLiquidViscosity() { return liquid_viscosity; }
	bool isClimbing() { return is_climbing; }

	void setPhysicsOverrideSpeed(const float speed) { physics_override_speed = speed; }
	void setPhysicsOverrideJump(const float j) { physics_override_jump = j; }
	void setPhysicsOverrideGravity(const float g) { physics_override_gravity = g; }
	float getPhysicsOverrideGravity() { return physics_override_gravity; }
	void setPhysicsOverrideSneak(const bool s) { physics_override_sneak = s; }
	void setPhysicsOverrideSneakGlitch(const bool s) { physics_override_sneak_glitch = s; }

	float getHurtTiltTimer() { return hurt_tilt_timer; }
	void setHurtTiltTimer(float t) { hurt_tilt_timer = t; }
	float getHurtTiltStrength() { return hurt_tilt_strength; }
	void setHurtTiltStrength(float t) { hurt_tilt_strength = t; }

	const v3s16 getLightPosition() const;

	f32 movement_acceleration_default;
	f32 movement_acceleration_air;
	f32 movement_acceleration_fast;
	f32 movement_speed_crouch;
	f32 movement_speed_climb;
	f32 movement_speed_jump;
	f32 movement_liquid_fluidity;
	f32 movement_liquid_fluidity_smooth;
	f32 movement_liquid_sink;
	f32 movement_gravity;

	const u16 getPeerID() const { return m_peer_id; }
	void setPeerID(const u16 peer_id) { m_peer_id = peer_id; }

	Inventory inventory;

	inline const v3f getSpeed() const { return m_speed; }
	void setSpeed(const v3f &speed) { m_speed = speed; }

	void setYaw(const f32 yaw) { m_yaw = yaw; }
	inline f32 getYaw() const { return m_yaw; }

	inline u16 getBreath() const { return m_breath; }
	void setBreath(const u16 breath) { m_breath = breath; }

	void setPitch(const f32 pitch) { m_pitch = pitch; }
	f32 getPitch() const { return m_pitch; }

	const v3f getEyeOffset()
	{
		float eye_height = camera_barely_in_ceiling ? 1.5f : 1.625f;
		return v3f(0, BS * eye_height, 0);
	}
	inline const v3f getEyePosition() { return m_position + getEyeOffset(); }

	inline s16 getHP() const { return m_hp; }
	inline void setHP(const s16 hp) { m_hp = hp; }

	inline void setTeleported(const bool t) { m_got_teleported = t; }
	inline const bool gotTeleported() const { return m_got_teleported; }
private:
	IGameDef *m_gamedef;

	void accelerateHorizontal(const v3f &target_speed, const f32 max_increase);
	void accelerateVertical(const v3f &target_speed, f32 max_increase);

	v3f m_speed = v3f(0,0,0);
	f32 m_yaw = 0;
	f32 m_pitch = 0;
	// This is used for determining the sneaking range
	v3s16 m_sneak_node;
	// Whether the player is allowed to sneak
	bool m_sneak_node_exists;
	// Whether recalculation of the sneak node is needed
	bool m_need_to_get_new_sneak_node;
	// Stores the max player uplift by m_sneak_node and is updated
	// when m_need_to_get_new_sneak_node == true
	f32 m_sneak_node_bb_ymax;
	// Node below player, used to determine whether it has been removed,
	// and its old type
	v3s16 m_old_node_below;
	std::string m_old_node_below_type;
	bool m_can_jump;
	bool touching_ground;
	bool m_got_teleported = false;
	// This oscillates so that the player jumps a bit above the surface
	bool in_liquid;
	// This is more stable and defines the maximum speed of the player
	bool in_liquid_stable;
	// Gets the viscosity of water to calculate friction
	u8 liquid_viscosity;
	bool is_climbing;
	bool swimming_vertical;
	u16 m_breath = PLAYER_MAX_BREATH;
	s16 m_hp = 0;
	bool camera_barely_in_ceiling = false;

	float physics_override_speed;
	float physics_override_jump;
	float physics_override_gravity;
	bool physics_override_sneak;
	bool physics_override_sneak_glitch;

	core::aabbox3d<f32> m_collisionbox = core::aabbox3d<f32>(-BS*0.30,0.0,-BS*0.30,BS*0.30,BS*1.75,BS*0.30);

	v2s32 local_animations[4];
	float local_animation_speed;

	u16 m_peer_id;

	float hurt_tilt_timer;
	float hurt_tilt_strength;

	GenericCAO* m_cao;
};

#endif

