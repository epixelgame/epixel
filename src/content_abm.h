/*
Minetest
Copyright (C) 2013 celeron55, Perttu Ahola <celeron55@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef CONTENT_ABM_HEADER
#define CONTENT_ABM_HEADER

#include <vector>
#include <string>
#include <set>
#include "irrlichttypes_bloated.h"
#include "lua_api/l_base.h"
#include "mapnode.h"

class ServerEnvironment;

class ActiveBlockModifier
{
public:
	ActiveBlockModifier(float interval);
	virtual ActiveBlockModifier* clone() const { return new ActiveBlockModifier(*this); }
	virtual ~ActiveBlockModifier() {}

	// Set of contents to trigger on
	virtual std::vector<content_t> getTriggerContents() { return std::vector<content_t>(); }

	const bool findNeighbor(content_t c)
	{
		return (m_required_neighbors.find(c) != m_required_neighbors.end());
	}

	bool hasRequiredNeighbors() { return !m_required_neighbors.empty(); }

	// Trigger interval in seconds
	virtual const float getTriggerInterval() { return m_trigger_interval; }

	// Random chance of (1 / return value), 0 is disallowed
	u32 getTriggerChance() { return m_trigger_chance; }
	const u32 getCurrentChance() { return m_chance; }

	// This is called usually at interval for 1/chance of the nodes
	virtual void trigger(ServerEnvironment *env, v3s16 p, MapNode n,
			u32 active_object_count, u32 active_object_count_wider) {}

	const float getNextInterval() { return m_trigger_timer; }
	void resetTimerIfTriggered();
	void step(const float dtime);
	const bool isReady() { return (m_trigger_timer <= 0.0f); }

protected:
	float m_trigger_timer; // Timer
	float m_trigger_interval; // Fixed value for timer
	u32 m_chance;
	u32 m_trigger_chance; // Fixed chance value

	std::set<content_t> m_required_neighbors;
};


class LuaABM : public ActiveBlockModifier
{
private:
	int m_id;

	std::vector<content_t> m_trigger_contents;

public:
	LuaABM(lua_State *L, int id,
			const std::vector<content_t> &trigger_contents,
			const std::set<content_t> &required_neighbors,
			float trigger_interval, u32 trigger_chance):
		ActiveBlockModifier(trigger_interval),
		m_id(id),
		m_trigger_contents(trigger_contents)
	{
		m_trigger_chance = trigger_chance;
		m_required_neighbors = required_neighbors;
	}

	virtual std::vector<content_t> getTriggerContents()
	{
		return m_trigger_contents;
	}
	virtual void trigger(ServerEnvironment *env, v3s16 p, MapNode n,
			u32 active_object_count, u32 active_object_count_wider);
};

#endif
