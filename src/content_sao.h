/*
Minetest
Copyright (C) 2010-2013 celeron55, Perttu Ahola <celeron55@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef CONTENT_SAO_HEADER
#define CONTENT_SAO_HEADER

#include "serverobject.h"
#include "itemgroup.h"
#include "player.h"
#include "object_properties.h"

struct DamageOverTime
{
	DamageOverTime(u32 m1, u32 m2, float p, float lifetimer):
		damage_min(m1), damage_max(m2), period(p), current_period(p), life(lifetimer) {}
	const u32 damage_min;
	const u32 damage_max;
	const float period;
	float current_period;
	float life;
};

class UnitSAO: public ServerActiveObject
{
public:
	UnitSAO(ServerEnvironment *env, v3f pos): ServerActiveObject(env, pos),
			m_hp(-1), m_yaw(0.0f) {}

	virtual inline void setHP(s16 hp)
	{
		if(hp < 0) hp = 0;
		m_hp = hp;
	}

	virtual s16 getHP() const { return m_hp; }
	inline void setYaw(const float yaw) { m_yaw = yaw; }
	inline f32 getYaw() const { return m_yaw; }
	inline f32 getPitch() const { return m_pitch; }
	f32 getRadYaw() const { return (m_yaw + 90.) * core::DEGTORAD; }
	f32 getRadPitch() const { return -1.0 * m_pitch * core::DEGTORAD; }

	virtual void step(float dtime, bool send_recommended);
	virtual void doDamage(u16 damage, ServerActiveObject *puncher = NULL) {}
	inline void applyDot(DamageOverTime* dot) { m_applied_dots.push_back(dot); }
protected:
	virtual bool castSpell(const u32 spellId, v3f range_pos, float y_cast_offset = 0.0f) const;
	std::vector<DamageOverTime*> m_applied_dots;
	s16 m_hp;
	f32 m_yaw = 0;
	f32 m_pitch = 0;
};

/*
	LuaEntitySAO needs some internals exposed.
*/

class LuaEntitySAO : public UnitSAO
{
public:
	LuaEntitySAO(ServerEnvironment *env, v3f pos,
			const std::string &name, const std::string &state);
	~LuaEntitySAO();
	ActiveObjectType getType() const
	{ return ACTIVEOBJECT_TYPE_LUAENTITY; }
	ActiveObjectType getSendType() const
	{ return ACTIVEOBJECT_TYPE_GENERIC; }
	virtual void addedToEnvironment(u32 dtime_s);
	static ServerActiveObject* create(ServerEnvironment *env, const v3f& pos,
			const std::string &data);
	bool isAttached();
	void step(float dtime, bool send_recommended);
	std::string getClientInitializationData(u16 protocol_version);
	void getStaticData(std::string &static_data) const;
	int punch(v3f dir,
			const ToolCapabilities *toolcap=NULL,
			ServerActiveObject *puncher=NULL,
			float time_from_last_punch=1000000);
	void rightClick(ServerActiveObject *clicker);
	void setPos(const v3f& pos);
	void moveTo(v3f pos, bool continuous);
	float getMinimumSavedMovement();
	std::string getDescription();
	void setArmorGroups(const ItemGroupList &armor_groups);
	ItemGroupList getArmorGroups();
	void setAnimation(v2f frame_range, float frame_speed, float frame_blend, bool frame_loop);
	void getAnimation(v2f *frame_range, float *frame_speed, float *frame_blend, bool *frame_loop);
	void setBonePosition(const std::string &bone, v3f position, v3f rotation);
	void getBonePosition(const std::string &bone, v3f *position, v3f *rotation);
	void setAttachment(int parent_id, const std::string &bone, v3f position, v3f rotation);
	void getAttachment(int *parent_id, std::string *bone, v3f *position, v3f *rotation);
	void addAttachmentChild(int child_id);
	void removeAttachmentChild(int child_id);
	std::set<int> getAttachmentChildIds();
	ObjectProperties* accessObjectProperties();
	void notifyObjectPropertiesModified();
	/* LuaEntitySAO-specific */
	inline virtual void setVelocity(const v3f velocity) { m_velocity = velocity; }
	inline v3f getVelocity() const { return m_velocity; }
	inline void setAcceleration(const v3f acceleration) { m_acceleration = acceleration; }
	inline v3f getAcceleration() const { return m_acceleration; }
	void setTextureMod(const std::string &mod);
	void setSprite(v2s16 p, int num_frames, float framelength,
			bool select_horiz_by_yawpitch);

	inline const std::string getName() {
		return m_init_name;
	}

	bool getCollisionBox(aabb3f *toset);
	bool collideWithObjects();
	std::string getInitState() { return m_init_state; }
protected:
	std::string m_init_name;
	std::string m_init_state;
	bool m_registered;

	v3f m_velocity;
	v3f m_acceleration;
private:
	void sendPosition(bool do_interpolate, bool is_movement_end);

	ItemGroupList m_armor_groups;

	bool m_properties_sent;
	float m_last_sent_yaw;
	v3f m_last_sent_position;
	v3f m_last_sent_velocity;
	float m_last_sent_position_timer;
	float m_last_sent_move_precision;
	bool m_armor_groups_sent;

	v2f m_animation_range;
	float m_animation_speed;
	float m_animation_blend;
	bool m_animation_loop;
	bool m_animation_sent;

	std::map<std::string, core::vector2d<v3f> > m_bone_position;
	bool m_bone_position_sent;

	int m_attachment_parent_id;
	std::set<int> m_attachment_child_ids;
	std::string m_attachment_bone;
	v3f m_attachment_position;
	v3f m_attachment_rotation;
	bool m_attachment_sent;
};

#endif
