/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "contrib/abm.h"
#include "contrib/areas.h"
#include "contrib/creature.h"
#include "daynightratio.h"
#include "craftdef.h"
#include "environment.h"
#include "map.h"
#include "mapblock.h"
#include "mg_schematic.h"
#include "server.h"
#include "settings.h"

static const char* inactive_furnace_formspec =
	"size[8,8.5]"
	"bgcolor[#080808BB;true]"
	"background[5,5;1,1;gui_formbg.png;true]"
	"listcolors[#00000069;#5A5A5A;#141318;#30434C;#FFF]"
	"list[current_name;src;2.75,0.5;1,1;]"
	"list[current_name;fuel;2.75,2.5;1,1;]"
	"image[2.75,1.5;1,1;default_furnace_fire_bg.png]"
	"image[3.75,1.5;1,1;gui_furnace_arrow_bg.png^[transformR270]"
	"list[current_name;dst;4.75,0.96;2,2;]"
	"list[current_player;main;0,4.25;8,1;]"
	"list[current_player;main;0,5.5;8,3;8]"
	"listring[current_name;dst]"
	"listring[current_player;main]"
	"listring[current_name;src]"
	"listring[current_player;main]"
	"default.get_hotbar_bg(0, 4.25)";

ActiveBlockModifier::ActiveBlockModifier(float interval):
	m_trigger_interval(interval)
{
	// Spread first ABM load
	m_trigger_timer = interval * (myrand_range(110, 210) / 100.0f);
	m_chance = 1;
	m_trigger_chance = 1;
}

void ActiveBlockModifier::step(const float dtime)
{
	m_trigger_timer -= dtime;
	if (m_trigger_timer <= 0.0f) {
		m_chance = (u32) (m_trigger_chance / dtime);
		if(m_chance == 0)
			m_chance = 1;

		m_trigger_timer = 0.0f;
	}
}

void ActiveBlockModifier::resetTimerIfTriggered()
{
	if (m_trigger_timer <= 0.0f) {
		m_trigger_timer = m_trigger_interval;
	}
}

namespace epixel
{

namespace abm {

GenericABM::GenericABM(INodeDefManager *ndef, float interval,
		u32 chance, const std::vector<std::string> &nodes):
		ActiveBlockModifier(interval)
{
	m_trigger_chance = chance;
	// Resolve node Ids
	for (const auto &s: nodes) {
		ndef->getIds(s, m_nodes);
	}
}

GenericABM::GenericABM(INodeDefManager *ndef, float interval,
		u32 chance, const std::vector<std::string> &nodes,
		const std::set<std::string> &neighbors):
	GenericABM(ndef, interval, chance, nodes)
{
	// Resolve node Ids
	for (const auto &s: neighbors) {
		ndef->getIds(s, m_required_neighbors);
	}
}

CreatureSpawningABM::CreatureSpawningABM(INodeDefManager *ndef,
		const std::vector<std::string> &nodes, const std::string &cname,
		float interval, u32 chance, u32 min_light, u32 max_light,
		u32 activeobject_count, s32 min_height, s32 max_height):
		GenericABM(ndef, interval, chance, nodes, {"air"}),
		m_min_light(min_light), m_max_light(max_light),
		m_activeobject_count(activeobject_count), m_min_height(min_height),
		m_max_height(max_height), m_creature_name(cname)
{
}

void CreatureSpawningABM::trigger(ServerEnvironment *env, v3s16 p,
		MapNode n, u32 active_object_count, u32 active_object_count_wider)
{
	// Too many SAO with this type, don't spawn
	if (active_object_count_wider > m_activeobject_count) {
		return;
	}

	Server* server = env->getGameDef();
	epixel::AreaMgr* areaMgr = server->getAreaMgr();

	v3s16 upper_pos(p.X, p.Y + 1, p.Z);

	// Don't spawn on non loaded nodes
	MapNode upper_n = env->getMap().getNode(upper_pos);
	if (upper_n.getContent() == CONTENT_IGNORE) {
		return;
	}

	// If light or position is not good
	u8 light = env->getNodeLight(upper_pos);
	if (!light || light > m_max_light || light < m_min_light ||
			upper_pos.Y > m_max_height || upper_pos.Y < m_min_height) {
		return;
	}

	INodeDefManager* ndef = server->getNodeDefManager();
	const ContentFeatures &f = ndef->get(upper_n);
	if (f.walkable) {
		return;
	}

	// @TODO, maybe later, mobs could be higher than 2 nodes, add support for this
	v3s16 doubleupper_pos(p.X, p.Y + 1, p.Z);
	MapNode doubleupper_n = env->getMap().getNode(doubleupper_pos);
	const ContentFeatures &f2 = ndef->get(doubleupper_n);
	// Look if there is a solid no on the mob head
	if (f2.walkable) {
		return;
	}

	v3f spawn_pos(p.X * BS, p.Y * BS, p.Z * BS);
	// If mob spawning in protected areas is disabled & zone is protected
	// This check is done here because it's the most consumming part
	if (!g_settings->get(BOOLSETTING_ENABLE_MOB_SPAWNING_IN_PROTECTED_AREAS) && !areaMgr->canInteract(spawn_pos)) {
		return;
	}

	// A little bit more higher spawn
	spawn_pos.Y += 1.5 * BS;

	// Don't spawn monsters if they are near players (15 blocks)
	epixel::CreatureDef* c_def = server->getCreatureStore()->getDefinition(m_creature_name);
	if (c_def && c_def->type == epixel::CREATURETYPE_MONSTER &&
			env->isPlayerNearPosition(spawn_pos, 15 * BS)) {
		logger.debug("Creature %s at (%f,%f,%f) is too near, don't spawn", m_creature_name.c_str(),
				spawn_pos.X, spawn_pos.Y, spawn_pos.Z);
		return;
	}

	epixel::Creature *obj = epixel::Creature::createCreatureObj(env, spawn_pos, m_creature_name);
	env->addActiveObject(obj);
}

NodeReplaceABM::NodeReplaceABM(INodeDefManager *ndef, float interval,
		u32 chance, const std::string &nodes, const std::string &replace_with):
		GenericABM(ndef, interval, chance, {nodes})
{
	ndef->getId(replace_with, m_replace_with);
}

void NodeReplaceABM::trigger(ServerEnvironment *env, v3s16 p, MapNode n,
		u32 active_object_count, u32 active_object_count_wider)
{
	env->setNode(p, MapNode(m_replace_with));
}

DirtABM::DirtABM(INodeDefManager *ndef):
		GenericABM(ndef, 2.0f, 150,
		{"default:dirt"}) {}

void DirtABM::trigger(ServerEnvironment *env, v3s16 p, MapNode n,
		u32 active_object_count, u32 active_object_count_wider)
{
	v3s16 above = p; above.Y++;
	Server* server = env->getGameDef();
	INodeDefManager* ndef = server->getNodeDefManager();
	MapNode node = env->getMap().getNode(above);
	const ContentFeatures &f = ndef->get(node);
	u32 dnr = time_to_daynight_ratio(env->getTimeOfDay(), true) % 24000;

	// Check if light touch the node and there is no liquid over
	if ((f.sunlight_propagates || f.param_type == CPT_LIGHT) &&
			f.liquid_type == LIQUID_NONE && node.getLightBlend(dnr, ndef) >= 13)
	{
		const std::string abovename = ndef->get(node).name;
		if (abovename.compare("default:snow") == 0 ||
				abovename.compare("default:snowblock") == 0) {
			env->setNode(p, MapNode(ndef, "default:dirt_with_snow", 0, 0));
		}
		else {
			env->setNode(p, MapNode(ndef, "default:dirt_with_grass", 0, 0));
		}
	}
}

DirtWithGrassABM::DirtWithGrassABM(INodeDefManager *ndef):
		GenericABM(ndef, 2.0f, 20,
		{"default:dirt_with_grass", "default:dirt_with_dry_grass"}) {}

void DirtWithGrassABM::trigger(ServerEnvironment *env, v3s16 p, MapNode n,
		u32 active_object_count, u32 active_object_count_wider)
{
	v3s16 above = p; above.Y++;
	INodeDefManager* ndef = env->getGameDef()->getNodeDefManager();
	MapNode node = env->getMap().getNode(above);
	const ContentFeatures &f = ndef->get(node);

	if (node.getContent() != CONTENT_IGNORE &&
			!(f.sunlight_propagates || f.param_type == CPT_LIGHT) &&
			f.liquid_type == LIQUID_NONE) {
		env->setNode(p, MapNode(ndef, "default:dirt", 0, 0));
	}
}

StoneABM::StoneABM(INodeDefManager *ndef):
		GenericABM(ndef, 60.0f, 30,
		{"default:stone", "default:cobblestone"},{"group:water"})
{
	ndef->getId("default:mossycobble", m_mossycobbleid);
}

void StoneABM::trigger(ServerEnvironment *env, v3s16 p, MapNode n,
		u32 active_object_count, u32 active_object_count_wider)
{
	env->setNode(p, MapNode(m_mossycobbleid));
}

TorchABM::TorchABM(INodeDefManager *ndef):
		GenericABM(ndef, 1.0f, 1,
		{"default:torch"}, {"group:water"})
{
	ndef->getIds("group:water", m_water_ids);
}

void TorchABM::trigger(ServerEnvironment *env, v3s16 p, MapNode n,
		u32 active_object_count, u32 active_object_count_wider)
{
	// check upper and right/left/front/behind nodes
	const std::vector<v3s16> pos_to_check = {
		v3s16(p.X, p.Y + 1, p.Z),
		v3s16(p.X, p.Y, p.Z + 1),
		v3s16(p.X + 1, p.Y, p.Z),
		v3s16(p.X - 1, p.Y, p.Z),
		v3s16(p.X, p.Y, p.Z - 1)
	};

	for (const auto &pos: pos_to_check) {
		MapNode node = env->getMap().getNode(pos);
		if (std::find(m_water_ids.begin(), m_water_ids.end(), node.getContent()) != m_water_ids.end()) {
			ItemStack item;
			item.deSerialize("default:torch");
			env->spawnItemActiveObject("default:torch", v3f(p.X * BS, p.Y * BS, p.Z * BS), item);
			env->removeNode(p);
			return;
		}
	}
}

LavaFlowingABM::LavaFlowingABM(INodeDefManager *ndef):
	GenericABM(ndef, 1, 2, {"default:lava_flowing"}, {"group:water"})
{
	// Cache for perfs
	ndef->getId("default:stone", m_stone_id);
}

void LavaFlowingABM::trigger(ServerEnvironment *env, v3s16 p, MapNode n,
		u32 active_object_count, u32 active_object_count_wider)
{
	env->setNode(p, MapNode(m_stone_id));
	((Server*)env->getGameDef())->playSound("default_cool_lava", p, 16, 0.25);
}

LavaSourceABM::LavaSourceABM(INodeDefManager *ndef):
	GenericABM(ndef, 1, 2, {"default:lava_source"}, {"group:water"})
{
	// cache for perfs
	ndef->getId("default:obsidian", m_obsidian_id);
}

void LavaSourceABM::trigger(ServerEnvironment *env, v3s16 p, MapNode n,
		u32 active_object_count, u32 active_object_count_wider)
{
	env->setNode(p, MapNode(m_obsidian_id));
	env->getGameDef()->playSound("default_cool_lava", p, 16, 0.25);
}

CactusABM::CactusABM(INodeDefManager *ndef):
	GenericABM(ndef, 50, 20, {"default:cactus"}, {"group:sand"})
{
	// cache for perfs
	ndef->getId("air", m_air_id);
	ndef->getId("default:cactus", m_cactus_id);
}

void CactusABM::trigger(ServerEnvironment *env, v3s16 p, MapNode n,
		u32 active_object_count, u32 active_object_count_wider)
{
	if (n.param2 >= 4) {
		return;
	}

	v3s16 pos_under(p.X, p.Y - 1, p.Z);
	MapNode n_under = env->getMap().getNode(pos_under);
	INodeDefManager* ndef = env->getGameDef()->getNodeDefManager();
	const ContentFeatures &f = ndef->get(n_under);

	// No sand under, cannot grow
	if (itemgroup_get(f.groups, "sand") == 0) {
		return;
	}

	u8 height = 0;
	while (n.getContent() == m_cactus_id && height < 4) {
		height++;
		p.Y++;
		n = env->getMap().getNode(p);
	}

	// If max size or there is no air above
	if (height == 4 || n.getContent() != m_air_id) {
		return;
	}

	env->setNode(p, MapNode(m_cactus_id));
}

FieldABM::FieldABM(INodeDefManager *ndef):
	GenericABM(ndef, 15, 4, {"group:field"}, {})
{
	// cache for perfs
	ndef->getIds("group:water", m_water_ids);
}

void FieldABM::trigger(ServerEnvironment *env, v3s16 p, MapNode n,
		u32 active_object_count, u32 active_object_count_wider)
{
	INodeDefManager* ndef = env->getGameDef()->getNodeDefManager();
	const ContentFeatures &f = ndef->get(n);

	// Invalid soil, ignore
	if (f.soil.base.length() == 0 || f.soil.dry.length() == 0 || f.soil.wet.length() == 0) {
		return;
	}

	bool pos_ok = false;
	v3s16 upper_pos(p.X, p.Y + 1, p.Z);
	MapNode upper_node = env->getMap().getNode(upper_pos, &pos_ok);
	if (!pos_ok) {
		return;
	}

	const ContentFeatures &upper_f = ndef->get(upper_node);
	int upper_plant_nb = itemgroup_get(upper_f.groups, "plant");
	// If uppernode is walkable and not a plant, set base soil
	if (upper_f.walkable && upper_plant_nb == 0) {
		env->setNode(p, MapNode(ndef, f.soil.base));
		return;
	}

	int wet_level = itemgroup_get(f.groups, "wet");
	// If water near the node
	if (env->findNodeNear(p, 3, m_water_ids)) {
		// and field is not wet, make wet
		if (wet_level == 0) {
			env->setNode(p, MapNode(ndef, f.soil.wet));
		}
	}
	else {
		// If block is near a non loaded mapblock, don't handle this part
		if (!env->findNodeNear(p, 3, {CONTENT_IGNORE})) {
			// If not wet
			if (wet_level == 0) {
				// If there is no plant and no seed over the node
				// Return back to normal node
				if (upper_plant_nb == 0 && itemgroup_get(upper_f.groups, "seed") == 0) {
					env->setNode(p, MapNode(ndef, f.soil.base));
				}
			}
			// If wet but no water near the node, make it dry
			else if (wet_level == 1) {
				env->setNode(p, MapNode(ndef, f.soil.dry));
			}
		}
	}
}

TNTABM::TNTABM(INodeDefManager *ndef):
	GenericABM(ndef, 1, 1, {"tnt:tnt"}, {"fire:basic_flame", "default:lava_source", "default:lava_flowing"})
{
	// cache for perfs
	ndef->getId("tnt:tnt", m_tnt_id);
	ndef->getId("tnt:gunpowder", m_gunpowder_id);
	ndef->getId("tnt:tnt_burning", m_tnt_burning_id);
	ndef->getId("tnt:gunpowder_burning", m_gunpowder_burning_id);
}

void TNTABM::trigger(ServerEnvironment *env, v3s16 p, MapNode n,
		u32 active_object_count, u32 active_object_count_wider)
{
	if (n.getContent() == m_tnt_id) {
		env->setNode(p, MapNode(m_tnt_burning_id));
		env->getGameDef()->playSound("tnt_ignite", p);
		env->getGameDef()->getMap().setNodeTimer(p, NodeTimer(1.0f, 0));
	}
	else if (n.getContent() == m_gunpowder_id) {
		env->getGameDef()->playSound("tnt_gunpowder_burning", p, 32 * BS, 2.0f);
		env->setNode(p, MapNode(m_gunpowder_burning_id));
		env->getGameDef()->getMap().setNodeTimer(p, NodeTimer(1.0f, 0));
	}
}

PapyrusABM::PapyrusABM(INodeDefManager *ndef):
	GenericABM(ndef, 50, 20, {"default:papyrus"}, {"default:dirt", "default:dirt_with_grass"})
{
	// cache for perfs
	ndef->getId("air", m_air_id);
	ndef->getId("default:dirt", m_dirt_id);
	ndef->getId("default:dirt_with_grass", m_dirt_with_grass_id);
	ndef->getId("default:papyrus", m_papyrus_id);
	ndef->getIds("group:water", m_water_ids);
}

void PapyrusABM::trigger(ServerEnvironment *env, v3s16 p, MapNode n,
		u32 active_object_count, u32 active_object_count_wider)
{
	v3s16 pos_under(p.X, p.Y - 1, p.Z);
	MapNode n_under = env->getMap().getNode(pos_under);

	// Papyrus should grow only on dirt & dirt with grass
	if (n_under.getContent() != m_dirt_id && n_under.getContent() != m_dirt_with_grass_id) {
		return;
	}

	// Papyrus needs water
	v3s16 p_water;
	if (!env->findNodeNear(p, 3, m_water_ids, p_water)) {
		return;
	}

	u8 height = 0;
	while (n.getContent() == m_papyrus_id && height < 4) {
		height++;
		p.Y++;
		n = env->getMap().getNode(p);
	}

	// If max size or there is no air above
	if (height == 4 || n.getContent() != m_air_id) {
		return;
	}

	env->setNode(p, MapNode(m_papyrus_id));

}

FurnaceABM::FurnaceABM(INodeDefManager *ndef):
		GenericABM(ndef, 1.0f, 1,
		{"default:furnace", "default:furnace_active"})
{
	ndef->getId("default:furnace", m_furnace_inactive_id);
	ndef->getId("default:furnace_active", m_furnace_active_id);
}

void FurnaceABM::trigger(ServerEnvironment *env, v3s16 p, MapNode n,
		u32 active_object_count, u32 active_object_count_wider)
{
	IGameDef* gdef = env->getGameDef();
	ICraftDefManager* cdef = gdef->cdef();
	IItemDefManager* idef = gdef->idef();
	NodeMetadata* meta = env->getMap().getNodeMetadataOrCreate(p, idef);
	Inventory* inv = meta->getInventory();
	if (!inv)
		return;

	bool cookable = true;
	float fuel_time = meta->getFloat("fuel_time");
	float src_time = meta->getFloat("src_time");
	float fuel_totaltime = meta->getFloat("fuel_totaltime");

	InventoryList *srclist = inv->getList("src");
	if (!srclist) {
		srclist = inv->addList("src", 1);
	}

	InventoryList *fuellist = inv->getList("fuel");
	if (!fuellist) {
		fuellist = inv->addList("fuel", 1);
	}

	InventoryList *dstlist = inv->getList("dst");
	if (!dstlist) {
		dstlist = inv->addList("dst", 4);
	}

	CraftInput cook_input, fuel_input;
	CraftOutput cook_output, fuel_output;
	std::vector<ItemStack> cook_output_replacements, fuel_output_replacements;

	cook_input.method = CRAFT_METHOD_COOKING;
	cook_input.width = srclist->getWidth();
	for (u16 i = 0; i < srclist->getSize(); i++) {
		cook_input.items.push_back(srclist->getItem(i));
	}
	cdef->getCraftResult(cook_input, cook_output, cook_output_replacements, true, gdef);

	ItemStack cook_item;
	cook_item.deSerialize(cook_output.item, idef);

	cookable = (cook_output.time != 0);

	// Check if we have enough fuel to burn
	if (fuel_time < fuel_totaltime) {
		fuel_time++;
		if (cookable) {
			src_time++;
			if (src_time >= cook_output.time && dstlist->roomForItem(cook_item)) {
				inv->addItem("dst", cook_item);
				srclist->getItem(0).remove(1);
				src_time = 0;
			}
		}
	} else if (cookable) {
		fuel_input.method = CRAFT_METHOD_FUEL;
		fuel_input.width = fuellist->getWidth() ? fuellist->getWidth() : 3;
		for (u16 i = 0; i < fuellist->getSize(); i++) {
			fuel_input.items.push_back(fuellist->getItem(i));
		}
		cdef->getCraftResult(fuel_input, fuel_output, fuel_output_replacements, true, gdef);

		if (fuel_output.time == 0) {
			fuel_totaltime = 0;
			src_time = 0;
		} else {
			fuellist->getItem(0).remove(1);
			fuel_totaltime = fuel_output.time;
		}
		fuel_time = 0;
	} else {
		fuel_totaltime = 0;
		fuel_time = 0;
		src_time = 0;
	}

	float item_percent = 0;
	std::string item_state = "";

	if (cookable) {
		item_percent = floor(src_time / cook_output.time * 100 );
		item_state = itos((u32)item_percent) + "%";
	} else {
		item_state = (srclist->isEmpty() ? "Empty" : "Not cookable");
	}

	std::stringstream ss_active_furnace_formspec;

	float fuel_percent = 0.0;
	std::string fuel_state = "Empty";
	std::string active = "Inactive";
	if (fuel_time <= fuel_totaltime && fuel_totaltime != 0) {
		active = "Active";
		fuel_percent = floor(fuel_time / fuel_totaltime * 100);
		fuel_state = itos((u32)fuel_percent) + "%";
		ss_active_furnace_formspec << "size[8,8.5]"
			<< "bgcolor[#080808BB;true]"
			<< "background[5,5;1,1;gui_formbg.png;true]"
			<< "listcolors[#00000069;#5A5A5A;#141318;#30434C;#FFF]"
			<< "list[current_name;src;2.75,0.5;1,1;]"
			<< "list[current_name;fuel;2.75,2.5;1,1;]"
			<< "image[2.75,1.5;1,1;default_furnace_fire_bg.png^[lowpart:"
			<< (100 - fuel_percent) << ":default_furnace_fire_fg.png]"
			<< "image[3.75,1.5;1,1;gui_furnace_arrow_bg.png^[lowpart:"
			<< item_percent << ":gui_furnace_arrow_fg.png^[transformR270]"
			<< "list[current_name;dst;4.75,0.96;2,2;]"
			<< "list[current_player;main;0,4.25;8,1;]"
			<< "list[current_player;main;0,5.5;8,3;8]"
			<< "listring[current_name;dst]"
			<< "listring[current_player;main]"
			<< "listring[current_name;src]"
			<< "listring[current_player;main]"
			<< "default.get_hotbar_bg(0, 4.25)";

		meta->setString("formspec", ss_active_furnace_formspec.str());
		n.setContent(m_furnace_active_id);
		env->swapNode(p, n);
	} else {
		if (!fuellist->isEmpty()) {
			fuel_state = "0%";
		}
		meta->setString("formspec", inactive_furnace_formspec);
		n.setContent(m_furnace_inactive_id);
		env->swapNode(p, n);
	}

	meta->setFloat("fuel_totaltime", fuel_totaltime);
	meta->setFloat("fuel_time", fuel_time);
	meta->setFloat("src_time", src_time);

	std::stringstream ss_infotext;
	ss_infotext <<  "Furnace " << active << " (Item: " << item_state << "; Fuel: " << fuel_state << ")";
	meta->setString("infotext", ss_infotext.str());

	env->getGameDef()->sendMetadataChanged(p);
}

}

}
