/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#include "content_abm.h"
#include "treegen.h"
#include "log.h"

namespace epixel
{

namespace abm
{

typedef std::vector<content_t> NodeIdList;

class GenericABM: public ActiveBlockModifier
{
public:
	GenericABM(INodeDefManager* ndef, float interval, u32 chance,
		const std::vector<std::string> &nodes);
	GenericABM(INodeDefManager* ndef, float interval, u32 chance,
		const std::vector<std::string> &nodes, const std::set<std::string> &neighbors);
	virtual std::vector<content_t> getTriggerContents() { return m_nodes; }
protected:
	NodeIdList m_nodes;
};

class NodeReplaceABM: public GenericABM
{
public:
	NodeReplaceABM(INodeDefManager *ndef, float interval, u32 chance,
		const std::string &nodes, const std::string &replace_with);
	virtual void trigger(ServerEnvironment *env, v3s16 p, MapNode n,
				u32 active_object_count, u32 active_object_count_wider);
protected:
	content_t m_replace_with;
};

class DirtABM: public GenericABM
{
public:
	DirtABM(INodeDefManager *ndef);
	virtual void trigger(ServerEnvironment *env, v3s16 p, MapNode n,
				u32 active_object_count, u32 active_object_count_wider);
};

class CreatureSpawningABM: public GenericABM
{
public:
	CreatureSpawningABM(INodeDefManager *ndef,
			const std::vector<std::string> &nodes, const std::string &cname,
			float interval, u32 chance, u32 min_light, u32 max_light,
			u32 activeobject_count, s32 min_height, s32 max_height);
	virtual void trigger(ServerEnvironment *env, v3s16 p, MapNode n,
				u32 active_object_count, u32 active_object_count_wider);
private:
	u32 m_min_light;
	u32 m_max_light;
	u32 m_activeobject_count;
	s32 m_min_height;
	s32 m_max_height;

	std::string m_creature_name;
};

class DirtWithGrassABM: public GenericABM
{
public:
	DirtWithGrassABM(INodeDefManager *ndef);
	virtual void trigger(ServerEnvironment *env, v3s16 p, MapNode n,
				u32 active_object_count, u32 active_object_count_wider);
};

class StoneABM: public GenericABM
{
public:
	StoneABM(INodeDefManager *ndef);
	virtual void trigger(ServerEnvironment *env, v3s16 p, MapNode n,
				u32 active_object_count, u32 active_object_count_wider);
private:
	content_t m_mossycobbleid;
};

class TorchABM: public GenericABM
{
public:
	TorchABM(INodeDefManager *ndef);
	virtual void trigger(ServerEnvironment *env, v3s16 p, MapNode n,
				u32 active_object_count, u32 active_object_count_wider);
private:
	NodeIdList m_water_ids;
};

class LavaFlowingABM: public GenericABM
{
public:
	LavaFlowingABM(INodeDefManager *ndef);
	virtual void trigger(ServerEnvironment *env, v3s16 p, MapNode n,
				u32 active_object_count, u32 active_object_count_wider);
private:
	content_t m_stone_id;
};

class LavaSourceABM: public GenericABM
{
public:
	LavaSourceABM(INodeDefManager *ndef);
	virtual void trigger(ServerEnvironment *env, v3s16 p, MapNode n,
				u32 active_object_count, u32 active_object_count_wider);
private:
	content_t m_obsidian_id;
};

class CactusABM: public GenericABM
{
public:
	CactusABM(INodeDefManager *ndef);
	virtual void trigger(ServerEnvironment *env, v3s16 p, MapNode n,
				u32 active_object_count, u32 active_object_count_wider);
private:
	content_t m_cactus_id;
	content_t m_air_id;
};

class FieldABM: public GenericABM
{
public:
	FieldABM(INodeDefManager *ndef);
	virtual void trigger(ServerEnvironment *env, v3s16 p, MapNode n,
				u32 active_object_count, u32 active_object_count_wider);
private:
	NodeIdList m_water_ids;
};

class TNTABM: public GenericABM
{
public:
	TNTABM(INodeDefManager *ndef);
	virtual void trigger(ServerEnvironment *env, v3s16 p, MapNode n,
				u32 active_object_count, u32 active_object_count_wider);
private:
	content_t m_tnt_id;
	content_t m_tnt_burning_id;
	content_t m_gunpowder_id;
	content_t m_gunpowder_burning_id;
};

class FurnaceABM: public GenericABM
{
public:
	FurnaceABM(INodeDefManager *ndef);
	virtual void trigger(ServerEnvironment *env, v3s16 p, MapNode n,
				u32 active_object_count, u32 active_object_count_wider);
private:
	content_t m_furnace_inactive_id;
	content_t m_furnace_active_id;
};

class PapyrusABM: public GenericABM
{
public:
	PapyrusABM(INodeDefManager *ndef);
	virtual void trigger(ServerEnvironment *env, v3s16 p, MapNode n,
				u32 active_object_count, u32 active_object_count_wider);
private:
	content_t m_dirt_id;
	content_t m_dirt_with_grass_id;
	content_t m_papyrus_id;
	content_t m_air_id;
	NodeIdList m_water_ids;
};

}
}

