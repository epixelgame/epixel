/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "fireabm.h"
#include "environment.h"
#include "server.h"
#include "settings.h"
#include "contrib/areas.h"

namespace epixel
{
namespace abm
{

FireGenericABM::FireGenericABM(INodeDefManager *ndef, float interval, u32 chance,
		std::vector<std::string> nodes, std::set<std::string> neighbors):
	GenericABM(ndef, interval, chance, nodes, neighbors)
{
	ndef->getIds("group:puts_out_fire", m_puts_out_fire_ids);
	ndef->getId("air", m_air_id);
}

bool FireGenericABM::shouldEstinguishFlame(v3s16 p, ServerEnvironment* env)
{
	if (!g_settings->get(BOOLSETTING_ENABLE_FIRE))
		return false;

	v3s16 p0(p.X - 2, p.Y, p.Z - 2), p1(p.X + 2, p.Y, p.Z + 2);

	return env->findIfNodeInArea(p0, p1, m_puts_out_fire_ids);
}

bool FireGenericABM::findPosForFlameAround(v3s16 p, v3s16 &pos, ServerEnvironment *env)
{
	if (!env->getGameDef()->getAreaMgr()->canInteract(intToFloat(p, 1))) {
		return false;
	}

	return env->findNodeNear(p, 1, {m_air_id}, pos);
}

FireFlammableABM::FireFlammableABM(INodeDefManager *ndef):
	FireGenericABM(ndef, 5, 2, {"group:flammable"}, {"group:igniter"})
{
	ndef->getId("fire:basic_flame", m_fire_id);
}

void FireFlammableABM::trigger(ServerEnvironment *env, v3s16 p, MapNode n,
		u32 active_object_count, u32 active_object_count_wider)
{
	if (shouldEstinguishFlame(p, env)) {
		return;
	}

	v3s16 pos;
	if (findPosForFlameAround(p, pos, env)) {
		env->setNode(pos, MapNode(m_fire_id));
	}
}

FireFarFlammableABM::FireFarFlammableABM(INodeDefManager *ndef):
	FireGenericABM(ndef, 5, 10, {"group:igniter"}, {"air"})
{
	ndef->getId("fire:basic_flame", m_fire_id);
	ndef->getIds("group:flammable", m_flammable_ids);
}

void FireFarFlammableABM::trigger(ServerEnvironment *env, v3s16 p, MapNode n,
		u32 active_object_count, u32 active_object_count_wider)
{
	INodeDefManager* ndef = env->getGameDef()->getNodeDefManager();
	const ContentFeatures &f = ndef->get(n);
	int ignite_nb = itemgroup_get(f.groups, "igniter");
	if (ignite_nb < 2) {
		return;
	}

	v3s16 pos;
	if (env->findNodeNear(p, ignite_nb, m_flammable_ids, pos)) {
		if (shouldEstinguishFlame(pos, env)) {
			return;
		}

		v3s16 pos2;
		if (findPosForFlameAround(pos, pos2, env)) {
			env->setNode(pos, MapNode(m_fire_id));
		}
	}
}

FireFlameABM::FireFlameABM(INodeDefManager *ndef):
	FireGenericABM(ndef, 3, 2, {"fire:basic_flame"}, {})
{
	ndef->getIds("group:flammable", m_flammable_ids);
}

void FireFlameABM::trigger(ServerEnvironment *env, v3s16 p, MapNode n,
		u32 active_object_count, u32 active_object_count_wider)
{
	// Check if something extinguish flame around
	if (shouldEstinguishFlame(p, env)) {
		env->removeNode(p);
		return;
	}

	if (myrand_range(1,3) == 1) {
		return;
	}

	v3s16 pos;
	// No flammable around, extinguish flame
	if (!env->findNodeNear(p, 1, m_flammable_ids, pos)) {
		env->removeNode(p);
		return;
	}

	if (myrand_range(1,4) == 1) {
		if (shouldEstinguishFlame(p, env)) {
			return;
		}

		env->removeNode(pos);
		// @TODO: call lua node update from core
	}
	else {
		env->removeNode(p);
	}
}

}
}

