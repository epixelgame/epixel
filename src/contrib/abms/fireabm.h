/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#include "contrib/abm.h"

namespace epixel
{

namespace abm
{

class FireGenericABM: public GenericABM
{
public:
	FireGenericABM(INodeDefManager* ndef, float interval, u32 chance,
		std::vector<std::string> nodes, std::set<std::string> neighbors);
protected:
	bool shouldEstinguishFlame(v3s16 p, ServerEnvironment *env);
	bool findPosForFlameAround(v3s16 p, v3s16 &pos, ServerEnvironment* env);
private:
	NodeIdList m_puts_out_fire_ids;
	content_t m_air_id;
};

class FireFlammableABM: public FireGenericABM
{
public:
	FireFlammableABM(INodeDefManager *ndef);
	virtual void trigger(ServerEnvironment *env, v3s16 p, MapNode n,
				u32 active_object_count, u32 active_object_count_wider);
private:
	content_t m_fire_id;
};

class FireFarFlammableABM: public FireGenericABM
{
public:
	FireFarFlammableABM(INodeDefManager *ndef);
	virtual void trigger(ServerEnvironment *env, v3s16 p, MapNode n,
				u32 active_object_count, u32 active_object_count_wider);
private:
	content_t m_fire_id;
	NodeIdList m_flammable_ids;
};

class FireFlameABM: public FireGenericABM
{
public:
	FireFlameABM(INodeDefManager *ndef);
	virtual void trigger(ServerEnvironment *env, v3s16 p, MapNode n,
				u32 active_object_count, u32 active_object_count_wider);
private:
	NodeIdList m_flammable_ids;
};

}

}
