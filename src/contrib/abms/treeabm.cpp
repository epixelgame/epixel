/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "treeabm.h"
#include "environment.h"
#include "filesys.h"
#include "mg_schematic.h"
#include "server.h"

namespace epixel
{

namespace abm
{

LeafDecayABM::LeafDecayABM(INodeDefManager *ndef):
	GenericABM(ndef, 2.0f, 5, {"group:leafdecay"},
	{"air", "group:liquid"})
{
	ndef->getIds("group:tree", m_tree_ids);
	// If ignore content, also keep the leaves if trunk is not loaded
	m_tree_ids.push_back(CONTENT_IGNORE);
}

void LeafDecayABM::trigger(ServerEnvironment *env, v3s16 p, MapNode n,
		u32 active_object_count, u32 active_object_count_wider)
{
	INodeDefManager* ndef = env->getGameDef()->getNodeDefManager();
	const ContentFeatures &f = ndef->get(n);

	int radius = itemgroup_get(f.groups,"leafdecay");
	// No leafdecay group or param 2 != 0, ignore
	if (radius == 0 || n.param2 != 0) {
		return;
	}

	if (!env->findNodeNear(p, radius, m_tree_ids)) {
		for (const auto &drop: f.drops.items) {
			// Test if we have chance to drop
			if (myrand_range(1,drop.rarity) != 1) {
				continue;
			}

			// If there is drops or item is not the current node name
			if (itemgroup_get(f.groups,"leafdecay_drop") > 0 ||
				drop.item.compare(f.name) != 0) {
				ItemStack item;
				item.deSerialize(drop.item);
				env->spawnItemActiveObject(drop.item, v3f(p.X * BS, p.Y * BS, p.Z * BS), item);
			}
		}
		env->removeNode(p);
	}
}

TreeFruitDecayABM::TreeFruitDecayABM(INodeDefManager *ndef):
	GenericABM(ndef, 2.0f, 5, {"moretrees:spruce_cone", "moretrees:fir_cone"},
	{"air", "group:liquid"})
{
	ndef->getIds("group:leafdecay", m_leaf_ids);
	// If ignore content, also keep the leaves if trunk is not loaded
	m_leaf_ids.push_back(CONTENT_IGNORE);
}

void TreeFruitDecayABM::trigger(ServerEnvironment *env, v3s16 p, MapNode n,
		u32 active_object_count, u32 active_object_count_wider)
{
	INodeDefManager* ndef = env->getGameDef()->getNodeDefManager();
	const ContentFeatures &f = ndef->get(n);

	if (!env->findNodeNear(p, 1, m_leaf_ids)) {
		// If there is drops or item is not the current node name
		ItemStack item;
		item.deSerialize(f.name);
		env->spawnItemActiveObject(f.name, intToFloat(p, BS), item);
		env->removeNode(p);
	}
}

#define MORETREES_DIRS2_MAX 5
static const u8 moretrees_dirs2[MORETREES_DIRS2_MAX] = { 12, 9, 18, 7, 12 };

TreeTrunkSideWaysABM::TreeTrunkSideWaysABM(INodeDefManager *ndef,
		const std::string &tree_name, const std::string &trunkstr):
	GenericABM(ndef, 1.0f, 1, {"moretrees:" + tree_name + trunkstr + "_sideways"})
{
	ndef->getId("moretrees:" + tree_name + trunkstr, m_tree_trunk_id);
}

void TreeTrunkSideWaysABM::trigger(ServerEnvironment *env, v3s16 p, MapNode n,
		u32 active_object_count, u32 active_object_count_wider)
{
	if (n.param2 > MORETREES_DIRS2_MAX - 2)
		return;

	u8 nfdir = moretrees_dirs2[n.param2 + 1];
	env->setNode(p, MapNode(m_tree_trunk_id, 0, nfdir));
}

void TreeABM::trigger(ServerEnvironment *env, v3s16 p, MapNode n,
		u32 active_object_count, u32 active_object_count_wider)
{
	Server* server = (Server*)env->getGameDef();
	INodeDefManager* ndef = server->getNodeDefManager();

	v3s16 p_top = p, p_bot = p;
	p_top.Y++; p_bot.Y--;;
	MapNode n_top = env->getMap().getNode(p_top),
		n_bot = env->getMap().getNode(p_bot);

	const std::string n_top_name = ndef->get(n_top).name;
	const std::string n_bottom_name = ndef->get(n_bot).name;

	// Check if top is okay
	if (n_top_name.compare("air") || n_top_name.compare("default:snow")) {
		bool bottomGood = false;
		// Now check if bottom is okay
		for (const auto &reqbottom: required_bottom_nodes) {
			if (n_bottom_name.compare(reqbottom) == 0) {
				bottomGood = true;
			}
		}

		if (bottomGood)
			growTree(env, p);
	}
}

void TreeABM::growTreeFromPath(ServerEnvironment *env, v3s16 pos, const std::string &mtsfile)
{
	Server* server = (Server*)env->getGameDef();
	INodeDefManager* ndef = server->getNodeDefManager();

	// If file doesn't exist error
	if (!fs::PathExists(mtsfile)) {
		logger.error("A tree would grow but '%s' doesn't exists.", mtsfile.c_str());
		return;
	}

	Schematic *schem = NULL;
	schem = SchematicManager::create(SCHEMATIC_NORMAL);

	if (!schem->loadSchematicFromFile(mtsfile, ndef)) {
		delete schem;
		return;
	}

	schem->placeStructure(env->getServerMap(), pos, 0, ROTATE_0, true);
}

void DBTreeSaplingABM::growTree(ServerEnvironment *env, v3s16 pos)
{
	Server* server = (Server*)env->getGameDef();
	INodeDefManager* ndef = server->getNodeDefManager();
	m_tree_def.fruitnode = ndef->getId(m_fruit_name);
	m_tree_def.leavesnode = ndef->getId(m_leaves_name);
	m_tree_def.trunknode = ndef->getId(m_trunk_name);
	m_tree_def.leaves2_chance = 0;

	env->removeNode(pos);
	treegen::spawn_ltree(env, pos, ndef, m_tree_def);
}

void AppleTreeABM::growTree(ServerEnvironment *env, v3s16 pos)
{
	Server* server = (Server*)env->getGameDef();
	const ModSpec *mod = server->getModSpec("default");
	std::string filename = mod->path + "/schematics/apple_tree.mts";

	// Mts position is not really correct, fix it
	pos.X -= 2;
	pos.Y -= 1;
	pos.Z -= 2;

	growTreeFromPath(env, pos, filename);
}

void AcaciaTreeABM::growTree(ServerEnvironment *env, v3s16 pos)
{
	Server* server = (Server*)env->getGameDef();
	const ModSpec *mod = server->getModSpec("default");
	std::string filename = mod->path + "/schematics/acacia_tree.mts";

	// Mts position is not really correct, fix it
	pos.X -= 4;
	pos.Y -= 1;
	pos.Z -= 4;

	growTreeFromPath(env, pos, filename);
}

void PineTreeABM::growTree(ServerEnvironment *env, v3s16 pos)
{
	Server* server = (Server*)env->getGameDef();
	const ModSpec *mod = server->getModSpec("default");
	std::string filename = mod->path + "/schematics/pine_tree.mts";

	// Mts position is not really correct, fix it
	pos.X -= 2;
	pos.Y -= 1;
	pos.Z -= 2;

	growTreeFromPath(env, pos, filename);
}

BirchTreeABM::BirchTreeABM(INodeDefManager *ndef):
	TreeABM(ndef, {"moretrees:birch_sapling", "moretrees:birch_sapling_ongen"})
{
	required_bottom_nodes = {"default:dirt_with_grass", "default:dirt", "default:dirt_with_dry_grass"};

	m_tree_def.initial_axiom = "FFFFFdddccA/FFFFFFcA/FFFFFFcB";
	m_tree_def.rules_c = "/";
	m_tree_def.rules_d = "F";
	m_tree_def.leaves2_chance = 0;
	m_tree_def.angle = 30;
	m_tree_def.iterations = 2;
	m_tree_def.iterations_random_level = 0;
	m_tree_def.trunk_type = "single";
	m_tree_def.thin_branches = true;
	m_tree_def.fruit_chance = 0;
}

void BirchTreeABM::growTree(ServerEnvironment *env, v3s16 pos)
{
	Server* server = (Server*)env->getGameDef();
	INodeDefManager* ndef = server->getNodeDefManager();

	m_tree_def.trunknode = ndef->getId("moretrees:birch_trunk");
	m_tree_def.leavesnode = ndef->getId("moretrees:birch_leaves");

	if (myrand() % 1 == 0) {
		m_tree_def.rules_a = "[&&&dddd^^ddddddd][&&&---dddd^^ddddddd][&&&+++dddd^^ddddddd][&&&++++++dddd^^ddddddd]";
		m_tree_def.rules_b = "[&&&ddd^^ddddd][&&&---ddd^^ddddd][&&&+++ddd^^ddddd][&&&++++++ddd^^ddddd]";
	}
	else {
		m_tree_def.rules_a = "[&&&dFFF^^FFFdd][&&&---dFFF^^FFFdd][&&&+++dFFF^^FFFdd][&&&++++++dFFF^^FFFdd]";
		m_tree_def.rules_b = "[&&&dFF^^FFFd][&&&---dFFF^^FFFd][&&&+++dFF^^FFFd][&&&++++++dFF^^FFFd]";
	}

	env->removeNode(pos);
	treegen::spawn_ltree(env, pos, ndef, m_tree_def);
}

SpruceTreeABM::SpruceTreeABM(INodeDefManager *ndef):
	TreeABM(ndef, {"moretrees:spruce_sapling", "moretrees:spruce_sapling_ongen"})
{
	required_bottom_nodes = {"default:dirt_with_grass", "default:dirt", "default:dirt_with_dry_grass"};

	m_tree_def.initial_axiom = "FFFFFAFFFFFFBFFFFFFCFFFFFFDFFFFFF[&&&F^^FF][&&&++F^^FF][&&&++++F^^FF][&&&++++++F^^FF][&&&--F^^FF][&&&----F^^FF][FFFFf]";
	m_tree_def.rules_b = "[&&&FFFFF^^FFF][&&&++FFFFF^^FFF][&&&++++FFFFF^^FFF][&&&++++++FFFFF^^FFF][&&&--FFFFF^^FFF][&&&----FFFFF^^FFF]";
	m_tree_def.rules_c = "[&&&FFFF^^FFF][&&&++FFFF^^FFF][&&&++++FFFF^^FFF][&&&++++++FFFF^^FFF][&&&--FFFF^^FFF][&&&----FFFF^^FFF]";
	m_tree_def.rules_d = "[&&&FFF^^FFF][&&&++FFF^^FFF][&&&++++FFF^^FFF][&&&++++++FFF^^FFF][&&&--FFF^^FFF][&&&----FFF^^FFF]";
	m_tree_def.leaves2_chance = 0;
	m_tree_def.angle = 30;
	m_tree_def.iterations = 2;
	m_tree_def.iterations_random_level = 0;
	m_tree_def.trunk_type = "crossed";
	m_tree_def.thin_branches = true;
	m_tree_def.fruit_chance = 8;
}

void SpruceTreeABM::growTree(ServerEnvironment *env, v3s16 pos)
{
	Server* server = (Server*)env->getGameDef();
	INodeDefManager* ndef = server->getNodeDefManager();

	m_tree_def.trunknode = ndef->getId("moretrees:spruce_trunk");
	m_tree_def.leavesnode = ndef->getId("moretrees:spruce_leaves");
	m_tree_def.fruitnode = ndef->getId("moretrees:spruce_cone");

	if (myrand() % 1 == 0) {
		m_tree_def.rules_a = "[&&&FFFFFF^^FFF][&&&++FFFFFF^^FFF][&&&++++FFFFFF^^FFF][&&&++++++FFFFFF^^FFF][&&&--FFFFFF^^FFF][&&&----FFFFFF^^FFF]";
	}

	env->removeNode(pos);
	treegen::spawn_ltree(env, pos, ndef, m_tree_def);
}

FirTreeABM::FirTreeABM(INodeDefManager *ndef):
	TreeABM(ndef, {"moretrees:fir_sapling", "moretrees:fir_sapling_ongen", "snow:snow"})
{
	required_bottom_nodes = {"default:dirt_with_grass", "default:dirt", "default:dirt_with_dry_grass"};

	m_tree_def.initial_axiom = "FFFAF[&&-F][&&+F][&&---F][&&+++F]Fff";
	m_tree_def.angle = 45;
	m_tree_def.iterations = 7;
	m_tree_def.iterations_random_level = 5;
	m_tree_def.trunk_type = "single";
	m_tree_def.thin_branches = true;
	m_tree_def.fruit_chance = 8;
}

void FirTreeABM::growTree(ServerEnvironment *env, v3s16 pos)
{
	Server* server = (Server*)env->getGameDef();
	INodeDefManager* ndef = server->getNodeDefManager();

	m_tree_def.trunknode = ndef->getId("moretrees:fir_trunk");
	m_tree_def.fruitnode = ndef->getId("moretrees:fir_cone");

	env->removeNode(pos);

	if (myrand() % 1 == 0) {
		m_tree_def.leavesnode = ndef->getId("moretrees:fir_leaves");
	}
	else {
		m_tree_def.leavesnode = ndef->getId("moretrees:fir_leaves_bright");
	}

	if (myrand() % 1 == 0) {
		m_tree_def.rules_a = "FF[FF][&&-FBF][&&+FBF][&&---FBF][&&+++FBF]F/A";
		m_tree_def.rules_b = "[-FBf][+FBf]";
	}
	else {
		m_tree_def.rules_a = "FF[FF][&&-FBF][&&+FBF][&&---FBF][&&+++FBF]F/A";
		m_tree_def.rules_b = "[-fB][+fB]";
	}

	MapNode n_bot = env->getMap().getNode(v3s16(pos.X, pos.Y - 1, pos.Z));
	const std::string n_bottom_name = ndef->get(n_bot).name;

	if (n_bottom_name.compare("snow:snow") == 0) {
		m_tree_def.iterations = 2;
		m_tree_def.iterations_random_level = 2;
	}
	else {
		m_tree_def.iterations = 7;
		m_tree_def.iterations_random_level = 5;
	}

	env->removeNode(pos);
	std::set<content_t> filter;
	ndef->getIds("default:leaves", filter);

	env->removeNodesInArea(pos, v3s16(pos.X, pos.Y + 5, pos.Z), filter);
	treegen::spawn_ltree(env, pos, ndef, m_tree_def);
}

JungleTreeABM::JungleTreeABM(INodeDefManager *ndef):
	TreeABM(ndef, {"default:junglesapling", "moretrees:jungletree_sapling_ongen"})
{
	required_bottom_nodes = {"default:dirt_with_grass", "default:dirt", "default:dirt_with_dry_grass"};

	m_tree_def.angle = 45;
	m_tree_def.iterations_random_level = 2;
	m_tree_def.thin_branches = true;
	m_tree_def.fruit_chance = 15;
}

void JungleTreeABM::growTree(ServerEnvironment *env, v3s16 pos)
{
	Server* server = (Server*)env->getGameDef();
	INodeDefManager* ndef = server->getNodeDefManager();

	m_tree_def.trunknode = ndef->getId("moretrees:jungletree_trunk");
	m_tree_def.leavesnode = ndef->getId("default:jungleleaves");
	m_tree_def.leaves2_chance = myrand_range(25,75);
	m_tree_def.fruitnode = ndef->getId("vines:vine");

	if (myrand() % 1 == 0) {
		m_tree_def.leaves2node = ndef->getId("moretrees:jungletree_leaves_red");
	}
	else {
		m_tree_def.leaves2node = ndef->getId("moretrees:jungletree_leaves_yellow");
	}

	switch (myrand_range(1,3)) {
	case 1:
		m_tree_def.trunk_type = "single";
		m_tree_def.iterations = 2;
		m_tree_def.initial_axiom = "FFFA";
		m_tree_def.rules_a = "FFF[&&-FBf[&&&Ff]^^^Ff][&&+FBFf[&&&FFf]^^^Ff][&&---FBFf[&&&Ff]^^^Ff][&&+++FBFf[&&&Ff]^^^Ff]F/A";
		m_tree_def.rules_b = "[-Ff&f][+Ff&f]B";
		break;
	case 2:
		m_tree_def.trunk_type = "double";
		m_tree_def.iterations = 4;
		m_tree_def.initial_axiom = "FFFFFA";
		m_tree_def.rules_a = "FFFFF[&&-FFFBF[&&&FFff]^^^FFf][&&+FFFBFF[&&&FFff]^^^FFf][&&---FFFBFF[&&&FFff]^^^FFf][&&+++FFFBFF[&&&FFff]^^^FFf]FF/A";
		m_tree_def.rules_b = "[-FFf&ff][+FFf&ff]B";
		break;
	case 3:
		m_tree_def.trunk_type = "crossed";
		m_tree_def.iterations = 4;
		m_tree_def.initial_axiom = "FFFFFA";
		m_tree_def.rules_a = "FFFFF[&&-FFFBF[&&&FFff]^^^FFf][&&+FFFBFF[&&&FFff]^^^FFf][&&---FFFBFF[&&&FFff]^^^FFf][&&+++FFFBFF[&&&FFff]^^^FFf]FF/A";
		m_tree_def.rules_b = "[-FFf&ff][+FFf&ff]B";
		break;
	}

	env->removeNode(pos);
	std::set<content_t> filter;
	ndef->getIds("default:leaves", filter);

	env->removeNodesInArea(pos, v3s16(pos.X, pos.Y + 5, pos.Z), filter);
	treegen::spawn_ltree(env, pos, ndef, m_tree_def);
}
}
}
