/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#include "contrib/abm.h"

namespace epixel
{

namespace abm
{

/**
 * @brief The LeafDecayABM class
 *
 * This is the ABM to apply leaf decay on trees
 */
class LeafDecayABM: public GenericABM
{
public:
	LeafDecayABM(INodeDefManager *ndef);
	virtual void trigger(ServerEnvironment *env, v3s16 p, MapNode n,
				u32 active_object_count, u32 active_object_count_wider);
private:
	NodeIdList m_tree_ids;
};

class TreeFruitDecayABM: public GenericABM
{
public:
	TreeFruitDecayABM(INodeDefManager *ndef);
	virtual void trigger(ServerEnvironment *env, v3s16 p, MapNode n,
				u32 active_object_count, u32 active_object_count_wider);
private:
	NodeIdList m_leaf_ids;
};

class TreeTrunkSideWaysABM: public GenericABM
{
public:
	TreeTrunkSideWaysABM(INodeDefManager *ndef, const std::string &tree_name,
			const std::string &trunkstr = "_trunk");
	virtual void trigger(ServerEnvironment *env, v3s16 p, MapNode n,
				u32 active_object_count, u32 active_object_count_wider);
private:
	content_t m_tree_trunk_id;
};

/**
 * @brief The TreeABM class
 *
 * This is the virtual class to use to spawn trees using ABM
 */
class TreeABM: public GenericABM
{
public:
	TreeABM(INodeDefManager *ndef, const std::vector<std::string> nodes):
		GenericABM(ndef, 2.0f, 20, nodes) {}
	virtual void trigger(ServerEnvironment *env, v3s16 p, MapNode n,
				u32 active_object_count, u32 active_object_count_wider);

	virtual void growTree(ServerEnvironment* env, v3s16 pos) {}
	void growTreeFromPath(ServerEnvironment* env, v3s16 pos, const std::string& mtsfile);
protected:
	std::vector<std::string> required_bottom_nodes;
	treegen::TreeDef m_tree_def;
};

class DBTreeSaplingABM: public TreeABM
{
public:
	DBTreeSaplingABM(INodeDefManager *ndef, const std::vector<std::string> nodes,
		float interval, u32 chance):
		TreeABM(ndef, nodes)
	{
		m_trigger_interval = interval;
		m_chance = chance;
		// @TODO set this into database
		required_bottom_nodes = {"default:dirt_with_grass", "default:dirt", "default:dirt_with_dry_grass"};
	}
	virtual void growTree(ServerEnvironment *env, v3s16 pos);
	treegen::TreeDef* def() { return &m_tree_def; }
	void setTrunkLeavesFruit(const std::string &t, const std::string &l, const std::string &f)
	{
		m_trunk_name = t;
		m_leaves_name = l;
		m_fruit_name = f;
	}
private:
	std::string m_trunk_name;
	std::string m_leaves_name;
	std::string m_fruit_name;
};

class AppleTreeABM: public TreeABM
{
public:
	AppleTreeABM(INodeDefManager *ndef): TreeABM(ndef, {"default:sapling"})
	{
		m_trigger_interval = 10.0f;
		m_chance = 50;
		required_bottom_nodes = {"default:dirt_with_grass", "default:dirt", "default:dirt_with_dry_grass"};
	}
	virtual void growTree(ServerEnvironment* env, v3s16 pos);
};

class AcaciaTreeABM: public TreeABM
{
public:
	AcaciaTreeABM(INodeDefManager *ndef): TreeABM(ndef, {"default:acacia_sapling"})
	{
		m_trigger_interval = 10.0f;
		m_chance = 50;
		required_bottom_nodes = {"default:dirt_with_grass", "default:dirt", "default:dirt_with_dry_grass"};
	}
	virtual void growTree(ServerEnvironment* env, v3s16 pos);
};

class PineTreeABM: public TreeABM
{
public:
	PineTreeABM(INodeDefManager *ndef): TreeABM(ndef, {"default:pine_sapling"})
	{
		m_trigger_interval = 10.0f;
		m_chance = 50;
		required_bottom_nodes = {"default:dirt_with_grass", "default:dirt", "default:dirt_with_dry_grass"};
	}
	virtual void growTree(ServerEnvironment* env, v3s16 pos);
};

class BirchTreeABM: public TreeABM
{
public:
	BirchTreeABM(INodeDefManager *ndef);
	virtual void growTree(ServerEnvironment* env, v3s16 pos);
};

class SpruceTreeABM: public TreeABM
{
public:
	SpruceTreeABM(INodeDefManager *ndef);
	virtual void growTree(ServerEnvironment* env, v3s16 pos);
};

class FirTreeABM: public TreeABM
{
public:
	FirTreeABM(INodeDefManager *ndef);
	virtual void growTree(ServerEnvironment* env, v3s16 pos);
};

class JungleTreeABM: public TreeABM
{
public:
	JungleTreeABM(INodeDefManager *ndef);
	virtual void growTree(ServerEnvironment* env, v3s16 pos);
};

}

}

