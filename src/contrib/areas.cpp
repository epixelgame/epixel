/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "areas.h"
#include "player.h"
#include "server.h"
#include "content_sao.h"
#include "database.h"
#include "settings.h"
#include "contrib/chathandler.h"
#include "contrib/playersao.h"
#include "contrib/utils.h"

namespace epixel {

std::string Area::toString() const
{
	std::ostringstream s;
	s << name << " [" << id << "]: " << owner_string << "[("  << pos1.X
			<< "," << pos1.Y << "," << pos1.Z << "),("
			<< pos2.X << "," << pos2.Y << "," << pos2.Z
			<< ")]";

	if (pvpflag != AREAPVP_UNSET) {
		s << ((pvpflag == AREAPVP_DISABLE) ? " pvp disabled (forced)" : " pvp enabled (forced)");
	}

	return s.str();
}

void AreaMgr::regenCachedStrings(Area* area)
{
	std::vector<std::string> v;
	m_server->getAuthDatabase()->getUserListByIds(area->owners, v);
	area->owner_string = epixel::join(v.begin(), v.end(), std::string(", "));

	std::stringstream s;
	s << std::endl << area->name << " [" << area->id << "] (" << area->owner_string;
	if (area->open) {
		s << ":open";
	}
	s << ")";

	area->hud_string = s.str();
}

/**
 * @brief AreaMgr::AreaMgr
 * @param server
 * @param world_path
 * @param database
 */
AreaMgr::AreaMgr(Server* server, const std::string &world_path, GameDatabase *database):
		m_server(server), m_game_database(database), m_setting_selfprotect(true),
		max_area_number_per_user(48)
{
	m_world_path = world_path;
}

void AreaMgr::Load()
{
	logger.notice("Loading areas...");

	m_game_database->loadAreas(m_areas);
	// Cache strings
	for (auto &a: m_areas) {
		regenCachedStrings(a.second);
	}

	logger.notice("%d areas loaded.", m_areas.size());
}

AreaMgr::~AreaMgr()
{
	for(const auto &it: m_areas) {
		delete it.second;
	}
}

/**
 * @brief AreaMgr::getArea
 * @param id
 * @return area Pointer
 */
Area* AreaMgr::getArea(const u32 id) const
{
	// Check if area exists
	AreaMap::const_iterator area_it = m_areas.find(id);
	if (area_it == m_areas.end()) {
		return nullptr;
	}

	return area_it->second;
}

/**
 * @brief AreaMgr::addArea
 * @param owner
 * @param name
 * @param pos1
 * @param pos2
 * @param parent_id
 * @param open
 * @param flag
 * @return areaId
 */
u32 AreaMgr::addArea(const u32 owner, const std::string &name, const v3f &pos1, const v3f &pos2,
					 u32 parent_id, bool open, AreaPvpFlag flag)
{
	u32 id = findFirstUnusedId();

	if (id == 0) {
		return 0;
	}

	std::vector<v3f> pos_s = sortPositions(pos1, pos2);

	m_areas[id] = new Area({owner}, name, pos_s[0], pos_s[1], parent_id, open, flag);
	m_areas[id]->id = id;
	m_areas[id]->init();
	regenCachedStrings(m_areas[id]);
	m_game_database->saveArea(m_areas[id]);

	return id;
}

void Area::genAreaLinesAndCorners()
{
	areabox_lines.push_back(core::line3d<f32>(pos1.X, pos1.Y, pos1.Z, pos1.X, pos1.Y, pos2.Z));
	areabox_lines.push_back(core::line3d<f32>(pos1.X, pos1.Y, pos1.Z, pos1.X, pos2.Y, pos1.Z));
	areabox_lines.push_back(core::line3d<f32>(pos1.X, pos1.Y, pos1.Z, pos2.X, pos1.Y, pos1.Z));

	areabox_lines.push_back(core::line3d<f32>(pos2.X, pos2.Y, pos2.Z, pos2.X, pos2.Y, pos1.Z));
	areabox_lines.push_back(core::line3d<f32>(pos2.X, pos2.Y, pos2.Z, pos2.X, pos1.Y, pos2.Z));
	areabox_lines.push_back(core::line3d<f32>(pos2.X, pos2.Y, pos2.Z, pos1.X, pos2.Y, pos2.Z));

	areabox_lines.push_back(core::line3d<f32>(pos1.X,pos2.Y, pos1.Z, pos1.X, pos2.Y, pos2.Z));
	areabox_lines.push_back(core::line3d<f32>(pos2.X,pos1.Y, pos2.Z, pos2.X, pos1.Y, pos1.Z));

	areabox_lines.push_back(core::line3d<f32>(pos1.X,pos2.Y, pos1.Z, pos2.X, pos2.Y, pos1.Z));
	areabox_lines.push_back(core::line3d<f32>(pos2.X,pos1.Y, pos2.Z, pos1.X, pos1.Y, pos2.Z));

	areabox_lines.push_back(core::line3d<f32>(pos1.X,pos1.Y, pos2.Z, pos1.X, pos2.Y, pos2.Z));
	areabox_lines.push_back(core::line3d<f32>(pos2.X,pos2.Y, pos1.Z, pos2.X, pos1.Y, pos1.Z));

	corners = { pos1, pos2,
			v3f(pos2.X, pos1.Y, pos1.Z),
			v3f(pos1.X, pos2.Y, pos1.Z),
			v3f(pos1.X, pos1.Y, pos2.Z),
			v3f(pos2.X, pos2.Y, pos1.Z),
			v3f(pos1.X, pos2.Y, pos2.Z),
			v3f(pos2.X, pos1.Y, pos2.Z)
	};
}

/**
 * @brief AreaMgr::renameArea
 * @param id
 * @param name
 */
void AreaMgr::renameArea(u32 id, const std::string &name)
{
	// Check if area exists
	AreaMap::iterator area_it = m_areas.find(id);
	if (area_it == m_areas.end()) {
		return;
	}

	area_it->second->name = name;
	regenCachedStrings(area_it->second);
	m_game_database->saveArea(m_areas[id]);
}

/**
 * @brief AreaMgr::changeAreaPvp
 * @param id
 * @param flag
 */
void AreaMgr::changeAreaPvp(u32 id, AreaPvpFlag flag)
{
	// Check if area exists
	AreaMap::iterator area_it = m_areas.find(id);
	if (area_it == m_areas.end()) {
		return;
	}

	area_it->second->pvpflag = flag;
	regenCachedStrings(area_it->second);
	m_game_database->saveArea(m_areas[id]);
}

/**
 * @brief AreaMgr::changeOwner
 * @param id
 * @param oldOwner
 * @param newOwner
 */
void AreaMgr::changeOwner(const u32 area_id, const u32 player_id, const u32 oldowner_id, const u32 newowner_id)
{
	// Check if area exists
	AreaMap::iterator area_it = m_areas.find(area_id);
	if (area_it == m_areas.end()) {
		return;
	}

	if (!isAreaOwner(area_id, player_id)) {
		return;
	}

	// Remove old owner id
	vremove<u32>(area_it->second->owners, oldowner_id);

	// Add player to owner if not currently owner
	if (!area_it->second->isOwner(newowner_id)) {
		area_it->second->owners.push_back(newowner_id);
	}

	regenCachedStrings(area_it->second);
	m_game_database->saveArea(m_areas[area_id]);
}

const AreaOwnerModRv AreaMgr::addOwner(const u32 area_id, const u32 owner_id, const u32 add_owner_id)
{
	// Check if area exists
	AreaMap::iterator area_it = m_areas.find(area_id);
	if (area_it == m_areas.end()) {
		return AREAOWNER_AREA_NOT_FOUND;
	}

	if (!isAreaOwner(area_id, owner_id)) {
		return AREAOWNER_NOT_OWNER;
	}

	AreaOwnerModRv rv = AREAOWNER_EXISTS;

	// If not owner, add it
	if (!area_it->second->isOwner(add_owner_id)) {
		area_it->second->owners.push_back(add_owner_id);
		rv = AREAOWNER_OK;
	}

	regenCachedStrings(area_it->second);
	m_game_database->saveArea(m_areas[area_id]);

	return rv;
}
const AreaOwnerModRv AreaMgr::removeOwner(const u32 area_id, const u32 owner_id, const u32 rm_owner_id)
{
	// Check if area exists
	AreaMap::iterator area_it = m_areas.find(area_id);
	if (area_it == m_areas.end()) {
		return AREAOWNER_AREA_NOT_FOUND;
	}

	if (!isAreaOwner(area_id, owner_id)) {
		return AREAOWNER_NOT_OWNER;
	}

	// Cannot remove the last owner
	if (area_it->second->owners.size() <= 1) {
		return AREAOWNER_LAST_OWNER;
	}

	// If is owner, remove it
	if (area_it->second->isOwner(rm_owner_id)) {
		// Remove owner from vector
		vremove<u32>(area_it->second->owners, rm_owner_id);

		regenCachedStrings(area_it->second);
		m_game_database->saveArea(m_areas[area_id]);
		return AREAOWNER_OK;
	}

	return AREAOWNER_NOT_EXISTS;
}

/**
 * @brief AreaMgr::changeOpen
 * @param id
 * @param owner
 * @param open
 * @return
 */
bool AreaMgr::changeOpen(u32 id, const u32 player_id, bool open)
{
	// Check if area exists
	AreaMap::iterator area_it = m_areas.find(id);
	if (area_it == m_areas.end()) {
		return false;
	}

	if (!isAreaOwner(id, player_id)) {
		return area_it->second->open;
	}

	area_it->second->open = open;
	regenCachedStrings(area_it->second);

	m_game_database->saveArea(m_areas[id]);

	return area_it->second->open;
}

/**
 * @brief AreaMgr::removeArea
 * @param id
 * @param recurse
 */
void AreaMgr::removeArea(u32 id, bool recurse)
{
	// Check if area exists
	AreaMap::iterator area_it = m_areas.find(id);
	if (area_it == m_areas.end()) {
		return;
	}

	if (recurse) {
		std::vector<u32> childs;
		getChildren(id, childs);
		for (const u32 childId: childs) {
			removeArea(childId, true);
		}
	}
	else {
		u32 parent_id = area_it->second->parent_id;
		std::vector<u32> childs;
		getChildren(id, childs);

		for (const u32 childId: childs) {
			if (m_areas[childId] != nullptr) {
				m_areas[childId]->parent_id = parent_id;
			}
		}
	}

	m_areas.erase(area_it);

	m_game_database->deleteArea(id);
}

/**
 * @brief AreaMgr::moveArea
 * @param id
 * @param pos1
 * @param pos2
 * @return error message if needed
 */
std::string AreaMgr::moveArea(u32 id, const v3f &pos1, const v3f &pos2)
{
	// Check if area exists
	AreaMap::iterator area_it = m_areas.find(id);
	if (area_it == m_areas.end()) {
		return "Area doesn't exist";
	}

	area_it->second->pos1 = pos1;
	area_it->second->pos2 = pos2;

	regenCachedStrings(area_it->second);
	m_game_database->saveArea(m_areas[id]);

	return "";
}

/**
 * @brief AreaMgr::getChildren
 * @param parent_id
 * @param childs
 */
void AreaMgr::getChildren(u32 parent_id, std::vector<u32> &childs)
{
	for (const auto &area_it: m_areas) {
		if (area_it.second->parent_id == parent_id) {
			childs.push_back(area_it.first);
		}
	}
}

u32 AreaMgr::findFirstUnusedId()
{
	for (u32 i = 1; i < MAX_AREA_ID; i++) {
		if (m_areas.find(i) == m_areas.end()) {
			return i;
		}
	}

	return 0;
}

/**
 * @brief AreaMgr::isSubArea
 * @param pos1
 * @param pos2
 * @param id
 * @return true if area is into pos1 x pos2
 */
bool AreaMgr::isSubArea(const v3f &pos1, const v3f &pos2, u32 id) const
{
	// Check if area exists
	AreaMap::const_iterator area_it = m_areas.find(id);
	if (area_it == m_areas.end()) {
		return false;
	}

	v3f id_p1 = area_it->second->pos1;
	v3f id_p2 = area_it->second->pos2;
	return (pos1.X >= id_p1.X && pos1.X <= id_p2.X) &&
		   (pos2.X >= id_p1.X && pos2.X <= id_p2.X) &&
		   (pos1.Y >= id_p1.Y && pos1.Y <= id_p2.Y) &&
		   (pos2.Y >= id_p1.Y && pos2.Y <= id_p2.Y) &&
		   (pos1.Z >= id_p1.Z && pos1.Z <= id_p2.Z) &&
		   (pos2.Z >= id_p1.Z && pos2.Z <= id_p2.Z);

}

/**
 * @brief AreaMgr::isAreaOwner
 * @param id
 * @param name
 * @return true is name is owner of the area
 */
bool AreaMgr::isAreaOwner(u32 id, const u32 player_id) const
{
	// Check if area exists
	AreaMap::const_iterator area_it = m_areas.find(id);
	if (area_it == m_areas.end()) {
		return false;
	}

	// Check if player has areas priv
	if (m_server->checkPrivByDBId(player_id, "areas")) {
		return true;
	}

	while (area_it != m_areas.end()) {
		if (area_it->second->isOwner(player_id)) {
			return true;
		}
		// Check parent areas
		else if (area_it->second->parent_id != 0) {
			area_it = m_areas.find(area_it->second->parent_id);
		}
		else {
			return false;
		}
	}

	return false;
}

/**
 * @brief AreaMgr::canInteract
 * @param pos
 * @param name
 * @return true is name can interact in this area
 */
bool AreaMgr::canInteract(const v3f &pos, const u32 player_id) const
{
	// Check if player has areas priv
	if (player_id > 0 && m_server->checkPrivByDBId(player_id, "areas")) {
		return true;
	}

	bool owned = false;
	AreaMap posAreas;
	getAreasAtPos(pos, posAreas);
	for (const auto &it: posAreas) {
		if (it.second->isOwner(player_id) || it.second->open) {
			return true;
		}
		else {
			owned = true;
		}
	}

	return (!owned);
}

/**
 * @brief AreaMgr::canDoPvp
 * @param pos
 * @return true is pvp is enabled in this area
 */
bool AreaMgr::canDoPvp(const v3f &pos) const
{
	AreaMap posAreas;
	getAreasAtPos(pos, posAreas);

	for (const auto &it: posAreas) {
		// If global setting is PvP enable, peace must be the winner
		if (g_settings->get(BOOLSETTING_ENABLE_PVP)) {
			if (it.second->pvpflag == AREAPVP_DISABLE) {
				return false;
			}
		}
		// Else if PvP is disabled, war must be the winner
		else if (it.second->pvpflag == AREAPVP_ENABLE) {
			return true;
		}
	}

	// No area found, return enable_pvp setting value
	return g_settings->get(BOOLSETTING_ENABLE_PVP);
}

/**
 * @brief AreaMgr::canInteractInArea
 * @param pos1
 * @param pos2
 * @param name
 * @param area
 * @param areaId
 * @return true if name can interact in the area
 */
bool AreaMgr::canInteractInArea(const v3f &pos1, const v3f &pos2, const u32 player_id, Area *&area, u32 &areaId) const
{
	// Check if player has areas priv
	if (player_id > 0 && m_server->checkPrivByDBId(player_id, "areas")) {
		return true;
	}

	std::vector<v3f> psnew = { pos1, pos2,
			v3f(pos2.X, pos1.Y, pos1.Z),
			v3f(pos1.X, pos2.Y, pos1.Z),
			v3f(pos1.X, pos1.Y, pos2.Z),
			v3f(pos2.X, pos2.Y, pos1.Z),
			v3f(pos1.X, pos2.Y, pos2.Z),
			v3f(pos2.X, pos1.Y, pos2.Z)
	};

	core::aabbox3d<f32> newAreaCollisionBox(pos1,pos2);

	for (const auto &it: m_areas) {
		// First check for a fully enclosing owned area.
		if (player_id && it.second->isOwner(player_id) && isSubArea(pos1, pos2, it.first)) {
			return true;
		}

		// Then check for intersecting (non-owned) areas.
		v3f p1 = it.second->pos1;
		v3f p2 = it.second->pos2;

		core::aabbox3d<f32> areaCollisionBox(p1,p2);

		// If the checked area is not opened, we are not owner
		if (!it.second->open && !isAreaOwner(it.first, player_id)) {
			// Check if no corner is inside an existing area
			for (const auto &p: psnew) {
				if (areaCollisionBox.isPointInside(p)) {
					areaId = it.first;
					area = it.second;
					return false;
				}
			}

			// Check if no area corner in new area
			for (const auto &p: it.second->corners) {
				if (newAreaCollisionBox.isPointInside(p)) {
					areaId = it.first;
					area = it.second;
					return false;
				}
			}

			// And the two collision boxes collide
			if (newAreaCollisionBox.intersectsWithBox(areaCollisionBox)) {
				areaId = it.first;
				area = it.second;
				return false;
			}

			// And the line collide
			for (const auto &line: it.second->areabox_lines) {
				if (newAreaCollisionBox.intersectsWithLine(line)) {
					areaId = it.first;
					area = it.second;
					return false;
				}
			}
		}
	}
	return true;
}

/**
 * @brief AreaMgr::canPlayerAddArea
 * @param pos1
 * @param pos2
 * @param name
 * @param errorMsg
 * @return
 */
bool AreaMgr::canPlayerAddArea(const v3f &pos1, const v3f &pos2, const u32 player_id, std::string &errorMsg)
{
	// Check if player has areas priv
	if (m_server->checkPrivByDBId(player_id, "areas")) {
		return true;
	}

	// Check self protection privileges, if it's enabled,
	// And the area is too big
	if (!m_setting_selfprotect && !m_server->checkPrivByDBId(player_id, "interact")) {
		errorMsg = "Self protection is disabled or you do not have the necessary privilege.";
		return false;
	}

	if (pos2.X - pos1.X > MAX_AREA_AUTOPROTECT_X ||
			pos2.Y - pos1.Y > MAX_AREA_AUTOPROTECT_Y ||
			pos2.Z - pos1.Z > MAX_AREA_AUTOPROTECT_Z) {
		errorMsg = "Area is too big.";
		return false;
	}

	u32 userAreaCount = 0;
	for(AreaMap::iterator it = m_areas.begin(); it != m_areas.end(); it++) {
		if (it->second->isOwner(player_id)) {
			userAreaCount++;
		}
	}

	if (userAreaCount >= max_area_number_per_user) {
		errorMsg = "You have reached the maximum amount of areas that you are allowed to  protect.";
		return false;
	}

	Area* area = nullptr;
	u32 areaId = 0;
	if (!canInteractInArea(pos1, pos2, player_id, area, areaId)) {
		std::ostringstream s;
		s << "The area intersects with " << area->name << "[" << areaId << "]";
		errorMsg = s.str();
		return false;
	}

	return true;
}

/**
 * @brief AreaMgr::getNodeOwners
 * @param pos
 * @return
 */
AreaOwnerList AreaMgr::getNodeOwners(const v3f &pos) const
{
	AreaOwnerList ownerList;
	AreaMap posAreas;
	getAreasAtPos(pos, posAreas);
	for(AreaMap::iterator it = posAreas.begin(); it != posAreas.end(); it++) {
		ownerList.push_back(it->second->owner_string);
	}

	return ownerList;
}

/**
 * @brief AreaMgr::sortPositions
 * @param pos1
 * @param pos2
 * @return (pos1, pos2) ordered
 */
std::vector<v3f> AreaMgr::sortPositions(v3f pos1, v3f pos2)
{
	std::vector<v3f> ps;
	ps.push_back(v3f(0,0,0));
	ps.push_back(v3f(0,0,0));

	if (pos1.X > pos2.X) {
		ps[0].X = pos2.X;
		ps[1].X = pos1.X;
	}
	else {
		ps[0].X = pos1.X;
		ps[1].X = pos2.X;
	}

	if (pos1.Y > pos2.Y) {
		ps[0].Y = pos2.Y;
		ps[1].Y = pos1.Y;
	}
	else {
		ps[0].Y = pos1.Y;
		ps[1].Y = pos2.Y;
	}

	if (pos1.Z > pos2.Z) {
		ps[0].Z = pos2.Z;
		ps[1].Z = pos1.Z;
	}
	else {
		ps[0].Z = pos1.Z;
		ps[1].Z = pos2.Z;
	}

	return ps;
}
/**
 * @brief AreaMgr::listAreas
 * @param peer_id
 * @return a list of all areas
 */
std::string AreaMgr::listAreas(u16 peer_id)
{
	std::ostringstream s;
	bool isAdmin = false;

	// Check if player has areas priv
	RemotePlayer* player = m_server->getEnv().getPlayer(peer_id);
	if (!player) {
		return "";
	}

	if (m_server->checkPriv(peer_id, "areas") || (peer_id == PEER_ID_SERVER)) {
		isAdmin = true;
	}

	for (auto area_it: m_areas) {
		if (!isAdmin && !isAreaOwner(area_it.first, player->getDBId())) {
			continue;
		}

		std::vector<u32> childs;
		getChildren(area_it.first, childs);

		s << area_it.second->toString();

		if (!childs.empty()) {
			s << " -> ";
		}

		for (std::vector<u32>::iterator it = childs.begin(); it != childs.end();) {
			s << (*it);

			it++;
			if (it != childs.end()) {
				s << ",";
			}
		}
		s << std::endl;
	}

	if (s.str().size() == 0) {
		return "No area found";
	}

	return s.str();
}

/**
 * @brief AreaMgr::updatePlayerHud
 * @param player
 */
void AreaMgr::updatePlayerHud(RemotePlayer* player)
{
	// Sometimes getHuds can be nil, then ignore update
	if (!player->getHuds()) {
		return;
	}

	AreaMap areas;

	getAreasAtPos(player->getPosition() / BS, areas);

	std::string areaText = "Areas: ";

	bool pvp_enabled = g_settings->get(BOOLSETTING_ENABLE_PVP);
	for (const auto &area: areas) {
		if (g_settings->get(BOOLSETTING_ENABLE_PVP)) {
			if (area.second->pvpflag == AREAPVP_DISABLE) {
				pvp_enabled = false;
			}
		}
		else if (area.second->pvpflag == AREAPVP_ENABLE) {
			pvp_enabled = true;
		}
		areaText += area.second->toStringHud();
	}

	if (pvp_enabled) {
		areaText = "PvP enabled\n" + areaText;
	}

	void* value = &areaText;

	m_server->hudChange(player, player->getHuds()->getHudId(PLAYERHUD_AREAS),
			HUD_STAT_TEXT, value);
}

/**
 * @brief AreaMgr::getAreaIdByPos1Pos2
 * @param pos1
 * @param pos2
 * @return
 */
u32 AreaMgr::getAreaIdByPos1Pos2(const v3f &pos1, const v3f &pos2)
{
	for(AreaMap::iterator area_it = m_areas.begin(); area_it != m_areas.end(); area_it++) {
		Area* area = area_it->second;
		if(((area->pos1.X == pos1.X && area->pos1.Z == pos1.Z)
				|| (area->pos1.X == pos1.X && area->pos1.Z == pos2.Z)
				|| (area->pos1.X == pos2.X && area->pos1.Z == pos1.Z)
				|| (area->pos1.X == pos2.X && area->pos1.Z == pos2.Z))
			   &&
			   ((area->pos2.X == pos1.X && area->pos2.Z == pos1.Z )
				|| (area->pos2.X == pos1.X && area->pos2.Z == pos2.Z )
				|| (area->pos2.X == pos2.X && area->pos2.Z == pos1.Z )
				|| (area->pos2.X == pos2.X && area->pos2.Z == pos2.Z ))) {

			// at least pos1 needs to have a hight value that fits in
			if((area->pos1.Y <= pos1.Y && area->pos2.Y >= pos1.Y)
					|| (area->pos1.Y >= pos1.Y && area->pos2.Y <= pos1.Y)) {
				return area_it->first;
			}
		}
	}
	return 0;
}
}

namespace epixel
{

bool ChatHandler::handleCommand_area_list(const std::string &args, const u16 peer_id)
{
	std::wstringstream ws;
	ws << m_server->getAreaMgr()->listAreas(peer_id).c_str() << std::endl;
	m_server->SendChatMessage(peer_id, ws.str());
	return true;
}

bool ChatHandler::handleCommand_area_remove(const std::string &args, const u16 peer_id)
{
	std::wstringstream ws;

	u32 areaId = (u32) atoi(args.c_str());
	if (areaId == 0) {
		ws << "Invalid argument. AreaId should be an integer.";
		m_server->SendChatMessage(peer_id, ws.str());
		return true;
	}

	PlayerSAO* sao = m_server->getPlayerSAO(peer_id);
	if (!sao) {
		return true;
	}

	std::string name = m_server->getPlayerName(peer_id);

	// Remove area only if owner (or admin)
	if ((!m_server->getAreaMgr()->isAreaOwner(areaId, sao->getPlayer()->getDBId())) &&
		(peer_id != PEER_ID_SERVER)) {
		ws << "Area " << areaId << " does not exist or is not owned by you";
	}
	else {
		m_server->getAreaMgr()->removeArea(areaId, false);
		ws << "Area " << areaId << " removed.";
		logger.notice("%s remove area %d", name.c_str(), areaId);
	}
	m_server->SendChatMessage(peer_id, ws.str());
	return true;
}

bool ChatHandler::handleCommand_area_rename(const std::string &args, const u16 peer_id)
{
	std::wstringstream ws;
	std::vector<std::string> commandline;
	std::stringstream ss(args);
	std::string item;
	while (std::getline(ss, item, ' ')) {
		commandline.push_back(item);
	}

	if (commandline.size() < 2) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help area rename");
		return true;
	}

	PlayerSAO* sao = m_server->getPlayerSAO(peer_id);
	if (!sao) {
		return true;
	}

	u32 areaId = (u32) atoi(commandline[0].c_str());
	if (areaId == 0) {
		ws << "Invalid argument. AreaId should be an integer.";
		m_server->SendChatMessage(peer_id, ws.str());
		return true;
	}

	std::string name = m_server->getPlayerName(peer_id);

	// Remove area only if owner (or admin)
	if ((!m_server->getAreaMgr()->isAreaOwner(areaId, sao->getPlayer()->getDBId())) &&
		(peer_id != PEER_ID_SERVER)) {
		ws << "Area " << areaId << " does not exist or is not owned by you";
	}
	else {
		std::string areaName = epixel::join(commandline.begin() + 1, commandline.end(), std::string(" "));
		m_server->getAreaMgr()->renameArea(areaId, areaName);
		ws << "Area " << areaId << " renamed to '" << areaName.c_str();
		logger.notice("%s rename area ", name.c_str(), areaId);
	}
	m_server->SendChatMessage(peer_id, ws.str());
	return true;
}

bool ChatHandler::handleCommand_area_pvp(const std::string &args, const u16 peer_id)
{
	std::wstringstream ws;
	std::vector<std::string> commandline;
	std::stringstream ss(args);
	std::string item;
	while (std::getline(ss, item, ' ')) {
		commandline.push_back(item);
	}

	if (commandline.size() < 2) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help area pvp");
		return true;
	}

	PlayerSAO* sao = m_server->getPlayerSAO(peer_id);
	if (!sao) {
		return true;
	}

	u32 areaId = (u32) atoi(commandline[0].c_str());
	if (areaId == 0) {
		ws << "Invalid argument. AreaId should be an integer.";
		m_server->SendChatMessage(peer_id, ws.str());
	}

	std::string name = m_server->getPlayerName(peer_id);

	// Modify area only if owner (or admin)
	if ((!m_server->getAreaMgr()->isAreaOwner(areaId, sao->getPlayer()->getDBId())) &&
		(peer_id != PEER_ID_SERVER)) {
		ws << "Area " << areaId << " does not exist or is not owned by you";
	}
	else if (g_settings->get(BOOLSETTING_ENABLE_PVP_PLAYER_CAN_PER_AREA)) {
		epixel::AreaPvpFlag flag = epixel::AREAPVP_UNSET;
		if (commandline[1].compare("enable") == 0) {
			flag = epixel::AREAPVP_ENABLE;
		}
		else if (commandline[1].compare("disable") == 0) {
			flag = epixel::AREAPVP_DISABLE;
		}
		else if (commandline[1].compare("default") != 0) {
			ws << "Invalid area PvP flag " << commandline[1].c_str();
			m_server->SendChatMessage(peer_id, ws.str());
			return true;
		}
		m_server->getAreaMgr()->changeAreaPvp(areaId, flag);
		ws << "Change area PvP mode for " << areaId << " to " << commandline[1].c_str();
		logger.notice("%s change area PVP mode for %d to %s", name.c_str(), areaId, commandline[1].c_str());
	}
	m_server->SendChatMessage(peer_id, ws.str());
	return true;
}

bool ChatHandler::handleCommand_area_pos1(const std::string &args, const u16 peer_id)
{
	if (!args.empty()) {
		m_server->SendChatMessage(peer_id, L"Invalid usage. See /help area pos1.");
		return true;
	}

	return handleCommand_area_pos(peer_id, 0);
}

bool ChatHandler::handleCommand_area_pos2(const std::string &args, const u16 peer_id)
{
	if (!args.empty()) {
		m_server->SendChatMessage(peer_id, L"Invalid usage. See /help area pos2.");
		return true;
	}

	return handleCommand_area_pos(peer_id, 1);
}

bool ChatHandler::handleCommand_area_pos(u16 peer_id, u8 pos)
{

	PlayerSAO* sao = m_server->getPlayerSAO(peer_id);
	if (!sao) {
		return true;
	}

	sao->setAreaPos(pos, floatToInt(sao->getBasePosition(), BS));
	v3s16 ppos = sao->getAreaPos(pos);
	std::wstringstream ws;
	ws << L"Area position " << pos + 1 << L" set to (" << ppos.X << L"," << ppos.Y << L"," << ppos.Z << L")";
	m_server->SendChatMessage(peer_id, ws.str());
	return true;
}

/**
 * @brief ChatHandler::handleCommand_area_add
 * @param args
 * @param peer_id
 * @return
 */
bool ChatHandler::handleCommand_area_add(const std::string &args, const u16 peer_id)
{
	std::vector<std::string> commandline;
	std::stringstream ss(args);
	std::string item;
	while (std::getline(ss, item, ' ')) {
		commandline.push_back(item);
	}

	if (args.empty() || commandline.size() < 2) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help area set_owner");
		return true;
	}

	PlayerSAO* sao = m_server->getPlayerSAO(peer_id);
	if (!sao) {
		return true;
	}

	const std::string owner = commandline[0];
	const std::string name = commandline[1];

	if (!m_server->getAuthDatabase()->userExists(owner)) {
		std::wstringstream ws;
		ws << "Player  '" << owner.c_str()
		   << "' doesn't exist!" << std::endl;
		m_server->SendChatMessage(peer_id, ws.str());
		return true;
	}

	v3f pos1 = intToFloat(sao->getAreaPos(0), 1);
	v3f pos2 = intToFloat(sao->getAreaPos(1), 1);
	if (pos1 == v3f(0,0,0) || pos2 == v3f(0,0,0)) {
		m_server->SendChatMessage(peer_id, L"You must define the both area positions of the area.");
		return true;
	}

	u32 areaId = m_server->getAreaMgr()->addArea(sao->getPlayer()->getDBId(), name, pos1, pos2, 0, false);
	if (!areaId) {
		m_server->SendChatMessage(peer_id, L"Failed to add area, no new area can be created.");
		return true;
	}

	std::wstringstream ws;
	ws << "Area " << name.c_str() << " [" << areaId << "] attributed to the owner : " << owner.c_str() << std::endl;
	m_server->SendChatMessage(peer_id, ws.str());

	RemotePlayer* otherPlayer = m_server->getEnv().getPlayer(owner.c_str());
	if (otherPlayer && otherPlayer->getPeerID() != peer_id) {
		std::wstringstream ws2;
		ws2 << "Area " << name.c_str() << " [" << areaId << "] was attributed to you." << std::endl;
		m_server->SendChatMessage(otherPlayer->getPeerID(), ws2.str());
	}
	logger.notice("%s create new area and attribute it to %s", sao->getPlayer()->getName(), owner.c_str());
	return true;
}

/**
 * @brief ChatHandler::handleCommand_area_change_owner
 * @param args
 * @param peer_id
 * @return
 */
bool ChatHandler::handleCommand_area_change_owner(const std::string &args, const u16 peer_id)
{
	std::vector<std::string> commandline;
	std::stringstream ss(args);
	std::string item;
	while (std::getline(ss, item, ' ')) {
		commandline.push_back(item);
	}

	if (args.empty() || commandline.size() != 3) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help area change_owner");
		return true;
	}

	PlayerSAO* sao = m_server->getPlayerSAO(peer_id);
	if (!sao) {
		return true;
	}

	u32 id_area = (u32) mystoi(commandline[0]);
	if (id_area == 0) {
		m_server->SendChatMessage(peer_id, L"Invalid argument. AreaId should be an integer.");
		return true;
	}
	const std::string old_owner = commandline[1];
	const std::string new_owner = commandline[2];

	if ((!m_server->getAreaMgr()->isAreaOwner(id_area, sao->getPlayer()->getDBId())) &&
		(peer_id != PEER_ID_SERVER)) {
		std::wstringstream ws;
		ws << "Area " << id_area << " does not exist or is not owned by you";
		m_server->SendChatMessage(peer_id, ws.str());
		return true;
	}

	u32 old_userid = m_server->getAuthDatabase()->userExists(old_owner);
	if (!old_userid) {
		std::wstringstream ws;
		ws << "Player  '" << old_owner.c_str()
		   << "' doesn't exist!" << std::endl;
		m_server->SendChatMessage(peer_id, ws.str());
		return true;
	}

	u32 nuserid = m_server->getAuthDatabase()->userExists(new_owner);
	if (!nuserid) {
		std::wstringstream ws;
		ws << "Player  '" << new_owner.c_str()
		   << "' doesn't exist!" << std::endl;
		m_server->SendChatMessage(peer_id, ws.str());
		return true;
	}

	if (old_userid == nuserid) {
		m_server->SendChatMessage(peer_id, L"Old owner should be different from new owner");
		return true;
	}

	m_server->getAreaMgr()->changeOwner(id_area, sao->getPlayer()->getDBId(), old_userid, nuserid);

	std::wstringstream ws;
	ws << "The new owner of area id " << id_area
	   << " is now " << new_owner.c_str() << std::endl;
	m_server->SendChatMessage(peer_id, ws.str());
	logger.notice("%s change owner of area id %d to %s", sao->getPlayer()->getName(), id_area, new_owner.c_str());
	return true;
}

/**
 * @brief ChatHandler::handleCommand_area_add_owner
 * @param args
 * @param peer_id
 * @return
 */
bool ChatHandler::handleCommand_area_add_owner(const std::string &args, const u16 peer_id)
{
	std::vector<std::string> commandline;
	std::stringstream ss(args);
	std::string item;
	while (std::getline(ss, item, ' ')) {
		commandline.push_back(item);
	}

	if (args.empty() || commandline.size() != 2) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help area add_owner");
		return true;
	}

	PlayerSAO* sao = m_server->getPlayerSAO(peer_id);
	if (!sao) {
		return true;
	}

	u32 id_area = (u32) mystoi(commandline[0]);
	if (id_area == 0) {
		m_server->SendChatMessage(peer_id, L"Invalid argument. AreaId should be an integer.");
		return true;
	}

	const std::string new_owner = commandline[1];

	u32 nuserid = m_server->getAuthDatabase()->userExists(new_owner);
	if (!nuserid) {
		std::wstringstream ws;
		ws << "Player  '" << new_owner.c_str() << "' doesn't exist!" << std::endl;
		m_server->SendChatMessage(peer_id, ws.str());
		return true;
	}

	AreaOwnerModRv rv = m_server->getAreaMgr()->addOwner(id_area, sao->getPlayer()->getDBId(), nuserid);
	std::wstringstream ws;
	switch(rv) {
		case AREAOWNER_AREA_NOT_FOUND:
			ws << "Area " << id_area << " does not exist.";
			break;
		case AREAOWNER_EXISTS:
			ws << "Player " << new_owner.c_str() << " already owns area " << id_area;
			break;
		case AREAOWNER_NOT_OWNER:
			ws << "Area " << id_area << " is not owned by you";
			break;
		case AREAOWNER_OK: {
			ws << "A new owner has been added to area id " << id_area << ": " << new_owner.c_str() << std::endl;
			logger.notice("%s add owner on area id %d: %s", sao->getPlayer()->getName(), id_area, new_owner.c_str());
		}
		break;
		default: assert(false);
	}

	m_server->SendChatMessage(peer_id, ws.str());
	return true;
}


/**
 * @brief ChatHandler::handleCommand_area_remove_owner
 * @param args
 * @param peer_id
 * @return
 */
bool ChatHandler::handleCommand_area_remove_owner(const std::string &args, const u16 peer_id)
{
	std::vector<std::string> commandline;
	std::stringstream ss(args);
	std::string item;
	while (std::getline(ss, item, ' ')) {
		commandline.push_back(item);
	}

	if (args.empty() || commandline.size() != 2) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help area remove_owner");
		return true;
	}

	PlayerSAO* sao = m_server->getPlayerSAO(peer_id);
	if (!sao) {
		return true;
	}

	u32 id_area = (u32) mystoi(commandline[0]);
	if (id_area == 0) {
		m_server->SendChatMessage(peer_id, L"Invalid argument. AreaId should be an integer.");
		return true;
	}

	const std::string new_owner = commandline[1];

	u32 nuserid = m_server->getAuthDatabase()->userExists(new_owner);
	if (!nuserid) {
		std::wstringstream ws;
		ws << "Player  '" << new_owner.c_str() << "' doesn't exist!" << std::endl;
		m_server->SendChatMessage(peer_id, ws.str());
		return true;
	}

	AreaOwnerModRv rv = m_server->getAreaMgr()->removeOwner(id_area, sao->getPlayer()->getDBId(), nuserid);
	std::wstringstream ws;
	switch(rv) {
		case AREAOWNER_AREA_NOT_FOUND:
			ws << "Area " << id_area << " does not exist.";
			break;
		case AREAOWNER_NOT_EXISTS:
			ws << "Player " << new_owner.c_str() << " doesn't own area " << id_area;
			break;
		case AREAOWNER_NOT_OWNER:
			ws << "Area " << id_area << " is not owned by you";
			break;
		case AREAOWNER_LAST_OWNER:
			ws << "Area " << id_area << " should have at least one owner. Cannot remove " << new_owner.c_str()
					<< " from owners";
			break;
		case AREAOWNER_OK:
			ws << "A new owner has been removed from area id " << id_area << ": " << new_owner.c_str() << std::endl;
			logger.notice("%s remove owner on area id %d: %s", sao->getPlayer()->getName(), id_area, new_owner.c_str());
			break;
		default: assert(false);
	}

	m_server->SendChatMessage(peer_id, ws.str());
	return true;
}

/**
 * @brief ChatHandler::handleCommand_area_open
 * @param args
 * @param peer_id
 * @return
 */
bool ChatHandler::handleCommand_area_open(const std::string &args, const u16 peer_id)
{
	std::vector<std::string> commandline;
	std::stringstream ss(args);
	std::string item;
	while (std::getline(ss, item, ' ')) {
		commandline.push_back(item);
	}

	if (args.empty() || commandline.size() > 2) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help area open");
		return true;
	}

	PlayerSAO* sao = m_server->getPlayerSAO(peer_id);
	if (!sao) {
		return true;
	}

	u32 id_area = (u32) mystoi(commandline[0]);
	if (id_area == 0) {
		m_server->SendChatMessage(peer_id, L"Invalid argument. AreaId should be an integer.");
		return true;
	}

	if ((!m_server->getAreaMgr()->isAreaOwner(id_area, sao->getPlayer()->getDBId())) &&
		(peer_id != PEER_ID_SERVER)) {
		std::wstringstream ws;
		ws << "Area " << id_area << " does not exist or is not owned by you";
		m_server->SendChatMessage(peer_id, ws.str());
		return true;
	}

	if (commandline[1].compare("true") == 0) {
		m_server->getAreaMgr()->changeOpen(id_area, sao->getPlayer()->getDBId(), true);
		logger.notice("%s open area [%d]", sao->getPlayer()->getName(), id_area);
		std::wstringstream ws;
		ws << "Area [" << id_area << "] opened";
		m_server->SendChatMessage(peer_id, ws.str());
	} else if (commandline[1].compare("false") == 0) {
		m_server->getAreaMgr()->changeOpen(id_area, sao->getPlayer()->getDBId(), false);
		std::wstringstream ws;
		ws << "Area [" << id_area << "] closed";
		m_server->SendChatMessage(peer_id, ws.str());
		logger.notice("%s close area [%d]", sao->getPlayer()->getName(), id_area);
	}
	return true;
}

}
