/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#include "irr_v3d.h"
#include <line3d.h>
#include <string>
#include <vector>
#include <unordered_map>
#include <algorithm>

class RemotePlayer;
class Server;
class GameDatabase;

namespace epixel {

#define MAX_AREA_ID 10240
#define MAX_AREA_AUTOPROTECT_X 256
#define MAX_AREA_AUTOPROTECT_Y 256
#define MAX_AREA_AUTOPROTECT_Z 256

enum AreaPvpFlag
{
	AREAPVP_UNSET,
	AREAPVP_ENABLE,
	AREAPVP_DISABLE,
};

struct Area {
	Area(const std::vector<u32> _owners, const std::string &name, const v3f &pos1, const v3f &pos2, u32 parent_id,
		 bool open, AreaPvpFlag pflag):
		name(name), owners(_owners), parent_id(parent_id), pos1(pos1),
		pos2(pos2), open(open), pvpflag(pflag), id(0) {}

	std::string name;
	std::vector<u32> owners;
	u32 parent_id;
	v3f pos1;
	v3f pos2;
	bool open;
	AreaPvpFlag pvpflag;

	u32 id;

	std::string toString() const;

	inline void init() { genAreaLinesAndCorners(); }

	inline bool isOwner(const u32 player_id) const
	{
		return (std::find(owners.begin(), owners.end(), player_id) != owners.end());
	}

	// Caching
	std::string hud_string = "";
	std::string owner_string = "";
	inline const std::string toStringHud() const { return hud_string; }

	std::vector<core::line3d<f32> > areabox_lines;
	std::vector<v3f> corners;
	void genAreaLinesAndCorners();
};

typedef std::unordered_map<u32, Area*> AreaMap;
typedef std::vector<std::string> AreaOwnerList;

enum AreaOwnerModRv
{
	AREAOWNER_OK,
	AREAOWNER_AREA_NOT_FOUND,
	AREAOWNER_NOT_OWNER,
	AREAOWNER_EXISTS,
	AREAOWNER_NOT_EXISTS,
	AREAOWNER_LAST_OWNER,
};

class AreaMgr {
public:
	AreaMgr(Server* server, const std::string& world_path, GameDatabase* database);
	~AreaMgr();
	u32 addArea(const u32 owner, const std::string &name, const v3f &pos1, const v3f &pos2, u32 parent_id,
				bool open = false, AreaPvpFlag flag = AREAPVP_UNSET);
	void removeArea(u32 id, bool recurse);
	bool isSubArea(const v3f &pos1, const v3f &pos2, u32 id) const;
	bool isAreaOwner(u32 id, const u32 player_id) const;
	Area* getArea(const u32 id) const;
	bool canInteract(const v3f &pos, const u32 player_id = 0) const;
	bool canDoPvp(const v3f &pos) const;
	AreaOwnerList getNodeOwners(const v3f &pos) const;
	void renameArea(u32 id, const std::string &name);
	void changeAreaPvp(u32 id, AreaPvpFlag flag);
	std::string moveArea(u32 id, const v3f &pos1, const v3f &pos2);
	void changeOwner(const u32 area_id, const u32 player_id, const u32 oldowner_id, const u32 newowner_id);
	const AreaOwnerModRv addOwner(const u32 area_id, const u32 owner_id, const u32 add_owner_id);
	const AreaOwnerModRv removeOwner(const u32 area_id, const u32 owner_id, const u32 rm_owner_id);
	bool changeOpen(u32 id, const u32 player_id, bool open);
	std::string listAreas(u16 peer_id);

	void updatePlayerHud(RemotePlayer* player);

	bool canPlayerAddArea(const v3f &pos1, const v3f &pos2, const u32 player_id, std::string &errorMsg);

	u32 getAreaIdByPos1Pos2(const v3f &pos1, const v3f &pos2);
	void Load();
	const std::string getWorldPath() { return m_world_path; }
private:
	u32 findFirstUnusedId();

	void getChildren(u32 parent_id, std::vector<u32> &childs);
	inline void getAreasAtPos(const v3f &pos, AreaMap &areas) const
	{
		for (const auto &it: m_areas) {
			if (pos.X >= it.second->pos1.X && pos.X <= it.second->pos2.X &&
			   pos.Y >= it.second->pos1.Y && pos.Y <= it.second->pos2.Y &&
			   pos.Z >= it.second->pos1.Z && pos.Z <= it.second->pos2.Z) {
					areas[it.first] = it.second;
			}
		}
	}

	std::vector<v3f> sortPositions(v3f pos1, v3f pos2);

	bool canInteractInArea(const v3f &pos1, const v3f &pos2, const u32 player_id, Area *&area, u32 &areaId) const;

	// Caching
	void regenCachedStrings(Area* area);

	AreaMap m_areas;
	Server* m_server;
	GameDatabase* m_game_database;
	std::string m_world_path;

	bool m_setting_selfprotect;
	u32 max_area_number_per_user;
};

}
