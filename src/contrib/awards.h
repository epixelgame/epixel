/*
 * Epixel
 * Copyright (C) 2015 puma.rc, Jeremy LOMORO <jeremy.lomoro@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef __AWARDS_H__
#define __AWARDS_H__

#include <string>
#include "irrlichttypes.h"

namespace epixel {

enum AchievementType {
	ACHIEVEMENT_DIG = 0,
	ACHIEVEMENT_PLACE,
	ACHIEVEMENT_JOIN,
	ACHIEVEMENT_DEATH,
	ACHIEVEMENT_KILL_PLAYER,
	ACHIEVEMENT_KILL_CREATURE,
};

struct Achievement
{
	u32 id;
	std::string name;
	std::string description;
	std::string image;
	AchievementType at;
	u32 target_count;
	std::string target_what;
};

struct AchievementProgress {
	AchievementProgress(const u32 a_id_achievement = 0, const u32 a_progress = 0, const bool a_unlocked = false):
		id_achievement(a_id_achievement), progress(a_progress), unlocked(a_unlocked) {}

	u32 id_achievement;
	u32 progress;
	bool unlocked;

};
}
#endif // AWARDS_H
