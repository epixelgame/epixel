/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "contrib/ban.h"
#include "server.h"
#include "contrib/chathandler.h"
#include "contrib/utils.h"
#include "database.h"
#include "log.h"
#include <ctime>
#include <iostream>
#include <sstream>
#include <iomanip>

namespace epixel {

BanMgr::BanMgr(GameDatabase* db):
		m_game_database(db)
{

}

BanMgr::~BanMgr()
{
	for (u8 i = 0; i < BANTYPE_COUNT; i++) {
		for (auto &ban: m_bans[i]) {
			delete ban.second;
		}
	}
}

void BanMgr::Load()
{
	logger.notice("Loading player bans...");

	m_game_database->loadPlayerBans(m_bans[BANTYPE_PLAYER]);
	m_game_database->loadIPBans(m_bans[BANTYPE_IP]);

	// Cleanup expired bans
	u32 removedBans = 0;
	u32 now = (u32) std::time(NULL);
	for (u8 i = 0; i < BANTYPE_COUNT; i++) {
		for (BanMap::const_iterator it = m_bans[i].begin(); it != m_bans[i].end();) {
			std::string bannedPlayer = it->first;
			BanMap::const_iterator its = it;
			++it;
			// remove ban if not infinite and expired
			if (its->second->bantime < now && its->second->bantime != U16_MAX) {
				removeBan(bannedPlayer, (BanType) i);
				removedBans++;
			}
		}
	}

	if (removedBans > 0) {
		logger.notice("%d bans removed due to expiration.", removedBans);
	}

	logger.notice("%d player bans loaded.", m_bans[BANTYPE_PLAYER].size());
	logger.notice("%d ip bans loaded.", m_bans[BANTYPE_IP].size());
}

void BanMgr::addBan(const std::string &who, const std::string &source, s32 bantime, const std::string &reason, BanType t)
{
	assert(t < BANTYPE_COUNT);
	epixel::Ban* ban;

	// If not infinite ban, add current date
	if (bantime != -1) {
		bantime += std::time(NULL);
	}

	BanMap::const_iterator it = m_bans[t].find(who);
	if (it != m_bans[t].end()) {
		ban = it->second;
		ban->bantime = (u32) bantime;
		ban->source = source;
		ban->reason = reason;
	}
	else {
		ban = new Ban(bantime, source, reason);
		m_bans[t][who] = ban;
	}

	if (t == BANTYPE_PLAYER) {
		m_game_database->savePlayerBan(who, ban);
	}
	else if (t == BANTYPE_IP){
		m_game_database->saveIPBan(who, ban);
	}
}

void BanMgr::removeBan(const std::string &who, BanType t)
{
	BanMap::const_iterator it = m_bans[t].find(who);
	if (it != m_bans[t].end()) {
		delete it->second;
		m_bans[t].erase(it->first);
		if (t == BANTYPE_PLAYER) {
			m_game_database->deletePlayerBan(who);
		}
		else if (t == BANTYPE_IP) {
			m_game_database->deleteIPBan(who);
		}
	}
}

bool BanMgr::isBanned(const std::string &who, std::string &reason, BanType t)
{
	BanMap::const_iterator it = m_bans[t].find(who);
	if (it != m_bans[t].end()) {
		u32 now = (u32) std::time(NULL);
		if (it->second->bantime > now) {
			std::stringstream ss;
			std::time_t tt = it->second->bantime;
			std::tm tm = *std::localtime(&tt);
			ss << "Banned ! Expires: ";
			if (it->second->bantime != 0xFFFF) {
				ss << std::put_time(&tm, "%a %d %b %Y at %H:%M:%S");
			}
			else {
				ss << "Never";
			}
			ss << ", Reason: " << it->second->reason;
			reason = ss.str();
			return true;
		}
		// If ban time expired, cleanup the ban from memory and database
		else {
			removeBan(who, t);
		}
	}
	return false;
}

/**
 * @brief ChatHandler::handleCommand_ban_list
 * @param args
 * @param peer_id
 * @return returns true
 *
 * Command to list all player bans, sources, and reasons
 */
bool ChatHandler::handleCommand_ban_list(const std::string &args, const u16 peer_id)
{
	std::wstringstream ws;

	for (auto it: m_server->getBanMgr()->getPlayerBans()) {
		ws << "Player[" << it.first.c_str() << "]: banned by " << it.second->source.c_str()
				<< ", reason: " << it.second->reason.c_str() << std::endl;
	}
	m_server->SendChatMessage(peer_id, ws.str());
	return true;
}

/**
 * @brief ChatHandler::handleCommand_ban_player
 * @param args
 * @param peer_id
 * @return returns true
 *
 * Command to ban a player by his name
 * Usage:
 *   /ban player <playername> <minutes/-1> <reason>
 * If minutes is -1, it's infinite time
 */
bool ChatHandler::handleCommand_ban_player(const std::string &args, const u16 peer_id)
{
	std::string name = m_server->getPlayerName(peer_id);

	std::vector<std::string> commandline;
	std::stringstream ss(args);
	std::string item;
	while (std::getline(ss, item, ' ')) {
		commandline.push_back(item);
	}

	if (commandline.size() < 3) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help ban player");
		return true;
	}

	if (commandline[0].compare(name) == 0) {
		m_server->SendChatMessage(peer_id, L"You cannot ban yourself !");
		return true;
	}

	std::string bannedplayer = commandline[0];
	s32 bantime = (s32) atoi(commandline[1].c_str());

	if (bantime == 0) {
		m_server->SendChatMessage(peer_id,
				L"Could not ban player for only zero minute. Please provide a valid value.");
		return true;
	}

	std::stringstream ss2;

	// Read other arguments as reason
	for (u8 i = 2; i < commandline.size(); i++) {
		// Add space if it's not the first word
		if (i != 2) {
			ss2 << " ";
		}
		ss2 << commandline[i];
	}

	std::string reason = ss2.str();

	std::wstringstream ws;
	if (bantime != -1) {
		ws << "Player " << bannedplayer.c_str() << " banned for " << secondsToHumanString((u32) (bantime * 60)).c_str()
				<< " (Reason: " << reason.c_str() << ")";
	}
	else {
		ws << "Player " << bannedplayer.c_str() << " banned for infinite"
				<< " time (Reason: " << reason.c_str() << ")";
	}
	m_server->getBanMgr()->addBan(bannedplayer, name, bantime * 60, reason, BANTYPE_PLAYER);
	m_server->SendChatMessage(peer_id, ws.str());
	return true;
}

/**
 * @brief ChatHandler::handleCommand_ban_ip
 * @param args
 * @param peer_id
 * @return returns true
 *
 * Command to ban an IP address.
 * Usage:
 *   /ban ip <ip> <minutes/-1> <reason>
 * If minutes is -1, it's infinite time
 */
bool ChatHandler::handleCommand_ban_ip(const std::string &args, const u16 peer_id)
{
	std::string name = m_server->getPlayerName(peer_id);

	std::vector<std::string> commandline;
	std::stringstream ss(args);
	std::string item;
	while (std::getline(ss, item, ' ')) {
		commandline.push_back(item);
	}

	if (commandline.size() < 3) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help ban ip");
		return true;
	}

	std::string bannedip = commandline[0];

	// Check if it's an IPv4
	if (!isIPv4(bannedip)) {
		m_server->SendChatMessage(peer_id,
				L"This is not and IPv4 address. Please provide a valid value.");
		return true;
	}

	s32 bantime = (s32) atoi(commandline[1].c_str());

	if (bantime == 0) {
		m_server->SendChatMessage(peer_id,
				L"Could not ban IP for only zero minute. Please provide a valid value.");
		return true;
	}

	std::stringstream ss2;

	// Read other arguments as reason
	for (u8 i = 2; i < commandline.size(); i++) {
		// Add space if it's not the first word
		if (i != 2) {
			ss2 << " ";
		}
		ss2 << commandline[i];
	}

	std::string reason = ss2.str();

	std::wstringstream ws;
	if (bantime != -1) {
		ws << "IP " << bannedip.c_str() << " banned for " << secondsToHumanString((u32) (bantime * 60)).c_str()
				<< " (Reason: " << reason.c_str() << ")";
	}
	else {
		ws << "IP " << bannedip.c_str() << " banned for infinite"
				<< " time (Reason: " << reason.c_str() << ")";
	}
	m_server->getBanMgr()->addBan(bannedip, name, bantime * 60, reason, BANTYPE_IP);
	m_server->SendChatMessage(peer_id, ws.str());
	return true;
}

/**
 * @brief ChatHandler::handleCommand_unban_player
 * @param args
 * @param peer_id
 * @return returns true
 *
 * Command to unban a banned player (/unban player <playername>)
 */
bool ChatHandler::handleCommand_unban_player(const std::string &args, const u16 peer_id)
{
	std::vector<std::string> commandline;
	std::stringstream ss(args);
	std::string item;
	while (std::getline(ss, item, ' ')) {
		commandline.push_back(item);
	}

	if (commandline.size() != 1) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help unban player");
		return true;
	}

	m_server->getBanMgr()->removeBan(args, BANTYPE_PLAYER);
	std::wstringstream ws;
	ws << "Player " << args.c_str() << " unbanned.";
	m_server->SendChatMessage(peer_id, ws.str());
	return true;
}

/**
 * @brief ChatHandler::handleCommand_unban_ip
 * @param args
 * @param peer_id
 * @return returns true
 *
 * Command to unban a banned IPv4 address (/unban ip <ip>)
 */
bool ChatHandler::handleCommand_unban_ip(const std::string &args, const u16 peer_id)
{
	std::vector<std::string> commandline;
	std::stringstream ss(args);
	std::string item;
	while (std::getline(ss, item, ' ')) {
		commandline.push_back(item);
	}

	if (commandline.size() != 1) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help unban ip");
		return true;
	}

	// Check if it's an IPv4
	if (!isIPv4(commandline[0])) {
		m_server->SendChatMessage(peer_id,
				L"This is not and IPv4 address. Please provide a valid value.");
		return true;
	}

	m_server->getBanMgr()->removeBan(args, BANTYPE_IP);
	std::wstringstream ws;
	ws << "IP " << args.c_str() << " unbanned.";
	m_server->SendChatMessage(peer_id, ws.str());
	return true;
}

}
