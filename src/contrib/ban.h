/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#include "irr_v3d.h"
#include <unordered_map>
#include <string>
#include <regex>

class GameDatabase;

namespace epixel
{

struct Ban
{
	Ban(u32 time, const std::string &src, const std::string &rs):
		bantime(time), source(src), reason(rs) {}
	u32 bantime;
	std::string source;
	std::string reason;
};

typedef std::unordered_map<std::string, epixel::Ban*> BanMap;

enum BanType
{
	BANTYPE_PLAYER,
	BANTYPE_IP,
	BANTYPE_COUNT,
};

class BanMgr
{
public:
	BanMgr(GameDatabase *db);
	~BanMgr();

	void Load();

	void addBan(const std::string &who, const std::string &source, s32 bantime, const std::string &reason, BanType t);
	void removeBan(const std::string &who, BanType t);
	BanMap getPlayerBans() { return m_bans[BANTYPE_PLAYER]; }
	BanMap getIPBans() { return m_bans[BANTYPE_IP]; }
	bool isBanned(const std::string &who, std::string &reason, BanType t);

private:
	BanMap m_bans[BANTYPE_COUNT];
	GameDatabase* m_game_database;
};

}

