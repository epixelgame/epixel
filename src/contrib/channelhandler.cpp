/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "channelhandler.h"
#include "server.h"
#include "contrib/chathandler.h"
#include "contrib/playersao.h"
#include "database.h"
#include "player.h"

namespace epixel
{

ChannelHandler::ChannelHandler(Server* server):
	m_server(server)
{
	m_channels.clear();
}

ChannelHandler::~ChannelHandler()
{
	for (const auto &it: m_channels) {
		delete it.second;
	}

	m_channels.clear();
}

/**
 * @brief ChannelHandler::onplayer_leave
 * @param peer_id
 *
 * Called from server when a player leave the server
 */
void ChannelHandler::onplayer_leave(const u16 peer_id)
{
	// Remove player_id from all subscribed channels
	for (ChannelList::iterator it = m_channels.begin(); it != m_channels.end();) {
		Channel* ch = it->second;

		// Remove peer_id from channel list
		ch->members.erase(peer_id);
		// If there is no member, remove chan
		if (ch->members.empty()) {
			delete ch;
			ChannelList::iterator rmit = it;
			++it;
			m_channels.erase(rmit);
		}
		else {
			announceLeaveChannel(peer_id, it->second, it->first.c_str());
			++it;
		}
	}
}

/**
 * @brief ChannelHandler::announceLeaveChannel
 * @param peer_id
 * @param channel
 * @param channelname
 *
 * Announce a player has left the channel to all its members
 */
void ChannelHandler::announceLeaveChannel(const u16 peer_id, const Channel *channel,
		const char* channelname)
{
	// Announce the player left to channel
	std::wstringstream announcestream;
	announcestream << L"<[" << channelname << L"]> "
			<< m_server->getPlayerName(peer_id).c_str() << L" left channel.";
	for (const auto &peer: channel->members) {
		m_server->SendChatMessage(peer, announcestream.str());
	}
}

/**
 * @brief ChatHandler::handleCommand_channel_join
 * @param args
 * @param peer_id
 * @return true
 *
 * Create a channel if it's the first player to join
 * else join existing channel
 */
bool ChatHandler::handleCommand_channel_join(const std::string &args, const u16 peer_id)
{
	std::vector<std::string> commandline;
	std::stringstream ss(args);
	std::string item;
	while (std::getline(ss, item, ' ')) {
		commandline.push_back(item);
	}

	if (commandline.size() != 1) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help ch join");
		return true;
	}

	ChannelList* chlist = m_channel_handler->getChannels();

	ChannelList::iterator cit = chlist->find(commandline[0]);

	// If no chan, create it
	if (cit == chlist->end()) {
		(*chlist)[commandline[0]] = new Channel();
		cit = chlist->find(commandline[0]);
	}
	// Else check if player is not already in the channel
	else if (cit->second->members.find(peer_id) != cit->second->members.end()) {
		m_server->SendChatMessage(peer_id, L"You already subscribed to this channel");
		return true;
	}

	// Announce player join channel
	std::wstringstream announcestream, joinstream;
	RemotePlayer* player = m_server->getEnv().getPlayer(peer_id);
	const char* channelname = cit->first.c_str();
	announcestream << L"<[" << channelname << L"]> "
			<< player->getName() << L" joined channel.";
	joinstream << L"You join channel " << channelname;
	for (const auto &it: cit->second->members) {
		m_server->SendChatMessage(it, announcestream.str());
	}
	m_server->SendChatMessage(peer_id, joinstream.str());
	cit->second->members.insert(peer_id);

	m_server->getGameDatabase()->addPlayerToChannel(player->getDBId(), cit->first);
	return true;
}

/**
 * @brief ChatHandler::handleCommand_channel_leave
 * @param args
 * @param peer_id
 * @return true
 *
 * Remove player from channel. If channel is empty, remove it
 */
bool ChatHandler::handleCommand_channel_leave(const std::string &args, const u16 peer_id)
{
	std::vector<std::string> commandline;
	std::stringstream ss(args);
	std::string item;
	while (std::getline(ss, item, ' ')) {
		commandline.push_back(item);
	}

	if (commandline.size() != 1) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help ch leave");
		return true;
	}

	ChannelList* chlist = m_channel_handler->getChannels();
	ChannelList::iterator cit = chlist->find(commandline[0]);

	// If no chan, error
	if (cit == chlist->end()) {
		m_server->SendChatMessage(peer_id, L"This channel doesn't exist, you cannot leave from it.");
		return true;
	}

	// Else check if player didn't joined to the channel
	if (cit->second->members.find(peer_id) == cit->second->members.end()) {
		m_server->SendChatMessage(peer_id, L"You didn't subscribed to this channel");
		return true;
	}

	// Remove player from channel
	cit->second->members.erase(peer_id);

	// Announce player left channel
	std::wstringstream leavestream;
	const char* channelname = cit->first.c_str();
	leavestream << L"You leave channel " << channelname;
	m_server->SendChatMessage(peer_id, leavestream.str());

	// If channel is empty
	if (cit->second->members.empty()) {
		delete cit->second;
		chlist->erase(cit);
	}
	// If not empty, announce
	else {
		m_channel_handler->announceLeaveChannel(peer_id, cit->second, channelname);
		RemotePlayer* player = m_server->getEnv().getPlayer(peer_id);
		m_server->getGameDatabase()->removePlayerFromChannel(player->getDBId(), cit->first);
	}
	return true;
}

/**
 * @brief ChatHandler::handleCommand_channel_say
 * @param args
 * @param peer_id
 * @return return true
 *
 * Handler for players to discuss on a channel
 */
bool ChatHandler::handleCommand_channel_say(const std::string &args, const u16 peer_id)
{
	std::vector<std::string> commandline;
	std::stringstream ss(args);
	std::string item;
	while (std::getline(ss, item, ' ')) {
		commandline.push_back(item);
	}

	if (commandline.size() < 2) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help ch say");
		return true;
	}

	ChannelList* chlist = m_channel_handler->getChannels();
	ChannelList::iterator cit = chlist->find(commandline[0]);

	// If not chan or player didn't joined to the channel
	if (cit == chlist->end() ||
			cit->second->members.find(peer_id) == cit->second->members.end()) {
		m_server->SendChatMessage(peer_id, L"You didn't subscribed to this channel");
		return true;
	}

	// Forge message
	std::wstringstream chanmessage;
	const char* channelname = cit->first.c_str();
	chanmessage << L"<[" << channelname << "]> "
			<< m_server->getPlayerName(peer_id).c_str() << L": ";
	for (u8 i = 1; i < commandline.size(); i++) {
		chanmessage << commandline[i].c_str() << L" ";
	}

	// Talk to channel
	for (const auto &dest_peer_id: cit->second->members) {
		PlayerSAO* dest_sao = m_server->getPlayerSAO(dest_peer_id);
		// If no SAO loaded ?!? or SAO doesn't ignore player, send message
		if (!dest_sao || !dest_sao->isPlayerIgnored(m_server->getPlayerName(peer_id))) {
			m_server->SendChatMessage(dest_peer_id, chanmessage.str());
		}
	}

	logger.notice("CHAT: %s", wide_to_narrow(chanmessage.str()).c_str());
	return true;
}

/**
 * @brief ChatHandler::handleCommand_emote
 * @param args
 * @param peer_id
 * @return return true
 *
 * Player send the given emote to the chat
 */
bool ChatHandler::handleCommand_emote(const std::string &args, const u16 peer_id)
{
	std::wstringstream ws;
	std::string playername;
	if (peer_id != PEER_ID_SERVER) {
		playername = m_server->getPlayerName(peer_id);
	}
	else {
		playername = "ConsoleAdmin said: ";
	}
	ws << L"* " << playername.c_str() << L" " << args.c_str();
	// @TODO add a range limit for emotes
	m_server->SendChatMessage(PEER_ID_BROADCAST, ws.str());
	m_server->SendChatMessage(PEER_ID_SERVER, ws.str());
	return true;
}

}
