/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#include <string>
#include <unordered_map>
#include <unordered_set>
#include "irrlichttypes.h"

class Server;
class GameDatabase;

namespace epixel
{

struct Channel
{
	std::unordered_set<u16> members;
};

typedef std::unordered_map<std::string, Channel*> ChannelList;

class ChannelHandler
{
public:
	ChannelHandler(Server *server);
	~ChannelHandler();

	void onplayer_leave(const u16 peer_id);
	void announceLeaveChannel(const u16 peer_id, const Channel* channel,
			const char *channelname);

	ChannelList* getChannels() { return &m_channels; }
private:
	ChannelList m_channels;
	Server* m_server;
};
}
