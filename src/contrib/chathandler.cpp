/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 * Copyright (C) 2005-2015  MaNGOS project <http://getmangos.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include <string.h>
#include <sstream>
#include "log.h"
#include "server.h"
#include "settings.h"
#include "contrib/chathandler.h"
#include "contrib/channelhandler.h"
#include "contrib/utils.h"

namespace epixel
{
#if defined(__FreeBSD__) || defined(__NetBSD__)    || \
		defined(__OpenBSD__) || defined(__DragonFly__) || \
		defined(__APPLE__)   ||                           \
		defined(__sun)       || defined(sun)           || \
		defined(__QNX__)     || defined(__QNXNTO__) || \
		defined(linux) || defined(__linux) || defined(__linux__)
static int kb_hit_return()
{
	struct timeval tv;
	fd_set fds;
	tv.tv_sec = 0;
	tv.tv_usec = 0;
	FD_ZERO(&fds);
	FD_SET(STDIN_FILENO, &fds);
	select(STDIN_FILENO + 1, &fds, NULL, NULL, &tv);
	return FD_ISSET(STDIN_FILENO, &fds);
}
#endif

void* ConsoleThread::run()
{
	porting::setThreadName("ConsoleThread");

	logger.notice("Starting server console");
	ThreadStarted();

	std::cout << "epixel> ";
	std::cout.flush();

	char commandbuf[256];

	while (!stopRequested()) {
#if defined(__FreeBSD__) || defined(__NetBSD__)    || \
		defined(__OpenBSD__) || defined(__DragonFly__) || \
		defined(__APPLE__)   ||                           \
		defined(__sun)       || defined(sun)           || \
		defined(__QNX__)     || defined(__QNXNTO__) || \
		defined(linux) || defined(__linux) || defined(__linux__)
		while (!kb_hit_return()) {
			sleep_ms(1);
		}
#endif

		char* command_str = fgets(commandbuf, sizeof(commandbuf), stdin);
		if (command_str != NULL) {
			for (int x = 0; command_str[x]; ++x) {
				if (command_str[x] == '\r' || command_str[x] == '\n') {
					command_str[x] = 0;
					break;
				}
			}

			if (command_str[0] != 0) {
				m_chat_handler->queueCommand(std::string(command_str));
			}
			std::cout << "epixel> ";
			std::cout.flush();
		}
	}
	return NULL;
}

ChatHandler::ChatHandler(Server *s): m_loaded(false), m_server(s)
{
	m_superadmin = "";
	g_settings->getNoEx("superadmin", m_superadmin);
	m_channel_handler = new epixel::ChannelHandler(s);
}

ChatHandler::~ChatHandler()
{
	while (!m_command_queue.empty()) {
		m_command_queue.pop_frontNoEx(1);
	}

	delete m_channel_handler;
}

static const ChatCommand COMMANDHANDLERFINISHER = { NULL, {}, false, NULL, NULL, L"" };
ChatCommand* ChatHandler::getCommandTable()
{
	static ChatCommand banCommandTable[] =
	{
		{ "ip",		{"ban"}, true, &ChatHandler::handleCommand_ban_ip, NULL, L"/ban ip <ip> <time/-1> <reason>" },
		{ "list",	{"ban"}, true, &ChatHandler::handleCommand_ban_list,	NULL, L"" },
		{ "player",	{"ban"}, true, &ChatHandler::handleCommand_ban_player, NULL, L"/ban player <name> <time/-1> <reason>" },
		COMMANDHANDLERFINISHER,
	};

	static ChatCommand areaCommandTable[] =
	{
		{ "add",			{"areas"}, false, &ChatHandler::handleCommand_area_add,   NULL, L"/area add <player> <name>" },
		{ "add_owner",		{}, true, &ChatHandler::handleCommand_area_add_owner,  NULL, L"/area add_owner <area id> <owner>" },
		{ "change_owner",	{}, true, &ChatHandler::handleCommand_area_change_owner,  NULL, L"/area change_owner <area id> <old owner> <new owner>" },
		{ "remove_owner",	{}, true, &ChatHandler::handleCommand_area_remove_owner,  NULL, L"/area add_owner <area id> <owner>" },
		{ "list",			{}, true, &ChatHandler::handleCommand_area_list,		NULL, L"/area list" },
		{ "open",			{}, true, &ChatHandler::handleCommand_area_open,		NULL, L"/area open <id> <true|false" },
		{ "pos1",			{"worldedit"}, false, &ChatHandler::handleCommand_area_pos1, NULL, L"Set the first area position to position 1" },
		{ "pos2",			{"worldedit"}, false, &ChatHandler::handleCommand_area_pos2, NULL, L"Set the second area position to position 2" },
		{ "pvp",			{}, true, &ChatHandler::handleCommand_area_pvp,		NULL, L"/area pvp <areaid> <enable|disable|default>" },
		{ "remove",			{}, true, &ChatHandler::handleCommand_area_remove,	NULL, L"/area remove <areaid>" },
		{ "rename",			{}, true, &ChatHandler::handleCommand_area_rename,	NULL, L"/area rename <areaid> <new name>" },
		COMMANDHANDLERFINISHER,
	};

	static ChatCommand channelCommandTable[] =
	{
		{ "join",	{}, false, &ChatHandler::handleCommand_channel_join,	NULL, L"/ch join <channel-name>" },
		{ "leave",	{}, false, &ChatHandler::handleCommand_channel_leave,	NULL, L"/ch leave <channel-name>" },
		{ "say",	{}, false, &ChatHandler::handleCommand_channel_say,		NULL, L"/ch say <channel-name> <msg>" },
		COMMANDHANDLERFINISHER,
	};

	static ChatCommand creatureCommandTable[] =
	{
		{ "spawn",	{"creature"}, false, &ChatHandler::handleCommand_creature_spawn,	NULL, L"/creature spawn <creaturename>" },
		COMMANDHANDLERFINISHER,
	};

	static ChatCommand homeCommandTable[] =
	{
		{ "set",	{"home"}, false, &ChatHandler::handleCommand_home_set,	NULL, L"/home set / Set your position to home" },
		{ "go",		{"home"}, false, &ChatHandler::handleCommand_home_go,	NULL, L"/home go / Teleport to home position" },
		COMMANDHANDLERFINISHER,
	};

	static ChatCommand privCommandTable[] =
	{
		{ "grant",	{"privs"}, true, &ChatHandler::handleCommand_grant_priv, NULL, L"/privs grant <player> <priv> / Grant privilege <priv> to <player>." },
		{ "list",	{}, true, &ChatHandler::handleCommand_list_privs, NULL, L"/privs list (<player>) / List player privs. If no param it's the current player privs." },
		{ "revoke",	{"privs"}, true, &ChatHandler::handleCommand_revoke_priv, NULL, L"/privs grant <player> <priv> / Revoke privilege <priv> to <player>." },
		COMMANDHANDLERFINISHER,
	};

	static ChatCommand rulesCommandTable[] =
	{
		{ "reload",	{"server"},	true,	&ChatHandler::handleCommand_rules_reload, NULL, L"Reload server rules from the configured file." },
		{ "show",	{},	false,	&ChatHandler::handleCommand_rules_show, NULL, L"Show the server rules." },
		COMMANDHANDLERFINISHER,
	};

	static ChatCommand serverCommandTable[] =
	{
		{ "clearobjects", {"server"}, false, &ChatHandler::handleCommand_server_clearobjects, NULL, L"Cleanup all world active objects. This action can take many time." },
		{ "shutdown", {"server"}, true, &ChatHandler::handleCommand_server_shutdown, NULL, L"/server shutdown (time) (message). Shutdown the server with the given "
				"maintenance message. If time is present and greater than 0, shutdown afeter a delay" },
		{ "status", {}, true, &ChatHandler::handleCommand_server_status, NULL, L"Display current server status." },
		COMMANDHANDLERFINISHER,
	};

	static ChatCommand teleportCommandTable[] =
	{
		{ "to",	{"teleport"}, false, &ChatHandler::handleCommand_teleport, NULL,	L"Teleport to given position -- <X>,<Y>,<Z> | <name> <X>,<Y>,<Z> | <name> <to_name>" },
		{ "add", {"teleportadmin"}, true, &ChatHandler::handleCommand_teleport_add, NULL,	L"/teleport add <X>,<Y>,<Z> <l>. Add a teleporter to location to position X,Y,Z with label l." },
		{ "remove", {"teleportadmin"}, true, &ChatHandler::handleCommand_teleport_remove, NULL, L"/teleport remove <X>,<Y>,<Z>. Remove teleporter location at position X,Y,Z." },
		COMMANDHANDLERFINISHER,
	};

	static ChatCommand timeCommandTable[] =
	{
		{ "set",	{"time"},	true,	&ChatHandler::handleCommand_time_set, NULL, L"Set time of map / /time set <0-24>:<0-60> -- example : 6:00 = 6:00 AM." },
		{ "",	{},	true,	&ChatHandler::handleCommand_time_get, NULL, L"Get time of map." },
		COMMANDHANDLERFINISHER,
	};

	static ChatCommand unbanCommandTable[] =
	{
		{ "ip",		{"ban"}, true, &ChatHandler::handleCommand_unban_ip, NULL, L"/unban ip <ip>" },
		{ "player", {"ban"}, true, &ChatHandler::handleCommand_unban_player, NULL, L"/unban player <player>" },
		COMMANDHANDLERFINISHER,
	};

	static ChatCommand ignoreCommandTable[] =
	{
		{ "add", {}, true, &ChatHandler::handleCommand_ignore_add, NULL, L"/ignore add <player>. Ignore chat messages coming from <player>" },
		{ "remove", {}, true, &ChatHandler::handleCommand_ignore_remove, NULL, L"/ignore remove <player>. Allow chat messages coming from <player>" },
		{ "list", {}, true, &ChatHandler::handleCommand_ignore_list, NULL, L"/ignore list. List the current ignore list." },
		COMMANDHANDLERFINISHER,
	};

	static ChatCommand worldeditorCommandTable[] =
	{
		{ "createscheme", {"worldedit"}, false, &ChatHandler::handleCommand_we_createscheme, NULL, L"/we createscheme <name> (<compress>). "
		  "Save the current region to [world]/schems/<name>.mts. <compress> make the mts compressed or not (default true)." },
		{ "importscheme", {"worldedit"}, false, &ChatHandler::handleCommand_we_importscheme, NULL, L"/we importscheme <name> (<rotation> (<compress>)). "
		  "Load the current region from [world]/schems/<name>.mts and replace the nodes between pos1 and (pos1.X + regionXsize, pos1.Y + regionYsize, "
		  "pos1.Z + regionYsize). <compress> load a compressed mts or not (default true). Rotation permit to rotate the import (default no. 1 => 90°, 2 => 180°, 3 => 270°)" },
		COMMANDHANDLERFINISHER,
	};

	static ChatCommand globalCommandTable[] =
	{
		{ "area",		{}, true, NULL, areaCommandTable,		L"Available subcommands: add, add_owner, change_owner, list, pos1, pos2, pvp, remove, remove_owner" },
		{ "ban",		{}, true, NULL, banCommandTable,		L"Available subcommands: player." },
		{ "ch",			{}, false, NULL, channelCommandTable,	L"Available subcommands: join, leave, say." },
		{ "creature",	{"creature"}, false, NULL, creatureCommandTable,	L"Available subcommands: spawn." },
		{ "give",		{"give"}, false, &ChatHandler::handleCommand_give, NULL,	L"Give objet -- /give [player] <objet> [number]" },
		{ "help",		{}, true, &ChatHandler::handleCommand_help, NULL, L"" },
		{ "home",		{}, false, NULL, homeCommandTable,		L"Available subcommands: set, go." },
		{ "ignore",		{}, false, NULL, ignoreCommandTable, L"Available subcommands: add, list, remove." },
		{ "kick",		{"kick"}, true, &ChatHandler::handleCommand_kick, NULL, L"/kick <player> <reason> Kick player <player> for reason <reason>" },
		{ "join",		{}, false, &ChatHandler::handleCommand_channel_join,	NULL, L"/join <channel-name>" },
		{ "lastlogin",	{}, true, &ChatHandler::handleCommand_last_login,	NULL, L"/lastlogin <player>" },
		{ "leave",		{}, false, &ChatHandler::handleCommand_channel_leave,	NULL, L"/leave <channel-name>" },
		{ "maintenance", {"server"}, true, &ChatHandler::handleCommand_maintenance, NULL, L"/maintenance <on/off> Change the maintenance status of the server." },
		{ "me",			{}, true, &ChatHandler::handleCommand_emote, NULL,	L"/me <emote>" },
		{ "mods",		{}, true, &ChatHandler::handleCommand_mods, NULL, L"List all available server mods." },
		{ "privs",		{}, false, NULL, privCommandTable,		L"Available subcommands: grant, list, revoke." },
		{ "rules",		{}, false, NULL, rulesCommandTable,		L"Available subcommands: show, reload." },
		{ "server",		{}, false, NULL, serverCommandTable,	L"Available subcommands: clearobjects, shutdown, status." },
		{ "teleport",	{}, false, NULL, teleportCommandTable,	L"Available subcommands: to, add, remove" },
		{ "time",		{}, true, NULL, timeCommandTable,		L"Set/Get time of day" },
		{ "unban",		{}, true, NULL, unbanCommandTable,	L"Available subcommands: player." },
		{ "w",			{"shout"}, false, &ChatHandler::handleCommand_whisper, NULL, L"/w <player> <msg>" },
		{ "we",			{}, false, NULL, worldeditorCommandTable,	L"Available subcommands: createscheme, importscheme." },
		COMMANDHANDLERFINISHER,
	};

	return globalCommandTable;
}

/**
 * @brief ChatHandler::handleCommand
 * @param text
 * @param peer_id
 * @return true if command was found and false if not found
 *
 * handleCommand is the core main handler for command.
 * This function look at the command table to find the correct handler
 * and, if found, permit to handle the command and its args.
 * If false is returned, the lua stack can try to handle the command
 */
bool ChatHandler::handleCommand(const std::string &text, u16 peer_id)
{
	// No slash or not 2 chars, it's not a command
	if (text.length() < 2 || (text[0] != '/' && peer_id != PEER_ID_SERVER)) {
		return false;
	}

	ChatCommand* command = NULL;
	ChatCommand* parentCommand = NULL;

	// Remove the initial slash
	const char* ctext = (peer_id != PEER_ID_SERVER) ? &(text.c_str())[1] : text.c_str();

	ChatCommandSearchResult res = findCommand(getCommandTable(), ctext, command, &parentCommand);
	switch (res) {
		case CHAT_COMMAND_OK: {
			// Handle console commands separately
			if (peer_id == PEER_ID_SERVER) {
				/*
				 * Execute command
				 * Re-add prompt
				 * return the command result
				 */
				bool commandResult = true;
				if (!command->console) {
					logger.error("This command cannot be run from console.");
				}
				else {
					commandResult = (this->*(command->Handler))(ctext, peer_id);
				}

				std::cout << "epixel> ";
				std::cout.flush();
				return commandResult;
			}

			std::string playername = m_server->getPlayerName(peer_id);

			// Search if player has rights for using the command, if empty rightlist: hasright = true
			bool hasright = (command->requiredprivs.size() == 0 ||
					m_superadmin.compare(playername) == 0);
			for (auto &right: command->requiredprivs) {
				std::set<std::string> privs = m_server->getPlayerEffectivePrivs(peer_id);
				if (privs.find(right) != privs.end()) {
					hasright = true;
					break;
				}
			}

			// Execute
			if (hasright) {
				return (this->*(command->Handler))(ctext, peer_id);
			}
			// Or error
			else {
				std::wstringstream ws;
				ws << L"You don't have permission to do this command (Missing privilege:";
				for (auto &priv: command->requiredprivs) {
					  ws << " " << priv.c_str();
				}
				ws << ")" << std::endl;
				m_server->SendChatMessage(peer_id, ws.str());
			}
			return true;
		}
		case CHAT_COMMAND_UNKNOWN_SUBCOMMAND:
			if (peer_id == PEER_ID_SERVER) {
				logger.error("Unknown subcommand.\n"
							 "%s", wide_to_utf8(command->help).c_str());
				std::cout << "epixel> ";
				std::cout.flush();
			}
			else {
				m_server->SendChatMessage(peer_id, command->help);
			}
			return true;
		case CHAT_COMMAND_UNKNOWN:
		default:
			if (peer_id == PEER_ID_SERVER) {
				logger.error("Unknown command.");
				std::cout << "epixel> ";
				std::cout.flush();
				return true;
			}
			return false;
	}
}

/**
 * @brief ChatHandler::findCommand
 * @param table
 * @param text
 * @param command
 * @param parentCommand
 * @return ChatCommandSearchResult
 *
 * Search the command recursively into tables and associated handlers
 */
ChatCommandSearchResult ChatHandler::findCommand(ChatCommand *table, const char* &text, ChatCommand*& command, ChatCommand** parentCommand)
{
	std::string cmd = "";
	// skip whitespaces
	while (*text != ' ' && *text != '\0') {
		cmd += *text;
		++text;
	}

	while (*text == ' ') { ++text; }

	// search first level command in table
	for (u32 i = 0; table[i].name != NULL; ++i) {
		// If it's not the searched command, skip
		size_t len = strlen(table[i].name);
		if (strncmp(table[i].name, cmd.c_str(), len + 1) != 0) {
			continue;
		}

		// If there is a child command
		if (table[i].childCommand != NULL) {
			const char* stext = text;
			ChatCommand* parentSubcommand = NULL;
			ChatCommandSearchResult res = findCommand(table[i].childCommand, text, command, &parentSubcommand);

			// Lookup child command
			switch (res) {
				case CHAT_COMMAND_OK:
					// if subcommand success search not return parent command, then
					// this parent command is owner of child commands
					if (parentCommand) {
						*parentCommand = parentSubcommand ? parentSubcommand : &table[i];
					}

					// name == "" is special case: restore original command text
					// for next level "" (where parentSubcommand==NULL)
					if (strlen(command->name) == 0 && !parentSubcommand) {
						text = stext;
					}
					return CHAT_COMMAND_OK;
				case CHAT_COMMAND_UNKNOWN:
					// command not found directly in child command list, return child command list owner
					command = &table[i];
					if (parentCommand)
						*parentCommand = NULL;              // we don't known parent of table list at this point

					text = stext;                           // restore text to stated just after parse found parent command
					return CHAT_COMMAND_UNKNOWN_SUBCOMMAND; // we not found subcommand for table[i]
				case CHAT_COMMAND_UNKNOWN_SUBCOMMAND:
				default:
					// some deep subcommand not found, if this second level subcommand then parentCommand can be NULL, use known value for it
					if (parentCommand)
						*parentCommand = parentSubcommand ? parentSubcommand : &table[i];
					return res;
			}
		}

		// Unhandled command, skip
		if (!table[i].Handler) {
			continue;
		}

		// Command handler found
		command = &table[i];

		if (parentCommand) {
			*parentCommand = NULL;
		}

		return CHAT_COMMAND_OK;
	}

	// command not found in table directly
	command = NULL;

	// unknown table owner at this point
	if (parentCommand)
		*parentCommand = NULL;

	return CHAT_COMMAND_UNKNOWN;
}

bool ChatHandler::handleCommand_help(const std::string &args, const u16 peer_id)
{
	if (args.empty()) {
		ChatCommand* cmds = getCommandTable();
		std::vector<std::string> availableCmds;
		for (u32 i = 0; cmds[i].name != NULL; ++i) {
			// If we are a player, add all, else add only console commands
			if (peer_id != PEER_ID_SERVER || cmds[i].console) {
				availableCmds.push_back(cmds[i].name);
			}
		}

		std::string jcmds = epixel::join(availableCmds.begin(), availableCmds.end(), std::string(", "));
		std::wstringstream ws;
		ws << "Server embedded commands: " << jcmds.c_str();
		m_server->SendChatMessage(peer_id, ws.str());
		// Return true permit to drop the lua command check for console
		return (peer_id == PEER_ID_SERVER);
	}

	ChatCommand* command = NULL;
	ChatCommand* parentCommand = NULL;
	const char* ctext = args.c_str();

	ChatCommandSearchResult res = findCommand(getCommandTable(), ctext, command, &parentCommand);
	if (res == CHAT_COMMAND_OK) {
		if (peer_id != PEER_ID_SERVER || command->console) {
			m_server->SendChatMessage(peer_id, command->help);
		}
		else {
			logger.error("This command cannot be run from console.");
		}
		return true;
	}

	if (peer_id == PEER_ID_INEXISTENT) {
		logger.error("Command help not found.");
	}

	// Let mods handle it if it's not the console
	return (peer_id == PEER_ID_SERVER);
}

/**
 * @brief ChatHandler::queueCommand
 * @param command
 *
 * Add the command from console thread to server thread command queue
 */
void ChatHandler::queueCommand(const std::string &command)
{
	m_command_queue.push_back(command);
}

/**
 * @brief ChatHandler::runEnqueuedCommands
 *
 * Run all the queued commands in server thread
 */
void ChatHandler::runEnqueuedCommands()
{
	while (!m_command_queue.empty()) {
		const std::string cmd = m_command_queue.pop_frontNoEx();
		// Commands can trigger exceptions, avoid it
		try {
			handleCommand(cmd, PEER_ID_SERVER);
		}
		catch (std::exception &) {}
	}
}

}
