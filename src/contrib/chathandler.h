/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 * Copyright (C) 2005-2015  MaNGOS project <http://getmangos.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#include <string>
#include <vector>
#include <queue>
#include "threads/thread.h"
#include "util/container.h"
#include "irrlichttypes_extrabloated.h"

class Server;

namespace epixel
{

class ChatHandler;
class ChannelHandler;

class ConsoleThread : public Thread
{
public:
	ConsoleThread(ChatHandler *ch):
		Thread(),
		m_chat_handler(ch)
	{}

	void * run();
private:
	ChatHandler* m_chat_handler;
};

struct ChatCommand
{
	const char* name;
	const std::vector<std::string> requiredprivs;
	bool console;
	bool (ChatHandler::*Handler)(const std::string &args, const u16 peer_id);
	ChatCommand* childCommand;
	const std::wstring help;
};

enum ChatCommandSearchResult
{
	CHAT_COMMAND_OK,                    // found accessible command by command string
	CHAT_COMMAND_UNKNOWN,               // first level command not found
	CHAT_COMMAND_UNKNOWN_SUBCOMMAND,    // command found but some level subcommand not find in subcommand list
};

class ChatHandler
{
public:
	ChatHandler(Server* s);
	~ChatHandler();
	bool handleCommand(const std::string &text, const u16 peer_id);

	// Handlers

	// Areas
	bool handleCommand_area_add(const std::string &args, const u16 peer_id);
	bool handleCommand_area_change_owner(const std::string &args, const u16 peer_id);
	bool handleCommand_area_add_owner(const std::string &args, const u16 peer_id);
	bool handleCommand_area_remove_owner(const std::string &args, const u16 peer_id);
	bool handleCommand_area_list(const std::string &args, const u16 peer_id);
	bool handleCommand_area_rename(const std::string &args, const u16 peer_id);
	bool handleCommand_area_remove(const std::string &args, const u16 peer_id);
	bool handleCommand_area_open(const std::string &args, const u16 peer_id);
	bool handleCommand_area_pos1(const std::string &args, const u16 peer_id);
	bool handleCommand_area_pos2(const std::string &args, const u16 peer_id);
	bool handleCommand_area_pvp(const std::string &args, const u16 peer_id);

	// Bans
	bool handleCommand_ban_ip(const std::string &args, const u16 peer_id);
	bool handleCommand_ban_list(const std::string &args, const u16 peer_id);
	bool handleCommand_ban_player(const std::string &args, const u16 peer_id);
	bool handleCommand_unban_ip(const std::string &args, const u16 peer_id);
	bool handleCommand_unban_player(const std::string &args, const u16 peer_id);

	// ChannelHandler
	bool handleCommand_emote(const std::string &args, const u16 peer_id);
	bool handleCommand_channel_join(const std::string &args, const u16 peer_id);
	bool handleCommand_channel_leave(const std::string &args, const u16 peer_id);
	bool handleCommand_channel_say(const std::string &args, const u16 peer_id);

	// ChatHandler
	bool handleCommand_help(const std::string &args, const u16 peer_id);

	// Home
	bool handleCommand_home_set(const std::string &args, const u16 peer_id);
	bool handleCommand_home_go(const std::string &args, const u16 peer_id);

	// Player privs
	bool handleCommand_list_privs(const std::string &args, const u16 peer_id);
	bool handleCommand_grant_priv(const std::string &args, const u16 peer_id);
	bool handleCommand_revoke_priv(const std::string &args, const u16 peer_id);
	bool handleCommand_rules_show(const std::string &args, const u16 peer_id);

	// Player ignore list
	bool handleCommand_ignore_add(const std::string &args, const u16 peer_id);
	bool handleCommand_ignore_remove(const std::string &args, const u16 peer_id);
	bool handleCommand_ignore_list(const std::string &args, const u16 peer_id);

	// Server related
	bool handleCommand_kick(const std::string &args, const u16 peer_id);
	bool handleCommand_last_login(const std::string &args, const u16 peer_id);
	bool handleCommand_mods(const std::string &args, const u16 peer_id);
	bool handleCommand_server_clearobjects(const std::string &args, const u16 peer_id);
	bool handleCommand_server_shutdown(const std::string &args, const u16 peer_id);
	bool handleCommand_server_status(const std::string &args, const u16 peer_id);
	bool handleCommand_whisper(const std::string &args, const u16 peer_id);
	bool handleCommand_maintenance(const std::string &args, const u16 peer_id);
	bool handleCommand_rules_reload(const std::string &args, const u16 peer_id);

	// Creatures
	bool handleCommand_creature_spawn(const std::string &args, const u16 peer_id);

	// Give
	bool handleCommand_give(const std::string &args, const u16 peer_id);

	// Time
	bool handleCommand_time_set(const std::string &args, const u16 peer_id);
	bool handleCommand_time_get(const std::string &args, const u16 peer_id);

	// Teleport
	bool handleCommand_teleport(const std::string &args, const u16 peer_id);
	bool handleCommand_teleport_add(const std::string &args, const u16 peer_id);
	bool handleCommand_teleport_remove(const std::string &args, const u16 peer_id);

	// World Editor
	bool handleCommand_we_createscheme(const std::string &args, const u16 peer_id);
	bool handleCommand_we_importscheme(const std::string &args, const u16 peer_id);

	epixel::ChannelHandler* getChannelHandler() { return m_channel_handler; }

	void queueCommand(const std::string &command);
	void runEnqueuedCommands();
private:
	// Factorized function
	bool handleCommand_area_pos(u16 peer_id, u8 pos);

	// Other
	ChatCommandSearchResult findCommand(ChatCommand* table, const char *&text, ChatCommand *&command, ChatCommand **parentCommand = NULL);
	ChatCommand* getCommandTable();

	bool m_loaded;

	Server* m_server;
	std::string m_superadmin;

	epixel::ChannelHandler* m_channel_handler;
	MutexedQueue<std::string> m_command_queue;
};

}

