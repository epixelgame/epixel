/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "contrib/creature.h"
#include "contrib/itemsao.h"
#include "contrib/chathandler.h"
#include "contrib/creatures/cow.h"
#include "contrib/creatures/sheep.h"
#include "contrib/playersao.h"
#include "contrib/projectilesao.h"
#include "contrib/spell.h"
#include "util/serialize.h"
#include "genericobject.h"
#include "nodedef.h"
#include "server.h"
#include "settings.h"
#include "scripting_game.h"
#include "tool.h"
#include <sstream>
#include <regex>

namespace epixel {

const CreatureStateHandler Creature::state_handler[CREATURESTATE_MAX] = {
	{ CREATURESTATE_NONE, &Creature::handleState_None },
	{ CREATURESTATE_STAND, &Creature::handleState_Stand },
	{ CREATURESTATE_ATTACK, &Creature::handleState_Attack },
	{ CREATURESTATE_WALK, &Creature::handleState_Walk },
	{ CREATURESTATE_FLOP, &Creature::handleState_Flop },
};

Creature* Creature::createCreatureObj(ServerEnvironment *env, v3f pos,
	const std::string &name, const std::string &state)
{
	epixel::Creature *sao = nullptr;
	if (name.compare("mobs:sheep") == 0) {
		sao = new epixel::creature::Sheep(env, pos, name, state);
	}
	else if (name.compare("mobs:cow") == 0) {
		sao = new epixel::creature::Cow(env, pos, name, state);
	}
	else {
		sao = new epixel::Creature(env, pos, name, state);
	}
	return sao;
}

Creature::Creature(ServerEnvironment *env, v3f pos,
		const std::string &name, const std::string &state):
		LuaEntitySAO(env, pos, name, state),
		m_state(CREATURESTATE_NONE),
		m_child(false), m_tamed(false),
		m_can_fly(false),
		m_can_attack_monsters(false), m_blink(false), m_knockbacked(false),
		m_standing_for_attack(false),
		m_target(nullptr), m_target_id(0),
		m_life_timer(0), m_random_sound_timer(30.0f), m_environmental_damages_timer(5.0f),
		m_searchfortarget_timer(1.0f), m_knockback_timer(0.0f),
		m_walk_timer(1.5f), m_stand_timer(1.0f), m_blink_timer(0.2f),
		m_current_animation(CREATUREANIMATION_STAND)
{
	if(env == NULL) {
		ServerActiveObject::registerType(getType(), create);
		return;
	}

	m_old_y = m_base_position.Y;
}

Creature::~Creature()
{
}

ServerActiveObject* Creature::create(ServerEnvironment *env, const v3f& pos,
		const std::string &data)
{
	std::string name;
	std::string state;
	s16 hp = 1;
	v3f velocity;
	float yaw = 0;
	if(data != ""){
		std::istringstream is(data, std::ios::binary);
		// read version
		u8 version = readU8(is);
		// check if version is supported
		if(version == 0){
			name = deSerializeString(is);
			state = deSerializeLongString(is);
		}
		else if(version == 1){
			name = deSerializeString(is);
			state = deSerializeLongString(is);
			hp = readS16(is);
			velocity = readV3F1000(is);
			yaw = readF1000(is);
		}
	}
	// create object
	logger.info("Creature::create(name='%s' state='%s'')", name.c_str(), state.c_str());

	epixel::Creature *sao = Creature::createCreatureObj(env, pos, name, state);
	sao->m_hp = hp;
	sao->m_velocity = velocity;
	sao->m_yaw = yaw;
	return sao;
}

void Creature::addedToEnvironment(u32 dtime_s)
{
	ServerActiveObject::addedToEnvironment(dtime_s);
	// Create entity from name
	m_registered = m_env->getScriptIface()->
			luaentity_Add(m_id, m_init_name.c_str(), true);

	initCreature();
}

void Creature::step(float dtime, bool send_recommended)
{
	LuaEntitySAO::step(dtime, send_recommended);

	INodeDefManager* ndef = m_env->getGameDef()->getNodeDefManager();
	// Set lifetimer of mob
	if (m_def->type != CREATURETYPE_NPC && !m_tamed) {
		m_life_timer -= dtime;
		if (m_life_timer <= 0 && m_state != CREATURESTATE_ATTACK) {
			// @TODO effect
			m_env->spawnEffect(m_base_position, 15, "tnt_smoke.png");
			m_removed = true;
			return;
		}
	}

	// If creature is knockbacked, don't do anything
	if (m_knockbacked) {
		m_knockback_timer -= dtime;
		if (m_knockback_timer <= 0.0f) {
			m_knockbacked = false;
		}
		else {
			return;
		}
	}

	// Check if mob need to drop/replace node
	if (m_state != CREATURESTATE_ATTACK &&
			m_def->replacerate && !m_child && (myrand_range(1, m_def->replacerate) == 1) &&
			m_replace_mapper.size()) {
		v3f pos = m_base_position / BS;
		pos.Y += m_def->replaceoffset;
		v3s16 pos16(pos.X, pos.Y, pos.Z);

		// Check if node is valid
		content_t c = m_env->getMap().getNode(pos16).getContent();
		if (c != CONTENT_IGNORE) {
			// Look at the mob replace map to find a node to replace
			std::map<content_t, content_t>::const_iterator it = m_replace_mapper.find(c);

			// If that match and the mob doesn't move, replace it
			if (getVelocity().Y == 0 && it != m_replace_mapper.end() && (*it).first == c) {
				m_env->setNode(pos16, MapNode((*it).second));
			}
		}
	}

	if (!m_can_fly) {
		v3f pos = m_base_position / BS;
		v3s16 pos16(pos.X, pos.Y, pos.Z);
		MapNode node = m_env->getMap().getNode(pos16);

		v3f vel = getVelocity();

		if (vel.Y > 0.1) {
			setAcceleration(v3f(0, m_def->fall_speed, 0));
		}

		// @TODO verify group is called water
		if (itemgroup_get(ndef->get(node).groups, "water")) {
			if (m_def->can_float) {
				setAcceleration(v3f(0, -m_def->fall_speed / pow(std::max(1.0f, vel.Y), 2), 0));
			}
		}
		else {
			setAcceleration(v3f(0, m_def->fall_speed, 0));
			if (m_def->can_take_fall_damage && getVelocity().Y == 0) {
				float dist = std::abs((int)((m_old_y - m_base_position.Y) / BS));

				if (dist > 5.0f) {
					// Damages by fall are distance - 5m
					u16 damage = floor(dist - 5);
					doDamage(damage);
					m_env->spawnEffect(m_base_position, 5, "tnt_smoke.png");
				}
				m_old_y = m_base_position.Y;
			}
		}
	}

	timer_RandomSound(dtime);
	timer_EnvironmentalDamages(dtime);
	timer_SearchForTarget(dtime);
	tryToFollowTarget(dtime);

	// Handle creature states
	const CreatureStateHandler& stHandle = Creature::state_handler[m_state];
	(this->*stHandle.handler)(dtime);
}

int Creature::punch(v3f dir,
		const ToolCapabilities *toolcap,
		ServerActiveObject *puncher,
		float time_from_last_punch)
{
	int pret = LuaEntitySAO::punch(dir, toolcap, puncher, time_from_last_punch);

	IItemDefManager *itemdefMgr = m_env->getGameDef()->idef();

	ItemStack st = puncher->getWieldedItem();
	ToolCapabilities* tc = st.getDefinition(itemdefMgr).tool_capabilities;
	if (tc) {
		st.addWear((s32) (tc->full_punch_interval / 75 * 9000), itemdefMgr);
		puncher->setWieldedItem(st);
	}

	// Play punch sound
	m_env->getGameDef()->playSound("default_punch",
			floatToInt(puncher->getBasePosition(), BS), 5 * BS);

	// Damages are already handled in LuaEntitySAO, only apply verification
	// to check death & loots
	doDamage(0, puncher);

	v3f pos = m_base_position / BS;
	pos.Y += (m_def->collisionbox[2] + m_def->collisionbox[5]) / 2;

	m_env->spawnEffect(m_base_position, 5, "mobs_blood.png");

	float knockback_factor = toolcap->full_punch_interval;
	if (time_from_last_punch < toolcap->full_punch_interval) {
		knockback_factor = time_from_last_punch / toolcap->full_punch_interval;
	}
	m_knockbacked = true;
	m_knockback_timer = 0.4f * knockback_factor;

	m_velocity = v3f(dir.X * knockback_factor * BS, 0, dir.Z * knockback_factor * BS);
	setCreatureAnimation(CREATUREANIMATION_STAND);
	// @TODO: search other mobs and call them
	return pret;
}

void Creature::rightClick(ServerActiveObject *clicker)
{
	if (!rightClickPrerequisites()) {
		return;
	}

	// Nothing here, it's generally creature specific
}

void Creature::resetAttackTime()
{
	switch (m_def->attacktype) {
		case CREATUREATTACK_DOGFIGHT: m_attack_timer = 1.0f; break;
		case CREATUREATTACK_EXPLODE: m_attack_timer = 3.0f; break;
		case CREATUREATTACK_SHOOT: m_attack_timer = 1.0f; break;
		default:
			logger.error("Unhandled attack type '%d'!", (int)m_def->attacktype);
			assert(false);
			break;
	}
}

const bool Creature::rightClickPrerequisites()
{
	if (!m_registered)
		return false;
	// It's best that attachments cannot be clicked
	return !isAttached();

}

void Creature::timer_RandomSound(const float dtime)
{
	// Every minute each creature has a chance to play a sound if she have
	m_random_sound_timer -= dtime;

	if (m_random_sound_timer <= 0.0f) {
		if (m_def->sounds.find(CREATURESOUND_RANDOM) == m_def->sounds.end())
			return;
		playSound(CREATURESOUND_RANDOM);
		m_random_sound_timer = myrand_range(1, 60) + 30.0f;
	}
}

void Creature::timer_EnvironmentalDamages(const float dtime)
{
	/*
	 * Environmental damages are damages receives by creatures
	 * depending on day time
	 */
	m_environmental_damages_timer -= dtime;

	if (m_environmental_damages_timer <= 0.0f) {
		u32 timeOfDay = m_env->getTimeOfDay();
		// Damages due to light on nightly creatures
		if (m_def->damages.find(CREATUREDAMAGE_LIGHT) != m_def->damages.end() && m_base_position.Y > 0.0f &&
				timeOfDay > MT_SUNRISE && timeOfDay < MT_SUNSET) {
			m_env->spawnEffect(m_base_position, 5, "tnt_smoke.png");
			doDamage(m_def->damages[CREATUREDAMAGE_LIGHT]);
		}

		// Now check on which node is Creature
		v3f pos = m_base_position / BS;
		pos.Y += m_def->collisionbox[2];
		v3s16 pos16(pos.X, pos.Y, pos.Z);

		MapNode node = m_env->getMap().getNode(pos16);
		if (node.getContent() != CONTENT_IGNORE) {
			INodeDefManager* ndef = m_env->getGameDef()->getNodeDefManager();
			pos.Y++;

			// If creature can take damages into water
			if (m_def->damages.find(CREATUREDAMAGE_WATER) != m_def->damages.end() &&
					itemgroup_get(ndef->get(node).groups, "water")) {
				doDamage(m_def->damages[CREATUREDAMAGE_WATER]);
				m_env->spawnEffect(m_base_position, 5, "bubble.png");
			}

			// If creature can take damages into lava or by fire
			if (m_def->damages.find(CREATUREDAMAGE_LAVA) != m_def->damages.end() &&
					(itemgroup_get(ndef->get(node).groups, "lava") ||
					 ndef->get(node).name == "fire:basic_flame")) {
				doDamage(m_def->damages[CREATUREDAMAGE_LAVA]);
				m_env->spawnEffect(m_base_position, 5, "bubble.png");
			}
		}

		m_environmental_damages_timer = 1.0f;
	}
}

void Creature::timer_SearchForTarget(const float dtime)
{
	// If target but target disappear from AO, reinit aggro
	if (m_target_id && !m_env->getActiveObject(m_target_id)) {
		reinitAggro();
	}

	// If we are attacking, we don't need to search for another target
	// If we are not a monster we don't need to search for a target
	if (m_state == CREATURESTATE_ATTACK || (m_def->type != CREATURETYPE_MONSTER &&
		m_def->type != CREATURETYPE_NPC)) {
		return;
	}

	// Reinit current target
	m_target = nullptr;
	u16 last_target_id = m_target_id;
	m_target_id = 0;

	m_searchfortarget_timer -= dtime;
	if (m_searchfortarget_timer <= 0.0f) {
		// @TODO: optimize research by looking for old target validity before searching new

		std::vector<ServerActiveObject*> objects;
		float target_dist = 150000.0f;
		m_env->getObjectsInsideRadius(objects, m_base_position, m_def->viewrange * BS);
		for (ServerActiveObject* obj: objects) {
			if (m_def->type == CREATURETYPE_MONSTER) {
				// Object must be a player or a CREATURETYPE_NPC
				if (obj->getType() != ACTIVEOBJECT_TYPE_PLAYER &&
						obj->getType() != ACTIVEOBJECT_TYPE_LUACREATURE) {
					continue;
				}

				// Object cannot be itself
				if (obj == this) {
					continue;
				}

				if (obj->getType() == ACTIVEOBJECT_TYPE_LUACREATURE) {
					if (((Creature*)obj)->getCreatureType() != CREATURETYPE_NPC) {
						continue;
					}
				}

				// If target is near than previous target, choose it
				v3s16 p;
				float dist = m_base_position.getDistanceFrom(obj->getBasePosition());
				if (m_env->line_of_sight(m_base_position, obj->getBasePosition(), 2, &p) && dist < target_dist) {
					m_target = (UnitSAO*)obj;
					m_target_id = obj->getId();
					target_dist = dist;
				}
			}
			else if (m_def->type == CREATURETYPE_NPC) {
				// @TODO
			}
		}

		// If we have a target, go to state attack
		if (m_target_id != last_target_id &&
				m_state != CREATURESTATE_ATTACK) {
			m_state = CREATURESTATE_ATTACK;
			resetAttackTime();
			playSound(CREATURESOUND_WARCRY);
		}

		m_searchfortarget_timer = 1.0f;
	}
}

void Creature::tryToFollowTarget(const float dtime)
{
	if (!m_target_id || !m_target) {
		return;
	}

	v3f pos = m_target->getBasePosition();
	f32 dist = pos.getDistanceFrom(m_base_position) / BS;

	// If too far, reinit target
	if (dist > m_def->viewrange) {
		m_target_id = 0;
		m_target = nullptr;
		return;
	}

	v3f vec((m_base_position.X - pos.X) / BS, (m_base_position.Y - pos.Y) / BS, (m_base_position.Z - pos.Z) / BS);
	float yaw = (atan(vec.Z / vec.X) + M_PI / 2) /* + m_rotate */;
	if (pos.X > m_base_position.X) {
		yaw += M_PI;
	}
	m_yaw = yaw * core::RADTODEG;

	// Dogfight should follow if > 1.9
	// Explode should follow if > 3.0
	if ((m_def->attacktype == CREATUREATTACK_DOGFIGHT && dist > 1.9f) ||
			(m_def->attacktype == CREATUREATTACK_EXPLODE && dist > 3.0f) ||
			(m_def->attacktype == CREATUREATTACK_SHOOT && dist > 8.0f)) {

		// If creature is exploding creature, stop trying to explode
		if (m_def->attacktype == CREATUREATTACK_EXPLODE) {
			setTextureMod("");
			resetAttackTime();
			m_standing_for_attack = false;
		}

		if (m_def->can_jump && getCreatureVelocity() <= 0.5 &&
				m_velocity.Y == 0) {
			doJump(m_yaw);
		}

		// If creature is in attacking state, run
		if (m_state == CREATURESTATE_ATTACK) {
			setCreatureVelocity(m_def->walk_velocity * 2.5);
		}
		else {
			setCreatureVelocity(m_def->walk_velocity);
		}

		if (m_def->walk_chance != 0) {
			setCreatureAnimation((m_state == CREATURESTATE_ATTACK) ? CREATUREANIMATION_RUN : CREATUREANIMATION_WALK);
		}
	}
	else {
		doStand();
	}

}

bool Creature::castSpell(const u32 spellId, v3f range_pos, float y_cast_offset) const
{
	y_cast_offset = (m_def->collisionbox[2] + m_def->collisionbox[5]) / 2 * BS - 1;
	return UnitSAO::castSpell(spellId, range_pos, y_cast_offset);
}

void Creature::handleState_Stand(const float dtime)
{
	m_stand_timer -= dtime;
	if (m_stand_timer > 0.0f) {
		return;
	}

	// Stand 1.0 -> 2.0 secs
	m_stand_timer = myrand_range(10, 20) / 10.0f;

	if (myrand_range(1, 4) == 1) {
		v3f found_pos = v3f(0,0,0);

		if (m_def->type == CREATURETYPE_NPC) {
			// @TODO we can optimize by cache this AO when searching for a target
			std::vector<ServerActiveObject*> objects;

			m_env->getObjectsInsideRadius(objects, m_base_position, 3.0f * BS);
			for (ServerActiveObject* obj: objects) {
				if (obj->getType() != ACTIVEOBJECT_TYPE_PLAYER) {
					continue;
				}

				found_pos = obj->getBasePosition();
			}
		}

		float yaw = 0.0f;
		if (found_pos != v3f(0,0,0)) {
			v3f v(found_pos.X - m_base_position.X, found_pos.Y - m_base_position.Y,
				  found_pos.Z - m_base_position.Z);
			yaw = (atan(v.Z / v.X) + M_PI / 2) + 0.0f; // @TODO 0.0f => self.rotate
			if (found_pos.X > m_base_position.X) {
				yaw += M_PI;
			}
			m_yaw = yaw * core::RADTODEG;
		}
		// Don't change direction to often
		else if (myrand_range(1, 100) < 30) {
			m_yaw += myrand_range(0, 359) - 180;
		}

	}

	setCreatureVelocity(0.0f);
	setCreatureAnimation(CREATUREANIMATION_STAND);

	if (m_def->type == CREATURETYPE_NPC) {
		if (m_state != CREATURESTATE_STAND) {
			m_state = CREATURESTATE_STAND;
		}
	}
	else {
		if (m_def->walk_chance && myrand_range(1, 100) > m_def->walk_chance) {
			doWalk();
		}
	}
}

void Creature::handleState_Attack(const float dtime)
{
	// No target, do nothing
	if (!m_target_id || !m_target) {
		reinitAggro();
		return;
	}

	v3f target_pos = m_target->getBasePosition();
	float dist = m_base_position.getDistanceFrom(target_pos) / BS;

	// Out of range or target dead, cancel aggro
	if (dist > m_def->viewrange || m_target->getHP() <= 0) {
		reinitAggro();
		return;
	}

	v3f vec(target_pos.X - m_base_position.X, target_pos.Y - m_base_position.Y,
			target_pos.Z - m_base_position.Z);
	float yaw = (atan(vec.Z / vec.X) + M_PI / 2) /* + self.rotate */;
	if (target_pos.X > m_base_position.X) {
		yaw += M_PI;
	}
	m_yaw = yaw * core::RADTODEG;

	v3s16 pos16 = floatToInt(m_base_position, BS);

	if (m_def->attacktype == CREATUREATTACK_EXPLODE) {
		if (m_standing_for_attack) {
			m_attack_timer -= dtime;
			m_blink_timer -= dtime;
			if (m_blink_timer <= 0.0f) {
				m_blink = !m_blink;
				setTextureMod(m_blink ? "^[brighten" : "");
				// Faster blink if 1 sec remaining
				m_blink_timer = (m_attack_timer <= 1.0f ? 0.1f : 0.2f);
			}

			if (m_attack_timer <= 0.0f) {
				pos16.Y--;
				doExplosion(pos16, 3.0f);
			}
		}
	}
	else if (m_def->attacktype == CREATUREATTACK_DOGFIGHT) {
		m_attack_timer -= dtime;
		// Fly velocity
		if (m_can_fly && dist > 2.0f) {
			u16 y = floor(m_base_position.Y);
			u16 t_y = floor(target_pos.Y) + 1;
			v3f vel = m_velocity;

			MapNode node = m_env->getMap().getNode(pos16);

			if (node.getContent() != CONTENT_IGNORE /* && node.name == self.fly_in */) {
				if (y < t_y) {
					m_velocity = v3f(vel.X, m_def->walk_velocity, vel.Z);
				}
				else if (y > t_y) {
					m_velocity = v3f(vel.X, -m_def->walk_velocity, vel.Z);
				}
			}
			else {
				if (y < t_y) {
					m_velocity = v3f(vel.X, 0.01f, vel.Z);
				}
				else if (y > t_y) {
					m_velocity = v3f(vel.X, -0.01f, vel.Z);
				}
			}
		}

		if (dist > ((m_def->collisionbox[1] + m_def->collisionbox[4]) / 2) + 2) {
			// @TODO
		}
		else {
			setCreatureVelocity(0.0f);
			if (m_attack_timer <= 0.0f) {
				v3f mypos = m_base_position;
				v3f tarpos = target_pos;
				mypos.Y += 1.5f;
				tarpos.Y += 1.5f;

				v3s16 p;
				if (m_env->line_of_sight(tarpos, mypos, 1.0f, &p)) {
					playSound(CREATURESOUND_ATTACK);
					setCreatureAnimation(CREATUREANIMATION_PUNCH);

					m_target->punch(tarpos - mypos, &m_toolcap, this, 1.0f);
					if (m_target->getType() == ACTIVEOBJECT_TYPE_PLAYER) {
						m_env->getGameDef()->SendPlayerHPOrDie((PlayerSAO *)m_target);
					}

					// Target is dead, unaggro
					if (m_target->getHP() <= 0.0f) {
						reinitAggro();
					}
				}

				m_attack_timer = 1.0f;
			}
		}
	}
	else if (m_def->attacktype == CREATUREATTACK_SHOOT) {
		for (auto &timer: m_spell_timers) {
			timer.second -= dtime;
			if (timer.second <= 0.0f) {
				timer.second = m_def->spells[timer.first]->timer;
				playSound(CREATURESOUND_ATTACK);
				setCreatureAnimation(CREATUREANIMATION_PUNCH);
				// Spell casted, don't cast another spell for this cycle
				if (castSpell(timer.first, m_target->getBasePosition())) {
					break;
				}
			}
		}
	}
}

void Creature::handleState_Walk(const float dtime)
{
	m_walk_timer -= dtime;
	if (m_walk_timer > 0) {
		return;
	}

	m_walk_timer = myrand_range(5, 20) / 10.0f;

	// Equivalent to find_node_near api call
	INodeDefManager* ndef = m_env->getGameDef()->getNodeDefManager();
	std::set<content_t> filter;
	ndef->getIds("group:water", filter);
	v3s16 node(0,0,0);
	v3s16 pos16(m_base_position.X / BS, m_base_position.Y / BS, m_base_position.Z / BS);
	for(int d=1; d<=1 && node == v3s16(0,0,0); d++){
		std::vector<v3s16> list = FacePositionCache::getFacePositions(d);
		for (auto &i: list) {
			v3s16 p = pos16 + i;
			content_t c = m_env->getMap().getNode(p).getContent();
			if(filter.count(c) != 0){
				node = p;
			}
		}
	}

	if (m_can_fly /* TODO && self.fly_in == "default:water_source"*/ && node == v3s16(0,0,0)) {
		setCreatureVelocity(0.0f);
		m_state = CREATURESTATE_FLOP;
		setCreatureAnimation(CREATUREANIMATION_STAND);
		return;
	}

	if (node != v3s16(0,0,0)) {
		v3f vec(node.X - m_base_position.X / BS, node.Y - m_base_position.Y / BS, node.Z - m_base_position.Z / BS);
		float yaw = atan(vec.Z / vec.X) + 3 * M_PI / 2 /* + self.rotate */;
		if (m_base_position.X / BS > node.X) {
			yaw += M_PI;
		}
		m_yaw = yaw * core::RADTODEG;
	}
	// Don't change direction too often
	else if (myrand_range(1, 100) < 30) {
		m_yaw += myrand_range(0, 359) - 180;
	}

	if (m_def->can_jump) {
		doJump(m_yaw);
	}

	doWalk();

	if (myrand_range(1, 100) < 30) {
		doStand();
	}
}

void Creature::handleState_Flop(const float dtime)
{

}

void Creature::reinitAggro()
{
	doStand();
	m_target_id = 0;
	m_target = nullptr;
	setTextureMod("");
	m_standing_for_attack = false;
	m_state = CREATURESTATE_STAND;
	resetAttackTime();
}

void Creature::doDamage(u16 damage, ServerActiveObject *puncher) {
	setHP(m_hp - damage);

	// If we set 0 damage it's because this is already handled by
	// LuaEntitySAO
	if (damage != 0) {
		std::string str = gob_cmd_punched(damage, m_hp);
		// create message and add to list
		ActiveObjectMessage aom(getId(), true, str);
		m_messages_out.push(aom);
	}

	if (m_hp > 0) {
		playSound(CREATURESOUND_DAMAGE);
		return;
	}

	m_removed = true;

	if(puncher && puncher->getType() == ACTIVEOBJECT_TYPE_PLAYER) {
		PlayerSAO *player = (PlayerSAO*)puncher;
		m_env->getGameDef()->applyAwardStep(player, m_init_name, epixel::ACHIEVEMENT_KILL_CREATURE);

		if (g_settings->get(BOOLSETTING_ENABLE_MOD_XP)) {
			player->modifyXP(myrand_range(m_def->xp_min, m_def->xp_max));
			player->updateHUD_Level();
		}
	}

	// Awards

	if (m_def->drops) {
		for (const auto &drop: *m_def->drops) {
			if (myrand_range(1,drop.chance) == 1) {
				ItemStack loot;
				loot.deSerialize(drop.item);
				loot.add(myrand_range(drop.min, drop.max));

				// Drop item on the floor
				if (epixel::ItemSAO* obj = m_env->spawnItemActiveObject(drop.item, m_base_position, loot)) {
					obj->setVelocity(v3f(myrand_range(-1, 1) * BS, 5 * BS, myrand_range(-1, 1) * BS));
				}
			}
		}
	}

	playSound(CREATURESOUND_DEATH);
	// @TODO: died callback or variable
}

void Creature::doJump(const float yaw)
{
	// If creature can fly, don't jump
	if (m_can_fly) {
		return;
	}

	v3f direction = v3f(sin(yaw / core::RADTODEG) * -1, -20, cos(yaw / core::RADTODEG));
	v3s16 pos16(round(m_base_position.X / BS), round(m_base_position.Y / BS), round(m_base_position.Z / BS));

	INodeDefManager* ndef = m_env->getGameDef()->getNodeDefManager();
	MapNode m = m_env->getMap().getNode(pos16);
	content_t c = m.getContent();
	// If current node isn't loaded or no direction choosen do nothing
	if (c == CONTENT_IGNORE || direction == v3f(0,0,0)) {
		return;
	}

	// Look at node in creature's direction
	v3s16 posdir16(pos16.X + direction.X, pos16.Y, pos16.Z + direction.Z);
	MapNode node = m_env->getMap().getNode(posdir16);
	c = node.getContent();

	// Don't try to jump on non loaded mapblock
	if (c == CONTENT_IGNORE) {
		return;
	}

	// If targeted node is air
	if (c == CONTENT_AIR) {
		v3s16 posdir16_above = posdir16, posdir16_under = posdir16;
		posdir16_above.Y += 1;
		posdir16_under.Y -= 1;
		MapNode nodeabove = m_env->getMap().getNode(posdir16_above),
				nodeunder = m_env->getMap().getNode(posdir16_under);

		const ContentFeatures &f_under = ndef->get(nodeunder.getContent());
		// And above node is air too, and under is walkable
		// We can jump
		if (nodeabove.getContent() == CONTENT_AIR && f_under.walkable) {
			m_velocity.Y = (m_def->jump_height) * BS;
			m_velocity.X *= 2.2;
			m_velocity.Z *= 2.2;
			playSound(CREATURESOUND_JUMP);
		}
	}
}

void Creature::doWalk()
{
	setCreatureVelocity(m_def->walk_velocity);
	if (m_state != CREATURESTATE_ATTACK) {
		m_state = CREATURESTATE_WALK;
	}
	setCreatureAnimation(CREATUREANIMATION_WALK);
}

void Creature::doStand()
{
	m_velocity = v3f(0,0,0);
	// If creature is not attacking a player, STAND
	if (m_state != CREATURESTATE_ATTACK) {
		m_state = CREATURESTATE_STAND;
	}
	// Else if it's a exploding creature, STAND_ATTACK
	// and play ignite sound
	else if (m_state == CREATURESTATE_ATTACK &&
			 m_def->attacktype == CREATUREATTACK_EXPLODE &&
			 !m_standing_for_attack) {
		SimpleSoundSpec spec;
		spec.name = "tnt_ignite";

		ServerSoundParams params;
		params.object = getId();
		params.type = ServerSoundParams::Type::SSP_OBJECT;
		params.max_hear_distance = 64 * BS;

		m_env->getGameDef()->playSound(spec, params);
		m_standing_for_attack = true;
	}
	setCreatureAnimation(CREATUREANIMATION_STAND);
	m_stand_timer = 1.0f;
}

void Creature::doExplosion(const v3s16& pos, const float radius)
{
	u32 player_id = 0;
	if (m_target->getType() == ACTIVEOBJECT_TYPE_PLAYER) {
		player_id = ((PlayerSAO*)m_target)->getPlayer()->getDBId();
	}

	if (m_env->makeExplosion(pos, radius, getId(), player_id)) {
		m_removed = true;
	}
	else {
		m_attack_timer = 3.0f;
	}
}

void Creature::playSound(CreatureSound s) const
{
	if (m_def->sounds.find(s) == m_def->sounds.end())
		return;

	SimpleSoundSpec spec;
	spec.name = m_def->sounds[s];

	ServerSoundParams params;
	params.object = getId();
	params.type = ServerSoundParams::Type::SSP_OBJECT;
	params.max_hear_distance = CREATURESOUND_MAX_DISTANCE * BS;

	m_env->getGameDef()->playSound(spec, params);
}

void Creature::setCreatureAnimation(const CreatureAnimation c)
{
	if (m_current_animation == c) {
		return;
	}

	m_current_animation = c;

	switch (c) {
	case CREATUREANIMATION_STAND:
		if (m_def->animation_infos[CREATUREANIMATIONINFOS_STAND_START] ||
				m_def->animation_infos[CREATUREANIMATIONINFOS_STAND_END]) {
			setAnimation(v2f(m_def->animation_infos[CREATUREANIMATIONINFOS_STAND_START],
					m_def->animation_infos[CREATUREANIMATIONINFOS_STAND_END]),
					m_def->animation_infos[CREATUREANIMATIONINFOS_SPEED_NORMAL], 0, true);
		}
		break;
	case CREATUREANIMATION_WALK:
		if (m_def->animation_infos[CREATUREANIMATIONINFOS_WALK_START] ||
				m_def->animation_infos[CREATUREANIMATIONINFOS_WALK_END]) {
			setAnimation(v2f(m_def->animation_infos[CREATUREANIMATIONINFOS_WALK_START],
					m_def->animation_infos[CREATUREANIMATIONINFOS_WALK_END]),
					m_def->animation_infos[CREATUREANIMATIONINFOS_SPEED_NORMAL], 0, true);
		}
		break;
	case CREATUREANIMATION_RUN:
		if (m_def->animation_infos[CREATUREANIMATIONINFOS_RUN_START] ||
				m_def->animation_infos[CREATUREANIMATIONINFOS_RUN_END]) {
			setAnimation(v2f(m_def->animation_infos[CREATUREANIMATIONINFOS_RUN_START],
				m_def->animation_infos[CREATUREANIMATIONINFOS_RUN_END]),
				m_def->animation_infos[CREATUREANIMATIONINFOS_SPEED_RUN], 0, true);
		}
		break;
	case CREATUREANIMATION_PUNCH:
		if (m_def->animation_infos[CREATUREANIMATIONINFOS_PUNCH_START] ||
				m_def->animation_infos[CREATUREANIMATIONINFOS_PUNCH_END]) {
			setAnimation(v2f(m_def->animation_infos[CREATUREANIMATIONINFOS_PUNCH_START],
					m_def->animation_infos[CREATUREANIMATIONINFOS_PUNCH_END]),
					m_def->animation_infos[CREATUREANIMATIONINFOS_SPEED_NORMAL], 0, true);
		}
		break;
	default:
		break;
	}
}

void Creature::setCreatureVelocity(const float vel)
{
	// @TODO drawtype
	float yaw = m_yaw / core::RADTODEG /* + self.rotate */;
	m_velocity = v3f(sin(yaw) * -vel, m_velocity.Y, cos(yaw) * vel);
}

float Creature::getCreatureVelocity()
{
	return pow(pow(m_velocity.X, 2) + pow(m_velocity.Z, 2), 0.5);
}

void Creature::initCreature() {
	INodeDefManager* ndef = m_env->getGameDef()->getNodeDefManager();

	// Get definition from CreatureStore
	CreatureStore* cs = m_env->getGameDef()->getCreatureStore();
	m_def = cs->getDefinition(m_init_name);

	m_state = CREATURESTATE_WALK;
	m_life_timer = 180;

	if (m_def->damage_min > 0) {
		m_toolcap.damageGroups["fleshy"] = m_def->damage_min;
	}

	m_replace_mapper.clear();

	// Init mob replace mapper by cache content_t ids
	if (m_def->replacelist) {
		for (auto &elem: *m_def->replacelist) {
			content_t src = CONTENT_IGNORE, dst = CONTENT_IGNORE;
			ndef->getId(elem.first, src);
			ndef->getId(elem.second, dst);
			if (src != CONTENT_IGNORE && dst != CONTENT_IGNORE) {
				m_replace_mapper[src] = dst;
			}
		}
	}

	m_hp = (s16) myrand_range(m_def->hp_min, m_def->hp_max);
	m_yaw = myrand_range(1, 360);

	ItemGroupList groups;
	groups["fleshy"] = m_def->armor;
	setArmorGroups(groups);

	m_prop.visual_size = m_def->visualsize;

	m_prop.collisionbox = core::aabbox3d<f32>(m_def->collisionbox[0],
			m_def->collisionbox[1], m_def->collisionbox[2], m_def->collisionbox[3],
			m_def->collisionbox[4], m_def->collisionbox[5]);

	if (m_def->meshlist.size() > 0) {
		m_prop.mesh = m_def->meshlist[0];
	}

	// Set a random texture
	if (m_def->texturelist.size() > 0) {
		m_prop.textures.clear();
		m_prop.textures.push_back(m_def->texturelist[myrand_range(0, m_def->texturelist.size() - 1)]);
	}

	m_prop.visual = "mesh";

	// @TODO make customizable
	m_prop.stepheight = 6.0f;
	m_prop.physical = true;

	notifyObjectPropertiesModified();

	m_spell_timers.clear();
	for (const auto &spell: m_def->spells) {
		m_spell_timers[spell.first] = spell.second->timer;
	}
}

/**
 * @brief ChatHandler::handleCommand_creature_spawn
 * @param args
 * @param peer_id
 * @return true
 *
 * Spawn a creature with the given name
 */
bool ChatHandler::handleCommand_creature_spawn(const std::string &args, const u16 peer_id)
{
	std::vector<std::string> commandline;
	std::stringstream ss(args);
	std::string item;
	while (std::getline(ss, item, ' ')) {
		commandline.push_back(item);
	}

	if (commandline.size() != 1) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help creature spawn");
		return true;
	}

	PlayerSAO* sao = m_server->getPlayerSAO(peer_id);
	if (!sao) {
		return true;
	}

	ServerEnvironment* env = &m_server->getEnv();
	v3f pos = sao->getBasePosition();
	pos.Y += BS;
	epixel::Creature *obj = epixel::Creature::createCreatureObj(env, pos, commandline[0]);
	env->addActiveObject(obj);
	return true;
}

}
