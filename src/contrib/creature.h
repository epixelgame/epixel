/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#include "irr_v3d.h"
#include <string>
#include <vector>
#include "contrib/creaturestore.h"
#include "mapnode.h"
#include "content_sao.h"
#include "tool.h"

namespace epixel {

/**
 * @brief State of the creature defining its comportment
 */
enum CreatureState {
	CREATURESTATE_NONE,
	CREATURESTATE_STAND,
	CREATURESTATE_ATTACK,
	CREATURESTATE_WALK,
	CREATURESTATE_FLOP,
	CREATURESTATE_MAX,
};

/**
 * @brief Creature different animations
 */
enum CreatureAnimation {
	CREATUREANIMATION_STAND,
	CREATUREANIMATION_WALK,
	CREATUREANIMATION_RUN,
	CREATUREANIMATION_PUNCH,
	CREATUREANIMATION_MAX,
};

#define CREATURESOUND_MAX_DISTANCE 15

class Creature;
struct CreatureStateHandler
{
	CreatureState state;
	void (Creature::*handler)(float dtime);
};

#define MT_SUNRISE 5800
#define MT_SUNSET 18500

/**
 * @brief The Creature class is derived from LuaEntitySAO
 * Creature class uses some LuaEntitySAO specificities
 * but ignore many Lua callbacks except the objectref creation
 * and deletion.
 */
class Creature: public LuaEntitySAO {
public:
	Creature(ServerEnvironment *env, v3f pos,
			const std::string &name, const std::string &state);
	~Creature();

	ActiveObjectType getType() const
	{ return ACTIVEOBJECT_TYPE_LUACREATURE; }

	static ServerActiveObject* create(ServerEnvironment *env, const v3f& pos,
			const std::string &data);
	static Creature* createCreatureObj(ServerEnvironment *env, v3f pos,
			const std::string &name, const std::string &state = "");

	bool collideWithObjects() { return LuaEntitySAO::collideWithObjects(); }
	bool getCollisionBox(aabb3f *toset) { return LuaEntitySAO::getCollisionBox(toset); }

	bool isStaticAllowed() const { return false; }

	const CreatureType getCreatureType() const { return m_def->type; }
	void setCreatureState(const CreatureState s) { m_state = s; }
	void setTamed(const bool t) { m_tamed = t; }

	virtual void step(float dtime, bool send_recommended);
	virtual void rightClick(ServerActiveObject *clicker);
	const bool rightClickPrerequisites();

	virtual void addedToEnvironment(u32 dtime_s);

	int punch(v3f dir,
			const ToolCapabilities *toolcap = NULL,
			ServerActiveObject *puncher = NULL,
			float time_from_last_punch = 1000000);

protected:
	void playSound(CreatureSound s) const;
	virtual void doDamage(u16 damage, ServerActiveObject *puncher = NULL);
	virtual bool castSpell(const u32 spellId, v3f range_pos, float y_cast_offset = 0.0f) const;
private:
	// Functions
	void doJump(const float yaw);
	void doWalk();
	void doStand();
	void doExplosion(const v3s16& pos, const float radius);

	void setCreatureAnimation(const CreatureAnimation c);
	void setCreatureVelocity(const float vel);
	float getCreatureVelocity();
	void reinitAggro();
	void resetAttackTime();
	void tryToFollowTarget(const float dtime);

	// Timers
	void timer_RandomSound(const float dtime);
	void timer_EnvironmentalDamages(const float dtime);
	void timer_SearchForTarget(const float dtime);
	// State handlers
	void handleState_None(const float dtime) {}
	void handleState_Stand(const float dtime);
	void handleState_Attack(const float dtime);
	void handleState_Walk(const float dtime);
	void handleState_Flop(const float dtime);

	void initCreature();

	// Creature attribute
	CreatureState m_state;
	bool m_child; // @TODO: lua setter
	bool m_tamed;
	bool m_can_fly;
	bool m_can_attack_monsters;
	bool m_blink;
	bool m_knockbacked;
	bool m_standing_for_attack;
	std::map<content_t, content_t> m_replace_mapper;
	UnitSAO* m_target;
	u16 m_target_id;
	ToolCapabilities m_toolcap;
	CreatureDef* m_def;

	// Timers
	float m_life_timer;
	float m_random_sound_timer;
	float m_environmental_damages_timer;
	float m_searchfortarget_timer;
	float m_knockback_timer;
	float m_attack_timer;
	float m_walk_timer;
	float m_stand_timer;
	float m_blink_timer;
	std::unordered_map<u32, float> m_spell_timers;

	float m_old_y;
	CreatureAnimation m_current_animation;

	static const CreatureStateHandler state_handler[CREATURESTATE_MAX];
};

}
