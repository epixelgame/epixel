/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


#include "cow.h"
#include "contrib/itemsao.h"
#include "environment.h"
#include "server.h"

/**
 * Cow are passive creatures.
 * They can be sheared using the appropriated tool
 * When a sheep is sheared, give
 */

namespace epixel
{
namespace creature
{

#define FOOD_BEFORE_MILK 15

void Cow::rightClick(ServerActiveObject *clicker)
{
	if (!rightClickPrerequisites())
		return;

	ItemStack hand_item = clicker->getWieldedItem();
	// Handle shears item
	if (m_has_milk && hand_item.name.compare("bucket:bucket_empty") == 0) {
		ItemStack item;
		item.deSerialize("mobs:bucket_milk");
		clicker->setWieldedItem(item);
		m_has_milk = false;
	}

	if (!m_has_milk && hand_item.name.compare("farming:wheat") == 0) {
		hand_item.takeItem(1);
		clicker->setWieldedItem(hand_item);
		m_food_count++;
		if (m_food_count == FOOD_BEFORE_MILK) {
			m_food_count = 0;
			m_has_milk = true;
			playSound(CREATURESOUND_RANDOM);
		}
	}
}

void Cow::step(float dtime, bool send_recommended)
{
	Creature::step(dtime, send_recommended);

	// Consume food over time
	if (m_food_count > 0) {
		m_foot_eat_timer -= dtime;
		if (m_foot_eat_timer <= 0.0f) {
			m_food_count--;
			m_foot_eat_timer = 30.0f;
		}
	}
}

}
}
