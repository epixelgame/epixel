/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


#include "sheep.h"
#include "server.h"
#include "contrib/itemsao.h"
#include "environment.h"

/**
 * Sheep are passive creatures.
 * They can be sheared using the appropriated tool
 * When a sheep is sheared, give
 */

namespace epixel
{
namespace creature
{

#define FOOD_BEFORE_WOOL 20

void Sheep::rightClick(ServerActiveObject *clicker)
{
	if (!rightClickPrerequisites())
		return;

	IItemDefManager* idef = m_env->getGameDef()->idef();

	ItemStack hand_item = clicker->getWieldedItem();
	// Handle shears item
	if (!m_sheared && hand_item.name.compare("mobs:shears") == 0) {
		ItemStack item;
		item.deSerialize("wool:white");
		item.add(myrand_range(1,2));

		// Use a little bit the item
		hand_item.addWear(650, idef);
		clicker->setWieldedItem(hand_item);

		// Drop item on the floor
		if (epixel::ItemSAO* obj = m_env->spawnItemActiveObject("wool:white", m_base_position, item)) {
			obj->setVelocity(v3f(myrand_range(-1, 1) * BS, 5 * BS, myrand_range(-1, 1) * BS));
		}

		m_sheared = true;
		m_wool_unsheared_timer = 300.0f;

		m_prop.textures = {"mobs_sheep_shaved.png"};
		m_prop.mesh = "mobs_sheep_shaved.b3d";
		notifyObjectPropertiesModified();
	}

	if (hand_item.name.compare("farming:wheat") == 0) {
		hand_item.takeItem(1);
		clicker->setWieldedItem(hand_item);
		m_food_count++;
		if (m_food_count == FOOD_BEFORE_WOOL) {
			reinitSheepWool();
			playSound(CREATURESOUND_RANDOM);
		}
	}
}

void Sheep::step(float dtime, bool send_recommended)
{
	Creature::step(dtime, send_recommended);

	// After a period of time sheep retrieve its wool
	if (m_sheared) {
		m_wool_unsheared_timer -= dtime;
		if (m_wool_unsheared_timer <= 0.0f) {
			reinitSheepWool();
		}
	}

	// Consume food over time
	if (m_food_count > 0) {
		m_foot_eat_timer -= dtime;
		if (m_foot_eat_timer <= 0.0f) {
			m_food_count--;
			m_foot_eat_timer = 30.0f;
		}
	}
}

void Sheep::reinitSheepWool()
{
	m_sheared = false;
	m_food_count = 0;
	m_prop.textures = {"mobs_sheep.png"};
	m_prop.mesh = "mobs_sheep.b3d";
	notifyObjectPropertiesModified();
}

}
}
