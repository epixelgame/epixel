/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#include "contrib/creature.h"

namespace epixel
{

namespace creature
{

class Sheep: public Creature
{
public:
	Sheep(ServerEnvironment *env, v3f pos,
		const std::string &name, const std::string &state):
		Creature(env, pos, name, state),
		m_sheared(false), m_wool_unsheared_timer(0.0f),
		m_foot_eat_timer(30.0f), m_food_count(0) {}
	virtual void rightClick(ServerActiveObject *clicker);
	virtual void step(float dtime, bool send_recommended);
private:
	void reinitSheepWool();

	bool m_sheared;
	float m_wool_unsheared_timer;
	float m_foot_eat_timer;
	u32 m_food_count;
};

}

}

