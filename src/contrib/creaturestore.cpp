/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "contrib/creaturestore.h"
#include "database.h"
#include "log.h"

namespace epixel {

CreatureDef::CreatureDef():
	type(CREATURETYPE_NONE), attacktype(CREATUREATTACK_NONE), can_float(true),
	can_jump(true), can_take_fall_damage(true),
	viewrange(15.0f), jump_height(6.5f), replaceoffset(0),
	walk_chance(50.0f), walk_velocity(10.0f), fall_speed(100.0f),
	replacerate(0), armor(0), hp_min(5), hp_max(10), drops(NULL), replacelist(NULL)
{
	collisionbox.reserve(6);
	for (u8 i = 0; i < 6; i++) {
		collisionbox[i] = 0.0f;
	}
	texturelist.clear();
	meshlist.clear();
	visualsize = v2f(1,1);
}

CreatureStore::CreatureStore(GameDatabase *db):
	m_database(db)
{
	m_definitions.clear();
	m_definitions["default_definition"] = new CreatureDef();
	m_drops.clear();
}

CreatureStore::~CreatureStore()
{
	clear();
}

void CreatureStore::clear()
{
	for (auto &def: m_definitions) {
		delete def.second;
	}

	for (auto &loot: m_drops) {
		delete loot.second;
	}

	for (auto &nr: m_node_replace) {
		delete nr.second;
	}
}

void CreatureStore::Load(std::unordered_map<u32,Spell*> spells)
{
	clear();

	logger.notice("Load Creature Loots...");
	u32 loot_nb = 0;
	m_database->loadLoots(m_drops, loot_nb);
	logger.notice("%d creature loots loaded (%d loot tables)", loot_nb, m_drops.size());

	logger.notice("Load Creatures nodes to replace...");
	u32 replace_nb = 0;
	m_database->loadCreatureNodeReplace(m_node_replace, replace_nb);
	logger.notice("%d creature nodes to replace loaded (%d nodes to replace tables)",
			replace_nb, m_node_replace.size());

	m_definitions.clear();
	m_definitions["default_definition"] = new CreatureDef();

	logger.notice("Loading Creatures definitions...");
	u32 meshes_nb = 0, textures_nb = 0;
	m_database->loadCreatureDefinitions(m_definitions, this, meshes_nb, textures_nb, spells);
	logger.notice("%d creature definitions loaded (including %d textures defs and %d meshes defs).",
			(m_definitions.size() - 1), textures_nb, meshes_nb);
}

CreatureDef* CreatureStore::getDefinition(const std::string &creature)
{
	CreatureDefMap::const_iterator it = m_definitions.find(creature);
	if (it == m_definitions.end()) {
		return m_definitions["default_definition"];
	}

	return (*it).second;
}

CreatureDropList* CreatureStore::getDropList(u32 id) const
{
	CreatureDropMap::const_iterator it = m_drops.find(id);
	return (it != m_drops.end() ? (*it).second : NULL);
}

CreatureNodeReplace* CreatureStore::getNodeReplaceList(u32 id) const
{
	CreatureNodeReplaceMap::const_iterator it = m_node_replace.find(id);
	return (it != m_node_replace.end() ? (*it).second : NULL);
}

}
