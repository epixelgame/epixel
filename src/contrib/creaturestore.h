/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#include <map>
#include <vector>
#include <string>
#include <unordered_map>
#include "irr_v3d.h"
#include "irr_v2d.h"

class GameDatabase;

namespace epixel {

struct Spell;

/**
 * @brief Creature types defined the general comportment of Creatures
 */
enum CreatureType {
	CREATURETYPE_NONE,
	CREATURETYPE_ANIMAL,
	CREATURETYPE_MONSTER,
	CREATURETYPE_NPC,
};

/**
 * @brief Each creature has a main attack type which define the animation
 * and the effect on player/zone
 */
enum CreatureAttackType {
	CREATUREATTACK_NONE,
	CREATUREATTACK_EXPLODE,
	CREATUREATTACK_DOGFIGHT,
	CREATUREATTACK_SHOOT,
};

/**
 * @brief Define index arrays of different sounds which can be played
 * by creatures during their life
 */
enum CreatureSound {
	CREATURESOUND_WARCRY,
	CREATURESOUND_RANDOM,
	CREATURESOUND_ATTACK,
	CREATURESOUND_DEATH,
	CREATURESOUND_DAMAGE,
	CREATURESOUND_JUMP,
	CREATURESOUND_MAX,
};

enum CreatureDamageType {
	CREATUREDAMAGE_LIGHT,
	CREATUREDAMAGE_WATER,
	CREATUREDAMAGE_LAVA,
	CREATUREDAMAGE_FALL,
	CREATUREDAMAGE_MAX,
};

typedef std::map<CreatureDamageType, u16> DamageMap;

/**
 * @brief The CreatureAnimationInfos enum
 * Those informations are fetched from database and permit to
 * run different animations based on comportment
 */
enum CreatureAnimationInfos {
	CREATUREANIMATIONINFOS_SPEED_NORMAL,
	CREATUREANIMATIONINFOS_STAND_START,
	CREATUREANIMATIONINFOS_STAND_END,
	CREATUREANIMATIONINFOS_WALK_START,
	CREATUREANIMATIONINFOS_WALK_END,
	CREATUREANIMATIONINFOS_RUN_START,
	CREATUREANIMATIONINFOS_RUN_END,
	CREATUREANIMATIONINFOS_SPEED_RUN,
	CREATUREANIMATIONINFOS_PUNCH_START,
	CREATUREANIMATIONINFOS_PUNCH_END,
	CREATUREANIMATIONINFOS_MAX
};

/**
 * @brief A single drop entry when creature die
 */
struct CreatureDrop {
	CreatureDrop(const std::string& i, u8 c, u8 m, u8 m2) {
		item = i; chance = c; min = m; max = m2;
	}

	std::string item;
	u8 chance;
	u8 min;
	u8 max;
};

typedef std::vector<CreatureDrop> CreatureDropList;
typedef std::unordered_map<u32, CreatureDropList*> CreatureDropMap;
typedef std::map<CreatureSound, std::string> SoundMap;

typedef std::unordered_map<std::string, std::string> CreatureNodeReplace;
typedef std::unordered_map<u32, CreatureNodeReplace*> CreatureNodeReplaceMap;

/**
 * @brief The CreatureDef struct
 * This structure contain the whole definition of the Creature.
 * It's loaded from database and each creature fetch the definition
 * depending on its type and point to the def
 */
struct CreatureDef
{
public:
	CreatureDef();

	CreatureType type;
	CreatureAttackType attacktype;

	bool can_float;
	bool can_jump;
	bool can_take_fall_damage;

	float viewrange;
	float jump_height;
	float replaceoffset;
	float walk_chance;
	float walk_velocity;
	float fall_speed;

	u32 replacerate;
	u32 armor;

	u16 damage_min;
	u16 hp_min;
	u16 hp_max;
	u32 xp_min;
	u32 xp_max;

	std::string replace_with;

	v2f visualsize;

	std::vector<float> collisionbox;
	SoundMap sounds;
	DamageMap damages;
	CreatureDropList* drops;
	CreatureNodeReplace* replacelist;
	std::vector<std::string> meshlist;
	std::vector<std::string> texturelist;
	std::unordered_map<u32,Spell*> spells;

	u16 animation_infos[CREATUREANIMATIONINFOS_MAX];
};

typedef std::unordered_map<std::string, CreatureDef*> CreatureDefMap;

/**
 * @brief This is the main Creature repository
 */
class CreatureStore
{
public:
	CreatureStore(GameDatabase* db);
	~CreatureStore();

	void Load(std::unordered_map<u32, Spell *> spells);
	CreatureDef* getDefinition(const std::string &creature);
	CreatureDropList* getDropList(u32 id) const;
	CreatureNodeReplace *getNodeReplaceList(u32 id) const;
private:
	void clear();

	CreatureDropMap m_drops;
	CreatureNodeReplaceMap m_node_replace;
	CreatureDefMap m_definitions;
	GameDatabase* m_database;
};

}
