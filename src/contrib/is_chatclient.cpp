/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "is_chatclient.h"
#include "settings.h"
#include "util/serialize.h"

namespace epixel
{

#define CHATCLIENT_SEND_QUEUE_MAX 100
#define CHATCLIENT_SEND_QUEUE_PER_LOOP 10
#define SOCKET_BUFFER_SIZE 4096
#define MAX_ALLOWED_PACKET_SIZE 1024
#define HEADER_LEN 4

struct ISChatClientPacketHandlerEntry
{
	const char* name;
	ConnectionState state;
	bool (ISChatClient::*handler)(const Json::Value& msg);
};

static const ISChatClientPacketHandlerEntry nullPacketHandlerEntry = { "NULL", CONNECTION_STATE_NOT_CONNECTED, nullptr };
const std::vector<ISChatClientPacketHandlerEntry> packetHandlingTable = {
	nullPacketHandlerEntry,
	nullPacketHandlerEntry,
	nullPacketHandlerEntry,
	{ "CMSG_AUTH", CONNECTION_STATE_NOT_CONNECTED, nullptr },
	{ "CMSG_CHATMSG", CONNECTION_STATE_NOT_CONNECTED, nullptr },
	{ "SMSG_CHATMSG", CONNECTION_STATE_AUTHED, &ISChatClient::handle_ChatServerMessage }
};

ISChatClient::~ISChatClient()
{
}

void* ISChatClient::run()
{
	porting::setThreadName("ISChatClient");
	logger_is_chat.notice("%s starting", m_client_name.c_str());

	try {
		m_chatserver_myname = g_settings->get("interserver_chat_servername");
	}
	catch (SettingNotFoundException &) {}
	try {
		m_chatserver_token = g_settings->get("interserver_chat_auth_token");
	}
	catch (SettingNotFoundException &) {}

	ThreadStarted();

	while (!stopRequested()) {
		// Drop exceeding messages in send queue
		try {
			while (m_send_queue.size() > CHATCLIENT_SEND_QUEUE_MAX) {
				delete m_send_queue.pop_front(1);
			}
		}
		catch (ItemNotFoundException &) {}

		switch (m_conn_state) {
			case CONNECTION_STATE_NOT_CONNECTED:
				// If not connected, try to reconnect
				if (!connectToChatServer()) {
					closeSocket();
					continue;
				}
				break;
			case CONNECTION_STATE_CONNECTED: sendAuthentication(); break;
			case CONNECTION_STATE_AUTHED: handleAuthedState(); break;
			default: break;
		}

		// Be cool with CPU
		m_sem.wait(10);
	}

	// Empty queues
	while (!m_recv_queue.empty()) {
		delete m_recv_queue.pop_frontNoEx(2);
	}

	while (!m_send_queue.empty()) {
		delete m_send_queue.pop_frontNoEx(2);
	}

	logger_is_chat.notice("%s shutdown", m_client_name.c_str());
	return NULL;
}

bool ISChatClient::handle_ChatServerMessage(const Json::Value& msg)
{
	if (!msg.isMember("server_name") || !msg["server_name"].isString() ||
			!msg.isMember("message") || !msg["message"].isString() ||
			!msg.isMember("author") || !msg["author"].isString() ||
			!msg.isMember("channel") || !msg["channel"].isString()) {
		logger.error("Invalid SMSG_CHAT received from server, ignoring");
		return false;
	}

	ISChatMessage* chatmsg = new ISChatMessage(msg["author"].asString(),
			msg["server_name"].asString(), msg["message"].asString());
	m_recv_queue.push_back(chatmsg);
	return true;
}

void ISChatClient::handleAuthedState()
{
	if (isSocketValid(m_sock) != 0) {
		closeSocket();
		logger.error("Connection with Interserver Chatserver lost.");
		return;
	}

	if (m_send_queue.size() > 0) {
		handleSendingDatas();
	}

	handleReceivedDatas();
}

void ISChatClient::handleSendingDatas()
{
	const u16 msgToSend = m_send_queue.size() > CHATCLIENT_SEND_QUEUE_PER_LOOP ? CHATCLIENT_SEND_QUEUE_PER_LOOP : m_send_queue.size();

	try {
		bool rv;
		for (u16 i = 0; i < msgToSend; i++) {
			ISChatMessage* msg = m_send_queue.pop_front(1);
			rv = sendPacket(4, msg->toJson());
			delete msg;

			if (!rv) {
				logger.error("Connection with Interserver Chatserver lost while sending message.");
				closeSocket();
				return;
			}
		}
	}
	catch (ItemNotFoundException &) {}
}

const bool ISChatClient::handleReceivedDatas()
{
	std::string recv_s = "";
	u32 packet_size_to_recv = 0;
	int rv;

	// Read one packet
	while (true) {
		// Read socket
		char buf[SOCKET_BUFFER_SIZE];
		size_t len = sizeof(buf);
		// Init buffer
		memset(buf, 0, len);

		rv = isSocketValid(m_sock);
		if (rv != 0) {
			closeSocket();
			return false;
		}

		// If packet_size_to_recv == 0, we should read the packet size
		if (packet_size_to_recv == 0) {
			size_t header_len = HEADER_LEN;
			// receive datas
			rv = read_socket(m_sock, (uint8_t *) buf, header_len);
			if (rv == 0) {
				return true;
			}

			if (rv != HEADER_LEN) {
				logger.error("Invalid packet header received (len %d). Closing socket.", rv);
				closeSocket();
				return false;
			}

			packet_size_to_recv = readU32((const u8*)buf);

			if (packet_size_to_recv > MAX_ALLOWED_PACKET_SIZE) {
				logger.error("InterServer ChatServer wants to send us a very large packet (size: %d). Closing socket.", packet_size_to_recv);
				closeSocket();
				return false;
			}

			// Reinit buf
			memset(buf, 0, len);
		}

		if (isSocketValid(m_sock) != 0) {
			return false;
		}

		if (len > packet_size_to_recv) {
			len = packet_size_to_recv;
		}

		// receive datas
		rv = read_socket(m_sock, (uint8_t*) buf, len);

		// Store them to outbuffer
		recv_s.append(buf, rv);

		// And decrement size to read
		packet_size_to_recv -= rv;
		if (rv <= 0 || packet_size_to_recv <= 0) {
			// Nothing more to read on the socket, exiting the loop
			break;
		}
	}

	Json::Value root;
	Json::Reader reader;
	if (!reader.parse(recv_s, root, false) || !root["o"].isInt() || !root["data"].isObject()) {
		logger.error("Invalid JSON received from InterServer ChatServer, ignoring and closing connection.");
		closeSocket();
		return false;
	}

	uint16_t opcode = root["o"].asInt();

	if (opcode < packetHandlingTable.size() && packetHandlingTable[opcode].handler) {
		const ISChatClientPacketHandlerEntry& opHandle = packetHandlingTable[opcode];
		(this->*opHandle.handler)(root["data"]);
	}
	else {
		logger.warn("Invalid opcode %d received. Closing ignoring.", opcode);
	}
	return true;
}

void ISChatClient::sendAuthentication()
{
	Json::Value jsonv;
	jsonv["server_name"] = m_chatserver_myname;
	jsonv["auth_token"] = m_chatserver_token;

	bool rv = sendPacket(3, jsonv);
	if (!rv) {
		logger.crit("Authentication to Interserver Chatserver failed.");
		closeSocket();
		return;
	}

	m_conn_state = CONNECTION_STATE_AUTHED;
}

const bool ISChatClient::connectToChatServer()
{
	bool rv;
	std::string chatserverHost = "chat.epixel-game.net";

	try {
		chatserverHost = g_settings->get("interserver_chat_host");
	}
	catch (SettingNotFoundException &)	{}

	rv = sconnect(chatserverHost, g_settings->get(U16SETTING_INTERSERVER_CHAT_PORT),
			g_settings->get(U16SETTING_INTERSERVER_CHAT_TIMEOUT));

	if (!rv) {
		return rv;
	}

	m_conn_state = CONNECTION_STATE_CONNECTED;

	reinitReconnectionTime();

	return true;
}

const bool ISChatClient::sendPacket(const u16 opcode, const Json::Value& json)
{
	Json::Value jsonr;
	jsonr["o"] = opcode;
	jsonr["data"] = json;

	// Convert Json to string
	const std::string jsonStr = m_json_writer.write(jsonr);
	u32 lenJson = jsonStr.size();

	// Write header
	u8 hdrBuf[4];
	writeU32(hdrBuf, lenJson);

	size_t len = lenJson + 4 * sizeof(u8);

	std::string buf;
	buf.append((const char*)hdrBuf, 4);
	buf.append(jsonStr.data(), lenJson);

	// Send datas
	return swrite(m_sock, (uint8_t *) buf.data(), len) > 0;
}

}
