/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#include <string>
#include <json/json.h>
#include "contrib/util/tcpclient.h"
#include "util/container.h"

class Server;

namespace epixel
{

// @TODO: handle channels
struct ISChatMessage
{
	ISChatMessage(const std::string &s, const std::string &sn, const std::string &msg):
		sender(s), server_name(sn), message(msg) {}
	std::string sender = "";
	std::string server_name = "";
	std::string message = "";
	const Json::Value toJson()
	{
		Json::Value json;
		json["author"] = sender;
		json["message"] = message;
		json["channel"] = "main";
		return json;
	}
};

class ISChatClient: public TCPClient<ISChatMessage*>
{
public:
	ISChatClient(): TCPClient("Interserver Chat Client", true) {}
	~ISChatClient();

	void * run();

	// Specific handlers
	bool handle_ChatServerMessage(const Json::Value& msg);
private:
	// Generic handlers
	const bool connectToChatServer();
	void sendAuthentication();
	void handleAuthedState();
	const bool handleReceivedDatas();
	void handleSendingDatas();

	// Socket
	const bool sendPacket(const u16 opcode, const Json::Value &json);

	// Protocol
	std::string m_chatserver_myname = "unknown_server";
	std::string m_chatserver_token = "12345";

	Json::FastWriter m_json_writer;
};


}
