/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "lua_api/l_object.h"
#include "player.h"
#include "content_sao.h"
#include "server.h"
#include "settings.h"
#include "contrib/creature.h"
#include "contrib/playersao.h"

int ObjectRef::l_set_hunger(lua_State *L)
{
	ObjectRef *ref = checkobject(L, 1);
	PlayerSAO* co = getplayersao(ref);
	if(co == NULL) return 0;

	s32 hunger = luaL_checknumber(L, 2);

	// If the object is a player sent the breath to client
	if (co->getType() == ACTIVEOBJECT_TYPE_PLAYER) {
		PlayerSAO* sao = ((PlayerSAO*)co);
		RemotePlayer* player = sao->getPlayer();
		if (hunger > PLAYER_HUNGER_MAX) {
			hunger = PLAYER_HUNGER_MAX;
		}
		else if(hunger < 0) {
			hunger = 0;
		}
		sao->setHunger(hunger);
		getServer(L)->SendHUDChange(sao->getPeerID(),
				player->getHuds()->getHudId(PLAYERHUD_HUNGER_FG), HUD_STAT_NUMBER, &hunger);
	}

	return 1;
}

int ObjectRef::l_get_hunger(lua_State *L)
{
	ObjectRef *ref = checkobject(L, 1);
	PlayerSAO* co = getplayersao(ref);
	if(co == NULL) return 0;

	// If the object is a player sent the breath to client
	if (co->getType() == ACTIVEOBJECT_TYPE_PLAYER) {
		lua_pushnumber(L, co->getHunger());
	}
	return 1;
}

int ObjectRef::l_set_armor(lua_State *L)
{
	ObjectRef *ref = checkobject(L, 1);
	PlayerSAO* co = getplayersao(ref);
	if(co == NULL) return 0;

	u32 armor = luaL_checknumber(L, 2);

	// If the object is a player sent the breath to client
	if (co->getType() == ACTIVEOBJECT_TYPE_PLAYER) {
		RemotePlayer* player = co->getPlayer();
		co->setArmor(armor);
		getServer(L)->SendHUDChange(co->getPeerID(),
				player->getHuds()->getHudId(PLAYERHUD_ARMOR_FG), HUD_STAT_NUMBER, &armor);
	}
	return 1;
}

/**
 * @brief ObjectRef::l_set_player_extended_attribute
 * @param L
 * @return 0 on fail, 1 on success
 *
 * Set an arbitrary value to an arbitrary player attribute
 * This usage is reserved to mods which needs some flexibility
 */
int ObjectRef::l_set_player_extended_attribute(lua_State *L)
{
	ObjectRef *ref = checkobject(L, 1);
	PlayerSAO* co = getplayersao(ref);
	if(co == NULL) return 0;

	std::string attr = luaL_checkstring(L, 2);
	std::string value = luaL_checkstring(L, 3);

	if (co->getType() == ACTIVEOBJECT_TYPE_PLAYER) {
		co->setExtendedAttribute(attr, value);
	}
	return 1;
}

/**
 * @brief ObjectRef::l_get_player_extended_attribute
 * @param L
 * @return 0 on fail, 1 on success
 *
 * Get an arbitrary value for an arbitrary player attribute
 * This usage is reserved to mods which needs some flexibility
 */
int ObjectRef::l_get_player_extended_attribute(lua_State *L)
{
	ObjectRef *ref = checkobject(L, 1);
	PlayerSAO* co = getplayersao(ref);
	if(co == NULL) return 0;

	std::string attr = luaL_checkstring(L, 2);

	if (co->getType() == ACTIVEOBJECT_TYPE_PLAYER) {
		lua_pushstring(L, (co->getExtendedAttribute(attr)).c_str());
	}
	return 1;
}

int ObjectRef::l_set_creature_tamed(lua_State *L)
{
	ObjectRef *ref = checkobject(L, 1);
	epixel::Creature* co = getcreatureobject(ref);

	if(co == NULL)
		return 0;

	bool tamed = lua_toboolean(L, 2);
	co->setTamed(tamed);
	return 1;
}

epixel::Creature* ObjectRef::getcreatureobject(ObjectRef *ref)
{
	ServerActiveObject *obj = getobject(ref);
	if (obj == NULL)
		return NULL;
	if (obj->getType() != ACTIVEOBJECT_TYPE_LUACREATURE)
		return NULL;
	return (epixel::Creature*)obj;
}

UnitSAO* ObjectRef::getunitobject(ObjectRef *ref)
{
	ServerActiveObject *obj = getobject(ref);
	if (obj == NULL)
		return NULL;
	switch (obj->getType()) {
		case ACTIVEOBJECT_TYPE_LUACREATURE:
		case ACTIVEOBJECT_TYPE_PLAYER:
		case ACTIVEOBJECT_TYPE_LUAENTITY:
			return (UnitSAO*)obj;
		default:
			return NULL;
	}
}

/**
 * @brief ObjectRef::l_home_teleport
 * @param L
 * @return 0 on fail, 1 on success
 *
 * Get home position for teleporting and send message
 * if mod mana is actived the teleport use mana
 * else timer is used
 * This usage is reserved to mods which needs some flexibility
 */
int ObjectRef::l_home_teleport(lua_State *L)
{
	ObjectRef *ref = checkobject(L, 1);
	PlayerSAO *co = getplayersao(ref);
	if(co == NULL) return 0;
	Server *srv = ref->getServer(L);

	if (!g_settings->get(BOOLSETTING_ENABLE_MOD_SETHOME)) {
		srv->SendChatMessage(co->getPeerID(), L"Mod 'home' not active");
		return 0;
	}

	// Check if player has home priv
	if (co->hasPriv("home")) {
		if (!co->tryTeleport())
			return false;
	} else {
		srv->SendChatMessage(co->getPeerID(), L"You don't have the 'home' privilege");
	}
	return 1;
}

/**
 * @brief ObjectRef::l_set_home
 * @param L
 * @return 0 on fail, 1 on success
 *
 * Set the current player position to home set position
 * and send message with position
 * This usage is reserved to mods which needs some flexibility
 */
int ObjectRef::l_set_home(lua_State *L)
{
	ObjectRef *ref = checkobject(L, 1);
	PlayerSAO* co = getplayersao(ref);
	if(co == NULL) return 0;
	Server *srv = ref->getServer(L);

	if (!g_settings->get(BOOLSETTING_ENABLE_MOD_SETHOME)) {
		srv->SendChatMessage(co->getPeerID(), L"Mod 'home' not active");
		return 0;
	}

	// Check if player has home priv
	if (co->hasPriv("home")) {
		co->setHome(co->getBasePosition());

		std::wstringstream ws;
		ws << "Home set to (" << co->getBasePosition().X / BS
		   << ", " << co->getBasePosition().Y / BS
		   << ", " << co->getBasePosition().Z / BS
		   << ") " << std::endl;
		ref->getServer(L)->SendChatMessage(co->getPeerID(), ws.str());
		return 1;
	}
	srv->SendChatMessage(co->getPeerID(), L"You don't have the 'home' privilege");
	return 1;
}

/**
 * @brief ObjectRef::l_mana_modify
 * @param L
 * @return 0 on fail, 1 on success
 *
 * Modify mana
 * This usage is reserved to mods which needs some flexibility
 */
int ObjectRef::l_mana_modify(lua_State *L)
{
	ObjectRef *ref = checkobject(L, 1);
	PlayerSAO* co = getplayersao(ref);
	if(co == NULL) return 0;
	Server *srv = ref->getServer(L);

	if (!g_settings->get(BOOLSETTING_ENABLE_MOD_MANA)) {
		srv->SendChatMessage(co->getPeerID(), L"Mod 'mana' not active");
		return 0;
	}

	s32 mana_use = luaL_checkinteger(L, 2);

	if (co->modifyMana(mana_use)) {
		lua_pushinteger(L, co->getRemainingOverHead());
	}
	return 1;
}

int ObjectRef::l_apply_dot(lua_State *L)
{
	ObjectRef *ref = checkobject(L, 1);
	UnitSAO* co = getunitobject(ref);
	if (co == NULL) return 0;
	u32 damage_min = luaL_checkinteger(L, 2);
	u32 damage_max = luaL_checkinteger(L, 3);
	float duration = lua_tonumber(L, 4);
	float period = lua_tonumber(L, 5);

	co->applyDot(new DamageOverTime(damage_min, damage_max, duration, period));
	return 1;
}

