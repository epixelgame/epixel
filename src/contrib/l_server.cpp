/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "lua_api/l_server.h"
#include "common/c_converter.h"
#include "common/c_content.h"
#include "server.h"
#include "player.h"
#include "database.h"
#include "contrib/areas.h"
#include "contrib/playersao.h"

int ModApiServer::l_set_money(lua_State *L)
{
	const char *name = luaL_checkstring(L, 1);
	u64 money = luaL_checknumber(L, 2);
	RemotePlayer *player = getEnv(L)->getPlayer(name);
	if (player == NULL || player->getPlayerSAO() == NULL) {
		lua_pushboolean(L, false); // No such player
		return 1;
	}
	player->getPlayerSAO()->setMoney(money);

	std::stringstream ss;
	ss << "Account: $" << money;
	std::string moneyStr = ss.str();
	void* value = &moneyStr;

	getServer(L)->SendHUDChange(player->getPlayerSAO()->getPeerID(),
			player->getHuds()->getHudId(PLAYERHUD_MONEY), HUD_STAT_TEXT, value);
	lua_pushboolean(L, true);
	return 1;
}

int ModApiServer::l_get_money(lua_State *L)
{
	const char *name = luaL_checkstring(L, 1);
	RemotePlayer *player = getEnv(L)->getPlayer(name);
	if (player == NULL || player->getPlayerSAO() == NULL) {
		lua_pushnil(L); // No such player
		return 1;
	}
	lua_pushnumber(L, player->getPlayerSAO()->getMoney());
	return 1;
}

int ModApiServer::l_get_area(lua_State *L)
{
	u32 id = luaL_checknumber(L, 1);

	epixel::Area* area = getServer(L)->getAreaMgr()->getArea(id);
	if (area) {
		lua_newtable(L);
		lua_pushstring(L, area->name.c_str());
		lua_setfield(L, -2, "name");
		lua_pushnumber(L, area->pos1.X);
		lua_setfield(L, -2, "p1x");
		lua_pushnumber(L, area->pos1.Y);
		lua_setfield(L, -2, "p1y");
		lua_pushnumber(L, area->pos1.Z);
		lua_setfield(L, -2, "p1z");
		lua_setfield(L, -2, "name");
		lua_pushnumber(L, area->pos2.X);
		lua_setfield(L, -2, "p2x");
		lua_pushnumber(L, area->pos2.Y);
		lua_setfield(L, -2, "p2y");
		lua_pushnumber(L, area->pos2.Z);
		lua_setfield(L, -2, "p2z");
		lua_pushstring(L, "some owners");
		lua_setfield(L, -2, "owner");
		lua_pushinteger(L, area->parent_id);
		lua_setfield(L, -2, "parent");
	}
	return 1;
}

int ModApiServer::l_add_area(lua_State *L)
{
	const char *owner = luaL_checkstring(L, 1);
	const char *areaname = luaL_checkstring(L, 2);
	v3f pos1 = read_v3f(L, 3);
	v3f pos2 = read_v3f(L, 4);
	u32 parent_id = 0;
	if(!lua_isnil(L, 5))
		parent_id = luaL_checknumber(L, 5);

	RemotePlayer *player = getEnv(L)->getPlayer(owner);
	if (player == NULL) {
		lua_pushnil(L); // No such player
		return 1;
	}

	lua_pushinteger(L, getServer(L)->getAreaMgr()->addArea(player->getDBId(), areaname, pos1, pos2, parent_id));
	return 1;
}

int ModApiServer::l_remove_area(lua_State *L)
{
	u32 id = luaL_checknumber(L, 1);
	bool recurse = (luaL_checknumber(L, 2) > 0);

	getServer(L)->getAreaMgr()->removeArea(id, recurse);

	return 1;
}

int ModApiServer::l_area_issubarea(lua_State *L)
{
	v3f pos1 = read_v3f(L, 1);
	v3f pos2 = read_v3f(L, 2);
	u32 id = luaL_checknumber(L, 3);

	lua_pushboolean(L, getServer(L)->getAreaMgr()->isSubArea(pos1, pos2, id));
	return 1;
}

int ModApiServer::l_is_areaowner(lua_State *L)
{
	u32 id = luaL_checknumber(L, 1);
	luaL_checktype(L, 2, LUA_TSTRING);
	std::string name = lua_tostring(L, 2);

	RemotePlayer *player = getEnv(L)->getPlayer(name.c_str());
	if (player == NULL) {
		lua_pushboolean(L, false); // No such player
		return 1;
	}

	lua_pushboolean(L, getServer(L)->getAreaMgr()->isAreaOwner(id, player->getDBId()));
	return 1;
}

int ModApiServer::l_area_caninteract(lua_State *L)
{
	v3f pos = read_v3f(L, 1);
	luaL_checktype(L, 2, LUA_TSTRING);
	std::string name = lua_tostring(L, 2);

	RemotePlayer *player = getEnv(L)->getPlayer(name.c_str());
	if (player == NULL) {
		lua_pushboolean(L, false); // No such player
		return 1;
	}

	lua_pushboolean(L, getServer(L)->getAreaMgr()->canInteract(pos, player->getDBId()));
	return 1;
}

int ModApiServer::l_area_getnodeowners(lua_State *L)
{
	v3f pos = read_v3f(L, 1);

	std::vector<std::string> owners = getServer(L)->getAreaMgr()->getNodeOwners(pos);

	// Package them up for Lua
	lua_createtable(L, owners.size(), 0);
	std::vector<std::string>::iterator iter = owners.begin();
	for (u32 i = 0; iter != owners.end(); iter++) {
		lua_pushstring(L, iter->c_str());
		lua_rawseti(L, -2, ++i);
	}
	return 1;
}

int ModApiServer::l_area_rename(lua_State *L)
{
	u32 id = luaL_checknumber(L, 1);
	const char *name = luaL_checkstring(L, 2);

	getServer(L)->getAreaMgr()->renameArea(id, name);
	return 1;
}

int ModApiServer::l_area_changeowner(lua_State *L)
{
	u32 id = luaL_checknumber(L, 1);
	const char *name = luaL_checkstring(L, 2);
	const char *newOwner = luaL_checkstring(L, 3);

	u32 olduserid = getServer(L)->getAuthDatabase()->userExists(name);
	if (!olduserid) {
		lua_pushboolean(L, false); // No such player
		return 1;
	}

	u32 nuserid = getServer(L)->getAuthDatabase()->userExists(newOwner);
	if (!nuserid) {
		lua_pushboolean(L, false); // No such player
		return 1;
	}

	getServer(L)->getAreaMgr()->changeOwner(id, 0, olduserid, nuserid);
	return 1;
}

int ModApiServer::l_area_open(lua_State *L)
{
	u32 id = luaL_checknumber(L, 1);
	const char *name = luaL_checkstring(L, 2);
	bool open = (luaL_checknumber(L, 3) > 0);

	RemotePlayer *player = getEnv(L)->getPlayer(name);
	if (player == NULL) {
		lua_pushboolean(L, false); // No such player
		return 1;
	}

	getServer(L)->getAreaMgr()->changeOpen(id, player->getDBId(), open);
	return 1;
}

int ModApiServer::l_area_canplayeradd(lua_State *L)
{
	v3f pos1 = read_v3f(L, 1);
	v3f pos2 = read_v3f(L, 2);
	const char *name = luaL_checkstring(L, 3);

	RemotePlayer *player = getEnv(L)->getPlayer(name);
	if (player == NULL) {
		lua_pushboolean(L, false); // No such player
		return 1;
	}

	std::string errorMsg = "";
	getServer(L)->getAreaMgr()->canPlayerAddArea(pos1, pos2, player->getDBId(), errorMsg);
	lua_pushstring(L, errorMsg.c_str());

	return 1;
}

int ModApiServer::l_area_move(lua_State *L)
{
	u32 id = luaL_checknumber(L, 1);
	v3f pos1 = read_v3f(L, 2);
	v3f pos2 = read_v3f(L, 3);

	lua_pushstring(L, getServer(L)->getAreaMgr()->moveArea(id, pos1, pos2).c_str());

	return 1;
}

int ModApiServer::l_area_get_by_pos1_pos2(lua_State *L)
{
	v3f pos1 = read_v3f(L, 1);
	v3f pos2 = read_v3f(L, 2);
	lua_pushnumber(L, getServer(L)->getAreaMgr()->getAreaIdByPos1Pos2(pos1, pos2));
	return 1;
}
