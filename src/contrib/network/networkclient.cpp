/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "socket.h"
#include "networkclient.h"
#include "network/clientopcodes.h"
#include "util/serialize.h"
#include "porting.h"
#include "log.h"


namespace epixel
{

NetworkClient::NetworkClient(const std::string &host, const u16 port): Thread(),
		m_server_host(host), m_server_port(port)
{
}

NetworkClient::~NetworkClient()
{
	if (m_server_peer) {
		enet_peer_disconnect(m_server_peer, 0);
	}
}

void* NetworkClient::run() {
	porting::setThreadName("EpixelNetworkClient");
	logger.notice("EpixelNetworkClient starting");

	ThreadStarted();

	ENetHost *client = enet_host_create(NULL, 1, 2, 0, 0);
	if (!client) {
		m_conn_state = CONNECTION_STATE_CONNECTION_FAILED;
		throw SocketException("Unable to create EpixelNetworkClient");
	}

	ENetAddress address;
	enet_address_set_host(&address, m_server_host.c_str());
	address.port = m_server_port;
	m_server_peer = enet_host_connect(client, &address, 2, 0);

	ENetEvent event;

	// Connect to server
	if (enet_host_service(client, &event, 5000) <= 0 ||
		event.type != ENET_EVENT_TYPE_CONNECT) {
		enet_peer_reset(m_server_peer);
		m_conn_state = CONNECTION_STATE_CONNECTION_FAILED;
		logger.error("EpixelNetworkClient: Failed to connect to %s:%d", m_server_host.c_str(), m_server_port);
		return NULL;
	}

	enet_socket_set_option(client->socket, ENET_SOCKOPT_NONBLOCK, 1);

	m_conn_state = CONNECTION_STATE_CONNECTED;
	logger.notice("EpixelNetworkClient: Connected to server");

	while (!stopRequested()) {
		ENetEvent event;
		while (enet_host_service(client, &event, 10) > 0) {
			switch (event.type) {
				case ENET_EVENT_TYPE_RECEIVE:
				{
					logger.debug("EpixelNetworkClient: A packet of length %u was received from server on channel %u.",
								 event.packet->dataLength, event.channelID);

					NetworkPacket *pkt = new NetworkPacket();
					pkt->putRawPacket(event.packet->data, event.packet->dataLength, PEER_ID_SERVER);
					m_recv_queue.push_back(pkt);

					enet_packet_destroy(event.packet);
					break;
				}
				case ENET_EVENT_TYPE_DISCONNECT:
					logger.notice("EpixelNetworkClient: disconnected by server.");
					event.peer->data = NULL;
					enet_peer_reset(m_server_peer);
					break;
				default: break;
			}
		}
	}

	enet_host_destroy(client);

	logger.notice("EpixelNetworkClient shutdown");
	return NULL;
}

void NetworkClient::pushMsg(NetworkPacket *pkt)
{
	u8* buf = new u8[pkt->getSize() + 2];
	writeU16(buf, pkt->getCommand());
	memcpy(&buf[2], pkt->getU8Ptr(0), pkt->getSize());

	ENetPacket* packet = enet_packet_create(buf, pkt->getSize() + 2,
			serverCommandFactoryTable[pkt->getCommand()].reliable ? ENET_PACKET_FLAG_RELIABLE : 0);
	enet_peer_send(m_server_peer, 0, packet);

	delete [] buf;
}
}
