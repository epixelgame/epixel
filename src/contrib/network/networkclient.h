/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#include <enet/enet.h>
#include "threads/thread.h"
#include "util/container.h"

class NetworkPacket;

namespace epixel
{

enum ConnectionState
{
	CONNECTION_STATE_NOT_CONNECTED,
	CONNECTION_STATE_CONNECTION_FAILED,
	CONNECTION_STATE_CONNECTED,
	CONNECTION_STATE_AUTHED,
};

class NetworkClient: public Thread
{
public:
	NetworkClient(const std::string &host, const u16 port);
	~NetworkClient();

	void* run();

	inline const bool isRecvQueueEmpty()
	{
		return m_recv_queue.empty();
	}

	NetworkPacket* popRecvMsg()
	{
		try {
			return m_recv_queue.pop_front(2);
		}
		catch (ItemNotFoundException &) {
			return nullptr;
		}
	}

	inline const ConnectionState getConnState() const { return m_conn_state; }

	void pushMsg(NetworkPacket* pkt);
private:
	ConnectionState m_conn_state = CONNECTION_STATE_NOT_CONNECTED;
	ENetPeer* m_server_peer = nullptr;
	MutexedQueue<NetworkPacket*> m_recv_queue;
	std::string m_server_host;
	u16 m_server_port;
};
}