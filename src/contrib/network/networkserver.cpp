/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "networkserver.h"
#include "settings.h"
#include "network/serveropcodes.h"
#include "util/serialize.h"


namespace epixel
{

void* NetworkServer::run()
{
	porting::setThreadName("EpixelNetworkServer");

	ThreadStarted();

	ENetAddress address;
	address.host = ENET_HOST_ANY;
#ifndef SERVER
	address.port = m_port;
#else
	address.port = g_settings->get(U16SETTING_EPIXEL_PORT);
#endif

	logger.notice("Starting epixel NetworkServer, listening on port %d", address.port);

	m_enet_server = enet_host_create(&address , 128, 2, 0, 0);
	if (m_enet_server == NULL) {
		throw SocketException("Unable to create EpixelNetworkServer");
	}

	enet_socket_set_option(m_enet_server->socket, ENET_SOCKOPT_NONBLOCK, 1);

	ENetEvent event;
	while (!stopRequested()) {
		while (enet_host_service(m_enet_server, &event, 10) > 0) {
			switch (event.type) {
				case ENET_EVENT_TYPE_CONNECT:
				{
					char addr_buf[64];
					enet_address_get_host_ip(&event.peer->address, addr_buf, sizeof(addr_buf));
					Session *sess = new Session(std::string(addr_buf), event.peer->address.port, event.peer);
					sess->peer_id = m_next_session_id;
					prepareNextSessionId();
					event.peer->data = (void *) sess;

					logger.info("A new epixel client connected from %s:%u.",	addr_buf, event.peer->address.port);

					// Try to create client. If session already allocated, find the next possible ID
					u16 try_number = 0;
					while (!m_client_iface->CreateClient(sess) && try_number < U16_MAX / 2) {
						sess->peer_id = m_next_session_id;
						prepareNextSessionId();
						try_number++;
					}

					// Peer_id not found, disconnect peer.
					if (try_number == U16_MAX / 2) {
						logger.crit("Peer %s:%d disconnected because we don't have peer slots.",
								addr_buf, event.peer->address.port);
						enet_peer_disconnect(event.peer, 0);
						delete sess;
					}
					break;
				}
				case ENET_EVENT_TYPE_RECEIVE:
				{
					logger.debug("A packet of length %u was received from %s on channel %u for peer %u",
							event.packet->dataLength, ((Session *) event.peer->data)->getAddress().c_str(),
							event.channelID, ((Session *) event.peer->data)->peer_id);
					std::shared_ptr<NetworkPacket> pkt(new NetworkPacket);
					pkt->putRawPacket(event.packet->data, event.packet->dataLength,
									  ((Session *) event.peer->data)->peer_id);
					m_recv_queue.push_back(pkt);
					enet_packet_destroy(event.packet);
					break;
				}
				case ENET_EVENT_TYPE_DISCONNECT:
				{
					Session* sess = (Session*) event.peer->data;
					logger.info("%s disconnected.", sess->getAddress().c_str());
					m_server->DeleteClient(sess->peer_id, CDR_LEAVE);
					event.peer->data = NULL;
					break;
				}

				default: break;
			}
		}
	}

	enet_host_destroy(m_enet_server);

	return nullptr;
}

void NetworkServer::pushMsg(epixel::Session *sess, NetworkPacket *pkt)
{
	u8* buf = new u8[pkt->getSize() + 2];
	writeU16(buf, pkt->getCommand());
	memcpy(&buf[2], pkt->getU8Ptr(0), pkt->getSize());

	ENetPacket* packet = enet_packet_create(buf, pkt->getSize() + 2,
			clientCommandFactoryTable[pkt->getCommand()].reliable ? ENET_PACKET_FLAG_RELIABLE : 0);
	enet_peer_send(sess->getPeer(), 0, packet);

	delete [] buf;
}
}