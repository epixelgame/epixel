/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#include <enet/enet.h>
#include "server.h"
#include "clientiface.h"
#include "network/networkpacket.h"

namespace epixel
{
class Session: public RemoteClient
{
public:
	Session(const std::string &addr, const u16 port, ENetPeer* p): RemoteClient(),
			m_address(addr), m_peer(p) { m_session_type = SESSION_TYPE_EPIXEL; }
	~Session() {}
	std::string getAddress() const { return m_address; }
	ENetPeer* getPeer() const { return m_peer; }
private:
	std::string m_address;
	ENetPeer* m_peer;
};

class NetworkServer: public Thread
{
public:
	NetworkServer(const std::string &bind_addr, const u16 port, Server* s, ClientInterface* i):
			Thread(), m_bind_addr(bind_addr),
#ifndef SERVER
			m_port(port),
#endif
			m_server(s), m_client_iface(i) {}
	~NetworkServer() {}

	void pushMsg(epixel::Session* sess, NetworkPacket* pkt);
	void * run();

	std::shared_ptr<NetworkPacket> popRecvMsg()
	{
		try {
			return m_recv_queue.pop_front(2);
		}
		catch (ItemNotFoundException &) {
			return nullptr;
		}
	}

	bool prepareNextSessionId()
	{
		if (m_next_session_id == U16_MAX) {
			// @TODO: lookup sessions to find which ID is available
			m_next_session_id = U16_MAX / 2;
			return true;
		}

		m_next_session_id++;
		return true;
	}

	void Disconnect(ENetPeer* peer) { enet_peer_disconnect(peer, 0); }
private:
	MutexedQueue<std::shared_ptr<NetworkPacket> > m_recv_queue;

	std::string m_bind_addr;
#ifndef SERVER
	u16 m_port;
#endif
	Server* m_server;
	ClientInterface* m_client_iface;
	ENetHost* m_enet_server;
	u16 m_next_session_id = U16_MAX / 2;
};
}
