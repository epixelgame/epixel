#pragma once

#include "../irr_v3d.h"

class ServerEnvironment;

namespace epixel
{
#define NODETIMERS_MAX_DEFS 1

struct nodeTimerEntry {
	nodeTimerEntry() {}
	bool (ServerEnvironment::*node_timer_handler)(const v3s16 &p, const float elapsed) = nullptr;
};

extern const nodeTimerEntry nodeTimerTableDefs[NODETIMERS_MAX_DEFS];
}