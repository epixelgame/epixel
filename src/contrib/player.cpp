/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include <iomanip>
#include <regex>

#include "player.h"
#include "database.h"
#include "gamedef.h"
#include "server.h"
#include "settings.h"
#include "content_sao.h"
#include "contrib/chathandler.h"
#include "contrib/playersao.h"
#include "contrib/utils.h"
#include "util/string.h"

namespace epixel
{

bool ChatHandler::handleCommand_list_privs(const std::string &args, const u16 peer_id)
{
	std::vector<std::string> commandline;
	std::stringstream ss(args);
	std::string item;
	while (std::getline(ss, item, ' ')) {
		commandline.push_back(item);
	}

	if (commandline.size() > 1) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help privs list");
		return true;
	}

	std::string playername;
	if (commandline.empty()) {
		playername = m_server->getPlayerName(peer_id);
	}
	else {
		playername = commandline[0];
	}

	u32 user_id = m_server->getAuthDatabase()->userExists(playername);
	if (!user_id) {
		std::wstringstream ws;
		ws << "Player '" << playername.c_str() << "' doesn't exist!" << std::endl;
		m_server->SendChatMessage(peer_id, ws.str());
		return true;
	}

	std::set<std::string> privs;
	m_server->getGameDatabase()->loadUserPrivs(user_id, privs);

	std::wstringstream ws;
	ws << "Available privileges for " << playername.c_str() << ": ";
	for (const auto &priv: privs) {
		ws << priv.c_str() << " ";
	}
	ws << std::endl;
	m_server->SendChatMessage(peer_id, ws.str());
	return true;
}

bool ChatHandler::handleCommand_grant_priv(const std::string &args, const u16 peer_id)
{
	std::vector<std::string> commandline;
	std::stringstream ss(args);
	std::string item;
	while (std::getline(ss, item, ' ')) {
		commandline.push_back(item);
	}

	if (commandline.size() != 2) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help privs grant");
		return true;
	}

	std::string playername = commandline[0], priv = commandline[1];
	std::wstringstream ws;

	u32 playerid = m_server->getAuthDatabase()->userExists(playername);
	if (!playerid) {
		ws << "Player " << playername.c_str() << " doesn't exists." << std::endl;
		m_server->SendChatMessage(peer_id, ws.str());
		return true;
	}

	// If player is connected, check the privs directly ingame
	RemotePlayer* player = m_server->getEnv().getPlayer(playername.c_str());
	std::set<std::string> privs;
	if (player) {
		privs = player->getPlayerSAO()->getPrivs();
	}
	// Else check from DB
	else {
		m_server->getGameDatabase()->loadUserPrivs(playerid, privs);
	}

	if (privs.find(priv) != privs.end()) {
		ws << "Player " << playername.c_str() << " already have this privilege." << std::endl;
		m_server->SendChatMessage(peer_id, ws.str());
		return true;
	}

	m_server->getGameDatabase()->addPrivs(playerid, {priv});

	// If player is connected, send him the modification
	if (player) {
		player->getPlayerSAO()->addPriv(priv);
		m_server->reportPrivsModified(playername);
	}

	ws << "Grant " << priv.c_str() << " priv to " << playername.c_str() << std::endl;
	m_server->SendChatMessage(peer_id, ws.str());

	return true;
}

bool ChatHandler::handleCommand_revoke_priv(const std::string &args, const u16 peer_id)
{
	std::vector<std::string> commandline;
	std::stringstream ss(args);
	std::string item;
	while (std::getline(ss, item, ' ')) {
		commandline.push_back(item);
	}

	if (commandline.size() != 2) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help privs revoke");
		return true;
	}

	std::string playername = commandline[0], priv = commandline[1];
	std::wstringstream ws;

	u32 playerid = m_server->getAuthDatabase()->userExists(playername);
	if (!playerid) {
		ws << "Player " << playername.c_str() << " doesn't exists." << std::endl;
		m_server->SendChatMessage(peer_id, ws.str());
		return true;
	}

	m_server->getGameDatabase()->removePrivs(playerid, {priv});

	// If player is connected, send him the modification
	RemotePlayer* player = m_server->getEnv().getPlayer(playername.c_str());
	if (player) {
		player->getPlayerSAO()->removePriv(priv);
		m_server->reportPrivsModified(playername);
	}

	ws << "Privilege '" << priv.c_str() << "' revoked for user " << playername.c_str() << std::endl;
	m_server->SendChatMessage(peer_id, ws.str());

	return true;
}

/**
 * @brief ChatHandler::handleCommand_home_set
 * @param args
 * @param peer_id
 * @return false on fail, true on success
 */
bool ChatHandler::handleCommand_home_set(const std::string &args, const u16 peer_id)
{
	if (!g_settings->get(BOOLSETTING_ENABLE_MOD_SETHOME)) {
		m_server->SendChatMessage(peer_id, L"Mod 'home' not active");
		return true;
	}

	if (!args.empty()) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help home set");
		return true;
	}

	PlayerSAO* sao = m_server->getPlayerSAO(peer_id);
	if (!sao) {
		return true;
	}

	sao->setHome(sao->getBasePosition());

	std::wstringstream ws;
	ws << "Home set to (" << sao->getBasePosition().X / BS
	   << ", " << sao->getBasePosition().Y / BS
	   << ", " << sao->getBasePosition().Z / BS
	   << ") " << std::endl;
	m_server->SendChatMessage(peer_id, ws.str());

	return true;
}

/**
 * @brief ChatHandler::handleCommand_home_go
 * @param args
 * @param peer_id
 * @return false on fail, true on success
 *
 *
 */
bool ChatHandler::handleCommand_home_go(const std::string &args, const u16 peer_id)
{
	if (!g_settings->get(BOOLSETTING_ENABLE_MOD_SETHOME)) {
		m_server->SendChatMessage(peer_id, L"Mod 'home' not active");
		return true;
	}

	if (!args.empty()) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help home go");
		return true;
	}

	PlayerSAO* sao = m_server->getPlayerSAO(peer_id);
	if (!sao) {
		return true;
	}

	if (!sao->tryTeleport())
		return true;

	return true;
}

bool ChatHandler::handleCommand_time_set(const std::string &args, const u16 peer_id)
{
	std::vector<std::string> commandline;
	std::stringstream ss(args);
	std::string item;
	while (std::getline(ss, item, ' ')) {
		commandline.push_back(item);
	}

	if (commandline.size() > 1) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help time set");
		return true;
	}

	const std::string str_time = commandline[0];

	std::smatch rem;
	const static std::regex re_time_int("^([0-9]+)$");
	const static std::regex re_time_str("^([0-9]{1,2})[:]([0-9]{1,2})$");
	std::wstringstream ws;

	if (std::regex_search(str_time, rem, re_time_int)) {
		u32 rawtime = mystoi(rem.str(1));
		m_server->setTimeOfDay(rawtime);
		ws << "Time set to : " << rawtime << " (raw)" << std::endl;
	}
	else if (std::regex_search(str_time, rem, re_time_str)) {
		u32 hour = mystoi(rem.str(1), 0, 24), minutes = mystoi(rem.str(2), 0, 60);
		m_server->setTimeOfDay((hour * 60 + minutes) * 16);
		ws << "Time set to : " << hour << ":";
		ws << std::setfill((wchar_t)'0') << std::setw(2) << minutes << std::endl;
	}
	else {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help time set");
		return true;
	}

	m_server->SendChatMessage(peer_id, ws.str());

	return true;
}

bool ChatHandler::handleCommand_time_get(const std::string &args, const u16 peer_id)
{
	if (!args.empty()) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help time");
		return true;
	}

	std::wstringstream ws;
	ws << "Time is : " <<  epixelTimeToString(m_server->getEnv().getTimeOfDay() / 10).c_str() << std::endl;
	m_server->SendChatMessage(peer_id, ws.str());

	return true;
}

/**
 * @brief ChatHandler::handleCommand_teleport
 * @param args
 * @param peer_id
 * @return
 *
 * Teleport player to position <X>,<Y>,<Z>
 * Teleport <player> to position <X>,<Y>,<Z>
 * Teleport <player> to <to_player>
 */
bool ChatHandler::handleCommand_teleport(const std::string &args, const u16 peer_id)
{
	if (args.empty()) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help teleport");
		return true;
	}

	PlayerSAO* sao = m_server->getPlayerSAO(peer_id);
	if (!sao) {
		return true;
	}

	std::smatch rem;
	// Regex <player> <X>,<Y>,<Z>
	static const std::regex re_player_pos("^([a-zA-Z0-9_]*)?[ ]?([-]?[0-9]*),[ ]?([-]?[0-9]*),[ ]?([-]?[0-9]*)$");
	// Regex <player> <to_player>
	static const std::regex re_toplayer("^([a-zA-Z0-9_]*)?[ ]?([a-zA-Z0-9_]*)?$");

	if (std::regex_search(args, rem, epixel::regex::position)) {
		v3f new_position(regex::mtof(rem, 1)* BS,
						 regex::mtof(rem, 2) * BS,
						 regex::mtof(rem, 3) * BS);
		sao->setPos(new_position);
		logger.notice("%s teleporting to (%f,%f,%f)", m_server->getPlayerName(peer_id).c_str(),
				new_position.X / BS, new_position.Y / BS, new_position.Z / BS);
		return true;
	} else if (std::regex_search(args, rem, re_player_pos)) {
		if (!m_server->getAuthDatabase()->userExists(rem.str(1))) {
			std::wstringstream ws;
			ws << "Player  '" << rem.str(1).c_str()
			   << "' doesn't exist!" << std::endl;
			m_server->SendChatMessage(peer_id, ws.str());
			return true;
		}

		RemotePlayer* player = m_server->getEnv().getPlayer(rem.str(1).c_str());
		if (!player) {
			std::wstringstream ws;
			ws << "Player '" << rem.str(1).c_str() << "' not connected." << std::endl;
			m_server->SendChatMessage(peer_id, ws.str());
			return true;
		}

		v3f new_position(regex::mtof(rem, 2) * BS,
						 regex::mtof(rem, 3) * BS,
						 regex::mtof(rem, 4) * BS);

		if (peer_id == player->getPeerID()) {
			sao->setPos(new_position);
			return true;
		} else {
			PlayerSAO* sao_player = m_server->getPlayerSAO(player->getPeerID());
			if (!sao_player) {
				return true;
			}
			sao_player->setPos(new_position);
			logger.notice("%s teleporting %s to (%f,%f,%f)", m_server->getPlayerName(peer_id).c_str(),
														   rem.str(1).c_str(), new_position.X / BS, new_position.Y / BS, new_position.Z / BS);
			return true;
		}
	} else if (std::regex_search(args, rem, re_toplayer)) {
		std::string str_player_from, str_player_to;
		if (!rem.str(2).empty()) {
			str_player_from = rem.str(1);
			str_player_to = rem.str(2);
		} else {
			str_player_from = m_server->getPlayerName(peer_id);
			str_player_to = rem.str(1);
		}

		std::wstringstream ws;
		if (!m_server->getAuthDatabase()->userExists(str_player_from)) {
			ws << "Player '" << str_player_from.c_str()
			   << "' doesn't exist!" << std::endl;
			m_server->SendChatMessage(peer_id, ws.str());
			return true;
		}
		if (!m_server->getAuthDatabase()->userExists(str_player_to)) {
			ws << "Player '" << str_player_to.c_str()
			   << "' doesn't exist!" << std::endl;
			m_server->SendChatMessage(peer_id, ws.str());
			return true;
		}

		RemotePlayer* player_from = m_server->getEnv().getPlayer(str_player_from.c_str());
		if (!player_from) {
			ws << "Player '" << str_player_from.c_str() << "' not connected." << std::endl;
			m_server->SendChatMessage(peer_id, ws.str());
			return true;
		}

		RemotePlayer* player_to = m_server->getEnv().getPlayer(str_player_to.c_str());
		if (!player_to) {
			ws << "Player '" << str_player_to.c_str() << "' not connected." << std::endl;
			m_server->SendChatMessage(peer_id, ws.str());
			return true;
		}

		PlayerSAO* sao_player_from = m_server->getPlayerSAO(player_from->getPeerID());
		if (!sao_player_from) {
			return true;
		}

		const v3f pos_player_to = player_to->getPosition();
		sao_player_from->setPos(pos_player_to);
		logger.notice("%s teleports %s to %s" , sao->getPlayer()->getName(), str_player_from.c_str(), str_player_to.c_str());
	}
	else {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help teleport");
	}
	return true;
}

/**
 * @brief ChatHandler::handleCommand_give
 * @param args
 * @param peer_id
 * @return
 *
 * Give item to player
 * give <item>
 * give <item> <number>
 * give <player> <item>
 * give <player> <item> <number>
 *
 */
bool ChatHandler::handleCommand_give(const std::string &args, const u16 peer_id)
{
	if (args.empty()) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help give");
		return true;
	}

	PlayerSAO* sao = m_server->getPlayerSAO(peer_id);
	if (!sao) {
		return true;
	}

	std::smatch rem;
	// Regex <item> [number]
	const static std::regex re_item_nbr("^([a-zA-Z0-9_]+:[a-zA-Z0-9_]+)[ ]?([0-9]*)?$");
	// Regex <player> <item> [number]
	const static std::regex re_player_item_nbr("^([a-zA-Z0-9_]*) ([a-zA-Z0-9_]+:[a-zA-Z0-9_]+)[ ]?([0-9]*)?$");

	if (std::regex_search(args, rem, re_player_item_nbr)) {
		std::string str_player = rem.str(1);
		if (!m_server->getAuthDatabase()->userExists(str_player)) {
			std::wstringstream ws;
			ws << "Player '" << str_player.c_str()
			   << "' doesn't exist!" << std::endl;
			m_server->SendChatMessage(peer_id, ws.str());
			return true;
		}

		RemotePlayer* player = m_server->getEnv().getPlayer(str_player.c_str());
		if (!player) {
			std::wstringstream ws;
			ws << "Player '" << str_player.c_str() << "' not connected." << std::endl;
			m_server->SendChatMessage(peer_id, ws.str());
			return true;
		}

		PlayerSAO* sao_player = m_server->getPlayerSAO(player->getPeerID());
		if (!sao_player) {
			return true;
		}

		std::string str_item = rem.str(2);
		Inventory *inv = sao_player->getInventory();
		if (!inv) {
			return true;
		}

		IItemDefManager *idef = m_server->getItemDefManager();
		ItemStack item(str_item, 1, 1, "", idef);
		if (!item.isKnown(idef)) {
			m_server->SendChatMessage(peer_id, L"Item doesn't exist!");
			return true;
		}

		InventoryList *il_main = inv->getList("main");
		if (!il_main) {
			return true;
		}

		if (!rem.str(3).empty()) {
			u16 nbr = mystoi(rem.str(3));
			item.count = nbr;
		}
		if (il_main->roomForItem(item)) {
			il_main->addItem(item);
			logger.notice("%s give '%s' to %s", sao->getPlayer()->getName(), item.getItemString().c_str(), sao_player->getPlayer()->getName());
		} else {
			m_server->SendChatMessage(peer_id, L"Inventory is full!");
			return true;
		}
		((Server*)m_server->getEnv().getGameDef())->SendInventory(sao_player);
	} else if (std::regex_search(args, rem, re_item_nbr)) {
		std::string str_item = rem.str(1);
		Inventory *inv = sao->getInventory();
		if (!inv) {
			return true;
		}

		IItemDefManager *idef = m_server->getItemDefManager();
		ItemStack item(str_item, 1, 1, "", idef);
		if (!item.isKnown(idef)) {
			m_server->SendChatMessage(peer_id, L"Item doesn't exist!");
			return true;
		}
		InventoryList *il_main = inv->getList("main");
		if (!il_main) {
			return true;
		}

		if (!rem.str(2).empty()) {
			u16 nbr = mystoi(rem.str(2));
			item.count = nbr;
		}
		if (il_main->roomForItem(item)) {
			il_main->addItem(item);
		} else {
			m_server->SendChatMessage(peer_id, L"Inventory is full!");
			return true;
		}

		((Server*)m_server->getEnv().getGameDef())->SendInventory(sao);
	} else {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help give");
		return true;
	}

	return true;
}

bool ChatHandler::handleCommand_rules_show(const std::string &args, const u16 peer_id)
{
	if (g_settings->get(BOOLSETTING_ENABLE_RULES_FORM)) {
		m_server->showRulesFormspec(peer_id);
	}
	else {
		m_server->SendChatMessage(peer_id, L"This function was disabled.");
	}
	return true;
}

bool ChatHandler::handleCommand_ignore_add(const std::string &args, const u16 peer_id)
{
	if (args.empty()) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help ignore add");
		return true;
	}

	PlayerSAO* sao = m_server->getPlayerSAO(peer_id);
	if (!sao) {
		return true;
	}

	std::vector<std::string> commandline;
	std::stringstream ss(args);
	std::string item;
	while (std::getline(ss, item, ' ')) {
		commandline.push_back(item);
	}

	if (commandline.size() != 1) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help ignore add");
		return true;
	}

	std::string playername = commandline[0];
	std::wstringstream ws;

	if (sao->isPlayerIgnored(playername)) {
		ws << "You already ignore " << playername.c_str() << "." << std::endl;
		m_server->SendChatMessage(peer_id, ws.str());
		return true;
	}

	sao->ignorePlayer(playername);
	m_server->getGameDatabase()->addPlayerIgnore(sao->getPlayer()->getDBId(), playername);
	ws << "Player " << playername.c_str() << " ignored." << std::endl;
	m_server->SendChatMessage(peer_id, ws.str());
	return true;
}

bool ChatHandler::handleCommand_ignore_remove(const std::string &args, const u16 peer_id)
{
	if (args.empty()) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help ignore remove");
		return true;
	}

	PlayerSAO* sao = m_server->getPlayerSAO(peer_id);
	if (!sao) {
		return true;
	}

	std::vector<std::string> commandline;
	std::stringstream ss(args);
	std::string item;
	while (std::getline(ss, item, ' ')) {
		commandline.push_back(item);
	}

	if (commandline.size() != 1) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help ignore remove");
		return true;
	}

	std::string playername = commandline[0];
	std::wstringstream ws;

	if (!sao->isPlayerIgnored(playername)) {
		ws << "You didn't ignore " << playername.c_str() << "." << std::endl;
		m_server->SendChatMessage(peer_id, ws.str());
		return true;
	}

	sao->unIgnorePlayer(playername);
	m_server->getGameDatabase()->removePlayerIgnore(sao->getPlayer()->getDBId(), playername);
	ws << "Player " << playername.c_str() << " unignored." << std::endl;
	m_server->SendChatMessage(peer_id, ws.str());
	return true;
}

bool ChatHandler::handleCommand_ignore_list(const std::string &args, const u16 peer_id) {
	if (!args.empty()) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help ignore list");
		return true;
	}

	PlayerSAO *sao = m_server->getPlayerSAO(peer_id);
	if (!sao) {
		return true;
	}

	const PlayerIgnoreList pilist = sao->getIgnoreList();
	if (pilist.size() == 0) {
		m_server->SendChatMessage(peer_id, L"You have no ignored player.");
		return true;
	}

	std::wstringstream ws;
	ws << "Ignored players: ";
	std::string ignorelist = join(pilist.begin(), pilist.end(), std::string(", "));
	ws << ignorelist.c_str() << std::endl;

	m_server->SendChatMessage(peer_id, ws.str());
	return true;
}

}
