/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 * Copyright (C) 2010-2013 celeron55, Perttu Ahola <celeron55@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include <httpfetch.h>
#include "playersao.h"
#include "database.h"
#include "environment.h"
#include "filesys.h"
#include "genericobject.h"
#include "inventory.h"
#include "log.h"
#include "nodemetadata.h"
#include "nodetimer.h"
#include "settings.h"
#include "server.h"
#include "scripting_game.h"
#include "tool.h"
#include "util/pointedthing.h"
#include "util/serialize.h"
#include "contrib/awards.h"
#include "contrib/itemsao.h"
#include "contrib/projectilesao.h"
#include "contrib/spell.h"
#include "contrib/teleport.h"
#include "contrib/stats.h"
#include "contrib/areas.h"

const v2s32 playermodel_animframes[PLAYERANIM_MAX] = {
	{0, 79}, // stand
	{162, 166}, // lay
	{168, 187}, // walk
	{189, 198}, // mine
	{200, 219}, // walk_mine
	{81, 160}, // sit
};

static const std::string bones_formspec = "size[8,9]"
"bgcolor[#080808BB;true]"
"background[5,5;1,1;gui_formbg.png;true]"
"listcolors[#00000069;#5A5A5A;#141318;#30434C;#FFF]"
"list[current_name;main;0,0.3;8,4;]"
"list[current_player;main;0,4.85;8,1;]"
"list[current_player;main;0,6.08;8,3;8]"
"image[0,4.85;1,1;gui_hb_bg.png]"
"image[1,4.85;1,1;gui_hb_bg.png]"
"image[2,4.85;1,1;gui_hb_bg.png]"
"image[3,4.85;1,1;gui_hb_bg.png]"
"image[4,4.85;1,1;gui_hb_bg.png]"
"image[5,4.85;1,1;gui_hb_bg.png]"
"image[6,4.85;1,1;gui_hb_bg.png]"
"image[7,4.85;1,1;gui_hb_bg.png]";
/*
	PlayerSAO
*/

// No prototype, PlayerSAO does not need to be deserialized

PlayerSAO::PlayerSAO(ServerEnvironment *env_, RemotePlayer *player_, u16 peer_id_,
		const std::set<std::string> &privs, bool is_singleplayer):
	UnitSAO(env_, v3f(0,0,0)),
	m_player(player_),
	m_peer_id(peer_id_),
	m_inventory(NULL),
	m_damage(0),
	m_last_good_position(0,0,0),
	m_time_from_last_punch(0),
	m_nocheat_dig_pos(32767, 32767, 32767),
	m_nocheat_dig_time(0),
	m_wield_index(0),
	m_position_not_sent(false),
	m_armor_groups_sent(false),
	m_properties_sent(true),
	m_privs(privs),
	m_is_singleplayer(is_singleplayer),
	m_animation_speed(0),
	m_animation_blend(0),
	m_animation_loop(true),
	m_animation_sent(false),
	m_bone_position_sent(false),
	m_attachment_parent_id(0),
	m_attachment_sent(false),
	m_extended_attributes_modified(false),
	m_home(v3f(0,0,0)),
	m_hunger(PLAYER_HUNGER_MAX),
	m_hunger_check_timer(PLAYER_HUNGER_CHECK_TIMER),
	m_exhaustion_check_timer(1),
	// public
	m_physics_override_jump(1),
	m_physics_override_gravity(1),
	m_physics_override_sneak(true),
	m_physics_override_sneak_glitch(true),
	m_physics_override_sent(false)
{
	assert(m_player);	// pre-condition
	assert(m_peer_id != 0);	// pre-condition
	setBasePosition(m_player->getPosition());
	m_inventory = new Inventory(env_->getGameDef()->getItemDefManager());

	// Reinit inventory
	m_inventory->clear();
	m_inventory->addList("main", PLAYER_INVENTORY_SIZE);
	InventoryList *craft = m_inventory->addList("craft", 9);
	craft->setWidth(3);
	m_inventory->addList("craftpreview", 1);
	m_inventory->addList("craftresult", 1);
	m_inventory->setModified(false);

	m_armor_groups["fleshy"] = 100;

	m_prop.hp_max = PLAYER_MAX_HP;
	m_prop.physical = false;
	m_prop.weight = 75;
	m_prop.collisionbox = core::aabbox3d<f32>(-1/3.,-1.0,-1/3., 1/3.,1.0,1/3.);
	// start of default appearance, this should be overwritten by LUA
	m_prop.visual = "upright_sprite";
	m_prop.visual_size = v2f(1, 2);
	m_prop.textures.clear();
	m_prop.textures.push_back("player.png");
	m_prop.textures.push_back("player_back.png");
	m_prop.colors.clear();
	m_prop.colors.push_back(video::SColor(255, 255, 255, 255));
	m_prop.spritediv = v2s16(1,1);
	// end of default appearance
	m_prop.is_visible = true;
	m_prop.makes_footstep_sound = true;
	m_extended_attributes.clear();

	// Area position for area selector
	m_area_pos[0] = m_area_pos[1] = v3s16(0,0,0);

	// Home position
	m_home_timer = g_settings->get(FSETTING_PLAYER_HOME_INTERVAL);

	// Mana
	m_mana_regen_timer = g_settings->get(FSETTING_MANA_REGEN_TIMER);

	// Hunger
	m_hunger_timer_tick = g_settings->get(FSETTING_PLAYER_HUNGER_TICK);

	// Awards
	m_awards.clear();

	// Rules acceptance
	m_rules_acceptance_timer = g_settings->get(FSETTING_RULES_ACCEPTANCE_TIMEOUT);

	m_hp = PLAYER_MAX_HP;
}

PlayerSAO::~PlayerSAO()
{
	delete m_inventory;
	m_extended_attributes.clear();
}

std::string PlayerSAO::getDescription()
{
	return std::string("player ") + m_player->getName();
}

// Called after id has been set and has been inserted in environment
void PlayerSAO::addedToEnvironment(u32 dtime_s)
{
	ServerActiveObject::addedToEnvironment(dtime_s);
	ServerActiveObject::setBasePosition(m_player->getPosition());
	m_player->setPlayerSAO(this);
	m_last_good_position = m_player->getPosition();
}

// Called before removing from environment
void PlayerSAO::removingFromEnvironment()
{
	ServerActiveObject::removingFromEnvironment();
	if(m_player->getPlayerSAO() == this) {
		// Save player before invalidating the SAO.
		// Some attributes are in the SAO and should be saved
		m_env->savePlayer(m_player);

		// Should be set to null AFTER the save
		m_player->setPlayerSAO(NULL);
		m_env->removePlayer(m_player);
	}
}

bool PlayerSAO::isStaticAllowed() const
{
	return false;
}

std::string PlayerSAO::getClientInitializationData(u16 protocol_version)
{
	std::ostringstream os(std::ios::binary);

	writeU8(os, 1); // version
	os<<serializeString(m_player->getName()); // name
	writeU8(os, 1); // is_player
	writeS16(os, getId()); //id
	writeV3F1000(os, m_player->getPosition() + v3f(0,BS*1,0));
	writeF1000(os, m_player->getPlayerSAO()->getYaw());
	writeS16(os, getHP());

	writeU8(os, 6 + m_bone_position.size()); // number of messages stuffed in here
	os<<serializeLongString(getPropertyPacket()); // message 1
	os<<serializeLongString(gob_cmd_update_armor_groups(m_armor_groups)); // 2
	os<<serializeLongString(gob_cmd_update_animation(
		m_animation_range, m_animation_speed, m_animation_blend, m_animation_loop)); // 3
	for(std::unordered_map<std::string, core::vector2d<v3f> >::const_iterator ii = m_bone_position.begin(); ii != m_bone_position.end(); ++ii){
		os<<serializeLongString(gob_cmd_update_bone_position((*ii).first, (*ii).second.X, (*ii).second.Y)); // m_bone_position.size
	}
	os<<serializeLongString(gob_cmd_update_attachment(m_attachment_parent_id, m_attachment_bone, m_attachment_position, m_attachment_rotation)); // 4
	os<<serializeLongString(gob_cmd_update_physics_override(m_physics_override_speed,
			m_physics_override_jump, m_physics_override_gravity, m_physics_override_sneak,
			m_physics_override_sneak_glitch)); // 5
	os << serializeLongString(gob_cmd_update_nametag_attributes(m_prop.nametag_color)); // 6 (GENERIC_CMD_UPDATE_NAMETAG_ATTRIBUTES) : Deprecated, for backwards compatibility only.

	// return result
	return os.str();
}

void PlayerSAO::getStaticData(std::string &static_data) const
{
	FATAL_ERROR("Deprecated function (?)");
}

bool PlayerSAO::isAttached()
{
	if(!m_attachment_parent_id)
		return false;
	// Check if the parent still exists
	ServerActiveObject *obj = m_env->getActiveObject(m_attachment_parent_id);
	if(obj)
		return true;
	return false;
}

void PlayerSAO::onDie()
{
	// Awards
	m_env->getGameDef()->applyAwardStep(this, "", epixel::ACHIEVEMENT_DEATH);

	// @TODO, actions to do when a player dies
	// stop there for creative mode.
	if (g_settings->get(BOOLSETTING_CREATIVE_MODE)) {
		return;
	}

	// No inventory, cancel
	if (!m_inventory) {
		return;
	}

	InventoryList *il_main = m_inventory->getList("main"),
			*il_craft = m_inventory->getList("craft"),
			*il_armor = m_inventory->getList("armor");

	// Main inventory & crafts inventory are mandatory, else
	// player could be died in loading
	if (!il_main || !il_craft || !il_armor) {
		return;
	}

	// No main, no craft, stop
	if (il_main->isEmpty() &&
			il_craft->isEmpty() && il_armor->isEmpty()) {
		return;
	}

	// Armor mod use detached inventory for some things, use it.
	InventoryLocation loc;
	std::string s_armor = m_player->getName();
	s_armor.append("_armor");
	loc.setDetached(s_armor);
	Inventory* armor_detached = m_env->getGameDef()->getInventory(loc);
	InventoryList* il_armor_detached = nullptr;
	if (armor_detached)
		il_armor_detached = armor_detached->getList("armor");

	/*
	 * Now look for spawning bones
	 */

	v3s16 pos(m_base_position.X / BS, m_base_position.Y / BS,
			m_base_position.Z / BS);
	pos.X = floor(pos.X + 0.5);
	pos.Y = floor(pos.Y + 0.5);
	pos.Z = floor(pos.Z + 0.5);

	// If cannot replace current node
	if (!canReplaceNodeWithBones(pos)) {
		// Look at upper node
		if (canReplaceNodeWithBones(v3s16(pos.X, pos.Y + 1, pos.Z))) {
			pos.Y++;
		}
		/*
		 * Player cannot replace current node with bones
		 * Drop all items on the ground
		 */
		else {
			m_env->spawnInventoryOnGround(il_main, m_base_position);
			m_env->spawnInventoryOnGround(il_craft, m_base_position);
			m_env->spawnInventoryOnGround(il_armor, m_base_position);
			if (il_armor_detached) {
				for (u32 i = 0; i < il_armor_detached->getSize(); i++) {
					il_armor_detached->changeItem(i, ItemStack());
				}
			}
			m_env->getGameDef()->SendInventory(this);
			m_env->getGameDef()->sendDetachedInventory(loc.name,PEER_ID_INEXISTENT);
			return;
		}
	}

	INodeDefManager* ndef = m_env->getGameDef()->getNodeDefManager();
	IItemDefManager* idef = m_env->getGameDef()->getItemDefManager();
	MapNode boneNode(ndef, "bones:bones", 0, 0);
	m_env->setNode(pos, boneNode);

	NodeMetadata* meta = m_env->getMap().getNodeMetadataOrCreate(pos, idef);
	Inventory* meta_inv = meta->getInventory();
	meta_inv->deleteList("main");
	InventoryList* meta_il_main = meta_inv->addList("main", 8 * 4);

	// First copy all main inventory to bones inventory
	u32 inv_size = il_main->getSize();
	// Test i < 8 * 4 in case of
	for (u32 i = 0; i < inv_size && i < 8 * 4; i++) {
		ItemStack s = il_main->getItem(i);
		meta_il_main->addItem(i, s);
	}

	// If there is empty slots in bones inventory

	// add armor inventory it it, else spawn on ground
	inv_size = il_armor->getSize();
	for (u32 i = 0; i < inv_size; i++) {
		ItemStack s = il_armor->getItem(i);
		if (s.count == 0)
			continue;

		if (meta_il_main->roomForItem(s)) {
			meta_il_main->addItem(s);
		}
		else {
			m_env->spawnItemActiveObject(s.name, m_base_position, s);
		}
		il_armor->changeItem(i, ItemStack());
		if (il_armor_detached)
			il_armor_detached->changeItem(i, ItemStack());
	}

	// add crafts inventory to it, else spawn on ground
	inv_size = il_craft->getSize();
	for (u32 i = 0; i < inv_size; i++) {
		ItemStack s = il_craft->getItem(i);
		if (s.count == 0)
			continue;

		if (meta_il_main->roomForItem(s)) {
			meta_il_main->addItem(s);
		}
		else {
			m_env->spawnItemActiveObject(s.name, m_base_position, s);
		}
	}

	// Cleanup player inventory
	il_main->clearItems();
	il_craft->clearItems();

	meta->setString("owner", m_player->getName());
	meta->setString("formspec", bones_formspec);
	meta->setString("time", "0");
	m_env->getGameDef()->getMap().setNodeTimer(pos,NodeTimer(10.0f,0));
	std::stringstream ss;
	ss << m_player->getName() << "'s fresh bones";
	meta->setString("infotext", ss.str());

	m_env->getGameDef()->SendInventory(this);
	m_env->getGameDef()->sendDetachedInventory(loc.name,PEER_ID_INEXISTENT);
	m_env->getGameDef()->sendMetadataChanged(pos);
}

bool PlayerSAO::onUseItem(ItemStack &stack, const PointedThing &pointed)
{
	IItemDefManager* idef = m_env->getGameDef()->getItemDefManager();
	ItemDefinition itemdef = stack.getDefinition(idef);

	// If it's a TOOL with projectiles, like a bow, handle it there directly
	if (itemdef.extended_type == epixel::ITEM_EXTTYPE_TOOL_PROJECTILE) {
		throwProjectile(stack, &itemdef);
		return true;
	}

	// Else it's other thing, like eatable item for example

	// Atm only healing items are handled there. Also check if stack not empty
	if (stack.count == 0 || itemdef.eat_amount == 0) {
		return false;
	}

	// Take 1 item on use
	stack.takeItem(1);

	// Eating handling, condition is useless now but will be useful later
	if (itemdef.eat_amount != 0) {
		m_hunger += itemdef.eat_amount;
		if (m_hunger > 20) {
			m_hunger = 20;
		}

		// @TODO damages over time for the meat

		s32 hunger = m_hunger;
		m_env->getGameDef()->SendHUDChange(m_peer_id,
				getPlayer()->getHuds()->getHudId(PLAYERHUD_HUNGER_FG), HUD_STAT_NUMBER, &hunger);
	}

	if (itemdef.on_use_replace_with != "") {
		ItemStack newstack(itemdef.on_use_replace_with, 1, idef);
		if (stack.count == 0) {
			stack.addItem(newstack, idef);
		}
		else if (InventoryList* invList = m_inventory->getList("main")) {
			if (invList->roomForItem(newstack)) {
				invList->addItem(newstack);
			}
			// No space, place on ground
			else {
				m_env->spawnItemActiveObject(itemdef.on_use_replace_with, m_base_position, newstack);
			}
		}
	}
	return true;
}

void PlayerSAO::throwProjectile(const ItemStack &stack, const ItemDefinition* itemdef)
{
	// GCD not ready, cancel
	if (m_global_spell_cooldown > 0.0f) {
		logger.debug("Player %s try to throwProjectile but GCD is not ready yet", m_player->getName());
		return;
	}

	InventoryList* invList = m_inventory->getList("main");
	if (!invList) {
		return;
	}

	u32 slotId = 0xFFFFFFFF;
	if (invList->getFirstSlotWithExtType(epixel::ITEM_EXTTYPE_PROJECTILE, slotId)) {
		const ItemStack stack2 = invList->getItem(slotId);
		const std::string &projectileName = stack2.getDefinition(m_env->getGameDef()->getItemDefManager()).name;
		u32 spellId = m_env->getGameDef()->getSpellId(projectileName);
		if (spellId == SPELL_UNK_ID) {
			logger.error("Spell not found for projectile %s, please fix it !", projectileName.c_str());
			return;
		}

		if (castBallisticSpell(spellId, 0.5f)) {
			invList->takeItem(slotId, 1);
			// Set gcd to spell timer
			m_global_spell_cooldown = m_env->getGameDef()->getSpell(spellId)->timer;
		}
		logger.info("Player %s throw projectile from his %s", m_player->getName(), itemdef->name.c_str());
	}
	else {
		logger.debug("No ammo for throwing projectiles");
	}
}

// @TODO, import this into UnitSAO
bool PlayerSAO::castBallisticSpell(const u32 spellId, float y_cast_offset) const
{
	epixel::Spell* spell = m_env->getGameDef()->getSpell(spellId);
	if (!spell) {
		// This should not occur
		assert(false);
	}

	// It's more realistic to use this multiplier to place projectile in a better place
	v3f spellspawn_pos = v3f(m_base_position.X,	m_base_position.Y + 8.5, m_base_position.Z);
	spellspawn_pos.Y += y_cast_offset;
	if (epixel::ProjectileSAO* spellProj = m_env->spawnProjectileActiveObject(spellId, getId(), spellspawn_pos)) {
		v3s16 npos = floatToInt(spellspawn_pos, BS);
		if (getType() == ACTIVEOBJECT_TYPE_PLAYER) {
			PlayerSAO* player = (PlayerSAO*)this;
			logger.info("Player %s casts ballistic spell %d at (%d,%d,%d)",
					player->getPlayer()->getName(), spellId, npos.X, npos.Y, npos.Z);
		}
		else {
			logger.info("Unit %d casts ballistic spell %d at (%d,%d,%d)", m_id, spellId, npos.X, npos.Y, npos.Z);
		}

		spellProj->applyBallistics(getRadPitch(), getRadYaw());
		// Remove sound hardcode for this, use DB
		m_env->getGameDef()->playSound("throwing_bow_sound", npos);
		return true;
	}

	return false;
}

static const u32 dir_to_wallmounted(const v3s16& dir)
{
	if (abs(dir.Y) > std::max(abs(dir.X), abs(dir.Z))) {
		if (dir.Y < 0) return 0;
		else return 1;
	}
	else if (abs(dir.X) > abs(dir.Z)) {
		if (dir.X < 0) return 3;
		else return 2;
	}
	else {
		if (dir.Z < 0) return 5;
		return 4;
	}
}

static u32 dir_to_facedir(v3s16 dir) {
	if (abs(dir.X) > abs(dir.Z)) {
		if (dir.X < 0) return 3;
		else return 1;
	}
	else {
		if (dir.Z < 0) return 2;
		else return 0;
	}
}

bool PlayerSAO::onPlaceItem(ItemStack &stack, const PointedThing &pointed)
{
	IItemDefManager* idef = m_env->getGameDef()->getItemDefManager();
	INodeDefManager* ndef = m_env->getGameDef()->getNodeDefManager();

	MapNode n_under = m_env->getMap().getNode(pointed.node_undersurface);
	const ContentFeatures &f_under = ndef->get(n_under);

	// If we can rightclick under, handle it. If rightclick is false, let LUA handle it
	if (f_under.rightclickable) {
		return rightClick(f_under);
	}

	// It's not a coreside item, don't handle it there
	const ItemDefinition &itemdef = idef->get(stack.name);
	if (!itemdef.coreside) {
		return false;
	}

	const ContentFeatures &f = ndef->get(ndef->getId(stack.name));
	if (itemdef.type != ITEM_NODE || pointed.type != POINTEDTHING_NODE) {
		return false;
	}

	// Awards
	m_env->getGameDef()->applyAwardStep(this, f.name, epixel::ACHIEVEMENT_PLACE);

	MapNode n_above = m_env->getMap().getNode(pointed.node_abovesurface);

	if (n_above.getContent() == CONTENT_IGNORE || n_under.getContent() == CONTENT_IGNORE) {
		logger.info("%s tried to place node in unloaded position.", m_player->getName());
		return true;
	}

	if (!ndef->get(n_above).buildable_to && !f_under.buildable_to) {
		logger.info("%s tried to place node in invalid position.", m_player->getName());
		return true;
	}

	v3s16 place_to = pointed.node_abovesurface;

	if (f_under.buildable_to) {
		place_to = pointed.node_undersurface;
	}

	if (!m_env->getGameDef()->getAreaMgr()->canInteract(intToFloat(place_to, 1), m_player->getDBId())) {
		logger.notice("%s tried to place %s at protected position.", m_player->getName(), f.name.c_str());
		m_env->getGameDef()->SendChatMessage(m_peer_id, L"You are not allowed to place a node here. It's a "
			L"restricted area!");
		return true;
	}

	MapNode n_new = MapNode(ndef, f.name, 0, 0);
	if (f.param_type_2 == CPT2_WALLMOUNTED) {
		n_new.param2 = (u8) dir_to_wallmounted(
				v3s16(pointed.node_undersurface.X - pointed.node_abovesurface.X,
					pointed.node_undersurface.Y - pointed.node_abovesurface.Y,
					pointed.node_undersurface.Z - pointed.node_abovesurface.Z));
	}
	else if (f.param_type_2 == CPT2_FACEDIR) {
		n_new.param2 = (u8) dir_to_facedir(
				v3s16(pointed.node_abovesurface.X - (m_base_position.X / BS),
					  pointed.node_abovesurface.Y - (m_base_position.Y / BS),
					  pointed.node_abovesurface.Z - (m_base_position.Z / BS)));
	}

	// @TODO check attached nodes

	m_env->setNode(place_to, n_new);
	stack.takeItem(1);

	// Add exhaustion on place
	addExhaustion(g_settings->get(FSETTING_PLAYER_EXHAUS_ONPLACE));
	logger.notice("%s places node %s at (%d,%d,%d)", m_player->getName(), f.name.c_str(),
			pointed.node_abovesurface.X,
			pointed.node_abovesurface.Y,
			pointed.node_abovesurface.Z);

	if (g_settings->get(BOOLSETTING_ENABLE_STATSERVER)) {
		httppost_async_stat(new epixel::PlayerStat_PlaceNode(m_player->getDBId(), f.name));
	}

	return true;
}

bool PlayerSAO::onDropItem(ItemStack &stack)
{
	v3f pos = m_base_position;
	pos.Y += 10.2;

	if (epixel::ItemSAO* sao = m_env->spawnItemActiveObject(stack.name, pos, stack)) {
		float pitch = getRadPitch();
		float yaw = getRadYaw();
		v3f v(cos(pitch)*cos(yaw), sin(pitch), cos(pitch) * sin(yaw));
		v.X *= 20;
		v.Y = v.Y * 20 + 20;
		v.Z *= 20;
		stack.takeItem(stack.count);
		sao->setVelocity(v);
	}

	return true;
}

void PlayerSAO::onDigNode(v3s16 pos, MapNode n)
{
	IItemDefManager* idef = m_env->getGameDef()->getItemDefManager();
	const ContentFeatures &f = m_env->getGameDef()->getNodeDefManager()->get(n);
	v3f pf = intToFloat(pos, 1), pf2 = intToFloat(pos, BS);

	// Check if node is diggable
	if (!f.diggable) {
		return;
	}

	// Check we can interact with node
	if (!m_env->getGameDef()->getAreaMgr()->canInteract(pf, m_player->getDBId())) {
		logger.info("Player %s tried to interact at protected position (%d,%d,%d) but is not allowed",
				m_player->getName(), pf.X, pf.Y, pf.Z);
		m_env->getGameDef()->SendChatMessage(m_peer_id, L"You are not allowed to dig a node here. It's a "
			L"restricted area!");
		return;
	}

	ToolCapabilities tc = getWieldedItem().getToolCapabilities(idef);
	if (!g_settings->get(BOOLSETTING_CREATIVE_MODE)) {
		DigParams dp = getDigParams(f.groups, &tc);
		ItemStack wield = getWieldedItem();
		wield.addWear(dp.wear, idef);
		setWieldedItem(wield);
	}

	if (f.droppable) {
		m_env->handleNodeDrops(f, pf2, this);

	}

	// Awards
	m_env->getGameDef()->applyAwardStep(this, f.name, epixel::ACHIEVEMENT_DIG);

	m_env->removeNode(pos);
	logger.notice("%s digs %s at (%d,%d,%d)", m_player->getName(), f.name.c_str(),
			pos.X, pos.Y, pos.Z);

	if (g_settings->get(BOOLSETTING_ENABLE_STATSERVER)) {
		httppost_async_stat(new epixel::PlayerStat_Dig(m_player->getDBId(), f.name));
	}
}

bool PlayerSAO::canReplaceNodeWithBones(v3s16 pos)
{
	epixel::AreaMgr* areaMgr = m_env->getGameDef()->getAreaMgr();
	MapNode n = m_env->getMap().getNode(pos);
	const ContentFeatures &f = m_env->getGameDef()->getNodeDefManager()->get(n);

	// If unknown node, look at protections
	if (n.getContent() == CONTENT_IGNORE) {
		return areaMgr->canInteract(intToFloat(pos, 1), m_player->getDBId());
	}

	// Allow replacing air & liquidtype
	if (n.getContent() == CONTENT_AIR || f.liquid_type != LIQUID_NONE) {
		return true;
	}

	// If other node
	return f.buildable_to && areaMgr->canInteract(intToFloat(pos, 1), m_player->getDBId());
}

bool PlayerSAO::canSendChatMessage()
{
	// Rate limit messages
	u32 now = time(NULL);
	float time_passed = now - m_last_chat_message_sent;
	m_last_chat_message_sent = now;

	float max_messages_rate = g_settings->get(FSETTING_MAX_MESSAGES_PER_10SEC);
	m_chat_message_allowance += time_passed * (max_messages_rate / 8.0f);
	if (m_chat_message_allowance > max_messages_rate) {
		m_chat_message_allowance = max_messages_rate;
	}

	if (m_chat_message_allowance < 1.0f) {
		std::wstringstream ws;
		ws << L"You cannot send more messages. You are limited to " << max_messages_rate << " messages per 10 seconds.";
		m_env->getGameDef()->SendChatMessage(m_peer_id, ws.str());
		logger_chat.notice("Player %s chat limited due to excessive message amount.", m_player->getName());

		// Kick player if flooding is too intensive
		m_message_rate_overhead++;
		if (m_message_rate_overhead > g_settings->get(U16SETTING_MESSAGE_RATE_KICK)) {
			m_env->getGameDef()->DenyAccess_Legacy(m_peer_id, L"You have been kicked due to message flooding.");
		}
		return false;
	}

	// Reinit message overhead
	if (m_message_rate_overhead > 0) {
		m_message_rate_overhead = 0;
	}
	m_chat_message_allowance -= 1.0f;
	return true;
}

void PlayerSAO::addExhaustion(float modifier)
{
	m_exhaus += modifier;
	if (m_exhaus > PLAYER_EXHAUST_LVL) {
		m_exhaus = 0;
		if (m_hunger > 0) {
			m_hunger--;
			s32 hunger = m_hunger;
			m_env->getGameDef()->SendHUDChange(m_peer_id,
					getPlayer()->getHuds()->getHudId(PLAYERHUD_HUNGER_FG), HUD_STAT_NUMBER, &hunger);
		}
	}
}


void PlayerSAO::step(float dtime, bool send_recommended)
{
	UnitSAO::step(dtime, send_recommended);
	INodeDefManager* ndef = m_env->getGameDef()->getNodeDefManager();

	// Tick for rules, if enabled & non accepted & !singleplayer
	if (!m_rules_accepted &&
#ifndef SERVER
			!m_env->getGameDef()->isSingleplayer() &&
#endif
			g_settings->get(BOOLSETTING_ENABLE_RULES_FORM) &&
			g_settings->get(FSETTING_RULES_ACCEPTANCE_TIMEOUT) != 0) {
		if (m_rules_acceptance_timer < 0) {
			m_env->getGameDef()->DenyAccessVerCompliant(m_peer_id, m_player->getProtocolVersion(),
				SERVER_ACCESSDENIED_CUSTOM_STRING, "You have been disconnected because you didn't accept the server's rules.");
			logger.notice("Player %s was disconnected because he didn't accept server rules.", m_player->getName());
			return;
		}
		else {
			m_rules_acceptance_timer -= dtime;
		}
	}

	// Tick for node damages
	m_node_damage_tick -= dtime;
	if (m_node_damage_tick < 0.0f) {
		m_node_damage_tick = 1.0f;
		// Feet, middle and head
		v3s16 p1 = floatToInt(m_base_position + v3f(0, BS * 0.1, 0), BS);
		MapNode n1 = m_env->getMap().getNode(p1);
		v3s16 p2 = floatToInt(m_base_position + v3f(0, BS * 0.8, 0), BS);
		MapNode n2 = m_env->getMap().getNode(p2);
		v3s16 p3 = floatToInt(m_base_position + v3f(0, BS * 1.6, 0), BS);
		MapNode n3 = m_env->getMap().getNode(p3);

		u32 damage_per_second = 0;
		damage_per_second = MYMAX(damage_per_second,
								  ndef->get(n1).damage_per_second);
		damage_per_second = MYMAX(damage_per_second,
								  ndef->get(n2).damage_per_second);
		damage_per_second = MYMAX(damage_per_second,
								  ndef->get(n3).damage_per_second);

		if (damage_per_second != 0) {
			setHP(m_hp - damage_per_second);
			m_env->getGameDef()->SendPlayerHPOrDie(this);
		}
	}

	// Tick for breath & drowning
	/*
		Drowning
	*/
	m_drowning_interval -= dtime;
	if (m_drowning_interval <= 0.0f) {
		m_drowning_interval = g_settings->get(FSETTING_DROWNING_INTERVAL);
		// head
		v3s16 p = floatToInt(m_base_position + v3f(0, BS*1.6, 0), BS);
		MapNode n = m_env->getMap().getNode(p);
		const ContentFeatures &c = ndef->get(n);
		u8 drowning_damage = c.drowning;
		if (drowning_damage > 0 && getHP() > 0) {
			u16 breath = getBreath();
			if(breath > 10) {
				breath = 11;
			}

			if(breath > 0) {
				breath--;
			}

			setBreath(breath);
		}

		if(!getBreath() && drowning_damage > 0) {
			setHP(m_hp - drowning_damage);
			m_env->getGameDef()->SendPlayerHPOrDie(this);
		}
	}

	m_breathing_interval -= dtime;
	if (m_breathing_interval <= 0.0f) {
		m_breathing_interval = g_settings->get(FSETTING_BREATH_INTERVAL);
		// head
		v3s16 p = floatToInt(m_base_position + v3f(0, BS*1.6, 0), BS);
		MapNode n = m_env->getMap().getNode(p);
		const ContentFeatures &c = ndef->get(n);
		if (getHP() && c.drowning == 0) {
			u16 breath = getBreath();
			if (breath <= 10) {
				breath++;
				setBreath(breath);
			}
		}
	}

	// Tick for hunger
	if (m_hunger_timer_tick < 0) {
		s32 hunger = m_hunger;
		if(hunger - 1 >= 0) {
			hunger--;
		}

		m_hunger = hunger;
		m_env->getGameDef()->SendHUDChange(m_peer_id,
				getPlayer()->getHuds()->getHudId(PLAYERHUD_HUNGER_FG),
				HUD_STAT_NUMBER, &hunger);

		m_hunger_timer_tick = g_settings->get(FSETTING_PLAYER_HUNGER_TICK);
	}
	else
		m_hunger_timer_tick -= dtime;

	// GCD timer
	m_global_spell_cooldown -= dtime;
	if (m_global_spell_cooldown <= 0.0f) {
		m_global_spell_cooldown = 0.0f;
	}

	// Hunger verification, do player dmg if hunger
	if (m_hunger_check_timer < 0) {
		if (m_hunger > 15 && m_hp > 0 && m_breath > 0) {
			setHP(m_hp + 1);
			m_env->getGameDef()->SendPlayerHPOrDie(this);
		}
		// If we have hunger
		else if(m_hunger < 1) {
			setHP(m_hp - 1);
			m_env->getGameDef()->SendPlayerHPOrDie(this);
		}
		m_hunger_check_timer = PLAYER_HUNGER_CHECK_TIMER;
	}
	else {
		m_hunger_check_timer -= dtime;
	}

	if (m_exhaustion_check_timer < 0) {
		if (getPlayer()->control.up || getPlayer()->control.down ||
				getPlayer()->control.left || getPlayer()->control.right) {
			addExhaustion(g_settings->get(FSETTING_PLAYER_EXHAUS_ONMOVE));
		}

		m_exhaustion_check_timer = 1;
	}
	else {
		m_exhaustion_check_timer -= dtime;
	}

	float anim_speed = 30;

	if (getPlayer()->control.sneak) {
		anim_speed /= 2;
	}

	if (m_hp == 0) {
		setPlayerAnimation(PLAYERANIM_LAY, anim_speed);
	}
	else {
		if (m_anim_state == PLAYERANIMSTATE_LAY ||
				m_anim_state == PLAYERANIMSTATE_SIT) {
			// Do no animation in these cases, we are sitting or laying, no animation
		}
		else if (getPlayer()->control.up || getPlayer()->control.down ||
				 getPlayer()->control.left || getPlayer()->control.right) {
			if (getPlayer()->control.LMB) {
				setPlayerAnimation(PLAYERANIM_WALKMINE, anim_speed);
			}
			else {
				setPlayerAnimation(PLAYERANIM_WALK, anim_speed);
			}
		}
		else if (getPlayer()->control.LMB) {
			setPlayerAnimation(PLAYERANIM_MINE, anim_speed);
		}
		else {
			setPlayerAnimation(PLAYERANIM_STAND, anim_speed);
		}
	}

	if(!m_properties_sent)
	{
		m_properties_sent = true;
		std::string str = getPropertyPacket();
		// create message and add to list
		ActiveObjectMessage aom(getId(), true, str);
		m_messages_out.push(aom);
	}

	// If attached, check that our parent is still there. If it isn't, detach.
	if(m_attachment_parent_id && !isAttached())
	{
		m_attachment_parent_id = 0;
		m_attachment_bone = "";
		m_attachment_position = v3f(0,0,0);
		m_attachment_rotation = v3f(0,0,0);
		m_player->setPosition(m_last_good_position);
		m_env->getGameDef()->SendMovePlayer(m_peer_id);
	}

	// Set lag pool maximums based on estimated lag
	static const float LAG_POOL_MIN = 5.0;
	float lag_pool_max = m_env->getMaxLagEstimate() * 2.0;
	if(lag_pool_max < LAG_POOL_MIN)
		lag_pool_max = LAG_POOL_MIN;
	m_dig_pool.setMax(lag_pool_max);
	m_move_pool.setMax(lag_pool_max);

	// Increment cheat prevention timers
	m_dig_pool.add(dtime);
	m_move_pool.add(dtime);
	m_time_from_last_punch += dtime;
	m_nocheat_dig_time += dtime;

	// Each frame, parent position is copied if the object is attached, otherwise it's calculated normally
	// If the object gets detached this comes into effect automatically from the last known origin
	if(isAttached())
	{
		v3f pos = m_env->getActiveObject(m_attachment_parent_id)->getBasePosition();
		m_last_good_position = pos;
		m_player->setPosition(pos);
	}

	if(!send_recommended)
		return;

	// If the object is attached client-side, don't waste bandwidth sending its position to clients
	if(m_position_not_sent && !isAttached())
	{
		m_position_not_sent = false;
		v3f pos;
		if(isAttached()) // Just in case we ever do send attachment position too
			pos = m_env->getActiveObject(m_attachment_parent_id)->getBasePosition();
		else
			pos = m_player->getPosition() + v3f(0,BS*1,0);
		std::string str = gob_cmd_update_position(
			pos,
			v3f(0,0,0),
			v3f(0,0,0),
			m_yaw,
			true,
			false,
			g_settings->get(FSETTING_DEDICATED_SERVER_STEP)
		);
		// create message and add to list
		ActiveObjectMessage aom(getId(), false, str);
		m_messages_out.push(aom);
	}

	if(!m_armor_groups_sent) {
		m_armor_groups_sent = true;
		std::string str = gob_cmd_update_armor_groups(
				m_armor_groups);
		// create message and add to list
		ActiveObjectMessage aom(getId(), true, str);
		m_messages_out.push(aom);
	}

	if(!m_physics_override_sent){
		m_physics_override_sent = true;
		std::string str = gob_cmd_update_physics_override(m_physics_override_speed,
				m_physics_override_jump, m_physics_override_gravity,
				m_physics_override_sneak, m_physics_override_sneak_glitch);
		// create message and add to list
		ActiveObjectMessage aom(getId(), true, str);
		m_messages_out.push(aom);
	}

	if(!m_animation_sent){
		m_animation_sent = true;
		std::string str = gob_cmd_update_animation(
			m_animation_range, m_animation_speed, m_animation_blend, m_animation_loop);
		// create message and add to list
		ActiveObjectMessage aom(getId(), true, str);
		m_messages_out.push(aom);
	}

	if(!m_bone_position_sent){
		m_bone_position_sent = true;
		for(std::unordered_map<std::string, core::vector2d<v3f> >::const_iterator ii = m_bone_position.begin(); ii != m_bone_position.end(); ++ii){
			std::string str = gob_cmd_update_bone_position((*ii).first, (*ii).second.X, (*ii).second.Y);
			// create message and add to list
			ActiveObjectMessage aom(getId(), true, str);
			m_messages_out.push(aom);
		}
	}

	if(!m_attachment_sent){
		m_attachment_sent = true;
		std::string str = gob_cmd_update_attachment(m_attachment_parent_id, m_attachment_bone, m_attachment_position, m_attachment_rotation);
		// create message and add to list
		ActiveObjectMessage aom(getId(), true, str);
		m_messages_out.push(aom);
	}

	m_home_timer -= dtime;
	if (m_home_timer <= 0.0f)
		m_home_timer = 0.0f;

	if (g_settings->get(BOOLSETTING_ENABLE_MOD_MANA))
		timer_regenMana(dtime);
}

void PlayerSAO::setBasePosition(const v3f &position)
{
	// This needs to be ran for attachments too
	ServerActiveObject::setBasePosition(position);
	m_position_not_sent = true;
}

void PlayerSAO::setPos(const v3f& pos)
{
	if(isAttached())
		return;
	m_player->setPosition(pos);
	// Movement caused by this command is always valid
	m_last_good_position = pos;
	m_env->getGameDef()->SendMovePlayer(m_peer_id);
}

void PlayerSAO::moveTo(v3f pos, bool continuous)
{
	if(isAttached())
		return;
	m_player->setPosition(pos);
	// Movement caused by this command is always valid
	m_last_good_position = pos;
	m_env->getGameDef()->SendMovePlayer(m_peer_id);
}

void PlayerSAO::setYaw(const float yaw, bool send)
{
	if (yaw != m_yaw)
		m_player->setDirty();

	m_yaw = yaw;

	if (send)
		m_env->getGameDef()->SendMovePlayer(m_peer_id);
}

void PlayerSAO::setBreath(const u16 breath)
{
	if (breath != m_breath)
		m_player->setDirty();

	m_breath = breath;
	m_env->getGameDef()->SendPlayerBreath(this);
}

void PlayerSAO::setPitch(const float pitch, bool send)
{
	if (pitch != m_pitch)
		m_player->setDirty();

	m_pitch = pitch;

	if (send)
		m_env->getGameDef()->SendMovePlayer(m_peer_id);
}

void PlayerSAO::doDamage(u16 damage, ServerActiveObject *puncher)
{
	setHP(getHP() - damage);

	m_env->getGameDef()->SendPlayerHPOrDie(this);
	if (puncher && puncher->getType() == ACTIVEOBJECT_TYPE_PLAYER) {
		std::string str = gob_cmd_punched(0, getHP());
		// create message and add to list
		ActiveObjectMessage aom(getId(), true, str);
		m_messages_out.push(aom);
	}
}

int PlayerSAO::punch(v3f dir,
	const ToolCapabilities *toolcap,
	ServerActiveObject *puncher,
	float time_from_last_punch)
{
	// It's best that attachments cannot be punched
	if (isAttached())
		return 0;

	if (!toolcap)
		return 0;

	// No effect if PvP disabled globally and locally
	if (!m_env->getGameDef()->getAreaMgr()->canDoPvp(m_base_position / BS)) {
		if (puncher->getType() == ACTIVEOBJECT_TYPE_PLAYER) {
			std::string str = gob_cmd_punched(0, getHP());
			// create message and add to list
			ActiveObjectMessage aom(getId(), true, str);
			m_messages_out.push(aom);
			return 0;
		}
	}

	HitParams hitparams = getHitParams(m_armor_groups, toolcap,
			time_from_last_punch);

	std::string punchername = "nil";

	if (puncher != 0)
		punchername = puncher->getDescription();

	PlayerSAO *playersao = m_player->getPlayerSAO();

	bool damage_handled = m_env->getScriptIface()->on_punchplayer(playersao,
				puncher, time_from_last_punch, toolcap, dir,
				hitparams.hp);

	// Awards and XP
	if (puncher && puncher->getType() == ACTIVEOBJECT_TYPE_PLAYER &&
			(getHP() - hitparams.hp) <= 0) {
		PlayerSAO *pl = (PlayerSAO*)puncher;
		m_env->getGameDef()->applyAwardStep(pl, "", epixel::ACHIEVEMENT_KILL_PLAYER);
		if (g_settings->get(BOOLSETTING_ENABLE_MOD_XP)) {
				pl->modifyXP(playersao->getXP() * g_settings->get(FSETTING_MOD_XP_WIN_PVP));
				pl->updateHUD_Level();
		}
	}

	if (!damage_handled) {
		setHP(getHP() - hitparams.hp);
	} else { // override client prediction
		if (puncher->getType() == ACTIVEOBJECT_TYPE_PLAYER) {
			std::string str = gob_cmd_punched(0, getHP());
			// create message and add to list
			ActiveObjectMessage aom(getId(), true, str);
			m_messages_out.push(aom);
		}
	}


	if (!damage_handled) {
		logger.notice("Player %s punched by %s, damage %d HP",
				m_player->getName(), punchername.c_str(), hitparams.hp);
	} else {
		logger.notice("Player %s punched by %s, damage %d HP",
				m_player->getName(), punchername.c_str(), hitparams.hp);
	}

	return hitparams.wear;
}

void PlayerSAO::rightClick(ServerActiveObject *clicker)
{
	// @TODO
}

bool PlayerSAO::rightClick(const ContentFeatures& cf)
{
	switch (cf.rc_formspec_type) {
		case epixel::CFRC_TELEPORTER:
			m_env->getGameDef()->showFormspec(m_peer_id, m_env->getTeleportMgr()->getFormSpec(), "teleporter");
			return true;
		default:
			return false;
	}
}

s16 PlayerSAO::readDamage()
{
	s16 damage = m_damage;
	m_damage = 0;
	return damage;
}

void PlayerSAO::setHP(s16 hp)
{
	s16 oldhp = m_hp;

	s16 hp_change = m_env->getScriptIface()->on_player_hpchange(this,
		hp - oldhp);
	if (hp_change == 0)
		return;
	hp = oldhp + hp_change;

	if (hp < 0)
		hp = 0;
	else if (hp > PLAYER_MAX_HP)
		hp = PLAYER_MAX_HP;

	if (g_settings->get(BOOLSETTING_ENABLE_MOD_XP) && hp == 0) {
		setXP(getXP() * g_settings->get(FSETTING_MOD_XP_LOSS_DEAD));
		updateHUD_Level();
	}

	if(hp < oldhp && !g_settings->get(BOOLSETTING_ENABLE_DAMAGE)) {
		return;
	}

	m_hp = hp;

	if (oldhp > hp)
		m_damage += (oldhp - hp);

	// Update properties on death
	if ((hp == 0) != (oldhp == 0))
		m_properties_sent = false;
}

void PlayerSAO::setArmorGroups(const ItemGroupList &armor_groups)
{
	m_armor_groups = armor_groups;
	m_armor_groups_sent = false;
}

ItemGroupList PlayerSAO::getArmorGroups()
{
	return m_armor_groups;
}

void PlayerSAO::setAnimation(v2f frame_range, float frame_speed, float frame_blend, bool frame_loop)
{
	// store these so they can be updated to clients
	m_animation_range = frame_range;
	m_animation_speed = frame_speed;
	m_animation_blend = frame_blend;
	m_animation_loop = frame_loop;
	m_animation_sent = false;
}

void PlayerSAO::getAnimation(v2f *frame_range, float *frame_speed, float *frame_blend, bool *frame_loop)
{
	*frame_range = m_animation_range;
	*frame_speed = m_animation_speed;
	*frame_blend = m_animation_blend;
	*frame_loop = m_animation_loop;
}

void PlayerSAO::setBonePosition(const std::string &bone, v3f position, v3f rotation)
{
	// store these so they can be updated to clients
	m_bone_position[bone] = core::vector2d<v3f>(position, rotation);
	m_bone_position_sent = false;
}

void PlayerSAO::getBonePosition(const std::string &bone, v3f *position, v3f *rotation)
{
	*position = m_bone_position[bone].X;
	*rotation = m_bone_position[bone].Y;
}

void PlayerSAO::setAttachment(int parent_id, const std::string &bone, v3f position, v3f rotation)
{
	// Attachments need to be handled on both the server and client.
	// If we just attach on the server, we can only copy the position of the parent. Attachments
	// are still sent to clients at an interval so players might see them lagging, plus we can't
	// read and attach to skeletal bones.
	// If we just attach on the client, the server still sees the child at its original location.
	// This breaks some things so we also give the server the most accurate representation
	// even if players only see the client changes.

	m_attachment_parent_id = parent_id;
	m_attachment_bone = bone;
	m_attachment_position = position;
	m_attachment_rotation = rotation;
	m_attachment_sent = false;
}

void PlayerSAO::getAttachment(int *parent_id, std::string *bone, v3f *position,
	v3f *rotation)
{
	*parent_id = m_attachment_parent_id;
	*bone = m_attachment_bone;
	*position = m_attachment_position;
	*rotation = m_attachment_rotation;
}

void PlayerSAO::addAttachmentChild(int child_id)
{
	m_attachment_child_ids.insert(child_id);
}

void PlayerSAO::removeAttachmentChild(int child_id)
{
	m_attachment_child_ids.erase(child_id);
}

std::set<int> PlayerSAO::getAttachmentChildIds()
{
	return m_attachment_child_ids;
}

ObjectProperties* PlayerSAO::accessObjectProperties()
{
	return &m_prop;
}

void PlayerSAO::notifyObjectPropertiesModified()
{
	m_properties_sent = false;
}

Inventory* PlayerSAO::getInventory()
{
	return m_inventory;
}
const Inventory* PlayerSAO::getInventory() const
{
	return m_inventory;
}

InventoryLocation PlayerSAO::getInventoryLocation() const
{
	InventoryLocation loc;
	loc.setPlayer(m_player->getName());
	return loc;
}

std::string PlayerSAO::getWieldList() const
{
	return "main";
}

int PlayerSAO::getWieldIndex() const
{
	return m_wield_index;
}

void PlayerSAO::setWieldIndex(int i)
{
	if(i != m_wield_index) {
		m_wield_index = i;
	}
}

void PlayerSAO::disconnected()
{
	m_peer_id = PEER_ID_INEXISTENT;
	m_removed = true;
	if(m_player->getPlayerSAO() == this)
	{
		m_player->setPlayerSAO(NULL);
	}
}

std::string PlayerSAO::getPropertyPacket()
{
	m_prop.is_visible = (true);
	return ServerActiveObject::getPropertyPacket();
}

bool PlayerSAO::checkMovementCheat()
{
	bool cheated = false;
	if(isAttached() || m_is_singleplayer ||
			g_settings->get(BOOLSETTING_DISABLE_ANTICHEAT))
	{
		m_last_good_position = m_player->getPosition();
	}
	else
	{
		/*
			Check player movements

			NOTE: Actually the server should handle player physics like the
			client does and compare player's position to what is calculated
			on our side. This is required when eg. players fly due to an
			explosion. Altough a node-based alternative might be possible
			too, and much more lightweight.
		*/

		float player_max_speed = 0;
		if(hasPriv("fast")) {
			// Fast speed
			player_max_speed = m_player->movement_speed_fast;
		} else {
			// Normal speed
			player_max_speed = m_player->movement_speed_walk;
		}
		// Tolerance. With the lag pool we shouldn't need it.
		//player_max_speed *= 2.5;
		//player_max_speed_up *= 2.5;

		v3f diff = (m_player->getPosition() - m_last_good_position);
		float d_vert = diff.Y;
		diff.Y = 0;
		float d_horiz = diff.getLength();
		float required_time = d_horiz/player_max_speed;
		if(d_vert > 0 && d_vert/player_max_speed > required_time)
			required_time = d_vert/player_max_speed;
		if(m_move_pool.grab(required_time)){
			m_last_good_position = m_player->getPosition();
		} else {
			logger.notice("Player %s moved too fast; resetting position",
					m_player->getName());
			m_player->setPosition(m_last_good_position);
			cheated = true;
		}
	}
	return cheated;
}

bool PlayerSAO::getCollisionBox(aabb3f *toset) {
	//update collision box
	*toset = m_collisionbox;

	toset->MinEdge += m_base_position;
	toset->MaxEdge += m_base_position;

	return true;
}

bool PlayerSAO::collideWithObjects(){
	return true;
}

std::string PlayerSAO::getExtendedAttribute(const std::string &attr)
{
	if (m_extended_attributes.find(attr) == m_extended_attributes.end())
		return "";

	return m_extended_attributes[attr];
}

void PlayerSAO::setExtendedAttribute(const std::string &attr, const std::string &value)
{
	m_extended_attributes[attr] = value;
	m_extended_attributes_modified = true;
}

void PlayerSAO::setHome(const v3f position, bool save)
{
	if (save) {
		GameDatabase* m_game_database = m_env->getGameDef()->getGameDatabase();
		m_game_database->setPlayerHome(m_player);
	}
	m_home = position;
}

void PlayerSAO::resetHomeTimer()
{
	m_home_timer = g_settings->get(FSETTING_PLAYER_HOME_INTERVAL);
}

bool PlayerSAO::tryTeleport()
{
	if (getHome() != v3f(0,0,0)) {
		std::wstringstream ws;
		Server *srv = m_env->getGameDef();
		if (g_settings->get(BOOLSETTING_ENABLE_MOD_MANA)) {
			if (modifyMana(-(g_settings->get(U16SETTING_MANA_COST_HOME))) == 3) {
				setPos(getHome());

				ws << "Teleporting to your home position" << std::endl;
				srv->SendChatMessage(getPeerID(), ws.str());
				return true;
			} else {
				ws << "Not enough mana." << std::endl;
				srv->SendChatMessage(getPeerID(), ws.str());
				return false;
			}
		} else if (isHomeTimerReady()) {
			setPos(getHome());

			ws << "Teleporting to your home position" << std::endl;
			srv->SendChatMessage(getPeerID(), ws.str());

			resetHomeTimer();
			return true;
		} else {
			ws << "Home timer is not ready." << std::endl;
			srv->SendChatMessage(getPeerID(), ws.str());
			return false;
		}
	}
	return false;
}

void PlayerSAO::timer_regenMana(float dtime)
{
	if (m_mana < g_settings->get(U16SETTING_MANA_MAX)) {
		m_mana_regen_timer -= dtime;
		if (m_mana_regen_timer <= 0.0f) {
			m_mana += g_settings->get(U16SETTING_MANA_DEFAULT_REGEN);
			updateHudMana();
			m_mana_regen_timer = g_settings->get(FSETTING_MANA_REGEN_TIMER);
		}
	}
}

int PlayerSAO::modifyMana(s32 mana)
{
	if (mana > 0) {
		if (m_mana == g_settings->get(U16SETTING_MANA_MAX)) {
			m_mana_overhead = 0;
			return 0;
		} else if (mana + m_mana > g_settings->get(U16SETTING_MANA_MAX)) {
			m_mana_overhead = mana + m_mana - g_settings->get(U16SETTING_MANA_MAX);
			m_mana = g_settings->get(U16SETTING_MANA_MAX);
		} else {
			m_mana += mana;
			m_mana_overhead = 0;
		}
		updateHudMana();
		return 2;
	} else if (mana < 0) {
		if (m_mana >= -mana) {
			m_mana += mana;
			updateHudMana();
			m_mana_overhead = 0;
			return 3;
		}
	}
	return 1;
}

void PlayerSAO::updateHudMana()
{
	// SendHUDChange accept only type u32
	u32 mana_hud = m_mana / 10;
	m_env->getGameDef()->SendHUDChange(getPeerID(),
			getPlayer()->getHuds()->getHudId(PLAYERHUD_MANA_FG),
			HUD_STAT_NUMBER,
			&mana_hud);
}

void PlayerSAO::updateHUD_Level()
{
	if (!getPlayer()->getHuds()) {
		return;
	}

	double intpart;
	u32 xp = modf(getLvl(), &intpart)*37;
	std::string areaText;
	try{
		areaText = "lvl : " + std::to_string((int)intpart);
	} catch(std::exception const& e) {
		logger.error("FATAL ERROR when converting double to string (xp)");
		assert(false);
	}
	m_env->getGameDef()->hudChange(getPlayer(),
			getPlayer()->getHuds()->getHudId(PLAYERHUD_LVL), HUD_STAT_TEXT, &areaText);
	m_env->getGameDef()->hudChange(getPlayer(),
			getPlayer()->getHuds()->getHudId(PLAYERHUD_XP_FG), HUD_STAT_NUMBER, &xp);
}

void PlayerSAO::updateAchievementProgress(const u32 id_achievement, const epixel::Achievement &achievement)
{
	epixel::AchievementProgress ap = m_awards[id_achievement];
	if (ap.progress < achievement.target_count) {
		ap.id_achievement = id_achievement;
		ap.progress++;
		if (ap.progress == achievement.target_count) {
			ap.unlocked = true;
			logger.notice("Player %s unlocked: %s => %s", m_player->getName(),
					achievement.name.c_str(), achievement.description.c_str());
			std::wstringstream ws;
			ws << "Congratulation! You unlocked award: "
			   << achievement.name.c_str();
			m_env->getGameDef()->SendChatMessage(m_peer_id, ws.str());

			std::string icon = (achievement.image.empty() ? "awards_unknown.png" : achievement.image);
			static const std::string bg = "awards_bg_default.png";
			std::stringstream ss;
			ss << "size[4,2]"
			   << "image_button_exit[0,0;4,2;" << bg << ";close1; ]"
			   << "image_button_exit[0.2,0.8;1,1;" << icon << ";close2; ]"
			   << "label[1.1,1;" << achievement.name << "]"
			   << "label[0.3,0.1;" << "Congratulation !!!" << "]";
			m_env->getGameDef()->showFormspec(m_player->getName(), ss.str(), "award");
		}
		m_awards[id_achievement] = ap;
	}
}

bool PlayerSAO::tryToAcceptServerRules(const std::string& str)
{
	if (hasAcceptedRules()) {
		m_env->getGameDef()->SendChatMessage(m_peer_id, L"You have already accepted the server rules. Aborting.");
		return true;
	}

	if (str.compare(g_settings->get("rules_magic_sentence")) != 0) {
		m_env->getGameDef()->SendChatMessage(m_peer_id, L"You sent an invalid magic sentence. Please re-read the rules and tell us the"
				" correct sentence.");
		m_env->getGameDef()->showRulesFormspec(m_peer_id);
		return true;
	}

	acceptRules();
	m_env->getGameDef()->SendChatMessage(m_peer_id, L"You accepted the server rules.");

	if (g_settings->get(BOOLSETTING_ENABLE_RULES_AUTOPRIVS)) {
		std::string autoPrivs = g_settings->get("rules_autoprivs_list");
		str_remove_spaces(autoPrivs);
		for (const auto &s: str_split(autoPrivs, ',')) {
			addPriv(s);
		}

		std::wstringstream ws;
		ws << L"Server granted you the following privs:	" << autoPrivs.c_str();
		m_env->getGameDef()->SendChatMessage(m_peer_id, ws.str());
	}

	logger.notice("Player %s has accepted server rules.", m_player->getName());

	return true;
}
