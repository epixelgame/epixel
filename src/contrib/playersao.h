/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#include <unordered_map>
#include <unordered_set>
#include "awards.h"
#include "serverobject.h"
#include "itemgroup.h"
#include "player.h"
#include "object_properties.h"
#include "content_sao.h"
#include "nodedef.h"

enum PlayerAnimations
{
	PLAYERANIM_STAND = 0,
	PLAYERANIM_LAY = 1,
	PLAYERANIM_WALK = 2,
	PLAYERANIM_MINE = 3,
	PLAYERANIM_WALKMINE = 4,
	PLAYERANIM_SIT = 5,
	PLAYERANIM_MAX = 6,
};

extern const v2s32 playermodel_animframes[PLAYERANIM_MAX];

/*
	PlayerSAO needs some internals exposed.
*/

class LagPool
{
	float m_pool;
	float m_max;
public:
	LagPool(): m_pool(15), m_max(15)
	{}
	void setMax(float new_max)
	{
		m_max = new_max;
		if(m_pool > new_max)
			m_pool = new_max;
	}
	void add(float dtime)
	{
		m_pool -= dtime;
		if(m_pool < 0)
			m_pool = 0;
	}
	bool grab(float dtime)
	{
		if(dtime <= 0)
			return true;
		if(m_pool + dtime > m_max)
			return false;
		m_pool += dtime;
		return true;
	}
};

struct PointedThing;
struct MapNode;

typedef std::unordered_set<std::string> PlayerIgnoreList;

class PlayerSAO : public UnitSAO
{
public:
	PlayerSAO(ServerEnvironment *env_, RemotePlayer *player_, u16 peer_id_,
			const std::set<std::string> &privs, bool is_singleplayer);
	~PlayerSAO();
	ActiveObjectType getType() const
	{ return ACTIVEOBJECT_TYPE_PLAYER; }
	ActiveObjectType getSendType() const
	{ return ACTIVEOBJECT_TYPE_GENERIC; }
	std::string getDescription();

	/*
		Active object <-> environment interface
	*/

	void addedToEnvironment(u32 dtime_s);
	void removingFromEnvironment();
	bool isStaticAllowed() const;
	std::string getClientInitializationData(u16 protocol_version);
	void getStaticData(std::string &static_data) const;
	bool isAttached();
	void step(float dtime, bool send_recommended);
	void setBasePosition(const v3f &position);
	void setPos(const v3f &pos);
	void moveTo(v3f pos, bool continuous);
	void setYaw(const float yaw, bool send = false);
	void setPitch(const float pitch, bool send = false);

	/*
		Interaction interface
	*/

	int punch(v3f dir,
		const ToolCapabilities *toolcap,
		ServerActiveObject *puncher,
		float time_from_last_punch);
	virtual void rightClick(ServerActiveObject *clicker);
	bool rightClick(const ContentFeatures &cf);
	void setHP(s16 hp);
	s16 readDamage();
	u16 getBreath() const { return m_breath; }
	void setBreath(const u16 breath);
	void setArmorGroups(const ItemGroupList &armor_groups);
	ItemGroupList getArmorGroups();
	void setAnimation(v2f frame_range, float frame_speed, float frame_blend, bool frame_loop);
	void getAnimation(v2f *frame_range, float *frame_speed, float *frame_blend, bool *frame_loop);
	void setBonePosition(const std::string &bone, v3f position, v3f rotation);
	void getBonePosition(const std::string &bone, v3f *position, v3f *rotation);
	void setAttachment(int parent_id, const std::string &bone, v3f position, v3f rotation);
	void getAttachment(int *parent_id, std::string *bone, v3f *position, v3f *rotation);
	void addAttachmentChild(int child_id);
	void removeAttachmentChild(int child_id);
	std::set<int> getAttachmentChildIds();
	ObjectProperties* accessObjectProperties();
	void notifyObjectPropertiesModified();
	const s64 getXP() const { return m_xp; }
	void setXP(s64 xp) { m_xp = xp; }
	inline void modifyXP(s64 xp) {
		if (xp < 0 && m_xp < -xp)
			m_xp = 0;
		else
			m_xp += xp;
	}
	const float getLvl() const { return sqrt(m_xp) / 3; }
	void updateHUD_Level();

	/*
		Inventory interface
	*/

	Inventory* getInventory();
	const Inventory* getInventory() const;
	InventoryLocation getInventoryLocation() const;
	std::string getWieldList() const;
	int getWieldIndex() const;
	void setWieldIndex(int i);

	/*
		PlayerSAO-specific
	*/

	void disconnected();

	RemotePlayer* getPlayer()
	{
		return m_player;
	}
	u16 getPeerID() const
	{
		return m_peer_id;
	}

	// Cheat prevention

	v3f getLastGoodPosition() const
	{
		return m_last_good_position;
	}
	float resetTimeFromLastPunch()
	{
		float r = m_time_from_last_punch;
		m_time_from_last_punch = 0.0;
		return r;
	}
	void noCheatDigStart(v3s16 p)
	{
		m_nocheat_dig_pos = p;
		m_nocheat_dig_time = 0;
	}
	v3s16 getNoCheatDigPos()
	{
		return m_nocheat_dig_pos;
	}
	float getNoCheatDigTime()
	{
		return m_nocheat_dig_time;
	}
	void noCheatDigEnd()
	{
		m_nocheat_dig_pos = v3s16(32767, 32767, 32767);
	}
	LagPool& getDigPool()
	{
		return m_dig_pool;
	}
	// Returns true if cheated
	bool checkMovementCheat();

	// Privs
	void updatePrivileges(const std::set<std::string> &privs,
			bool is_singleplayer)
	{
		m_privs = privs;
		m_is_singleplayer = is_singleplayer;
	}

	inline void addPriv(const std::string &priv)
	{
		if (!hasPriv(priv))
			m_privs.insert(priv);
	}

	inline void removePriv(const std::string &priv)
	{
		std::set<std::string>::const_iterator it = m_privs.find(priv);
		if (it != m_privs.end())
			m_privs.erase(it);
	}

	std::set<std::string> getPrivs() { return m_privs; }
	inline bool hasPriv(const std::string &priv) const
	{
		return m_privs.find(priv) != m_privs.end();
	}

	// Player ignored
	inline void ignorePlayer(const std::string &playername)
	{
		if (m_ignore_list.find(playername) == m_ignore_list.end())
			m_ignore_list.insert(playername);
	}

	inline void unIgnorePlayer(const std::string &playername)
	{
		PlayerIgnoreList::iterator it = m_ignore_list.find(playername);
		if (it != m_ignore_list.end())
			m_ignore_list.erase(it);
	}

	inline bool isPlayerIgnored(const std::string &playername) const
	{
		return m_ignore_list.find(playername) != m_ignore_list.end();
	}

	const PlayerIgnoreList & getIgnoreList() const { return m_ignore_list; }


	// Extended attributes
	std::string getExtendedAttribute(const std::string &attr);
	void setExtendedAttribute(const std::string &attr, const std::string &value);
	const std::unordered_map<std::string, std::string> getExtendedAttributes() { return m_extended_attributes; }
	void clearExtendedAttributes() { m_extended_attributes.clear(); }
	bool extendedAttributedModified() { return m_extended_attributes_modified; }
	void notifyExtendedAttributesSaved() { m_extended_attributes_modified = false; }

	// Others
	bool getCollisionBox(aabb3f *toset);
	bool collideWithObjects();

	inline void setAreaPos(u8 index, v3s16 value)
	{
		assert(index < 2); // Indexed values are 0, 1
		m_area_pos[index] = value;
	}

	inline v3s16 getAreaPos(u8 index)
	{
		assert(index < 2); // Indexed values are 0, 1
		return m_area_pos[index];
	}

	s32 getHunger() { return m_hunger; }
	void setHunger(s32 h) { m_hunger = h; }
	void addExhaustion(float modifier);
	void setExhaustion(float value) { m_exhaus = value; }

	void setAnimationState(PlayerAnimState state) { m_anim_state = state; }

	void onDie();
	bool onUseItem(ItemStack &stack, const PointedThing &pointed);
	void throwProjectile(const ItemStack &stack, const ItemDefinition* itemdef);
	bool castBallisticSpell(const u32 spellId, float y_cast_offset) const;
	void onDigNode(v3s16 pos, MapNode n);
	bool onPlaceItem(ItemStack &stack, const PointedThing &pointed);
	bool onDropItem(ItemStack &stack);
	bool canReplaceNodeWithBones(v3s16 pos);

	bool canSendChatMessage();

	// Home
	void setHome(const v3f position, bool save = true);
	v3f getHome() {	return m_home; }
	bool isHomeTimerReady() { return (m_home_timer == 0.0f); }
	void resetHomeTimer();
	bool tryTeleport();
	float getHomeTimer() { return m_home_timer; }

	// Mana
	void setMana(u16 mana) { m_mana = mana;	}
	u16 getMana() { return m_mana; }
	void timer_regenMana(float dtime);
	int modifyMana(s32 mana);
	void updateHudMana();
	u32 getRemainingOverHead() { return m_mana_overhead; }

	// Awards
	inline void setAchievementProgress(const u32 id_achievement, const u32 progress, const bool unlocked)
	{
		m_awards[id_achievement] = epixel::AchievementProgress(id_achievement, progress, unlocked);
	}

	std::unordered_map<u32, epixel::AchievementProgress> getAchievementProgress() { return m_awards; }
	void updateAchievementProgress(const u32 id_achievement, const epixel::Achievement &achievement);

	// Rules
	void acceptRules() { m_rules_accepted = true; }
	bool hasAcceptedRules() { return m_rules_accepted; }
	bool tryToAcceptServerRules(const std::string& str);

	void setSpeed(const v3f &speed) { m_speed = speed; }
	inline const v3f getSpeed() const { return m_speed; }

	inline u64 getMoney() const { return m_money; }
	void setMoney(const u64 &m) { m_money = m; }

	inline u32 getArmor() const { return m_armor; }
	void setArmor(const u32 a) { m_armor = a; }

	inline const v3f getEyeOffset() { return v3f(0, BS * 1.625f, 0); }
	inline const v3f getEyePosition() { return m_base_position + getEyeOffset(); }
protected:
	virtual void doDamage(u16 damage, ServerActiveObject *puncher = NULL);
private:
	std::string getPropertyPacket();

	RemotePlayer *m_player;
	u16 m_peer_id;
	Inventory *m_inventory;
	s16 m_damage;

	// Cheat prevention
	LagPool m_dig_pool;
	LagPool m_move_pool;
	v3f m_last_good_position;
	float m_time_from_last_punch;
	v3s16 m_nocheat_dig_pos;
	float m_nocheat_dig_time;

	int m_wield_index;
	bool m_position_not_sent;
	ItemGroupList m_armor_groups;
	bool m_armor_groups_sent;

	bool m_properties_sent;
	std::set<std::string> m_privs;
	std::unordered_set<std::string> m_ignore_list;
	bool m_is_singleplayer;

	v2f m_animation_range;
	float m_animation_speed;
	float m_animation_blend;
	bool m_animation_loop;
	bool m_animation_sent;

	std::unordered_map<std::string, core::vector2d<v3f> > m_bone_position; // Stores position and rotation for each bone name
	bool m_bone_position_sent;

	int m_attachment_parent_id;
	std::set<int> m_attachment_child_ids;
	std::string m_attachment_bone;
	v3f m_attachment_position;
	v3f m_attachment_rotation;
	bool m_attachment_sent;

	std::unordered_map<std::string, std::string> m_extended_attributes;
	bool m_extended_attributes_modified;
	v3s16 m_area_pos[2];
	v3f m_home;

	u64 m_money = 0;
	u32 m_armor = 0;
	u16 m_mana = 0;
	u32 m_mana_overhead = 0;
	s32 m_hunger;
	float m_exhaus = 0.0f;
	float m_global_spell_cooldown = 1.0f;
	bool m_rules_accepted = false;

	// Awards
	std::unordered_map<u32, epixel::AchievementProgress> m_awards; // Stores position and rotation for each bone name

	// Timers
	float m_hunger_check_timer;
	float m_hunger_timer_tick;
	float m_node_damage_tick = 1.0f;
	float m_drowning_interval = 2.0f;
	float m_breathing_interval = 0.5f;
	float m_exhaustion_check_timer;
	float m_home_timer;
	float m_mana_regen_timer;
	float m_rules_acceptance_timer;

	u32 m_last_chat_message_sent = time(NULL);
	float m_chat_message_allowance = 5.0f;
	u16 m_message_rate_overhead = 0;

	PlayerAnimState m_anim_state = PLAYERANIMSTATE_NORMAL;
	s64 m_xp = 0;
	u16 m_breath = PLAYER_MAX_BREATH;

	v3f m_speed = v3f(0,0,0);

	core::aabbox3d<f32> m_collisionbox = core::aabbox3d<f32>(-BS*0.30,0.0,-BS*0.30,BS*0.30,BS*1.75,BS*0.30);
public:
	float m_physics_override_speed = 1;
	float m_physics_override_jump;
	float m_physics_override_gravity;
	bool m_physics_override_sneak;
	bool m_physics_override_sneak_glitch;
	bool m_physics_override_sent;

	void setPlayerAnimation(u8 anim, float speed)
	{
		assert(anim < PLAYERANIM_MAX);
		setAnimation(v2f(playermodel_animframes[anim].X, playermodel_animframes[anim].Y), speed, 0, true);
	}
};
