/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "projectilesao.h"
#include "contrib/spell.h"
#include "environment.h"
#include "map.h"
#include "scripting_game.h"
#include "server.h"
#include "util/serialize.h"

namespace epixel
{

ProjectileSAO::ProjectileSAO(ServerEnvironment *env, v3f pos,
		const std::string &name, const std::string &state,
		const Spell* spell, const u32 casterId):
		LuaEntitySAO(env, pos, name, state), m_caster_id(casterId),
		m_original_pos(pos), m_spell(spell)
{
	if(env == NULL) {
		ServerActiveObject::registerType(getType(), create);
		return;
	}

	m_prop.physical = false;
	m_prop.collisionbox = core::aabbox3d<f32>(0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

	if (spell) {
		m_max_range = spell->max_range * 2 * BS;
		m_prop.visual = spell->visual;
		m_prop.visual_size = spell->visual_size;
		m_prop.textures = {spell->texture};
		m_velocity_modifier = spell->velocity;
		m_node_explosion_radius = spell->damages_nodes_radius;
		m_player_damages = spell->damages_player;
		m_creature_damages = spell->damages_npc;
	}
	else {
		m_max_range = 30 * BS;
	}
}

ServerActiveObject* ProjectileSAO::create(ServerEnvironment *env, const v3f& pos,
		const std::string &data)
{
	std::string name;
	std::string state;
	s16 hp = 1;
	v3f velocity;
	float yaw = 0;
	if(data != "") {
		std::istringstream is(data, std::ios::binary);
		// read version
		u8 version = readU8(is);
		// check if version is supported
		if(version == 0){
			name = deSerializeString(is);
			state = deSerializeLongString(is);
		}
		else if(version == 1){
			name = deSerializeString(is);
			state = deSerializeLongString(is);
			hp = readS16(is);
			velocity = readV3F1000(is);
			yaw = readF1000(is);
		}
	}
	// create object
	logger.info("ProjectileSAO::create(name='%s' state='%s')", name.c_str(), state.c_str());
	Spell* spell = NULL;
	epixel::ProjectileSAO *sao = new epixel::ProjectileSAO(env, pos, name, state, spell, 0);
	sao->m_hp = hp;
	sao->m_velocity = velocity;
	sao->m_yaw = yaw;
	return sao;
}

void ProjectileSAO::addedToEnvironment(u32 dtime_s)
{
	ServerActiveObject::addedToEnvironment(dtime_s);
	m_env->getScriptIface()->
		luaentity_Add(m_id, m_init_name.c_str(), true);
	m_registered = true;

	// And make it immortal
	std::map<std::string, int> armor_groups;
	armor_groups["immortal"] = 1;
	setArmorGroups(armor_groups);
}

void ProjectileSAO::step(float dtime, bool send_recommended)
{
	if (m_removed) {
		return;
	}

	if (m_differed_removal_timer > 0.0f) {
		m_differed_removal_timer -= dtime;

		if (m_differed_removal_timer <= 0.0f) {
			m_differed_removal_timer = 0.0f;
			m_removed = true;
		}
		return;
	}

	LuaEntitySAO::step(dtime, send_recommended);

	if (m_base_position.getDistanceFrom(m_original_pos) > m_max_range) {
		removeProjectile();
		return;
	}

	// If the X velocity wants to go in a different direction than original, cancel
	if ((m_velocity.X > 0 && m_initial_velocity.X < 0) ||
			(m_velocity.X < 0 && m_initial_velocity.X > 0)) {
		m_velocity.X = 0;
	}

	// If the Z velocity wants to go in a different direction than original, cancel
	if ((m_velocity.Z > 0 && m_initial_velocity.Z < 0) ||
			(m_velocity.Z < 0 && m_initial_velocity.Z > 0)) {
		m_velocity.Z = 0;
	}

	v3s16 pos = floatToInt(m_base_position, BS);

	bool validNode = false;
	MapNode n = m_env->getMap().getNode(pos, &validNode);

	// If node not valid, remove the projectile
	if (!validNode) {
		removeProjectile();
		return;
	}

	// Spell touched a node, explode & remove
	if (n.getContent() != CONTENT_AIR) {
		if (m_node_explosion_radius > 0) {
			m_env->makeExplosion(pos, m_node_explosion_radius, m_caster_id);
		}
		removeProjectile();
		return;
	}

	// Fetch object in the spell radius
	// Radius is 1 node * med(visual_size)
	std::vector<UnitSAO*> units;
	m_env->getUnitsInsideRadius(units,
			m_base_position, BS * (m_prop.visual_size.X + m_prop.visual_size.Y) / 2);

	// if objects inside radius
	if (units.size() > 0) {
		bool targetFound = false;

		// Multi target mode, attack every unit except caster
		if (m_spell->target_type == SPELL_TARGETTYPE_MULTI_TARGET) {
			for (const auto &unit: units) {
				// ignore caster
				if (unit->getId() == m_caster_id) {
					continue;
				}

				targetFound = true;
				applyDamages(unit);
			}
		}
		// Mono target, attack the nearest unit
		else if (m_spell->target_type == SPELL_TARGETTYPE_MONO_TARGET) {
			f32 lowestDistance = -1.0f;
			UnitSAO* target = nullptr;
			for (const auto &unit: units) {
				// ignore caster
				if (unit->getId() == m_caster_id) {
					continue;
				}

				f32 dist = m_base_position.getDistanceFrom(unit->getBasePosition());
				if (lowestDistance < dist || lowestDistance == -1.0f) {
					lowestDistance = dist;
					target = unit;
				}
			}

			if (target != nullptr) {
				targetFound = true;
				applyDamages(target, m_caster_id);
			}
		}

		// if it's an exploding spell and a target was found, explode
		if (targetFound) {
			if (m_node_explosion_radius > 0) {
				m_env->makeExplosion(pos, m_node_explosion_radius, m_caster_id);
			}

			removeProjectile();
		}
	}
}

inline void ProjectileSAO::removeProjectile()
{
	// If it's a single target don't remove instant
	if (m_spell->target_type != SPELL_TARGETTYPE_MONO_TARGET) {
		m_removed = true;
	}
	else {
		m_differed_removal_timer = 0.5f;
	}

	m_velocity = v3f(0,0,0);
	m_acceleration = v3f(0,0,0);
	m_env->spawnEffect(m_base_position, 5, "tnt_smoke.png");
	// @TODO play sound at removal
}

void ProjectileSAO::applyDamages(UnitSAO *target, const u16 casterId)
{
	ServerActiveObject* puncher = this;
	if (casterId) {
		if (ServerActiveObject* obj = m_env->getActiveObject(casterId))	{
			puncher = obj;
		}
	}

	target->punch(target->getBasePosition() - m_base_position,
			 (target->getType() == ACTIVEOBJECT_TYPE_PLAYER ?
			 &m_spell->toolcap_player : &m_spell->toolcap_npc), puncher, 0.3f);

	if (target->getType() == ACTIVEOBJECT_TYPE_PLAYER) {
		m_env->getGameDef()->SendPlayerHPOrDie((PlayerSAO*)target);
	}
}

inline void ProjectileSAO::setVelocity(const v3f velocity)
{
	m_velocity = velocity * m_velocity_modifier;
	m_initial_velocity = m_velocity;
}

void ProjectileSAO::applyVelocity(const v3f tpos)
{
	// + 10 is to ajust spell height
	v3f v(m_base_position.X - tpos.X, m_base_position.Y - (tpos.Y + 4),
			m_base_position.Z - tpos.Z);
	v /= BS;

	float yaw = (atan(v.Z / v.X) + M_PI / 2);
	if (tpos.X > m_base_position.X) {
		yaw += M_PI;
	}
	m_yaw = yaw * core::RADTODEG;

	float amount = pow(v.X * v.X + v.Y * v.Y + v.Z * v.Z, 0.5);
	v.X = v.X * -m_velocity_modifier / amount;
	v.Y = v.Y * -m_velocity_modifier / amount;
	v.Z = v.Z * -m_velocity_modifier / amount;
	setVelocity(v);
}

void ProjectileSAO::applyBallistics(const float pitch, const float yaw)
{
	v3f dir(cos(pitch)*cos(yaw), sin(pitch), cos(pitch)*sin(yaw));
	setVelocity(dir * BS);
	// If ballistic direction is to ground, cancel the Y direction
	if (dir.Y < 0) dir.Y = 0;
	m_acceleration = v3f(dir.X * -3, dir.Y * -8.5, dir.Z * -3) * BS;
	m_yaw = (yaw + M_PI) * core::RADTODEG;
}

}
