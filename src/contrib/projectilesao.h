/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#include "content_sao.h"

namespace epixel
{

struct Spell;

class ProjectileSAO: public LuaEntitySAO
{
public:
	ProjectileSAO(ServerEnvironment *env, v3f pos,
			const std::string &name, const std::string &state,
			const Spell *spell, const u32 casterId);
	~ProjectileSAO() {}

	ActiveObjectType getType() const
	{ return ACTIVEOBJECT_TYPE_PROJECTILE; }

	static ServerActiveObject* create(ServerEnvironment *env, const v3f& pos,
			const std::string &data);

	virtual void addedToEnvironment(u32 dtime_s);

	void step(float dtime, bool send_recommended);
	bool isStaticAllowed() const { return false; }

	inline void removeProjectile();
	inline void applyDamages(UnitSAO* target, const u16 casterId = 0);
	void setVelocity(const v3f velocity);
	void applyVelocity(const v3f tpos);
	void applyBallistics(const float pitch, const float yaw);

private:
	s32 m_player_damages;
	s32 m_creature_damages;
	s32 m_node_explosion_radius;
	const u32 m_caster_id;
	const v3f m_original_pos;
	v3f m_initial_velocity;
	float m_velocity_modifier;
	float m_max_range;
	float m_differed_removal_timer = -1.0f;

	const Spell* m_spell;
};

}
