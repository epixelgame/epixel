/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "server.h"

#include "craftdef.h"
#include "nodedef.h"
#include "player.h"
#include "content_sao.h"
#include "emerge.h"
#include "mg_ore.h"
#include "database.h"
#include "mg_biome.h"
#include "mg_decoration.h"
#include "scripting_game.h"
#include "settings.h"
#include "contrib/abm.h"
#include "contrib/abms/fireabm.h"
#include "contrib/abms/treeabm.h"
#include "contrib/ban.h"
#include "contrib/creaturestore.h"
#include "contrib/chathandler.h"
#include "contrib/channelhandler.h"
#include "contrib/playersao.h"
#include "contrib/spell.h"
#include "contrib/teleport.h"
#include "contrib/utils.h"
#include <iomanip>
#ifdef SERVER
#include "contrib/is_chatclient.h"
#endif

#define MAX_QUEUE_INTERSERVER_CHATMSG_PER_STEP 200
static const v2s32 localplayermodel_animframes[4] = {
	{0, 79},
	{168, 187},
	{189, 198},
	{200, 219}
};

bool Server::contrib_on_prejoinplayer(const std::string &playerName, std::string &reason)
{
	if (m_ban_mgr->isBanned(playerName, reason, epixel::BANTYPE_PLAYER)) {
		return true;
	}
	return false;
}

void Server::contrib_on_joinplayer(RemoteClient* client, PlayerSAO* playerSao)
{
	// First is minetest original version
	// Second is epixel version
	if ((client->getMajor() == 0 && (client->getMinor() < 4 || client->getPatch() < 13)) ||
			(client->getMajor() == 1 && (client->getMinor() < VERSION_MINOR || client->getPatch() < VERSION_PATCH))) {
		SendChatMessage(client->peer_id, L"*[EN] Server Notification* You client version is outdated. We recommend you to upgrade to 0.4.13 or later.\nIf you are on Android, please use the new free and official version 'Minetest' on the Google Play Store available here:\nhttps://play.google.com/store/apps/details?id=net.minetest.minetest");
		SendChatMessage(client->peer_id, L"*[FR] Notification serveur* Votre client n'est pas à jour. Nous vous recommandons la version 0.4.13 ou plus récent.\nSi vous êtes sur Android, merci d'utiliser la version officielle gratuite 'Minetest' présente sur le Google Play Store et disponible ici:\nhttps://play.google.com/store/apps/details?id=net.minetest.minetest");
	}

	RemotePlayer* player = playerSao->getPlayer();

	// Initialize player Huds
	player->initHuds();
	PlayerHud* hud = player->getHuds();

	// Now declare Huds to client
	for (u8 i=PLAYERHUD_LIFE_BG; i < PLAYERHUD_MAX; i++) {
		u32 id = hudAdd(player, hud->getHud((PlayerHudIdx)i));
		hud->setHudId((PlayerHudIdx)i, id);
	}

	// Initialize player model
	setLocalPlayerAnimations(player, (v2s32*)localplayermodel_animframes, 20.0);

	ObjectProperties* props = playerSao->accessObjectProperties();
	props->mesh = "3d_armor_character.b3d";
	props->textures = {"character.png", "3d_armor_trans.png", "3d_armor_trans.png"};
	props->visual = "mesh";
	props->visual_size = v2f(1.0, 1.0);

	playerSao->setPlayerAnimation(PLAYERANIM_STAND, 30);

	// Load players channels and set last login to now
	std::vector<std::string> channels;
	m_game_database->loadPlayerChannels(player->getDBId(), channels);
	for (const auto &channel: channels) {
		m_chat_handler->handleCommand_channel_join(channel, client->peer_id);
	}

	m_auth_database->setLastLogin(player->getName());

	// Awards
	m_env->getGameDef()->applyAwardStep(playerSao, "", epixel::ACHIEVEMENT_JOIN);

	// Show server rules if not accepted && !singleplayer
	if (!playerSao->hasAcceptedRules() && g_settings->get(BOOLSETTING_ENABLE_RULES_FORM)
#ifndef SERVER
			&& !m_singleplayer_mode
#endif
		) {
		m_env->getGameDef()->showRulesFormspec(playerSao->getPeerID());
	}
}

void Server::contrib_on_leaveplayer(PlayerSAO* playerSao)
{
	RemotePlayer* player = playerSao->getPlayer();

	PlayerHud* hud = player->getHuds();
	if (hud) {
		// Now declare Huds to client
		for (u8 i=PLAYERHUD_LIFE_BG; i < PLAYERHUD_MAX; i++) {
			hud->setHudId((PlayerHudIdx)i, PLAYERHUD_INVALID);
			hud->setHud((PlayerHudIdx)i, NULL);
		}
	}

	m_chat_handler->getChannelHandler()->onplayer_leave(player->getPeerID());
}

void Server::contrib_on_respawn(PlayerSAO* playerSao)
{
	// On respawn set hunger to default
	RemotePlayer* player = playerSao->getPlayer();
	assert(player != NULL);

	// Reinit hunger, exhaustion, breath and mana
	playerSao->setExhaustion(0);
	playerSao->setHunger(20);
	playerSao->setBreath(11);

	u32 hunger = 20;
	SendHUDChange(playerSao->getPeerID(), player->getHuds()->getHudId(PLAYERHUD_HUNGER_FG),
			HUD_STAT_NUMBER, &hunger);

	if (g_settings->get(BOOLSETTING_ENABLE_MOD_MANA)) {
		playerSao->setMana(0);
		playerSao->updateHudMana();
	}

	// Hud is updated here for breath
	SendPlayerBreath(playerSao);
}

#define SHUTDOWN_AUTOMSG_COUNT 19
static const float shutdown_automsg_steps[SHUTDOWN_AUTOMSG_COUNT] =
{
	1, 2, 3, 4, 5, 10, 15, 20, 25, 30, 45, 60, 120, 180, 300, 600, 1200, 1800, 3600
};

void Server::contrib_asyncrunstep(float dtime)
{
	// If shutdown timer is in progress,
	if (m_shutdown_timer > 0.0f) {

		// Automated messages
		if (m_shutdown_timer < shutdown_automsg_steps[SHUTDOWN_AUTOMSG_COUNT - 1]) {
			for (u16 i = 0; i < SHUTDOWN_AUTOMSG_COUNT - 1; i++) {
				// If shutdown timer matches an automessage, shot it
				if (m_shutdown_timer > shutdown_automsg_steps[i] && m_shutdown_timer - dtime < shutdown_automsg_steps[i]) {
					std::wstringstream ws;
					ws << L"*** Server shutting down in "
						<< epixelDurationToString(round(m_shutdown_timer - dtime)).c_str() << ".";
					logger.notice("%s", wide_to_utf8(ws.str()).c_str());
					SendChatMessage(PEER_ID_INEXISTENT, ws.str());
					break;
				}
			}
		}
		m_shutdown_timer -= dtime;
		if (m_shutdown_timer < 0.0f) {
			m_shutdown_timer = 0.0f;
			m_shutdown_requested = true;
		}
	}

	if (g_settings->get(BOOLSETTING_ENABLE_TIPS)) {
		if (m_automated_msg_timer < 0) {
			m_automated_msg_timer = rand() % 300 +
					g_settings->get(U16SETTING_MINTIME_BETWEEN_TIPS);

			u16 randMsg = rand() % m_tips.size();
			while(randMsg == contrib_last_msgid) {
				randMsg = rand() % m_tips.size();
			}

			SendChatMessage(PEER_ID_BROADCAST, m_tips[randMsg]);
			contrib_last_msgid = randMsg;
		}
		else {
			m_automated_msg_timer -= dtime;
		}
	}

#ifdef SERVER
	if (m_ischatclient_thread && g_settings->get(BOOLSETTING_ENABLE_INTERSERVER_CHAT_CLIENT)) {
		u16 queue_counter = 0;
		while (!m_ischatclient_thread->isRecvQueueEmpty() && queue_counter < MAX_QUEUE_INTERSERVER_CHATMSG_PER_STEP) {
			epixel::ISChatMessage* msg = m_ischatclient_thread->popRecvMsg();
			// This should not happen, but in case of...
			if (msg == nullptr) {
				break;
			}

			std::wstringstream ws;
			ws << L"<" << msg->sender.c_str() << L"@" << msg->server_name.c_str() << "> " << narrow_to_wide(msg->message) << std::endl;
			SendChatMessage(PEER_ID_BROADCAST, ws.str());
			delete msg;
			queue_counter++;

		}
	}
#endif

	if (g_settings->get(BOOLSETTING_ENABLE_CONSOLE) && m_chat_handler) {
		m_chat_handler->runEnqueuedCommands();
	}
}

void Server::load_server_rules()
{
	// Disable in single player mode
#ifndef SERVER
	if (m_singleplayer_mode) {
		return;
	}
#endif

	// We don't need to enable rule form, stop there
	if (!g_settings->get(BOOLSETTING_ENABLE_RULES_FORM)) {
		return;
	}

	const std::string rulesFile = g_settings->get("rules_file");
	std::ifstream ifs;
	ifs.open(rulesFile);
	if (!ifs.good()) {
		logger.warn("You set rules_files to '%s' but epixel is unable to read it. Disabling rules validation.",
				rulesFile.c_str());
		g_settings->set(BOOLSETTING_ENABLE_RULES_FORM, false);
		return;
	}

	std::stringstream sStream;
	sStream << ifs.rdbuf();
	ifs.close();

	// Disable if rules files has empty body
	if (sStream.str().empty()) {
		logger.warn("You set rules_files to '%s' but file seems empty. Disabling rules validation.",
				rulesFile.c_str());
		g_settings->set(BOOLSETTING_ENABLE_RULES_FORM, false);
		return;
	}

	// Formspec escape
	static const std::regex reg1("[],]"), reg2("[;]"), reg3("\\["), reg4("\\]");
	std::string rules = sStream.str();
	rules = std::regex_replace(rules, reg1, "\\,");
	rules = std::regex_replace(rules, reg2, "\\;");
	rules = std::regex_replace(rules, reg3, "\\[");
	rules = std::regex_replace(rules, reg4, "\\]");

	// Multiline formspec split
	static const std::regex regline("\n");
	rules = std::regex_replace(rules, regline, ",");

	std::stringstream formspecss;

	formspecss << "size[10,9]"
	"label[4.3,0.2;Server rules]"
	"textlist[1,1.0;8,5.5;rules_ta;"
	<< rules << ";;]"
	"field[2.5,7.2;6,0.7;rules_magic;Rules magic sentence;fake sentence]"
	"label[1.5,7.7;If you accept server rules, type the magic sentence,]"
	"label[1.5,7.9;else you will be disconnected after a short amount of time.]"
	"button_exit[4,8.5;2,0.7;rules_ok;Validate]";

	m_server_rules_form = formspecss.str();
}

void Server::contrib_onstart()
{
	load_server_rules();

	logger.notice("Loading core crafts...");

	std::vector<CraftDefinition*> craftDefs;
	m_game_database->loadCrafts(craftDefs);

	for (auto &craft: craftDefs) {
		m_craftdef->registerCraft(craft, this);
	}

	logger.notice("%d crafts loaded.", craftDefs.size());

	logger.notice("Loading tips...");
	m_tips.clear();
	m_game_database->loadTips(m_tips);
	logger.notice("%d tips loaded.", m_tips.size());

	logger.notice("Loading spells...");
	m_spells.clear();
	m_game_database->loadSpells(m_spells);
	logger.notice("%d spells loaded.", m_spells.size());

	// Load all the creature store
	m_creaturestore->Load(m_spells);

	// Set default privs and remove spaces
	std::string settingDefaultPrivs = g_settings->get("default_privs");
	str_remove_spaces(settingDefaultPrivs);
	defaultPrivs = str_split(settingDefaultPrivs, ',');

	m_env->addActiveBlockModifier(new epixel::abm::CactusABM(m_nodedef));
	m_env->addActiveBlockModifier(new epixel::abm::DirtABM(m_nodedef));
	m_env->addActiveBlockModifier(new epixel::abm::DirtWithGrassABM(m_nodedef));
	m_env->addActiveBlockModifier(new epixel::abm::FieldABM(m_nodedef));
	m_env->addActiveBlockModifier(new epixel::abm::LavaFlowingABM(m_nodedef));
	m_env->addActiveBlockModifier(new epixel::abm::LavaSourceABM(m_nodedef));
	m_env->addActiveBlockModifier(new epixel::abm::LeafDecayABM(m_nodedef));
	m_env->addActiveBlockModifier(new epixel::abm::TreeFruitDecayABM(m_nodedef));
	m_env->addActiveBlockModifier(new epixel::abm::StoneABM(m_nodedef));
	m_env->addActiveBlockModifier(new epixel::abm::TorchABM(m_nodedef));
	m_env->addActiveBlockModifier(new epixel::abm::FurnaceABM(m_nodedef));
	m_env->addActiveBlockModifier(new epixel::abm::FireFlammableABM(m_nodedef));
	m_env->addActiveBlockModifier(new epixel::abm::FireFarFlammableABM(m_nodedef));
	m_env->addActiveBlockModifier(new epixel::abm::FireFlameABM(m_nodedef));
	m_env->addActiveBlockModifier(new epixel::abm::PapyrusABM(m_nodedef));
	m_env->addActiveBlockModifier(new epixel::abm::BirchTreeABM(m_nodedef));
	m_env->addActiveBlockModifier(new epixel::abm::SpruceTreeABM(m_nodedef));
	m_env->addActiveBlockModifier(new epixel::abm::FirTreeABM(m_nodedef));
	m_env->addActiveBlockModifier(new epixel::abm::JungleTreeABM(m_nodedef));
	m_env->addActiveBlockModifier(new epixel::abm::AcaciaTreeABM(m_nodedef));
	m_env->addActiveBlockModifier(new epixel::abm::AppleTreeABM(m_nodedef));
	m_env->addActiveBlockModifier(new epixel::abm::PineTreeABM(m_nodedef));

	static const std::vector<std::string> moretrees_treenames = {
		"acacia", "apple_tree", "beech", "birch", "fir", "jungletree",
		"oak", "palm", "pine", "rubber_tree", "sequoia", "spruce", "willow"
	};

	// Load ABMS from database (sapling)
	std::vector<epixel::abm::DBTreeSaplingABM*> tree_abms;
	m_game_database->loadTreeABM(m_nodedef, tree_abms);
	for (const auto &abm: tree_abms) {
		m_env->addActiveBlockModifier(abm);
	}

	std::vector<epixel::abm::NodeReplaceABM*> replace_abms;
	m_game_database->loadNodeReplaceABM(m_nodedef, replace_abms);
	for (const auto &abm: replace_abms) {
		m_env->addActiveBlockModifier(abm);
	}

	// Load creature spawning ABM
	std::vector<epixel::abm::CreatureSpawningABM*> creaturespawn_abms;
	m_game_database->loadCreatureSpawningABM(m_nodedef, creaturespawn_abms);

	for (const auto &abm: creaturespawn_abms) {
		m_env->addActiveBlockModifier(abm);
	}

	logger.notice("%d dynamic ABM loaded.", (tree_abms.size() + replace_abms.size() + creaturespawn_abms.size()));

	// Load static trees ABM
	for (const auto &tn: moretrees_treenames) {
		m_env->addActiveBlockModifier(new epixel::abm::TreeTrunkSideWaysABM(m_nodedef, tn));
	}
	m_env->addActiveBlockModifier(new epixel::abm::TreeTrunkSideWaysABM(m_nodedef, "rubber_tree", "_trunk_empty"));

	logger.notice("%d static trees ABM loaded.", (moretrees_treenames.size() + 1));

	m_env->generateNodeIdCache();
	logger.notice("%d node ids cached.", ENVIRONMENT_NODECACHE_ID_MAX);

	OreManager* oremgr = m_emerge->oremgr;
	std::vector<Ore*> ores;
	u32 oreCount = 0;
	m_game_database->loadOres(ores, oremgr);
	for (auto &ore: ores) {
		ObjDefHandle handle = oremgr->add(ore);
		if (handle == OBJDEF_INVALID_HANDLE) {
			logger.warn("Failed to add ore to manager");
			delete ore;
			continue;
		}
		oreCount++;
		m_nodedef->pendNodeResolve(ore);
	}

	logger.notice("%d ores loaded.", oreCount);

	// Load achievement
	m_game_database->loadAchievements(m_map_achievement);

	// Load chat handler
	m_chat_handler = new epixel::ChatHandler(this);
	m_console_thread = nullptr;
	if (g_settings->get(BOOLSETTING_ENABLE_CONSOLE)) {
		m_console_thread = new epixel::ConsoleThread(m_chat_handler);
	}

	// Load achievement
	m_game_database->loadAchievements(m_map_achievement);
	logger.notice("%d achievements loaded", m_map_achievement.size());

	m_game_database->loadTeleportLocations(m_env->getTeleportMgr());
	logger.notice("%d teleport locations loaded.", m_env->getTeleportMgr()->locCount());

	// Load biomes
	BiomeManager* biomemgr = m_emerge->biomemgr;
	std::vector<Biome*> biomes;
	u32 biomeCount = 0;
	m_game_database->loadBiomes(biomes, biomemgr);
	for (const auto &biome: biomes) {
		ObjDefHandle handle = biomemgr->add(biome);
		if (handle == OBJDEF_INVALID_HANDLE) {
			logger.warn("Failed to add biome to manager");
			delete biome;
			continue;
		}
		biomeCount++;
		m_nodedef->pendNodeResolve(biome);
	}

	logger.notice("%d biomes loaded.", biomeCount);

	// Load decorations
	DecorationManager* decomgr = m_emerge->decomgr;
	SchematicManager* schmgr = m_emerge->schemmgr;
	std::vector<Decoration*> decorations;
	u32 decoCount = 0;
	m_game_database->loadDecorations(decorations, decomgr, schmgr, biomemgr);
	for (const auto &deco: decorations) {
		ObjDefHandle handle = decomgr->add(deco);
		if (handle == OBJDEF_INVALID_HANDLE) {
			logger.warn("Failed to add decoration to manager");
			delete deco;
			continue;
		}
		decoCount++;
		m_nodedef->pendNodeResolve(deco);
	}

	logger.notice("%d decorations loaded.", decoCount);

#ifdef SERVER
	m_ischatclient_thread = nullptr;
	if (g_settings->get(BOOLSETTING_ENABLE_INTERSERVER_CHAT_CLIENT)) {
		m_ischatclient_thread = new epixel::ISChatClient();
	}
#endif
}

bool Server::contrib_on_chat_message(RemotePlayer* player,
			const std::string &message)
{
	if (!player->getPlayerSAO()->canSendChatMessage()) {
		// Return true to ignore message
		return true;
	}

	u16 peer_id = player->getPlayerSAO()->getPeerID();

	// ChatHandler (light) imported from Mangos project
	return m_chat_handler->handleCommand(message, peer_id);

}

epixel::Spell* Server::getSpell(const u32 id)
{
	epixel::SpellMap::const_iterator it = m_spells.find(id);
	if (it == m_spells.end()) {
		return NULL;
	}

	return it->second;
}

const u32 Server::getSpellId(const std::string &name) const
{
	for (const auto &spell: m_spells) {
		if (spell.second->name.compare(name) == 0) {
			return spell.first;
		}
	}

	return SPELL_UNK_ID;
}

void Server::applyAwardStep(PlayerSAO *player_sao, const std::string &nodename, epixel::AchievementType at)
{
	for (const auto &a: m_map_achievement) {
		epixel::Achievement achiev = a.second;
		if (achiev.at == at) {
			if ((achiev.target_what.empty()) || (achiev.target_what.compare(nodename) == 0)) {
				player_sao->updateAchievementProgress(achiev.id, achiev);
			}
		}
	}
}

#ifndef SERVER
Database_SQLite3* Server::getMapDatabase()
#else
Database_PostgreSQL* Server::getMapDatabase()
#endif
{
	return m_map_database->get(m_map_database->getCursor());
}

namespace epixel
{

bool ChatHandler::handleCommand_kick(const std::string &args, const u16 peer_id)
{
	std::vector<std::string> commandline;
	std::stringstream ss(args);
	std::string item;
	while (std::getline(ss, item, ' ')) {
		commandline.push_back(item);
	}

	if (commandline.empty()) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help kick");
		return true;
	}

	std::string name = m_server->getPlayerName(peer_id);
	if (name.compare(commandline[0]) == 0) {
		m_server->SendChatMessage(peer_id, L"Why do you want to kick yourself ? I refuse it.");
		return true;
	}

	std::wstringstream ws;
	RemotePlayer* player = m_server->getEnv().getPlayer(commandline[0].c_str());
	if (!player) {
		ws << "Player '" << commandline[0].c_str() << "' not found or not connected." << std::endl;
		m_server->SendChatMessage(peer_id, ws.str());
		return true;
	}

	ws << "Kicked. ";

	// Read other arguments as reason
	for (u8 i = 1; i < commandline.size(); i++) {
		// Add space if it's not the first word
		if (i != 1) {
			ws << L" ";
		}
		ws << commandline[i].c_str();
	}

	m_server->DenyAccess_Legacy(player->getPeerID(), ws.str());
	return true;
}

bool ChatHandler::handleCommand_last_login(const std::string &args, const u16 peer_id)
{
	std::vector<std::string> commandline;
	std::stringstream ss(args);
	std::string item;
	while (std::getline(ss, item, ' ')) {
		commandline.push_back(item);
	}

	if (commandline.size() != 1) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help lastlogin");
		return true;
	}

	std::string playername = commandline[0];
	std::wstringstream ws;

	u32 playerid = m_server->getAuthDatabase()->userExists(playername);
	if (!playerid) {
		ws << "Player " << playername.c_str() << " doesn't exists." << std::endl;
		m_server->SendChatMessage(peer_id, ws.str());
		return true;
	}

	u32 lastlogin = m_server->getAuthDatabase()->getLastLogin(playerid);

	std::time_t t = lastlogin;
	std::tm tm = *std::localtime(&t);

	ws << L"Last login time for player " << playername.c_str() << L" was ";
	if (lastlogin != 0) {
		std::stringstream ss_;
		ss_ << std::put_time(&tm, "%a %d %b %Y at %H:%M:%S");
		ws << ss_.str().c_str();
	}
	else {
		ws << "Never";
	}

	m_server->SendChatMessage(peer_id, ws.str());
	return true;
}

bool ChatHandler::handleCommand_mods(const std::string &args, const u16 peer_id)
{
	std::vector<std::string> modlist;
	m_server->getModNames(modlist);
	std::string modstrlist = epixel::join(modlist.rbegin(),modlist.rend(), std::string(", "));
	m_server->SendChatMessage(peer_id, utf8_to_wide(modstrlist));
	return true;
}

bool ChatHandler::handleCommand_server_clearobjects(const std::string &args, const u16)
{
	m_server->SendChatMessage(PEER_ID_BROADCAST, L"*** Server clearobjects command has been requested by an operator."
			"Operation can take some time and disconnection can occur.");
	m_server->getEnv().clearAllObjects();
	m_server->SendChatMessage(PEER_ID_BROADCAST, L"*** Server clearobjects command finished. Thanks for waiting.");
	return true;
}

bool ChatHandler::handleCommand_server_shutdown(const std::string &args, const u16 peer_id)
{
	std::smatch rem;
	const static std::regex shutdown_regex("^([0-9]+)*[ ]*(.*)$");

	if (std::regex_search(args, rem, shutdown_regex)) {
		s32 rawtime = mystoi(rem.str(1));
		if (rawtime < 0) {
			rawtime = 0;
		}

		m_server->requestShutdown(args, rawtime, true);

		std::wstringstream ws;
		ws << L"*** Server shutting down ";
		if (rawtime == 0) {
			ws << L"now.";
		}
		else {
			ws << L"in " << epixelDurationToString(rawtime).c_str() << ".";
		}

		m_server->SendChatMessage(PEER_ID_INEXISTENT, ws.str());

		// This command can be run from server console
		if (peer_id == PEER_ID_SERVER) {
			std::cout << "Server shutdown request sent." << std::endl;
		}

		return true;
	}

	m_server->SendChatMessage(peer_id, L"*** Invalid syntax. See /help server shutdown.");

	return true;
}

bool ChatHandler::handleCommand_server_status(const std::string &args, const u16 peer_id)
{
	m_server->SendChatMessage(peer_id, m_server->getStatusString());
	return true;
}

bool ChatHandler::handleCommand_whisper(const std::string &args, const u16 peer_id)
{
	std::vector<std::string> commandline;
	std::stringstream ss(args);
	std::string item;
	while (std::getline(ss, item, ' ')) {
		commandline.push_back(item);
	}

	if (commandline.size() < 2) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help w");
		return true;
	}

	std::wstringstream ws;
	std::string playerdest = commandline[0];
	if (RemotePlayer *player = m_server->getEnv().getPlayer(playerdest.c_str())) {
		PlayerSAO* destsao = player->getPlayerSAO();
		if (!destsao) {
			goto player_not_connected;
		}

		const char* playersrc = m_server->getPlayerName(peer_id).c_str();

		// Check if player is ignored by dest
		if (destsao->isPlayerIgnored(playersrc)) {
			ws << playerdest.c_str() << " ignores you." << std::endl;
			m_server->SendChatMessage(peer_id, ws.str());
			return true;
		}

		std::wstringstream srcws;
		std::stringstream logss;
		ws << playersrc << " whispers: ";
		srcws << "You whisper:";
		for (u16 i = 1; i < commandline.size(); i++) {
			ws << L" " << commandline[i].c_str();
			srcws <<  L" " << commandline[i].c_str();
			logss << " " << commandline[i].c_str();
		}
		m_server->SendChatMessage(player->getPeerID(), ws.str());
		m_server->SendChatMessage(peer_id, srcws.str());
		logger_chat.notice("%s -> %s: %s", playersrc, playerdest.c_str(), logss.str().c_str());
		return true;
	}

player_not_connected:
	ws << "Player '" << playerdest.c_str() << "' is not connected.";
	m_server->SendChatMessage(peer_id, ws.str());
	return true;
}

bool ChatHandler::handleCommand_maintenance(const std::string &args, const u16)
{
	if (args.compare("on") == 0) {
		g_settings->set(BOOLSETTING_ENABLE_MAINTENANCE_MODE, true);
		m_server->SendChatMessage(PEER_ID_BROADCAST,
				L"*** Server maintenance mode set to on. Kicking all players.");
		m_server->getEnv().kickAllPlayers(SERVER_ACCESSDENIED_SHUTDOWN,
				"Server has been set to maintenance mode. Please come back later.", false, false);
		logger.notice("Maintenance mode set to on");
	}
	else if (args.compare("off") == 0) {
		g_settings->set(BOOLSETTING_ENABLE_MAINTENANCE_MODE, false);
		m_server->SendChatMessage(PEER_ID_BROADCAST,
				L"*** Server maintenance mode set to off. Players are now able to connect.");
		logger.notice("Maintenance mode set to off");
	}
	else {

	}
	return true;
}

bool ChatHandler::handleCommand_rules_reload(const std::string &args, const u16 peer_id)
{
	m_server->SendChatMessage(peer_id, L"Reloading server rules...");
	m_server->load_server_rules();
	m_server->SendChatMessage(PEER_ID_BROADCAST, L"*** Server rules have been reloaded.");
	return true;
}

}
