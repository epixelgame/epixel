/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#include <string>
#include "tool.h"
#include "irrlichttypes_bloated.h"

namespace epixel
{

enum SpellTargetType {
	SPELL_TARGETTYPE_NONE,
	SPELL_TARGETTYPE_MONO_TARGET,
	SPELL_TARGETTYPE_MULTI_TARGET,
	SPELL_TARGETTYPE_MAX,
};

enum SpellDamageGroup {
	SPELLDAMAGEGROUP_FLESHY,
	SPELLDAMAGEGROUP_MAX,
};

#define SPELL_UNK_ID 0xFFFFFFFF

struct Spell {
	Spell();
	u32 id = 0;
	SpellTargetType target_type = SPELL_TARGETTYPE_NONE;
	std::string name = "";
	std::string visual = "";
	v2f visual_size = v2f(1,1);
	std::string texture = "";
	float max_range = 1;
	float timer = 1;
	float velocity = 1;
	s32 damages_player = 0;
	s32 damages_npc = 0;
	ToolCapabilities toolcap_npc;
	ToolCapabilities toolcap_player;
	SpellDamageGroup damagegroup = SPELLDAMAGEGROUP_FLESHY;
	u32 damages_nodes_radius = 1;

	static const char* DamageGroupMapper[SPELLDAMAGEGROUP_MAX];
};

}

