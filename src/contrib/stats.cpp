/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "stats.h"
#include <json/json.h>
#include <regex>
#include "util/string.h"

namespace epixel
{
void PlayerStat::toJson(Json::Value &res)
{
	res["user_id"] = user_id;
	res["event_time"] = (Json::UInt64) timestamp;
}

void PlayerStat_Dig::toJson(Json::Value &res)
{
	PlayerStat::toJson(res);
	res["node"] = node;
}

void PlayerStat_PlaceNode::toJson(Json::Value &res)
{
	PlayerStat::toJson(res);
	res["node"] = node;
}

void PlayerStat_Craft::toJson(Json::Value &res)
{
	PlayerStat::toJson(res);
	static const std::regex re("^([a-zA-Z]+[:]?[a-zA-Z0-9]+)[ ]([0-9]+)?");
	std::smatch rem;
	if (std::regex_search(craft, rem, re)) {
		res["item"] = rem.str(1);
		res["count"] = mystoi(rem.str(2));
	}
	else {
		res["item"] = craft;
		res["count"] = 1;
	}
}
}