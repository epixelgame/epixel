/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#include <string>
#include <time.h>
#include "irrlichttypes.h"

namespace Json
{
	class Value;
}

namespace epixel
{
enum PlayerStatType
{
	PLAYERSTATTYPE_DIG,
	PLAYERSTATTYPE_PLACENODE,
	PLAYERSTATTYPE_CRAFT,
	PLAYERSTATTYPE_CONNECTION,
	PLAYERSTATTYPE_NONE,
};

struct PlayerStat
{
public:
	PlayerStat(PlayerStatType t, const u32 id):
			type(t), user_id(id), timestamp(time(NULL)) {}
	virtual ~PlayerStat() {}
	PlayerStatType type = PLAYERSTATTYPE_NONE;
	virtual void toJson(Json::Value &res);
	u32 user_id = 0;
	u64 timestamp = 0;
};

struct PlayerStat_Dig: public PlayerStat
{
public:
	PlayerStat_Dig(const u32 uid, const std::string &n):
			PlayerStat(PLAYERSTATTYPE_DIG, uid), node(n) {}
	~PlayerStat_Dig() {}
	void toJson(Json::Value &res);
	std::string node;
};

struct PlayerStat_PlaceNode: public PlayerStat
{
public:
	PlayerStat_PlaceNode(const u32 uid, const std::string &n):
			PlayerStat(PLAYERSTATTYPE_PLACENODE, uid), node(n) {}
	~PlayerStat_PlaceNode() {}
	void toJson(Json::Value &res);
	std::string node;
};

struct PlayerStat_Craft: public PlayerStat
{
public:
	PlayerStat_Craft(const u32 uid, const std::string &n):
			PlayerStat(PLAYERSTATTYPE_CRAFT, uid), craft(n) {}
	~PlayerStat_Craft() {}
	void toJson(Json::Value &res);
	std::string craft;
};

struct PlayerStat_Connection: public PlayerStat
{
public:
	PlayerStat_Connection(const u32 uid):
			PlayerStat(PLAYERSTATTYPE_CONNECTION, uid) {}
	~PlayerStat_Connection() {}
	void toJson(Json::Value &res) { PlayerStat::toJson(res); }
};
}