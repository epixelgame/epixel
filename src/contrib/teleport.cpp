/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include <sstream>
#include "teleport.h"
#include "database.h"
#include "server.h"
#include "contrib/chathandler.h"
#include "contrib/playersao.h"
#include "contrib/utils.h"

namespace epixel
{

const std::string TeleportMgr::s_formspec_tpl = "size[8,2.7]"
"image[3,0;1,1;travelnet_inv.png]"
"label[4,0.2;Teleporter]"
"dropdown[1,1;6;teleporter_selection;Choose a teleport location";

const std::string TeleportMgr::s_formspec_tpl_end = ";1]"
"button_exit[3,2;2,0.7;teleport_go;Teleport]";

const std::string TeleportMgr::s_formspec_tpl_empty = "size[8,2.7]"
"image[3,0;1,1;travelnet_inv.png]"
"label[4,0.2;Teleporter]"
"label[2,1;No teleport location defined by teleportadmin]"
"button_exit[3,2;2,0.7;cancel;Cancel]";

TeleportMgrError TeleportMgr::addTeleportLocation(const TeleportLocation &loc, const bool database_loading)
{
	for (const auto &tl: m_teleport_locations) {
		if (tl.x == loc.x && tl.y == loc.y && tl.z == loc.z) {
			return TELEPORTMGR_ERR_COORD_EXISTS;
		}

		if (tl.label.compare(loc.label) == 0) {
			return TELEPORTMGR_ERR_NAME_EXISTS;
		}
	}

	m_teleport_locations.push_back(loc);
	m_db->addTeleportLocation(loc);

	if (!database_loading) {
		regenFormspec();
	}

	return TELEPORTMGR_ERR_NONE;
}

TeleportMgrError TeleportMgr::removeTeleportLocation(const TeleportLocation &loc)
{
	for (std::vector<TeleportLocation>::iterator it = m_teleport_locations.begin();
		 it != m_teleport_locations.end(); ++it) {
		if ((*it).x == loc.x && (*it).y == loc.y && (*it).z == loc.z) {
			m_teleport_locations.erase(it);
			m_db->removeTeleportLocation(loc);
			regenFormspec();
			return TELEPORTMGR_ERR_NONE;
		}
	}

	return TELEPORTMGR_ERR_LOC_NOT_EXISTS;
}

inline bool TeleportMgr::exists(const TeleportLocation &loc)
{
	for (const auto &l: m_teleport_locations) {
		if (loc.x == l.x && loc.y == l.y && loc.z == l.z) {
			return true;
		}
	}

	return false;
}

/**
 * @brief TeleportMgr::regenFormspec
 *
 * Generate teleporter formspec
 */
void TeleportMgr::regenFormspec()
{
	if (m_teleport_locations.empty()) {
		m_formspec = TeleportMgr::s_formspec_tpl_empty;
		return;
	}

	std::stringstream ss;
	ss << TeleportMgr::s_formspec_tpl;
	for (const auto &tl: m_teleport_locations) {
		ss << "," << tl.label << " (" << tl.x << " " << tl.y << " " << tl.z << ")";
	}
	ss << s_formspec_tpl_end;
	m_formspec = ss.str();
}

void TeleportMgr::teleportIfValid(PlayerSAO *sao, const std::string &field)
{
	std::smatch rem;
	static const std::regex regex_position("^(.+)[ ][(]([-]?[0-9]+)[ ]?([-]?[0-9]+)[ ]?([-]?[0-9]+)[)]$");
	if (std::regex_search(field, rem, regex_position)) {
		TeleportLocation tl = {rem.str(1), regex::mtof(rem,2), regex::mtof(rem,3), regex::mtof(rem,4)};
		if (exists(tl)) {
			sao->setPos(v3f(tl.x, tl.y, tl.z) * BS);
			return;
		}
	}

	sao->getEnv()->getGameDef()->SendChatMessage(sao->getPeerID(), L"Could not teleport to invalid position. Aborting.");
}

/**
 * @brief ChatHandler::handleCommand_teleport_add
 * @param args
 * @param peer_id
 * @return true
 *
 * Add a teleport location at x,y,z with label l
 * teleport add <x>,<y>,<z> <l>
 */
bool ChatHandler::handleCommand_teleport_add(const std::string &args, const u16 peer_id)
{
	if (args.empty()) {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help teleport add");
		return true;
	}

	static const std::regex regex_position("^([-]?[0-9]+),[ ]?([-]?[0-9]+),[ ]?([-]?[0-9]+)[ ](.+)$");
	std::smatch rem;
	if (std::regex_search(args, rem, regex_position)) {
		TeleportLocation tl = {rem.str(4), regex::mtof(rem, 1),
				regex::mtof(rem, 2), regex::mtof(rem, 3)};
		switch (m_server->getEnv().getTeleportMgr()->addTeleportLocation(tl)) {
			case TELEPORTMGR_ERR_NONE: {
				std::wstringstream ws;
				ws << L"Teleport location '" << tl.label.c_str() << "' added at position (" << tl.x
						<< "," << tl.y << "," << tl.z << ")";
				m_server->SendChatMessage(peer_id, ws.str());
				break;
			}
			case TELEPORTMGR_ERR_COORD_EXISTS:
				m_server->SendChatMessage(peer_id,
						L"Unable to add this teleport location. Coordinates are already referenced with another label.");
				break;
			case TELEPORTMGR_ERR_NAME_EXISTS:
				m_server->SendChatMessage(peer_id,
						L"Unable to add this teleport location. This name was allocated to another location.");
				break;
			default: assert(false);
		}
	}
	else {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help teleport add");
	}

	return true;
}

/**
 * @brief ChatHandler::handleCommand_teleport_remove
 * @param args
 * @param peer_id
 * @return true
 *
 * Remove teleport location at x,y,z
 * teleport remove <x>,<y>,<z>
 */
bool ChatHandler::handleCommand_teleport_remove(const std::string &args, const u16 peer_id)
{
	std::smatch rem;
	if (std::regex_search(args, rem, regex::position)) {
		TeleportLocation tl = {"", regex::mtof(rem, 1),
				regex::mtof(rem, 2), regex::mtof(rem, 3)};
		switch (m_server->getEnv().getTeleportMgr()->removeTeleportLocation(tl)) {
			case TELEPORTMGR_ERR_NONE:
				m_server->SendChatMessage(peer_id,
						L"Teleport location removed.");
				break;
			case TELEPORTMGR_ERR_LOC_NOT_EXISTS:
				m_server->SendChatMessage(peer_id,
						L"Unable to remove this teleport location. It doesn't exists.");
				break;
			default: assert(false);
		}
	}
	else {
		m_server->SendChatMessage(peer_id, L"Invalid usage, see /help teleport remove");
	}

	return true;
}

}
