/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#include <string>
#include <vector>
#include "irrlichttypes_bloated.h"

class PlayerSAO;
class GameDatabase;

namespace epixel
{

struct TeleportLocation
{
	TeleportLocation(const std::string &l, float _x, float _y, float _z):
			x(_x), y(_y), z(_z), label(l) {}
	float x = 0, y = 0, z = 0;
	std::string label = "";
};

enum TeleportMgrError
{
	TELEPORTMGR_ERR_NONE,
	TELEPORTMGR_ERR_NAME_EXISTS,
	TELEPORTMGR_ERR_COORD_EXISTS,
	TELEPORTMGR_ERR_LOC_NOT_EXISTS,
};

class TeleportMgr
{
public:
	TeleportMgr(GameDatabase* db): m_db(db) { regenFormspec(); }
	~TeleportMgr() {}
	TeleportMgrError addTeleportLocation(const TeleportLocation& loc, const bool database_loading = false);
	TeleportMgrError removeTeleportLocation(const TeleportLocation& loc);
	void teleportIfValid(PlayerSAO* sao, const std::string& field);
	const std::string getFormSpec() const { return m_formspec; }
	void regenFormspec();
	bool exists(const TeleportLocation &loc);
	inline const u32 locCount() { return m_teleport_locations.size(); }
private:
	GameDatabase* m_db;
	static const std::string s_formspec_tpl;
	static const std::string s_formspec_tpl_end;
	static const std::string s_formspec_tpl_empty;
	std::string m_formspec = "";
	std::vector<TeleportLocation> m_teleport_locations = {};
};
}
