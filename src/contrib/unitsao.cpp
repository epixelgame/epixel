/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "content_sao.h"
#include "contrib/playersao.h"
#include "contrib/projectilesao.h"
#include "contrib/spell.h"
#include "server.h"


void UnitSAO::step(float dtime, bool send_recommended)
{
	// Look at Unit applied Dots
	for (std::vector<DamageOverTime*>::iterator it = m_applied_dots.begin();
		 it != m_applied_dots.end();) {
		DamageOverTime* dot = (*it);

		// Dot tick
		dot->current_period -= dtime;
		if (dot->current_period <= 0.0f) {
			dot->current_period = dot->period;
			doDamage(myrand_range(dot->damage_min,dot->damage_max));

			// No more HP, cleanup dots and leave the loop
			if (getHP() <= 0) {
				for (auto &applied_dot: m_applied_dots) {
					delete applied_dot;
					m_applied_dots.clear();
				}
				break;
			}
		}

		// Dot expiration
		dot->life -= dtime;
		if (dot->life <= 0.0f) {
			delete dot;
			it = m_applied_dots.erase(it);
		}
		else {
			++it;
		}
	}
}

bool UnitSAO::castSpell(const u32 spellId, v3f range_pos, float y_cast_offset) const
{
	epixel::Spell* spell = m_env->getGameDef()->getSpell(spellId);
	if (!spell) {
		// This should not occur
		assert(false);
	}

	// Check if target is in the cast range
	if (range_pos.getDistanceFrom(m_base_position) > spell->max_range * BS) {
		return false;
	}

	v3f spellspawn_pos = m_base_position;
	spellspawn_pos.Y += y_cast_offset;
	if (epixel::ProjectileSAO* spellProj = m_env->spawnProjectileActiveObject(spellId, getId(), spellspawn_pos)) {
		v3s16 npos = floatToInt(spellspawn_pos, BS);
		if (getType() == ACTIVEOBJECT_TYPE_PLAYER) {
			PlayerSAO* player = (PlayerSAO*)this;
			logger.info("Player %s casts spell %d at (%d,%d,%d)",
					player->getPlayer()->getName(), spellId, npos.X, npos.Y, npos.Z);
		}
		else {
			logger.info("Unit %d casts spell %d at (%d,%d,%d)", m_id, spellId, npos.X, npos.Y, npos.Z);
		}
		spellProj->applyVelocity(range_pos);
		return true;
	}

	return false;
}
