#include "base64.h"
#include <string.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>
#include <log.h>

size_t calcDecodeLength(const char* b64input) { //Calculates the length of a decoded string
	size_t len = strlen(b64input),
			padding = 0;

	if (b64input[len-1] == '=' && b64input[len-2] == '=') //last two chars are =
		padding = 2;
	else if (b64input[len-1] == '=') //last char is =
		padding = 1;

	return (len*3)/4 - padding;
}

int Base64Decode(const std::string &b64message, std::string &res)
{
	BIO *bio, *b64;

	const char* b64messagec = b64message.data();
	char* buffer;
	size_t length;

	size_t decodeLen = calcDecodeLength(b64messagec);
	buffer = (char*)malloc(decodeLen + 1);
	buffer[decodeLen] = '\0';

	bio = BIO_new_mem_buf((char*)b64messagec, -1);
	b64 = BIO_new(BIO_f_base64());
	bio = BIO_push(b64, bio);

	BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);
	length = (size_t) BIO_read(bio, buffer, b64message.size());
	BIO_free_all(bio);

	res.assign(buffer, length);
	delete [] buffer;

	return (0); //success
}

int Base64Encode(const std::string &input, std::string &res)
{
	const char* buffer = input.data();
	size_t length = input.size();
	BIO *bio, *b64;
	BUF_MEM *bufferPtr;

	b64 = BIO_new(BIO_f_base64());
	bio = BIO_new(BIO_s_mem());
	bio = BIO_push(b64, bio);

	BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL); //Ignore newlines - write everything in one line
	BIO_write(bio, buffer, length);
	(void) BIO_flush(bio);
	BIO_get_mem_ptr(bio, &bufferPtr);
	(void) BIO_set_close(bio, BIO_NOCLOSE);
	BIO_free_all(bio);

	res.assign((*bufferPtr).data, (*bufferPtr).length);
	delete [] (*bufferPtr).data;
	return (0); //success
}