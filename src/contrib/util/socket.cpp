/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "socket.h"
#include <cassert>

#ifdef _WIN32
	#include <io.h>
	#define LAST_SOCKET_ERR() WSAGetLastError()
	typedef SOCKET socket_t;
	typedef int socklen_t;
#else
	#include <fcntl.h>
	#include <unistd.h>
	#include <arpa/inet.h>
	#include <cstring>

	#define LAST_SOCKET_ERR() (errno)
	typedef int socket_t;
#endif

namespace epixel {

void Socket::startup_init() {
#ifdef _WIN32
	// Windows needs sockets to be initialized before use
	WSADATA WsaData;
	if(WSAStartup( MAKEWORD(2,2), &WsaData ) != NO_ERROR)
		throw SocketException("WSAStartup failed");
#endif
}

void Socket::deinit_atexit() {
#ifdef _WIN32
	// On Windows, cleanup sockets after use
	WSACleanup();
#endif
}

Socket::Socket(int proto): m_proto(proto)
{
	assert(proto == IPPROTO_UDP || proto == IPPROTO_TCP);

	init();
}

Socket::~Socket()
{
	sclose();
}

void Socket::init()
{
	m_sock = socket(AF_INET, m_proto == IPPROTO_TCP ? SOCK_STREAM : SOCK_DGRAM, m_proto);
	if (m_sock <= 0) {
		throw SocketException("Failed to create socket");
	}
}

void Socket::sclose()
{
	if (m_sock) {
#ifdef _WIN32
		closesocket(m_sock);
#else
		close(m_sock);
#endif
		m_sock = 0;
	}
}

const int Socket::setNonBlocking(const bool nblock) {
	if (m_sock < 0)
		return -1;

#ifdef _WIN32
	unsigned long mode = nblock ? 1 : 0;
	   return ioctlsocket(m_sock, FIONBIO, &mode);
#else
	int flags = fcntl(m_sock, F_GETFL, 0);
	if (flags < 0) return -1;
	flags = nblock ? (flags | O_NONBLOCK) : (flags & ~O_NONBLOCK);
	return fcntl(m_sock, F_SETFL, flags);
#endif
}

void Socket::setSocketTimeout(const unsigned int timeout_ms) {
	struct timeval timeout;
	timeout.tv_sec = timeout_ms * 1000;
	timeout.tv_usec = timeout_ms;

	if (setsockopt(m_sock, SOL_SOCKET, SO_RCVTIMEO, (char *) &timeout,
				   sizeof(timeout)) < 0) {
		throw SocketException("setsockopt failed for SO_RCVTIMEO");
	}

	if (setsockopt(m_sock, SOL_SOCKET, SO_SNDTIMEO, (char *) &timeout,
				   sizeof(timeout)) < 0) {
		throw ("setsockopt failed for SO_SNDTIMEO");
	}
}

const int Socket::isSocketValid(int sock) {
	if (!sock) {
		return -1;
	}

	int optval, rv;
	socklen_t optlen;

#ifdef WIN32
	rv = getsockopt(sock, SOL_SOCKET, SO_RCVBUF, (char*)&optval, &optlen);
#else
	rv = getsockopt(sock, SOL_SOCKET, SO_RCVBUF, &optval, &optlen);
#endif
	if (rv != 0) {
		return errno;
	}

	return 0;
}

int Socket::swrite(int sock, uint8_t *buf, size_t len) {
	size_t wrote = 0;

	do {
		int written = write(sock, buf + wrote, len);
		if (written < 0 && errno != EAGAIN) {
			return -1;
		}
		wrote += written;
		len -= written;
	}
	while (len);

	return wrote;
}

const bool Socket::resolve(const std::string &address, struct in_addr* addr) {
	struct addrinfo hints;
	memset(&hints, 0, sizeof(hints));

	// Setup hints
	hints.ai_socktype = 0;
	hints.ai_protocol = 0;
	hints.ai_flags = 0;
	hints.ai_family = AF_INET;

	struct addrinfo *res;

	int result = getaddrinfo (address.c_str(), NULL, &hints, &res);
	if (result == 0) {
		memcpy(addr, &((struct sockaddr_in *) res->ai_addr)->sin_addr,
			   sizeof(struct in_addr));
		freeaddrinfo(res);
		return true;
	}
	return false;
}

}