/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#ifdef _WIN32
#include "contrib/util/win32.h"
#else
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#endif

#include "exceptions.h"

class SocketException : public BaseException
{
public:
	SocketException(const std::string &s):
			BaseException(s)
	{}
};

namespace epixel
{
class Socket
{
public:
	Socket(int proto);
	virtual ~Socket();
	void init();
	static void startup_init();
	static void deinit_atexit();

	static int swrite(int sock, uint8_t *buf, size_t len);
	void sclose();
	static const bool resolve(const std::string &address, struct in_addr* addr);
protected:
	const int setNonBlocking(const bool nblock = true);
	void setSocketTimeout(const unsigned int timeout_ms);

	/**
	 * @brief isSocketValid
	 * @return apr_status_t
	 */
	static const int isSocketValid(int sock);

	int m_sock = 0;
	int m_proto = 0;
};
}