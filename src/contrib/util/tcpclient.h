/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#include "tcpsocket.h"
#include "irrlichttypes.h"
#include "log.h"
#include "porting.h"

namespace epixel
{

#define RECONNECTION_TIME_LIMIT 300000 // 5 mins

enum ConnectionState
{
	CONNECTION_STATE_NOT_CONNECTED,
	CONNECTION_STATE_CONNECTION_FAILED,
	CONNECTION_STATE_CONNECTED,
	CONNECTION_STATE_AUTHED,
};

template <typename T>
class TCPClient: public TCPSocket<T>
{
public:
	TCPClient(const std::string& client_name, const bool inc_reconnection_time):
		TCPSocket<T>(),
		m_client_name(client_name), m_inc_reconnection_time(inc_reconnection_time)
	{}
	/**
	 * @brief TCPClient::pushMsg
	 * @param msg
	 */
	void pushMsg(T msg)
	{
		this->m_send_queue.push_back(msg);
		this->m_sem.post();
	}

	const bool sconnect(const std::string host, const u16 port, const u16 timeout)
	{
		int rv;

		logger.notice("%s/Connecting to %s...", m_client_name.c_str(), host.c_str());

		this->setNonBlocking(false);
		this->setSocketTimeout(5000);

		struct sockaddr_in chost;
		if (!epixel::Socket::resolve(host.c_str(), &(chost.sin_addr))) {
			logger.error("%s/sconnect: Unable to resolve host %s", m_client_name.c_str(), host.c_str());
			this->sclose();
			return false;
		}

		chost.sin_port = htons(port);
		chost.sin_family = AF_INET;

		rv = connect(this->m_sock, (struct sockaddr*) &chost, sizeof(chost));
		if (rv != 0) {
			logger.error("%s/sconnect: unable to connect to server (rv: %d)", m_client_name.c_str(), errno);
			this->sclose();
			return false;
		}

		do {
			rv = this->isSocketValid(this->m_sock);
		}
		while (rv == EINPROGRESS);

		if (rv != 0) {
			logger.error("%s/sconnect: unable to connect to server, phase 2. (error %d)", m_client_name.c_str(), rv);
			this->sclose();
			return rv;
		}

		logger.notice("%s connected to %s:%d", m_client_name.c_str(), host.c_str(), port);
		this->setSocketTimeout(0);
		this->setNonBlocking();
		return true;
	}

protected:
	/**
	 * @brief closeSocket
	 */
	void closeSocket(const bool wait = true)
	{
		m_conn_state = CONNECTION_STATE_NOT_CONNECTED;

		// Don't listen anything & close socket
		this->sclose();

		if (wait) {
		    if (m_inc_reconnection_time) {
				if (m_reconnection_time * 2 > RECONNECTION_TIME_LIMIT) {
					m_reconnection_time = RECONNECTION_TIME_LIMIT;
				}
				else {
					m_reconnection_time *= 2;
				}
		    }
		    logger.notice("%s | Waiting %.0f seconds before retrying connection to the server...",
			    m_client_name.c_str(), m_reconnection_time / 1000.0f);
			// Create a new socket fd
			this->init();
		    TCPSocket<T>::m_sem.wait(m_reconnection_time);
		}
	}

	inline void reinitReconnectionTime() { m_reconnection_time = 1000; }

	std::string m_client_name = "";
	ConnectionState m_conn_state = CONNECTION_STATE_NOT_CONNECTED;

private:
	u32 m_reconnection_time = 1000;
	const bool m_inc_reconnection_time;
};

}
