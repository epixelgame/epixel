/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#include "contrib/util/socket.h"
#include "threads/thread.h"
#include "util/container.h"
#include "log.h"

namespace epixel
{

template <typename T>
class TCPSocket: public Socket, public Thread
{
public:
	TCPSocket(): Socket(IPPROTO_TCP)
	{}

	/**
	 * @brief isRecvQueueEmpty
	 * @return bool
	 */
	inline const bool isRecvQueueEmpty()
	{
		return m_recv_queue.empty();
	}

	virtual void pushMsg(T msg) = 0;

	/**
	 * @brief popRecvMsg
	 * @return T object or nullptr
	 */
	T popRecvMsg()
	{
		try {
			return m_recv_queue.pop_front(2);
		}
		catch (ItemNotFoundException &) {
			return nullptr;
		}
	}

	/**
	 * @brief stop
	 */
	inline void stop()
	{
		m_sem.post();
		Thread::stop();
	}

protected:
	/**
	 * @brief read_socket
	 *
	 * This method was imported from Apache 2.4 ajp socket source code
	 */
	static int read_socket(int sock, uint8_t *buf, size_t &len)
	{
		int length = len;
		size_t   rdlen  = 0;

		while (rdlen < len) {

			length = recv(sock, (char *)(buf + rdlen), (size_t)length, 0);
			if (errno == EAGAIN && rdlen > 0)
				continue;
			else if (length == 0)
				return 0; // Nothing to read
			else if (length == -1 || isSocketValid(sock) != 0)
				return -1;          /* any error. */

			rdlen += length;
			length = len - rdlen;
		}
		len -= rdlen;
		return rdlen;
	}



	// Queues
	MutexedQueue<T> m_recv_queue;
	MutexedQueue<T> m_send_queue;
	Semaphore m_sem;
};

}
