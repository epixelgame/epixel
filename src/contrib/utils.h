/*
 * Epixel
 * Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#include <regex>
namespace epixel
{

template<class T>
void vremove(std::vector<T> &src, T v)
{
	std::vector<T> new_v;
	for (const auto &old_v: src) {
		if (old_v == v) {
			continue;
		}

		new_v.push_back(old_v);
	}
	src = new_v;
}

template <class T, class A>
T join(const A &begin, const A &end, const T &t)
{
	T result;
	for (A it=begin; it!=end; it++) {
		if (!result.empty())
			result.append(t);
		result.append(*it);
	}
	return result;
}

inline bool isIPv4(const std::string &ip)
{
	std::regex re("^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
	std::smatch match;
	return std::regex_search(ip, match, re);
}

namespace regex
{
// Regex <X>,<Y>,<Z>
static const std::regex position("^([-]?[0-9]+),[ ]?([-]?[0-9]+),[ ]?([-]?[0-9]+)$");

inline float mtof(std::smatch &m, u16 pos)
{
	return (float)atof(m.str(pos).c_str());
}
}

}
