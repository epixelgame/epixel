
/*
Minetest
Copyright (C) 2013 celeron55, Perttu Ahola <celeron55@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef DATABASE_HEADER
#define DATABASE_HEADER

#include <atomic>
#include <cassert>
#include <vector>
#include <string>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include "contrib/awards.h"
#include "exceptions.h"
#include "irr_v3d.h"
#include "irrlichttypes.h"

#define CURRENT_GAMEDB_VERSION 59
#define CURRENT_MAPDB_VERSION 55
#define CURRENT_AUTHDB_VERSION 55

class DatabaseException : public BaseException {
public:
	DatabaseException(const std::string &s): BaseException(s) {}
};

namespace epixel
{
	struct Area;
	struct Ban;
	struct MapBlockDB;
	struct CreatureDef;
	struct CreatureDrop;
	struct Spell;
	struct TeleportLocation;

	class CreatureStore;
	class TeleportMgr;

	typedef std::unordered_map<std::string, CreatureDef*> CreatureDefMap;
	typedef std::vector<CreatureDrop> CreatureDropList;
	typedef std::unordered_map<u32, CreatureDropList*> CreatureDropMap;
	typedef std::unordered_map<std::string, std::string> CreatureNodeReplace;
	typedef std::unordered_map<u32, CreatureNodeReplace*> CreatureNodeReplaceMap;
	typedef std::unordered_map<u32,epixel::Spell*> SpellMap;
namespace abm
{
	class CreatureSpawningABM;
	class DBTreeSaplingABM;
	class NodeReplaceABM;
}
}

struct ItemDefinition;
class IWritableItemDefManager;
class GameScripting;
class CraftDefinition;
class Ore;
class OreManager;
class PlayerSAO;
class RemotePlayer;
class Decoration;
class DecorationManager;
class Biome;
class BiomeManager;
class SchematicManager;
class ServerEnvironment;
class INodeDefManager;
class IWritableNodeDefManager;

namespace epixel
{

class VirtualDatabase
{
public:
	virtual ~VirtualDatabase() {}
	virtual void precheck() = 0;
	virtual void init_worker() = 0;
	virtual void beginSave() = 0;
	virtual void endSave() = 0;

	virtual bool initialized() const { return true; }
};

template<class T>
class DatabasePool
{
public:
	DatabasePool(u8 connection_number)
	{
		assert(connection_number > 0);
		m_pool.push_back(new T());
		static_cast<T*>(m_pool[0])->precheck();
		static_cast<T*>(m_pool[0])->init_worker();
		for (u16 i = 1; i < connection_number; i++) {
			m_pool.push_back(new T());
			static_cast<T*>(m_pool[i])->init_worker();
		}
		m_cursor = 0;
	}

	DatabasePool(u8 connection_number, const std::string &P, const std::string &name)
	{
		assert(connection_number > 0);
		m_pool.push_back(new T(P, name));
		static_cast<T*>(m_pool[0])->precheck();
		static_cast<T*>(m_pool[0])->init_worker();
		for (u16 i = 1; i < connection_number; i++) {
			m_pool.push_back(new T(P, name));
			static_cast<T*>(m_pool[i])->init_worker();
		}
		m_cursor = 0;
	}

	~DatabasePool()
	{
		for (auto &db: m_pool) {
			delete db;
		}
		m_pool.clear();
	}

	T* get(int c = -1)
	{
		int c_to_use = c;
		if (c == -1) {
			m_cursor++;
			if (m_cursor >= m_pool.size()) {
				m_cursor = 0;
			}
			c_to_use = m_cursor;
		}

		return m_pool[c_to_use];
	}

	const u16 getCursor() const { return m_cursor; }
private:
	std::vector<T*> m_pool;
	std::atomic<u16> m_cursor;
};

}

class MapDatabase: public epixel::VirtualDatabase
{
public:
	virtual bool saveBlock(const v3s16 &pos, const std::string &data, const epixel::MapBlockDB &blockdb_data) = 0;
	virtual void loadBlock(const v3s16 &pos, std::string &data, epixel::MapBlockDB &blockdb_data) = 0;

	virtual void listAllLoadableBlocks(std::vector<v3s16> &dst) = 0;

	// Env Meta
	virtual bool saveEnvMeta(u32 g_time, u32 time_of_day, s16 automapgen_offset, const u64 &lbm_version, const std::string &lbm_introtimes_str, const u32 day_count) = 0;
	virtual bool loadEnvMeta(ServerEnvironment* s_env) = 0;

};

class AuthDatabase: public epixel::VirtualDatabase
{
public:
	virtual bool getUsernamesWithoutCase(const std::string &login, std::vector<std::string> &names) = 0;

	virtual u32 userExists(const std::string &login) = 0;
	virtual bool userExists(u32 id) = 0;
	virtual bool getUserListByIds(const std::vector<u32> &user_ids, std::vector<std::string> &res) = 0;

	virtual u32 getLastLogin(const u32 id) = 0;
	virtual bool setLastLogin(const std::string &login) = 0;

	virtual bool createUser(const std::string &login, const std::string &pwdhash) = 0;
	virtual bool removeUser(u32 id) = 0;
	virtual bool loadUser(const std::string &login, std::string &dstpassword) = 0;
	virtual bool loadUserAndVerifyPassword(const std::string &login, const std::string &pwdhash) = 0;
	virtual bool setPassword(const std::string &login, const std::string &pwdhash) = 0;
};

class GameDatabase: public epixel::VirtualDatabase
{
public:
	virtual bool saveArea(epixel::Area* area) = 0;
	virtual bool deleteArea(u32 areaId) = 0;
	virtual bool loadAreas(std::unordered_map<u32, epixel::Area*> &areas) = 0;

	virtual bool savePlayerBan(const std::string&, epixel::Ban*) = 0;
	virtual bool saveIPBan(const std::string&, epixel::Ban*) = 0;
	virtual bool deletePlayerBan(const std::string&) = 0;
	virtual bool deleteIPBan(const std::string&) = 0;
	virtual bool loadPlayerBans(std::unordered_map<std::string, epixel::Ban*> &bans) = 0;
	virtual bool loadIPBans(std::unordered_map<std::string, epixel::Ban*> &bans) = 0;

	virtual bool loadCrafts(std::vector<CraftDefinition*> &crafts) = 0;

	virtual bool loadTips(std::vector<std::wstring> &tips) = 0;
	virtual bool loadTreeABM(INodeDefManager* ndef, std::vector<epixel::abm::DBTreeSaplingABM*> &abms) = 0;
	virtual bool loadNodeReplaceABM(INodeDefManager* ndef, std::vector<epixel::abm::NodeReplaceABM*> &abms) = 0;
	virtual bool loadOres(std::vector<Ore*> &ores, OreManager* oremgr) = 0;
	virtual bool loadBiomes(std::vector<Biome *> &biomes, BiomeManager *biomemgr) = 0;
	virtual bool loadDecorations(std::vector<Decoration*> &decorations, DecorationManager* decomgr, SchematicManager *schmgr, BiomeManager* biomemgr) = 0;

	/*
	 * Items
	 */
	virtual bool loadItemDefinitions(IWritableItemDefManager* idef, IWritableNodeDefManager* ndef, GameScripting* script) = 0;

	/*
	 * Creatures
	 */
	virtual bool loadLoots(epixel::CreatureDropMap &creaturedrops, u32 &loot_nb) = 0;
	virtual bool loadCreatureNodeReplace(epixel::CreatureNodeReplaceMap &nodereplace, u32 &replace_nb) = 0;
	virtual bool loadCreatureDefinitions(epixel::CreatureDefMap &creaturedefs, epixel::CreatureStore* cstore,
			u32 &meshes_nb, u32 &textures_nb, const epixel::SpellMap &spells) = 0;
	virtual bool loadCreatureSpawningABM(INodeDefManager* ndef, std::vector<epixel::abm::CreatureSpawningABM*> &abms) = 0;
	virtual bool loadSpells(epixel::SpellMap &spells) = 0;

	/*
	 * Auth
	 */

	virtual bool loadUserPrivs(const u32 user_id, std::set<std::string> &privs) = 0;

	// Ingame player
	virtual bool loadPlayerExtendedAttributes(PlayerSAO* sao) = 0;
	virtual bool savePlayerExtendedAttributes(const u32 user_id, const std::unordered_map<std::string, std::string> &attributes) = 0;
	virtual bool loadPlayerIgnores(PlayerSAO* sao) = 0;
	virtual bool addPlayerIgnore(const u32 user_id, const std::string &ignorename) = 0;
	virtual bool removePlayerIgnore(const u32 user_id, const std::string &ignorename) = 0;

	// Channels
	virtual bool addPlayerToChannel(const u32 user_id, const std::string &channel) = 0;
	virtual bool removePlayerFromChannel(const u32 user_id, const std::string &channel) = 0;
	virtual bool loadPlayerChannels(const u32 user_id, std::vector<std::string> &channels) = 0;

	// Privileges
	virtual bool addPrivs(u32 id, const std::vector<std::string> &privs) = 0;
	virtual bool removePrivs(u32 id, const std::vector<std::string> &privs) = 0;

	// Player
	virtual bool savePlayer(RemotePlayer* player) = 0;
	virtual bool loadPlayer(RemotePlayer* player, PlayerSAO* sao) = 0;
	virtual bool playerDataExists(const u32 user_id) = 0;
	virtual bool setPlayerHome(RemotePlayer* player) = 0;

	// Teleporters
	virtual bool addTeleportLocation(const epixel::TeleportLocation& tl) = 0;
	virtual bool removeTeleportLocation(const epixel::TeleportLocation& tl) = 0;
	virtual bool loadTeleportLocations(epixel::TeleportMgr* tmgr) = 0;

	// Awards
	virtual bool loadAchievements(std::unordered_map<u32, epixel::Achievement> &achievements) = 0;
};

#endif
