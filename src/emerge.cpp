/*
Minetest
Copyright (C) 2010-2013 celeron55, Perttu Ahola <celeron55@gmail.com>
Copyright (C) 2010-2013 kwolekr, Ryan Kwolek <kwolekr@minetest.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "emerge.h"

#include <iostream>
#include <queue>

#include "util/container.h"
#include "threads/event.h"

#include "config.h"
#include "constants.h"
#include "environment.h"
#include "log.h"
#include "map.h"
#include "mapblock.h"
#include "mapgen_v6.h"
#include "mapgen_v7.h"
#include "mapgen_valleys.h"
#include "mapgen_singlenode.h"
#include "mg_biome.h"
#include "mg_ore.h"
#include "mg_decoration.h"
#include "mg_schematic.h"
#include "nodedef.h"
#include "scripting_game.h"
#include "server.h"
#include "serverobject.h"
#include "settings.h"
#include "voxel.h"


struct MapgenDesc {
	const char *name;
	MapgenFactory *factory;
	bool is_user_visible;
};

class EmergeThread : public Thread {
public:
	int id;

	EmergeThread(Server *server, int ethreadid);
	~EmergeThread();

	void *run();
	void signal();

	// Requires queue mutex held
	bool pushBlock(const v3s16& pos);

	void cancelPendingItems();
private:
	Server *m_server;
	ServerMap *m_map;
	EmergeManager *m_emerge;
	Mapgen *m_mapgen;

	Event m_queue_event;
	std::queue<v3s16> m_block_queue;

	bool popBlockEmerge(v3s16 *pos, BlockEmergeData *bedata);

	EmergeAction getBlockOrStartGen(const v3s16& pos, bool allow_gen, MapBlock **block, BlockMakeData *data);
	MapBlock *finishGen(const v3s16& pos, BlockMakeData *bmdata,
		std::map<v3s16, MapBlock *> *modified_blocks);

	friend class EmergeManager;
};

////
//// Built-in mapgens
////

MapgenDesc g_reg_mapgens[] = {
	{"v6",         new MapgenFactoryV6,         true},
	{"v7",         new MapgenFactoryV7,         true},
	{"valleys",    new MapgenFactoryValleys,    true},
	{"singlenode", new MapgenFactorySinglenode, false},
};

BlockMakeData::~BlockMakeData()
{
	delete vmanip;
}

////
//// EmergeManager
////

EmergeManager::EmergeManager(IGameDef *gamedef)
{
	this->ndef      = gamedef->getNodeDefManager();
	this->biomemgr  = new BiomeManager(gamedef);
	this->oremgr    = new OreManager(gamedef);
	this->decomgr   = new DecorationManager(gamedef);
	this->schemmgr  = new SchematicManager(gamedef);
	this->gen_notify_on = 0;

	// Note that accesses to this variable are not synchronized.
	// This is because the *only* thread ever starting or stopping
	// EmergeThreads should be the ServerThread.
	this->m_threads_active = false;

	// If unspecified, leave a proc for the main thread and one for
	// some other misc thread
	u16 nthreads = g_settings->get(U16SETTING_NUM_EMERGE_THREADS);
	if (!nthreads)
		nthreads = porting::getNumberOfProcessors() - 2;
	if (nthreads < 1)
		nthreads = 1;

	// don't trust user input for something very important like this
	if (g_settings->get(U16SETTING_EMERGEQUEUE_LIMIT_TOTAL) < 1) {
		g_settings->set(U16SETTING_EMERGEQUEUE_LIMIT_TOTAL, nthreads * 5 + 1);
	}

	if (g_settings->get(U16SETTING_EMERGEQUEUE_LIMIT_DISKONLY) < 1) {
		g_settings->set(U16SETTING_EMERGEQUEUE_LIMIT_DISKONLY, 1);
	}

	if (g_settings->get(U16SETTING_EMERGEQUEUE_LIMIT_GENERATE) < 1) {
		g_settings->set(U16SETTING_EMERGEQUEUE_LIMIT_GENERATE, nthreads + 1);
	}

	for (s16 i = 0; i < nthreads; i++)
		m_threads.push_back(new EmergeThread((Server *)gamedef, i));

	logger.info("EmergeManager: using %d threads", nthreads);
}


EmergeManager::~EmergeManager()
{
	for (u32 i = 0; i != m_threads.size(); i++) {
		EmergeThread *thread = m_threads[i];

		if (m_threads_active) {
			thread->stop();
			thread->signal();
			thread->wait();
		}

		delete thread;
		delete m_mapgens[i];
	}

	delete biomemgr;
	delete oremgr;
	delete decomgr;
	delete schemmgr;

	delete params.sparams;
}

void EmergeManager::loadMapgenParams()
{
	params.load(*g_settings);
}


void EmergeManager::initMapgens()
{
	if (m_mapgens.size())
		return;

	MapgenFactory *mgfactory = getMapgenFactory(params.mg_name);
	if (!mgfactory) {
		logger.error("EmergeManager: mapgen %s not registered; falling back to %s",
				params.mg_name.c_str(), DEFAULT_MAPGEN);

		params.mg_name = DEFAULT_MAPGEN;

		mgfactory = getMapgenFactory(params.mg_name);
		FATAL_ERROR_IF(mgfactory == NULL, "Couldn't use any mapgen!");
	}

	if (!params.sparams) {
		params.sparams = mgfactory->createMapgenParams();
		params.sparams->readParams(g_settings);
	}

	for (u32 i = 0; i != m_threads.size(); i++) {
		Mapgen *mg = mgfactory->createMapgen(i, &params, this);
		m_mapgens.push_back(mg);
	}
}

void EmergeManager::startThreads()
{
	if (m_threads_active)
		return;

	for (u32 i = 0; i != m_threads.size(); i++)
		m_threads[i]->start();

	m_threads_active = true;
}

void EmergeManager::stopThreads()
{
	if (!m_threads_active)
		return;

	// Request thread stop in parallel
	for (u32 i = 0; i != m_threads.size(); i++) {
		m_threads[i]->stop();
		m_threads[i]->signal();
	}

	// Then do the waiting for each
	for (u32 i = 0; i != m_threads.size(); i++)
		m_threads[i]->wait();

	m_threads_active = false;
}


bool EmergeManager::isRunning()
{
	return m_threads_active;
}


bool EmergeManager::enqueueBlockEmerge(
	u16 peer_id,
	const v3s16& blockpos,
	bool allow_generate,
	bool ignore_queue_limits)
{
	u16 flags = 0;
	if (allow_generate)
		flags |= BLOCK_EMERGE_ALLOW_GEN;
	if (ignore_queue_limits)
		flags |= BLOCK_EMERGE_FORCE_QUEUE;

	EmergeThread *thread = NULL;
	bool entry_already_exists = false;

	{
		std::lock_guard<std::mutex> queuelock(m_queue_mutex);

		if (!pushBlockEmergeData(blockpos, peer_id, flags,
				&entry_already_exists))
			return false;

		if (entry_already_exists)
			return true;

		thread = getOptimalThread();
		thread->pushBlock(blockpos);
	}

	thread->signal();
	return true;
}

//
// Mapgen-related helper functions
//

inline v3s16 EmergeManager::getContainingChunk(const v3s16& blockpos)
{
	return getContainingChunk(blockpos, params.chunksize);
}


v3s16 EmergeManager::getContainingChunk(const v3s16& blockpos, s16 chunksize)
{
	s16 coff = -chunksize / 2;
	v3s16 chunk_offset(coff, coff, coff);

	return getContainerPos(blockpos - chunk_offset, chunksize)
		* chunksize + chunk_offset;
}


int EmergeManager::getGroundLevelAtPoint(v2s16 p)
{
	if (m_mapgens.size() == 0 || !m_mapgens[0]) {
		logger.error("EmergeManager: getGroundLevelAtPoint() called before mapgen init");
		return 0;
	}

	return m_mapgens[0]->getGroundLevelAtPoint(p);
}


bool EmergeManager::isBlockUnderground(const v3s16& blockpos)
{
#if 0
	v2s16 p = v2s16((blockpos.X * MAP_BLOCKSIZE) + MAP_BLOCKSIZE / 2,
					(blockpos.Y * MAP_BLOCKSIZE) + MAP_BLOCKSIZE / 2);
	int ground_level = getGroundLevelAtPoint(p);
	return blockpos.Y * (MAP_BLOCKSIZE + 1) <= min(water_level, ground_level);
#endif

	// Use a simple heuristic; the above method is wildly inaccurate anyway.
	return blockpos.Y * (MAP_BLOCKSIZE + 1) <= params.water_level;
}


void EmergeManager::getMapgenNames(
	std::vector<const char *> *mgnames, bool include_hidden)
{
	for (u32 i = 0; i != ARRLEN(g_reg_mapgens); i++) {
		if (include_hidden || g_reg_mapgens[i].is_user_visible)
			mgnames->push_back(g_reg_mapgens[i].name);
	}
}


MapgenFactory *EmergeManager::getMapgenFactory(const std::string &mgname)
{
	for (u32 i = 0; i != ARRLEN(g_reg_mapgens); i++) {
		if (mgname == g_reg_mapgens[i].name)
			return g_reg_mapgens[i].factory;
	}

	return NULL;
}


bool EmergeManager::pushBlockEmergeData(const v3s16& pos,
	u16 peer_requested,
	u16 flags,
	bool *entry_already_exists)
{
	u16 &count_peer = m_peer_queue_count[peer_requested];

	if ((flags & BLOCK_EMERGE_FORCE_QUEUE) == 0) {
		if (m_blocks_enqueued.size() >= g_settings->get(U16SETTING_EMERGEQUEUE_LIMIT_TOTAL))
			return false;

		if (peer_requested != PEER_ID_INEXISTENT) {
			u16 qlimit_peer = (flags & BLOCK_EMERGE_ALLOW_GEN) ?
				g_settings->get(U16SETTING_EMERGEQUEUE_LIMIT_GENERATE) : g_settings->get(U16SETTING_EMERGEQUEUE_LIMIT_DISKONLY);
			if (count_peer >= qlimit_peer)
				return false;
		}
	}

	std::pair<std::map<v3s16, BlockEmergeData>::iterator, bool> findres;
	findres = m_blocks_enqueued.insert(std::make_pair(pos, BlockEmergeData()));

	BlockEmergeData &bedata = findres.first->second;
	*entry_already_exists   = !findres.second;

	if (*entry_already_exists) {
		bedata.flags |= flags;
	} else {
		bedata.flags = flags;
		bedata.peer_requested = peer_requested;

		count_peer++;
	}

	return true;
}


bool EmergeManager::popBlockEmergeData(const v3s16& pos,
	BlockEmergeData *bedata)
{
	std::map<v3s16, BlockEmergeData>::iterator it;
	std::unordered_map<u16, u16>::iterator it2;

	it = m_blocks_enqueued.find(pos);
	if (it == m_blocks_enqueued.end())
		return false;

	*bedata = it->second;

	it2 = m_peer_queue_count.find(bedata->peer_requested);
	if (it2 == m_peer_queue_count.end())
		return false;

	u16 &count_peer = it2->second;
	assert(count_peer != 0);
	count_peer--;

	m_blocks_enqueued.erase(it);

	return true;
}


EmergeThread *EmergeManager::getOptimalThread()
{
	size_t nthreads = m_threads.size();

	FATAL_ERROR_IF(nthreads == 0, "No emerge threads!");

	size_t index = 0;
	size_t nitems_lowest = m_threads[0]->m_block_queue.size();

	for (size_t i = 1; i < nthreads; i++) {
		size_t nitems = m_threads[i]->m_block_queue.size();
		if (nitems < nitems_lowest) {
			index = i;
			nitems_lowest = nitems;
		}
	}

	return m_threads[index];
}


////
//// EmergeThread
////

EmergeThread::EmergeThread(Server *server, int ethreadid) :
	id(ethreadid),
	m_server(server),
	m_map(NULL),
	m_emerge(NULL),
	m_mapgen(NULL)
{
}

EmergeThread::~EmergeThread()
{
}

void EmergeThread::signal()
{
	m_queue_event.signal();
}


bool EmergeThread::pushBlock(const v3s16& pos)
{
	m_block_queue.push(pos);
	return true;
}


void EmergeThread::cancelPendingItems()
{
	std::lock_guard<std::mutex> queuelock(m_emerge->m_queue_mutex);

	v3s16 pos;
	BlockEmergeData bedata;
	while (!m_block_queue.empty()) {
		pos = m_block_queue.front();
		m_block_queue.pop();
		m_emerge->popBlockEmergeData(pos, &bedata);
	}
}

bool EmergeThread::popBlockEmerge(v3s16 *pos, BlockEmergeData *bedata)
{
	std::lock_guard<std::mutex> queuelock(m_emerge->m_queue_mutex);

	if (m_block_queue.empty())
		return false;

	*pos = m_block_queue.front();
	m_block_queue.pop();

	m_emerge->popBlockEmergeData(*pos, bedata);

	return true;
}


EmergeAction EmergeThread::getBlockOrStartGen(
	const v3s16& pos, bool allow_gen, MapBlock **block, BlockMakeData *bmdata)
{
	{
		std::lock_guard<std::mutex> envlock(m_server->m_map_mutex);

		// 1). Attempt to fetch block from memory
		*block = m_map->getBlockNoCreateNoEx(pos);
		if (*block && !(*block)->isDummy() && (*block)->isGenerated())
			return EMERGE_FROM_MEMORY;
	}

	{
		std::lock_guard<std::mutex> envlock(m_server->m_map_mutex);

		// 2). Attempt to load block from disk
		*block = m_map->loadBlock(pos);
		if (*block && (*block)->isGenerated())
			return EMERGE_FROM_DISK;
	}

	{
		std::lock_guard<std::mutex> envlock(m_server->m_map_mutex);

		// 3). Attempt to start generation
		if (allow_gen && m_map->initBlockMake(pos, bmdata))
			return EMERGE_GENERATED;
	}

	// All attempts failed; cancel this block emerge
	return EMERGE_CANCELLED;
}


MapBlock *EmergeThread::finishGen(const v3s16& pos, BlockMakeData *bmdata,
	std::map<v3s16, MapBlock *> *modified_blocks)
{
	std::lock_guard<std::mutex> envlock(m_server->m_map_mutex);

	/*
		Perform post-processing on blocks (invalidate lighting, queue liquid
		transforms, etc.) to finish block make
	*/
	m_map->finishBlockMake(bmdata, modified_blocks);

	MapBlock *block = m_map->getBlockNoCreateNoEx(pos);
	if (!block) {
		logger.crit("EmergeThread::finishGen: Couldn't grab block we "
			"just generated: (%d,%d,%d)", pos.X, pos.Y, pos.Z);
		return NULL;
	}

	v3s16 minp = bmdata->blockpos_min * MAP_BLOCKSIZE;
	v3s16 maxp = bmdata->blockpos_max * MAP_BLOCKSIZE +
				 v3s16(1,1,1) * (MAP_BLOCKSIZE - 1);

	// Ignore map edit events, they will not need to be sent
	// to anybody because the block hasn't been sent to anybody
	MapEditEventAreaIgnorer ign(
		&m_server->m_ignore_map_edit_events_area,
		VoxelArea(minp, maxp));

	/*
		Run Lua on_generated callbacks
	*/
	try {
		m_server->getScriptIface()->environment_OnGenerated(
			minp, maxp, m_mapgen->blockseed);
	} catch (LuaError &e) {
		m_server->setAsyncFatalError("Lua: " + std::string(e.what()));
	}

	/*
		Activate the block
	*/
	m_server->m_env->activateBlock(block);

	return block;
}


void *EmergeThread::run()
{
	ThreadStarted();
	porting::setThreadName((std::string("EmergeThread" + itos(id)).c_str()));
	BEGIN_DEBUG_EXCEPTION_HANDLER

	v3s16 pos;
	BlockEmergeData bedata;
	BlockMakeData bmdata;
	EmergeAction action;

	m_map    = (ServerMap *)&(m_server->m_env->getMap());
	m_emerge = m_server->m_emerge;
	m_mapgen = m_emerge->m_mapgens[id];

	try {
	while (!stopRequested()) {
		std::map<v3s16, MapBlock *> modified_blocks;
		MapBlock *block;

		if (!popBlockEmerge(&pos, &bedata)) {
			m_queue_event.wait();
			continue;
		}

		if (blockpos_over_limit(pos))
			continue;

		bool allow_gen = bedata.flags & BLOCK_EMERGE_ALLOW_GEN;

		action = getBlockOrStartGen(pos, allow_gen, &block, &bmdata);
		if (action == EMERGE_GENERATED) {
			m_mapgen->makeChunk(&bmdata);
			block = finishGen(pos, &bmdata, &modified_blocks);
		}

		if (block)
			modified_blocks[pos] = block;

		if (!modified_blocks.empty())
			m_server->AsyncSetBlocksNotSent(modified_blocks);
	}
	} catch (VersionMismatchException &e) {
		std::ostringstream err;
		err << "World data version mismatch in MapBlock " << PP(pos) << std::endl
			<< "----" << std::endl
			<< "\"" << e.what() << "\"" << std::endl
			<< "See debug.txt." << std::endl
			<< "World probably saved by a newer version of " << PROJECT_NAME_C << "."
			<< std::endl;
		m_server->setAsyncFatalError(err.str());
	} catch (SerializationError &e) {
		std::ostringstream err;
		err << "Invalid data in MapBlock " << PP(pos) << std::endl
			<< "----" << std::endl
			<< "\"" << e.what() << "\"" << std::endl
			<< "See debug.txt." << std::endl
			<< "You can ignore this using [ignore_world_load_errors = true]."
			<< std::endl;
		m_server->setAsyncFatalError(err.str());
	}

	END_DEBUG_EXCEPTION_HANDLER()
	return NULL;
}
