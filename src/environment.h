/*
Minetest
Copyright (C) 2010-2013 celeron55, Perttu Ahola <celeron55@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ENVIRONMENT_HEADER
#define ENVIRONMENT_HEADER

/*
	This class is the game's environment.
	It contains:
	- The map
	- Players
	- Other objects
	- The current time in the game
	- etc.
*/

#include <cassert>
#include <set>
#include <list>
#include <queue>
#include <map>
#include <atomic>
#include <unordered_map>
#include "util/macros.h"
#include "irr_v3d.h"
#include "activeobject.h"
#include "util/numeric.h"
#include "mapnode.h"
#include "network/networkprotocol.h" // for AccessDeniedCode

class ServerEnvironment;
class ActiveBlockModifier;
class ServerActiveObject;
class ITextureSource;
class IGameDef;
class Map;
class ServerMap;
class ClientMap;
class GameScripting;
class InventoryList;
class Player;
class PlayerSAO;
class LuaEntitySAO;
class UnitSAO;
struct ItemStack;
class RemotePlayer;
class Server;
class MapBlock;
class Inventory;
class NodeTimer;

namespace epixel
{
struct Spell;
class ItemSAO;
class FallingSAO;
class ProjectileSAO;
class TeleportMgr;
class AreaMgr;
}

class Environment
{
public:
	// Environment will delete the map passed to the constructor
	Environment();
	virtual ~Environment();

	/*
		Step everything in environment.
		- Move players
		- Step mobs
		- Run timers of map
	*/
	virtual void step(f32 dtime) = 0;

	virtual Map & getMap() = 0;

	virtual void addPlayer(Player *player);
	void removePlayer(RemotePlayer *player);
	Player * getPlayer(u16 peer_id);
	Player * getPlayer(const char *name);

	u32 getDayNightRatio();

	// 0-23999
	void setTimeOfDay(const u32 time)
	{
		m_time_of_day = time;
		m_time_of_day_f = (float)time / 24000.0;
	}

	const u32 getTimeOfDay() const { return m_time_of_day; }
	const float getTimeOfDayF() const { return m_time_of_day_f; }

	void stepTimeOfDay(const float dtime);

	inline void setTimeOfDaySpeed(const float speed) { m_time_of_day_speed = speed; }
	const float getTimeOfDaySpeed() const { return m_time_of_day_speed; }

	void setDayNightRatioOverride(bool enable, u32 value)
	{
		m_enable_day_night_ratio_override = enable;
		m_day_night_ratio_override = value;
	}

	inline const u32 getDayCount() const { return m_day_count; }
	inline void setDayCount(const u32 dc) { m_day_count = dc; }

	// counter used internally when triggering ABMs
	u32 m_added_objects;

protected:
	// peer_ids in here should be unique, except that there may be many 0s
	std::vector<Player*> m_players;
	// Time of day in milli-hours (0-23999); determines day and night
	std::atomic<u32> m_time_of_day;
	// Time of day in 0...1
	std::atomic<float> m_time_of_day_f;
	std::atomic<float> m_time_of_day_speed;
	// Used to buffer dtime for adding to m_time_of_day
	float m_time_counter;
	// Overriding the day-night ratio is useful for custom sky visuals
	bool m_enable_day_night_ratio_override;
	u32 m_day_night_ratio_override;
	// Days from the server start, accounts for time shift
	// in game (e.g. /time or bed usage)
	std::atomic<u32> m_day_count;

	bool m_cache_enable_shaders;

private:
	DISABLE_CLASS_COPY(Environment);
};

struct LoadingBlockModifierDef
{
	// Set of contents to trigger on
	std::set<std::string> trigger_contents;
	std::string name;
	bool run_at_every_load;

	virtual ~LoadingBlockModifierDef() {}
	virtual void trigger(ServerEnvironment *env, v3s16 p, MapNode n){};
};

struct LBMContentMapping
{
	typedef std::map<content_t, std::vector<LoadingBlockModifierDef *> > container_map;
	container_map map;

	std::vector<LoadingBlockModifierDef *> lbm_list;

	// Needs to be separate method (not inside destructor),
	// because the LBMContentMapping may be copied and destructed
	// many times during operation in the lbm_lookup_map.
	void deleteContents();
	void addLBM(LoadingBlockModifierDef *lbm_def, IGameDef *gamedef);
	const std::vector<LoadingBlockModifierDef *> *lookup(content_t c) const;
};

class LBMManager
{
public:
	LBMManager():
		m_query_mode(false)
	{}

	~LBMManager();

	// Don't call this after loadIntroductionTimes() ran.
	void addLBMDef(LoadingBlockModifierDef *lbm_def);

	void loadIntroductionTimes(const std::string &times,
		Server *gamedef, u32 now);

	// Don't call this before loadIntroductionTimes() ran.
	std::string createIntroductionTimesString();

	// Don't call this before loadIntroductionTimes() ran.
	void applyLBMs(ServerEnvironment *env, MapBlock *block, u32 stamp);

	// Warning: do not make this std::unordered_map, order is relevant here
	typedef std::map<u32, LBMContentMapping> lbm_lookup_map;

private:
	// Once we set this to true, we can only query,
	// not modify
	bool m_query_mode;

	// For m_query_mode == false:
	// The key of the map is the LBM def's name.
	std::unordered_map<std::string, LoadingBlockModifierDef *> m_lbm_defs;

	// For m_query_mode == true:
	// The key of the map is the LBM def's first introduction time.
	lbm_lookup_map m_lbm_lookup;

	// Returns an iterator to the LBMs that were introduced
	// after the given time. This is guaranteed to return
	// valid values for everything
	lbm_lookup_map::const_iterator getLBMsIntroducedAfter(u32 time)
	{ return m_lbm_lookup.lower_bound(time); }
};

/*
	List of active blocks, used by ServerEnvironment
*/

class ActiveBlockList
{
public:
	void update(std::vector<v3s16> &active_positions,
			s16 radius,
			std::set<v3s16> &blocks_removed,
			std::set<v3s16> &blocks_added);

	bool contains(v3s16 p){
		return (m_list.find(p) != m_list.end());
	}

	void clear(){
		m_list.clear();
	}

	std::set<v3s16> m_list;
	std::set<v3s16> m_forceloaded_list;
};

class ABMHandler
{
public:
	ABMHandler(ServerEnvironment* env);
	~ABMHandler();
	void step(const float dtime_s);

	void apply(MapBlock *block);
	void addABM(ActiveBlockModifier* abm) { m_abms.push_back(abm); }
	void resetTriggeredTimers();

private:
	// Find out how many objects the given block and its neighbours contain.
	// Returns the number of objects in the block, and also in 'wider' the
	// number of objects in the block and all its neighbours. The latter
	// may an estimate if any neighbours are unloaded.
	u32 countObjects(MapBlock *block, ServerMap * map, u32 &wider);

	ServerEnvironment *m_env;
	std::vector<ActiveBlockModifier*> m_abms;
	std::vector<ActiveBlockModifier*> m_tabm[MAX_REGISTERED_CONTENT];
	std::atomic<bool> m_aabms_calculated;
};

/*
	The server-side environment.

	This is not thread-safe. Server uses an environment mutex.
*/

enum EnvironmentCacheIds {
	ENVIRONMENT_NODECACHE_GROUP_WATER,
	ENVIRONMENT_NODECACHE_CHEST,
	ENVIRONMENT_NODECACHE_CHEST_LOCKED,
	ENVIRONMENT_NODECACHE_OBSIDIAN,
	ENVIRONMENT_NODECACHE_OBSIDIAN_BRICK,
	ENVIRONMENT_NODECACHE_FURNACE,
	ENVIRONMENT_NODECACHE_FURNACE_ACTIVE,
	ENVIRONMENT_NODECACHE_ID_MAX
};


class ServerEnvironment : public Environment
{
public:
	ServerEnvironment(ServerMap *map, GameScripting *scriptIface,
			Server *gamedef, const std::string &path_world, epixel::AreaMgr* areaMgr);
	~ServerEnvironment();

	Map & getMap();

	ServerMap* getServerMap();

	//TODO find way to remove this fct!
	GameScripting* getScriptIface()
		{ return m_script; }

	Server *getGameDef() { return m_gamedef; }

	RemotePlayer* getPlayerByDBId(const u32 db_id);
	RemotePlayer* getPlayer(u16 peer_id) { return (RemotePlayer*) Environment::getPlayer(peer_id); }
	RemotePlayer* getPlayer(const char* name) { return (RemotePlayer*) Environment::getPlayer(name); }

	void kickAllPlayers(AccessDeniedCode reason,
		const std::string &str_reason, bool reconnect, bool kick_superadmin = true);
	// Save players
	void saveLoadedPlayers();
	void savePlayer(RemotePlayer *player);
	bool isPlayerNearPosition(const v3f &p, const f32 dist);

	/*
		Save and load time of day and game timer
	*/
	void saveMeta();
	void loadMeta();

	u32 addParticleSpawner(float exptime);
	void deleteParticleSpawner(u32 id);

	/*
		External ActiveObject interface
		-------------------------------------------
	*/

	ServerActiveObject* getActiveObject(u16 id);

	/*
		Add an active object to the environment.
		Environment handles deletion of object.
		Object may be deleted by environment immediately.
		If id of object is 0, assigns a free id to it.
		Returns the id of the object.
		Returns 0 if not added and thus deleted.
	*/
	u16 addActiveObject(ServerActiveObject *object);

	epixel::ItemSAO* spawnItemActiveObject(const std::string &itemName, v3f pos,
			const ItemStack& items);

	epixel::FallingSAO *spawnFallingActiveObject(const std::string &nodeName, v3f pos,
			const MapNode n);

	epixel::ProjectileSAO *spawnProjectileActiveObject(const u32 spellId, const u32 casterId, v3f pos);

	/*
		Add an active object as a static object to the corresponding
		MapBlock.
		Caller allocates memory, ServerEnvironment frees memory.
		Return value: true if succeeded, false if failed.
		(note:  not used, pending removal from engine)
	*/
	//bool addActiveObjectAsStatic(ServerActiveObject *object);

	/*
		Find out what new objects have been added to
		inside a radius around a position
	*/
	void getAddedActiveObjects(RemotePlayer *player, s16 radius,
			s16 player_radius,
			std::set<u16> &current_objects,
			std::queue<u16> &added_objects);

	/*
		Find out what new objects have been removed from
		inside a radius around a position
	*/
	void getRemovedActiveObjects(RemotePlayer* player, s16 radius,
			s16 player_radius,
			std::set<u16> &current_objects,
			std::queue<u16> &removed_objects);

	/*
		Get the next message emitted by some active object.
		Returns a message with id=0 if no messages are available.
	*/
	ActiveObjectMessage getActiveObjectMessage();

	/*
		Activate objects and dynamically modify for the dtime determined
		from timestamp and additional_dtime
	*/
	u32 activateBlock(MapBlock *block);

	/*
		{Active,Loading}BlockModifiers
		-------------------------------------------
	*/

	void addActiveBlockModifier(ActiveBlockModifier *abm);
	void addLoadingBlockModifierDef(LoadingBlockModifierDef *lbm);

	/*
		Other stuff
		-------------------------------------------
	*/

	// Script-aware node setters
	bool setNode(v3s16 p, const MapNode &n);
	bool removeNode(v3s16 p);
	bool swapNode(v3s16 p, const MapNode &n);

	// Find all active objects inside a radius around a point
	void getObjectsInsideRadius(std::vector<u16> &objects, const v3f& pos, float radius);
	void getObjectsInsideRadius(std::vector<ServerActiveObject *> &objects, const v3f& pos, float radius);
	void getUnitsInsideRadius(std::vector<UnitSAO*> &objects, const v3f& pos, float radius);

	// Clear all objects, loading and going through every MapBlock
	void clearAllObjects();

	// This makes stuff happen
	void step(f32 dtime);

	//check if there's a line of sight between two positions
	bool line_of_sight(const v3f& pos1, const v3f& pos2, float stepsize=1.0, v3s16 *p=NULL);

	u32 getGameTime() { return m_game_time; }
	void setGameTime(const u32 game_time) { m_game_time = game_time; }

	void reportMaxLagEstimate(float f) { m_max_lag_estimate = f; }
	float getMaxLagEstimate() { return m_max_lag_estimate; }

	std::set<v3s16>* getForceloadedBlocks() { return &m_active_blocks.m_forceloaded_list; }

	// Sets the static object status all the active objects in the specified block
	// This is only really needed for deleting blocks from the map
	void setStaticForActiveObjectsInBlock(v3s16 blockpos,
		bool static_exists, v3s16 static_block=v3s16(0,0,0));

	void removeNodesInArea(v3s16 minp, v3s16 maxp, std::set<content_t> &filter);
	bool findIfNodeInArea(v3s16 minp, v3s16 maxp, const std::vector<content_t>& filter);
	bool findNodeNear(const v3s16 pos, const s32 radius,
			const std::vector<content_t>& filter, v3s16 &found_pos);
	bool findNodeNear(const v3s16 pos, const s32 radius,
			const std::vector<content_t>& filter);
	const u8 getNodeLight(const v3s16 pos);

	/*
	 * Contrib functions added to core under GPLv3 by Epixel
	 */
	void nodeUpdate(const v3s16 pos, u16 recursion_limit = 0);
	void handleNodeDrops(const ContentFeatures &f, const v3f& pos, PlayerSAO* player=NULL);
	void setCurrentAutoMapgenOffset(s32 offset) { m_current_autogen_offset = offset; }
	void generateNodeIdCache();
	inline std::set<content_t> getCachedNodeIds(const EnvironmentCacheIds id)
	{
		assert(id < ENVIRONMENT_NODECACHE_ID_MAX);
		return m_nodeid_cache[id];
	}

	bool makeExplosion(const v3s16 pos, const float radius, const u16 objId = 0, u32 player_id = 0);
	void spawnEffect(const v3f& pos, u16 amount, const std::string &texture, float max_size = 1.0f);

	void spawnInventoryOnGround(InventoryList* inv_list, const v3f& pos);

	epixel::TeleportMgr* getTeleportMgr() { return m_teleport_mgr; }
	LBMManager* getLBMMgr() { return &m_lbm_mgr; }
private:

	/*
		Internal ActiveObject interface
		-------------------------------------------
	*/

	/*
		Add an active object to the environment.

		Called by addActiveObject.

		Object may be deleted by environment immediately.
		If id of object is 0, assigns a free id to it.
		Returns the id of the object.
		Returns 0 if not added and thus deleted.
	*/
	u16 addActiveObjectRaw(ServerActiveObject *object, bool set_changed, u32 dtime_s);

	/*
		Remove all objects that satisfy (m_removed && m_known_by_count==0)
	*/
	void removeRemovedObjects();

	/*
		Convert stored objects from block to active
	*/
	void activateObjects(MapBlock *block, u32 dtime_s);

	/*
		Convert objects that are not in active blocks to static.

		If m_known_by_count != 0, active object is not deleted, but static
		data is still updated.

		If force_delete is set, active object is deleted nevertheless. It
		shall only be set so in the destructor of the environment.
	*/
	void deactivateFarObjects(bool force_delete);

	/*
	 * Contrib functions added to core under GPLv3 by Epixel
	 */

	void contrib_player_globalstep(RemotePlayer *player, const float dtime);
	void contrib_lookupitemtogather(RemotePlayer* player, const v3f& playerPos,
			Inventory* inv, ServerActiveObject* obj);
	void contrib_globalstep(const float dtime);
	bool checkAttachedNode(v3s16 pos, MapNode n, const ContentFeatures &f);
	void explodeNode(const v3s16 pos);
	bool onNodeTimer(const v3s16 &p, MapNode n, const NodeTimer &t);

	/*
		Member variables
	*/

	// The map
	ServerMap *m_map;
	// Lua state
	GameScripting* m_script;
	// Game definition
	Server *m_gamedef;
	// World path
	const std::string m_path_world;
	// Active object list
	std::unordered_map<u16, ServerActiveObject*> m_active_objects;
	// Outgoing network message buffer for active objects
	std::queue<ActiveObjectMessage> m_active_object_messages;
	// Some timers
	float m_send_recommended_timer;
	float m_object_management_interval;
	// List of active blocks
	ActiveBlockList m_active_blocks;
	float m_active_blocks_management_interval;
	float m_active_block_modifier_interval;
	float m_active_blocks_nodemetadata_interval;
	int m_active_block_interval_overload_skip;
	// Time from the beginning of the game in seconds.
	// Incremented in step().
	u32 m_game_time;
	// A helper variable for incrementing the latter
	float m_game_time_fraction_counter;
	// Estimate for general maximum lag as determined by server.
	// Can raise to high values like 15s with eg. map generation mods.
	float m_max_lag_estimate;

	LBMManager m_lbm_mgr;
	ABMHandler* m_abm_handler;

	// epixel specific
	epixel::AreaMgr* m_area_mgr;
	epixel::TeleportMgr* m_teleport_mgr;
	float m_hunger_timer;
	float m_autogenerator_timer;
	float m_hud_refresh_timer = 0.1f;
	s16 m_current_autogen_offset;
	std::deque<v3s16> m_explosion_queue;
	std::deque<v3s16> m_nodeupdate_queue;
	u8 m_explosion_loop_counter = 0;

	std::set<content_t> m_nodeid_cache[ENVIRONMENT_NODECACHE_ID_MAX];

	// Particles
	float m_particle_management_interval = 0.0;
	std::map<u32, float> m_particle_spawners;
};

#ifndef SERVER

#include "clientobject.h"
#include "content_cao.h"

class ClientSimpleObject;

/*
	The client-side environment.

	This is not thread-safe.
	Must be called from main (irrlicht) thread (uses the SceneManager)
	Client uses an environment mutex.
*/

enum ClientEnvEventType
{
	CEE_NONE,
	CEE_PLAYER_DAMAGE,
};

struct ClientEnvEvent
{
	ClientEnvEventType type;
	union {
		//struct{
		//} none;
		struct{
			u8 amount;
			bool send_to_server;
		} player_damage;
		struct{
			u16 amount;
		} player_breath;
	};
};

class ClientEnvironment : public Environment
{
public:
	ClientEnvironment(ClientMap *map, scene::ISceneManager *smgr,
			ITextureSource *texturesource, IGameDef *gamedef,
			IrrlichtDevice *device);
	~ClientEnvironment();

	Map & getMap();
	ClientMap* getClientMap();

	IGameDef *getGameDef() { return m_gamedef; }

	void step(f32 dtime);

	virtual void addPlayer(Player *player);
	LocalPlayer * getLocalPlayer();

	/*
		ClientSimpleObjects
	*/

	void addSimpleObject(ClientSimpleObject *simple);

	/*
		ActiveObjects
	*/

	GenericCAO* getGenericCAO(u16 id);
	ClientActiveObject* getActiveObject(u16 id);

	/*
		Adds an active object to the environment.
		Environment handles deletion of object.
		Object may be deleted by environment immediately.
		If id of object is 0, assigns a free id to it.
		Returns the id of the object.
		Returns 0 if not added and thus deleted.
	*/
	u16 addActiveObject(ClientActiveObject *object);

	void addActiveObject(u16 id, u8 type, const std::string &init_data);
	void removeActiveObject(u16 id);

	void processActiveObjectMessage(u16 id, const std::string &data);

	/*
		Callbacks for activeobjects
	*/

	void damageLocalPlayer(u8 damage, bool handle_hp=true);

	/*
		Client likes to call these
	*/

	// Get all nearby objects
	void getActiveObjects(const v3f& origin, f32 max_d,
			std::vector<DistanceSortedActiveObject> &dest);

	// Get event from queue. CEE_NONE is returned if queue is empty.
	ClientEnvEvent getClientEvent();

	u16 attachement_parent_ids[USHRT_MAX + 1] = {0};

	std::list<std::string> getPlayerNames() const
	{ return m_player_names; }
	inline void addPlayerName(const std::string &name)
	{ m_player_names.push_back(name); }
	inline void removePlayerName(const std::string &name)
	{ m_player_names.remove(name); }
	inline void updateCameraOffset(const v3s16 camera_offset)
	{ m_camera_offset = camera_offset; }
	v3s16 getCameraOffset() const
	{ return m_camera_offset; }

private:
	ClientMap *m_map;
	scene::ISceneManager *m_smgr;
	ITextureSource *m_texturesource;
	IGameDef *m_gamedef;
	IrrlichtDevice *m_irr;
	std::unordered_map<u16, ClientActiveObject*> m_active_objects;
	std::vector<ClientSimpleObject*> m_simple_objects;
	std::queue<ClientEnvEvent> m_client_event_queue;
	float m_active_object_light_update_interval;
	std::list<std::string> m_player_names;
	v3s16 m_camera_offset;
};

#endif

#endif

