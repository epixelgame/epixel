/*
Minetest
Copyright (C) 2013 celeron55, Perttu Ahola <celeron55@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef HTTPFETCH_HEADER
#define HTTPFETCH_HEADER

#include <vector>
#include <curl/curl.h>
#include "contrib/stats.h"
#include "util/container.h"
#include "util/string.h"
#include "config.h"
#include "threads/thread.h"
#include "threads/event.h"

// Can be used in place of "caller" in asynchronous transfers to discard result
// (used as default value of "caller")
#define HTTPFETCH_DISCARD 0
#define HTTPFETCH_SYNC 1

struct HTTPFetchRequest
{
	std::string url;

	// Identifies the caller (for asynchronous requests)
	// Ignored by httpfetch_sync
	unsigned long caller;

	// Some number that identifies the request
	// (when the same caller issues multiple httpfetch_async calls)
	unsigned long request_id;

	// Timeout for the whole transfer, in milliseconds
	long timeout;

	// Timeout for the connection phase, in milliseconds
	long connect_timeout;

	// Indicates if this is multipart/form-data or
	// application/x-www-form-urlencoded.  POST-only.
	bool multipart;

	// POST fields.  Fields are escaped properly.
	// If this is empty a GET request is done instead.
	StringMap post_fields;

	// Raw POST data, overrides post_fields.
	std::string post_data;

	// If not empty, should contain entries such as "Accept: text/html"
	std::vector<std::string> extra_headers;

	//useragent to use
	std::string useragent;

	HTTPFetchRequest();
};

struct HTTPFetchResult
{
	bool succeeded;
	bool timeout;
	long response_code;
	std::string data;
	// The caller and request_id from the corresponding HTTPFetchRequest.
	unsigned long caller;
	unsigned long request_id;

	HTTPFetchResult()
	{
		succeeded = false;
		timeout = false;
		response_code = 0;
		data = "";
		caller = HTTPFETCH_DISCARD;
		request_id = 0;
	}

	HTTPFetchResult(const HTTPFetchRequest &fetch_request)
	{
		succeeded = false;
		timeout = false;
		response_code = 0;
		data = "";
		caller = fetch_request.caller;
		request_id = fetch_request.request_id;
	}

};


class CurlHandlePool
{
	std::list<CURL*> handles;

public:
	CurlHandlePool() {}
	~CurlHandlePool()
	{
		for (auto &h: handles) {
			curl_easy_cleanup(h);
		}
	}
	CURL * alloc();
	void free(CURL *handle)
	{
		if (handle)
			handles.push_back(handle);
	}
};

class HTTPFetchOngoing
{
public:
	HTTPFetchOngoing(HTTPFetchRequest request, CurlHandlePool *pool);
	~HTTPFetchOngoing();

	CURLcode start(CURLM *multi);
	const HTTPFetchResult * complete(CURLcode res);

	const HTTPFetchRequest &getRequest()    const { return request; }
	const CURL             *getEasyHandle() const { return curl; }

private:
	CurlHandlePool *pool;
	CURL *curl;
	CURLM *multi;
	HTTPFetchRequest request;
	HTTPFetchResult result;
	std::ostringstream oss;
	struct curl_slist *http_header;
	curl_httppost *post;
};

class CurlFetchThread : public Thread
{
protected:
	enum RequestType {
		RT_FETCH,
		RT_CLEAR,
		RT_WAKEUP,
	};

	struct Request {
		RequestType type;
		HTTPFetchRequest fetch_request;
		Event *event;
	};

	CURLM *m_multi;
	MutexedQueue<Request> m_requests;
	MutexedQueue<epixel::PlayerStat*> m_requests_stats;
	size_t m_parallel_limit;

	// Variables exclusively used within thread
	std::vector<HTTPFetchOngoing*> m_all_ongoing;
	std::list<HTTPFetchRequest> m_queued_fetches;

	Semaphore m_sem;

public:
	CurlFetchThread(int parallel_limit);
	void requestFetch(const HTTPFetchRequest &fetch_request);
	void requestClear(unsigned long caller, Event *event);
	void requestWakeUp();

	inline void queueStat(epixel::PlayerStat *stat)
	{
		m_requests_stats.push_back(stat);
		m_sem.post();
	}

protected:
	// Handle a request from some other thread
	// E.g. new fetch; clear fetches for one caller; wake up
	void processRequest(const Request &req);

	// Start new ongoing fetches if m_parallel_limit allows
	void processQueued(CurlHandlePool *pool);

	// Process CURLMsg (indicates completion of a fetch)
	void processCurlMessage(CURLMsg *msg);

	// Wait until some IO happens, or timeout elapses
	void waitForIO(long timeout);
	void * run();
};

extern CurlFetchThread *g_httpfetch_thread;

// Initializes the httpfetch module
void httpfetch_init();

// Stops the httpfetch thread and cleans up resources
void httpfetch_cleanup();

// Starts an asynchronous HTTP fetch request
void httpfetch_async(const HTTPFetchRequest &fetch_request);

// Starts an asynchronous HTTP fetch request for stats
void httppost_async_stat(epixel::PlayerStat* stat);

// If any fetch for the given caller ID is complete, removes it from the
// result queue, sets the fetch result and returns true. Otherwise returns false.
bool httpfetch_async_get(unsigned long caller, HTTPFetchResult &fetch_result);

// Allocates a caller ID for httpfetch_async
// Not required if you want to set caller = HTTPFETCH_DISCARD
unsigned long httpfetch_caller_alloc();

// Allocates a non-predictable caller ID for httpfetch_async
unsigned long httpfetch_caller_alloc_secure();

// Frees a caller ID allocated with httpfetch_caller_alloc
// Note: This can be expensive, because the httpfetch thread is told
// to stop any ongoing fetches for the given caller.
void httpfetch_caller_free(unsigned long caller);

// Performs a synchronous HTTP request. This blocks and therefore should
// only be used from background threads.
void httpfetch_sync(const HTTPFetchRequest &fetch_request,
		HTTPFetchResult &fetch_result);


#endif // !HTTPFETCH_HEADER
