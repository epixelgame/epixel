/*
Minetest
Copyright (C) 2010-2013 celeron55, Perttu Ahola <celeron55@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifdef _MSC_VER
	#pragma comment(lib, "Shell32.lib")
	#pragma comment(lib,"ws2_32.lib")
	#pragma comment(lib,"wldap32.lib")
#endif

#include "irrlicht.h" // createDevice
#include <log4cpp/Category.hh>
#include <log4cpp/PropertyConfigurator.hh>
#include <log4cpp/OstreamAppender.hh>
#include <log4cpp/PatternLayout.hh>
#include <enet/enet.h>
#include "contrib/util/socket.h"
#include "contrib/util/optparse.h"
#ifndef NDEBUG
#include "unittest/test.h"
#endif
#include "server.h"
#include "version.h"
#include "game.h"
#include "defaultsettings.h"
#include "gettext.h"
#include "httpfetch.h"
#include "guiEngine.h"
#include "mapsector.h"
#include "gameparams.h"
#include "database.h"
#include "filesys.h"

#ifndef SERVER
#include "client/clientlauncher.h"
#else
#include "contrib/util/tcpsocket.h"
#endif

#ifdef HAVE_TOUCHSCREENGUI
	#include "touchscreengui.h"
#endif

#define DEFAULT_SERVER_PORT 30000

/**********************************************************************
 * Private functions
 **********************************************************************/

static void cleanup_before_stop();

static void print_help();
static void print_version();
static void print_worldspecs(const std::vector<WorldSpec> &worldspecs);

static void list_game_ids();
static void list_worlds();
static void setup_log_params(const Settings &cmd_args);
static bool create_userdata_path();
static bool init_common(const Settings &cmd_args, int argc, char *argv[]);
static void startup_message();
static bool read_config_file(const Settings &cmd_args);
static void init_debug_streams(const Settings &cmd_args);

static bool game_configure(GameParams *game_params, const Settings &cmd_args);
static void game_configure_port(GameParams *game_params, const Settings &cmd_args);

static bool game_configure_world(GameParams *game_params, const Settings &cmd_args);
static bool get_world_from_cmdline(GameParams *game_params, const Settings &cmd_args);
static bool get_world_from_config(GameParams *game_params, const Settings &cmd_args);
static bool auto_select_world(GameParams *game_params);
static std::string get_clean_world_path(const std::string &path);

static bool game_configure_subgame(GameParams *game_params, const Settings &cmd_args);
static bool get_game_from_cmdline(GameParams *game_params, const Settings &cmd_args);
static bool determine_subgame(GameParams *game_params);

static bool run_dedicated_server(const GameParams &game_params, const Settings &cmd_args);

/**********************************************************************/

/*
	gettime.h implementation
*/

#ifdef SERVER

u32 getTimeMs()
{
	/* Use imprecise system calls directly (from porting.h) */
	return porting::getTime(PRECISION_MILLI);
}

u32 getTime(TimePrecision prec)
{
	return porting::getTime(prec);
}

#endif

Settings *g_settings = nullptr;
log4cpp::Category& logger = log4cpp::Category::getRoot();
log4cpp::Category& logger_chat = log4cpp::Category::getInstance(std::string("chat"));
log4cpp::Category& logger_is_chat = log4cpp::Category::getInstance(std::string("interserver_chat"));
log4cpp::Category& logger_irrlicht = log4cpp::Category::getInstance(std::string("irrlicht"));
CurlFetchThread *g_httpfetch_thread = nullptr;

static const struct optparse_long _allowed_options[] = {
	/* long-option, short-option, has-arg flag, description */
	{ "help", 'h', OPTPARSE_NONE, _("Show allowed options") },
	{ "config", 'c', OPTPARSE_REQUIRED, _("Show version information") },
	{ "gameid", 'g', OPTPARSE_REQUIRED, _("Set gameid (\"--gameid list\" prints available ones)") },
	{ "info", 'i', OPTPARSE_NONE, _("Print more information to console") },
	{ "logfile", 'l', OPTPARSE_REQUIRED, _("Set logfile path ('' = no logging)") },
	{ "log4cpp-props", 'L', OPTPARSE_REQUIRED, _("Set log4cpp properties file. If not, search log4cpp.properties in current dir,"
		" /usr/local/etc/epixel and /etc/epixel") },
	{ "migrate", 'm', OPTPARSE_NONE, _("Migrate from current map backend to another (Only works when using minetestserver or with --server)") },
	{ "port", 'p', OPTPARSE_REQUIRED, _("Set network port (UDP)") },
	{ "quiet", 'q', OPTPARSE_NONE, _("Print to console errors only") },
	{ "run-unittests", 'u', OPTPARSE_NONE, _("Run the unit tests and exit") },
	{ "trace", 't', OPTPARSE_NONE, _("Print enormous amounts of information to log and console") },
	{ "verbose", 'v', OPTPARSE_NONE, _("Print even more information to console") },
	{ "version", 'V', OPTPARSE_NONE, _("Show version information") },
	{ "world", 'w', OPTPARSE_REQUIRED, _("Set world path (implies local game) ('list' lists all)") },
	{ "worldname", 'W', OPTPARSE_REQUIRED, _("Set world by name (implies local game)") },
#ifndef SERVER
	{ "address", 'a', OPTPARSE_REQUIRED, _("Address to connect to. ('' = local game)") },
	{ "go", 'G', OPTPARSE_NONE, _("Disable main menu") },
	{ "password", 'P', OPTPARSE_REQUIRED, _("Set password") },
	{ "name", 'N', OPTPARSE_REQUIRED, _("Set player name") },
	{ "server", 's', OPTPARSE_NONE, _("Run dedicated server") },
	{ "speedtests", 'T', OPTPARSE_NONE, _("Run speed tests") },
	{ "videomodes", 'x', OPTPARSE_NONE, _("Show available video modes") },
#endif
	{ 0 },
};

int main(int argc, char *argv[])
{
	int retval;

	enet_initialize();

	g_settings = new Settings();

	debug_set_exception_handler();

	atexit(cleanup_before_stop);

	Settings cmd_args;
	bool cmd_args_ok = cmd_args.parseCommandLine(argc, argv, _allowed_options);
	if (!cmd_args_ok
			|| cmd_args.getFlag("help")
			|| cmd_args.exists("nonopt1")) {
		print_help();
		return cmd_args_ok ? 0 : 1;
	}

	if (cmd_args.getFlag("version")) {
		print_version();
		return 0;
	}

	std::string log4cpp_properties = "log4cpp.properties";
	try {
		log4cpp_properties = cmd_args.get("log4cpp_props");
	}
	catch (SettingNotFoundException &) {
		std::vector<std::string> log4cpp_paths = {
			"log4cpp.properties",
			"/usr/local/etc/epixel/log4cpp.properties",
			"/etc/epixel/log4cpp.properties"
		};
		for (const auto &p: log4cpp_paths) {
			if (fs::PathExists(p)) {
				log4cpp_properties = p;
				std::cout << "Selected " << p << " logging properties file." << std::endl;
				break;
			}
		}
	}

	try {
		log4cpp::PropertyConfigurator::configure(log4cpp_properties);
	}
	catch (log4cpp::ConfigureFailure &) {
		std::cout << "/!\\ Log4Cpp properties not found, logging enabled with default mode /!\\" << std::endl
				<< "To fix this warning please create log4cpp.properties and specify it with -L option" << std::endl
				<< "Enabling default logging in console mode with NOTICE logging level." << std::endl;

		logger.setPriority(log4cpp::Priority::NOTICE);
		logger_chat.setPriority(log4cpp::Priority::NOTICE);
		logger_is_chat.setPriority(log4cpp::Priority::NOTICE);
		logger_irrlicht.setPriority(log4cpp::Priority::NOTICE);
		log4cpp::Appender *console_appender = new log4cpp::OstreamAppender("console", &std::cout);
		log4cpp::PatternLayout* pl = new log4cpp::PatternLayout();
		pl->setConversionPattern("%d [%p] %m%n");
		console_appender->setLayout(pl);
		logger.addAppender(console_appender);
	}

	setup_log_params(cmd_args);

	porting::signal_handler_init();

#ifdef __ANDROID__
	porting::initAndroid();
	porting::initializePathsAndroid();
#else
	porting::initializePaths();
#endif

	if (!create_userdata_path()) {
		logger.fatal("Cannot create user data directory");
		return 1;
	}

	// Debug handler
	BEGIN_DEBUG_EXCEPTION_HANDLER

	// List gameids if requested
	if (cmd_args.exists("gameid") && cmd_args.get("gameid") == "list") {
		list_game_ids();
		return 0;
	}

	// List worlds if requested
	if (cmd_args.exists("world") && cmd_args.get("world") == "list") {
		list_worlds();
		delete g_settings;
		return 0;
	}

	GameParams game_params;
	if (!init_common(cmd_args, argc, argv)) {
		return 1;
	}

#if !defined(__ANDROID__) && !defined(NDEBUG)
	// Run unit tests
	if (cmd_args.getFlag("run-unittests")) {
		run_tests();
		return 0;
	}
#endif

#ifdef SERVER
	game_params.is_dedicated_server = true;
#else
	game_params.is_dedicated_server = cmd_args.getFlag("server");
#endif

	if (!game_configure(&game_params, cmd_args)) {
		return 1;
	}

	sanity_check(game_params.world_path != "");

	logger.info("Using commanded world path [%s]", game_params.world_path.c_str());

	//Run dedicated server if asked to or no other option
	g_settings->set(BOOLSETTING_SERVER_DEDICATED,
			game_params.is_dedicated_server);

	if (game_params.is_dedicated_server) {
		int ret = run_dedicated_server(game_params, cmd_args) ? 0 : 1;
		return ret;
	}

#ifndef SERVER
	ClientLauncher launcher;
	retval = launcher.run(game_params, cmd_args) ? 0 : 1;
#else
	retval = 0;
#endif

	// Update configuration file
	if (g_settings_path != "")
		g_settings->updateConfigFile(g_settings_path.c_str(), true);

	END_DEBUG_EXCEPTION_HANDLER()

	return retval;
}

/*****************************************************************************
 * Stopping
 *****************************************************************************/

static void cleanup_before_stop()
{
	// Stop httpfetch thread (if started)
	if (g_httpfetch_thread)
		httpfetch_cleanup();

	delete g_settings;

	enet_deinitialize();

	epixel::Socket::deinit_atexit();
}

/*****************************************************************************
 * Startup / Init
 *****************************************************************************/

static void print_help()
{
	std::cout << _("Allowed options:") << std::endl;
	for (u16 i = 0; _allowed_options[i].longname != NULL; i++) {
		std::cout << "  --" << _allowed_options[i].longname;
		if (_allowed_options[i].argtype != OPTPARSE_NONE)
			std::cout << _(" <value>");

		if (_allowed_options[i].help != NULL) {
			std::cout << ", " << _allowed_options[i].help;
		}

		std::cout << std::endl;
	}

}

static void print_version()
{
	logger.debugStream() << PROJECT_NAME_C " " << g_version_hash
	          << " (" << porting::getPlatformName() << ")" << "\n";
#ifndef SERVER
	logger.debug("Using Irrlicht %s", IRRLICHT_SDK_VERSION);
#endif
	logger.debug("Build info: %s", g_build_info);
}

static void list_game_ids()
{
	std::set<std::string> gameids = getAvailableGameIds();
	for (const auto &gameid: gameids)
		logger.notice(gameid);
}

static void list_worlds()
{
	logger.info("%s", _("Available worlds:"));
	std::vector<WorldSpec> worldspecs = getAvailableWorlds();
	print_worldspecs(worldspecs);
}

static void print_worldspecs(const std::vector<WorldSpec> &worldspecs)
{
	for (size_t i = 0; i < worldspecs.size(); i++) {
		std::string name = worldspecs[i].name;
		std::string path = worldspecs[i].path;
		if (name.find(" ") != std::string::npos)
			name = std::string("'") + name + "'";
		path = std::string("'") + path + "'";
		name = padStringRight(name, 14);
		logger.infoStream() << "  " << name << " " << path << "\n";
	}
}

static void setup_log_params(const Settings &cmd_args)
{
	// Quiet mode, print errors only
	if (cmd_args.getFlag("quiet")) {
		logger.setRootPriority(log4cpp::Priority::ERROR);
	}

	// If trace is enabled, enable logging of certain things
	if (cmd_args.getFlag("trace")) {
		std::cout << _("Enabling trace level debug output") << std::endl;
		logger.setRootPriority(log4cpp::Priority::DEBUG);
	}
	else if (cmd_args.getFlag("info") || cmd_args.getFlag("speedtests")) {
		logger.setRootPriority(log4cpp::Priority::INFO);
	}
}

static bool create_userdata_path()
{
	bool st = true;
	if (!fs::PathExists(porting::path_user)) {
		st = fs::CreateDir(porting::path_user);
	}
#ifdef __ANDROID__
	porting::copyAssets();
#endif

	return st;
}

static bool init_common(const Settings &cmd_args, int argc, char *argv[])
{
	startup_message();
	set_default_settings(g_settings);

	if (!read_config_file(cmd_args))
		return false;

	init_debug_streams(cmd_args);

	// Initialize random seed
	srand(time(0));
	mysrand(time(0));

	// Initialize HTTP fetcher
	httpfetch_init();

	init_gettext(porting::path_locale.c_str(),
		g_settings->get("language"), argc, argv);

	epixel::Socket::startup_init();

	return true;
}

static void startup_message()
{
	logger.info("%s %s SER_FMT_VER_HIGHEST_READ=%d, %s",
		PROJECT_NAME, _("with"), (int)SER_FMT_VER_HIGHEST_READ, g_build_info);
}

static bool read_config_file(const Settings &cmd_args)
{
	// Path of configuration file in use
	sanity_check(g_settings_path == "");	// Sanity check

	if (cmd_args.exists("config")) {
		bool r = g_settings->readConfigFile(cmd_args.get("config").c_str());
		if (!r) {
			logger.crit("Could not read configuration from \"%s\"",
						cmd_args.get("config").c_str());
			return false;
		}
		g_settings_path = cmd_args.get("config");
	} else {
		std::string filename = porting::path_user + DIR_DELIM + "epixel.conf";
		g_settings->readConfigFile(filename.c_str());
		g_settings_path = filename;
	}

	return true;
}

static void init_debug_streams(const Settings &cmd_args)
{
	debugstreams_init(false);
}

static bool game_configure(GameParams *game_params, const Settings &cmd_args)
{
	game_configure_port(game_params, cmd_args);

	if (!game_configure_world(game_params, cmd_args)) {
		logger.fatal("No world path specified or found.");
		return false;
	}

	game_configure_subgame(game_params, cmd_args);

	return true;
}

static void game_configure_port(GameParams *game_params, const Settings &cmd_args)
{
	if (cmd_args.exists("port"))
		cmd_args.getU16NoEx("port", game_params->socket_port);
	else
		game_params->socket_port = g_settings->get(U16SETTING_PORT);

	if (game_params->socket_port == 0)
		game_params->socket_port = DEFAULT_SERVER_PORT;
}

static bool game_configure_world(GameParams *game_params, const Settings &cmd_args)
{
	if (get_world_from_cmdline(game_params, cmd_args))
		return true;
	if (get_world_from_config(game_params, cmd_args))
		return true;

	return auto_select_world(game_params);
}

static bool get_world_from_cmdline(GameParams *game_params, const Settings &cmd_args)
{
	std::string commanded_world = "";

	// World name
	std::string commanded_worldname = "";
	if (cmd_args.exists("worldname"))
		commanded_worldname = cmd_args.get("worldname");

	// If a world name was specified, convert it to a path
	if (commanded_worldname != "") {
		// Get information about available worlds
		std::vector<WorldSpec> worldspecs = getAvailableWorlds();
		bool found = false;
		for (u32 i = 0; i < worldspecs.size(); i++) {
			std::string name = worldspecs[i].name;
			if (name == commanded_worldname) {
				logger.notice("%s", _("Using world specified by --worldname on the "
					"command line"));
				commanded_world = worldspecs[i].path;
				found = true;
				break;
			}
		}
		if (!found) {
			logger.error("%s '%s' %s", _("World"), commanded_worldname.c_str(),
					_("not available. Available worlds:"));
			print_worldspecs(worldspecs);
			return false;
		}

		game_params->world_path = get_clean_world_path(commanded_world);
		return commanded_world != "";
	}

	if (cmd_args.exists("world"))
		commanded_world = cmd_args.get("world");
	else if (cmd_args.exists("map-dir"))
		commanded_world = cmd_args.get("map-dir");
	else if (cmd_args.exists("nonopt0")) // First nameless argument
		commanded_world = cmd_args.get("nonopt0");

	game_params->world_path = get_clean_world_path(commanded_world);
	return commanded_world != "";
}

static bool get_world_from_config(GameParams *game_params, const Settings &cmd_args)
{
	// World directory
	std::string commanded_world = "";

	if (g_settings->exists("map-dir"))
		commanded_world = g_settings->get("map-dir");

	game_params->world_path = get_clean_world_path(commanded_world);

	return commanded_world != "";
}

static bool auto_select_world(GameParams *game_params)
{
	// No world was specified; try to select it automatically
	// Get information about available worlds

	logger.debug(_("Determining world path"));

	std::vector<WorldSpec> worldspecs = getAvailableWorlds();
	std::string world_path;

	// If there is only a single world, use it
	if (worldspecs.size() == 1) {
		world_path = worldspecs[0].path;
		logger.notice("%s [%s]", _("Automatically selecting world at"),
				world_path.c_str());
	// If there are multiple worlds, list them
	} else if (worldspecs.size() > 1 && game_params->is_dedicated_server) {
		logger.error(_("Multiple worlds are available."));
		logger.error(_("Please select one using --worldname <name>"
				" or --world <path>"));
		print_worldspecs(worldspecs);
		return false;
	// If there are no worlds, automatically create a new one
	} else {
		// This is the ultimate default world path
		world_path = porting::path_user + DIR_DELIM + "worlds" +
				DIR_DELIM + "world";
		logger.info("Creating default world at [%s]", world_path.c_str());
	}

	assert(world_path != "");	// Post-condition
	game_params->world_path = world_path;
	return true;
}

static std::string get_clean_world_path(const std::string &path)
{
	const std::string worldmt = "world.mt";
	std::string clean_path;

	if (path.size() > worldmt.size()
			&& path.substr(path.size() - worldmt.size()) == worldmt) {
		logger.notice(_("Supplied world.mt file - stripping it off."));
		clean_path = path.substr(0, path.size() - worldmt.size());
	} else {
		clean_path = path;
	}
	return path;
}


static bool game_configure_subgame(GameParams *game_params, const Settings &cmd_args)
{
	bool success;

	success = get_game_from_cmdline(game_params, cmd_args);
	if (!success)
		success = determine_subgame(game_params);

	return success;
}

static bool get_game_from_cmdline(GameParams *game_params, const Settings &cmd_args)
{
	SubgameSpec commanded_gamespec;

	if (cmd_args.exists("gameid")) {
		std::string gameid = cmd_args.get("gameid");
		commanded_gamespec = findSubgame(gameid);
		if (!commanded_gamespec.isValid()) {
			logger.fatal("Game \"%s\" not found", gameid.c_str());
			return false;
		}
		logger.notice(_("Using game specified by --gameid on the command line"));
		game_params->game_spec = commanded_gamespec;
		return true;
	}

	return false;
}

static bool determine_subgame(GameParams *game_params)
{
	SubgameSpec gamespec;

	assert(game_params->world_path != "");	// Pre-condition

	logger.debug(_("Determining gameid/gamespec"));
	// If world doesn't exist
	if (game_params->world_path != ""
			&& !getWorldExists(game_params->world_path)) {
		// Try to take gamespec from command line
		if (game_params->game_spec.isValid()) {
			gamespec = game_params->game_spec;
			logger.info("Using commanded gameid [%s]", gamespec.id.c_str());
		} else { // Otherwise we will be using "minetest"
			gamespec = findSubgame(g_settings->get("default_game"));
			logger.info("Using default gameid [%s]", gamespec.id.c_str());
			if (!gamespec.isValid()) {
				logger.error("Subgame specified in default_game [%s] is invalid.", g_settings->get("default_game").c_str());
				return false;
			}
		}
	} else { // World exists
		std::string world_gameid = getWorldGameId(game_params->world_path, false);
		// If commanded to use a gameid, do so
		if (game_params->game_spec.isValid()) {
			gamespec = game_params->game_spec;
			if (game_params->game_spec.id != world_gameid) {
				logger.error("WARNING: Using commanded gameid [%s] instead of world gameid [%s]",
						gamespec.id.c_str(), world_gameid.c_str());
			}
		} else {
			// If world contains an embedded game, use it;
			// Otherwise find world from local system.
			gamespec = findWorldSubgame(game_params->world_path);
			logger.info("Using world gameid [%s]", gamespec.id.c_str());
		}
	}

	if (!gamespec.isValid()) {
		logger.error("Subgame [%s] could not be found.", gamespec.id.c_str());
		return false;
	}

	game_params->game_spec = gamespec;
	return true;
}


/*****************************************************************************
 * Dedicated server
 *****************************************************************************/
static bool run_dedicated_server(const GameParams &game_params, const Settings &cmd_args)
{
	logger.debug("%s [%s]",_("Using world path"), game_params.world_path.c_str());
	logger.debug("%s [%s]", _("Using gameid"), game_params.game_spec.id.c_str());

	// Bind address
	std::string bind_str = g_settings->get("bind_address");
	Address bind_addr(0, 0, 0, 0, game_params.socket_port);

	try {
		bind_addr.Resolve(bind_str);
	} catch (ResolveError &e) {
		logger.info("Resolving bind address '%s' failed: %s  -- Listening on all addresses.",bind_str.c_str(), e.what());
	}
	if (bind_addr.isIPv6() && !g_settings->get(BOOLSETTING_ENABLE_IPV6)) {
		logger.error("Unable to listen on %s because IPv6 is disabled", bind_addr.serializeString().c_str());
		return false;
	}

	try {
		// Create server
		Server server(game_params.world_path, game_params.game_spec,
#ifndef SERVER
				false,
#endif
				bind_addr.isIPv6());

		server.start(bind_addr);

		// Run server
		bool &kill = *porting::signal_handler_killstatus();
		dedicated_server_loop(server, kill);

		server.stop();
	}
	catch (DatabaseException &e) {
		logger.error("%s", e.what());
		return false;
	}

	return true;
}
