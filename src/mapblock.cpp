/*
Minetest
Copyright (C) 2013 celeron55, Perttu Ahola <celeron55@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mapblock.h"

#include <sstream>
#include "map.h"
#include "light.h"
#include "nodedef.h"
#include "nodemetadata.h"
#include "gamedef.h"
#include "log.h"
#include "nameidmapping.h"
#include "content_mapnode.h" // For legacy name-id mapping
#include "content_nodemeta.h" // For legacy deserialization
#include "serialization.h"
#ifndef SERVER
#include "mapblock_mesh.h"
#endif
#include "util/string.h"
#include "util/serialize.h"

#define PP(x) "("<<(x).X<<","<<(x).Y<<","<<(x).Z<<")"

static const char *modified_reason_strings[] = {
	"initial",
	"reallocate",
	"setIsUnderground",
	"setLightingExpired",
	"setGenerated",
	"setNode",
	"setNodeNoCheck",
	"setTimestamp",
	"NodeMetaRef::reportMetadataChange",
	"clearAllObjects",
	"Timestamp expired (step)",
	"addActiveObjectRaw",
	"removeRemovedObjects/remove",
	"removeRemovedObjects/deactivate",
	"Stored list cleared in activateObjects due to overflow",
	"deactivateFarObjects: Static data moved in",
	"deactivateFarObjects: Static data moved out",
	"deactivateFarObjects: Static data changed considerably",
	"finishBlockMake: expireDayNightDiff",
	"unknown",
};


/*
	MapBlock
*/

MapBlock::MapBlock(Map *parent, const v3s16 &pos, IGameDef *gamedef, bool dummy):
		m_parent(parent),
		m_pos(pos),
		m_pos_relative(pos * MAP_BLOCKSIZE),
		m_gamedef(gamedef),
		m_modified(MOD_STATE_WRITE_NEEDED),
		m_modified_reason(MOD_REASON_INITIAL),
		is_underground(false),
		m_lighting_expired(true),
		m_day_night_differs(false),
		m_day_night_differs_expired(true),
		m_generated(false),
		m_timestamp(BLOCK_TIMESTAMP_UNDEFINED),
		m_disk_timestamp(BLOCK_TIMESTAMP_UNDEFINED),
		m_usage_timer(0),
		m_refcount(0)
{
	data = NULL;
	if(dummy == false)
		reallocate();

#ifndef SERVER
	mesh = NULL;
#endif
}

MapBlock::~MapBlock()
{
#ifndef SERVER
	{
		//MutexLocker lock(&mesh_mutex);

		if(mesh)
		{
			delete mesh;
			mesh = NULL;
		}
	}
#endif

	if(data)
		delete[] data;
}

MapNode MapBlock::getNodeParent(const v3s16& p, bool *is_valid_position)
{
	if (isValidPosition(p) == false)
		return m_parent->getNode(getPosRelative() + p, is_valid_position);

	if (data == NULL) {
		if (is_valid_position)
			*is_valid_position = false;
		return MapNode(CONTENT_IGNORE);
	}
	if (is_valid_position)
		*is_valid_position = true;
	return data[p.Z * zstride + p.Y * ystride + p.X];
}

std::string MapBlock::getModifiedReasonString()
{
	std::string reason;

	const u32 ubound = MYMIN(sizeof(m_modified_reason) * CHAR_BIT,
		ARRLEN(modified_reason_strings));

	for (u32 i = 0; i != ubound; i++) {
		if ((m_modified_reason & (1 << i)) == 0)
			continue;

		reason += modified_reason_strings[i];
		reason += ", ";
	}

	if (reason.length() > 2)
		reason.resize(reason.length() - 2);

	return reason;
}

/*
	Propagates sunlight down through the block.
	Doesn't modify nodes that are not affected by sunlight.

	Returns false if sunlight at bottom block is invalid.
	Returns true if sunlight at bottom block is valid.
	Returns true if bottom block doesn't exist.

	If there is a block above, continues from it.
	If there is no block above, assumes there is sunlight, unless
	is_underground is set or highest node is water.

	All sunlighted nodes are added to light_sources.

	if remove_light==true, sets non-sunlighted nodes black.

	if black_air_left!=NULL, it is set to true if non-sunlighted
	air is left in block.
*/
bool MapBlock::propagateSunlight(std::vector<v3s16> & light_sources,
		bool remove_light, bool *black_air_left)
{
	INodeDefManager *nodemgr = m_gamedef->ndef();

	// Whether the sunlight at the top of the bottom block is valid
	bool block_below_is_valid = true;

	v3s16 pos_relative = getPosRelative();

	for(s16 x=0; x<MAP_BLOCKSIZE; x++)
	{
		for(s16 z=0; z<MAP_BLOCKSIZE; z++)
		{
			bool no_sunlight = false;
			//bool no_top_block = false;

			// Check if node above block has sunlight

			bool is_valid_position;
			MapNode n = getNodeParent(v3s16(x, MAP_BLOCKSIZE, z),
				&is_valid_position);
			if (is_valid_position)
			{
				if(n.getContent() == CONTENT_IGNORE)
				{
					// Trust heuristics
					no_sunlight = is_underground;
				}
				else if(n.getLight(LIGHTBANK_DAY, m_gamedef->ndef()) != LIGHT_SUN)
				{
					no_sunlight = true;
				}
			}
			else
			{
				//no_top_block = true;

				// NOTE: This makes over-ground roofed places sunlighted
				// Assume sunlight, unless is_underground==true
				if(is_underground)
				{
					no_sunlight = true;
				}
				else
				{
					MapNode n = getNodeNoEx(v3s16(x, MAP_BLOCKSIZE-1, z));
					if(m_gamedef->ndef()->get(n).sunlight_propagates == false)
					{
						no_sunlight = true;
					}
				}
				// NOTE: As of now, this just would make everything dark.
				// No sunlight here
				//no_sunlight = true;
			}

			s16 y = MAP_BLOCKSIZE-1;

			// This makes difference to diminishing in water.
			bool stopped_to_solid_object = false;

			u8 current_light = no_sunlight ? 0 : LIGHT_SUN;

			for(; y >= 0; y--)
			{
				v3s16 pos(x, y, z);
				MapNode &n = getNodeRef(pos);

				if(current_light == 0)
				{
					// Do nothing
				}
				else if(current_light == LIGHT_SUN && nodemgr->get(n).sunlight_propagates)
				{
					// Do nothing: Sunlight is continued
				}
				else if(nodemgr->get(n).light_propagates == false)
				{
					// A solid object is on the way.
					stopped_to_solid_object = true;

					// Light stops.
					current_light = 0;
				}
				else
				{
					// Diminish light
					current_light = diminish_light(current_light);
				}

				u8 old_light = n.getLight(LIGHTBANK_DAY, nodemgr);

				if(current_light > old_light || remove_light)
				{
					n.setLight(LIGHTBANK_DAY, current_light, nodemgr);
				}

				if(diminish_light(current_light) != 0)
				{
					light_sources.push_back(pos_relative + pos);
				}

				if(current_light == 0 && stopped_to_solid_object)
				{
					if(black_air_left)
					{
						*black_air_left = true;
					}
				}
			}

			// Whether or not the block below should see LIGHT_SUN
			bool sunlight_should_go_down = (current_light == LIGHT_SUN);

			/*
				If the block below hasn't already been marked invalid:

				Check if the node below the block has proper sunlight at top.
				If not, the block below is invalid.

				Ignore non-transparent nodes as they always have no light
			*/

			if(block_below_is_valid)
			{
				MapNode n = getNodeParent(v3s16(x, -1, z), &is_valid_position);
				if (is_valid_position) {
					if(nodemgr->get(n).light_propagates)
					{
						if(n.getLight(LIGHTBANK_DAY, nodemgr) == LIGHT_SUN
								&& sunlight_should_go_down == false)
							block_below_is_valid = false;
						else if(n.getLight(LIGHTBANK_DAY, nodemgr) != LIGHT_SUN
								&& sunlight_should_go_down == true)
							block_below_is_valid = false;
					}
				}
			}
		}
	}

	return block_below_is_valid;
}


void MapBlock::copyTo(VoxelManipulator &dst)
{
	v3s16 data_size(MAP_BLOCKSIZE, MAP_BLOCKSIZE, MAP_BLOCKSIZE);
	VoxelArea data_area(v3s16(0,0,0), data_size - v3s16(1,1,1));

	// Copy from data to VoxelManipulator
	dst.copyFrom(data, data_area, v3s16(0,0,0),
			getPosRelative(), data_size);
}

void MapBlock::copyFrom(VoxelManipulator &dst)
{
	v3s16 data_size(MAP_BLOCKSIZE, MAP_BLOCKSIZE, MAP_BLOCKSIZE);
	VoxelArea data_area(v3s16(0,0,0), data_size - v3s16(1,1,1));

	// Copy from VoxelManipulator to data
	dst.copyTo(data, data_area, v3s16(0,0,0),
			getPosRelative(), data_size);
}

void MapBlock::actuallyUpdateDayNightDiff()
{
	INodeDefManager *nodemgr = m_gamedef->ndef();

	// Running this function un-expires m_day_night_differs
	m_day_night_differs_expired = false;

	if (data == NULL) {
		m_day_night_differs = false;
		return;
	}

	bool differs;

	/*
		Check if any lighting value differs
	*/
	for (u32 i = 0; i < nodecount; i++) {
		MapNode &n = data[i];

		differs = !n.isLightDayNightEq(nodemgr);
		if (differs)
			break;
	}

	/*
		If some lighting values differ, check if the whole thing is
		just air. If it is just air, differs = false
	*/
	if (differs) {
		bool only_air = true;
		for (u32 i = 0; i < nodecount; i++) {
			MapNode &n = data[i];
			if (n.getContent() != CONTENT_AIR) {
				only_air = false;
				break;
			}
		}
		if (only_air)
			differs = false;
	}

	// Set member variable
	m_day_night_differs = differs;
}

void MapBlock::expireDayNightDiff()
{
	//INodeDefManager *nodemgr = m_gamedef->ndef();

	if(data == NULL){
		m_day_night_differs = false;
		m_day_night_differs_expired = false;
		return;
	}

	m_day_night_differs_expired = true;
}

/*
	Serialization
*/
// List relevant id-name pairs for ids in the block using nodedef
// Renumbers the content IDs (starting at 0 and incrementing
// use static memory requires about 65535 * sizeof(int) ram in order to be
// sure we can handle all content ids. But it's absolutely worth it as it's
// a speedup of 4 for one of the major time consuming functions on storing
// mapblocks.
static content_t getBlockNodeIdMapping_mapping[USHRT_MAX + 1];
static void getBlockNodeIdMapping(NameIdMapping *nimap, MapNode *nodes,
		INodeDefManager *nodedef)
{
	memset(getBlockNodeIdMapping_mapping, 0xFF, (USHRT_MAX + 1) * sizeof(content_t));

	std::set<content_t> unknown_contents;
	content_t id_counter = 0;
	for (u32 i = 0; i < MapBlock::nodecount; i++) {
		content_t global_id = nodes[i].getContent();
		content_t id = CONTENT_IGNORE;

		// Try to find an existing mapping
		if (getBlockNodeIdMapping_mapping[global_id] != 0xFFFF) {
			id = getBlockNodeIdMapping_mapping[global_id];
		}
		else
		{
			// We have to assign a new mapping
			id = id_counter++;
			getBlockNodeIdMapping_mapping[global_id] = id;

			const ContentFeatures &f = nodedef->get(global_id);
			const std::string &name = f.name;
			if(name == "")
				unknown_contents.insert(global_id);
			else
				nimap->set(id, name);
		}

		// Update the MapNode
		nodes[i].setContent(id);
	}
	for(const auto &c: unknown_contents) {
		logger.error("getBlockNodeIdMapping(): IGNORING ERROR: "
				"Name for node id %d not known", c);
	}
}
// Correct ids in the block to match nodedef based on names.
// Unknown ones are added to nodedef.
// Will not update itself to match id-name pairs in nodedef.
static void correctBlockNodeIds(const NameIdMapping *nimap, MapNode *nodes,
		IGameDef *gamedef)
{
	INodeDefManager *nodedef = gamedef->ndef();
	// This means the block contains incorrect ids, and we contain
	// the information to convert those to names.
	// nodedef contains information to convert our names to globally
	// correct ids.
	std::set<content_t> unnamed_contents;
	std::set<std::string> unallocatable_contents;
	for (u32 i = 0; i < MapBlock::nodecount; i++) {
		content_t local_id = nodes[i].getContent();
		std::string name;
		bool found = nimap->getName(local_id, name);
		if(!found){
			unnamed_contents.insert(local_id);
			continue;
		}
		content_t global_id;
		found = nodedef->getId(name, global_id);
		if(!found){
			global_id = gamedef->allocateUnknownNodeId(name);
			if(global_id == CONTENT_IGNORE){
				unallocatable_contents.insert(name);
				continue;
			}
		}
		nodes[i].setContent(global_id);
	}
	for(const auto &c: unnamed_contents) {
		logger.error("correctBlockNodeIds(): IGNORING ERROR: "
				"Block contains id %d, with no name mapping", c);
	}

	for(const auto &str: unallocatable_contents) {
		logger.error("correctBlockNodeIds(): IGNORING ERROR: "
				"Could not allocate global id for node name \"%s\"", str.c_str());
	}
}

void MapBlock::serialize(std::ostream &os, u8 version, bool disk, epixel::MapBlockDB& blockdb_data)
{
	if (!ser_ver_supported(version))
		throw VersionMismatchException("ERROR: MapBlock format not supported");

	if (!epixel_ser_version_supported(blockdb_data.epixel_version))
		throw VersionMismatchException("ERROR: MapBlock format not supported (epixel_version)");

	if (data == NULL) {
		throw SerializationError("ERROR: Not writing dummy block.");
	}

	FATAL_ERROR_IF(version < SER_FMT_VER_LOWEST_WRITE, "Serialisation version error");

	// First byte
	u8 flags = 0;
	if(is_underground)
		flags |= 0x01;
	if(getDayNightDiff())
		flags |= 0x02;
	if(m_lighting_expired)
		flags |= 0x04;
	if(m_generated == false)
		flags |= 0x08;
	if (blockdb_data.epixel_version > 0) {
		blockdb_data.flags = flags;
	}
	else {
		writeU8(os, flags);
	}

	/*
		Bulk node data
	*/
	NameIdMapping nimap;
	if (disk) {
		MapNode *tmp_nodes = new MapNode[nodecount];
		for(u32 i=0; i<nodecount; i++)
			tmp_nodes[i] = data[i];
		getBlockNodeIdMapping(&nimap, tmp_nodes, m_gamedef->ndef());

		u8 content_width = 2;
		u8 params_width = 2;

		blockdb_data.content_width = content_width;
		blockdb_data.params_width = params_width;
		std::ostringstream oss(std::ios_base::binary);

		switch (blockdb_data.epixel_version) {
			case 1: {
				MapNode::serializeBulk(oss, version, tmp_nodes, nodecount,
					content_width, params_width, false);
				blockdb_data.raw_nodes = oss.str();
				break;
			}
			case 2: {
				MapNode::serializeBulk(oss, version, tmp_nodes, nodecount,
					content_width, params_width, false);
				compressLZ4(oss.str(), blockdb_data.raw_nodes);
				break;
			}
			default: {
				writeU8(os, content_width);
				writeU8(os, params_width);
				MapNode::serializeBulk(os, version, tmp_nodes, nodecount,
						content_width, params_width, true);
				break;
			}
		}

		delete[] tmp_nodes;
	}
	else {
		u8 content_width = 2;
		u8 params_width = 2;
		writeU8(os, content_width);
		writeU8(os, params_width);
		MapNode::serializeBulk(os, version, data, nodecount,
				content_width, params_width, true);
	}

	/*
		Node metadata
	*/
	std::ostringstream oss(std::ios_base::binary);
	m_node_metadata.serialize(oss);
	switch (blockdb_data.epixel_version) {
		case 1: blockdb_data.raw_nodes_metas = oss.str(); break;
		case 2: {
			compressLZ4(oss.str(), blockdb_data.raw_nodes_metas);
			break;
		}
		default: compressZlib(oss.str(), os); break;
	}

	/*
		Data that goes to disk, but not the network
	*/
	if(disk) {
		if(version <= 24) {
			// Node timers
			m_node_timers.serialize(os, version);
		}

		// Static objects
		m_static_objects.serialize(os);

		// Timestamp
		if (blockdb_data.epixel_version > 0) {
			blockdb_data.timestamp = getTimestamp();
		}
		else {
			writeU32(os, getTimestamp());
		}

		// Write block-specific node definition id mapping
		nimap.serialize(os);

		if(version >= 25){
			// Node timers
			m_node_timers.serialize(os, version);
		}
	}
}

void MapBlock::serializeNetworkSpecific(std::ostream &os, u16 net_proto_version)
{
	if(data == NULL)
	{
		throw SerializationError("ERROR: Not writing dummy block.");
	}

	if(net_proto_version >= 21){
		int version = 1;
		writeU8(os, version);
		writeF1000(os, 0); // deprecated heat
		writeF1000(os, 0); // deprecated humidity
	}
}

void MapBlock::deSerialize(std::istream &is, u8 version, bool disk, const epixel::MapBlockDB& blockdb_data)
{
	if(!ser_ver_supported(version))
		throw VersionMismatchException("ERROR: MapBlock format not supported");

	if (!epixel_ser_version_supported(blockdb_data.epixel_version))
		throw VersionMismatchException("ERROR: MapBlock format not supported (epixel_version)");

	logger.debug("MapBlock::deSerialize (%d,%d,%d)", getPos().X, getPos().Y, getPos().Z);

	m_day_night_differs_expired = false;

	u8 flags = (blockdb_data.epixel_version > 0) ? blockdb_data.flags : readU8(is);
	is_underground = (flags & 0x01) ? true : false;
	m_day_night_differs = (flags & 0x02) ? true : false;
	m_lighting_expired = (flags & 0x04) ? true : false;
	m_generated = (flags & 0x08) ? false : true;

	/*
		Bulk node data
	*/
	logger.debug("MapBlock::deSerialize (%d,%d,%d): Bulk node data", getPos().X, getPos().Y, getPos().Z);
	u8 content_width = (blockdb_data.epixel_version > 0) ? blockdb_data.content_width : readU8(is);
	u8 params_width = (blockdb_data.epixel_version > 0) ? blockdb_data.params_width : readU8(is);
	if(content_width != 1 && content_width != 2)
		throw SerializationError("MapBlock::deSerialize(): invalid content_width");
	if(params_width != 2)
		throw SerializationError("MapBlock::deSerialize(): invalid params_width");
	switch (blockdb_data.epixel_version) {
		case 1: {
			std::istringstream iss(blockdb_data.raw_nodes, std::ios_base::binary);
			MapNode::deSerializeBulk(iss, version, data, nodecount,
					content_width, params_width, false);
			break;
		}
		case 2: {
			std::string data_u;
			decompressLZ4(blockdb_data.raw_nodes, data_u);
			std::istringstream iss_u(data_u, std::ios_base::binary);
			MapNode::deSerializeBulk(iss_u, version, data, nodecount,
					content_width, params_width, false);
			break;
		}
		default: {
			MapNode::deSerializeBulk(is, version, data, nodecount,
					content_width, params_width, blockdb_data.epixel_version == 0);
			break;
		}
	}

	/*
		NodeMetadata
	*/
	logger.debug("MapBlock::deSerialize (%d,%d,%d): Node metadata", getPos().X, getPos().Y, getPos().Z);
	// Ignore errors
	try {
		switch (blockdb_data.epixel_version) {
			case 1: {
				std::istringstream iss(blockdb_data.raw_nodes_metas, std::ios_base::binary);
				m_node_metadata.deSerialize(iss, m_gamedef->idef());
				break;
			}
			case 2: {
				std::string data_u;
				decompressLZ4(blockdb_data.raw_nodes_metas, data_u);
				std::istringstream iss_u(data_u, std::ios_base::binary);
				m_node_metadata.deSerialize(iss_u, m_gamedef->idef());
				break;
			}
			default: {
				std::ostringstream oss(std::ios_base::binary);
				decompressZlib(is, oss);
				std::istringstream iss(oss.str(), std::ios_base::binary);
				if (version >= 23)
					m_node_metadata.deSerialize(iss, m_gamedef->idef());
				else
					content_nodemeta_deserialize_legacy(iss,
						&m_node_metadata, &m_node_timers,
						m_gamedef->idef());
				break;
			}
		}
	} catch(SerializationError &e) {
		logger.warn("MapBlock::deSerialize(): Ignoring an error"
				" while deserializing node metadata at (%d,%d,%d): %s",
				m_pos.X, m_pos.Y, m_pos.Z, e.what());
	}

	/*
		Data that is only on disk
	*/
	if(disk)
	{
		// Node timers
		if(version == 23){
			// Read unused zero
			readU8(is);
		}
		if(version == 24) {
			logger.debug("MapBlock::deSerialize (%d,%d,%d): Node timers (ver==24)",
					getPos().X, getPos().Y, getPos().Z);
			m_node_timers.deSerialize(is, version);
		}

		// Static objects
		logger.debug("MapBlock::deSerialize (%d,%d,%d): Static objects",
				getPos().X, getPos().Y, getPos().Z);
		m_static_objects.deSerialize(is);

		// Timestamp
		logger.debug("MapBlock::deSerialize (%d,%d,%d): Timestamp",
				getPos().X, getPos().Y, getPos().Z);
		setTimestamp((blockdb_data.epixel_version > 0) ? blockdb_data.timestamp : readU32(is));
		m_disk_timestamp = m_timestamp;

		// Dynamically re-set ids based on node names
		logger.debug("MapBlock::deSerialize (%d,%d,%d): NameIdMapping",
				getPos().X, getPos().Y, getPos().Z);
		NameIdMapping nimap;
		nimap.deSerialize(is);
		correctBlockNodeIds(&nimap, data, m_gamedef);

		if(version >= 25) {
			logger.debug("MapBlock::deSerialize (%d,%d,%d): Node timers (ver>=25)",
					getPos().X, getPos().Y, getPos().Z);
			m_node_timers.deSerialize(is, version);
		}
	}

	logger.debug("MapBlock::deSerialize (%d,%d,%d): Done.",
			getPos().X, getPos().Y, getPos().Z);
}

void MapBlock::deSerializeNetworkSpecific(std::istream &is)
{
	try {
		int version = readU8(is);
		//if(version != 1)
		//	throw SerializationError("unsupported MapBlock version");
		if(version >= 1) {
			readF1000(is); // deprecated heat
			readF1000(is); // deprecated humidity
		}
	}
	catch(SerializationError &e) {
		logger.warn("MapBlock::deSerializeNetworkSpecific(): Ignoring an error: %s",
				e.what());
	}
}

/*
	Get a quick string to describe what a block actually contains
*/
std::string analyze_block(MapBlock *block)
{
	if(block == NULL)
		return "NULL";

	std::ostringstream desc;

	v3s16 p = block->getPos();
	char spos[20];
	snprintf(spos, 20, "(%2d,%2d,%2d), ", p.X, p.Y, p.Z);
	desc<<spos;

	switch(block->getModified())
	{
	case MOD_STATE_CLEAN:
		desc<<"CLEAN,           ";
		break;
	case MOD_STATE_WRITE_AT_UNLOAD:
		desc<<"WRITE_AT_UNLOAD, ";
		break;
	case MOD_STATE_WRITE_NEEDED:
		desc<<"WRITE_NEEDED,    ";
		break;
	default:
		desc<<"unknown getModified()="+itos(block->getModified())+", ";
	}

	if(block->isGenerated())
		desc<<"is_gen [X], ";
	else
		desc<<"is_gen [ ], ";

	if(block->getIsUnderground())
		desc<<"is_ug [X], ";
	else
		desc<<"is_ug [ ], ";

	if(block->getLightingExpired())
		desc<<"lighting_exp [X], ";
	else
		desc<<"lighting_exp [ ], ";

	if(block->isDummy())
	{
		desc<<"Dummy, ";
	}
	else
	{
		bool full_ignore = true;
		bool some_ignore = false;
		bool full_air = true;
		bool some_air = false;
		for(s16 z0=0; z0<MAP_BLOCKSIZE; z0++)
		for(s16 y0=0; y0<MAP_BLOCKSIZE; y0++)
		for(s16 x0=0; x0<MAP_BLOCKSIZE; x0++)
		{
			v3s16 p(x0,y0,z0);
			MapNode n = block->getNodeNoEx(p);
			content_t c = n.getContent();
			if(c == CONTENT_IGNORE)
				some_ignore = true;
			else
				full_ignore = false;
			if(c == CONTENT_AIR)
				some_air = true;
			else
				full_air = false;
		}

		desc<<"content {";

		std::ostringstream ss;

		if(full_ignore)
			ss<<"IGNORE (full), ";
		else if(some_ignore)
			ss<<"IGNORE, ";

		if(full_air)
			ss<<"AIR (full), ";
		else if(some_air)
			ss<<"AIR, ";

		if(ss.str().size()>=2)
			desc<<ss.str().substr(0, ss.str().size()-2);

		desc<<"}, ";
	}

	return desc.str().substr(0, desc.str().size()-2);
}


//END
