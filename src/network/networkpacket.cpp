/*
Minetest
Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "networkpacket.h"
#include "debug.h"
#include "util/serialize.h"

NetworkPacket::NetworkPacket(u16 command, u32 datasize, u16 peer_id):
ByteBuffer(), m_command(command), m_peer_id(peer_id)
{
	m_datasize = datasize;
	m_read_offset = 0;
	m_data.resize(m_datasize);
}

NetworkPacket::NetworkPacket(u16 command, u32 datasize):
ByteBuffer(), m_command(command), m_peer_id(0)
{
	m_datasize = datasize;
	m_read_offset = 0;
	m_data.resize(m_datasize);
}

NetworkPacket::~NetworkPacket()
{
	m_data.clear();
}

void NetworkPacket::putRawPacket(u8 *data, u32 datasize, u16 peer_id)
{
	// If a m_command is already set, we are rewriting on same packet
	// This is not permitted
	assert(m_command == 0);

	m_datasize = datasize - 2;
	m_peer_id = peer_id;

	// split command and datas
	m_command = readU16(&data[0]);
	m_data = std::vector<u8>(&data[2], &data[2 + m_datasize]);
}

Buffer<u8> NetworkPacket::oldForgePacket()
{
	Buffer<u8> sb(m_datasize + 2);
	writeU16(&sb[0], m_command);

	u8* datas = getU8Ptr(0);

	if (datas != NULL)
		memcpy(&sb[2], datas, m_datasize);
	return sb;
}
