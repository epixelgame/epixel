/*
Minetest
Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef NETWORKPACKET_HEADER
#define NETWORKPACKET_HEADER

#include "util/bytebuffer.h"
#include "util/pointer.h"
#include "util/numeric.h"
#include "networkprotocol.h"

class NetworkPacket: public ByteBuffer
{

public:
	NetworkPacket(u16 command, u32 datasize, u16 peer_id);
	NetworkPacket(u16 command, u32 datasize);
	NetworkPacket(): m_command(0), m_peer_id(0)
			{ m_datasize = 0; m_read_offset = 0; }
	~NetworkPacket();

	void putRawPacket(u8 *data, u32 datasize, u16 peer_id);

	// Getters
	u16 getPeerId() const { return m_peer_id; }
	u16 getCommand() const { return m_command; }

	// Temp, we remove SharedBuffer when migration finished
	Buffer<u8> oldForgePacket();
private:
		u16 m_command;
		u16 m_peer_id;
};

/**
 * @brief The PacketDestructor class
 *
 * RAII class permitting to destroy every NetworkPacket
 */
class PacketDestructor
{
public:
	PacketDestructor(NetworkPacket* pkt_): pkt(pkt_) {}
	~PacketDestructor() { delete pkt; }
private:
	NetworkPacket* pkt;
};

#endif
