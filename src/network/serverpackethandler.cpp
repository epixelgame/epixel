/*
Minetest
Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "server.h"
#include "content_abm.h"
#include "content_sao.h"
#include "database.h"
#include "emerge.h"
#include "mapblock.h"
#include "scripting_game.h"
#include "tool.h"
#include "version.h"
#include "network/serveropcodes.h"
#include "util/auth.h"
#include "util/pointedthing.h"
#include "util/serialize.h"
#include "util/srp.h"
#include "contrib/playersao.h"
#include "contrib/teleport.h"
#include <algorithm>
#ifdef SERVER
#include "contrib/is_chatclient.h"
#endif

static inline const bool is_guest(const std::string &playername)
{
	std::regex re("^[gG]uest"), re2("^[pP]layer[0-9][0-9]"),
		re3("^[iI]nvite");
	std::smatch match;
	return (std::regex_search(playername, match, re) ||
			std::regex_search(playername, match, re2) ||
			std::regex_search(playername, match, re3));
}

static inline const bool is_base64(const unsigned char c) {
	return (isalnum(c) || (c == '+') || (c == '/') || c == '=');
}

static inline const bool base64_is_valid(const std::string &s)
{
	for(size_t i=0; i<s.size(); i++)
		if(!is_base64(s[i])) return false;
	return true;
}


void Server::handleCommand_Deprecated(NetworkPacket* pkt)
{
	logger.info("Server: %s not supported anymore", toServerCommandTable[pkt->getCommand()].name.c_str());
}

bool Server::handle_CommonInit(const u16 peer_id, const u16 net_proto_version, const std::string &addr_s,
		const std::string &playername)
{
	// We don't care about superadmin setting not present
	std::string superadmin = "";
	g_settings->getNoEx("superadmin", superadmin);
	// If multiplayer, not superadmin & maintenance enabled, refuse
	if (playername.compare(superadmin) != 0 &&
#ifndef SERVER
		!isSingleplayer() &&
#endif
		g_settings->get(BOOLSETTING_ENABLE_MAINTENANCE_MODE)) {
		DenyAccess_Legacy(peer_id, L"This server is actually in maintenance. "
				L"Please come back later.");
		return false;
	}

	// Disable legacy MT/Multicraft guests
	if (g_settings->get(BOOLSETTING_DISALLOW_GUEST_CONNECTION) &&
		is_guest(playername)) {
		logger.notice("Server: Player with a guest name "
							  " tried to connect from %s", addr_s.c_str());
		DenyAccess_Legacy(peer_id, L"Guest nicknames are not allowed on "
				L"this server. Please choose a valid nickname.");
		return false;
	}

	// Disable users with same name but different cases
	if (!g_settings->get(BOOLSETTING_ENABLE_MULTICASE_USERNAMES)) {
		std::vector<std::string> names;
		m_auth_database->getUsernamesWithoutCase(playername, names);

		// If there are names found for this username
		if (names.size() > 0) {
			bool account_exists = false;
			for (const auto &name: names) {
				if (name.compare(playername) == 0) {
					account_exists = true;
					break;
				}
			}

			// If account not found in the list it's an homonyme
			if (!account_exists) {
				logger.notice("Server: Player with a different case name "
									  " tried to connect from %s", addr_s.c_str());
				DenyAccess_Legacy(peer_id, L"This nickname is already "
						"registered with a different case. Please choose another nickname.");
				return false;
			}
		}
	}

	{
		std::string reason;
		if (contrib_on_prejoinplayer(std::string(playername), reason)) {
			logger.notice("Server: Player with the name \"%s\" tried to connect from %s "
					"but it was disallowed for the following reason: %s",
					playername.c_str(), addr_s.c_str(), reason.c_str());
			DenyAccessVerCompliant(peer_id, net_proto_version, SERVER_ACCESSDENIED_CUSTOM_STRING, reason);
			return false;
		}
	}

	return true;
}

void Server::handleCommand_Init(NetworkPacket* pkt)
{
	if(pkt->getSize() < 1)
		return;

	RemoteClient* client = getClient(pkt->getPeerId(), CS_Created);

	std::string addr_s;
	try {
		addr_s = getPeerAddressStr(pkt->getPeerId());
	}
	catch (con::PeerNotFoundException &e) {
		/*
		 * no peer for this packet found
		 * most common reason is peer timeout, e.g. peer didn't
		 * respond for some time, your server was overloaded or
		 * things like that.
		 */
		logger.info("Server::ProcessData(): Canceling: peer %d not found", pkt->getPeerId());
		return;
	}

	// If net_proto_version is set, this client has already been handled
	if (client->getState() > CS_Created) {
		logger.debug("Server: Ignoring multiple TOSERVER_INITs from %s"
				" (peer_id=%d)", addr_s.c_str(), pkt->getPeerId());
		return;
	}

	logger.debug("Server: Got TOSERVER_INIT from %s (peer_id=%d)",
			addr_s.c_str(), pkt->getPeerId());

#ifndef SERVER
	// Do not allow multiple players in simple singleplayer mode.
	// This isn't a perfect way to do it, but will suffice for now
	if (m_singleplayer_mode &&
		m_clients.getClientIDs().size() > 1) {
		logger.notice("Server: Not allowing another client (%s"
				") to connect in simple singleplayer mode", addr_s.c_str());
		DenyAccess(pkt->getPeerId(), SERVER_ACCESSDENIED_SINGLEPLAYER);
		return;
	}
#endif

	// First byte after command is maximum supported
	// serialization version
	u8 client_max;
	u16 supp_compr_modes;
	u16 min_net_proto_version = 0;
	u16 max_net_proto_version;
	std::string playerName;

	*pkt >> client_max >> supp_compr_modes >> min_net_proto_version
			>> max_net_proto_version >> playerName;

	u8 our_max = SER_FMT_VER_HIGHEST_READ;
	// Use the highest version supported by both
	u8 depl_serial_v = std::min(client_max, our_max);
	// If it's lower than the lowest supported, give up.
	if (depl_serial_v < SER_FMT_VER_LOWEST_READ)
		depl_serial_v = SER_FMT_VER_INVALID;

	if (depl_serial_v == SER_FMT_VER_INVALID) {
		logger.info("Server: A mismatched client tried to connect from %s (Protocol v%d)\n"
				"Server: Cannot negotiate serialization version with %s",
				addr_s.c_str(), (int)depl_serial_v, addr_s.c_str());
		DenyAccess(pkt->getPeerId(), SERVER_ACCESSDENIED_WRONG_VERSION);
		return;
	}

	client->setPendingSerializationVersion(depl_serial_v);

	/*
		Read and check network protocol version
	*/

	u16 net_proto_version = 0;

	// Figure out a working version if it is possible at all
	if (max_net_proto_version >= SERVER_PROTOCOL_VERSION_MIN ||
			min_net_proto_version <= SERVER_PROTOCOL_VERSION_MAX) {
		// If maximum is larger than our maximum, go with our maximum
		if (max_net_proto_version > SERVER_PROTOCOL_VERSION_MAX)
			net_proto_version = SERVER_PROTOCOL_VERSION_MAX;
		// Else go with client's maximum
		else
			net_proto_version = max_net_proto_version;
	}

	logger.debug("Server: %s: Protocol version: min: %d, max: %d, chosen: %d",
			addr_s.c_str(), min_net_proto_version, max_net_proto_version, net_proto_version);

	client->net_proto_version = net_proto_version;

	// On this handler at least protocol version 25 is required
	if (net_proto_version < 25 ||
			net_proto_version < SERVER_PROTOCOL_VERSION_MIN ||
			net_proto_version > SERVER_PROTOCOL_VERSION_MAX) {
		logger.info("Server: A mismatched client tried to connect from %s (Protocol v%d)",
				addr_s.c_str(), (int)depl_serial_v);
		DenyAccess(pkt->getPeerId(), SERVER_ACCESSDENIED_WRONG_VERSION);
		return;
	}

	if (g_settings->get(BOOLSETTING_STRICT_PROTOCOL_VERSION)) {
		if (net_proto_version != LATEST_PROTOCOL_VERSION) {
			logger.info("Server: A mismatched client tried to connect from %s (Protocol v%d). Strict checking enabled.",
					addr_s.c_str(), (int)depl_serial_v);
			DenyAccess(pkt->getPeerId(), SERVER_ACCESSDENIED_WRONG_VERSION);
			return;
		}
	}

	/*
		Validate player name
	*/
	const char* playername = playerName.c_str();

	size_t pns = playerName.size();
	if (pns == 0 || pns > PLAYERNAME_SIZE) {
		logger.notice("Server: Player with %s"
			" name tried to connect from %s",
			((pns > PLAYERNAME_SIZE) ? "a too long" : "an empty"), addr_s.c_str());
		DenyAccess(pkt->getPeerId(), SERVER_ACCESSDENIED_WRONG_NAME);
		return;
	}

	if (!string_allowed(playerName, PLAYERNAME_ALLOWED_CHARS)) {
		logger.notice("Server: Player with an invalid name "
				"tried to connect from %s", addr_s.c_str());
		DenyAccess(pkt->getPeerId(), SERVER_ACCESSDENIED_WRONG_CHARS_IN_NAME);
		return;
	}

	m_clients.setPlayerName(pkt->getPeerId(), playername);
	//TODO (later) case insensitivity

	std::string legacyPlayerNameCasing = playerName;

	if (strcasecmp(playername, "singleplayer") == 0
#ifndef SERVER
			&& !isSingleplayer()
#endif
		) {
		logger.notice("Server: Player with the name \"singleplayer\" "
				"tried to connect from %s", addr_s.c_str());
		DenyAccess(pkt->getPeerId(), SERVER_ACCESSDENIED_WRONG_NAME);
		return;
	}

	if (!handle_CommonInit(pkt->getPeerId(), net_proto_version, addr_s, playerName)) {
		return;
	}

	logger.info("Server: New connection: \"%s\" from %s (peer_id=%d)",
			playername, addr_s.c_str(), pkt->getPeerId());

	// Enforce user limit.
	// Don't enforce for users that have some admin right
	if (m_clients.getClientIDs(CS_Created).size() >= g_settings->get(U16SETTING_MAX_USERS) &&
			!checkPriv(playername, "server") &&
			!checkPriv(playername, "ban") &&
			!checkPriv(playername, "privs") &&
			!checkPriv(playername, "password") &&
			playername != g_settings->get("name")) {
		logger.notice("Server: %s tried to join from %s, but there are already max_users="
				"%d players.",
				playername, addr_s.c_str(), g_settings->get(U16SETTING_MAX_USERS));
		DenyAccess(pkt->getPeerId(), SERVER_ACCESSDENIED_TOO_MANY_USERS);
		return;
	}

	/*
		Compose auth methods for answer
	*/
	std::string encpwd; // encrypted Password field for the user
	bool has_auth = m_auth_database->loadUser(playername, encpwd);
	u32 auth_mechs = 0;

	client->chosen_mech = AUTH_MECHANISM_NONE;

	if (has_auth) {
		std::vector<std::string> pwd_components = str_split(encpwd, '#');
		if (pwd_components.size() == 4) {
			if (pwd_components[1] == "1") { // 1 means srp
				auth_mechs |= AUTH_MECHANISM_SRP;
				client->enc_pwd = encpwd;
			} else {
				logger.notice("User %s tried to log in, but password field"
					" was invalid (unknown mechcode).", playername);
				DenyAccess(pkt->getPeerId(), SERVER_ACCESSDENIED_SERVER_FAIL);
				return;
			}
		} else if (base64_is_valid(encpwd)) {
			auth_mechs |= AUTH_MECHANISM_LEGACY_PASSWORD;
			client->enc_pwd = encpwd;
		} else {
			logger.notice("User %s tried to log in, but password field"
				" was invalid (invalid base64).", playername);
			DenyAccess(pkt->getPeerId(), SERVER_ACCESSDENIED_SERVER_FAIL);
			return;
		}
	} else {
		std::string default_password = g_settings->get("default_password");
		if (default_password.length() == 0) {
			auth_mechs |= AUTH_MECHANISM_FIRST_SRP;
		} else {
			// Take care of default passwords.
			client->enc_pwd = get_encoded_srp_verifier(playerName, default_password);
			auth_mechs |= AUTH_MECHANISM_SRP;
		}
		// Create auth, but only on successful login
		client->create_player_on_auth_success = true;
	}

	/*
	*	Answer with a TOCLIENT_HELLO
	*/

	logger.debug("Sending TOCLIENT_HELLO with auth method field: %d", auth_mechs);

	NetworkPacket resp_pkt(TOCLIENT_HELLO, 1 + 4
		+ legacyPlayerNameCasing.size(), pkt->getPeerId());

	u16 depl_compress_mode = NETPROTO_COMPRESSION_NONE;
	resp_pkt << depl_serial_v << depl_compress_mode << net_proto_version
		<< auth_mechs << legacyPlayerNameCasing;

	Send(&resp_pkt);

	client->allowed_auth_mechs = auth_mechs;
	client->setDeployedCompressionMode(depl_compress_mode);

	m_clients.event(pkt->getPeerId(), CSE_Hello);
}

void Server::handleCommand_Init_Legacy(NetworkPacket* pkt)
{
	// [0] u8 SER_FMT_VER_HIGHEST_READ
	// [1] u8[20] player_name
	// [21] u8[28] password <--- can be sent without this, from old versions

	if (pkt->getSize() < 1+PLAYERNAME_SIZE)
		return;

	RemoteClient* client = getClient(pkt->getPeerId(), CS_Created);

	std::string addr_s;
	try {
		addr_s = getPeerAddressStr(pkt->getPeerId());
	}
	catch (con::PeerNotFoundException &e) {
		/*
		 * no peer for this packet found
		 * most common reason is peer timeout, e.g. peer didn't
		 * respond for some time, your server was overloaded or
		 * things like that.
		 */
		logger.info("Server::ProcessData(): Canceling: peer %d not found", pkt->getPeerId());
		return;
	}

	// If net_proto_version is set, this client has already been handled
	if (client->getState() > CS_Created) {
		logger.debug("Server: Ignoring multiple TOSERVER_INITs from %s (peer_id=%d)",
				addr_s.c_str(), pkt->getPeerId());
		return;
	}

	logger.debug("Server: Got TOSERVER_INIT_LEGACY from %s (peer_id=%d)",
			addr_s.c_str(), pkt->getPeerId());

#ifndef SERVER
	// Do not allow multiple players in simple singleplayer mode.
	// This isn't a perfect way to do it, but will suffice for now
	if (
		m_singleplayer_mode &&
		m_clients.getClientIDs().size() > 1) {
		logger.info("Server: Not allowing another client (%s"
				") to connect in simple singleplayer mode", addr_s.c_str());
		DenyAccess_Legacy(pkt->getPeerId(), L"Running in simple singleplayer mode.");
		return;
	}
#endif

	// First byte after command is maximum supported
	// serialization version
	u8 client_max;

	*pkt >> client_max;

	u8 our_max = SER_FMT_VER_HIGHEST_READ;
	// Use the highest version supported by both
	int deployed = std::min(client_max, our_max);
	// If it's lower than the lowest supported, give up.
	if (deployed < SER_FMT_VER_LOWEST_READ)
		deployed = SER_FMT_VER_INVALID;

	if (deployed == SER_FMT_VER_INVALID) {
		logger.info("Server: A mismatched client tried to connect from %s (Protocol v%d)\n"
				"Server: Cannot negotiate serialization version with %s",
				addr_s.c_str(), (int)deployed, addr_s.c_str());
		DenyAccess_Legacy(pkt->getPeerId(), std::wstring(
				L"Your client's version is not supported.\n"
				L"Server version is ")
				+ utf8_to_wide(g_version_string) + L"."
		);
		return;
	}

	client->setPendingSerializationVersion(deployed);

	/*
		Read and check network protocol version
	*/

	u16 min_net_proto_version = 0;
	if (pkt->getSize() >= 1 + PLAYERNAME_SIZE + PASSWORD_SIZE + 2)
		min_net_proto_version = pkt->getU16(1 + PLAYERNAME_SIZE + PASSWORD_SIZE);

	// Use same version as minimum and maximum if maximum version field
	// doesn't exist (backwards compatibility)
	u16 max_net_proto_version = min_net_proto_version;
	if (pkt->getSize() >= 1 + PLAYERNAME_SIZE + PASSWORD_SIZE + 2 + 2)
		max_net_proto_version = pkt->getU16(1 + PLAYERNAME_SIZE + PASSWORD_SIZE + 2);

	// Start with client's maximum version
	u16 net_proto_version = max_net_proto_version;

	// Figure out a working version if it is possible at all
	if (max_net_proto_version >= SERVER_PROTOCOL_VERSION_MIN ||
			min_net_proto_version <= SERVER_PROTOCOL_VERSION_MAX) {
		// If maximum is larger than our maximum, go with our maximum
		if (max_net_proto_version > SERVER_PROTOCOL_VERSION_MAX)
			net_proto_version = SERVER_PROTOCOL_VERSION_MAX;
		// Else go with client's maximum
		else
			net_proto_version = max_net_proto_version;
	}

	// The client will send up to date init packet, ignore this one
	if (net_proto_version >= 25)
		return;

	logger.debug("Server: %s: Protocol version: min: %d, max %d, chosen: %d",
			addr_s.c_str(), min_net_proto_version, max_net_proto_version, net_proto_version);

	client->net_proto_version = net_proto_version;

	if (net_proto_version < SERVER_PROTOCOL_VERSION_MIN ||
			net_proto_version > SERVER_PROTOCOL_VERSION_MAX) {
		logger.info("Server: A mismatched client tried to connect from %s (Protocol v%d)",
				addr_s.c_str(), (int)deployed);
		DenyAccess_Legacy(pkt->getPeerId(), std::wstring(
				L"Your client's version is not supported.\n"
				L"Server version is ")
				+ utf8_to_wide(g_version_string) + L",\n"
				+ L"server's PROTOCOL_VERSION is "
				+ utf8_to_wide(itos(SERVER_PROTOCOL_VERSION_MIN))
				+ L"..."
				+ utf8_to_wide(itos(SERVER_PROTOCOL_VERSION_MAX))
				+ L", client's PROTOCOL_VERSION is "
				+ utf8_to_wide(itos(min_net_proto_version))
				+ L"..."
				+ utf8_to_wide(itos(max_net_proto_version))
		);
		return;
	}

	if (g_settings->get(BOOLSETTING_STRICT_PROTOCOL_VERSION)) {
		if (net_proto_version != LATEST_PROTOCOL_VERSION) {
			logger.info("Server: A mismatched client tried to connect from %s (Protocol v%d). Strict checking enabled.",
					addr_s.c_str(), (int)deployed);
			DenyAccess_Legacy(pkt->getPeerId(), std::wstring(
					L"Your client's version is not supported.\n"
					L"Server version is ")
					+ utf8_to_wide(g_version_string) + L",\n"
					+ L"server's PROTOCOL_VERSION (strict) is "
					+ utf8_to_wide(itos(LATEST_PROTOCOL_VERSION))
					+ L", client's PROTOCOL_VERSION is "
					+ utf8_to_wide(itos(min_net_proto_version))
					+ L"..."
					+ utf8_to_wide(itos(max_net_proto_version))
			);
			return;
		}
	}

	/*
		Set up player
	*/
	char playername[PLAYERNAME_SIZE];
	unsigned int playername_length = 0;
	for (; playername_length < PLAYERNAME_SIZE; playername_length++ ) {
		playername[playername_length] = pkt->getChar(1+playername_length);
		if (pkt->getChar(1+playername_length) == 0)
			break;
	}

	if (playername_length == PLAYERNAME_SIZE) {
		logger.notice("Server: Player with name exceeding max length "
				"tried to connect from %s", addr_s.c_str());
		DenyAccess_Legacy(pkt->getPeerId(), L"Name too long");
		return;
	}

	if (playername[0] == '\0') {
		logger.notice("Server: Player with an empty name "
				"tried to connect from %s", addr_s.c_str());
		DenyAccess_Legacy(pkt->getPeerId(), L"Empty name");
		return;
	}

	if (!string_allowed(playername, PLAYERNAME_ALLOWED_CHARS)) {
		logger.notice("Server: Player with an invalid name "
				"tried to connect from %s", addr_s.c_str());
		DenyAccess_Legacy(pkt->getPeerId(), L"Name contains unallowed characters");
		return;
	}

	if (strlen(playername) < g_settings->get(U16SETTING_MIN_PLAYERNAME_SIZE)) {
		logger.notice("Server: Player with a too short name "
				" tried to connect from %s", addr_s.c_str());
		DenyAccess_Legacy(pkt->getPeerId(), L"Your nickname is too short. "
				L"Please choose a valid nickname.");
		return;
	}

	if (!handle_CommonInit(pkt->getPeerId(), net_proto_version, addr_s, playername)) {
		return;
	}

	if (
#ifndef SERVER
		!isSingleplayer() &&
#endif
			strcasecmp(playername, "singleplayer") == 0) {
		logger.notice("Server: Player with the name \"singleplayer\" "
				"tried to connect from %s", addr_s.c_str());
		DenyAccess_Legacy(pkt->getPeerId(), L"Name is not allowed");
		return;
	}

	logger.info("Server: New connection: '%s' from %s (peer_id=%d)",
			playername, addr_s.c_str(), pkt->getPeerId());

	// Get password
	char given_password[PASSWORD_SIZE];
	if (pkt->getSize() < 1 + PLAYERNAME_SIZE + PASSWORD_SIZE) {
		// old version - assume blank password
		given_password[0] = 0;
	}
	else {
		for (u16 i = 0; i < PASSWORD_SIZE - 1; i++) {
			given_password[i] = pkt->getChar(21 + i);
		}
		given_password[PASSWORD_SIZE - 1] = 0;
	}

	if (!base64_is_valid(given_password)) {
		logger.notice("Server: %s supplied invalid password hash",
				playername);
		DenyAccess_Legacy(pkt->getPeerId(), L"Invalid password hash");
		return;
	}

	// Enforce user limit.
	// Don't enforce for users that have some admin right
	if (m_clients.getClientIDs(CS_Created).size() >= g_settings->get(U16SETTING_MAX_USERS) &&
			!checkPriv(playername, "server") &&
			!checkPriv(playername, "ban") &&
			!checkPriv(playername, "privs") &&
			!checkPriv(playername, "password") &&
			playername != g_settings->get("name")) {
		logger.notice("Server: %s tried to join, but there are already max_users=%s"
				" players.", playername, g_settings->get(U16SETTING_MAX_USERS));
		DenyAccess_Legacy(pkt->getPeerId(), L"Too many users.");
		return;
	}

	// If no authentication info exists for user, create it
	if (!m_auth_database->userExists(playername)) {
		if (std::string(given_password).empty()
#ifndef SERVER
				&& !isSingleplayer()
#endif
		) {
			logger.notice("Server: %s supplied empty password", playername);
			DenyAccess_Legacy(pkt->getPeerId(), L"Empty passwords are "
					L"disallowed. Set a password and try again.");
			return;
		}
		std::string raw_default_password =
			g_settings->get("default_password");
		std::string initial_password =
			translate_password(playername, raw_default_password);

		// If default_password is empty, allow any initial password
		if (raw_default_password.length() == 0)
			initial_password = given_password;

		m_auth_database->createUser(playername, initial_password);
		client->markAsNewPlayer(true);
	}

	if (!m_auth_database->loadUserAndVerifyPassword(playername, given_password)) {
		logger.notice("Server: %s supplied wrong password", playername);
		DenyAccess_Legacy(pkt->getPeerId(), L"Wrong password");
		return;
	}

	RemotePlayer *player = m_env->getPlayer(playername);
	if (player && player->getPeerID() != PEER_ID_INEXISTENT) {
		logger.notice("Server: %s: Failed to emerge player"
				" (player allocated to an another client)", playername);
		DenyAccess_Legacy(pkt->getPeerId(), L"Another client is connected with this "
				L"name. If your client closed unexpectedly, try again in "
				L"a minute.");
	}

	m_clients.setPlayerName(pkt->getPeerId(), playername);

	/*
		Answer with a TOCLIENT_INIT
	*/

	NetworkPacket resp_pkt(TOCLIENT_INIT_LEGACY, 1 + 6 + 8 + 4,
			pkt->getPeerId());

	resp_pkt << (u8) deployed << (v3s16) floatToInt(v3f(0,0,0), BS)
			<< (u64) m_env->getServerMap()->getSeed()
			<< g_settings->get(FSETTING_DEDICATED_SERVER_STEP);

	Send(&resp_pkt);

	m_clients.event(pkt->getPeerId(), CSE_InitLegacy);
}

void Server::handleCommand_Init2(NetworkPacket* pkt)
{
	logger.debug("Server: Got TOSERVER_INIT2 from %d", pkt->getPeerId());

	m_clients.event(pkt->getPeerId(), CSE_GotInit2);
	u16 protocol_version = m_clients.getProtocolVersion(pkt->getPeerId());


	///// begin compatibility code
	PlayerSAO* playersao = NULL;
	if (protocol_version <= 22) {
		playersao = StageTwoClientInit(pkt->getPeerId());

		if (playersao == NULL) {
			logger.notice("TOSERVER_INIT2 stage 2 client init failed for peer %d",
				pkt->getPeerId());
			return;
		}
	}
	///// end compatibility code

	/*
		Send some initialization data
	*/

	logger.info("Server: Sending content to %s",
			getPlayerName(pkt->getPeerId()).c_str());

	// Send player movement settings
	SendMovement(pkt->getPeerId());

	// Send item definitions
	SendItemDef(pkt->getPeerId(), m_itemdef, protocol_version);

	// Send node definitions
	SendNodeDef(pkt->getPeerId(), m_nodedef, protocol_version);

	m_clients.event(pkt->getPeerId(), CSE_SetDefinitionsSent);

	// Send media announcement
	sendMediaAnnouncement(pkt->getPeerId());

	// Send detached inventories
	sendDetachedInventories(pkt->getPeerId());

	// Send time of day
	u16 time = m_env->getTimeOfDay();
	SendTimeOfDay(pkt->getPeerId(), time, g_settings->get(FSETTING_TIME_SPEED));

	///// begin compatibility code
	if (protocol_version <= 22) {
		m_clients.event(pkt->getPeerId(), CSE_SetClientReady);

		if (RemoteClient* client = getClientNoEx(pkt->getPeerId()))
			contrib_on_joinplayer(client, playersao);

		m_script->on_joinplayer(playersao);
	}
	///// end compatibility code

	// Warnings about protocol version can be issued here
	if (getClient(pkt->getPeerId())->net_proto_version < LATEST_PROTOCOL_VERSION) {
		SendChatMessage(pkt->getPeerId(), L"# Server: WARNING: YOUR CLIENT'S "
				L"VERSION MAY NOT BE FULLY COMPATIBLE WITH THIS SERVER!");
	}
}

void Server::handleCommand_RequestMedia(NetworkPacket* pkt)
{
	std::vector<std::string> tosend;
	u16 numfiles;

	*pkt >> numfiles;

	logger.info("Sending %d files to %s", numfiles, getPlayerName(pkt->getPeerId()).c_str());
	logger.debug("TOSERVER_REQUEST_MEDIA: ");

	for (u16 i = 0; i < numfiles; i++) {
		std::string name;

		*pkt >> name;

		tosend.push_back(name);
		logger.debug("TOSERVER_REQUEST_MEDIA: requested file %s",
				name.c_str());
	}

	sendRequestedMedia(pkt->getPeerId(), tosend);
}

void Server::handleCommand_ReceivedMedia(NetworkPacket* pkt)
{
}

void Server::handleCommand_ClientReady(NetworkPacket* pkt)
{
	u16 peer_id = pkt->getPeerId();
	u16 peer_proto_ver = getClient(peer_id, CS_InitDone)->net_proto_version;

	// clients <= protocol version 22 did not send ready message,
	// they're already initialized
	if (peer_proto_ver <= 22) {
		logger.info("Client sent message not expected by a client using protocol version <= 22, disconnecting peer_id: %d",
				peer_id);
		m_con.DisconnectPeer(peer_id);
		return;
	}

	if (pkt->getSize() < 8) {
		logger.error("TOSERVER_CLIENT_READY client sent inconsistent data, disconnecting peer_id: %d", peer_id);
		m_con.DisconnectPeer(peer_id);
		return;
	}

	u8 major_ver, minor_ver, patch_ver, reserved;
	std::string full_ver;
	*pkt >> major_ver >> minor_ver >> patch_ver >> reserved >> full_ver;

	m_clients.setClientVersion(
			peer_id, major_ver, minor_ver, patch_ver,
			full_ver);

	PlayerSAO* playersao = StageTwoClientInit(peer_id);

	if (playersao == NULL) {
		logger.notice("TOSERVER_CLIENT_READY stage 2 client init failed for peer_id: %d",
			peer_id);
		m_con.DisconnectPeer(peer_id);
		return;
	}

	m_clients.event(peer_id, CSE_SetClientReady);

	if (RemoteClient* client = getClientNoEx(pkt->getPeerId()))
			contrib_on_joinplayer(client, playersao);

	m_script->on_joinplayer(playersao);
}

void Server::handleCommand_GotBlocks(NetworkPacket* pkt)
{
	if (pkt->getSize() < 1)
		return;

	/*
		[0] u16 command
		[2] u8 count
		[3] v3s16 pos_0
		[3+6] v3s16 pos_1
		...
	*/

	u8 count;
	*pkt >> count;

	RemoteClient *client = getClient(pkt->getPeerId());

	if ((s16)pkt->getSize() < 1 + count * 6) {
		logger.error("Count %d pkt size %d", count, pkt->getSize());
		throw con::InvalidIncomingDataException
				("GOTBLOCKS length is too short");
	}

	for (u16 i = 0; i < count; i++) {
		v3s16 p;
		*pkt >> p;
		client->GotBlock(p);
	}
}

void Server::handleCommand_PlayerPos(NetworkPacket* pkt)
{
	if (pkt->getSize() < 12 + 12 + 4 + 4)
		return;

	v3s32 ps, ss;
	s32 f32pitch, f32yaw;

	*pkt >> ps;
	*pkt >> ss;
	*pkt >> f32pitch;
	*pkt >> f32yaw;

	f32 pitch = (f32)f32pitch / 100.0;
	f32 yaw = (f32)f32yaw / 100.0;
	u32 keyPressed = 0;

	if (pkt->getSize() >= 12 + 12 + 4 + 4 + 4)
		*pkt >> keyPressed;

	v3f position((f32)ps.X / 100.0, (f32)ps.Y / 100.0, (f32)ps.Z / 100.0);
	v3f speed((f32)ss.X / 100.0, (f32)ss.Y / 100.0, (f32)ss.Z / 100.0);

	pitch = modulo360f(pitch);
	yaw = modulo360f(yaw);

	RemotePlayer *player = m_env->getPlayer(pkt->getPeerId());
	if (player == NULL) {
		logger.error("Server::ProcessData(): Canceling: No player for peer_id=%d disconnecting peer!",
				pkt->getPeerId());
		m_con.DisconnectPeer(pkt->getPeerId());
		return;
	}

	PlayerSAO *playersao = player->getPlayerSAO();
	if (playersao == NULL) {
		logger.error("Server::ProcessData(): Canceling: "
				"No player object for peer_id=%d, disconnecting peer!",
				pkt->getPeerId());
		m_con.DisconnectPeer(pkt->getPeerId());
		return;
	}

	// If player is dead we don't care of this packet
	if (player->isDead()) {
		logger.debug("TOSERVER_PLAYERPOS: %s is dead. Ignoring packet.",
				player->getName());
		return;
	}

	player->setPosition(position);
	playersao->setSpeed(speed);
	playersao->setPitch(pitch);
	playersao->setYaw(yaw);
	player->keyPressed = keyPressed;
	player->control.up = (keyPressed & 1);
	player->control.down = (keyPressed & 2);
	player->control.left = (keyPressed & 4);
	player->control.right = (keyPressed & 8);
	player->control.jump = (keyPressed & 16);
	player->control.aux1 = (keyPressed & 32);
	player->control.sneak = (keyPressed & 64);
	player->control.LMB = (keyPressed & 128);
	player->control.RMB = (keyPressed & 256);

	if (playersao->checkMovementCheat()) {
		// Call callbacks
		m_script->on_cheat(playersao, "moved_too_fast");
		SendMovePlayer(pkt->getPeerId());
	}

	// Prevent player to go out of bounds
	if (objectpos_over_limit(position)) {
		// Set position to 99.9% of current position if out of bounds
		player->setPosition(position * 0.999);
		SendMovePlayer(pkt->getPeerId());
	}
}

void Server::handleCommand_DeletedBlocks(NetworkPacket* pkt)
{
	if (pkt->getSize() < 1)
		return;

	/*
		[0] u16 command
		[2] u8 count
		[3] v3s16 pos_0
		[3+6] v3s16 pos_1
		...
	*/

	u8 count;
	*pkt >> count;

	RemoteClient *client = getClient(pkt->getPeerId());

	if ((s16)pkt->getSize() < 1 + count * 6) {
		throw con::InvalidIncomingDataException
				("DELETEDBLOCKS length is too short");
	}

	for (u16 i = 0; i < count; i++) {
		v3s16 p;
		*pkt >> p;
		client->SetBlockNotSent(p);
	}
}

void Server::handleCommand_InventoryAction(NetworkPacket* pkt)
{
	RemotePlayer *player = m_env->getPlayer(pkt->getPeerId());
	if (player == NULL) {
		logger.error("Server::ProcessData(): Canceling: No player for peer_id=%d disconnecting peer!",
				pkt->getPeerId());
		m_con.DisconnectPeer(pkt->getPeerId());
		return;
	}

	PlayerSAO *playersao = player->getPlayerSAO();
	if (playersao == NULL) {
		logger.error("Server::ProcessData(): Canceling: No player object for peer_id=%d disconnecting peer!",
				pkt->getPeerId());
		m_con.DisconnectPeer(pkt->getPeerId());
		return;
	}

	// Strip command and create a stream
	std::string datastring(pkt->getString(0), pkt->getSize());
	logger.debug("TOSERVER_INVENTORY_ACTION: data=%s", datastring.c_str());
	std::istringstream is(datastring, std::ios_base::binary);
	// Create an action
	InventoryAction *a = InventoryAction::deSerialize(is);
	if (a == NULL) {
		logger.info("TOSERVER_INVENTORY_ACTION: "
				"InventoryAction::deSerialize() returned NULL");
		return;
	}

	/*
		Note: Always set inventory not sent, to repair cases
		where the client made a bad prediction.
	*/

	/*
		Handle restrictions and special cases of the move action
	*/
	if (a->getType() == IACTION_MOVE) {
		IMoveAction *ma = (IMoveAction*)a;

		ma->from_inv.applyCurrentPlayer(player->getName());
		ma->to_inv.applyCurrentPlayer(player->getName());

		if (ma->from_inv != ma->to_inv)
			setInventoryModified(ma->from_inv, false);
		setInventoryModified(ma->to_inv, false);

		bool from_inv_is_current_player =
			(ma->from_inv.type == InventoryLocation::PLAYER) &&
			(ma->from_inv.name == player->getName());

		bool to_inv_is_current_player =
			(ma->to_inv.type == InventoryLocation::PLAYER) &&
			(ma->to_inv.name == player->getName());

		/*
			Disable moving items out of craftpreview
		*/
		if (ma->from_list == "craftpreview") {
			logger.info("Ignoring IMoveAction from %s:%s to %s:%s because src is %s",
					ma->from_inv.dump().c_str(), ma->from_list.c_str(),ma->to_inv.dump().c_str(),
					ma->to_list.c_str(), ma->from_list.c_str());
			delete a;
			return;
		}

		/*
			Disable moving items into craftresult and craftpreview
		*/
		if (ma->to_list == "craftpreview" || ma->to_list == "craftresult") {
			logger.info("Ignoring IMoveAction from %s:%s to %s:%s because dst is %s",
					ma->from_inv.dump().c_str(), ma->from_list.c_str(), ma->to_inv.dump().c_str(),
					ma->to_list.c_str(), ma->to_list.c_str());
			delete a;
			return;
		}

		// Disallow moving items in elsewhere than player's inventory
		// if not allowed to interact
		if (!playersao->hasPriv("interact") &&
				(!from_inv_is_current_player ||
				!to_inv_is_current_player)) {
			logger.info("Cannot move outside of player's inventory: No interact privilege");
			delete a;
			return;
		}
	}
	/*
		Handle restrictions and special cases of the drop action
	*/
	else if (a->getType() == IACTION_DROP) {
		IDropAction *da = (IDropAction*)a;

		da->from_inv.applyCurrentPlayer(player->getName());

		setInventoryModified(da->from_inv, false);

		/*
			Disable dropping items out of craftpreview
		*/
		if (da->from_list == "craftpreview") {
			logger.info("Ignoring IDropAction from %s:%s because src is %s",
					da->from_inv.dump().c_str(), da->from_list.c_str(), da->from_list.c_str());
			delete a;
			return;
		}

		// Disallow dropping items if not allowed to interact
		if (!playersao->hasPriv("interact")) {
			delete a;
			return;
		}
	}
	/*
		Handle restrictions and special cases of the craft action
	*/
	else if (a->getType() == IACTION_CRAFT) {
		ICraftAction *ca = (ICraftAction*)a;

		ca->craft_inv.applyCurrentPlayer(player->getName());

		setInventoryModified(ca->craft_inv, false);

		//bool craft_inv_is_current_player =
		//	(ca->craft_inv.type == InventoryLocation::PLAYER) &&
		//	(ca->craft_inv.name == player->getName());

		// Disallow crafting if not allowed to interact
		if (!playersao->hasPriv("interact")) {
			logger.info("Cannot craft: No interact privilege");
			delete a;
			return;
		}
	}

	// Do the action
	a->apply(this, playersao, this);
	// Eat the action
	delete a;

	SendInventory(playersao);
}

void Server::handleCommand_ChatMessage(NetworkPacket* pkt)
{
	/*
		u16 command
		u16 length
		wstring message
	*/
	u16 len;
	*pkt >> len;

	std::wstring message;
	for (u16 i = 0; i < len; i++) {
		u16 tmp_wchar;
		*pkt >> tmp_wchar;

		message += (wchar_t)tmp_wchar;
	}

	RemotePlayer *player = m_env->getPlayer(pkt->getPeerId());
	if (player == NULL) {
		logger.error("Server::ProcessData(): Canceling: No player for peer_id=%d disconnecting peer!",
				pkt->getPeerId());
		m_con.DisconnectPeer(pkt->getPeerId());
		return;
	}

	// Get player name of this client
	std::wstring name = narrow_to_wide(player->getName());

	// Run script hook
	bool ate = contrib_on_chat_message(player,
			wide_to_narrow(message));

	if (ate)
		return;

	ate = m_script->on_chat_message(player->getName(),
			wide_to_narrow(message));
	// If script ate the message, don't proceed
	if (ate)
		return;

	// Line to send to players
	std::wstring line;
	// Whether to send to the player that sent the line
	bool send_to_sender_only = false;

	if (message.length() > g_settings->get(U16SETTING_MAX_CHATMESSAGE_LENGTH)) {
		SendChatMessage(pkt->getPeerId(), L"Your message exceed the maximum chat message limit set on the server."
				"It was refused. Send a shorter message");
		return;
	}

	// Commands are implemented in Lua, so only catch invalid
	// commands that were not "eaten" and send an error back
	if (message[0] == L'/') {
		message = message.substr(1);
		send_to_sender_only = true;
		if (message.length() == 0)
			line += L"-!- Empty command";
		else
			line += L"-!- Invalid command: " + str_split(message, L' ')[0];
	}
	else {
		if (player->getPlayerSAO() && player->getPlayerSAO()->hasPriv("shout")) {
			line += L" [WORLD] <";
			line += name;
			line += L"> ";
			line += message;
		} else {
			line += L"-!- You don't have permission to shout.";
			send_to_sender_only = true;
		}
	}

	if (!line.empty()) {
		/*
			Send the message to sender
		*/
		if (send_to_sender_only) {
			SendChatMessage(pkt->getPeerId(), line);
		}
		/*
			Send the message to others
		*/
		else {
			logger_chat.notice("CHAT: %s", wide_to_narrow(line).c_str());

			std::vector<u16> clients = m_clients.getClientIDs();

			for (const auto &peer_id: clients) {
				if (peer_id != pkt->getPeerId()) {
					PlayerSAO* dest_sao = getPlayerSAO(peer_id);
					// If no SAO loaded ?!? or SAO doesn't ignore player, send message
					if (!dest_sao || !dest_sao->isPlayerIgnored(player->getName())) {
						SendChatMessage(peer_id, line);
					}
				}
			}
#ifdef SERVER
			if (m_ischatclient_thread && g_settings->get(BOOLSETTING_ENABLE_INTERSERVER_CHAT_CLIENT)) {
				m_ischatclient_thread->pushMsg(new epixel::ISChatMessage(std::string(player->getName()), "", wide_to_narrow(message)));
			}
#endif
		}
	}
}

void Server::handleCommand_Damage(NetworkPacket* pkt)
{
	u8 damage;

	*pkt >> damage;

	RemotePlayer *player = m_env->getPlayer(pkt->getPeerId());
	if (player == NULL) {
		logger.error("Server::ProcessData(): Canceling: No player for peer_id=%d disconnecting peer!",
				pkt->getPeerId());
		m_con.DisconnectPeer(pkt->getPeerId());
		return;
	}

	PlayerSAO *playersao = player->getPlayerSAO();
	if (playersao == NULL) {
		logger.error("Server::ProcessData(): Canceling: No player object for peer_id=%d disconnecting peer!",
				pkt->getPeerId());
		m_con.DisconnectPeer(pkt->getPeerId());
		return;
	}

	if (g_settings->get(BOOLSETTING_ENABLE_DAMAGE)) {
		v3f pos(player->getPosition());
		logger.notice("%s damaged by %d hp at (%f,%f,%f)", player->getName(),
				(int)damage, pos.X / BS, pos.Y / BS, pos.Z / BS);

		playersao->setHP(playersao->getHP() - damage);
		SendPlayerHPOrDie(playersao);
	}
}

void Server::handleCommand_Password(NetworkPacket* pkt)
{
	if (pkt->getSize() != PASSWORD_SIZE * 2)
		return;

	std::string oldpwd;
	std::string newpwd;

	// Deny for clients using the new protocol
	RemoteClient* client = getClient(pkt->getPeerId(), CS_Created);
	if (client->net_proto_version >= 25) {
		logger.info("Server::handleCommand_Password(): Denying change: Client protocol version for peer_id=%d too new!",
				pkt->getPeerId());
		return;
	}

	for (u16 i = 0; i < PASSWORD_SIZE - 1; i++) {
		char c = pkt->getChar(i);
		if (c == 0)
			break;
		oldpwd += c;
	}

	for (u16 i = 0; i < PASSWORD_SIZE - 1; i++) {
		char c = pkt->getChar(PASSWORD_SIZE + i);
		if (c == 0)
			break;
		newpwd += c;
	}

	RemotePlayer *player = m_env->getPlayer(pkt->getPeerId());
	if (player == NULL) {
		logger.error("Server::ProcessData(): Canceling: No player for peer_id=%d disconnecting peer!",
				pkt->getPeerId());
		m_con.DisconnectPeer(pkt->getPeerId());
		return;
	}

	if (!base64_is_valid(newpwd)) {
		logger.info("Server: %s supplied invalid password hash", player->getName());
		// Wrong old password supplied!!
		SendChatMessage(pkt->getPeerId(), L"Invalid new password hash supplied. Password NOT changed.");
		return;
	}

	logger.info("Server: Client requests a password change");

	std::string playername = player->getName();

	if (!m_auth_database->loadUserAndVerifyPassword(playername, oldpwd)) {
		logger.info("Server: invalid old password");
		// Wrong old password supplied!!
		SendChatMessage(pkt->getPeerId(), L"Invalid old password supplied. Password NOT changed.");
		return;
	}

	if (m_auth_database->setPassword(playername, newpwd)) {
		logger.notice("%s changes password", player->getName());
		SendChatMessage(pkt->getPeerId(), L"Password change successful.");
	} else {
		logger.notice("%s tries to change password but it fails", player->getName());
		SendChatMessage(pkt->getPeerId(), L"Password change failed or unavailable.");
	}
}

void Server::handleCommand_PlayerItem(NetworkPacket* pkt)
{
	if (pkt->getSize() < 2)
		return;

	RemotePlayer *player = m_env->getPlayer(pkt->getPeerId());
	if (player == NULL) {
		logger.error("Server::ProcessData(): Canceling: No player for peer_id=%d disconnecting peer!",
				pkt->getPeerId());
		m_con.DisconnectPeer(pkt->getPeerId());
		return;
	}

	PlayerSAO *playersao = player->getPlayerSAO();
	if (playersao == NULL) {
		logger.error("Server::ProcessData(): Canceling: No player object for peer_id=%d disconnecting peer!",
				pkt->getPeerId());
		m_con.DisconnectPeer(pkt->getPeerId());
		return;
	}

	u16 item;

	*pkt >> item;

	playersao->setWieldIndex(item);
}

void Server::handleCommand_Respawn(NetworkPacket* pkt)
{
	RemotePlayer *player = m_env->getPlayer(pkt->getPeerId());
	if (player == NULL) {
		logger.error("Server::ProcessData(): Canceling: No player for peer_id=%d disconnecting peer!",
				pkt->getPeerId());
		m_con.DisconnectPeer(pkt->getPeerId());
		return;
	}

	if (!player->isDead())
		return;

	RespawnPlayer(pkt->getPeerId());

	v3f p(player->getPosition()/BS);
	logger.notice("%s respawns at (%f,%f,%f)", player->getName(), p.X, p.Y, p.Z);

	// ActiveObject is added to environment in AsyncRunStep after
	// the previous addition has been successfully removed
}

void Server::handleCommand_Interact(NetworkPacket* pkt)
{
	std::string datastring(pkt->getString(0), pkt->getSize());
	std::istringstream is(datastring, std::ios_base::binary);

	/*
		[0] u16 command
		[2] u8 action
		[3] u16 item
		[5] u32 length of the next item
		[9] serialized PointedThing
		actions:
		0: start digging (from undersurface) or use
		1: stop digging (all parameters ignored)
		2: digging completed
		3: place block or item (to abovesurface)
		4: use item
	*/
	u8 action = readU8(is);
	u16 item_i = readU16(is);
	std::istringstream tmp_is(deSerializeLongString(is), std::ios::binary);
	PointedThing pointed;
	pointed.deSerialize(tmp_is);

	logger.debug("TOSERVER_INTERACT: action=%d, item=%d, pointed=%s",
			(int)action, item_i, pointed.dump().c_str());

	RemotePlayer *player = m_env->getPlayer(pkt->getPeerId());
	if (player == NULL) {
		logger.error("Server::ProcessData(): Canceling: No player for peer_id=%d disconnecting peer!",
				pkt->getPeerId());
		m_con.DisconnectPeer(pkt->getPeerId());
		return;
	}

	PlayerSAO *playersao = player->getPlayerSAO();
	if (playersao == NULL) {
		logger.error("Server::ProcessData(): Canceling: No player object for peer_id=%d disconnecting peer!",
				pkt->getPeerId());
		m_con.DisconnectPeer(pkt->getPeerId());
		return;
	}

	if (player->isDead()) {
		logger.debug("TOSERVER_INTERACT: %s is dead. Ignoring packet.", player->getName());
		return;
	}

	v3f player_pos = playersao->getLastGoodPosition();

	// Update wielded item
	playersao->setWieldIndex(item_i);

	// Get pointed to node (undefined if not POINTEDTYPE_NODE)
	v3s16 p_under = pointed.node_undersurface;
	v3s16 p_above = pointed.node_abovesurface;

	// Get pointed to object (NULL if not POINTEDTYPE_OBJECT)
	ServerActiveObject *pointed_object = NULL;
	if (pointed.type == POINTEDTHING_OBJECT) {
		pointed_object = m_env->getActiveObject(pointed.object_id);
		if (pointed_object == NULL) {
			logger.debug("TOSERVER_INTERACT: pointed object is NULL");
			return;
		}

	}

	v3f pointed_pos_under = player_pos;
	v3f pointed_pos_above = player_pos;
	if (pointed.type == POINTEDTHING_NODE) {
		pointed_pos_under = intToFloat(p_under, BS);
		pointed_pos_above = intToFloat(p_above, BS);
	}
	else if (pointed.type == POINTEDTHING_OBJECT) {
		pointed_pos_under = pointed_object->getBasePosition();
		pointed_pos_above = pointed_pos_under;
	}

	/*
		Check that target is reasonably close
		(only when digging or placing things)
	*/
	if (action == 0 || action == 2 || action == 3) {
		float d = player_pos.getDistanceFrom(pointed_pos_under);
		float max_d = BS * 14; // Just some large enough value
		if (d > max_d) {
			logger.notice("Player %s tried to access %s"
					" from too far: d=%f, max_d=%f. ignoring.",
					player->getName(), pointed.dump().c_str(), d, max_d);
			// Re-send block to revert change on client-side
			RemoteClient *client = getClient(pkt->getPeerId());
			v3s16 blockpos = getNodeBlockPos(floatToInt(pointed_pos_under, BS));
			client->SetBlockNotSent(blockpos);
			// Call callbacks
			m_script->on_cheat(playersao, "interacted_too_far");
			// Do nothing else
			return;
		}
	}

	/*
		Make sure the player is allowed to do it
	*/
	if (!playersao->hasPriv("interact")) {
		logger.notice("%s attempted to interact with "
				"%s without 'interact' privilege",
				player->getName(), pointed.dump().c_str());
		// Re-send block to revert change on client-side
		RemoteClient *client = getClient(pkt->getPeerId());
		// Digging completed -> under
		if (action == 2) {
			v3s16 blockpos = getNodeBlockPos(floatToInt(pointed_pos_under, BS));
			client->SetBlockNotSent(blockpos);
		}
		// Placement -> above
		if (action == 3) {
			v3s16 blockpos = getNodeBlockPos(floatToInt(pointed_pos_above, BS));
			client->SetBlockNotSent(blockpos);
		}
		return;
	}

	/*
		0: start digging or punch object
	*/
	if (action == 0) {
		if (pointed.type == POINTEDTHING_NODE) {
			/*
				NOTE: This can be used in the future to check if
				somebody is cheating, by checking the timing.
			*/

			MapNode n(CONTENT_IGNORE);
			bool pos_ok;

			n = m_env->getMap().getNode(p_under, &pos_ok);
			if (!pos_ok) {
				logger.info("Server: Not punching: Node not found. Adding block to emerge queue.");
				m_emerge->enqueueBlockEmerge(pkt->getPeerId(),
					getNodeBlockPos(p_above), false);
			}

			const ContentFeatures &f = m_nodedef->get(n.getContent());
			if (n.getContent() != CONTENT_IGNORE && !m_itemdef->get(f.name).coreside)
				m_script->node_on_punch(p_under, n, playersao, pointed);

			// Cheat prevention
			playersao->noCheatDigStart(p_under);
		}
		else if (pointed.type == POINTEDTHING_OBJECT) {
			// Skip if object has been removed
			if (pointed_object->m_removed)
				return;

			logger.notice("%s punches object %d: %s",
					player->getName(), pointed.object_id,
					pointed_object->getDescription().c_str());

			ItemStack punchitem = playersao->getWieldedItem();
			ToolCapabilities toolcap =
					punchitem.getToolCapabilities(m_itemdef);
			v3f dir = (pointed_object->getBasePosition() -
					(player->getPosition() + playersao->getEyeOffset())
						).normalize();
			float time_from_last_punch =
				playersao->resetTimeFromLastPunch();

			s16 src_original_hp = pointed_object->getHP();
			s16 dst_origin_hp = playersao->getHP();

			pointed_object->punch(dir, &toolcap, playersao,
					time_from_last_punch);

			// If the object is a player and its HP changed
			if (src_original_hp != pointed_object->getHP() &&
					pointed_object->getType() == ACTIVEOBJECT_TYPE_PLAYER) {
				SendPlayerHPOrDie((PlayerSAO *)pointed_object);
			}

			// If the puncher is a player and its HP changed
			if (dst_origin_hp != playersao->getHP())
				SendPlayerHPOrDie(playersao);
		}

	} // action == 0

	/*
		1: stop digging
	*/
	else if (action == 1) {
	} // action == 1

	/*
		2: Digging completed
	*/
	else if (action == 2) {
		// Only digging of nodes
		if (pointed.type == POINTEDTHING_NODE) {
			bool pos_ok;
			MapNode n = m_env->getMap().getNode(p_under, &pos_ok);
			if (!pos_ok) {
				logger.info("Server: Not finishing digging: Node not found. Adding block to emerge queue.");
				m_emerge->enqueueBlockEmerge(pkt->getPeerId(),
					getNodeBlockPos(p_above), false);
			}

			/* Cheat prevention */
			bool is_valid_dig = true;
			if (
#ifndef SERVER
				!isSingleplayer() &&
#endif
				!g_settings->get(BOOLSETTING_DISABLE_ANTICHEAT)) {
				v3s16 nocheat_p = playersao->getNoCheatDigPos();
				float nocheat_t = playersao->getNoCheatDigTime();
				playersao->noCheatDigEnd();
				// If player didn't start digging this, ignore dig
				if (nocheat_p != p_under) {
					logger.info("Server: NoCheat: %sstarted digging (%d,%d,%d) and "
							"completed digging (%d,%d,%d); not digging.",
							player->getName(), nocheat_p.X, nocheat_p.Y, nocheat_p.Z,
							p_under.X, p_under.Y, p_under.Z);
					is_valid_dig = false;
					// Call callbacks
					m_script->on_cheat(playersao, "finished_unknown_dig");
				}
				// Get player's wielded item
				ItemStack playeritem;
				InventoryList *mlist = playersao->getInventory()->getList("main");
				if (mlist != NULL)
					playeritem = mlist->getItem(playersao->getWieldIndex());
				ToolCapabilities playeritem_toolcap =
						playeritem.getToolCapabilities(m_itemdef);
				// Get diggability and expected digging time
				DigParams params = getDigParams(m_nodedef->get(n).groups,
						&playeritem_toolcap);
				// If can't dig, try hand
				if (!params.diggable) {
					const ItemDefinition &hand = m_itemdef->get("");
					const ToolCapabilities *tp = hand.tool_capabilities;
					if (tp)
						params = getDigParams(m_nodedef->get(n).groups, tp);
				}
				// If can't dig, ignore dig
				if (!params.diggable) {
					logger.info("Server: NoCheat: %s completed digging (%d,%d,%d), which is not diggable with tool. not digging.",
							player->getName(), p_under.X, p_under.Y, p_under.Z);
					is_valid_dig = false;
					// Call callbacks
					m_script->on_cheat(playersao, "dug_unbreakable");
				}
				// Check digging time
				// If already invalidated, we don't have to
				if (!is_valid_dig) {
					// Well not our problem then
				}
				// Clean and long dig
				else if (params.time > 2.0 && nocheat_t * 1.2 > params.time) {
					// All is good, but grab time from pool; don't care if
					// it's actually available
					playersao->getDigPool().grab(params.time);
				}
				// Short or laggy dig
				// Try getting the time from pool
				else if (playersao->getDigPool().grab(params.time)) {
					// All is good
				}
				// Dig not possible
				else {
					logger.info("Server: NoCheat: %s completed digging (%d,%d,%d) too fast; not digging.",
							player->getName(), p_under.X, p_under.Y, p_under.Z);
					is_valid_dig = false;
					// Call callbacks
					m_script->on_cheat(playersao, "dug_too_fast");
				}
			}

			/* Actually dig node */

			if (is_valid_dig && n.getContent() != CONTENT_IGNORE) {
				// Add exhaustion on dig
				playersao->addExhaustion(g_settings->get(FSETTING_PLAYER_EXHAUS_ONDIG));
				// Handle coreside nodes which are coming from DB
				const ContentFeatures &f = m_nodedef->get(n.getContent());
				if (m_itemdef->get(f.name).coreside) {
					playersao->onDigNode(p_under, n);
					m_env->nodeUpdate(p_under);
				}
				else {
					m_script->node_on_dig(p_under, n, playersao);
				}
			}

			v3s16 blockpos = getNodeBlockPos(floatToInt(pointed_pos_under, BS));
			RemoteClient *client = getClient(pkt->getPeerId());
			// Send unusual result (that is, node not being removed)
			if (m_env->getMap().getNode(p_under).getContent() != CONTENT_AIR) {
				// Re-send block to revert change on client-side
				client->SetBlockNotSent(blockpos);
			}
			else {
				client->ResendBlockIfOnWire(blockpos);
			}
		}
	} // action == 2

	/*
		3: place block or right-click object
	*/
	else if (action == 3) {
		ItemStack item = playersao->getWieldedItem();

		// Reset build time counter
		if (pointed.type == POINTEDTHING_NODE &&
				item.getDefinition(m_itemdef).type == ITEM_NODE)
			getClient(pkt->getPeerId())->m_time_from_building = 0.0;

		if (pointed.type == POINTEDTHING_OBJECT) {
			// Right click object

			// Skip if object has been removed
			if (pointed_object->m_removed)
				return;

			logger.info("%s right-clicks object %d:%s", player->getName(), pointed.object_id, pointed_object->getDescription().c_str());

			// Do stuff
			pointed_object->rightClick(playersao);
		}
		else if (playersao->onPlaceItem(item, pointed)) {
			m_env->nodeUpdate(p_under);
			// Apply returned ItemStack
			playersao->setWieldedItem(item);
		}
		else if (m_script->item_OnPlace(
				item, playersao, pointed)) {
			// Placement was handled in lua

			// Add exhaustion on place
			playersao->addExhaustion(g_settings->get(FSETTING_PLAYER_EXHAUS_ONPLACE));

			// Apply returned ItemStack
			playersao->setWieldedItem(item);
		}

		// If item has node placement prediction, always send the
		// blocks to make sure the client knows what exactly happened
		RemoteClient *client = getClient(pkt->getPeerId());
		v3s16 blockpos = getNodeBlockPos(floatToInt(pointed_pos_above, BS));
		v3s16 blockpos2 = getNodeBlockPos(floatToInt(pointed_pos_under, BS));
		if (item.getDefinition(m_itemdef).node_placement_prediction != "") {
			client->SetBlockNotSent(blockpos);
			if (blockpos2 != blockpos) {
				client->SetBlockNotSent(blockpos2);
			}
		}
		else {
			client->ResendBlockIfOnWire(blockpos);
			if (blockpos2 != blockpos) {
				client->ResendBlockIfOnWire(blockpos2);
			}
		}
	} // action == 3

	/*
		4: use
	*/
	else if (action == 4) {
		ItemStack item = playersao->getWieldedItem();

		logger.info("%s uses %s, pointing at %s",
				player->getName(), item.name.c_str(), pointed.dump().c_str());

		// Epixel specific
		if (item.getDefinition(m_itemdef).coreside && playersao->onUseItem(item, pointed)) {
			// Apply returned ItemStack
			playersao->setWieldedItem(item);
		}
		else if (m_script->item_OnUse(
				item, playersao, pointed)) {
			// Apply returned ItemStack
			playersao->setWieldedItem(item);
		}

	} // action == 4

	/*
		5: rightclick air
	*/
	else if (action == 5) {
		ItemStack item = playersao->getWieldedItem();

		logger.info("%s activates %s", player->getName(), item.name.c_str());

		if (m_script->item_OnSecondaryUse(
				item, playersao)) {
			if( playersao->setWieldedItem(item)) {
				SendInventory(playersao);
			}
		}
	}


	/*
		Catch invalid actions
	*/
	else {
		logger.info("WARNING: Server: Invalid action %d", action);
	}
}

void Server::handleCommand_RemovedSounds(NetworkPacket* pkt)
{
	u16 num;
	*pkt >> num;
	for (u16 k = 0; k < num; k++) {
		s32 id;

		*pkt >> id;

		std::unordered_map<s32, ServerPlayingSound>::iterator i =
			m_playing_sounds.find(id);

		if (i == m_playing_sounds.end())
			continue;

		ServerPlayingSound &psound = i->second;
		psound.clients.erase(pkt->getPeerId());
		if (psound.clients.empty())
			m_playing_sounds.erase(i++);
	}
}

void Server::handleCommand_NodeMetaFields(NetworkPacket* pkt)
{
	v3s16 p;
	std::string formname;
	u16 num;

	*pkt >> p >> formname >> num;

	StringMap fields;
	for (u16 k = 0; k < num; k++) {
		std::string fieldname;
		*pkt >> fieldname;
		fields[fieldname] = pkt->readLongString();
	}

	RemotePlayer *player = m_env->getPlayer(pkt->getPeerId());
	if (player == NULL) {
		logger.error("Server::ProcessData(): Canceling: No player for peer_id=%d disconnecting peer!",
				pkt->getPeerId());
		m_con.DisconnectPeer(pkt->getPeerId());
		return;
	}

	PlayerSAO *playersao = player->getPlayerSAO();
	if (playersao == NULL) {
		logger.error("Server::ProcessData(): Canceling: No player object for peer_id=%d disconnecting peer!",
				pkt->getPeerId());
		m_con.DisconnectPeer(pkt->getPeerId());
		return;
	}

	m_script->node_on_receive_fields(p, formname, fields, playersao);
}

void Server::handleCommand_InventoryFields(NetworkPacket* pkt)
{
	std::string formname;
	u16 num;

	*pkt >> formname >> num;

	bool field_quit_ok = false;
	StringMap fields;
	for (u16 k = 0; k < num; k++) {
		std::string fieldname;
		*pkt >> fieldname;
		fields[fieldname] = pkt->readLongString();
		if (fieldname.compare("quit") == 0 && fields[fieldname].compare("true") == 0) {
			field_quit_ok = true;
		}
	}

	RemotePlayer *player = m_env->getPlayer(pkt->getPeerId());
	if (player == NULL) {
		logger.error("Server::ProcessData(): Canceling: No player for peer_id=%d disconnecting peer!",
				pkt->getPeerId());
		m_con.DisconnectPeer(pkt->getPeerId());
		return;
	}

	PlayerSAO *playersao = player->getPlayerSAO();
	if (playersao == NULL) {
		logger.error("Server::ProcessData(): Canceling: No player object for peer_id=%d disconnecting peer!",
				pkt->getPeerId());
		m_con.DisconnectPeer(pkt->getPeerId());
		return;
	}

	if (field_quit_ok) {
		// Teleporter handling
		if (formname.compare("teleporter") == 0) {
			// Ignore invalid formspec
			if (fields.find("teleporter_selection") == fields.end()) {
				return;
			}
			m_env->getTeleportMgr()->teleportIfValid(playersao, fields["teleporter_selection"]);
			return;
		}
		else if (formname.compare("rules") == 0) {
			// Ignore invalid formspec
			if (fields.find("rules_magic") == fields.end()) {
				return;
			}
			playersao->tryToAcceptServerRules(fields["rules_magic"]);
			return;
		}
	}

	// We don't know, let LUA handle it
	m_script->on_playerReceiveFields(playersao, formname, fields);
}

void Server::handleCommand_FirstSrp(NetworkPacket* pkt)
{
	RemoteClient* client = getClient(pkt->getPeerId(), CS_Invalid);

	ClientState cstate = client->getState();

	std::string playername = client->getName();

	std::string salt;
	std::string verification_key;

	std::string addr_s = getPeerAddressStr(pkt->getPeerId());
	u8 is_empty;

	*pkt >> salt >> verification_key >> is_empty;

	logger.debug("Server: Got TOSERVER_FIRST_SRP from %s, with is_empty=%d",
		addr_s.c_str(), (int)is_empty);

	// Either this packet is sent because the user is new or to change the password
	if (cstate == CS_HelloSent) {
		if (!client->isMechAllowed(AUTH_MECHANISM_FIRST_SRP)) {
			logger.notice("Server: Client from %s"
					" tried to set password without being "
					"authenticated, or the username being new.", addr_s.c_str());
			DenyAccess(pkt->getPeerId(), SERVER_ACCESSDENIED_UNEXPECTED_DATA);
			return;
		}

		if (is_empty == 1
#ifndef SERVER
				&& !isSingleplayer()
#endif
		) {
			logger.notice("Server: %s supplied empty password from %s", playername.c_str(), addr_s.c_str());
			DenyAccess(pkt->getPeerId(), SERVER_ACCESSDENIED_EMPTY_PASSWORD);
			return;
		}

		std::string initial_ver_key;

		initial_ver_key = encode_srp_verifier(verification_key, salt);
		if (!m_auth_database->userExists(playername)) {
			m_auth_database->createUser(playername, initial_ver_key);
			client->markAsNewPlayer(true);
		}

		acceptAuth(pkt->getPeerId(), false);
	} else {
		if (cstate < CS_SudoMode) {
			logger.info("Server::ProcessData(): Ignoring TOSERVER_FIRST_SRP from %s"
					": Client has wrong state %d.", addr_s.c_str(), cstate);
			return;
		}
		m_clients.event(pkt->getPeerId(), CSE_SudoLeave);
		std::string pw_db_field = encode_srp_verifier(verification_key, salt);
		bool success = m_auth_database->setPassword(playername, pw_db_field);
		if (success) {
			logger.notice("%s changes password", playername.c_str());
			SendChatMessage(pkt->getPeerId(), L"Password change successful.");
		} else {
			logger.notice("%s tries to change password but it fails.", playername.c_str());
			SendChatMessage(pkt->getPeerId(), L"Password change failed or unavailable.");
		}
	}
}

void Server::handleCommand_SrpBytesA(NetworkPacket* pkt)
{
	RemoteClient* client = getClient(pkt->getPeerId(), CS_Invalid);
	ClientState cstate = client->getState();

	bool wantSudo = (cstate == CS_Active);

	if (!((cstate == CS_HelloSent) || (cstate == CS_Active))) {
		logger.notice("Server: got SRP _A packet in wrong state %d from %s. Ignoring.",
			cstate, getPeerAddressStr(pkt->getPeerId()).c_str());
		return;
	}

	if (client->chosen_mech != AUTH_MECHANISM_NONE) {
		logger.notice("Server: got SRP _A packet, while auth"
			"is already going on with mech %d from %s"
			" from %s (wantSudo=%d). Ignoring.",
			client->chosen_mech, getPeerAddressStr(pkt->getPeerId()).c_str(),
			wantSudo);
		if (wantSudo) {
			DenySudoAccess(pkt->getPeerId());
			return;
		} else {
			DenyAccess(pkt->getPeerId(), SERVER_ACCESSDENIED_UNEXPECTED_DATA);
			return;
		}
	}

	std::string bytes_A;
	u8 based_on;
	*pkt >> bytes_A >> based_on;

	logger.info("Server: TOSERVER_SRP_BYTES_A received with based_on=%d and len_A=%d.",
				based_on, bytes_A.length());

	AuthMechanism chosen = (based_on == 0) ?
		AUTH_MECHANISM_LEGACY_PASSWORD : AUTH_MECHANISM_SRP;

	if (wantSudo) {
		if (!client->isSudoMechAllowed(chosen)) {
			logger.notice("Server: Player \"%s\" at %s"
				" tried to change password using unallowed mech %d",
				client->getName().c_str(), getPeerAddressStr(pkt->getPeerId()).c_str(), chosen);
			DenySudoAccess(pkt->getPeerId());
			return;
		}
	} else {
		if (!client->isMechAllowed(chosen)) {
			logger.notice("Server: Client tried to authenticate from %s"
				" using unallowed mech %d.", getPeerAddressStr(pkt->getPeerId()).c_str(), chosen);
			DenyAccess(pkt->getPeerId(), SERVER_ACCESSDENIED_UNEXPECTED_DATA);
			return;
		}
	}

	client->chosen_mech = chosen;

	std::string salt;
	std::string verifier;

	if (based_on == 0) {

		generate_srp_verifier_and_salt(client->getName(), client->enc_pwd,
			&verifier, &salt);
	} else if (!decode_srp_verifier_and_salt(client->enc_pwd, &verifier, &salt)) {
		// Non-base64 errors should have been catched in the init handler
		logger.notice("Server: User %s"
			" tried to log in, but srp verifier field"
			" was invalid (most likely invalid base64).", client->getName().c_str());
		DenyAccess(pkt->getPeerId(), SERVER_ACCESSDENIED_SERVER_FAIL);
		return;
	}

	char *bytes_B = 0;
	size_t len_B = 0;

	client->auth_data = srp_verifier_new(SRP_SHA256, SRP_NG_2048,
		client->getName().c_str(),
		(const unsigned char *) salt.c_str(), salt.size(),
		(const unsigned char *) verifier.c_str(), verifier.size(),
		(const unsigned char *) bytes_A.c_str(), bytes_A.size(),
		NULL, 0,
		(unsigned char **) &bytes_B, &len_B, NULL, NULL);

	if (!bytes_B) {
		logger.notice("Server: User %s tried to log in, SRP-6a safety check violated in _A handler.",
			client->getName().c_str());
		if (wantSudo) {
			DenySudoAccess(pkt->getPeerId());
			return;
		} else {
			DenyAccess(pkt->getPeerId(), SERVER_ACCESSDENIED_UNEXPECTED_DATA);
			return;
		}
	}

	NetworkPacket resp_pkt(TOCLIENT_SRP_BYTES_S_B, 0, pkt->getPeerId());
	resp_pkt << salt << std::string(bytes_B, len_B);
	Send(&resp_pkt);
}

void Server::handleCommand_SrpBytesM(NetworkPacket* pkt)
{
	RemoteClient* client = getClient(pkt->getPeerId(), CS_Invalid);
	ClientState cstate = client->getState();

	bool wantSudo = (cstate == CS_Active);

	logger.debug("Server: Recieved TOCLIENT_SRP_BYTES_M.");

	if (!((cstate == CS_HelloSent) || (cstate == CS_Active))) {
		logger.notice("Server: got SRP _M packet in wrong state %d from %s. Ignoring.",
			cstate, getPeerAddressStr(pkt->getPeerId()).c_str());
		return;
	}

	if ((client->chosen_mech != AUTH_MECHANISM_SRP)
		&& (client->chosen_mech != AUTH_MECHANISM_LEGACY_PASSWORD)) {
		logger.notice("Server: got SRP _M packet, while auth"
			"is going on with mech %d from %s"
			" (wantSudo=%d). Denying.",
			client->chosen_mech, getPeerAddressStr(pkt->getPeerId()).c_str(),
			wantSudo);
		if (wantSudo) {
			DenySudoAccess(pkt->getPeerId());
			return;
		} else {
			DenyAccess(pkt->getPeerId(), SERVER_ACCESSDENIED_UNEXPECTED_DATA);
			return;
		}
	}

	std::string bytes_M;
	*pkt >> bytes_M;

	if (srp_verifier_get_session_key_length((SRPVerifier *) client->auth_data)
			!= bytes_M.size()) {
		logger.notice("Server: User %s at %s sent bytes_M with invalid length %d",
			client->getName().c_str(), getPeerAddressStr(pkt->getPeerId()).c_str(),
			bytes_M.size());
		DenyAccess(pkt->getPeerId(), SERVER_ACCESSDENIED_UNEXPECTED_DATA);
		return;
	}

	unsigned char *bytes_HAMK = 0;

	srp_verifier_verify_session((SRPVerifier *) client->auth_data,
		(unsigned char *)bytes_M.c_str(), &bytes_HAMK);

	if (!bytes_HAMK
#ifndef SERVER
		&& !m_singleplayer_mode
#endif
		) {
		if (wantSudo) {
			logger.notice("Server: User %s at %s"
				" tried to change their password, but supplied wrong"
				" (SRP) password for authentication.",
				client->getName().c_str(), getPeerAddressStr(pkt->getPeerId()).c_str());
			DenySudoAccess(pkt->getPeerId());
			return;
		} else {
			logger.notice("Server: User %s at %s supplied wrong (SRP) password.",
				client->getName().c_str(), getPeerAddressStr(pkt->getPeerId()).c_str());
			DenyAccess(pkt->getPeerId(), SERVER_ACCESSDENIED_WRONG_PASSWORD);
			return;
		}
	}

	if (client->create_player_on_auth_success) {
		std::string playername = client->getName();
		m_auth_database->createUser(playername, client->enc_pwd);
		client->markAsNewPlayer(true);

		if (!m_auth_database->loadUserAndVerifyPassword(playername, client->enc_pwd)) {
			logger.notice("Server: %s cannot be authenticated"
				" (auth handler does not work?)", playername.c_str());
			DenyAccess(pkt->getPeerId(), SERVER_ACCESSDENIED_SERVER_FAIL);
			return;
		}
		client->create_player_on_auth_success = false;
	}

	acceptAuth(pkt->getPeerId(), wantSudo);
}
