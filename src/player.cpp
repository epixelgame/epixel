/*
Minetest
Copyright (C) 2010-2013 celeron55, Perttu Ahola <celeron55@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "player.h"

#include <fstream>
#include "util/numeric.h"
#include "hud.h"
#include "constants.h"
#include "gamedef.h"
#include "settings.h"
#include "content_sao.h"
#include "filesys.h"
#include "log.h"
#include "porting.h"  // strlcpy
#include "contrib/playersao.h"


Player::Player(const char *name):
	keyPressed(0),
// protected
	m_position(0,0,0),
	m_hud(NULL)
{
	strlcpy(m_name, name, PLAYERNAME_SIZE);

	// Can be redefined via Lua
	inventory_formspec = "size[8,7.5]"
		//"image[1,0.6;1,2;player.png]"
		"list[current_player;main;0,3.5;8,4;]"
		"list[current_player;craft;3,0;3,3;]"
		"listring[]"
		"list[current_player;craftpreview;7,1;1,1;]";

	// Initialize movement settings at default values, so movement can work
	// if the server fails to send them
	movement_speed_walk             = 4    * BS;
	movement_speed_fast             = 20   * BS;

	hud_flags =
		HUD_FLAG_HOTBAR_VISIBLE    | HUD_FLAG_HEALTHBAR_VISIBLE |
		HUD_FLAG_CROSSHAIR_VISIBLE | HUD_FLAG_WIELDITEM_VISIBLE |
		HUD_FLAG_BREATHBAR_VISIBLE | HUD_FLAG_MINIMAP_VISIBLE;

	hud_hotbar_itemcount = HUD_HOTBAR_ITEMCOUNT_DEFAULT;

	hud.clear();
}

Player::~Player()
{
	delete m_hud;
	hud.clear();
}

u32 Player::addHud(HudElement *toadd)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	u32 id = hud.size();
	hud[id] = toadd;

	return id;
}

HudElement* Player::getHud(u32 id)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	std::unordered_map<u32, HudElement*>::iterator it = hud.find(id);
	if (it == hud.end()) {
		return NULL;
	}

	return it->second;
}

bool Player::removeHud(u32 id)
{
	std::lock_guard<std::mutex> lock(m_mutex);

	if (id >= hud.size()) {
		return false;
	}

	delete hud[id];
	hud[id] = NULL;

	return true;
}

void Player::clearHud()
{
	std::lock_guard<std::mutex> lock(m_mutex);
	for (auto &elem: hud) {
		delete elem.second;
	}

	hud.clear();
}

const u16 RemotePlayer::getPeerID() const
{
	return (m_sao ? m_sao->getPeerID() : PEER_ID_INEXISTENT);
}

bool RemotePlayer::isDead() const
{
	return (m_sao && m_sao->getHP() == 0);
}

void RemotePlayer::initHuds()
{
	clearHud();

	delete m_hud;

	m_hud = new PlayerHud();

	HudElement* he = new HudElement();
	he->type = HUD_ELEM_STATBAR;
	he->pos = v2f(0.5, 1);
	he->size = v2s32(24, 24);
	he->name   = "life_bar";
	he->text   = "hud_heart_bg.png";
	he->number = 20;
	he->dir    = 0;
	he->offset = v2f(-262, -92);
	he->align = v2f(-1 -1);
	m_hud->setHud(PLAYERHUD_LIFE_BG, he);

	he = new HudElement();
	he->type = HUD_ELEM_STATBAR;
	he->pos = v2f(0.5, 1);
	he->size = v2s32(24, 24);
	he->name   = "life_bar";
	he->text   = "hud_heart_fg.png";
	he->number = getPlayerSAO()->getHP();
	he->dir    = 0;
	he->offset = v2f(-262, -92);
	he->align = v2f(-1 -1);
	m_hud->setHud(PLAYERHUD_LIFE_FG, he);

	if (g_settings->get(BOOLSETTING_ENABLE_MOD_MANA)) {
		he = new HudElement();
		he->type = HUD_ELEM_STATBAR;
		he->pos = v2f(0.5, 1);
		he->size = v2s32(24, 24);
		he->name   = "life_bar";
		he->text   = "hud_mana_bg.png";
		he->number = 20;
		he->dir    = 0;
		he->offset = v2f(-262, -115);
		he->align = v2f(-1 -1);
		m_hud->setHud(PLAYERHUD_MANA_BG, he);

		he = new HudElement();
		he->type = HUD_ELEM_STATBAR;
		he->pos = v2f(0.5, 1);
		he->size = v2s32(24, 24);
		he->name   = "life_bar";
		he->text   = "hud_mana_fg.png";
		he->number = getPlayerSAO()->getMana() / 10;
		he->dir    = 0;
		he->offset = v2f(-262, -115);
		he->align = v2f(-1 -1);
		m_hud->setHud(PLAYERHUD_MANA_FG, he);
	}

	he = new HudElement();
	he->type = HUD_ELEM_STATBAR;
	he->pos = v2f(0.5, 1);
	he->size = v2s32(24, 24);
	he->name   = "breath_bar";
	he->text   = "hud_air_fg.png";
	he->number = 0;
	he->dir    = 0;

	he->offset = v2f(-262, -133);

	he->align = v2f(-1, -1);
	m_hud->setHud(PLAYERHUD_BREATH, he);

	he = new HudElement();
	he->type = HUD_ELEM_TEXT;
	he->pos = v2f(0, 1);
	he->scale = v2f(200, 60);
	he->size = v2s32();
	he->name   = "Areas";
	he->text   = "Areas:";
	he->number = 0xFFFFFF;
	he->item   = 0;
	he->dir    = 0;
	he->align = v2f(1, -1);
	he->offset = v2f(8, -8);
	he->world_pos = v3f();
	m_hud->setHud(PLAYERHUD_AREAS, he);

	// Hunger background
	he = new HudElement();
	he->type = HUD_ELEM_STATBAR;
	he->text = "hud_hunger_bg.png";
	he->name = "hunger_bg";
	he->pos = v2f(0.5, 1);
	he->size = v2s32(24, 24);
	he->number = 20;
	he->align = v2f(-1, -1);
	he->offset = v2f(15, -92);
	m_hud->setHud(PLAYERHUD_HUNGER_BG, he);

	// Hunger foreground
	he = new HudElement();
	he->type = HUD_ELEM_STATBAR;
	he->text   = "hud_hunger_fg.png";
	he->name = "hunger_fg";
	he->pos = v2f(0.5, 1);
	he->size = v2s32(24, 24);
	he->number = getPlayerSAO()->getHunger();
	he->align = v2f(-1, -1);
	he->offset = v2f(15, -92);
	m_hud->setHud(PLAYERHUD_HUNGER_FG, he);

	// Armor background
	he = new HudElement();
	he->type = HUD_ELEM_STATBAR;
	he->text   = "hud_armor_bg.png";
	he->name = "armor_bg";
	he->pos = v2f(0.5, 1);
	he->size = v2s32(24, 24);
	he->number = 0;
	he->align = v2f(-1, -1);

	he->offset = v2f(15, -110);

	m_hud->setHud(PLAYERHUD_ARMOR_BG, he);

	he = new HudElement();
	he->type = HUD_ELEM_STATBAR;
	he->text   = "hud_armor_fg.png";
	he->name = "armor_fg";
	he->pos = v2f(0.5, 1);
	he->size = v2s32(24, 24);
	he->number = 0;
	he->align = v2f(-1, -1);

	he->offset = v2f(15, -110);

	m_hud->setHud(PLAYERHUD_ARMOR_FG, he);

	std::stringstream ss;
	ss << "Account: $" << m_sao->getMoney();
	he = new HudElement();
	he->type = HUD_ELEM_TEXT;
	he->text   = ss.str();
	he->name = "money";
	he->pos = v2f(1, 1);
	he->number = 0xFFFFFF;
	he->align = v2f(-1.2, -1);
	he->offset = v2f(-8, -8);
	he->scale = v2f(200, 60);
	m_hud->setHud(PLAYERHUD_MONEY, he);

	if (g_settings->get(BOOLSETTING_ENABLE_MOD_XP)) {
		std::stringstream lvl;
		lvl << "Lvl: " << floor(getPlayerSAO()->getLvl());
		he = new HudElement();
		he->type = HUD_ELEM_TEXT;
		he->text   = lvl.str();
		he->name = "lvl";
		he->pos = v2f(1, 1);
		he->number = 0xFFFFFF;
		he->align = v2f(-1.2, -1);
		he->offset = v2f(-56, -24);
		he->scale = v2f(200, 60);
		m_hud->setHud(PLAYERHUD_LVL, he);

		he = new HudElement();
		he->type = HUD_ELEM_STATBAR;
		he->pos = v2f(0.5, 1);
		he->size = v2s32(24, 24);
		he->name   = "lvl_bar";
		he->text   = "hud_xp_bg.png";
		he->number = 37;
		he->dir    = 0;
		he->offset = v2f(-225, -77);
		he->align = v2f(-1 -1);
		m_hud->setHud(PLAYERHUD_XP_BG, he);

		he = new HudElement();
		he->type = HUD_ELEM_STATBAR;
		he->pos = v2f(0.5, 1);
		he->size = v2s32(24, 24);
		he->name   = "lvl_bar";
		he->text   = "hud_xp_fg.png";
		double intpart;
		he->number = modf(getPlayerSAO()->getLvl(), &intpart)*37;
		he->dir    = 0;
		he->offset = v2f(-225, -77);
		he->align = v2f(-1 -1);
		m_hud->setHud(PLAYERHUD_XP_FG, he);
	}
}

/*
	RemotePlayer
*/
void RemotePlayer::setPosition(const v3f &position)
{
	if (position != m_position)
		m_dirty = true;

	Player::setPosition(position);

	if(m_sao)
		m_sao->setBasePosition(position);
}

bool RemotePlayer::checkModified() const
{
	return m_dirty || (m_sao && m_sao->getInventory()->checkModified());
}

void RemotePlayer::setModified(const bool x)
{
	m_dirty = x;
	if (!x && m_sao)
		m_sao->getInventory()->setModified(x);
}

PlayerHud::~PlayerHud()
{
	for (auto &elem: m_huds) {
		delete elem;
	}
}

void PlayerHud::setHud(PlayerHudIdx idx, HudElement* el)
{
	assert(idx < PLAYERHUD_MAX);
	m_mutex.lock();
	m_huds[idx] = el;
	m_mutex.unlock();
}

RemotePlayer::RemotePlayer(const char *name):
	Player(name),
	m_sao(NULL),
	m_dirty(false)
{
	m_sky_type = "";
	m_sky_params.clear();
}

HudElement* PlayerHud::getHud(PlayerHudIdx idx)
{
	assert(idx < PLAYERHUD_MAX);
	m_mutex.lock();
	HudElement* el = m_huds[idx];
	m_mutex.unlock();
	return el;
}

void PlayerHud::setHudId(PlayerHudIdx idx, u32 id) {
	assert(idx < PLAYERHUD_MAX);
	m_mutex.lock();
	m_hudids[idx] = id;
	m_mutex.unlock();
}

u32 PlayerHud::getHudId(PlayerHudIdx idx)
{
	assert(idx < PLAYERHUD_MAX);
	m_mutex.lock();
	u32 id = m_hudids[idx];
	m_mutex.unlock();
	return id;
}

