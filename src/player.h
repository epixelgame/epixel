/*
Minetest
Copyright (C) 2010-2013 celeron55, Perttu Ahola <celeron55@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef PLAYER_HEADER
#define PLAYER_HEADER

#include "irrlichttypes_bloated.h"
#include "inventory.h"
#include "constants.h" // BS
#include <mutex>
#include <list>
#include <unordered_map>

#define PLAYERNAME_SIZE 20

#define PLAYER_HUNGER_MAX 20
#define PLAYER_HUNGER_CHECK_TIMER 4

#define PLAYER_EXHAUST_LVL 160

enum PlayerAnimState
{
	PLAYERANIMSTATE_NORMAL,
	PLAYERANIMSTATE_LAY,
	PLAYERANIMSTATE_SIT,
};

#define PLAYERNAME_ALLOWED_CHARS "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_"

struct PlayerControl
{
	PlayerControl()
	{
		up = false;
		down = false;
		left = false;
		right = false;
		jump = false;
		aux1 = false;
		sneak = false;
		LMB = false;
		RMB = false;
		pitch = 0;
		yaw = 0;
	}
	PlayerControl(
		bool a_up,
		bool a_down,
		bool a_left,
		bool a_right,
		bool a_jump,
		bool a_aux1,
		bool a_sneak,
		bool a_LMB,
		bool a_RMB,
		float a_pitch,
		float a_yaw
	)
	{
		up = a_up;
		down = a_down;
		left = a_left;
		right = a_right;
		jump = a_jump;
		aux1 = a_aux1;
		sneak = a_sneak;
		LMB = a_LMB;
		RMB = a_RMB;
		pitch = a_pitch;
		yaw = a_yaw;
	}
	bool up;
	bool down;
	bool left;
	bool right;
	bool jump;
	bool aux1;
	bool sneak;
	bool LMB;
	bool RMB;
	float pitch;
	float yaw;
};

class Map;
class IGameDef;
struct CollisionInfo;
class PlayerSAO;
struct HudElement;
class Environment;
class RemoteClient;

enum PlayerHudIdx
{
	PLAYERHUD_INVALID = 0,
	PLAYERHUD_LIFE_BG = 1,
	PLAYERHUD_LIFE_FG = 2,
	PLAYERHUD_MANA_BG = 3,
	PLAYERHUD_MANA_FG = 4,
	PLAYERHUD_BREATH = 5,
	PLAYERHUD_AREAS = 6,
	PLAYERHUD_HUNGER_BG = 7,
	PLAYERHUD_HUNGER_FG = 8,
	PLAYERHUD_ARMOR_BG = 9,
	PLAYERHUD_ARMOR_FG = 10,
	PLAYERHUD_MONEY = 11,
	PLAYERHUD_LVL = 12,
	PLAYERHUD_XP_BG = 13,
	PLAYERHUD_XP_FG = 14,
	PLAYERHUD_MAX = 15
};

class PlayerHud
{
public:
	PlayerHud()
	{
		for (u8 i=0; i < PLAYERHUD_MAX; i++) {
			m_huds[i] = NULL;
			m_hudids[i] = (u32) 0;
		}
	}
	~PlayerHud();

	void setHud(PlayerHudIdx idx, HudElement* el);
	HudElement* getHud(PlayerHudIdx idx);
	void setHudId(PlayerHudIdx idx, u32 id);
	u32 getHudId(PlayerHudIdx idx);

private:
	HudElement* m_huds[PLAYERHUD_MAX];
	u32 m_hudids[PLAYERHUD_MAX];
	std::mutex m_mutex;
};

// IMPORTANT:
// Do *not* perform an assignment or copy operation on a Player or
// RemotePlayer object!  This will copy the lock held for HUD synchronization
class Player
{
public:

	Player(const char *name);
	virtual ~Player() = 0;

	virtual void move(f32 dtime, Environment *env, f32 pos_max_d)
	{}
	virtual void move(f32 dtime, Environment *env, f32 pos_max_d,
			std::vector<CollisionInfo> *collision_info)
	{}

	v3f getPosition()
	{
		return m_position;
	}

	virtual void setPosition(const v3f &position) { m_position = position; }

	const char *getName() const
	{
		return m_name;
	}

	void setHotbarItemcount(s32 hotbar_itemcount)
	{
		hud_hotbar_itemcount = hotbar_itemcount;
	}

	virtual bool isLocal() const { return false; }

	v3f eye_offset_first;
	v3f eye_offset_third;

	f32 movement_speed_walk;
	f32 movement_speed_fast;

	std::string inventory_formspec;

	PlayerControl control;
	PlayerControl getPlayerControl()
	{
		return control;
	}

	u32 keyPressed;

	HudElement* getHud(u32 id);
	u32 addHud(HudElement* hud);
	bool removeHud(u32 id);
	void clearHud();
	inline u32 maxHudId() {
		std::lock_guard<std::mutex> lock(m_mutex);
		return hud.size();
	}

	u32 hud_flags;
	s32 hud_hotbar_itemcount;
	virtual const u16 getPeerID() const = 0;
protected:
	char m_name[PLAYERNAME_SIZE];
	v3f m_position;

	std::unordered_map<u32, HudElement *> hud;
	PlayerHud* m_hud;
private:
	// Protect some critical areas
	// hud for example can be modified by EmergeThread
	// and ServerThread
	std::mutex m_mutex;
};


/*
	Player on the server
*/
class RemotePlayer : public Player
{
public:
	RemotePlayer(const char *name);
	virtual ~RemotePlayer() {}

	PlayerSAO *getPlayerSAO()
	{ return m_sao; }
	void setPlayerSAO(PlayerSAO *sao)
	{ m_sao = sao; }
	void setPosition(const v3f &position);

	void initHuds();
	PlayerHud* getHuds() { return m_hud; }

	const u16 getPeerID() const;

	// Use a function, if isDead can be defined by other conditions
	bool isDead() const;

	void overrideDayNightRatio(bool do_override, float ratio)
	{
		m_day_night_ratio_do_override = do_override;
		m_day_night_ratio = ratio;
	}

	void getDayNightRatio(bool *do_override, float *ratio)
	{
		*do_override = m_day_night_ratio_do_override;
		*ratio = m_day_night_ratio;
	}

	void setSky(const video::SColor &bgcolor, const std::string &type,
		const std::vector<std::string> &params)
	{
		m_sky_bgcolor = bgcolor;
		m_sky_type = type;
		m_sky_params = params;
	}

	void getSky(video::SColor *bgcolor, std::string *type,
		std::vector<std::string> *params)
	{
		*bgcolor = m_sky_bgcolor;
		*type = m_sky_type;
		*params = m_sky_params;
	}

	inline void setDirty() { m_dirty = true; }

	bool checkModified() const;
	void setModified(const bool x);

	void setHotbarImage(const std::string &name) { m_hud_hotbar_image = name; }
	std::string getHotbarImage() { return m_hud_hotbar_image; }

	void setHotbarSelectedImage(const std::string &name) { m_hud_hotbar_selected_image = name; }
	std::string getHotbarSelectedImage() { return m_hud_hotbar_selected_image; }

	const s32 getHotbarItemcount() { return hud_hotbar_itemcount; }

	inline RemoteClient* getRemoteClient() const { return m_remote_client; }
	inline void setRemoteClient(RemoteClient* iface) { m_remote_client = iface; }

	inline const u16 getProtocolVersion() const { return m_protocol_version; }
	inline void setProtocolVersion(const u16 p) { m_protocol_version = p; }

	inline const u32 getDBId() const { return m_db_id; }
	inline void setDBId(const u32 id) { m_db_id = id; }
private:
	PlayerSAO *m_sao = nullptr;
	RemoteClient* m_remote_client = nullptr;
	u16 m_protocol_version = 0;
	u32 m_db_id = 0;

	bool m_day_night_ratio_do_override;
	float m_day_night_ratio;

	std::string m_sky_type;
	video::SColor m_sky_bgcolor;
	std::vector<std::string> m_sky_params;

	std::string m_hud_hotbar_image;
	std::string m_hud_hotbar_selected_image;

	bool m_dirty;
};

#endif

