/*
Minetest
Copyright (C) 2010-2013 celeron55, Perttu Ahola <celeron55@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "server.h"
#include <future>
#include <openssl/sha.h>
#include "contrib/util/base64.h"
#include "network/serveropcodes.h"
#include "version.h"
#include "filesys.h"
#include "database.h"
#include "serverobject.h"
#include "genericobject.h"
#include "scripting_game.h"
#include "craftdef.h"
#include "content_nodemeta.h"
#include "content_abm.h"
#include "content_sao.h"
#include "event_manager.h"
#include "serverlist.h"
#include "util/serialize.h"
#include "defaultsettings.h"
#include "emerge.h"
#include "mapblock.h"
#include "util/hex.h"
#include "contrib/areas.h"
#include "contrib/ban.h"
#include "contrib/chathandler.h"
#include "contrib/creature.h"
#include "contrib/playersao.h"
#include "contrib/network/networkserver.h"
#include "contrib/stats.h"
#include "httpfetch.h"
#ifndef SERVER
#include "client/database-sqlite3.h"
#else
#include "server/database-postgresql.h"
#include "contrib/is_chatclient.h"
#endif

class ClientNotFoundException : public BaseException
{
public:
	ClientNotFoundException(const char *s):
		BaseException(s)
	{}
};

class ServerThread : public Thread
{
	Server *m_server;

public:

	ServerThread(Server *server):
		Thread(),
		m_server(server)
	{
	}

	void * run();
};

void *ServerThread::run()
{
	BEGIN_DEBUG_EXCEPTION_HANDLER

	m_server->AsyncRunStep(true);

	ThreadStarted();

	porting::setThreadName("ServerThread");

	while (!stopRequested()) {
		try {
			m_server->AsyncRunStep();

			m_server->Receive();

		} catch (con::NoIncomingDataException &) {
		} catch (con::PeerNotFoundException &) {
			logger.info("Server: PeerNotFoundException");
		} catch (ClientNotFoundException &e) {
		} catch (con::ConnectionBindFailed &e) {
			m_server->setAsyncFatalError(e.what());
		} catch (LuaError &e) {
			m_server->setAsyncFatalError("Lua: " + std::string(e.what()));
		}
	}

	END_DEBUG_EXCEPTION_HANDLER()

	return NULL;
}

v3f ServerSoundParams::getPos(ServerEnvironment *env, bool *pos_exists) const
{
	if(pos_exists) *pos_exists = false;
	switch(type){
	case SSP_LOCAL:
		return v3f(0,0,0);
	case SSP_POSITIONAL:
		if(pos_exists) *pos_exists = true;
		return pos;
	case SSP_OBJECT: {
		if(object == 0)
			return v3f(0,0,0);
		ServerActiveObject *sao = env->getActiveObject(object);
		if(!sao)
			return v3f(0,0,0);
		if(pos_exists) *pos_exists = true;
		return sao->getBasePosition(); }
	}
	return v3f(0,0,0);
}

static const float map_timer_and_unload_dtime = 2.92;

/*
	Server
*/

Server::Server(
		const std::string &path_world,
		const SubgameSpec &gamespec,
#ifndef SERVER
		bool simple_singleplayer_mode,
#endif
		bool ipv6
	):
	m_path_world(path_world),
	m_gamespec(gamespec),
#ifndef SERVER
	m_singleplayer_mode(simple_singleplayer_mode),
#endif
	m_async_fatal_error(""),
	m_env(NULL),
	m_con(PROTOCOL_ID,
			512,
			CONNECTION_TIMEOUT,
			ipv6,
			this,
#ifndef SERVER
			simple_singleplayer_mode),
#else
			false),
#endif
	m_emerge(NULL),
	m_script(NULL),
	m_itemdef(createItemDefManager()),
	m_nodedef(createNodeDefManager()),
	m_craftdef(createCraftDefManager()),
	m_event(new EventManager()),
	m_thread(NULL),
	m_time_of_day_send_timer(0),
	m_uptime(0),
	m_clients(&m_con),
	m_shutdown_requested(false),
	m_shutdown_ask_reconnect(false),
	m_ignore_map_edit_events(false),
	m_ignore_map_edit_events_peer_id(0),
	m_next_sound_id(0),
	m_automated_msg_timer(30),
	contrib_last_msgid(0)
{
	m_liquid_transform_timer = 0.0;
	m_masterserver_timer = 0.0;
	m_emergethread_trigger_timer = 0.0;
	m_savemap_timer = 0.0;

	m_step_dtime = 0.0;
	m_lag = g_settings->get(FSETTING_DEDICATED_SERVER_STEP);

	m_save_meta_interval = g_settings->get(U16SETTING_SAVE_META_INTERVAL);
	m_map_timer_and_unload_interval = map_timer_and_unload_dtime;

	if(path_world == "")
		throw ServerError("Supplied empty world path");

	if(!gamespec.isValid())
		throw ServerError("Supplied invalid gamespec");

	logger.info("Server created for gameid \"%s\"", m_gamespec.id.c_str());
#ifndef SERVER
	if(m_singleplayer_mode)
		logger.info("\t- in simple singleplayer mode");
#endif
	logger.info("\t- world:  %s", m_path_world.c_str());
	logger.info("- game: %s", m_gamespec.path.c_str());

	// Create world if it doesn't exist
	if(!loadGameConfAndInitWorld(m_path_world, m_gamespec))
		throw ServerError("Failed to initialize world");

	// Create server thread
	m_thread = new ServerThread(this);
	m_epixel_con = nullptr;

	// Create emerge manager
	m_emerge = new EmergeManager(this);

	ModConfiguration modconf(m_path_world);
	m_mods = modconf.getMods();
	std::vector<ModSpec> unsatisfied_mods = modconf.getUnsatisfiedMods();
	// complain about mods with unsatisfied dependencies
	if(!modconf.isConsistent()) {
		for(std::vector<ModSpec>::iterator it = unsatisfied_mods.begin();
			it != unsatisfied_mods.end(); ++it) {
			ModSpec mod = *it;
			logger.errorStream() << "mod \"" << mod.name << "\" has unsatisfied dependencies: ";
			for(std::set<std::string>::iterator dep_it = mod.unsatisfied_depends.begin();
				dep_it != mod.unsatisfied_depends.end(); ++dep_it)
				logger.errorStream() << " \"" << *dep_it << "\"";
			logger.errorStream() << "\n";
		}
	}

	Settings worldmt_settings;
	std::string worldmt = m_path_world + DIR_DELIM + "world.mt";
	worldmt_settings.readConfigFile(worldmt.c_str());
	std::vector<std::string> names = worldmt_settings.getNames();
	std::set<std::string> load_mod_names;
	for(std::vector<std::string>::iterator it = names.begin();
		it != names.end(); ++it) {
		std::string name = *it;
		if(name.compare(0,9,"load_mod_")==0 && worldmt_settings.get(name) == "true")
			load_mod_names.insert(name.substr(9));
	}

	// complain about mods declared to be loaded, but not found
	for(const auto& m: m_mods)
		load_mod_names.erase(m.name);

	for(const auto& m: unsatisfied_mods)
		load_mod_names.erase(m.name);

	if(!load_mod_names.empty()) {
		logger.errorStream() << "The following mods could not be found:";
		for(std::set<std::string>::iterator it = load_mod_names.begin();
			it != load_mod_names.end(); ++it)
			logger.errorStream() << " \"" << (*it) << "\"";
		logger.errorStream() << "\n";
	}

	// Lock environment
	std::lock_guard<std::mutex> envlock(m_map_mutex);

	// Load mapgen params from Settings
	m_emerge->loadMapgenParams();

	// Determine which database backend to use
	std::string conf_path = path_world + DIR_DELIM + "database.conf";
	Settings conf;
	conf.readConfigFile(conf_path.c_str());
#ifndef SERVER
	m_map_database = new epixel::DatabasePool<Database_SQLite3>(1, path_world, "map");
	m_game_database = new Database_SQLite3(path_world, "game");
	m_auth_database = new Database_SQLite3(path_world, "auth");
#else
	m_map_database = new epixel::DatabasePool<Database_PostgreSQL>(g_settings->get(U16SETTING_MAPDB_POOL_SIZE),
			g_settings->get("pg_mapdb_connection"), "map");
	m_game_database = new Database_PostgreSQL(g_settings->get("pg_gamedb_connection"), "game");
	m_auth_database = new Database_PostgreSQL(g_settings->get("pg_authdb_connection"), "auth");
#endif
	// Create auth_database before game
	m_auth_database->precheck();
	m_auth_database->init_worker();

	m_game_database->precheck();
	m_game_database->init_worker();

	if (!conf.updateConfigFile(conf_path.c_str())) {
		logger.error("ServerMap::ServerMap(): Failed to update world.mt!");
	}

	m_area_mgr = new epixel::AreaMgr(this, path_world, m_game_database);
	m_ban_mgr = new epixel::BanMgr(m_game_database);
	m_creaturestore = new epixel::CreatureStore(m_game_database);

	// Create the Map (loads map_meta.txt, overriding configured mapgen params)
	ServerMap *servermap = new ServerMap(path_world, this, m_emerge, m_map_database);

	m_area_mgr->Load();
	m_ban_mgr->Load();

	// Initialize scripting
	logger.info("Server: Initializing Lua");

	m_script = new GameScripting(this);

	const static std::string script_path = getBuiltinLuaPath() + DIR_DELIM + "init.lua";
	std::string error_msg;

	if (!m_script->loadMod(script_path, BUILTIN_MOD_NAME, &error_msg))
		throw ModError("Failed to load and run " + script_path
				+ "\nError from Lua:\n" + error_msg);

	// Nodes should be loaded at the beginning but after m_script
	m_game_database->loadItemDefinitions(m_itemdef, m_nodedef, m_script);

	// Print mods
	logger.infoStream() << "Server: Loading mods: ";
	for(const auto &mod: m_mods) {
		logger.infoStream() << mod.name << " ";
	}
	logger.infoStream() << "\n";

	// Load and run "mod" scripts
	for (const auto &mod: m_mods) {
		if (!string_allowed(mod.name, MODNAME_ALLOWED_CHARS)) {
			std::ostringstream err;
			err << "Error loading mod \"" << mod.name
					<< "\": mod_name does not follow naming conventions: "
					<< "Only chararacters [a-z0-9_] are allowed." << std::endl;
			logger.error("%s", err.str().c_str());
			throw ModError(err.str());
		}
		std::string script_path = mod.path + DIR_DELIM "init.lua";
		logger.info("  [%s] [\"%s\"]", padStringRight(mod.name, 12).c_str(), script_path.c_str());
		if (!m_script->loadMod(script_path, mod.name, &error_msg)) {
			logger.error("Server: Failed to load and run %s", script_path.c_str());
			throw ModError("Failed to load and run " + script_path
					+ "\nError from Lua:\n" + error_msg);
		}
	}

	// Read Textures and calculate sha1 sums
	fillMediaCache();

	// Apply item aliases in the node definition manager
	m_nodedef->updateAliases(m_itemdef);

	// Apply texture overrides from texturepack/override.txt
	std::string texture_path = g_settings->get("texture_path");
	if (texture_path != "" && fs::IsDir(texture_path))
		m_nodedef->applyTextureOverrides(texture_path + DIR_DELIM + "override.txt");

	m_nodedef->setNodeRegistrationStatus(true);

	// Perform pending node name resolutions
	m_nodedef->runNodeResolveCallbacks();

	// unmap node names for connected nodeboxes
	m_nodedef->mapNodeboxConnections();

	// init the recipe hashes to speed up crafting
	m_craftdef->initHashes(this);

	// Initialize Environment
	m_env = new ServerEnvironment(servermap, m_script, this, m_path_world, m_area_mgr);

	m_clients.setEnv(m_env);

	// Initialize mapgens
	m_emerge->initMapgens();

	// Give environment reference to scripting api
	m_script->initializeEnvironment(m_env);

	logger.info("Server: Loading environment metadata");
	m_env->loadMeta();

	contrib_onstart();
}

Server::~Server()
{
	logger.info("Server destruction");

	// Send shutdown message
	SendChatMessage(PEER_ID_BROADCAST, L"*** Server shutting down");

	{
		std::lock_guard<std::mutex> envlock(m_map_mutex);

		// Execute script shutdown hooks
		m_script->on_shutdown();

		logger.info("Server: Saving players");
		m_env->saveLoadedPlayers();

		logger.info("Server: Kicking players");
		std::string kick_msg;
		bool reconnect = false;
		if (getShutdownRequested()) {
			reconnect = m_shutdown_ask_reconnect;
			kick_msg = m_shutdown_msg;
		}
		if (kick_msg == "") {
			kick_msg = g_settings->get("kick_msg_shutdown");
		}
		m_env->kickAllPlayers(SERVER_ACCESSDENIED_SHUTDOWN,
			kick_msg, reconnect);

		logger.info("Server: Saving environment metadata");
		m_env->saveMeta();
	}

	stop();

	// stop all emerge threads before deleting players that may have
	// requested blocks to be emerged
	m_emerge->stopThreads();

	// Delete things in the reverse order of creation
	delete m_env;

	// N.B. the EmergeManager should be deleted after the Environment since Map
	// depends on EmergeManager to write its current params to the map meta
	delete m_emerge;
	delete m_event;
	delete m_itemdef;
	delete m_nodedef;
	delete m_craftdef;

	delete m_area_mgr;
	delete m_ban_mgr;
	delete m_chat_handler;
	delete m_creaturestore;

	// Deinitialize scripting
	logger.info("Server: Deinitializing scripting");
	delete m_script;

	// Delete detached inventories
	for (auto &i: m_detached_inventories) {
		delete i.second;
	}

	delete m_game_database;
	delete m_auth_database;
	delete m_map_database;
}

void Server::start(Address bind_addr)
{
#ifndef SERVER
	if (!m_singleplayer_mode)
#endif
	{
		m_bind_addr = bind_addr;
		logger.info("Starting server on %s...", bind_addr.serializeString().c_str());
	}

	// Stop thread if already running
	m_thread->stop();
	if (m_console_thread) {
		m_console_thread->stop();
	}

#ifndef SERVER
	if (!m_singleplayer_mode)
#endif
	{
#ifdef SERVER
		if (m_ischatclient_thread) {
			m_ischatclient_thread->stop();
		}
#endif

		// Initialize connection
		m_con.SetTimeoutMs(3);
		m_epixel_con = new epixel::NetworkServer(bind_addr.serializeString(), bind_addr.getPort(), this, &m_clients);
		m_epixel_con->start();

		// Force MT legacy to use config port
		bind_addr.setPort(g_settings->get(U16SETTING_PORT));
		m_con.Serve(bind_addr);
	}
#ifndef SERVER
	else {
		m_clients.CreateClient(PEER_ID_SINGLEPLAYER);
	}
#endif

	// Start thread
	m_thread->start();

	// ASCII art for the win!
	logger.noticeStream() << "\n"
	<< " _____                              ___      \n"
	<< "/\\  __`\\          __               /\\_ \\     \n"
	<< "\\ \\ \\_     _____ /\\_\\   __  _    __\\//\\ \\    \n"
	<< " \\ \\  _\\  /\\ '__`\\/\\ \\ /\\ \\/'\\ /'__`\\\\ \\ \\   \n"
	<< "  \\ \\ \\___\\ \\ \\L\\ \\ \\ \\\\/>  <//\\  __/ \\_\\ \\_ \n"
	<< "   \\ \\____/\\ \\ ,__/\\ \\_\\/\\_/\\_\\ \\____\\/\\____\\\n"
	<< "    \\/___/  \\ \\ \\/  \\/_/\\//\\/_/\\/____/\\/____/\n"
	<< "             \\ \\_\\                           \n"
	<< "              \\/_/                           ";

#ifndef SERVER
	if (!m_singleplayer_mode)
#endif
	{
		logger.notice("Server for gameid=\"%s\" listening on %s:%d",
				m_gamespec.id.c_str(), bind_addr.serializeString().c_str(), bind_addr.getPort());
#ifdef SERVER
		if (m_ischatclient_thread) {
			m_ischatclient_thread->start();
		}
#endif
	}

	if (m_console_thread) {
		m_console_thread->start();
	}
}

void Server::stop()
{
	logger.info("Server: Stopping and waiting threads");

	if (m_epixel_con) {
		m_epixel_con->stop_and_wait();
		delete m_epixel_con;
		m_epixel_con = nullptr;
	}

	if (m_thread) {
		m_thread->stop_and_wait();
		delete m_thread;
		m_thread = nullptr;
	}

	if (m_console_thread) {
		m_console_thread->stop();
		delete m_console_thread;
		m_console_thread = nullptr;
	}

#ifdef SERVER
	if (m_ischatclient_thread) {
		m_ischatclient_thread->stop_and_wait();
		delete m_ischatclient_thread;
		m_ischatclient_thread = nullptr;
	}
#endif

	logger.info("Server: Threads stopped");
}

void Server::step(float dtime)
{
	// Limit a bit
	if(dtime > 2.0)
		dtime = 2.0;

	m_step_dtime = m_step_dtime + dtime;

	// Throw if fatal error occurred in thread
	std::string async_err = m_async_fatal_error;
	if (!async_err.empty()) {
#ifndef SERVER
		if (m_singleplayer_mode) {
			throw ServerError(async_err);
		}
		else
#endif
		{
			m_env->kickAllPlayers(SERVER_ACCESSDENIED_CRASH,
				g_settings->get("kick_msg_crash"),
				g_settings->get(BOOLSETTING_ASK_RECONNECT_ON_CRASH));
			logger.fatal("UNRECOVERABLE error occurred. Stopping server. "
					"Please fix the following error: %s", async_err.c_str());
			FATAL_ERROR(async_err.c_str());
		}
	}
}

void Server::AsyncRunStep(bool initial_step)
{
	float dtime = m_step_dtime;

	SendBlocks(dtime);

	if (dtime < 0.001 && initial_step == false)
		return;

	m_step_dtime = m_step_dtime - dtime;

	/*
		Update uptime
	*/
	m_uptime = m_uptime + dtime;

	handlePeerChanges();

	/*
		Update time of day and overall game time
	*/
	m_env->setTimeOfDaySpeed(g_settings->get(FSETTING_TIME_SPEED));

	/*
		Send to clients at constant intervals
	*/

	m_time_of_day_send_timer -= dtime;
	if(m_time_of_day_send_timer < 0.0) {
		m_time_of_day_send_timer = g_settings->get(FSETTING_TIME_SEND_INTERVAL);
		u16 time = m_env->getTimeOfDay();
		float time_speed = g_settings->get(FSETTING_TIME_SPEED);
		SendTimeOfDay(PEER_ID_INEXISTENT, time, time_speed);
	}

	{
		// Figure out and report maximum lag to environment
		float max_lag = m_env->getMaxLagEstimate();
		max_lag *= 0.9998; // Decrease slowly (about half per 5 minutes)
		if(dtime > max_lag){
			if(dtime > 0.1 && dtime > max_lag * 2.0)
				logger.info("Server: Maximum lag peaked to %d s", dtime);
			max_lag = dtime;
		}
		m_env->reportMaxLagEstimate(max_lag);

		m_env->step(dtime);
	}

	contrib_asyncrunstep(dtime);

	static const float map_timer_and_unload_dtime = 2.92;
	m_map_timer_and_unload_interval -= dtime;
	if (m_map_timer_and_unload_interval <= 0.0f)
	{
		m_map_timer_and_unload_interval = map_timer_and_unload_dtime;
		std::lock_guard<std::mutex> lock(m_map_mutex);
		// Run Map's timers and unload unused data
		m_env->getMap().timerUpdate(map_timer_and_unload_dtime,
			g_settings->get(FSETTING_SERVER_UNLOAD_UNUSED_DATA_TIMEOUT),
			(u32)U32_MAX);
	}

	/*
		Do background stuff
	*/

	ConsumeAsyncBlocksNotSent();

	/* Transform liquids */
	m_liquid_transform_timer += dtime;
	if(m_liquid_transform_timer >= g_settings->get(FSETTING_LIQUID_UPDATE))
	{
		m_liquid_transform_timer -= g_settings->get(FSETTING_LIQUID_UPDATE);

		std::lock_guard<std::mutex> lock(m_map_mutex);

		std::map<v3s16, MapBlock*> modified_blocks;
		m_env->getMap().transformLiquids(modified_blocks);
		/*
			Set the modified blocks unsent for all the clients
		*/
		if(!modified_blocks.empty())
		{
			SetBlocksNotSent(modified_blocks);
		}
	}
	m_clients.step(dtime);

	m_lag += (m_lag > dtime ? -1 : 1) * dtime/100;
	// send masterserver announce
	{
		float &counter = m_masterserver_timer;
		if (
#ifndef SERVER
				!isSingleplayer() &&
#endif
				(!counter || counter >= 60.0) &&
				g_settings->get(BOOLSETTING_SERVER_ANNOUNCE))
		{
			ServerList::sendLegacyAnnounce(counter ? "update" : "start",
					m_bind_addr.getPort(),
					m_clients.getPlayerNames(),
					m_uptime,
					m_env->getGameTime(),
					m_lag,
					m_gamespec.id,
					m_emerge->params.mg_name,
					m_mods);

			ServerList::sendAnnounce(m_clients.getPlayerNames(), m_lag, m_gamespec.id, m_emerge->params.mg_name, m_mods);
			counter = 0.01;
		}
		counter += dtime;
	}
	/*
		Check added and deleted active objects
	*/
	{
		RemoteClientMap clients = m_clients.getClientList();

		// Radius inside which objects are active
		s16 radius = g_settings->get(S16SETTING_ACTIVEOBJECT_SEND_RANGE_BLOCKS);
		s16 player_radius = g_settings->get(S16SETTING_PLAYER_TRANSFER_DISTANCE);

		if (player_radius == 0 &&
				!g_settings->get(BOOLSETTING_UNLIMITED_PLAYER_TRANSFER_DISTANCE))
			player_radius = radius;

		radius *= MAP_BLOCKSIZE;
		player_radius *= MAP_BLOCKSIZE;

		for (RemoteClientMap::iterator
			i = clients.begin();
			i != clients.end(); ++i) {
			RemoteClient *client = i->second;

			// If definitions and textures have not been sent, don't
			// send objects either
			if (client->getState() < CS_DefinitionsSent)
				continue;

			RemotePlayer *player = m_env->getPlayer(client->peer_id);
			if(player == NULL) {
				continue;
			}

			std::queue<u16> removed_objects;
			std::queue<u16> added_objects;

			std::lock_guard<std::mutex> aolock(m_ao_mutex);
			m_env->getRemovedActiveObjects(player, radius, player_radius,
					client->m_known_objects, removed_objects);
			m_env->getAddedActiveObjects(player, radius, player_radius,
					client->m_known_objects, added_objects);

			// Ignore if nothing happened
			if(removed_objects.empty() && added_objects.empty()) {
				continue;
			}

			std::string data_buffer;
			char buf[4];

			// Handle removed objects
			writeU16((u8*)buf, removed_objects.size());
			data_buffer.append(buf, 2);
			while (!removed_objects.empty()) {
				// Get object
				u16 id = removed_objects.front();
				ServerActiveObject* obj = m_env->getActiveObject(id);

				// Add to data buffer for sending
				writeU16((u8*)buf, id);
				data_buffer.append(buf, 2);

				// Remove from known objects
				client->m_known_objects.erase(id);

				if(obj && obj->m_known_by_count > 0)
					obj->m_known_by_count--;
				removed_objects.pop();
			}

			// Handle added objects
			writeU16((u8*)buf, added_objects.size());
			data_buffer.append(buf, 2);
			while (!added_objects.empty()) {
				// Get object
				u16 id = added_objects.front();
				ServerActiveObject* obj = m_env->getActiveObject(id);

				// Get object type
				u8 type = ACTIVEOBJECT_TYPE_INVALID;
				if(obj == NULL) {
					logger.warn("WARNING: %s: NULL object", __FUNCTION_NAME);
				}
				else
					type = obj->getSendType();

				// Add to data buffer for sending
				writeU16((u8*)buf, id);
				data_buffer.append(buf, 2);
				writeU8((u8*)buf, type);
				data_buffer.append(buf, 1);

				if(obj)
					data_buffer.append(serializeLongString(
							obj->getClientInitializationData(client->net_proto_version)));
				else
					data_buffer.append(serializeLongString(""));

				// Add to known objects
				client->m_known_objects.insert(id);

				if(obj)
					obj->m_known_by_count++;

				added_objects.pop();
			}

			u32 pktSize = SendActiveObjectRemoveAdd(client->peer_id, data_buffer);
			logger.debug("Server: Sent object remove/add: %d removed, %d added, packet size is %d",
					removed_objects.size(), added_objects.size(), pktSize);
		}
	}

	/*
		Send object messages
	*/
	{
		std::lock_guard<std::mutex> envlock(m_map_mutex);

		// Key = object id
		// Value = data sent by object
		std::unordered_map<u16, std::vector<ActiveObjectMessage>* > buffered_messages;

		// Get active object messages from environment
		for(;;) {
			ActiveObjectMessage aom = m_env->getActiveObjectMessage();
			if (aom.id == 0)
				break;

			std::vector<ActiveObjectMessage>* message_list = NULL;
			std::unordered_map<u16, std::vector<ActiveObjectMessage>* >::iterator n = buffered_messages.find(aom.id);
			if (n == buffered_messages.end()) {
				message_list = new std::vector<ActiveObjectMessage>;
				buffered_messages[aom.id] = message_list;
			}
			else {
				message_list = n->second;
			}
			message_list->push_back(aom);
		}

		RemoteClientMap clients = m_clients.getClientList();
		// Route data to every client
		for (RemoteClientMap::iterator
			i = clients.begin();
			i != clients.end(); ++i) {
			RemoteClient *client = i->second;
			std::string reliable_data;
			std::string unreliable_data;
			// Go through all objects in message buffer
			for (std::unordered_map<u16, std::vector<ActiveObjectMessage>* >::iterator
					j = buffered_messages.begin();
					j != buffered_messages.end(); ++j) {
				// If object is not known by client, skip it
				u16 id = j->first;
				if (client->m_known_objects.find(id) == client->m_known_objects.end())
					continue;

				// Get message list of object
				std::vector<ActiveObjectMessage>* list = j->second;
				// Go through every message
				for (std::vector<ActiveObjectMessage>::iterator
						k = list->begin(); k != list->end(); ++k) {
					// Compose the full new data with header
					ActiveObjectMessage aom = *k;
					std::string new_data;
					// Add object id
					char buf[2];
					writeU16((u8*)&buf[0], aom.id);
					new_data.append(buf, 2);
					// Add data
					new_data += serializeString(aom.datastring);
					// Add data to buffer
					if(aom.reliable)
						reliable_data += new_data;
					else
						unreliable_data += new_data;
				}
			}
			/*
				reliable_data and unreliable_data are now ready.
				Send them.
			*/
			if (!reliable_data.empty()) {
				SendActiveObjectMessages(client->peer_id, reliable_data);
			}

			if (!unreliable_data.empty()) {
				SendActiveObjectMessages(client->peer_id, unreliable_data);
			}
		}
		// Clear buffered_messages
		for(auto &msg: buffered_messages) {
			delete msg.second;
		}
	}

	/*
		Send queued-for-sending map edit events.
	*/
	{
		// We will be accessing the environment
		std::lock_guard<std::mutex> lock(m_map_mutex);

		// Don't send too many at a time
		//u32 count = 0;

		// Single change sending is disabled if queue size is not small
		bool disable_single_change_sending = false;
		if(m_unsent_map_edit_queue.size() >= 4)
			disable_single_change_sending = true;

		int event_count = m_unsent_map_edit_queue.size();

		// We'll log the amount of each
		while(!m_unsent_map_edit_queue.empty())
		{
			MapEditEvent* event = m_unsent_map_edit_queue.front();
			m_unsent_map_edit_queue.pop();

			// Players far away from the change are stored here.
			// Instead of sending the changes, MapBlocks are set not sent
			// for them.
			std::vector<u16> far_players;

			switch (event->type) {
			case MEET_ADDNODE:
			case MEET_SWAPNODE:
				sendAddNode(event->p, event->n, event->already_known_by_peer,
						&far_players, disable_single_change_sending ? 5 : 30,
						event->type == MEET_ADDNODE);
				break;
			case MEET_REMOVENODE:
				sendRemoveNode(event->p, event->already_known_by_peer,
						&far_players, disable_single_change_sending ? 5 : 30);
				break;
			case MEET_BLOCK_NODE_METADATA_CHANGED:
				logger.info("Server: MEET_BLOCK_NODE_METADATA_CHANGED");
				sendMetadataChanged(event->p, &far_players,
						disable_single_change_sending ? 5 : 30);
			case MEET_OTHER:
				logger.info("Server: MEET_OTHER");
				for(std::set<v3s16>::iterator
						i = event->modified_blocks.begin();
						i != event->modified_blocks.end(); ++i) {
					setBlockNotSent(*i);
				}
				break;
			default:
				logger.warn("Server: Unknown MapEditEvent %d", ((u32)event->type));
				break;
			}

			/*
				Set blocks not sent to far players
			*/
			if(!far_players.empty()) {
				// Convert list format to that wanted by SetBlocksNotSent
				std::map<v3s16, MapBlock*> modified_blocks2;
				for(std::set<v3s16>::iterator
						i = event->modified_blocks.begin();
						i != event->modified_blocks.end(); ++i) {
					modified_blocks2[*i] =
							m_env->getMap().getBlockNoCreateNoEx(*i);
				}

				// Set blocks not sent
				for(std::vector<u16>::iterator
						i = far_players.begin();
						i != far_players.end(); ++i) {
					if(RemoteClient *client = getClient(*i))
						client->SetBlocksNotSent(modified_blocks2);
				}
			}

			delete event;
		}

		if(event_count >= 5) {
			logger.info("Server: MapEditEvents:");
		} else if(event_count != 0) {
			logger.debug("Server: MapEditEvents:");
		}

	}

	/*
		Trigger emergethread (it somehow gets to a non-triggered but
		bysy state sometimes)
	*/
	{
		float &counter = m_emergethread_trigger_timer;
		counter += dtime;
		if(counter >= 2.0)
		{
			counter = 0.0;

			m_emerge->startThreads();
		}
	}

	// Save map, players and auth stuff
	{
		float &counter = m_savemap_timer;
		counter += dtime;

		if(counter >= g_settings->get(FSETTING_SERVER_MAP_SAVE_INTERVAL))
		{
			counter = 0.0;
			std::lock_guard<std::mutex> lock(m_map_mutex);

			// Save changed parts of map
			m_env->getMap().save(MOD_STATE_WRITE_NEEDED);

			// Save players
			m_env->saveLoadedPlayers();

			if (m_save_meta_interval <= 0) {
				// Save environment metadata
				m_env->saveMeta();
				m_save_meta_interval = g_settings->get(U16SETTING_SAVE_META_INTERVAL);
			}
			m_save_meta_interval--;
		}
	}
}

void Server::Receive()
{
	u16 peer_id = 0;
	try {
		NetworkPacket* pkt = nullptr;
#ifndef SERVER
		if (m_singleplayer_mode) {
			pkt = m_receive_packet_queue.pop_front(1);
		}
		else
#endif
		{
			// First process a maximum of 1k movement packets
			u16 movement_counter = 0;
			bool packetrecv = true;
			while (packetrecv && movement_counter < 1000) {
				movement_counter++;
				m_con.ReceiveMovement(pkt);
				if (pkt != nullptr) {
					PacketDestructor pktdtor(pkt);
					peer_id = pkt->getPeerId();
					ProcessData(pkt);
				}
				else {
					packetrecv = false;
				}
			}

			m_con.Receive(pkt);
		}

		PacketDestructor pktdtor(pkt);
		peer_id = pkt->getPeerId();
		ProcessData(pkt);
	}
	catch(con::InvalidIncomingDataException &e) {
		logger.info("Server::Receive(): InvalidIncomingDataException: what()=",
				e.what());
	}
	catch(SerializationError &e) {
		logger.info("Server::Receive(): SerializationError: what()=",
				e.what());
	}
	catch(ClientStateError &e) {
		logger.error("ProcessData: peer=%d %s", peer_id, e.what());
		DenyAccess_Legacy(peer_id, L"Your client sent something server didn't expect."
				L"Try reconnecting or updating your client");
	}
	catch(con::PeerNotFoundException &) {
		// Do nothing
	}
	catch(con::NoIncomingDataException &) {
		// Do nothing
	}
	catch(ItemNotFoundException &) {
		// Do nothing
	}

	if (m_epixel_con) {
		// Now fetch from TCP connection
		std::shared_ptr<NetworkPacket> epixel_pkt = m_epixel_con->popRecvMsg();
		if (epixel_pkt) {
			ProcessData(epixel_pkt.get());
		}
	}
}

PlayerSAO* Server::StageTwoClientInit(u16 peer_id)
{
	std::string playername = "";
	PlayerSAO *playersao = NULL;
	RemoteClient* client = m_clients.getClientNoEx(peer_id, CS_InitDone);
	try {
		if (client != NULL) {
			playername = client->getName();
			playersao = emergePlayer(playername.c_str(), peer_id, client->net_proto_version);
		}
	} catch (std::exception &e) {
		logger.error("Exception on StageTwoClientInit: %s", e.what());
		return NULL;
	}

	RemotePlayer *player = m_env->getPlayer(playername.c_str());

	// If failed, cancel
	if ((playersao == NULL) || (player == NULL)) {
		if (player && player->getPeerID() != PEER_ID_INEXISTENT) {
			logger.notice("Server: Failed to emerge player \"%s"
					"\" (player allocated to an another client)", playername.c_str());
			DenyAccess_Legacy(peer_id, L"Another client is connected with this "
					L"name. If your client closed unexpectedly, try again in "
					L"a minute.");
		} else {
			logger.crit("Server: Failed to emerge player %s", playername.c_str());
			DenyAccess_Legacy(peer_id, L"Could not allocate player.");
		}
		return NULL;
	}

	/*
		Send complete position information
	*/
	SendMovePlayer(peer_id);

	// Send privileges
	SendPlayerPrivileges(player);

	// Send inventory formspec
	SendPlayerInventoryFormspec(player);

	// Send inventory
	SendInventory(playersao);

	// Send HP
	SendPlayerHPOrDie(playersao);

	// Send Breath
	SendPlayerBreath(playersao);

	// Show death screen if necessary
	if(player->isDead())
		SendDeathscreen(peer_id, false, v3f(0,0,0));

	// Note things in chat if not in simple singleplayer mode
#ifndef SERVER
	if(!m_singleplayer_mode)
#endif
	{
		// Send information about server to player in chat
		SendChatMessage(peer_id, getStatusString());

		// Send information about joining in chat
		{
			std::wstring name = L"unknown";
			RemotePlayer *player = m_env->getPlayer(peer_id);
			if(player != NULL)
				name = narrow_to_wide(player->getName());

			std::wstring message;
			message += L"*** ";
			message += name;
			message += L" joined the game.";
			SendChatMessage(PEER_ID_BROADCAST,message);
		}
	}
	std::string ip_str = getPeerAddressStr(player->getPeerID());
	logger.noticeStream() << player->getName() <<" [" << ip_str << " - v" <<
			(int)client->getMajor() << "." << (int)client->getMinor() <<
			"." << (int)client->getPatch()
			<< (client->isEpixelClient() ? " - epixel" : "")
			<< "] joins game. \n";
	/*
		Print out action
	*/
	{
		std::vector<std::string> names = m_clients.getPlayerNames();
		std::stringstream ss;
		ss << "List of players: ";

		for (const auto &str: names) {
			ss << str << " ";
		}

		logger.notice("%s%s", ss.str().c_str(), player->getName());

		httppost_async_stat(new epixel::PlayerStat_Connection(player->getDBId()));
	}
	return playersao;
}

inline void Server::handleCommand(NetworkPacket* pkt)
{
	std::lock_guard<std::mutex> envlock(m_map_mutex);
	const ToServerCommandHandler& opHandle = toServerCommandTable[pkt->getCommand()];
	(this->*opHandle.handler)(pkt);
}

Address Server::getPeerAddress(u16 peer_id)
{
	if (peer_id < U16_MAX / 2) {
		return m_con.GetPeerAddress(peer_id);
	}
	RemoteClient* client = m_clients.getClientNoEx(peer_id, CS_Created);
	if (!client) {
		throw con::PeerNotFoundException("No address for peer found!");
	}

	if (client->getSessionType() == SESSION_TYPE_EPIXEL) {
		return Address();
	}

	throw con::PeerNotFoundException("No address for peer found!");
}

const std::string Server::getPeerAddressStr(u16 peer_id)
{
	if (peer_id < U16_MAX / 2) {
		return m_con.GetPeerAddress(peer_id).serializeString();
	}

	RemoteClient* client = m_clients.getClientNoEx(peer_id, CS_Created);
	if (!client) {
		throw con::PeerNotFoundException("No address for peer found!");
	}

	if (client->getSessionType() == SESSION_TYPE_EPIXEL) {
		return ((epixel::Session*)client)->getAddress();
	}

	throw con::PeerNotFoundException("No address for peer found!");
}

void Server::ProcessData(NetworkPacket *pkt)
{
	u32 peer_id = pkt->getPeerId();

	try {
		std::string addr_s = getPeerAddressStr(peer_id);
		std::string reason;

		if(m_ban_mgr->isBanned(addr_s, reason, epixel::BANTYPE_IP)) {
			logger.info("Server: A banned client tried to connect from %s; reason: %s",
					addr_s.c_str(), reason.c_str());
			// This actually doesn't seem to transfer to the client
			DenyAccess_Legacy(peer_id, L"Your ip is banned. Reason "
					+ utf8_to_wide(reason));
			return;
		}
	}
	catch(con::PeerNotFoundException &e) {
		/*
		 * no peer for this packet found
		 * most common reason is peer timeout, e.g. peer didn't
		 * respond for some time, your server was overloaded or
		 * things like that.
		 */
		logger.info("Server::ProcessData(): Canceling: peer %d not found",
				peer_id);
		return;
	}

	try {
		ToServerCommand command = (ToServerCommand) pkt->getCommand();

		// Command must be handled into ToServerCommandHandler
		if (command >= TOSERVER_NUM_MSG_TYPES) {
			logger.info("Server: Ignoring unknown command %d", command);
			return;
		}

		if (toServerCommandTable[command].state == TOSERVER_STATE_NOT_CONNECTED) {
			handleCommand(pkt);
			return;
		}

		u8 peer_ser_ver = getClient(peer_id, CS_InitDone)->serialization_version;

		if(peer_ser_ver == SER_FMT_VER_INVALID) {
			logger.error("Server::ProcessData(): Cancelling: Peer"
					" serialization format invalid or not initialized."
					" Skipping incoming command=%d", command);
			return;
		}

		/* Handle commands related to client startup */
		if (toServerCommandTable[command].state == TOSERVER_STATE_STARTUP) {
			handleCommand(pkt);
			return;
		}

		if (m_clients.getClientState(peer_id) < CS_Active) {
			if (command == TOSERVER_PLAYERPOS) return;

			logger.error("Got packet command: %d for peer id "
					"%d but client isn't active yet. Dropping packet ",
					command, peer_id);
			return;
		}

		handleCommand(pkt);
	} catch (SendFailedException &e) {
		logger.crit("Server::ProcessData(): SendFailedException: "
				"what=", e.what());
	} catch (PacketError &e) {
		logger.warn("Server::ProcessData(): PacketError: "
				"what=%s", e.what());
	}
}

void Server::setTimeOfDay(u32 time)
{
	m_env->setTimeOfDay(time);
	m_time_of_day_send_timer = 0;
}

void Server::onMapEditEvent(MapEditEvent *event)
{
	if(m_ignore_map_edit_events)
		return;
	if(m_ignore_map_edit_events_area.contains(event->getArea()))
		return;
	MapEditEvent *e = event->clone();
	m_unsent_map_edit_queue.push(e);
}

Inventory* Server::getInventory(const InventoryLocation &loc)
{
	switch (loc.type) {
	case InventoryLocation::UNDEFINED:
	case InventoryLocation::CURRENT_PLAYER:
		break;
	case InventoryLocation::PLAYER:
	{
		RemotePlayer *player = m_env->getPlayer(loc.name.c_str());
		if(!player)
			return NULL;
		PlayerSAO *playersao = player->getPlayerSAO();
		if(!playersao)
			return NULL;
		return playersao->getInventory();
	}
		break;
	case InventoryLocation::NODEMETA:
	{
		NodeMetadata *meta = m_env->getMap().getNodeMetadata(loc.p);
		if(!meta)
			return NULL;
		return meta->getInventory();
	}
		break;
	case InventoryLocation::DETACHED:
	{
		if(m_detached_inventories.count(loc.name) == 0)
			return NULL;
		return m_detached_inventories[loc.name];
	}
		break;
	default:
		sanity_check(false); // abort
		break;
	}
	return NULL;
}
void Server::setInventoryModified(const InventoryLocation &loc, bool playerSend)
{
	switch(loc.type){
	case InventoryLocation::UNDEFINED:
		break;
	case InventoryLocation::PLAYER:
	{
		if (!playerSend)
			return;

		RemotePlayer *player = m_env->getPlayer(loc.name.c_str());
		if(!player)
			return;
		PlayerSAO *playersao = player->getPlayerSAO();
		if(!playersao)
			return;

		SendInventory(playersao);
	}
		break;
	case InventoryLocation::NODEMETA:
	{
		sendMetadataChanged(loc.p);
	}
		break;
	case InventoryLocation::DETACHED:
	{
		sendDetachedInventory(loc.name,PEER_ID_INEXISTENT);
	}
		break;
	default:
		sanity_check(false); // abort
		break;
	}
}

void Server::AsyncSetBlocksNotSent(const std::map<v3s16, MapBlock*> &blocks)
{
	std::lock_guard<std::mutex> lock(m_async_blocs_ns_mutex);

	// If list is empty replace, else merge
	if (m_async_blocks_not_sent.empty())
		m_async_blocks_not_sent = blocks;
	else {
		for (const auto &block: blocks) {
			m_async_blocks_not_sent[block.first] = block.second;
		}
	}
}

void Server::ConsumeAsyncBlocksNotSent()
{
	std::lock_guard<std::mutex> lock(m_async_blocs_ns_mutex);
	SetBlocksNotSent(m_async_blocks_not_sent);
	m_async_blocks_not_sent.clear();
}

void Server::SetBlocksNotSent(const std::map<v3s16, MapBlock *>& block)
{
	std::vector<u16> clients = m_clients.getClientIDs();
	// Set the modified blocks unsent for all the clients
	for (std::vector<u16>::iterator i = clients.begin();
		 i != clients.end(); ++i) {
			if (RemoteClient *client = m_clients.getClientNoEx(*i))
				client->SetBlocksNotSent(block);
	}
}

void Server::peerAdded(con::Peer *peer)
{
	logger.debug("Server::peerAdded(): peer->id=%d", peer->id);

	con::PeerChange c;
	c.type = con::PEER_ADDED;
	c.peer_id = peer->id;
	c.timeout = false;
	m_peer_change_queue.push(c);
}

void Server::deletingPeer(con::Peer *peer, bool timeout)
{
	logger.debug("Server::deletingPeer(): peer->id=%d, timeout=%d",
			peer->id, (int)timeout);

	m_clients.event(peer->id, CSE_Disconnect);
	con::PeerChange c;
	c.type = con::PEER_REMOVED;
	c.peer_id = peer->id;
	c.timeout = timeout;
	m_peer_change_queue.push(c);
}

bool Server::getClientConInfo(u16 peer_id, con::rtt_stat_type type, float* retval)
{
	*retval = m_con.getPeerStat(peer_id,type);
	if (*retval == -1) return false;
	return true;
}

bool Server::getClientInfo(
		u16          peer_id,
		ClientState* state,
		u32*         uptime,
		u8*          ser_vers,
		u16*         prot_vers,
		u8*          major,
		u8*          minor,
		u8*          patch,
		std::string* vers_string
	)
{
	*state = m_clients.getClientState(peer_id);
	RemoteClient* client = m_clients.getClientNoEx(peer_id, CS_Invalid);

	if (client == NULL) {
		return false;
	}

	*uptime = client->uptime();
	*ser_vers = client->serialization_version;
	*prot_vers = client->net_proto_version;

	*major = client->getMajor();
	*minor = client->getMinor();
	*patch = client->getPatch();
	*vers_string = client->getPatch();

	return true;
}

void Server::handlePeerChanges()
{
	while(m_peer_change_queue.size() > 0)
	{
		con::PeerChange c = m_peer_change_queue.front();
		m_peer_change_queue.pop();

		logger.debug("Server: Handling peer change: id=%d, timeout=%d",
				c.peer_id, c.timeout);

		switch(c.type)
		{
		case con::PEER_ADDED:
			m_clients.CreateClient(c.peer_id);
			break;

		case con::PEER_REMOVED:
			DeleteClient(c.peer_id, c.timeout?CDR_TIMEOUT:CDR_LEAVE);
			break;

		default:
			FATAL_ERROR("Invalid peer change event received!");
			break;
		}
	}
}

void Server::Send(NetworkPacket* pkt)
{
#ifndef SERVER
	if (!m_singleplayer_mode)
#endif
	{
		if (pkt->getPeerId() < U16_MAX / 2) {
		m_clients.send(pkt->getPeerId(),
			   clientCommandFactoryTable[pkt->getCommand()].channel,
			   pkt, clientCommandFactoryTable[pkt->getCommand()].reliable);
		}
		else if (RemoteClient* client = m_clients.getClientNoEx(pkt->getPeerId(), CS_Created)) {
			if (client->getSessionType() != SESSION_TYPE_EPIXEL) {
				assert(false); // This should not happen
			}
			m_epixel_con->pushMsg((epixel::Session*)client, pkt);
		}
	}
#ifndef SERVER
	else {
		NetworkPacket *spkt = new NetworkPacket(pkt->getCommand(), pkt->getSize(), PEER_ID_SERVER);
		spkt->putRawString((const char*)pkt->getU8Ptr(0), pkt->getSize());
		spkt->reinitReadOffset();
		m_send_packet_queue.push_back(spkt);
	}
#endif
}

void Server::SendMovement(u16 peer_id)
{
	std::ostringstream os(std::ios_base::binary);

	NetworkPacket pkt(TOCLIENT_MOVEMENT, 12 * sizeof(float), peer_id);

	pkt << g_settings->get(FSETTING_MOVEMENT_ACCELERATION_DEFAULT);
	pkt << g_settings->get(FSETTING_MOVEMENT_ACCELERATION_AIR);
	pkt << g_settings->get(FSETTING_MOVEMENT_ACCELERATION_FAST);
	pkt << g_settings->get(FSETTING_MOVEMENT_SPEED_WALK);
	pkt << g_settings->get(FSETTING_MOVEMENT_SPEED_CROUCH);
	pkt << g_settings->get(FSETTING_MOVEMENT_SPEED_FAST);
	pkt << g_settings->get(FSETTING_MOVEMENT_SPEED_CLIMB);
	pkt << g_settings->get(FSETTING_MOVEMENT_SPEED_JUMP);
	pkt << g_settings->get(FSETTING_MOVEMENT_LIQUID_FLUIDITY);
	pkt << g_settings->get(FSETTING_MOVEMENT_LIQUID_FLUIDITY_SMOOTH);
	pkt << g_settings->get(FSETTING_MOVEMENT_LIQUID_SINK);
	pkt << g_settings->get(FSETTING_MOVEMENT_GRAVITY);

	Send(&pkt);
}

void Server::SendPlayerHPOrDie(PlayerSAO *playersao)
{
	if (!g_settings->get(BOOLSETTING_ENABLE_DAMAGE))
		return;

	if (playersao->getHP() > 0) {
		SendPlayerHP(playersao);
	}
	else {
		DiePlayer(playersao);
	}
}

void Server::SendHP(u16 peer_id, u8 hp)
{
	NetworkPacket pkt(TOCLIENT_HP, 1, peer_id);
	pkt << hp;
	Send(&pkt);
}

void Server::SendBreath(u16 peer_id, u16 breath)
{
	NetworkPacket pkt(TOCLIENT_BREATH, 2, peer_id);
	pkt << (u16) breath;
	Send(&pkt);
}

void Server::SendAccessDenied(u16 peer_id, AccessDeniedCode reason,
		const std::string &custom_reason, bool reconnect)
{
	assert(reason < SERVER_ACCESSDENIED_MAX);

	NetworkPacket pkt(TOCLIENT_ACCESS_DENIED, 1, peer_id);
	pkt << (u8)reason;
	if (reason == SERVER_ACCESSDENIED_CUSTOM_STRING)
		pkt << custom_reason;
	else if (reason == SERVER_ACCESSDENIED_SHUTDOWN ||
			reason == SERVER_ACCESSDENIED_CRASH)
		pkt << custom_reason << (u8)reconnect;
	Send(&pkt);
}

void Server::SendAccessDenied_Legacy(u16 peer_id,const std::wstring &reason)
{
	NetworkPacket pkt(TOCLIENT_ACCESS_DENIED_LEGACY, 0, peer_id);
	pkt << reason;
	Send(&pkt);
}

void Server::SendDeathscreen(u16 peer_id, bool set_camera_point_target,
		v3f camera_point_target)
{
	NetworkPacket pkt(TOCLIENT_DEATHSCREEN, 1 + sizeof(v3f), peer_id);
	pkt << set_camera_point_target << camera_point_target;
	Send(&pkt);
}

void Server::SendItemDef(u16 peer_id,
		IItemDefManager *itemdef, u16 protocol_version)
{
	NetworkPacket pkt(TOCLIENT_ITEMDEF, 0, peer_id);

	/*
		u16 command
		u32 length of the next item
		zlib-compressed serialized ItemDefManager
	*/
	std::ostringstream tmp_os(std::ios::binary);
	itemdef->serialize(tmp_os, protocol_version);
	std::ostringstream tmp_os2(std::ios::binary);
	compressZlib(tmp_os.str(), tmp_os2);
	pkt.putLongString(tmp_os2.str());

	// Make data buffer
	logger.debug("Server: Sending item definitions to id(%d): size=%d",
			peer_id, pkt.getSize());

	Send(&pkt);
}

void Server::SendNodeDef(u16 peer_id,
		INodeDefManager *nodedef, u16 protocol_version)
{
	NetworkPacket pkt(TOCLIENT_NODEDEF, 0, peer_id);

	/*
		u16 command
		u32 length of the next item
		zlib-compressed serialized NodeDefManager
	*/
	std::ostringstream tmp_os(std::ios::binary);
	nodedef->serialize(tmp_os, protocol_version);
	std::ostringstream tmp_os2(std::ios::binary);
	compressZlib(tmp_os.str(), tmp_os2);

	pkt.putLongString(tmp_os2.str());

	// Make data buffer
	logger.debug("Server: Sending node definitions to id(%d)",
			peer_id, pkt.getSize());

	Send(&pkt);
}

/*
	Non-static send methods
*/

void Server::SendInventory(PlayerSAO* playerSAO)
{
	UpdateCrafting(playerSAO->getPlayer());

	/*
		Serialize it
	*/

	NetworkPacket pkt(TOCLIENT_INVENTORY, 0, playerSAO->getPeerID());

	std::ostringstream os;
	playerSAO->getInventory()->serialize(os);

	std::string s = os.str();

	pkt.putRawString(s.c_str(), s.size());
	Send(&pkt);
}

void Server::SendChatMessage(u16 peer_id, const std::wstring &message)
{
	// PEER_ID_SERVER is used by console user
	if (peer_id == PEER_ID_SERVER) {
		std::cout << wide_to_utf8(message) << std::endl;
		return;
	}

	NetworkPacket pkt(TOCLIENT_CHAT_MESSAGE, 0, peer_id);
	pkt << message;

	if (peer_id != PEER_ID_INEXISTENT && peer_id != PEER_ID_BROADCAST) {
		Send(&pkt);
	}
	else {
		m_clients.sendToAll(0, &pkt, true);
	}
}

void Server::SendShowFormspecMessage(u16 peer_id, const std::string &formspec,
		const std::string &formname)
{
	NetworkPacket pkt(TOCLIENT_SHOW_FORMSPEC, 0 , peer_id);

	pkt.putLongString(FORMSPEC_VERSION_STRING + formspec);
	pkt << formname;

	Send(&pkt);
}

// Spawns a particle on peer with peer_id
void Server::SendSpawnParticle(u16 peer_id, v3f pos, v3f velocity, v3f acceleration,
				float expirationtime, float size, bool collisiondetection,
				bool vertical, std::string texture)
{
	NetworkPacket pkt(TOCLIENT_SPAWN_PARTICLE, 0, peer_id);

	pkt << pos << velocity << acceleration << expirationtime
			<< size << collisiondetection;
	pkt.putLongString(texture);
	pkt << vertical;

	if (peer_id != PEER_ID_INEXISTENT && peer_id != PEER_ID_BROADCAST) {
		Send(&pkt);
	}
	else {
		m_clients.sendToAll(0, &pkt, true);
	}
}

// Adds a ParticleSpawner on peer with peer_id
void Server::SendAddParticleSpawner(u16 peer_id, u16 amount, float spawntime, v3f minpos, v3f maxpos,
	v3f minvel, v3f maxvel, v3f minacc, v3f maxacc, float minexptime, float maxexptime,
	float minsize, float maxsize, bool collisiondetection, bool vertical, std::string texture, u32 id)
{
	NetworkPacket pkt(TOCLIENT_ADD_PARTICLESPAWNER, 0, peer_id);

	pkt << amount << spawntime << minpos << maxpos << minvel << maxvel
			<< minacc << maxacc << minexptime << maxexptime << minsize
			<< maxsize << collisiondetection;

	pkt.putLongString(texture);

	pkt << id << vertical;

	if (peer_id != PEER_ID_INEXISTENT && peer_id != PEER_ID_BROADCAST) {
		Send(&pkt);
	}
	else {
		m_clients.sendToAll(0, &pkt, true);
	}
}

void Server::SendDeleteParticleSpawner(u16 peer_id, u32 id)
{
	NetworkPacket pkt(TOCLIENT_DELETE_PARTICLESPAWNER_LEGACY, 2, peer_id);

	// Ugly error in this packet
	pkt << (u16) id;

	if (peer_id != PEER_ID_INEXISTENT && peer_id != PEER_ID_BROADCAST) {
		Send(&pkt);
	}
	else {
		m_clients.sendToAll(0, &pkt, true);
	}

}

void Server::SendHUDAdd(u16 peer_id, u32 id, HudElement *form)
{
	NetworkPacket pkt(TOCLIENT_HUDADD, 1 + 4 + 0 + 4 + 0 + 4 + 4  + 4 + 4 + 4 + 6 + 8 , peer_id);
	pkt << id << (u8) form->type << form->pos << form->name << form->scale
			<< form->text << form->number << form->item << form->dir
			<< form->align << form->offset << form->world_pos << form->size;

	Send(&pkt);
}

void Server::SendHUDRemove(u16 peer_id, u32 id)
{
	NetworkPacket pkt(TOCLIENT_HUDRM, 4, peer_id);
	pkt << id;
	Send(&pkt);
}

void Server::SendHUDChange(u16 peer_id, u32 id, HudElementStat stat, void *value)
{
	NetworkPacket pkt(TOCLIENT_HUDCHANGE, 0, peer_id);
	pkt << id << (u8) stat;

	switch (stat) {
		case HUD_STAT_POS:
		case HUD_STAT_SCALE:
		case HUD_STAT_ALIGN:
		case HUD_STAT_OFFSET:
			pkt << *(v2f *) value;
			break;
		case HUD_STAT_NAME:
		case HUD_STAT_TEXT:
			pkt << *(std::string *) value;
			break;
		case HUD_STAT_WORLD_POS:
			pkt << *(v3f *) value;
			break;
		case HUD_STAT_SIZE:
			pkt << *(v2s32 *) value;
			break;
		case HUD_STAT_NUMBER:
		case HUD_STAT_ITEM:
		case HUD_STAT_DIR:
		default:
			pkt << *(u32 *) value;
			break;
	}

	Send(&pkt);
}

void Server::SendHUDSetFlags(u16 peer_id, u32 flags, u32 mask)
{
	NetworkPacket pkt(TOCLIENT_HUD_SET_FLAGS, 4 + 4, peer_id);

	flags &= ~(HUD_FLAG_HEALTHBAR_VISIBLE | HUD_FLAG_BREATHBAR_VISIBLE);

	pkt << flags << mask;

	Send(&pkt);
}

void Server::SendHUDSetParam(u16 peer_id, u16 param, const std::string &value)
{
	NetworkPacket pkt(TOCLIENT_HUD_SET_PARAM, 0, peer_id);
	pkt << param << value;
	Send(&pkt);
}

void Server::SendSetSky(u16 peer_id, const video::SColor &bgcolor,
		const std::string &type, const std::vector<std::string> &params)
{
	NetworkPacket pkt(TOCLIENT_SET_SKY, 0, peer_id);
	pkt << bgcolor << type << (u16) params.size();

	for(size_t i=0; i<params.size(); i++)
		pkt << params[i];

	Send(&pkt);
}

void Server::SendOverrideDayNightRatio(u16 peer_id, bool do_override,
		float ratio)
{
	NetworkPacket pkt(TOCLIENT_OVERRIDE_DAY_NIGHT_RATIO,
			1 + 2, peer_id);

	pkt << do_override << (u16) (ratio * 65535);

	Send(&pkt);
}

void Server::SendTimeOfDay(u16 peer_id, u16 time, f32 time_speed)
{
	NetworkPacket pkt(TOCLIENT_TIME_OF_DAY, 0, peer_id);
	pkt << time << time_speed;

	if (peer_id == PEER_ID_INEXISTENT || peer_id == PEER_ID_BROADCAST) {
		m_clients.sendToAll(0, &pkt, true);
	}
	else {
		Send(&pkt);
	}
}

void Server::SendPlayerHP(PlayerSAO* playersao)
{
	// In some rare case if the player is disconnected
	// while Lua call l_punch, for example, this can be NULL
	if (!playersao)
		return;

	SendHP(playersao->getPeerID(), playersao->getHP());
	m_script->player_event(playersao,"health_changed");

	// Send to other clients
	std::string str = gob_cmd_punched(playersao->readDamage(), playersao->getHP());
	ActiveObjectMessage aom(playersao->getId(), true, str);
	playersao->m_messages_out.push(aom);
}

void Server::SendPlayerBreath(PlayerSAO* playersao)
{
	assert(playersao);

	m_script->player_event(playersao, "breath_changed");
	SendBreath(playersao->getPeerID(), playersao->getBreath());

	u32 breath2 = (u32) (playersao->getBreath() * 2);

	// Hide the bar if breath is full
	if (breath2 > 20) {
		breath2 = 0;
	}

	// Hud are not inited every time there (StageTwoClientInit)
	if (playersao->getPlayer() && playersao->getPlayer()->getHuds()) {
		SendHUDChange(playersao->getPeerID(),
				playersao->getPlayer()->getHuds()->getHudId(PLAYERHUD_BREATH),
				HUD_STAT_NUMBER, &breath2);
	}
}

void Server::SendMovePlayer(u16 peer_id)
{
	RemotePlayer *player = m_env->getPlayer(peer_id);
	// Ignore non fully loaded players
	if (!player || !player->getPlayerSAO()) {
		return;
	}

	NetworkPacket pkt(TOCLIENT_MOVE_PLAYER, sizeof(v3f) + sizeof(f32) * 2, peer_id);
	pkt << player->getPosition() << player->getPlayerSAO()->getPitch() << player->getPlayerSAO()->getYaw();

	{
		v3f pos = player->getPosition();
		f32 pitch = player->getPlayerSAO()->getPitch();
		f32 yaw = player->getPlayerSAO()->getYaw();
		logger.debug("Server: Sending TOCLIENT_MOVE_PLAYER pos=(%f,%f,%f) pitch=%f yaw=%f",
				pos.X, pos.Y, pos.Z, pitch, yaw);
	}

	Send(&pkt);
}

void Server::SendLocalPlayerAnimations(u16 peer_id, v2s32 animation_frames[4], f32 animation_speed)
{
	NetworkPacket pkt(TOCLIENT_LOCAL_PLAYER_ANIMATIONS, 0,
		peer_id);

	pkt << animation_frames[0] << animation_frames[1] << animation_frames[2]
			<< animation_frames[3] << animation_speed;

	Send(&pkt);
}

void Server::SendEyeOffset(u16 peer_id, v3f first, v3f third)
{
	NetworkPacket pkt(TOCLIENT_EYE_OFFSET, 0, peer_id);
	pkt << first << third;
	Send(&pkt);
}
void Server::SendPlayerPrivileges(RemotePlayer* player)
{
	if (player->getPeerID() == PEER_ID_INEXISTENT)
		return;

	assert(player->getPlayerSAO());
	std::set<std::string> privs = player->getPlayerSAO()->getPrivs();

	NetworkPacket pkt(TOCLIENT_PRIVILEGES, 0, player->getPeerID());
	pkt << (u16) privs.size();

	for (const auto &priv: privs) {
		pkt << priv;
	}

	Send(&pkt);
}

void Server::SendPlayerInventoryFormspec(RemotePlayer* player)
{
	assert(player);

	if(player->getPeerID() == PEER_ID_INEXISTENT)
		return;

	NetworkPacket pkt(TOCLIENT_INVENTORY_FORMSPEC, 0, player->getPeerID());
	pkt.putLongString(FORMSPEC_VERSION_STRING + player->inventory_formspec);
	Send(&pkt);
}

u32 Server::SendActiveObjectRemoveAdd(u16 peer_id, const std::string &datas)
{
	NetworkPacket pkt(TOCLIENT_ACTIVE_OBJECT_REMOVE_ADD, datas.size(), peer_id);
	pkt.putRawString(datas.c_str(), datas.size());
	Send(&pkt);
	return pkt.getSize();
}

void Server::SendActiveObjectMessages(u16 peer_id, const std::string &datas, bool reliable)
{
	NetworkPacket pkt(TOCLIENT_ACTIVE_OBJECT_MESSAGES,
			datas.size(), peer_id);

	pkt.putRawString(datas.c_str(), datas.size());

	m_clients.send(pkt.getPeerId(),
			reliable ? clientCommandFactoryTable[pkt.getCommand()].channel : 1,
			&pkt, reliable);

}

s32 Server::playSound(const SimpleSoundSpec &spec,
		const ServerSoundParams &params)
{
	// Find out initial position of sound
	bool pos_exists = false;
	v3f pos = params.getPos(m_env, &pos_exists);
	// If position is not found while it should be, cancel sound
	if(pos_exists != (params.type != ServerSoundParams::SSP_LOCAL))
		return -1;

	// Filter destination clients
	std::vector<u16> dst_clients;
	if(params.to_player != "")
	{
		RemotePlayer *player = m_env->getPlayer(params.to_player.c_str());
		if(!player){
			logger.info("Server::playSound: Player \"%s\" not found",
					params.to_player.c_str());
			return -1;
		}
		if(player->getPeerID() == PEER_ID_INEXISTENT){
			logger.info("Server::playSound: Player \"%s\" not connected",
					params.to_player.c_str());
			return -1;
		}
		dst_clients.push_back(player->getPeerID());
	}
	else {
		std::vector<u16> clients = m_clients.getClientIDs();

		for(std::vector<u16>::iterator
				i = clients.begin(); i != clients.end(); ++i) {
			RemotePlayer *player = m_env->getPlayer(*i);
			if(!player)
				continue;

			if(pos_exists) {
				if(player->getPosition().getDistanceFrom(pos) >
						params.max_hear_distance)
					continue;
			}
			dst_clients.push_back(*i);
		}
	}

	if(dst_clients.empty())
		return -1;

	// Create the sound
	s32 id = m_next_sound_id++;
	// The sound will exist as a reference in m_playing_sounds
	m_playing_sounds[id] = ServerPlayingSound();
	ServerPlayingSound &psound = m_playing_sounds[id];
	psound.params = params;

	NetworkPacket pkt(TOCLIENT_PLAY_SOUND, 0);
	pkt << id << spec.name << (float) (spec.gain * params.gain)
			<< (u8) params.type << pos << params.object << params.loop;

	for(const auto &i: dst_clients) {
		psound.clients.insert(i);
		m_clients.send(i, 0, &pkt, true);
	}
	return id;
}

s32 Server::playSound(const std::string &sound_file, v3s16 p,
		float max_hear_distance, float gain)
{
	SimpleSoundSpec spec;
	spec.name = sound_file;

	ServerSoundParams params;
	params.pos = v3f(p.X * BS, p.Y * BS, p.Z * BS);
	params.type = ServerSoundParams::Type::SSP_POSITIONAL;
	params.max_hear_distance = max_hear_distance * BS;
	params.gain = gain;

	return playSound(spec, params);
}

void Server::stopSound(s32 handle)
{
	// Get sound reference
	std::unordered_map<s32, ServerPlayingSound>::iterator i =
			m_playing_sounds.find(handle);
	if(i == m_playing_sounds.end())
		return;
	ServerPlayingSound &psound = i->second;

	NetworkPacket pkt(TOCLIENT_STOP_SOUND, 4);
	pkt << handle;

	for(const auto &i: psound.clients) {
		// Send as reliable
		m_clients.send(i, 0, &pkt, true);
	}
	// Remove sound reference
	m_playing_sounds.erase(i);
}

void Server::sendRemoveNode(v3s16 p, u16 ignore_id,
	std::vector<u16> *far_players, float far_d_nodes)
{
	float maxd = far_d_nodes*BS;
	v3f p_f = intToFloat(p, BS);

	NetworkPacket pkt(TOCLIENT_REMOVENODE, 6);
	pkt << p;

	std::vector<u16> clients = m_clients.getClientIDs();
	for(std::vector<u16>::iterator i = clients.begin();
		i != clients.end(); ++i) {
		if (far_players) {
			// Get player
			if(RemotePlayer *player = m_env->getPlayer(*i)) {
				// If player is far away, only set modified blocks not sent
				v3f player_pos = player->getPosition();
				if(player_pos.getDistanceFrom(p_f) > maxd) {
					far_players->push_back(*i);
					continue;
				}
			}
		}

		// Send as reliable
		m_clients.send(*i, 0, &pkt, true);
	}
}

void Server::sendAddNode(v3s16 p, MapNode n, u16 ignore_id,
		std::vector<u16> *far_players, float far_d_nodes,
		bool remove_metadata)
{
	float maxd = far_d_nodes*BS;
	v3f p_f = intToFloat(p, BS);

	std::vector<u16> clients = m_clients.getClientIDs();
	for(std::vector<u16>::iterator i = clients.begin();
			i != clients.end(); ++i) {

		if(far_players) {
			// Get player
			if(RemotePlayer *player = m_env->getPlayer(*i)) {
				// If player is far away, only set modified blocks not sent
				v3f player_pos = player->getPosition();
				if(player_pos.getDistanceFrom(p_f) > maxd) {
					far_players->push_back(*i);
					continue;
				}
			}
		}

		NetworkPacket pkt(TOCLIENT_ADDNODE, 6 + 2 + 1 + 1 + 1);
		RemoteClient* client = m_clients.getClientNoEx(*i);
		if (client != 0) {
			pkt << p << n.param0 << n.param1 << n.param2
					<< (u8) (remove_metadata ? 0 : 1);
		}
		// Send as reliable
		if (pkt.getSize() > 0)
			m_clients.send(*i, 0, &pkt, true);
	}
}

void Server::sendMetadataChanged(v3s16 p, std::vector<u16> *far_players,
	float far_d_nodes)
{
	v3s16 blockpos = getNodeBlockPos(p);
	float maxd = far_d_nodes * BS;
	v3f p_f = intToFloat(p, BS);
	MapBlock *block = m_env->getMap().getBlockNoCreateNoEx(blockpos);
	if (block)
		block->raiseModified(MOD_STATE_WRITE_NEEDED,
			MOD_REASON_REPORT_META_CHANGE);

	NetworkPacket pkt(TOCLIENT_NODEMETA_CHANGED, 0);
	std::vector<u16> clients = m_clients.getClientIDs();

	if (clients.size() > 0) {
		if (NodeMetadata *meta = m_env->getMap().getNodeMetadata(p)) {
			std::ostringstream os(std::ios::binary);
			meta->serialize(os);
			std::ostringstream oss(std::ios::binary);
			compressZlib(os.str(), oss);
			pkt << p;
			pkt.putLongString(oss.str());
		}
	}

	for (std::vector<u16>::iterator i = clients.begin();
			i != clients.end(); ++i) {
		if (far_players) {
			// Get player
			if (RemotePlayer *player = m_env->getPlayer(*i)) {
				// If player is far away, only set modified blocks not sent
				v3f player_pos = player->getPosition();
				if (player_pos.getDistanceFrom(p_f) > maxd) {
					far_players->push_back(*i);
					continue;
				}
			}
		}

		RemoteClient* client = m_clients.getClientNoEx(*i);
		if (client != 0) {
			// pre 27 clients expect whole mapblock
			if (client->net_proto_version < 27) {
				client->SetBlockNotSent(blockpos);
			} else if (pkt.getSize() > 0){
				m_clients.send(*i, 0, &pkt, true);
			}
		}
	}
}

void Server::setBlockNotSent(v3s16 p)
{
	std::vector<u16> clients = m_clients.getClientIDs();
	for(std::vector<u16>::iterator i = clients.begin();
		i != clients.end(); ++i) {
		RemoteClient *client = m_clients.getClientNoEx(*i);
		client->SetBlockNotSent(p);
	}
}

void Server::SendBlockNoLock(u16 peer_id, MapBlock *block, u8 ver, u16 net_proto_version)
{
	v3s16 p = block->getPos();

	/*
		Create a packet with the block in the right format
	*/

	std::ostringstream os(std::ios_base::binary);
	epixel::MapBlockDB blockdb_data;
	block->serialize(os, ver, false, blockdb_data);
	block->serializeNetworkSpecific(os, net_proto_version);
	std::string s = os.str();

	NetworkPacket pkt(TOCLIENT_BLOCKDATA, 2 + 2 + 2 + 2 + s.size(), peer_id);

	pkt << p;
	pkt.putRawString(s.c_str(), s.size());
	Send(&pkt);
}

void Server::SendBlocks(float dtime)
{
	std::vector<PrioritySortedBlockTransfer> queue;

	s32 total_sending = 0;
	std::lock_guard<std::mutex> envlock(m_map_mutex);

	{
		std::vector<u16> clients = m_clients.getClientIDs();

		for(std::vector<u16>::iterator i = clients.begin();
			i != clients.end(); ++i) {
			RemoteClient *client = m_clients.getClientNoEx(*i, CS_Active);

			if (client == NULL)
				continue;

			total_sending += client->SendingCount();
			client->GetNextBlocks(m_env,m_emerge, dtime, queue);
		}
	}

	// Sort.
	// Lowest priority number comes first.
	// Lowest is most important.
	std::sort(queue.begin(), queue.end());

	for(u32 i=0; i<queue.size(); i++)
	{
		//TODO: Calculate limit dynamically
		if(total_sending >=
				g_settings->get(S32SETTING_MAX_SIMULTANEOUS_BLOCK_SENDS_SERVER_TOTAL))
			break;

		PrioritySortedBlockTransfer q = queue[i];

		MapBlock *block = NULL;
		try
		{
			block = m_env->getMap().getBlockNoCreate(q.pos);
		}
		catch(InvalidPositionException &e)
		{
			continue;
		}

		RemoteClient *client = m_clients.getClientNoEx(q.peer_id, CS_Active);

		if(!client)
			continue;

		SendBlockNoLock(q.peer_id, block, client->serialization_version, client->net_proto_version);

		client->SentBlock(q.pos);
		total_sending++;
	}
}

void Server::fillMediaCache()
{
	logger.info("Server: Calculating media file checksums");

	// Collect all media file paths
	std::vector<std::string> paths;
	for (auto &mod: m_mods) {
		paths.push_back(mod.path + DIR_DELIM + "textures");
		paths.push_back(mod.path + DIR_DELIM + "sounds");
		paths.push_back(mod.path + DIR_DELIM + "media");
		paths.push_back(mod.path + DIR_DELIM + "models");
	}
	paths.push_back(porting::path_user + DIR_DELIM + "textures" + DIR_DELIM + "server");

	// Collect media file information from paths into cache
	for(auto &mediapath: paths) {
		std::vector<fs::DirListNode> dirlist = fs::GetDirListing(mediapath);
		for (u32 j = 0; j < dirlist.size(); j++) {
			if (dirlist[j].dir) // Ignode dirs
				continue;
			std::string filename = dirlist[j].name;
			// If name contains illegal characters, ignore the file
			if (!string_allowed(filename, TEXTURENAME_ALLOWED_CHARS)) {
				logger.info("Server: ignoring illegal file name: \"%s\"",
						filename.c_str());
				continue;
			}
			// If name is not in a supported format, ignore it
			const char *supported_ext[] = {
				".png", ".jpg", ".bmp", ".tga",
				".pcx", ".ppm", ".psd", ".wal", ".rgb",
				".ogg",
				".x", ".b3d", ".md2", ".obj",
				NULL
			};
			if (removeStringEnd(filename, supported_ext) == ""){
				logger.info("Server: ignoring unsupported file extension: \"%s\"",
						filename.c_str());
				continue;
			}
			// Ok, attempt to load the file and add to cache
			std::string filepath = mediapath + DIR_DELIM + filename;
			// Read data
			std::ifstream fis(filepath.c_str(), std::ios_base::binary);
			if (!fis.good()) {
				logger.error("Server::fillMediaCache(): Could not open \"%s\" for reading",
						filename.c_str());
				continue;
			}
			std::ostringstream tmp_os(std::ios_base::binary);
			bool bad = false;
			for(;;) {
				char buf[1024];
				fis.read(buf, 1024);
				std::streamsize len = fis.gcount();
				tmp_os.write(buf, len);
				if (fis.eof())
					break;
				if (!fis.good()) {
					bad = true;
					break;
				}
			}
			if(bad) {
				logger.error("Server::fillMediaCache(): Failed to read \"%s\"",
						filename.c_str());
				continue;
			}
			if(tmp_os.str().length() == 0) {
				logger.error("Server::fillMediaCache(): Empty file \"%s\"",
						filepath.c_str());
				continue;
			}

			unsigned char digest[SHA_DIGEST_LENGTH];
			SHA1((const unsigned char*)tmp_os.str().c_str(), tmp_os.str().length(), digest);
			const std::string sha1_hex = hex_encode((char*)digest, 20);
			std::string encbuf;
			Base64Encode(std::string((char*)digest, SHA_DIGEST_LENGTH), encbuf);
			// Put in list
			m_media[filename] = MediaInfo(filepath, encbuf, tmp_os.str());
			logger.debug("Server: %s is %s", sha1_hex.c_str(), filename.c_str());
		}
	}
}

void Server::sendMediaAnnouncement(u16 peer_id)
{
	logger.debug("Server: Announcing files to id(%d)", peer_id);

	// Make packet
	std::ostringstream os(std::ios_base::binary);

	NetworkPacket pkt(TOCLIENT_ANNOUNCE_MEDIA, 0, peer_id);
	pkt << (u16) m_media.size();

	for (std::unordered_map<std::string, MediaInfo>::iterator i = m_media.begin();
			i != m_media.end(); ++i) {
		pkt << i->first << i->second.sha1_digest;
	}

	pkt << g_settings->get("remote_media");
	Send(&pkt);
}

struct SendableMedia
{
	std::string name;
	std::string path;
	std::string data;

	SendableMedia(const std::string &name_="", const std::string &path_="",
			const std::string &data_=""):
		name(name_),
		path(path_),
		data(data_)
	{}
};

void Server::sendRequestedMedia(u16 peer_id,
		const std::vector<std::string> &tosend)
{
	logger.debug("Server::sendRequestedMedia(): "
			"Sending files to client");

	/* Read files */

	// Put 5kB in one bunch (this is not accurate)
	u32 bytes_per_bunch = 5000;

	std::vector< std::vector<SendableMedia> > file_bunches;
	file_bunches.push_back(std::vector<SendableMedia>());

	u32 file_size_bunch_total = 0;

	for(std::vector<std::string>::const_iterator i = tosend.begin();
			i != tosend.end(); ++i) {
		const std::string &name = *i;

		std::unordered_map<std::string, MediaInfo>::const_iterator it = m_media.find(name);

		if(it == m_media.end()) {
			logger.error("Server::sendRequestedMedia(): Client asked for "
					"unknown file \"%s\"", name.c_str());
			continue;
		}

		//TODO get path + name
		std::string tpath = it->second.path;

		// Put in list
		file_bunches[file_bunches.size()-1].push_back(
				SendableMedia(name, tpath, it->second.content));

		// Start next bunch if got enough data
		if(file_size_bunch_total >= bytes_per_bunch) {
			file_bunches.push_back(std::vector<SendableMedia>());
			file_size_bunch_total = 0;
		}

	}

	/* Create and send packets */

	u16 num_bunches = file_bunches.size();
	for(u16 i = 0; i < num_bunches; i++) {
		/*
			u16 command
			u16 total number of texture bunches
			u16 index of this bunch
			u32 number of files in this bunch
			for each file {
				u16 length of name
				string name
				u32 length of data
				data
			}
		*/

		NetworkPacket pkt(TOCLIENT_MEDIA, 4 + 0, peer_id);
		pkt << num_bunches << i << (u32) file_bunches[i].size();

		for(std::vector<SendableMedia>::iterator
				j = file_bunches[i].begin();
				j != file_bunches[i].end(); ++j) {
			pkt << j->name;
			pkt.putLongString(j->data);
		}

		logger.debug("Server::sendRequestedMedia(): bunch %d/%d files=%d size=%d",
				i, num_bunches, file_bunches[i].size(), pkt.getSize());
		Send(&pkt);
	}
}

void Server::sendDetachedInventory(const std::string &name, u16 peer_id)
{
	if(m_detached_inventories.count(name) == 0) {
		logger.error("sendDetachedInventory: \"%s\" not found", name.c_str());
		return;
	}

	Inventory *inv = m_detached_inventories[name];
	std::ostringstream os(std::ios_base::binary);

	os << serializeString(name);
	inv->serialize(os);

	// Make data buffer
	std::string s = os.str();

	NetworkPacket pkt(TOCLIENT_DETACHED_INVENTORY, 0, peer_id);
	pkt.putRawString(s.c_str(), s.size());

	if (peer_id != PEER_ID_INEXISTENT && peer_id != PEER_ID_BROADCAST) {
		Send(&pkt);
	}
	else {
		m_clients.sendToAll(0, &pkt, true);
	}
}

void Server::sendDetachedInventories(u16 peer_id)
{
	for(const auto &i: m_detached_inventories) {
		const std::string &name = i.first;
		//Inventory *inv = i->second;
		sendDetachedInventory(name, peer_id);
	}
}

/*
	Something random
*/

void Server::DiePlayer(PlayerSAO* playersao)
{
	// In some rare cases this can be NULL -- if the player is disconnected
	// when a Lua function modifies l_punch, for example
	if (!playersao)
		return;

	logger.info("Server::DiePlayer(): Player %s dies.",
			playersao->getPlayer()->getName());

	playersao->setHP(0);

	// Trigger scripted stuff
	playersao->onDie();
	m_script->on_dieplayer(playersao);

	SendPlayerHP(playersao);
	SendDeathscreen(playersao->getPeerID(), false, v3f(0,0,0));
}

void Server::RespawnPlayer(u16 peer_id)
{
	PlayerSAO *playersao = getPlayerSAO(peer_id);
	assert(playersao);

	logger.info("Server::DiePlayer(): Player %s respawns.",
			playersao->getPlayer()->getName());

	playersao->setHP(PLAYER_MAX_HP);
	playersao->setBreath(PLAYER_MAX_BREATH);

	SendPlayerHP(playersao);
	SendPlayerBreath(playersao);

	bool repositioned = m_script->on_respawnplayer(playersao);
	contrib_on_respawn(playersao);
	if(!repositioned){
		v3f pos = findSpawnPos();
		// setPos will send the new position to client
		playersao->setPos(pos);
	}
}


void Server::DenySudoAccess(u16 peer_id)
{
	NetworkPacket pkt(TOCLIENT_DENY_SUDO_MODE, 0, peer_id);
	Send(&pkt);
}


void Server::DenyAccessVerCompliant(u16 peer_id, u16 proto_ver, AccessDeniedCode reason,
		const std::string &str_reason, bool reconnect)
{
	if (proto_ver >= 25) {
		SendAccessDenied(peer_id, reason, str_reason, reconnect);
	} else {
		std::wstring wreason = utf8_to_wide(
			reason == SERVER_ACCESSDENIED_CUSTOM_STRING ? str_reason :
			accessDeniedStrings[(u8)reason]);
		SendAccessDenied_Legacy(peer_id, wreason);
	}

	m_clients.event(peer_id, CSE_SetDenied);
	if (peer_id < U16_MAX / 2) {
		m_con.DisconnectPeer(peer_id);
	}
	else {
		if (RemoteClient* client = m_clients.getClientNoEx(peer_id, CS_Created)) {
			if (client->getSessionType() != SESSION_TYPE_EPIXEL) { assert(false); }
			m_clients.m_epixel_netserver->Disconnect(((epixel::Session*)client)->getPeer());
			m_clients.DeleteClient(nullptr, peer_id);
		}
	}
}


void Server::DenyAccess(u16 peer_id, AccessDeniedCode reason, const std::string &custom_reason)
{
	SendAccessDenied(peer_id, reason, custom_reason);
	m_clients.event(peer_id, CSE_SetDenied);
	if (peer_id < U16_MAX / 2) {
		m_con.DisconnectPeer(peer_id);
	}
	else {
		if (RemoteClient* client = m_clients.getClientNoEx(peer_id, CS_Created)) {
			if (client->getSessionType() != SESSION_TYPE_EPIXEL) { assert(false); }
			m_clients.m_epixel_netserver->Disconnect(((epixel::Session*)client)->getPeer());
			m_clients.DeleteClient(nullptr, peer_id);
		}
	}
}

// 13/03/15: remove this function when protocol version 25 will become
// the minimum version for MT users, maybe in 1 year
void Server::DenyAccess_Legacy(u16 peer_id, const std::wstring &reason)
{
	SendAccessDenied_Legacy(peer_id, reason);
	m_clients.event(peer_id, CSE_SetDenied);
	if (peer_id < U16_MAX / 2) {
		m_con.DisconnectPeer(peer_id);
	}
	else {
		if (RemoteClient* client = m_clients.getClientNoEx(peer_id, CS_Created)) {
			if (client->getSessionType() != SESSION_TYPE_EPIXEL) { assert(false); }
			m_clients.m_epixel_netserver->Disconnect(((epixel::Session*)client)->getPeer());
			m_clients.DeleteClient(nullptr, peer_id);
		}
	}
}

void Server::acceptAuth(u16 peer_id, bool forSudoMode)
{
	if (!forSudoMode) {
		RemoteClient* client = getClient(peer_id, CS_Invalid);

		NetworkPacket resp_pkt(TOCLIENT_AUTH_ACCEPT, 1 + 6 + 8 + 4, peer_id);

		// Right now, the auth mechs don't change between login and sudo mode.
		u32 sudo_auth_mechs = client->allowed_auth_mechs;
		client->allowed_sudo_mechs = sudo_auth_mechs;

		resp_pkt << v3f(0,0,0) << (u64) m_env->getServerMap()->getSeed()
				<< g_settings->get(FSETTING_DEDICATED_SERVER_STEP)
				<< sudo_auth_mechs;

		Send(&resp_pkt);
		m_clients.event(peer_id, CSE_AuthAccept);
	} else {
		NetworkPacket resp_pkt(TOCLIENT_ACCEPT_SUDO_MODE, 1 + 6 + 8 + 4, peer_id);

		// We only support SRP right now
		u32 sudo_auth_mechs = AUTH_MECHANISM_FIRST_SRP;

		resp_pkt << sudo_auth_mechs;
		Send(&resp_pkt);
		m_clients.event(peer_id, CSE_SudoSuccess);
	}
}

static const char* ClientDeletionReason_str[CDR_INVALID_INPUT + 1] =
{
	"leaves game",
	"times out",
	"is refused",
	"send invalid datas"
};

void Server::DeleteClient(u16 peer_id, ClientDeletionReason reason)
{
	std::wstring message;

	/*
		Clear references to playing sounds
	*/
	for (std::unordered_map<s32, ServerPlayingSound>::iterator
			i = m_playing_sounds.begin();
			i != m_playing_sounds.end();) {
		ServerPlayingSound &psound = i->second;
		psound.clients.erase(peer_id);
		if(psound.clients.empty())
			m_playing_sounds.erase(i++);
		else
			++i;
	}

	RemotePlayer *player = m_env->getPlayer(peer_id);

	// Collect information about leaving in chat
	{
		if(player != NULL && reason != CDR_DENY)
		{
			std::wstring name = narrow_to_wide(player->getName());
			message += L"*** ";
			message += name;
			message += L" left the game.";
			if(reason == CDR_TIMEOUT)
				message += L" (timed out)";
		}
	}

	/* Run scripts and remove from environment */
	if(player != NULL) {
		PlayerSAO *playersao = player->getPlayerSAO();
		assert(playersao);

		m_script->on_leaveplayer(playersao);
		contrib_on_leaveplayer(playersao);

		playersao->disconnected();
	}

	/*
		Print out action
	*/
	{
		if(player != NULL && reason != CDR_DENY) {
			std::ostringstream os(std::ios_base::binary);
			std::vector<u16> clients = m_clients.getClientIDs();

			for(std::vector<u16>::iterator i = clients.begin();
				i != clients.end(); ++i) {
				// Get player
				RemotePlayer *player = m_env->getPlayer(*i);
				if (!player)
					continue;

				// Get name of player
				os << player->getName() << " ";
			}

			logger.notice("%s %s. List of players: %s", player->getName(), ClientDeletionReason_str[reason],
					os.str().c_str());
		}
	}
	{
		std::lock_guard<std::mutex> env_lock(m_ao_mutex);
		m_clients.DeleteClient(player, peer_id);
	}

	// Send leave chat message to all remaining clients
	if (!message.empty())
		SendChatMessage(PEER_ID_BROADCAST, message);
}

void Server::UpdateCrafting(RemotePlayer* player)
{
	// Get a preview for crafting
	ItemStack preview;
	InventoryLocation loc;
	Inventory* inv = player->getPlayerSAO()->getInventory();
	loc.setPlayer(player->getName());
	std::vector<ItemStack> output_replacements;
	getCraftingResult(inv, preview, output_replacements, false, this);
	m_env->getScriptIface()->item_CraftPredict(preview, player->getPlayerSAO(), inv->getList("craft"), loc);

	// Put the new preview in
	InventoryList *plist = inv->getList("craftpreview");
	sanity_check(plist);
	sanity_check(plist->getSize() >= 1);
	plist->changeItem(0, preview);
}

RemoteClient* Server::getClient(u16 peer_id, ClientState state_min)
{
	RemoteClient *client = getClientNoEx(peer_id,state_min);
	if(!client)
		throw ClientNotFoundException("Client not found");

	return client;
}
RemoteClient* Server::getClientNoEx(u16 peer_id, ClientState state_min)
{
	return m_clients.getClientNoEx(peer_id, state_min);
}

std::string Server::getPlayerName(u16 peer_id)
{
	RemotePlayer *player = m_env->getPlayer(peer_id);
	if(player == NULL)
		return "[id="+itos(peer_id)+"]";
	return player->getName();
}

PlayerSAO* Server::getPlayerSAO(u16 peer_id)
{
	RemotePlayer *player = m_env->getPlayer(peer_id);
	if(player == NULL)
		return NULL;
	return player->getPlayerSAO();
}

std::wstring Server::getStatusString()
{
	std::wostringstream os(std::ios_base::binary);
	os << L"# Server: ";
	// Version
	os << L"version=" << narrow_to_wide(g_version_string);
	// Uptime
	os << L", uptime: " << secondsToHumanString(m_uptime).c_str();
	// Max lag estimate
	os << L", max_lag=" << m_env->getMaxLagEstimate();
	// Information about clients
	bool first = true;
	std::vector<u16> clients = m_clients.getClientIDs();
	os << L", " << clients.size() << L" players {";
	for(std::vector<u16>::iterator i = clients.begin();
		i != clients.end(); ++i) {
		// Get player
		RemotePlayer *player = m_env->getPlayer(*i);
		// Get name of player
		std::wstring name = L"unknown";
		if(player != NULL)
			name = narrow_to_wide(player->getName());
		// Add name to information string
		if(!first)
			os << L", ";
		else
			first = false;
		os << name;
	}
	os << L"}";
	if(!((ServerMap*)(&m_env->getMap()))->isSavingEnabled())
		os<<std::endl<<L"# Server: "<<" WARNING: Map saving is disabled.";
	if(g_settings->get("motd") != "")
		os<<std::endl<<L"# Server: "<<narrow_to_wide(g_settings->get("motd"));
	return os.str();
}

std::set<std::string> Server::getPlayerEffectivePrivs(const std::string &name) const
{
	RemotePlayer *player = m_env->getPlayer(name.c_str());
	if (!player || !player->getPlayerSAO()) {
		return std::set<std::string>();
	}
	return player->getPlayerSAO()->getPrivs();
}

std::set<std::string> Server::getPlayerEffectivePrivs(u16 peer_id) const
{
	RemotePlayer *player = m_env->getPlayer(peer_id);
	if (!player || !player->getPlayerSAO()) {
		return std::set<std::string>();
	}
	return player->getPlayerSAO()->getPrivs();
}

std::set<std::string> Server::getPlayerEffectivePrivsByDBId(u32 player_id) const
{
	RemotePlayer *player = m_env->getPlayerByDBId(player_id);
	if (!player || !player->getPlayerSAO()) {
		return std::set<std::string>();
	}
	return player->getPlayerSAO()->getPrivs();
}

void Server::reportPrivsModified(const std::string &name)
{
	if (name.empty()) {
		std::vector<u16> clients = m_clients.getClientIDs();
		for(std::vector<u16>::iterator i = clients.begin();
				i != clients.end(); ++i) {
			RemotePlayer *player = m_env->getPlayer(*i);
			reportPrivsModified(player->getName());
		}
	} else {
		RemotePlayer *player = m_env->getPlayer(name.c_str());
		if (!player)
			return;
		SendPlayerPrivileges(player);
		PlayerSAO *sao = player->getPlayerSAO();
		if (!sao)
			return;
		sao->updatePrivileges(
				getPlayerEffectivePrivs(name),
#ifndef SERVER
				isSingleplayer());
#else
				false);
#endif
	}
}

void Server::reportInventoryFormspecModified(const std::string &name)
{
	RemotePlayer *player = m_env->getPlayer(name.c_str());
	if (!player)
		return;

	SendPlayerInventoryFormspec(player);
}

void Server::notifyPlayer(const char *name, const std::wstring &msg)
{
	// m_env will be NULL if the server is initializing
	if (!m_env)
		return;

	RemotePlayer *player = m_env->getPlayer(name);
	if (!player)
		return;

	if (player->getPeerID() == PEER_ID_INEXISTENT)
		return;

	SendChatMessage(player->getPeerID(), msg);
}

bool Server::showFormspec(const char *playername, const std::string &formspec,
	const std::string &formname)
{
	// m_env will be NULL if the server is initializing
	if (!m_env)
		return false;

	RemotePlayer *player = m_env->getPlayer(playername);
	if (!player)
		return false;

	SendShowFormspecMessage(player->getPeerID(), formspec, formname);
	return true;
}

u32 Server::hudAdd(RemotePlayer *player, HudElement *form) {

	if (!player || !form)
		return -1;

	u32 id = player->addHud(form);
	SendHUDAdd(player->getPeerID(), id, form);

	return id;
}

bool Server::hudRemove(RemotePlayer *player, u32 id) {
	if (!player)
		return false;

	bool todel = player->removeHud(id);

	if (!todel)
		return false;

	SendHUDRemove(player->getPeerID(), id);
	return true;
}

bool Server::hudChange(RemotePlayer *player, u32 id, HudElementStat stat, void *data)
{
	if (!player)
		return false;

	SendHUDChange(player->getPeerID(), id, stat, data);
	return true;
}

bool Server::hudSetFlags(RemotePlayer *player, u32 flags, u32 mask)
{
	if (!player)
		return false;

	SendHUDSetFlags(player->getPeerID(), flags, mask);
	player->hud_flags &= ~mask;
	player->hud_flags |= flags;

	PlayerSAO* playersao = player->getPlayerSAO();

	if (playersao == NULL)
		return false;

	m_script->player_event(playersao, "hud_changed");
	return true;
}

bool Server::hudSetHotbarItemcount(RemotePlayer *player, s32 hotbar_itemcount)
{
	if (!player)
		return false;
	if (hotbar_itemcount <= 0 || hotbar_itemcount > HUD_HOTBAR_ITEMCOUNT_MAX)
		return false;

	player->setHotbarItemcount(hotbar_itemcount);
	std::ostringstream os(std::ios::binary);
	writeS32(os, hotbar_itemcount);
	SendHUDSetParam(player->getPeerID(), HUD_PARAM_HOTBAR_ITEMCOUNT, os.str());
	return true;
}

s32 Server::hudGetHotbarItemcount(RemotePlayer *player)
{
	if (!player)
		return 0;
	return player->getHotbarItemcount();
}

void Server::hudSetHotbarImage(RemotePlayer *player, const std::string &name)
{
	if (!player)
		return;

	player->setHotbarImage(name);
	SendHUDSetParam(player->getPeerID(), HUD_PARAM_HOTBAR_IMAGE, name);
}

std::string Server::hudGetHotbarImage(RemotePlayer *player)
{
	if (!player)
		return "";
	return player->getHotbarImage();
}

void Server::hudSetHotbarSelectedImage(RemotePlayer *player, const std::string &name)
{
	if (!player)
		return;

	player->setHotbarSelectedImage(name);
	SendHUDSetParam(player->getPeerID(), HUD_PARAM_HOTBAR_SELECTED_IMAGE, name);
}

std::string Server::hudGetHotbarSelectedImage(RemotePlayer *player)
{
	if (!player)
		return "";

	return player->getHotbarSelectedImage();
}

bool Server::setLocalPlayerAnimations(RemotePlayer *player,
	v2s32 animation_frames[4], f32 frame_speed)
{
	if (!player)
		return false;

	SendLocalPlayerAnimations(player->getPeerID(), animation_frames, frame_speed);
	return true;
}

bool Server::setPlayerEyeOffset(RemotePlayer *player, v3f first, v3f third)
{
	if (!player)
		return false;

	player->eye_offset_first = first;
	player->eye_offset_third = third;
	SendEyeOffset(player->getPeerID(), first, third);
	return true;
}

bool Server::setSky(RemotePlayer *player, const video::SColor &bgcolor,
	const std::string &type, const std::vector<std::string> &params)
{
	if (!player)
		return false;

	player->setSky(bgcolor, type, params);
	SendSetSky(player->getPeerID(), bgcolor, type, params);
	return true;
}

bool Server::overrideDayNightRatio(RemotePlayer *player, bool do_override,
	float ratio)
{
	if (!player)
		return false;

	player->overrideDayNightRatio(do_override, ratio);
	SendOverrideDayNightRatio(player->getPeerID(), do_override, ratio);
	return true;
}

void Server::notifyPlayers(const std::wstring &msg)
{
	SendChatMessage(PEER_ID_BROADCAST,msg);
}

void Server::spawnParticle(const std::string &playername, v3f pos,
	v3f velocity, v3f acceleration,
	float expirationtime, float size, bool
	collisiondetection, bool vertical, const std::string &texture)
{
	// m_env will be NULL if the server is initializing
	if (!m_env)
		return;

	u16 peer_id = PEER_ID_INEXISTENT;
	if (!playername.empty()) {
		RemotePlayer* player = m_env->getPlayer(playername.c_str());
		if (!player)
			return;
		peer_id = player->getPeerID();
	}

	SendSpawnParticle(peer_id, pos, velocity, acceleration,
			expirationtime, size, collisiondetection, vertical, texture);
}

u32 Server::addParticleSpawner(u16 amount, float spawntime,
	v3f minpos, v3f maxpos, v3f minvel, v3f maxvel, v3f minacc, v3f maxacc,
	float minexptime, float maxexptime, float minsize, float maxsize,
	bool collisiondetection, bool vertical, const std::string &texture,
	const std::string &playername)
{
	// m_env will be NULL if the server is initializing
	if (!m_env)
		return -1;

	u16 peer_id = PEER_ID_INEXISTENT;
	if (playername != "") {
		RemotePlayer* player = m_env->getPlayer(playername.c_str());
		if (!player)
			return -1;
		peer_id = player->getPeerID();
	}

	u32 id = m_env->addParticleSpawner(spawntime);
	SendAddParticleSpawner(peer_id, amount, spawntime,
		minpos, maxpos, minvel, maxvel, minacc, maxacc,
		minexptime, maxexptime, minsize, maxsize,
		collisiondetection, vertical, texture, id);

	return id;
}

void Server::deleteParticleSpawner(const std::string &playername, u32 id)
{
	// m_env will be NULL if the server is initializing
	if (!m_env)
		throw ServerError("Can't delete particle spawners during initialisation!");

	u16 peer_id = PEER_ID_INEXISTENT;
	if (playername != "") {
		RemotePlayer* player = m_env->getPlayer(playername.c_str());
		if (!player)
			return;
		peer_id = player->getPeerID();
	}

	m_env->deleteParticleSpawner(id);
	SendDeleteParticleSpawner(peer_id, id);
}

void Server::deleteParticleSpawnerAll(u32 id)
{
	m_env->deleteParticleSpawner(id);
	SendDeleteParticleSpawner(PEER_ID_INEXISTENT, id);
}

Inventory* Server::createDetachedInventory(const std::string &name)
{
	if(m_detached_inventories.count(name) > 0){
		logger.info("Server clearing detached inventory \"%s\"", name.c_str());
		delete m_detached_inventories[name];
	}
	else {
		logger.info("Server creating detached inventory \"%s\"", name.c_str());
	}
	Inventory *inv = new Inventory(m_itemdef);
	sanity_check(inv);
	m_detached_inventories[name] = inv;
	//TODO find a better way to do this
	sendDetachedInventory(name,PEER_ID_INEXISTENT);
	return inv;
}

u16 Server::allocateUnknownNodeId(const std::string &name)
{
	return m_nodedef->allocateDummy(name);
}

const ModSpec *Server::getModSpec(const std::string &modname) const
{
	std::vector<ModSpec>::const_iterator it;
	for (it = m_mods.begin(); it != m_mods.end(); ++it) {
		const ModSpec &mod = *it;
		if (mod.name == modname)
			return &mod;
	}
	return NULL;
}

void Server::getModNames(std::vector<std::string> &modlist)
{
	std::vector<ModSpec>::iterator it;
	for (it = m_mods.begin(); it != m_mods.end(); ++it)
		modlist.push_back(it->name);
}

std::string Server::getBuiltinLuaPath()
{
	return porting::path_share + DIR_DELIM + "builtin";
}

v3f Server::findSpawnPos()
{
	ServerMap* map = m_env->getServerMap();
	v3f nodeposf;
	if (g_settings->getV3FNoEx("static_spawnpoint", nodeposf)) {
		return nodeposf * BS;
	}

	s16 water_level = map->getWaterLevel();
	s16 vertical_spawn_range = g_settings->get(S16SETTING_VERTICAL_SPAWN_RANGE);
	bool is_good = false;

	// Try to find a good place a few times
	for (s32 i = 0; i < 1000 && !is_good; i++) {
		s32 range = 1 + i;
		// We're going to try to throw the player to this position
		v2s16 nodepos2d = v2s16(
				-range + (myrand() % (range * 2)),
				-range + (myrand() % (range * 2)));

		// Get ground height at point
		s16 groundheight = map->findGroundLevel(nodepos2d);
		// Don't go underwater or to high places
		if (groundheight <= water_level ||
				groundheight > water_level + vertical_spawn_range)
			continue;

		v3s16 nodepos(nodepos2d.X, groundheight, nodepos2d.Y);

		s32 air_count = 0;
		for (s32 j = 0; j < 10; j++) {
			v3s16 blockpos = getNodeBlockPos(nodepos);
			map->emergeBlock(blockpos, true);
			content_t c = map->getNode(nodepos).getContent();
			if (c == CONTENT_AIR || c == CONTENT_IGNORE) {
				air_count++;
				if (air_count >= 2) {
					nodeposf = intToFloat(nodepos, BS);
					// Don't spawn the player outside map boundaries
					if (objectpos_over_limit(nodeposf))
						continue;
					is_good = true;
					break;
				}
			}
			nodepos.Y++;
		}
	}

	return nodeposf;
}

PlayerSAO* Server::emergePlayer(const char *name, u16 peer_id, u16 proto_version)
{
	// Try to retrieve the current client
	RemoteClient* client = m_clients.getClientNoEx(peer_id, CS_Created);
	if (!client) {
		logger.crit("Unable to find client associated with peer_id %d", peer_id);
		return NULL;
	}

	/*
		Try to get an existing player
	*/
	RemotePlayer *player = m_env->getPlayer(name);

	// If player is already connected, cancel
	if(player && player->getPeerID() != PEER_ID_INEXISTENT) {
		logger.info("emergePlayer(): Player already connected");
		return NULL;
	}

	/*
		If player with the wanted peer_id already exists, cancel.
	*/
	if (m_env->getPlayer(peer_id) != NULL) {
		logger.info("emergePlayer(): Player with wrong name but same"
				" peer_id already exists");
		return NULL;
	}

	// Load player if it isn't already loaded
	if (!player) {
		player = new RemotePlayer(name);

		// Add player to environment
		m_env->addPlayer(player);
	}

	player->setRemoteClient(client);

	// Load player privileges
	u32 user_id = m_auth_database->userExists(name);
	if (!user_id) {
		logger.crit("Unable to find previously created player %s", name);
		return NULL;
	}

	player->setDBId(user_id);

	std::set<std::string> privs;

	m_game_database->loadUserPrivs(user_id, privs);

	// Create a new player active object
	PlayerSAO *playersao = new PlayerSAO(m_env, player, peer_id, privs,
#ifndef SERVER
			isSingleplayer());
#else
			false);
#endif

	// PlayerSAO
	m_game_database->loadPlayer(player, playersao);
	m_game_database->loadPlayerExtendedAttributes(playersao);
	m_game_database->loadPlayerIgnores(playersao);

	player->setProtocolVersion(proto_version);

	/* Clean up old HUD elements from previous sessions */
	player->clearHud();

	/* Add object to environment */
	m_env->addActiveObject(playersao);

	/* If player is new, remove flag and find new spawning position */
	if (client->isNewPlayer()) {
		client->markAsNewPlayer(false);
		// Set player position
		logger.info("Server: Finding spawn place for new player \"%s\"", name);
		playersao->setPos(findSpawnPos());
		m_script->on_newplayer(playersao);

		// Save player first time to permit saving privs
		m_game_database->savePlayer(player);
		m_game_database->addPrivs(user_id, defaultPrivs);

		// And load default privs in memory
		for (const auto &priv: defaultPrivs) {
			playersao->addPriv(priv);
		}
	}

	// Check if playre is not outside map limits
	if (objectpos_over_limit(player->getPosition())) {
		logger.notice("Respawn position for player \"%s"
			"\" outside limits, resetting", name);
		playersao->setPos(findSpawnPos());
	}

	return playersao;
}

void dedicated_server_loop(Server &server, bool &kill)
{
	logger.debug("dedicated_server_loop()");

	while (!server.getShutdownRequested() && !kill) {
		float steplen = g_settings->get(FSETTING_DEDICATED_SERVER_STEP);
		sleep_ms((int)(steplen * 1000.0));
		server.step(steplen);
	}

	logger.info("Dedicated server quitting");
	if(g_settings->get(BOOLSETTING_SERVER_ANNOUNCE)) {
		ServerList::sendLegacyAnnounce("delete", server.m_bind_addr.getPort());
	}
}
