/*
Minetest
Copyright (C) 2010-2013 celeron55, Perttu Ahola <celeron55@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SERVER_HEADER
#define SERVER_HEADER

#include <string>
#include <list>
#include <map>
#include <unordered_map>
#include <vector>
#include "irr_v3d.h"
#include "network/connection.h"
#include "map.h"
#include "hud.h"
#include "gamedef.h"
#include "serialization.h" // For SER_FMT_VER_INVALID
#include "mods.h"
#include "inventorymanager.h"
#include "subgame.h"
#include "contrib/awards.h"
#include "util/numeric.h"
#include "threads/threadutils.h"
#include "environment.h"
#include "clientiface.h"

#define PP(x) "("<<(x).X<<","<<(x).Y<<","<<(x).Z<<")"

class IWritableItemDefManager;
class IWritableNodeDefManager;
class IWritableCraftDefManager;
class BanManager;
class EventManager;
class Inventory;
class PlayerSAO;
class IRollbackManager;
struct RollbackAction;
class EmergeManager;
class GameScripting;
class ServerEnvironment;
struct SimpleSoundSpec;
class ServerThread;
class Database_SQLite3;
class Database_PostgreSQL;
class AuthDatabase;
class NetworkPacket;

namespace epixel
{
	class AreaMgr;
	struct Spell;
	struct Achievement;
	struct AchievementProgress;
	class BanMgr;
	class ChatHandler;
	class ConsoleThread;
	class CreatureStore;
	class NetworkServer;
	struct ISChatMessage;
#ifdef SERVER
	class ISChatClient;
#endif
	template <class T>
	class DatabasePool;
}

enum ClientDeletionReason {
	CDR_LEAVE,
	CDR_TIMEOUT,
	CDR_DENY,
	CDR_INVALID_INPUT,
};

class MapEditEventIgnorer
{
public:
	MapEditEventIgnorer(bool *flag):
		m_flag(flag)
	{
		if(*m_flag == false)
			*m_flag = true;
		else
			m_flag = NULL;
	}

	~MapEditEventIgnorer()
	{
		if(m_flag)
		{
			assert(*m_flag);
			*m_flag = false;
		}
	}

private:
	bool *m_flag;
};

class MapEditEventAreaIgnorer
{
public:
	MapEditEventAreaIgnorer(VoxelArea *ignorevariable, const VoxelArea &a):
		m_ignorevariable(ignorevariable)
	{
		if(m_ignorevariable->getVolume() == 0)
			*m_ignorevariable = a;
		else
			m_ignorevariable = NULL;
	}

	~MapEditEventAreaIgnorer()
	{
		if(m_ignorevariable)
		{
			assert(m_ignorevariable->getVolume() != 0);
			*m_ignorevariable = VoxelArea();
		}
	}

private:
	VoxelArea *m_ignorevariable;
};

struct MediaInfo
{
	std::string path;
	std::string sha1_digest;
	std::string content;

	MediaInfo(const std::string &path_ = "",
			  const std::string &sha1_digest_ = "",
			  const std::string &content_ = ""):
		path(path_),
		sha1_digest(sha1_digest_),
		content(content_)
	{
	}
};

struct ServerSoundParams
{
	float gain;
	std::string to_player;
	enum Type{
		SSP_LOCAL=0,
		SSP_POSITIONAL=1,
		SSP_OBJECT=2
	} type;
	v3f pos;
	u16 object;
	float max_hear_distance;
	bool loop;

	ServerSoundParams():
		gain(1.0),
		to_player(""),
		type(SSP_LOCAL),
		pos(0,0,0),
		object(0),
		max_hear_distance(32*BS),
		loop(false)
	{}

	v3f getPos(ServerEnvironment *env, bool *pos_exists) const;
};

struct ServerPlayingSound
{
	ServerSoundParams params;
	std::set<u16> clients; // peer ids
};

class Server : public con::PeerHandler,
		public InventoryManager, public IGameDef
{
public:
	/*
		NOTE: Every public method should be thread-safe
	*/

	Server(
		const std::string &path_world,
		const SubgameSpec &gamespec,
#ifndef SERVER
		bool simple_singleplayer_mode,
#endif
		bool ipv6
	);
	~Server();
	void start(Address bind_addr);
	void stop();
	// This is mainly a way to pass the time to the server.
	// Actual processing is done in an another thread.
	void step(float dtime);
	// This is run by ServerThread and does the actual processing
	void AsyncRunStep(bool initial_step=false);
	void Receive();
	PlayerSAO* StageTwoClientInit(u16 peer_id);

	/*
	 * Command Handlers
	 */

	void handleCommand(NetworkPacket* pkt);

	void handleCommand_Null(NetworkPacket* pkt) {}
	void handleCommand_Deprecated(NetworkPacket* pkt);
	void handleCommand_Init(NetworkPacket* pkt);
	void handleCommand_Init_Legacy(NetworkPacket* pkt);
	void handleCommand_Init2(NetworkPacket* pkt);
	void handleCommand_RequestMedia(NetworkPacket* pkt);
	void handleCommand_ReceivedMedia(NetworkPacket* pkt);
	void handleCommand_ClientReady(NetworkPacket* pkt);
	void handleCommand_GotBlocks(NetworkPacket* pkt);
	void handleCommand_PlayerPos(NetworkPacket* pkt);
	void handleCommand_DeletedBlocks(NetworkPacket* pkt);
	void handleCommand_InventoryAction(NetworkPacket* pkt);
	void handleCommand_ChatMessage(NetworkPacket* pkt);
	void handleCommand_Damage(NetworkPacket* pkt);
	void handleCommand_Password(NetworkPacket* pkt);
	void handleCommand_PlayerItem(NetworkPacket* pkt);
	void handleCommand_Respawn(NetworkPacket* pkt);
	void handleCommand_Interact(NetworkPacket* pkt);
	void handleCommand_RemovedSounds(NetworkPacket* pkt);
	void handleCommand_NodeMetaFields(NetworkPacket* pkt);
	void handleCommand_InventoryFields(NetworkPacket* pkt);
	void handleCommand_FirstSrp(NetworkPacket* pkt);
	void handleCommand_SrpBytesA(NetworkPacket* pkt);
	void handleCommand_SrpBytesM(NetworkPacket* pkt);

	bool handle_CommonInit(const u16 peer_id, const u16 net_proto_version, const std::string &addr_s, const std::string &playername);
	void ProcessData(NetworkPacket *pkt);

	void Send(NetworkPacket* pkt);

	// Environment must be locked when called
	void setTimeOfDay(u32 time);

	/*
		Shall be called with the environment locked.
		This is accessed by the map, which is inside the environment,
		so it shouldn't be a problem.
	*/
	void onMapEditEvent(MapEditEvent *event);

	/*
		Shall be called with the environment and the connection locked.
	*/
	Inventory* getInventory(const InventoryLocation &loc);
	void setInventoryModified(const InventoryLocation &loc, bool playerSend = true);

	// Connection must be locked when called
	std::wstring getStatusString();

	// read shutdown state
	inline bool getShutdownRequested()
			{ return m_shutdown_requested; }

	// request server to shutdown
	inline void requestShutdown() { m_shutdown_requested = true; }
	void requestShutdown(const std::string &msg, const float stime = 0, bool reconnect = false)
	{
		if (stime == 0) {
			m_shutdown_requested = true;
		}
		m_shutdown_msg = msg;
		m_shutdown_ask_reconnect = reconnect;
		m_shutdown_timer = stime;
	}

	// Returns -1 if failed, sound handle on success
	// Envlock
	s32 playSound(const SimpleSoundSpec &spec, const ServerSoundParams &params);
	s32 playSound(const std::string &sound_file, v3s16 p, float max_hear_distance = 32 * BS, float gain = 1.0f);
	void stopSound(s32 handle);

	// Envlock
	std::set<std::string> getPlayerEffectivePrivs(const std::string &name) const;
	std::set<std::string> getPlayerEffectivePrivs(u16 peer_id) const;
	std::set<std::string> getPlayerEffectivePrivsByDBId(u32 player_id) const;

	inline bool checkPriv(const std::string &name, const std::string &priv) const
	{
		const std::set<std::string> &privs = getPlayerEffectivePrivs(name);
		return (privs.count(priv) != 0);
	}
	inline bool checkPriv(const u32 peer_id, const std::string &priv) const
	{
		const std::set<std::string> &privs = getPlayerEffectivePrivs(peer_id);
		return (privs.count(priv) != 0);
	}

	inline bool checkPrivByDBId(const u32 player_id, const std::string &priv) const
	{
		if (player_id == 0) { return false; } // No player id it's not a player, set priv to false
		const std::set<std::string> &privs = getPlayerEffectivePrivsByDBId(player_id);
		return (privs.count(priv) != 0);
	}

	void reportPrivsModified(const std::string &name=""); // ""=all
	void reportInventoryFormspecModified(const std::string &name);

	void notifyPlayer(const char *name, const std::wstring &msg);
	void notifyPlayers(const std::wstring &msg);
	void spawnParticle(const std::string &playername,
		v3f pos, v3f velocity, v3f acceleration,
		float expirationtime, float size,
		bool collisiondetection, bool vertical, const std::string &texture);

	u32 addParticleSpawner(u16 amount, float spawntime,
		v3f minpos, v3f maxpos,
		v3f minvel, v3f maxvel,
		v3f minacc, v3f maxacc,
		float minexptime, float maxexptime,
		float minsize, float maxsize,
		bool collisiondetection, bool vertical, const std::string &texture,
		const std::string &playername);

	void deleteParticleSpawner(const std::string &playername, u32 id);
	void deleteParticleSpawnerAll(u32 id);

	// Creates or resets inventory
	Inventory* createDetachedInventory(const std::string &name);

	// Envlock and conlock should be locked when using scriptapi
	GameScripting *getScriptIface(){ return m_script; }

	// IGameDef interface
	// Under envlock
	virtual IItemDefManager* getItemDefManager() { return (IItemDefManager*)m_itemdef; }
	virtual INodeDefManager* getNodeDefManager() { return (INodeDefManager*)m_nodedef; }
	virtual ICraftDefManager* getCraftDefManager() { return (ICraftDefManager*)m_craftdef; }
	virtual ITextureSource* getTextureSource() { return NULL; }
	virtual IShaderSource* getShaderSource() { return NULL; }
	virtual u16 allocateUnknownNodeId(const std::string &name);
	virtual ISoundManager* getSoundManager() { return NULL; }
	virtual MtEventManager* getEventManager() { return (MtEventManager*)m_event; }
	virtual scene::ISceneManager* getSceneManager()	{ return NULL; }
	virtual EmergeManager *getEmergeManager() { return m_emerge; }

	IWritableItemDefManager* getWritableItemDefManager() { return m_itemdef; }
	IWritableNodeDefManager* getWritableNodeDefManager() { return m_nodedef; }
	IWritableCraftDefManager* getWritableCraftDefManager() { return m_craftdef; }

	const ModSpec* getModSpec(const std::string &modname) const;
	void getModNames(std::vector<std::string> &modlist);
	std::string getBuiltinLuaPath();
	inline std::string getWorldPath() const
			{ return m_path_world; }

#ifndef SERVER
	inline bool isSingleplayer()
			{ return m_singleplayer_mode; }
#endif

	inline void setAsyncFatalError(const std::string &error)
	{
		m_fatalerror_mutex.lock();
		m_async_fatal_error = error;
		m_fatalerror_mutex.unlock();
	}

	bool showFormspec(const char *name, const std::string &formspec, const std::string &formname);
	inline void showFormspec(u16 peer_id, const std::string &formspec, const std::string &formname)
	{
		SendShowFormspecMessage(peer_id, formspec, formname);
	}

	Map & getMap() { return m_env->getMap(); }
	ServerEnvironment & getEnv() { return *m_env; }

	u32 hudAdd(RemotePlayer *player, HudElement *element);
	bool hudRemove(RemotePlayer *player, u32 id);
	bool hudChange(RemotePlayer *player, u32 id, HudElementStat stat, void *value);
	bool hudSetFlags(RemotePlayer *player, u32 flags, u32 mask);
	bool hudSetHotbarItemcount(RemotePlayer *player, s32 hotbar_itemcount);
	s32 hudGetHotbarItemcount(RemotePlayer *player);
	void hudSetHotbarImage(RemotePlayer *player, const std::string &name);
	std::string hudGetHotbarImage(RemotePlayer *player);
	void hudSetHotbarSelectedImage(RemotePlayer *player, const std::string &name);
	std::string hudGetHotbarSelectedImage(RemotePlayer *player);

	Address getPeerAddress(u16 peer_id);
	const std::string getPeerAddressStr(u16 peer_id);

	bool setLocalPlayerAnimations(RemotePlayer *player, v2s32 animation_frames[4], f32 frame_speed);
	bool setPlayerEyeOffset(RemotePlayer *player, v3f first, v3f third);

	bool setSky(RemotePlayer *player, const video::SColor &bgcolor,
			const std::string &type, const std::vector<std::string> &params);

	bool overrideDayNightRatio(RemotePlayer *player, bool do_override,
			float brightness);

	/* con::PeerHandler implementation. */
	void peerAdded(con::Peer *peer);
	void deletingPeer(con::Peer *peer, bool timeout);

	void DenySudoAccess(u16 peer_id);
	void DenyAccessVerCompliant(u16 peer_id, u16 proto_ver, AccessDeniedCode reason,
		const std::string &str_reason = "", bool reconnect = false);
	void DenyAccess(u16 peer_id, AccessDeniedCode reason, const std::string &custom_reason="");
	void acceptAuth(u16 peer_id, bool forSudoMode);
	void DenyAccess_Legacy(u16 peer_id, const std::wstring &reason);
	bool getClientConInfo(u16 peer_id, con::rtt_stat_type type,float* retval);
	bool getClientInfo(u16 peer_id,ClientState* state, u32* uptime,
			u8* ser_vers, u16* prot_vers, u8* major, u8* minor, u8* patch,
			std::string* vers_string);

	void SendPlayerHPOrDie(PlayerSAO *player);
	void SendPlayerBreath(PlayerSAO *player);
	void SendInventory(PlayerSAO* playerSAO);
	void SendMovePlayer(u16 peer_id);
	void SendHUDChange(u16 peer_id, u32 id, HudElementStat stat, void *value);
	void SendChatMessage(u16 peer_id, const std::wstring &message);
	void sendMetadataChanged(v3s16 p, std::vector<u16> *far_players=NULL,
			float far_d_nodes=100);
	void sendDetachedInventory(const std::string &name, u16 peer_id);

	// Award
	std::unordered_map<u32,epixel::Achievement> getAchievements() { return m_map_achievement; }
	void applyAwardStep(PlayerSAO *player_sao, const std::string &nodename, epixel::AchievementType at);

	std::string getPlayerName(u16 peer_id);
	// When called, environment mutex should be locked
	PlayerSAO* getPlayerSAO(u16 peer_id);

	// Bind address
	Address m_bind_addr;

	epixel::AreaMgr* getAreaMgr() { return m_area_mgr; }
	epixel::BanMgr* getBanMgr() { return m_ban_mgr; }
	epixel::CreatureStore* getCreatureStore() { return m_creaturestore; }
	GameDatabase* getGameDatabase() { return m_game_database; }
	AuthDatabase* getAuthDatabase() { return m_auth_database; }
#ifndef SERVER
	Database_SQLite3* getMapDatabase();
#else
	Database_PostgreSQL* getMapDatabase();
#endif
	u32 getClientsCount() { return m_clients.getClientsCount(); }

	// Fine lock only on Server Active Objects
	std::mutex m_ao_mutex;
	std::mutex m_map_mutex;

	// Epixel specific
	void load_server_rules();
	inline void showRulesFormspec(const u16 peer_id) {
		showFormspec(peer_id, m_server_rules_form, "rules");
	}

	void pushPacket(NetworkPacket *pkt) { m_receive_packet_queue.push_back(pkt); }
	NetworkPacket* popPacket() { return m_send_packet_queue.pop_front(1); }
	epixel::Spell* getSpell(const u32 id);
	const u32 getSpellId(const std::string &name) const;

	void DeleteClient(u16 peer_id, ClientDeletionReason reason);
private:

	friend class EmergeThread;
	friend class RemoteClient;

	void SendMovement(u16 peer_id);
	void SendHP(u16 peer_id, u8 hp);
	void SendBreath(u16 peer_id, u16 breath);
	void SendAccessDenied(u16 peer_id, AccessDeniedCode reason,
		const std::string &custom_reason, bool reconnect = false);
	void SendAccessDenied_Legacy(u16 peer_id, const std::wstring &reason);
	void SendDeathscreen(u16 peer_id,bool set_camera_point_target, v3f camera_point_target);
	void SendItemDef(u16 peer_id,IItemDefManager *itemdef, u16 protocol_version);
	void SendNodeDef(u16 peer_id,INodeDefManager *nodedef, u16 protocol_version);

	/* mark blocks not sent for all clients */
	void AsyncSetBlocksNotSent(const std::map<v3s16, MapBlock *>& block);
	void ConsumeAsyncBlocksNotSent();
	void SetBlocksNotSent(const std::map<v3s16, MapBlock *>& block);

	void SendTimeOfDay(u16 peer_id, u16 time, f32 time_speed);
	void SendPlayerHP(PlayerSAO* playersao);

	void SendLocalPlayerAnimations(u16 peer_id, v2s32 animation_frames[4], f32 animation_speed);
	void SendEyeOffset(u16 peer_id, v3f first, v3f third);
	void SendPlayerPrivileges(RemotePlayer* player);
	void SendPlayerInventoryFormspec(RemotePlayer* player);
	void SendShowFormspecMessage(u16 peer_id, const std::string &formspec, const std::string &formname);
	void SendHUDAdd(u16 peer_id, u32 id, HudElement *form);
	void SendHUDRemove(u16 peer_id, u32 id);
	void SendHUDSetFlags(u16 peer_id, u32 flags, u32 mask);
	void SendHUDSetParam(u16 peer_id, u16 param, const std::string &value);
	void SendSetSky(u16 peer_id, const video::SColor &bgcolor,
			const std::string &type, const std::vector<std::string> &params);
	void SendOverrideDayNightRatio(u16 peer_id, bool do_override, float ratio);

	/*
		Send a node removal/addition event to all clients except ignore_id.
		Additionally, if far_players!=NULL, players further away than
		far_d_nodes are ignored and their peer_ids are added to far_players
	*/
	// Envlock and conlock should be locked when calling these
	void sendRemoveNode(v3s16 p, u16 ignore_id=0,
			std::vector<u16> *far_players=NULL, float far_d_nodes=100);
	void sendAddNode(v3s16 p, MapNode n, u16 ignore_id=0,
			std::vector<u16> *far_players=NULL, float far_d_nodes=100,
			bool remove_metadata=true);
	void setBlockNotSent(v3s16 p);

	// Environment and Connection must be locked when called
	void SendBlockNoLock(u16 peer_id, MapBlock *block, u8 ver, u16 net_proto_version);

	// Sends blocks to clients (locks env and con on its own)
	void SendBlocks(float dtime);

	void fillMediaCache();
	void sendMediaAnnouncement(u16 peer_id);
	void sendRequestedMedia(u16 peer_id,
			const std::vector<std::string> &tosend);

	void sendDetachedInventories(u16 peer_id);

	// Adds a ParticleSpawner on peer with peer_id (PEER_ID_INEXISTENT == all)
	void SendAddParticleSpawner(u16 peer_id, u16 amount, float spawntime,
		v3f minpos, v3f maxpos,
		v3f minvel, v3f maxvel,
		v3f minacc, v3f maxacc,
		float minexptime, float maxexptime,
		float minsize, float maxsize,
		bool collisiondetection, bool vertical, std::string texture, u32 id);

	void SendDeleteParticleSpawner(u16 peer_id, u32 id);

	// Spawns particle on peer with peer_id (PEER_ID_INEXISTENT == all)
	void SendSpawnParticle(u16 peer_id,
		v3f pos, v3f velocity, v3f acceleration,
		float expirationtime, float size,
		bool collisiondetection, bool vertical, std::string texture);

	u32 SendActiveObjectRemoveAdd(u16 peer_id, const std::string &datas);
	void SendActiveObjectMessages(u16 peer_id, const std::string &datas, bool reliable = true);
	/*
		Something random
	*/

	void DiePlayer(PlayerSAO* playersao);
	void RespawnPlayer(u16 peer_id);
	void UpdateCrafting(RemotePlayer *player);

	v3f findSpawnPos();

	// When called, connection mutex should be locked
	RemoteClient* getClient(u16 peer_id,ClientState state_min=CS_Active);
	RemoteClient* getClientNoEx(u16 peer_id,ClientState state_min=CS_Active);

	/*
		Get a player from memory or creates one.
		If player is already connected, return NULL
		Does not verify/modify auth info and password.

		Call with env and con locked.
	*/
	PlayerSAO *emergePlayer(const char *name, u16 peer_id, u16 proto_version);

	void handlePeerChanges();

	/*
		Variables
	*/

	// World directory
	std::string m_path_world;
	// Subgame specification
	SubgameSpec m_gamespec;

#ifndef SERVER
	// If true, do not allow multiple players and hide some multiplayer
	// functionality
	bool m_singleplayer_mode;
#endif

	// Thread can set; step() will throw as ServerError
	std::string m_async_fatal_error;

	// Some timers
	float m_liquid_transform_timer;
	float m_masterserver_timer;
	float m_emergethread_trigger_timer;
	float m_savemap_timer;

	float m_map_timer_and_unload_interval;

	// Environment
	ServerEnvironment *m_env;
	std::mutex m_fatalerror_mutex;

	// server connection
	con::Connection m_con;
	epixel::NetworkServer* m_epixel_con;

	// Emerge manager
	EmergeManager *m_emerge;

	// Scripting
	// Envlock and conlock should be locked when using Lua
	GameScripting *m_script;

	// Item definition manager
	IWritableItemDefManager *m_itemdef;

	// Node definition manager
	IWritableNodeDefManager *m_nodedef;

	// Craft definition manager
	IWritableCraftDefManager *m_craftdef;

	// Event manager
	EventManager *m_event;

	// Mods
	std::vector<ModSpec> m_mods;

	/*
		Threads
	*/

	// A buffer for time steps
	// step() increments and AsyncRunStep() run by m_thread reads it.
	std::atomic<float> m_step_dtime;

	// current server step lag counter
	float m_lag;

	// The server mainly operates in this thread
	ServerThread *m_thread;

	/*
		Time related stuff
	*/

	// Timer for sending time of day over network
	float m_time_of_day_send_timer;
	// Uptime of server in seconds
	std::atomic<double> m_uptime;

	/*
	 Client interface
	 */
	ClientInterface m_clients;

	std::mutex m_async_blocs_ns_mutex;
	std::map<v3s16, MapBlock*> m_async_blocks_not_sent;

	/*
		Peer change queue.
		Queues stuff from peerAdded() and deletingPeer() to
		handlePeerChanges()
	*/
	std::queue<con::PeerChange> m_peer_change_queue;

	/*
		Random stuff
	*/

	bool m_shutdown_requested;
	std::string m_shutdown_msg;
	bool m_shutdown_ask_reconnect;
	float m_shutdown_timer = 0.0f;

	/*
		Map edit event queue. Automatically receives all map edits.
		The constructor of this class registers us to receive them through
		onMapEditEvent

		NOTE: Should these be moved to actually be members of
		ServerEnvironment?
	*/

	/*
		Queue of map edits from the environment for sending to the clients
		This is behind m_env_mutex
	*/
	std::queue<MapEditEvent*> m_unsent_map_edit_queue;
	/*
		Set to true when the server itself is modifying the map and does
		all sending of information by itself.
		This is behind m_env_mutex
	*/
	bool m_ignore_map_edit_events;
	/*
		If a non-empty area, map edit events contained within are left
		unsent. Done at map generation time to speed up editing of the
		generated area, as it will be sent anyway.
		This is behind m_env_mutex
	*/
	VoxelArea m_ignore_map_edit_events_area;
	/*
		If set to !=0, the incoming MapEditEvents are modified to have
		this peed id as the disabled recipient
		This is behind m_env_mutex
	*/
	u16 m_ignore_map_edit_events_peer_id;

	// media files known to server
	std::unordered_map<std::string,MediaInfo> m_media;

	/*
		Sounds
	*/
	std::unordered_map<s32, ServerPlayingSound> m_playing_sounds;
	s32 m_next_sound_id;

	/*
		Detached inventories (behind m_env_mutex)
	*/
	// key = name
	std::map<std::string, Inventory*> m_detached_inventories;

	DISABLE_CLASS_COPY(Server);

	/*
	 * Contrib functions added to core under GPLv3
	 */
	bool contrib_on_prejoinplayer(const std::string &playerName, std::string &reason);
	void contrib_on_joinplayer(RemoteClient* client, PlayerSAO* playerSao);
	void contrib_on_leaveplayer(PlayerSAO* playerSao);
	void contrib_on_respawn(PlayerSAO* playerSao);
	void contrib_asyncrunstep(float dtime);
	void contrib_onstart();
	bool contrib_on_chat_message(RemotePlayer* player,
		const std::string &message);

	float m_automated_msg_timer;

	u16 contrib_last_msgid;

	epixel::AreaMgr* m_area_mgr;
	epixel::BanMgr* m_ban_mgr;
	epixel::CreatureStore* m_creaturestore;
	std::vector<std::wstring> m_tips;
	std::unordered_map<u32,epixel::Spell*> m_spells;
	epixel::ChatHandler* m_chat_handler;
	epixel::ConsoleThread* m_console_thread;
	std::string m_server_rules_form = "";

#ifndef SERVER
	epixel::DatabasePool<Database_SQLite3>* m_map_database;
#else
	epixel::DatabasePool<Database_PostgreSQL>* m_map_database;
	epixel::ISChatClient* m_ischatclient_thread;
#endif
	GameDatabase* m_game_database;
	AuthDatabase* m_auth_database;
	std::vector<std::string> defaultPrivs;

	u16 m_save_meta_interval;

	// For client singplayer only
	MutexedQueue<NetworkPacket*> m_receive_packet_queue;
	MutexedQueue<NetworkPacket*> m_send_packet_queue;

	// Awards
	std::unordered_map<u32,epixel::Achievement> m_map_achievement;
};

/*
	Runs a simple dedicated server loop.

	Shuts down when kill is set to true.
*/
void dedicated_server_loop(Server &server, bool &kill);

#endif

