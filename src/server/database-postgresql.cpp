/*
Copyright (C) 2016 Loic Blot <loic.blot@unix-experience.fr>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <fstream>
#include <codecvt>
#include <locale>
#include "config.h"
#include "database-postgresql.h"
#include "log.h"
#include "filesys.h"
#include "inventory.h"
#include "mg_ore.h"
#include "mg_schematic.h"
#include "nodedef.h"
#include "porting.h"
#include "content_sao.h"
#include "contrib/abms/treeabm.h"
#include "contrib/areas.h"
#include "contrib/ban.h"
#include "contrib/creaturestore.h"
#include "contrib/playersao.h"
#include "contrib/spell.h"
#include "contrib/teleport.h"
#include "craftdef.h"
#include "debug.h"
#include "environment.h"
#include "mapblock.h"
#include "scripting_game.h"
#include "tool.h"

/*
 * IMPORTANT NOTICE
 * Every conversion from non string to const char* values
 * should be done by using an intermediate std::string
 * buffer.
 * If you don't use this intermediate, the conversion
 * to std::string followed by conversion to const char*
 * could have bad issues, refering to bad memory areas.
 */

Database_PostgreSQL::Database_PostgreSQL(const std::string &connect_string, const std::string &name)
{
	m_name = name;
	m_connect_string = connect_string;
	dbconnect();
}

void Database_PostgreSQL::precheck()
{
	createDatabase();
	incrementDatabase();
}

void Database_PostgreSQL::init_worker()
{
	initStatements();
}

/**
 * @brief Database_PostgreSQL::beginSave
 *
 * Start a write transaction
 */
void Database_PostgreSQL::beginSave()
{
	verifyDatabase();
	resultsCheck(PQexec(m_conn, "BEGIN;"));
}

/**
 * @brief Database_PostgreSQL::endSave
 *
 * Commit current transaction to DB
 */
void Database_PostgreSQL::endSave()
{
	resultsCheck(PQexec(m_conn, "COMMIT;"));
}

/**
 * @brief Database_PostgreSQL::saveBlock
 * @param pos
 * @param data
 * @return always true
 *
 * Save a block to the database
 */
bool Database_PostgreSQL::saveBlock(const v3s16 &pos, const std::string &data,
		const epixel::MapBlockDB &blockdb_data)
{
	// This case is manually handled because we manipulate blob datas
	std::string x = itos(pos.X), y = itos(pos.Y), z = itos(pos.Z),
			ev = itos(blockdb_data.epixel_version), v = itos(blockdb_data.version), f = itos(blockdb_data.flags),
			cw = itos(blockdb_data.content_width), pw = itos(blockdb_data.params_width), t = itos(blockdb_data.timestamp);
	const char *values[] = {x.c_str(), y.c_str(), z.c_str(), data.c_str(),
			v.c_str(), ev.c_str(), f.c_str(), cw.c_str(), pw.c_str(), blockdb_data.raw_nodes.c_str(),
			blockdb_data.raw_nodes_metas.c_str(), t.c_str()};
	const int valLen[] = {sizeof(int), sizeof(int), sizeof(int), (int)data.size(),
			sizeof(int), sizeof(int), sizeof(int), sizeof(int), sizeof(int), (int)blockdb_data.raw_nodes.size(),
			(int)blockdb_data.raw_nodes_metas.size(), sizeof(int)};
	const int valFormats[] = { 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0 };

	resultsCheck(PQexecPrepared(m_conn, "write_block", 12,
				values, valLen, valFormats, 1), true);
	return true;
}

/**
 * @brief Database_PostgreSQL::loadBlock
 * @param pos
 * @return block datas
 */
void Database_PostgreSQL::loadBlock(const v3s16 &pos, std::string &data, epixel::MapBlockDB& blockdb_data)
{
	verifyDatabase();

	std::string x = itos(pos.X), y = itos(pos.Y), z = itos(pos.Z);
	const char *values[] = {x.c_str(), y.c_str(), z.c_str()};

	PGresult *results = execPrepared("read_block", 3, values, false, false);

	if (PQntuples(results)) {
		size_t rd_s, rn_s, rnm_s;
		unsigned char* rd = PQunescapeBytea((unsigned char*)PQgetvalue(results, 0, 0), &rd_s);
		unsigned char* rn = PQunescapeBytea((unsigned char*)PQgetvalue(results, 0, 6), &rn_s);
		unsigned char* rnm = PQunescapeBytea((unsigned char*)PQgetvalue(results, 0, 7), &rnm_s);

		data = std::string((char*)rd, rd_s);
		blockdb_data.version = pg_to_int(results, 0, 1);
		blockdb_data.epixel_version = pg_to_int(results, 0, 2);
		blockdb_data.flags = pg_to_int(results, 0, 3);
		blockdb_data.content_width = pg_to_int(results, 0, 4);
		blockdb_data.params_width = pg_to_int(results, 0, 5);
		blockdb_data.raw_nodes = std::string((char*)rn, rn_s);
		blockdb_data.raw_nodes_metas = std::string((char*)rnm, rnm_s);
		blockdb_data.timestamp = (u32)pg_to_int(results, 0, 8);
		PQfreemem(rd);
		PQfreemem(rn);
		PQfreemem(rnm);
	}

	PQclear(results);
}

void Database_PostgreSQL::listAllLoadableBlocks(std::vector<v3s16> &dst)
{
	verifyDatabase();

	PGresult *results = execPrepared("list_all_loadable_blocks", 0, NULL, false, false);
	int numrows = PQntuples(results);

	for (int row = 0; row < numrows; row++) {
		dst.push_back(pg_to_v3s16(results, row, 0));
	}

	PQclear(results);
}

/**
 * @brief Database_PostgreSQL::saveArea
 * @param area
 * @return true
 *
 * Save an area to database
 */
bool Database_PostgreSQL::saveArea(epixel::Area* area)
{
	verifyDatabase();
	beginSave();
	std::string area_id = itos(area->id), parent_id = itos(area->parent_id),
		pos1x = itos(area->pos1.X), pos1y = itos(area->pos1.Y), pos1z = itos(area->pos1.Z),
		pos2x = itos(area->pos2.X), pos2y = itos(area->pos2.Y), pos2z = itos(area->pos2.Z),
		isopen = itos(area->open ? 1 : 0), pvpflag = itos((u32)area->pvpflag);

	const char *values[]  = {area_id.c_str(), area->name.c_str(), parent_id.c_str(), pos1x.c_str(),
		pos1y.c_str(), pos1z.c_str(),
		pos2x.c_str(), pos2y.c_str(), pos2z.c_str(), isopen.c_str(), pvpflag.c_str()};
	const char* rmvalues[] = {area_id.c_str()};

	execPrepared("write_area", 11, values);
	execPrepared("remove_area_owners", 1, rmvalues);

	std::string pid_s;
	for (const auto &pid: area->owners) {
		pid_s = itos(pid);
		const char *owner_values[] = {area_id.c_str(), pid_s.c_str()};
		execPrepared("write_area_owner", 2, owner_values);
	}

	endSave();

	return true;
}

bool Database_PostgreSQL::loadAreas(std::unordered_map<u32, epixel::Area*> &areas)
{
	PGresult *results = resultsCheck(PQexec(m_conn,
		"SELECT id, name, parent_id, pos1x, pos1y, pos1z,"
		"pos2x, pos2y, pos2z, isopen, pvpflag FROM areas"),
		false);

	int numrows = PQntuples(results);

	if (!numrows) {
		PQclear(results);
		return false;
	}

	for (int row = 0; row < numrows; row++) {
		u32 area_id = pg_to_uint(results, row, 0);
		std::string name(PQgetvalue(results, row, 1));
		u32 parent_id = pg_to_uint(results, row, 2);
		v3f pos1, pos2;
		pos1.X = pg_to_int(results, row, 3);
		pos1.Y = pg_to_int(results, row, 4);
		pos1.Z = pg_to_int(results, row, 5);
		pos2.X = pg_to_int(results, row, 6);
		pos2.Y = pg_to_int(results, row, 7);
		pos2.Z = pg_to_int(results, row, 8);
		bool isopen = pg_to_bool(results, row, 9);
		epixel::AreaPvpFlag pvpflag = (epixel::AreaPvpFlag) pg_to_int(results, row, 10);

		std::vector<u32> owners;

		const char* value[] = {PQgetvalue(results, row, 0)};
		PGresult *results2 = execPrepared("load_area_owners", 1,
				value, false, false);

		int numrows2 = PQntuples(results2);
		for (int row2 = 0; row2 < numrows2; row2++) {
			owners.push_back(pg_to_uint(results2, row2, 0));
		}

		areas[area_id] = new epixel::Area(owners, name, pos1,
				pos2, parent_id, isopen, pvpflag);
		areas[area_id]->id = area_id;
		areas[area_id]->init();

		PQclear(results2);
	}

	PQclear(results);
	return true;
}

bool Database_PostgreSQL::deleteArea(u32 areaId)
{
	verifyDatabase();
	std::string area_id = itos(areaId);
	const char *rmvalues[] = {area_id.c_str()};
	execPrepared("remove_area", 1, rmvalues);
	return true;
}

bool Database_PostgreSQL::savePlayerBan(const std::string &playername, epixel::Ban* ban)
{
	verifyDatabase();
	std::string bantime = itos(ban->bantime);
	const char *values[]  = {playername.c_str(), bantime.c_str(),
		ban->source.c_str(), ban->reason.c_str()};

	execPrepared("write_ban", 4, values);
	return true;
}

bool Database_PostgreSQL::saveIPBan(const std::string &ip, epixel::Ban* ban)
{
	verifyDatabase();
	std::string bantime = itos(ban->bantime);
	const char *values[]  = {ip.c_str(), bantime.c_str(),
		ban->source.c_str(), ban->reason.c_str()};

	execPrepared("write_ban_ip", 4, values);
	return true;
}

bool Database_PostgreSQL::deletePlayerBan(const std::string &playername)
{
	verifyDatabase();
	const char *rmvalues[] = {playername.c_str()};
	execPrepared("remove_ban", 1, rmvalues);
	return true;
}

bool Database_PostgreSQL::deleteIPBan(const std::string &ip)
{
	verifyDatabase();
	const char *rmvalues[] = {ip.c_str()};
	execPrepared("remove_ban_ip", 1, rmvalues);
	return true;
}

bool Database_PostgreSQL::loadPlayerBans(std::unordered_map<std::string, epixel::Ban*> &bans)
{
	PGresult *results = resultsCheck(PQexec(m_conn,
		"SELECT playername, cast(extract(epoch from bantime) as integer), reason, source from bans"),
		false);

	int numrows = PQntuples(results);

	if (!numrows) {
		PQclear(results);
		return false;
	}

	for (int row = 0; row < numrows; row++) {
		std::string playername(PQgetvalue(results, row, 0));
		u32 bantime = pg_to_uint(results, row, 1);
		// Special case, PgSQL return 3599 for -1
		if (bantime == 3599)
			bantime = -1;
		std::string reason(PQgetvalue(results, row, 2));
		std::string source(PQgetvalue(results, row, 3));

		bans[playername] = new epixel::Ban(bantime, source, reason);
	}

	PQclear(results);
	return true;
}

bool Database_PostgreSQL::loadIPBans(std::unordered_map<std::string, epixel::Ban*> &bans)
{
	PGresult *results = resultsCheck(PQexec(m_conn,
		"SELECT ip, cast(extract(epoch from bantime) as integer), reason, source from bans_ip"),
		false);

	int numrows = PQntuples(results);

	if (!numrows) {
		PQclear(results);
		return false;
	}

	for (int row = 0; row < numrows; row++) {
		std::string ip(PQgetvalue(results, row, 0));
		u32 bantime = pg_to_uint(results, row, 1);
		// Special case, PgSQL return 3599 for -1
		if (bantime == 3599)
			bantime = -1;
		std::string reason(PQgetvalue(results, row, 2));
		std::string source(PQgetvalue(results, row, 3));

		bans[ip] = new epixel::Ban(bantime, source, reason);
	}

	PQclear(results);
	return true;
}

bool Database_PostgreSQL::loadCrafts(std::vector<CraftDefinition *> &crafts)
{
	// Load shaped crafts
	PGresult *results = resultsCheck(PQexec(m_conn,
		"SELECT craftoutput, craftwidth, recipe1, recipe2, recipe3, recipe4, recipe5,"
		"recipe6, recipe7, recipe8, recipe9 FROM craft_shaped"),
		false);

	int numrows = PQntuples(results);

	if (numrows) {
		for (int row = 0; row < numrows; row++) {
			CraftReplacements replacements;
			std::vector<std::string> recipe;

			std::string output(PQgetvalue(results, row, 0));
			u32 width = pg_to_uint(results, row, 1);
			recipe.push_back(PQgetvalue(results, row, 2));
			recipe.push_back(PQgetvalue(results, row, 3));
			recipe.push_back(PQgetvalue(results, row, 4));
			recipe.push_back(PQgetvalue(results, row, 5));
			recipe.push_back(PQgetvalue(results, row, 6));
			recipe.push_back(PQgetvalue(results, row, 7));
			recipe.push_back(PQgetvalue(results, row, 8));
			recipe.push_back(PQgetvalue(results, row, 9));
			recipe.push_back(PQgetvalue(results, row, 10));

			CraftDefinition *def = new CraftDefinitionShaped(
				output, width, recipe, replacements);

			crafts.push_back(def);
		}
	}

	PQclear(results);

	// Load cooking crafts
	results = resultsCheck(PQexec(m_conn,
		"SELECT craftoutput, recipe, cooktime FROM craft_cooking"),
		false);

	numrows = PQntuples(results);

	if (numrows) {
		for (int row = 0; row < numrows; row++) {
			CraftReplacements replacements;

			std::string output(PQgetvalue(results, row, 0));
			std::string recipe(PQgetvalue(results, row, 1));
			float cooktime = pg_to_float(results, row, 2);

			CraftDefinition *def = new CraftDefinitionCooking(
					output, recipe, cooktime, replacements);

			crafts.push_back(def);
		}
	}

	PQclear(results);

	// Load fuel crafts
	results = resultsCheck(PQexec(m_conn,
		"SELECT recipe, burntime FROM craft_fuel"),
		false);

	numrows = PQntuples(results);

	if (numrows) {
		for (int row = 0; row < numrows; row++) {
			CraftReplacements replacements;

			std::string recipe(PQgetvalue(results, row, 0));
			float burntime = pg_to_float(results, row, 1);

			CraftDefinition *def = new CraftDefinitionFuel(
				recipe, burntime, replacements);

			crafts.push_back(def);
		}
	}

	PQclear(results);

	// Load shapeless crafts
	results = resultsCheck(PQexec(m_conn,
		"SELECT craftoutput, recipe1, recipe2, recipe3, recipe4, recipe5,"
		"recipe6, recipe7, recipe8, recipe9 FROM craft_shapeless"),
		false);

	numrows = PQntuples(results);

	if (numrows) {
		for (int row = 0; row < numrows; row++) {
			CraftReplacements replacements;
			std::vector<std::string> recipe;

			std::string output(PQgetvalue(results, row, 0));
			for (u8 i = 1; i < 10; i++) {
				const std::string component = PQgetvalue(results, row, i);
				if (component.length() > 0)
					recipe.push_back(component);
			}

			CraftDefinition *def = new CraftDefinitionShapeless(
				output, recipe, replacements);

			crafts.push_back(def);
		}
	}

	PQclear(results);
	return true;
}

/**
 * @brief Database_PostgreSQL::loadTips
 * @param tips
 * @return true if results
 *
 * Load tips from database
 */
bool Database_PostgreSQL::loadTips(std::vector<std::wstring> &tips)
{
	// Load shaped crafts
	PGresult *results = resultsCheck(PQexec(m_conn,
		"SELECT tip_en FROM tips"),
		false);

	int numrows = PQntuples(results);

	if (!numrows) {
		PQclear(results);
		return false;
	}

	for (int row = 0; row < numrows; row++) {
		std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
		std::wstring ws = converter.from_bytes(PQgetvalue(results, row, 0));
		tips.push_back(ws);
	}

	PQclear(results);
	return true;
}

/**
 * @brief Database_PostgreSQL::loadItemDefinitions
 * @param idef
 * @param ndef
 * @param script
 * @return always true
 *
 * Load Item/node definitions from database
 */
bool Database_PostgreSQL::loadItemDefinitions(IWritableItemDefManager *idef, IWritableNodeDefManager* ndef, GameScripting *script)
{
	verifyDatabase();

	PGresult *results = execPrepared("load_itemdefs", 0, {}, false, false);

	int numrows = PQntuples(results);

	for (int row = 0; row < numrows; row++) {
		ItemDefinition itemdef;
		itemdef.coreside = true;
		ContentFeatures f; // For ITEM_NODE type only

		const char* itemname_value[] = {PQgetvalue(results, row, 0)};
		itemdef.name = PQgetvalue(results, row, 0);
		itemdef.description = PQgetvalue(results, row, 1);
		itemdef.type = (ItemType)pg_to_int(results, row, 2);
		itemdef.inventory_image = PQgetvalue(results, row, 3);
		itemdef.wield_image = PQgetvalue(results, row, 4);
		itemdef.wield_scale = v3f(pg_to_float(results, row, 5),
				pg_to_float(results, row, 6), pg_to_float(results, row, 7));
		itemdef.stack_max = pg_to_int(results, row, 8);
		itemdef.range = pg_to_float(results, row, 9);
		itemdef.usable = pg_to_bool(results, row, 10);
		itemdef.liquids_pointable = pg_to_bool(results, row, 11);
		itemdef.node_placement_prediction = PQgetvalue(results, row, 12);
		if (itemdef.node_placement_prediction == "" && itemdef.type == ITEM_NODE) {
			itemdef.node_placement_prediction = itemdef.name;
		}
		itemdef.eat_amount = pg_to_int(results, row, 13);
		itemdef.on_use_replace_with = PQgetvalue(results, row, 14);
		itemdef.extended_type = (epixel::ItemExtendedType)pg_to_int(results, row, 15);

		// Load item groups
		PGresult *results2 = execPrepared("load_itemdefs_groups", 1, itemname_value, false, false);
		int numrows2 = PQntuples(results2);
		// Clear groups from default values
		itemdef.groups.clear();
		f.groups.clear();
		for (int row2 = 0; row2 < numrows2; row2++) {
			std::string groupname = PQgetvalue(results2, row2, 0);
			itemdef.groups[groupname] = pg_to_int(results2, row2, 1);
			if (itemdef.type == ITEM_NODE) {
				f.groups[groupname] = pg_to_int(results2, row2, 1);
				if (groupname.compare("unbreakable") == 0 && f.groups[groupname] > 0) {
					f.unbreakable = true;
				}
				else if (groupname.compare("explode_in_chain") == 0 && f.groups[groupname] > 0) {
					f.explodeInChain = true;
				}
				else if (groupname.compare("explosion_radius") == 0 && f.groups[groupname] > 0) {
					f.explosionRadius = f.groups[groupname];
				}
			}
		}
		PQclear(results2);

		results2 = execPrepared("load_itemdefs_sounds", 1, itemname_value, false, false);
		numrows2 = PQntuples(results2);
		for (int row2 = 0; row2 < numrows2; row2++) {
			std::string sound_name = PQgetvalue(results2, row2, 0);
			if (sound_name.compare("place") == 0) {
				itemdef.sound_place = SimpleSoundSpec(PQgetvalue(results2, row2, 1), pg_to_float(results2, row2, 2));
			}
			else if (itemdef.type == ITEM_NODE) {
				if (sound_name.compare("dig") == 0) {
					f.sound_dig = SimpleSoundSpec(PQgetvalue(results2, row2, 1), pg_to_float(results2, row2, 2));
				}
				else if (sound_name.compare("dug") == 0) {
					f.sound_dug = SimpleSoundSpec(PQgetvalue(results2, row2, 1), pg_to_float(results2, row2, 2));
				}
				else if (sound_name.compare("footstep") == 0) {
					f.sound_footstep = SimpleSoundSpec(PQgetvalue(results2, row2, 1), pg_to_float(results2, row2, 2));
				}
			}
		}
		PQclear(results2);

		// Special handling for tools
		if (itemdef.type == ITEM_TOOL) {
			// Read tool capabilities
			results2 = execPrepared("load_itemdefs_tool_capabilities", 1, itemname_value, false, false);
			numrows2 = PQntuples(results2);

			float full_punch_interval = numrows2 ? pg_to_float(results2, 0, 0) : 1.4f;
			int max_drop_level = numrows2 ? pg_to_int(results2, 0, 1) : 1;
			itemdef.tool_capabilities = new ToolCapabilities(full_punch_interval, max_drop_level);

			PQclear(results2);

			// Tool capabilities damage groups
			results2 = execPrepared("load_itemdefs_tool_capabilities_damage_groups", 1, itemname_value, false, false);
			numrows2 = PQntuples(results2);
			for (int row2 = 0; row2 < numrows2; row2++) {
				itemdef.tool_capabilities->damageGroups[PQgetvalue(results2, row2, 0)] = pg_to_int(results2, row2, 1);
			}
			PQclear(results2);

			// Tool capabilities groupcap
			results2 = execPrepared("load_itemdefs_tool_capabilities_groupcap", 1, itemname_value, false, false);
			numrows2 = PQntuples(results2);
			for (int row2 = 0; row2 < numrows2; row2++) {
				// item_name, groupcap_name
				const char* tgcn[] = {PQgetvalue(results, row, 0), PQgetvalue(results2, row2, 0)};
				ToolGroupCap tgc;
				tgc.maxlevel = pg_to_int(results2, row2, 1);
				tgc.uses = pg_to_int(results2, row2, 2);

				// ToolGroupCap times
				PGresult *results3 = execPrepared("load_itemdefs_tool_capabilities_groupcap_times", 2, tgcn, false, false);
				int numrows3 = PQntuples(results3);
				for (int row3 = 0; row3 < numrows3; row3++) {
					tgc.times[pg_to_int(results3, row3, 0)] = pg_to_float(results3, row3, 1);
				}
				PQclear(results3);

				itemdef.tool_capabilities->groupcaps[PQgetvalue(results2, row2, 0)] = tgc;
			}
			PQclear(results2);
		}
		else if (itemdef.type == ITEM_NODE) {
			// Nodes
			results2 = execPrepared("load_itemdefs_nodes", 1, itemname_value, false, false);
			numrows2 = PQntuples(results2);
			if (numrows2 == 0) {
				PQclear(results2);
				continue;
			}

			f.name = itemdef.name;
			f.drawtype = (NodeDrawType)pg_to_int(results2, 0, 0);
			f.visual_scale = pg_to_float(results2, 0, 1);
			f.mesh = PQgetvalue(results2, 0, 2);
			f.alpha = pg_to_int(results2, 0, 3);
			f.param_type = (ContentParamType)pg_to_int(results2, 0, 4);
			f.param_type_2 = (ContentParamType2)pg_to_int(results2, 0, 5);
			f.is_ground_content = pg_to_bool(results2, 0, 6);
			f.sunlight_propagates = pg_to_bool(results2, 0, 7);
			f.walkable = pg_to_bool(results2, 0, 8);
			f.pointable = pg_to_bool(results2, 0, 9);
			f.diggable = pg_to_bool(results2, 0, 10);
			f.climbable = pg_to_bool(results2, 0, 11);
			f.buildable_to = pg_to_bool(results2, 0, 12);
			f.rightclickable = pg_to_bool(results2, 0, 13);
			f.liquid_type = (LiquidType)pg_to_int(results2, 0, 14);
			f.liquid_alternative_flowing = PQgetvalue(results2, 0, 15);
			f.liquid_alternative_source = PQgetvalue(results2, 0, 16);
			f.liquid_viscosity = pg_to_int(results2, 0, 17);
			f.liquid_range = pg_to_int(results2, 0, 18);
			f.leveled = pg_to_int(results2, 0, 19);
			f.drowning = pg_to_int(results2, 0, 20);
			f.light_source = pg_to_int(results2, 0, 21);
			f.damage_per_second = pg_to_int(results2, 0, 22);
			f.waving = pg_to_int(results2, 0, 23);
			f.legacy_facedir_simple = pg_to_bool(results2, 0, 24);
			f.legacy_wallmounted = pg_to_bool(results2, 0, 25);

			f.light_propagates = (f.param_type == CPT_LIGHT);

			// Read tiles
			bool default_tiling = !(f.drawtype == NDT_PLANTLIKE || f.drawtype == NDT_FIRELIKE);
			f.tiledef[0].name = PQgetvalue(results2, 0, 26);
			f.tiledef[0].tileable_horizontal = default_tiling;
			f.tiledef[0].tileable_vertical = default_tiling;

			TileDef lasttile = f.tiledef[0];

			for (u8 i = 1; i < 6; i++) {
				std::string tilename = PQgetvalue(results2, 0, 26 + i);
				if (tilename == "") {
					f.tiledef[i] = lasttile;
				}
				else {
					f.tiledef[i].name = tilename;
					f.tiledef[i].tileable_horizontal = default_tiling;
					f.tiledef[i].tileable_vertical = default_tiling;
				}
			}

			// farming
			f.soil.base = PQgetvalue(results2, 0, 32);
			f.soil.dry = PQgetvalue(results2, 0, 33);
			f.soil.wet = PQgetvalue(results2, 0, 34);
			f.drops.max_items = pg_to_int(results2, 0, 35);
			f.droppable = pg_to_bool(results2, 0, 36);
			f.node_box.type = (NodeBoxType)pg_to_int(results2, 0, 37);
			f.collision_box.type = (NodeBoxType)pg_to_int(results2, 0, 38);
			f.selection_box.type = (NodeBoxType)pg_to_int(results2, 0, 39);
			if (pg_to_int(results2, 0, 40) < epixel::CFRC_MAX) {
				f.rc_formspec_type = (epixel::ContentFeaturesRightClickFormspecType)pg_to_int(results2, 0, 40);
			}
			else {
				logger.error("Wrong rightclick_formspec for item %s, defaulting to NONE", f.name.c_str());
			}

			PQclear(results2);

			// Load nodebox
			if (f.node_box.type == NODEBOX_FIXED) {
				const char* nbvalues[] = {PQgetvalue(results, row, 0), "1"};
				results2 = execPrepared("load_itemdefs_nodes_nodeboxes", 2, nbvalues, false, false);
				numrows2 = PQntuples(results2);

				for (int row2 = 0; row2 < numrows2; row2++) {
					f.node_box.fixed.push_back(pg_to_aabb3f(results2, row2, 1));
				}

				PQclear(results2);
			}
			else if (f.node_box.type == NODEBOX_WALLMOUNTED) {
				const char* nbvalues[] = {PQgetvalue(results, row, 0), "11", "12", "13"};
				results2 = execPrepared("load_itemdefs_nodes_nodeboxes_multi", 4, nbvalues, false, false);
				numrows2 = PQntuples(results2);

				for (int row2 = 0; row2 < numrows2; row2++) {
					switch (pg_to_int(results2, row2, 0)) {
						case 11: f.node_box.wall_bottom = pg_to_aabb3f(results2, row2, 1); break;
						case 12: f.node_box.wall_side = pg_to_aabb3f(results2, row2, 1); break;
						case 13: f.node_box.wall_top = pg_to_aabb3f(results2, row2, 1); break;
					}
				}

				PQclear(results2);
			}

			if (f.collision_box.type == NODEBOX_FIXED) {
				const char* nbvalues[] = {PQgetvalue(results, row, 0), "2"};
				results2 = execPrepared("load_itemdefs_nodes_nodeboxes", 2, nbvalues, false, false);
				numrows2 = PQntuples(results2);

				for (int row2 = 0; row2 < numrows2; row2++) {
					f.collision_box.fixed.push_back(pg_to_aabb3f(results2, row2, 1));
				}

				PQclear(results2);
			}
			else if (f.collision_box.type == NODEBOX_WALLMOUNTED) {
				const char* nbvalues[] = {PQgetvalue(results, row, 0), "21", "22", "23"};
				results2 = execPrepared("load_itemdefs_nodes_nodeboxes_multi", 4, nbvalues, false, false);
				numrows2 = PQntuples(results2);

				for (int row2 = 0; row2 < numrows2; row2++) {
					switch (pg_to_int(results2, row2, 0)) {
						case 21: f.collision_box.wall_bottom = pg_to_aabb3f(results2, row2, 1); break;
						case 22: f.collision_box.wall_side = pg_to_aabb3f(results2, row2, 1); break;
						case 23: f.collision_box.wall_top = pg_to_aabb3f(results2, row2, 1); break;
					}
				}

				PQclear(results2);
			}

			if (f.selection_box.type == NODEBOX_FIXED) {
				const char* nbvalues[] = {PQgetvalue(results, row, 0), "3"};
				results2 = execPrepared("load_itemdefs_nodes_nodeboxes", 2, nbvalues, false, false);
				numrows2 = PQntuples(results2);

				for (int row2 = 0; row2 < numrows2; row2++) {
					f.selection_box.fixed.push_back(pg_to_aabb3f(results2, row2, 1));
				}

				PQclear(results2);
			}
			else if (f.selection_box.type == NODEBOX_WALLMOUNTED) {
				const char* nbvalues[] = {PQgetvalue(results, row, 0), "31", "32", "33"};
				results2 = execPrepared("load_itemdefs_nodes_nodeboxes_multi", 4, nbvalues, false, false);
				numrows2 = PQntuples(results2);

				for (int row2 = 0; row2 < numrows2; row2++) {
					switch (pg_to_int(results2, row2, 0)) {
						case 31: f.collision_box.wall_bottom = pg_to_aabb3f(results2, row2, 1); break;
						case 32: f.collision_box.wall_side = pg_to_aabb3f(results2, row2, 1); break;
						case 33: f.collision_box.wall_top = pg_to_aabb3f(results2, row2, 1); break;
					}
				}

				PQclear(results2);
			}

			if (f.drawtype == NDT_NODEBOX) {
				f.selection_box = f.node_box;
			}
			else if (f.drawtype == NDT_FENCELIKE) {
				f.selection_box.type = NODEBOX_FIXED;
				f.selection_box.fixed = {aabb3f(1/8,1/2,1/8, 1/8,1/2,1/8)};
			}

			results2 = execPrepared("load_itemdefs_nodes_drops", 1, itemname_value, false, false);
			numrows2 = PQntuples(results2);

			for (int row2 = 0; row2 < numrows2; row2++) {
				std::string drop_name = PQgetvalue(results2, row2, 0);
				u16 drop_rarity = pg_to_int(results2, row2, 1);
				u16 min_items = pg_to_int(results2, row2, 2);
				u16 max_items = pg_to_int(results2, row2, 3);

				// Fix wrong min_items
				if (min_items < 1) {
					min_items = 1;
					logger.warn("min_items for drop '%s' lower than 1, fixing to 1. Please fix your database",
							drop_name.c_str());
				}

				// Fix wrong max_items
				if (max_items < min_items) {
					max_items = min_items;
					logger.warn("max_items < min_items for drop '%s', fixing to min_items. Please fix your database",
							drop_name.c_str());
				}

				f.drops.items.push_back({drop_name, drop_rarity, min_items, max_items});
			}

			PQclear(results2);

			content_t id = ndef->set(f.name, f);
			if (id > MAX_REGISTERED_CONTENT) {
				throw DatabaseException("Number of registerable nodes ("
						+ itos(MAX_REGISTERED_CONTENT + 1)
						+ ") exceeded (" + f.name + ")");
			}
		}

		idef->registerItem(itemdef);
		script->addItemDefinition(&itemdef, f);
	}

	logger.notice("%d items loaded.", numrows);

	PQclear(results);
	return true;
}

bool Database_PostgreSQL::loadTreeABM(INodeDefManager *ndef, std::vector<epixel::abm::DBTreeSaplingABM *> &abms)
{
	PGresult *results = execPrepared("load_tree_abms", 0, {}, false, false);

	int numrows = PQntuples(results);

	if (!numrows) {
		PQclear(results);
		return false;
	}

	for (int row = 0; row < numrows; row++) {
		u32 id = pg_to_int(results, row, 0),
			angle = pg_to_int(results, row, 8),
			iterations = pg_to_int(results, row, 9),
			random_level = pg_to_int(results, row, 10),
			fruit_chance = pg_to_int(results, row, 14),
			abm_chance = pg_to_int(results, row, 16);
		std::string axiom = PQgetvalue(results, row, 1),
			rules_a = PQgetvalue(results, row, 2),
			rules_b = PQgetvalue(results, row, 3),
			rules_c = PQgetvalue(results, row, 4),
			rules_d = PQgetvalue(results, row, 5),
			trunk = PQgetvalue(results, row, 6),
			leaves = PQgetvalue(results, row, 7),
			trunk_type = PQgetvalue(results, row, 11),
			fruit = PQgetvalue(results, row, 13),
			idStr = itos(id);
		float abm_interval = pg_to_float(results, row, 15);
		bool thin_branches = pg_to_bool(results, row, 12);

		const char* values[] = {idStr.c_str()};
		PGresult *results2 = execPrepared("load_tree_abm_saplings", 1, values, false);
		int numrows2 = PQntuples(results2);

		// No sapling, ignore entry
		if (!numrows2) {
			PQclear(results2);
			continue;
		}

		std::vector<std::string> saplings;
		for (int row2 = 0; row2 < numrows2; row2++) {
			saplings.push_back(PQgetvalue(results2, row2, 0));
		}

		PQclear(results2);

		epixel::abm::DBTreeSaplingABM* abm = new epixel::abm::DBTreeSaplingABM(ndef, saplings, abm_interval, abm_chance);
		treegen::TreeDef* def = abm->def();
		def->angle = angle;
		def->iterations = iterations;
		def->iterations_random_level = random_level;
		if (fruit.length() > 0)
			def->fruit_chance = fruit_chance;
		def->initial_axiom = axiom;
		def->rules_a = rules_a;
		def->rules_b = rules_b;
		def->rules_c = rules_c;
		def->rules_d = rules_d;
		def->trunk_type = trunk_type;
		def->thin_branches = thin_branches;
		abm->setTrunkLeavesFruit(trunk, leaves, fruit);
		abms.push_back(abm);
	}
	PQclear(results);
	return true;
}

bool Database_PostgreSQL::loadNodeReplaceABM(INodeDefManager *ndef, std::vector<epixel::abm::NodeReplaceABM *> &abms)
{
	PGresult *results = execPrepared("load_nodereplace_abms", 0, {}, false, false);

	int numrows = PQntuples(results);

	if (!numrows) {
		PQclear(results);
		return false;
	}

	for (int row = 0; row < numrows; row++) {
		const std::string node_name = PQgetvalue(results, row, 0),
				node_replace_name = PQgetvalue(results, row, 1);
		const float interval = pg_to_float(results, row, 2);
		const s32 chance = pg_to_int(results, row, 3);

		epixel::abm::NodeReplaceABM* abm = new epixel::abm::NodeReplaceABM(ndef, interval, chance, node_name, node_replace_name);
		abms.push_back(abm);
	}

	PQclear(results);
	return true;
}

bool Database_PostgreSQL::loadOres(std::vector<Ore *> &ores, OreManager *oremgr)
{
	PGresult *results = execPrepared("load_ores", 0, {}, false, false);

	int numrows = PQntuples(results);

	if (!numrows) {
		PQclear(results);
		return false;
	}

	for (int row = 0; row < numrows; row++) {
		u32 id = pg_to_int(results, row, 0);
		std::string name = PQgetvalue(results, row, 1);
		u16 oretype = pg_to_int(results, row, 2);
		if (oretype > ORE_VEIN) {
			logger.warn("Invalid ore %s with ID %d with type %d", name.c_str(), id, oretype);
			continue;
		}

		Ore *ore = oremgr->create((OreType)oretype);
		if (!ore) {
			logger.warn("Ore creation failed, unknown type ?");
			continue;
		}

		ore->name = "";
		ore->flags = pg_to_uint(results, row, 3);
		ore->clust_scarcity = pg_to_uint(results, row, 4);
		ore->clust_num_ores = pg_to_int(results, row, 5);
		ore->clust_size = pg_to_int(results, row, 6);
		ore->nthresh = pg_to_float(results, row, 7);
		ore->y_min = pg_to_int(results, row, 9);
		ore->y_max = pg_to_int(results, row, 10);
		ore->noise = NULL;

		// If it's noisy, load
		if (pg_to_bool(results, row, 8)) {
			ore->np.offset = pg_to_float(results, row, 11);
			ore->np.scale = pg_to_float(results, row, 12);
			ore->np.spread = pg_to_v3f(results, row, 13);
			ore->np.persist = pg_to_float(results, row, 16);
			ore->np.lacunarity = pg_to_float(results, row, 17);
			ore->np.seed = pg_to_int(results, row, 18);
			ore->np.octaves = pg_to_int(results, row, 19);
			ore->np.flags = pg_to_uint(results, row, 20);
		}

		if (oretype == ORE_VEIN) {
			OreVein *orevein = (OreVein *)ore;
			orevein->random_factor = 1.0f;
		}

		ore->m_nodenames.push_back(name);

		std::string oreIdStr = itos(id);
		const char* values[] = {oreIdStr.c_str()};
		PGresult *results2 = execPrepared("load_ore_pop_into", 1, values, false, false);
		int numrows2 = PQntuples(results2);

		for (int row2 = 0; row2 < numrows2; row2++) {
			ore->m_nodenames.push_back(PQgetvalue(results2, row2, 0));
		}
		ore->m_nnlistsizes.push_back(ore->m_nodenames.size() - 1);

		PQclear(results2);
		ores.push_back(ore);
	}

	PQclear(results);
	return true;
}

bool Database_PostgreSQL::loadLoots(epixel::CreatureDropMap &creaturedrops, u32 &loot_nb)
{
	PGresult *results = resultsCheck(PQexec(m_conn,
					"SELECT id, item, chance, mindrops, maxdrops FROM creaturedefs_loot"), false);

	int numrows = PQntuples(results);

	if (!numrows) {
		PQclear(results);
		return false;
	}

	for (int row = 0; row < numrows; row++) {
		u32 id = pg_to_int(results, row, 0);
		if (creaturedrops.find(id) == creaturedrops.end()) {
			creaturedrops[id] = new epixel::CreatureDropList();
		}

		std::string item = PQgetvalue(results, row, 1);
		u8 chance = pg_to_int(results, row, 2);
		u8 min = pg_to_int(results, row, 3);
		u8 max = pg_to_int(results, row, 4);
		epixel::CreatureDrop drop(item, chance, min, max);
		creaturedrops[id]->push_back(drop);
		loot_nb++;
	}

	PQclear(results);
	return true;
}

bool Database_PostgreSQL::loadCreatureNodeReplace(epixel::CreatureNodeReplaceMap &nodereplace, u32 &replace_nb)
{
	PGresult *results = resultsCheck(PQexec(m_conn,
			"SELECT id, sourcenode, destnode FROM creaturedefs_nodereplace"), false);

	int numrows = PQntuples(results);

	if (!numrows) {
		PQclear(results);
		return false;
	}

	for (int row = 0; row < numrows; row++) {
		u32 id = pg_to_int(results, row, 0);
		if (nodereplace.find(id) == nodereplace.end()) {
			nodereplace[id] = new epixel::CreatureNodeReplace();
		}

		std::string node_src = PQgetvalue(results, row, 1);
		std::string node_dst = PQgetvalue(results, row, 2);

		if (node_src.compare("") != 0 && node_dst.compare("") != 0) {
			(*nodereplace[id])[node_src] = node_dst;
			replace_nb++;
		}
	}

	PQclear(results);
	return true;
}

bool Database_PostgreSQL::loadCreatureDefinitions(epixel::CreatureDefMap &creaturedefs,
		epixel::CreatureStore* cstore, u32 &meshes_nb, u32 &texture_nb,
		const epixel::SpellMap &spells)
{
	// Load creature definitions
	PGresult *results = resultsCheck(PQexec(m_conn,
		"SELECT creaturedefs.name, creaturetype, attacktype, canfloat, canjump, viewrange, replacerate, replaceoffset, "
		"collisionbox1, collisionbox2, collisionbox3, collisionbox4, collisionbox5, collisionbox6, "
		"can_take_fall_damage, fall_speed, jump_height, walk_chance, walk_velocity, damage_min, loot_table, nodereplace_table, "
		"min_hp, max_hp, armor, visualsize_x, visualsize_y, xp_min, xp_max, "
		"speed_normal, stand_start, stand_end, walk_start, walk_end, run_start, run_end, speed_run, punch_start, punch_end, "
		"sound_warcry, sound_random, sound_attack, sound_death, sound_damage, sound_jump, "
		"damage_light, damage_water, damage_lava, damage_fall "
		"FROM creaturedefs "
		"LEFT OUTER JOIN creaturedefs_animationinfos ON (creaturedefs.name = creaturedefs_animationinfos.name) "
		"LEFT OUTER JOIN creaturedefs_sounds ON (creaturedefs.name = creaturedefs_sounds.name) "
		"LEFT OUTER JOIN creaturedefs_damages ON (creaturedefs.name = creaturedefs_damages.name)"),
		false);

	int numrows = PQntuples(results);

	if (!numrows) {
		PQclear(results);
		return false;
	}

	for (int row = 0; row < numrows; row++) {
		std::string name = PQgetvalue(results, row, 0);
		if (creaturedefs.find(name) != creaturedefs.end()) {
			continue;
		}

		epixel::CreatureDef* def = new epixel::CreatureDef();
		def->type = (epixel::CreatureType)pg_to_int(results, row, 1);
		def->attacktype = (epixel::CreatureAttackType)pg_to_int(results, row, 2);
		def->can_float = pg_to_bool(results, row, 3);
		def->can_jump = pg_to_bool(results, row, 4);
		def->viewrange = pg_to_float(results, row, 5);
		def->replacerate = pg_to_int(results, row, 6);
		def->replaceoffset = pg_to_float(results, row, 7);
		def->collisionbox.push_back(pg_to_float(results, row, 8));
		def->collisionbox.push_back(pg_to_float(results, row, 9));
		def->collisionbox.push_back(pg_to_float(results, row, 10));
		def->collisionbox.push_back(pg_to_float(results, row, 11));
		def->collisionbox.push_back(pg_to_float(results, row, 12));
		def->collisionbox.push_back(pg_to_float(results, row, 13));

		def->can_take_fall_damage = pg_to_bool(results, row, 14);
		def->fall_speed = pg_to_float(results, row, 15);
		def->jump_height = pg_to_float(results, row, 16);
		def->walk_chance = pg_to_float(results, row, 17);
		def->walk_velocity = pg_to_float(results, row, 18);
		def->damage_min = pg_to_int(results, row, 19);
		u32 loot_table = pg_to_int(results, row, 20);
		u32 nr_table = pg_to_int(results, row, 21);
		def->hp_min = pg_to_int(results, row, 22);
		def->hp_max = pg_to_int(results, row, 23);
		def->armor = pg_to_int(results, row, 24);
		def->visualsize = v2f(pg_to_float(results, row, 25), pg_to_float(results, row, 26));
		def->xp_min = pg_to_int(results, row, 27);
		def->xp_max = pg_to_int(results, row, 28);

		def->drops = cstore->getDropList(loot_table);
		def->replacelist = cstore->getNodeReplaceList(nr_table);

		for (u8 i = 0; i < epixel::CREATUREANIMATIONINFOS_MAX; i++) {
			def->animation_infos[i] = pg_to_int(results, row, 29 + i);
		}

		for (u8 i = 0; i < epixel::CREATURESOUND_MAX; i++) {
			std::string sound = PQgetvalue(results, row, 29 + epixel::CREATUREANIMATIONINFOS_MAX + i);
			if (sound.compare("") != 0) {
				def->sounds[(epixel::CreatureSound)i] = sound;
			}
		}

		for (u8 i = 0; i < epixel::CREATUREDAMAGE_MAX; i++) {
			u16 dmg = pg_to_int(results, row, 29 +
					epixel::CREATUREANIMATIONINFOS_MAX + epixel::CREATURESOUND_MAX + i);
			if (dmg != 0) {
				def->damages[(epixel::CreatureDamageType)i] = dmg;
			}
		}

		// Load creature meshes
		const char *cdefvals[] = {name.c_str()};
		PGresult *results2 = execPrepared("load_creaturedef_meshes", 1,
			cdefvals, false);
		int numrows2 = PQntuples(results2);

		for (int row2 = 0; row2 < numrows2; row2++) {
			std::string mesh = PQgetvalue(results2, row2, 0);
			def->meshlist.push_back(mesh);
			meshes_nb++;
		}

		PQclear(results2);

		// Load creature textures
		results2 = execPrepared("load_creaturedef_textures", 1, cdefvals, false);
		numrows2 = PQntuples(results2);

		for (int row2 = 0; row2 < numrows2; row2++) {
			std::string texture = PQgetvalue(results2, row2, 0);
			def->texturelist.push_back(texture);
			texture_nb++;
		}
		PQclear(results2);

		results2 = execPrepared("load_creaturedef_spells", 1, cdefvals, false);
		numrows2 = PQntuples(results2);

		def->spells.clear();
		for (int row2 = 0; row2 < numrows2; row2++) {
			std::string spellname = PQgetvalue(results2, row2, 0);
			for (const auto &spell: spells) {
				if (spellname.compare(spell.second->name) == 0) {
					def->spells[spell.second->id] = spell.second;
					break;
				}
			}
		}
		PQclear(results2);

		creaturedefs[name] = def;
	}

	PQclear(results);
	return true;
}

bool Database_PostgreSQL::loadCreatureSpawningABM(INodeDefManager* ndef,
		std::vector<epixel::abm::CreatureSpawningABM *> &abms)
{
	verifyDatabase();
	// Load creature definitions
	PGresult *results = resultsCheck(PQexec(m_conn,
		"SELECT name,interval,chance,min_light,max_light,max_count_per_area,min_height,max_height "
		"FROM creaturedefs_spawning_abms WHERE name in (SELECT name FROM creaturedefs)"),
		false);

	int numrows = PQntuples(results);

	if (!numrows) {
		PQclear(results);
		return false;
	}

	for (int row = 0; row < numrows; row++) {
		const char* name = PQgetvalue(results, row, 0);
		const char *values[] = {name};
		PGresult *results2 = execPrepared("load_creaturedef_abm_nodes", 1, values, false);

		int numrows2 = PQntuples(results2);

		// No node, ignore this ABM
		if (!numrows2) {
			logger.crit("Creature spawning ABM for %s doesn't have linked nodes. Ignoring.",
					name);
			PQclear(results2);
			continue;
		}

		std::vector<std::string> nodes;
		for (int row2 = 0; row2 < numrows2; row2++) {
			nodes.push_back(PQgetvalue(results2, row2, 0));
		}

		float interval = pg_to_float(results, row, 1);
		u32 chance = pg_to_int(results, row, 2);
		u32 min_light = pg_to_int(results, row, 3);
		u32 max_light = pg_to_int(results, row, 4);
		u32 max_count_per_area = pg_to_int(results, row, 5);
		u32 min_height = pg_to_int(results, row, 6);
		u32 max_height = pg_to_int(results, row, 7);
		abms.push_back(new epixel::abm::CreatureSpawningABM(ndef, nodes, name, interval,
				chance, min_light, max_light, max_count_per_area, min_height, max_height));

		PQclear(results2);
	}

	PQclear(results);
	return true;
}

bool Database_PostgreSQL::createUser(const std::string &login, const std::string &pwdhash)
{
	verifyDatabase();

	const char *values[] = {login.c_str(), pwdhash.c_str()};

	execPrepared("write_user", 2, values);

	return true;
}

bool Database_PostgreSQL::loadUserAndVerifyPassword(const std::string &login, const std::string &pwdhash)
{
	verifyDatabase();

	const char *values[] = {login.c_str()};

	PGresult *results = execPrepared("read_user", 1, values, false, false);

	if (!PQntuples(results)) {
		PQclear(results);
		return false;
	}

	std::string password(PQgetvalue(results, 0, 1));

	PQclear(results);
	return pwdhash.compare(password) == 0;
}

bool Database_PostgreSQL::loadUser(const std::string &login, std::string &dstpassword)
{
	verifyDatabase();

	const char *values[] = {login.c_str()};

	PGresult *results = execPrepared("read_user", 1, values, false, false);

	if (!PQntuples(results)) {
		PQclear(results);
		return false;
	}

	dstpassword = std::string(PQgetvalue(results, 0, 1));
	PQclear(results);
	return true;
}

bool Database_PostgreSQL::loadUserPrivs(const u32 user_id, std::set<std::string> &privs)
{
	// Empty privileges
	privs.clear();

	std::string uid = itos(user_id);
	const char *values[] = {uid.c_str()};

	PGresult *results = execPrepared("read_user_privs", 1, values, false);

	int numrows = PQntuples(results);
	for (int row = 0; row < numrows; row++) {
		privs.insert(PQgetvalue(results, row, 0));
	}

	PQclear(results);
	return true;
}

bool Database_PostgreSQL::getUsernamesWithoutCase(const std::string &login, std::vector<std::string> &names)
{
	verifyDatabase();

	const char *values[] = {login.c_str()};
	PGresult *results = execPrepared("read_user_without_case", 1, values, false, false);

	int resultNb = PQntuples(results);
	if (!resultNb) {
		PQclear(results);
		return false;
	}

	for (u16 i = 0; i < resultNb; i++) {
		names.push_back(PQgetvalue(results, 0, 0));
	}

	PQclear(results);

	return true;
}

bool Database_PostgreSQL::removeUser(u32 id)
{
	verifyDatabase();

	if (userExists(id)) {
		std::string uid = itos(id);
		const char *rmvalues[] = {uid.c_str()};

		// Because we have proper constraints, removing player from this table will cascade on other tables
		execPrepared("remove_user", 1, rmvalues);
	}
	return false;
}

bool Database_PostgreSQL::setPassword(const std::string &login, const std::string &pwdhash)
{
	verifyDatabase();

	const char *values[] = {login.c_str(), pwdhash.c_str()};

	if (userExists(login)) {
		execPrepared("set_password", 2, values);
		return true;
	}

	return false;
}

u32 Database_PostgreSQL::userExists(const std::string &login)
{
	verifyDatabase();

	const char *values[] = {login.c_str()};

	PGresult *results = execPrepared("read_user", 1, values, false, false);

	u32 res = (PQntuples(results) > 0) ? pg_to_int(results, 0, 0) : 0;
	PQclear(results);

	return res;
}

bool Database_PostgreSQL::userExists(u32 id)
{
	verifyDatabase();

	std::string strid = itos(id);
	const char *values[] = {strid.c_str()};

	PGresult *results = execPrepared("read_user_id", 1, values, false);

	u32 res = PQntuples(results);
	PQclear(results);

	return (res > 0);
}

bool Database_PostgreSQL::getUserListByIds(const std::vector<u32> &user_ids, std::vector<std::string> &res)
{
	PGresult *results;
	for (const auto &uid: user_ids) {
		verifyDatabase();

		std::string uid_s = itos(uid);
		const char *values[] = {uid_s.c_str()};

		results = execPrepared("read_user_login", 1, values, false, false);
		if (PQntuples(results) > 0) {
			res.push_back(PQgetvalue(results, 0, 0));
		}
		PQclear(results);
	}
	return true;
}

u32 Database_PostgreSQL::getLastLogin(const u32 id)
{
	verifyDatabase();

	std::string strid = itos(id);
	const char *values[] = {strid.c_str()};

	PGresult *results = execPrepared("read_user_lastlogin", 1,
			values, false, false);

	if (!PQntuples(results)) {
		PQclear(results);
		return 0;
	}

	u32 res = pg_to_int(results, 0, 0);
	PQclear(results);
	return res;
}

bool Database_PostgreSQL::setLastLogin(const std::string &login)
{
	verifyDatabase();

	const char *values[] = {login.c_str()};
	execPrepared("update_user_lastlogin", 1, values);
	return true;
}

// Privileges
bool Database_PostgreSQL::addPrivs(u32 id, const std::vector<std::string> &privs)
{
	verifyDatabase();

	beginSave();
	for (auto &priv: privs) {
		std::string uid = itos(id);
		const char *addvalues[] = {uid.c_str(), priv.c_str()};
		execPrepared("write_user_priv", 2, addvalues);
	}
	endSave();
	return true;
}

bool Database_PostgreSQL::removePrivs(u32 id, const std::vector<std::string> &privs)
{
	beginSave();
	for (auto &priv: privs) {
		std::string uid = itos(id);
		const char *rmvalues[] = {uid.c_str(), priv.c_str()};
		execPrepared("remove_user_priv", 2, rmvalues);
	}
	endSave();
	return true;
}

bool Database_PostgreSQL::loadPlayerExtendedAttributes(PlayerSAO *sao)
{
	verifyDatabase();

	sao->clearExtendedAttributes();
	std::string uid = itos(sao->getPlayer()->getDBId());
	const char *values[] = {uid.c_str()};
	PGresult *results = execPrepared("load_player_extended_attributes", 1,
		values, false);

	int numrows = PQntuples(results);
	for (int row = 0; row < numrows; row++) {
		sao->setExtendedAttribute(PQgetvalue(results, row, 0),PQgetvalue(results, row, 1));
	}

	PQclear(results);
	return true;
}

bool Database_PostgreSQL::savePlayerExtendedAttributes(const u32 user_id, const std::unordered_map<std::string, std::string> &attributes)
{
	beginSave();

	const std::string uid = itos(user_id);
	// And reinsert all
	for (const auto &attr: attributes) {
		const char *values[] = {uid.c_str(), attr.first.c_str(), attr.second.c_str()};
		execPrepared("write_player_extended_attribute", 3, values);
	}
	endSave();
	return true;
}

bool Database_PostgreSQL::loadPlayerIgnores(PlayerSAO* sao)
{
	verifyDatabase();

	std::string uid = itos(sao->getPlayer()->getDBId());
	const char *values[] = {uid.c_str()};
	PGresult *results = execPrepared("load_player_ignores", 1, values, false);

	int numrows = PQntuples(results);
	for (int row = 0; row < numrows; row++) {
		sao->ignorePlayer(PQgetvalue(results, row, 0));
	}

	PQclear(results);
	return true;
}

bool Database_PostgreSQL::addPlayerIgnore(const u32 user_id, const std::string &ignorename)
{
	verifyDatabase();

	std::string uid = itos(user_id);
	const char *values[] = {uid.c_str(), ignorename.c_str()};
	execPrepared("add_player_ignore", 2, values);
	return true;
}

bool Database_PostgreSQL::removePlayerIgnore(const u32 user_id, const std::string &ignorename)
{
	verifyDatabase();

	std::string uid = itos(user_id);
	const char *values[] = {uid.c_str(), ignorename.c_str()};
	execPrepared("remove_player_ignore", 2, values);
	return true;
}

bool Database_PostgreSQL::addPlayerToChannel(const u32 user_id, const std::string &channel)
{
	verifyDatabase();
	std::string uid = itos(user_id);
	const char *values[] = {uid.c_str(), channel.c_str()};
	execPrepared("add_player_to_channel", 2, values);
	return true;
}

bool Database_PostgreSQL::removePlayerFromChannel(const u32 user_id, const std::string &channel)
{
	verifyDatabase();
	const std::string uid = itos(user_id);
	const char *values[] = {uid.c_str(), channel.c_str()};
	execPrepared("remove_player_from_channel", 2, values);
	return true;
}

bool Database_PostgreSQL::loadPlayerChannels(const u32 user_id, std::vector<std::string> &channels)
{
	verifyDatabase();

	// Empty privileges
	channels.clear();

	const std::string uid = itos(user_id);
	const char *values[] = {uid.c_str()};

	PGresult *results = execPrepared("load_player_channels", 1,
		values, false);

	int numrows = PQntuples(results);
	for (int row = 0; row < numrows; row++) {
		channels.push_back(PQgetvalue(results, row, 0));
	}

	PQclear(results);
	return true;
}

/**
 * @brief Database_PostgreSQL::savePlayer
 * @param player
 * @return false if failed, true if success
 *
 * Save player data to pgsql
 *
 */
bool Database_PostgreSQL::savePlayer(RemotePlayer *player)
{
	PlayerSAO* sao = player->getPlayerSAO();
	if (!sao)
		return false;

	v3f pos = player->getPosition();
	std::string uid = itos(player->getDBId()), p = ftos(sao->getPitch()), y = ftos(sao->getYaw()), px = ftos(pos.X),
			py = ftos(pos.Y), pz = ftos(pos.Z), hp = itos(sao->getHP()), b = itos(sao->getBreath()),
			m = itos(sao->getMoney()), h = itos(sao->getHunger()),
			mana = itos(sao->getMana()), xp = i64tos(sao->getXP()), rules_accepted = itos(sao->hasAcceptedRules() ? 1 : 0);
	const char *values[] = {uid.c_str(), p.c_str(), y.c_str(), px.c_str(), py.c_str(), pz.c_str(),
		hp.c_str(), b.c_str(), m.c_str(), h.c_str(), mana.c_str(), xp.c_str(), rules_accepted.c_str()};
	const char* rmvalues[] = {uid.c_str()};
	beginSave();
	execPrepared("save_player", 13, values, true, false);

	if (player->checkModified()) {
		// Write player inventories
		execPrepared("remove_player_inventories", 1, rmvalues);
		execPrepared("remove_player_inventory_items", 1, rmvalues);

		u16 i=0;
		for (const auto &list: sao->getInventory()->getLists()) {
			std::string name = list->getName(), width = itos(list->getWidth()),
					inv_id = itos(i), lsize = itos(list->getSize());

			const char* inv_values[] = {uid.c_str(), inv_id.c_str(),
					width.c_str(), name.c_str(), lsize.c_str()};
			execPrepared("add_player_inventory", 5, inv_values);

			for(u32 j = 0; j < list->getSize(); j++) {
				std::ostringstream os;
				list->getItem(j).serialize(os);
				std::string itemStr = os.str(), slotId = itos(j);

				const char* invitem_values[] = {uid.c_str(), inv_id.c_str(), slotId.c_str(), itemStr.c_str()};
				execPrepared("add_player_inventory_item", 4, invitem_values);
			}
			i++;
		}
	}

	for (const auto &achievement: sao->getAchievementProgress()) {
		epixel::AchievementProgress ap = achievement.second;
		std::string ap_id = itos(ap.id_achievement), ap_prog = itos(ap.progress), ap_lock = itos(ap.unlocked);
		const char *values[] = {uid.c_str(), ap_id.c_str(), ap_prog.c_str(), ap_lock.c_str()};
		execPrepared("save_user_awards", 4, values);
	}

	endSave();

	return true;
}

/**
 * @brief Database_PostgreSQL::loadPlayer
 * @param player
 * @param sao
 * @return false if failed, true if success
 *
 * Load player data from pgsql
 *
 */
bool Database_PostgreSQL::loadPlayer(RemotePlayer* player, PlayerSAO *sao)
{
	if (!sao)
		return false;
	verifyDatabase();

	std::string uid = itos(player->getDBId());
	const char *values[] = {uid.c_str()};
	PGresult *results = execPrepared("load_player", 1,
		values, false, false);

	if (PQntuples(results) > 0) {
		sao->setPitch(pg_to_float(results, 0, 0));
		sao->setYaw(pg_to_float(results, 0, 1));
		player->setPosition(v3f(pg_to_float(results, 0, 2), pg_to_float(results, 0, 3), pg_to_float(results, 0, 4)));
		sao->setHP(pg_to_int(results, 0, 5));
		sao->setBreath(pg_to_int(results, 0, 6));
		sao->setMoney(pg_to_int(results, 0, 7));
		sao->setHunger(pg_to_int(results, 0, 8));
		sao->setHome(v3f(pg_to_float(results, 0, 9), pg_to_float(results, 0, 10), pg_to_float(results, 0, 11)), false);
		sao->setMana(pg_to_int(results, 0, 12));
		sao->setXP((s64)pg_to_u64(results, 0, 13));
		if (pg_to_bool(results, 0, 14)) {
			sao->acceptRules();
		}
	}

	PQclear(results);

	// Load inventory
	results = execPrepared("load_player_inventories", 1,
			values, false, false);

	int resultCount = PQntuples(results);

	for (int row = 0; row < resultCount; ++row) {
		InventoryList* invList = sao->getInventory()->addList(PQgetvalue(results, row, 2), pg_to_uint(results, row, 3));
		invList->setWidth(pg_to_uint(results, row, 1));

		u32 invId = pg_to_uint(results, row, 0);
		std::string invIdStr = itos(invId);

		const char* values2[] = {uid.c_str(), invIdStr.c_str()};
		PGresult *results2 = execPrepared("load_player_inventory_items", 2,
			values2, false, false);

		int resultCount2 = PQntuples(results2);
		for (int row2 = 0; row2 < resultCount2; row2++) {
			const std::string itemStr = PQgetvalue(results2, row2, 1);
			if (itemStr.length() > 0) {
				ItemStack stack;
				stack.deSerialize(itemStr);
				invList->addItem(pg_to_uint(results2, row2, 0), stack);
			}
		}
		PQclear(results2);
	}

	PQclear(results);

	// Load awards
	results = execPrepared("load_user_awards", 1,
			values, false, false);

	resultCount = PQntuples(results);

	for (int row = 0; row < resultCount; ++row) {
		sao->setAchievementProgress(
			pg_to_int(results, row, 0),
			pg_to_int(results, row, 1),
			pg_to_bool(results, row, 2));
	}
	PQclear(results);

	return true;
}

/**
 * @brief Database_PostgreSQL::playerDataExists
 * @param player
 * @return false if failed, true if success
 *
 * Verify if player data exists in pgsql
 *
 */
bool Database_PostgreSQL::playerDataExists(const u32 user_id)
{
	verifyDatabase();

	std::string uid = itos(user_id);
	const char *values[] = { uid.c_str() };
	PGresult *results = execPrepared("load_player", 1,
		values, false);

	bool res = (PQntuples(results) > 0);
	PQclear(results);
	return res;
}

/**
 * @brief Database_PostgreSQL::setPlayerHome
 * @param player
 * @return false if failed, true if success
 *
 * Set player home position to pgsql
 *
 */
bool Database_PostgreSQL::setPlayerHome(RemotePlayer* player)
{
	if (!player->getPlayerSAO())
		return false;

	std::string hX = ftos(player->getPosition().X),
			hY = ftos(player->getPosition().Y),
			hZ = ftos(player->getPosition().Z),
			uid = itos(player->getDBId());

	const char *values[]  = {uid.c_str(), hX.c_str(),
		hY.c_str(), hZ.c_str()};
	execPrepared("update_player_home", 4, values);
	return true;
}

/**
 * @brief Database_PostgreSQL::saveEnvMeta
 * @param g_time
 * @param time_of_day
 * @return
 *
 * Save environement meta to pgsql
 */
bool Database_PostgreSQL::saveEnvMeta(u32 g_time, u32 time_of_day, s16 automapgen_offset,
		const u64 &lbm_version, const std::string &lbm_introtimes_str, const u32 day_count)
{
	std::string gtime = itos(g_time), tod = itos(time_of_day), amo = itos(automapgen_offset),
		lbm_v = i64tos(lbm_version), dc = itos(day_count);
	const char *values[]  = {gtime.c_str(), tod.c_str(), amo.c_str(), lbm_v.c_str(), lbm_introtimes_str.c_str(), dc.c_str()};

	beginSave();
	execPrepared("delete_env_meta", 0, NULL);
	execPrepared("save_env_meta", 6, values);
	endSave();

	return true;
}

/**
 * @brief Database_PostgreSQL::loadEnvMeta
 * @param s_env
 * @return
 *
 * Load environement meta to pgsql
 */
bool Database_PostgreSQL::loadEnvMeta(ServerEnvironment* s_env)
{
	verifyDatabase();

	PGresult *results = execPrepared("load_env_meta", 0,
		NULL, false, false);

	if (PQntuples(results) > 0) {
		s_env->setGameTime(pg_to_u64(results, 0, 0));
		s_env->setTimeOfDay(pg_to_u64(results, 0, 1));
		s_env->setCurrentAutoMapgenOffset(pg_to_int(results, 0, 2));
		s_env->getLBMMgr()->loadIntroductionTimes(PQgetvalue(results, 0, 4), s_env->getGameDef(), s_env->getGameTime());
		s_env->setDayCount(pg_to_int(results, 0, 5));
	}
	else {
		s_env->getLBMMgr()->loadIntroductionTimes("", s_env->getGameDef(), s_env->getGameTime());
	}

	PQclear(results);
	return true;
}

bool Database_PostgreSQL::loadSpells(epixel::SpellMap &spells)
{
	verifyDatabase();

	PGresult *results = execPrepared("load_spells", 0,
		NULL, false, false);

	int numrows = PQntuples(results);
	u32 spellId = 0;

	for (s32 row = 0; row < numrows; row++) {
		epixel::Spell* spell = new epixel::Spell();
		spell->name = PQgetvalue(results, row, 0);
		spell->visual = PQgetvalue(results, row, 1);
		spell->visual_size = pg_to_v2f(results, row, 2);
		spell->texture = PQgetvalue(results, row, 4);
		spell->velocity = pg_to_float(results, row, 5);
		spell->damages_player = pg_to_int(results, row, 6);
		spell->damages_npc = pg_to_int(results, row, 7);

		if (pg_to_int(results, row, 8) >= epixel::SPELLDAMAGEGROUP_MAX) {
			std::stringstream ss;
			ss << "Invalid spell damagegroup ID " << pg_to_int(results, row, 8)
					<< "for spell " << spell->name << ", aborting." << std::endl;
			delete spell;
			throw DatabaseException(ss.str());
		}
		spell->damagegroup = (epixel::SpellDamageGroup)pg_to_int(results, row, 8);
		if (spell->damagegroup >= epixel::SPELLDAMAGEGROUP_MAX) {
			logger.crit("Invalid spell damagegroup for spell %s: %d >= SPELLDAMAGEGROUP_MAX", spell->name.c_str(), spell->target_type);
			delete spell;
			continue;
		}
		const std::string dmgGroup = epixel::Spell::DamageGroupMapper[spell->damagegroup];

		spell->toolcap_npc.damageGroups[dmgGroup] = spell->damages_npc;
		spell->toolcap_player.damageGroups[dmgGroup] = spell->damages_player;

		spell->damages_nodes_radius = pg_to_int(results, row, 9);
		spell->max_range = pg_to_float(results, row, 10);
		spell->timer = pg_to_float(results, row, 11);
		spell->target_type = (epixel::SpellTargetType)pg_to_int(results, row, 12);
		if (spell->target_type > epixel::SPELL_TARGETTYPE_MAX) {
			logger.crit("Invalid spell_target type for spell %s: %d > SPELL_TARGETTYPE_MAX", spell->name.c_str(), spell->target_type);
			delete spell;
			continue;
		}
		spell->id = spellId;
		spells[spellId] = spell;
		spellId++;
	}

	PQclear(results);

	return true;
}

/**
 * @brief Database_PostgreSQL::loadAchievements
 * @param achievements
 * @return
 */
bool Database_PostgreSQL::loadAchievements(std::unordered_map<u32, epixel::Achievement> &achievements)
{
	verifyDatabase();

	PGresult *results = execPrepared("load_achievements", 0,
		NULL, false, false);

	int numrows = PQntuples(results);

	for (int row = 0; row < numrows; row++) {
		epixel::Achievement achievement;
		achievement.id = pg_to_int(results, row, 0);
		achievement.name = PQgetvalue(results, row, 1);
		achievement.image = PQgetvalue(results, row, 2);
		achievement.description = PQgetvalue(results, row, 3);
		achievement.at = (epixel::AchievementType)pg_to_int(results, row, 4);
		achievement.target_count = pg_to_int(results, row, 5);
		achievement.target_what = PQgetvalue(results, row, 6);
		achievements[achievement.id] = achievement;
	}

	PQclear(results);
	return true;
}

/**
 * @brief Database_PostgreSQL::addTeleportLocation
 * @param tl
 * @return
 */
bool Database_PostgreSQL::addTeleportLocation(const epixel::TeleportLocation &tl)
{
	verifyDatabase();
	std::string xs = ftos(tl.x), ys = ftos(tl.y), zs = ftos(tl.z);
	const char *values[]  = {xs.c_str(), ys.c_str(), zs.c_str(), tl.label.c_str()};
	execPrepared("save_teleporter", 4, values);
	return true;
}

/**
 * @brief Database_PostgreSQL::removeTeleportLocation
 * @param tl
 * @return
 */
bool Database_PostgreSQL::removeTeleportLocation(const epixel::TeleportLocation &tl)
{
	verifyDatabase();
	std::string xs = ftos(tl.x), ys = ftos(tl.y), zs = ftos(tl.z);
	const char *rmvalues[]  = {xs.c_str(), ys.c_str(), zs.c_str()};
	execPrepared("remove_teleporter", 3, rmvalues);
	return true;
}

/**
 * @brief Database_PostgreSQL::loadTeleportLocations
 * @param tmgr
 * @return
 */
bool Database_PostgreSQL::loadTeleportLocations(epixel::TeleportMgr *tmgr)
{
	verifyDatabase();

	PGresult *results = execPrepared("load_teleporters", 0,
		NULL, false, false);

	int numrows = PQntuples(results);

	for (int row = 0; row < numrows; row++) {
		tmgr->addTeleportLocation({
			PQgetvalue(results, row, 3),
			pg_to_float(results, row, 0),
			pg_to_float(results, row, 1),
			pg_to_float(results, row, 2)
		}, true);
	}

	PQclear(results);

	tmgr->regenFormspec();

	return true;
}

bool Database_PostgreSQL::loadBiomes(std::vector<Biome *> &biomes, BiomeManager *biomemgr)
{
	verifyDatabase();

	BiomeType biometype = BIOME_NORMAL;

	std::string value_str = itos(g_settings->get(U16SETTING_MG_VERSION));
	const char *value[] = {value_str.c_str()};
	PGresult *results = execPrepared("load_biomes", 1,
		value, false, false);

	int numrows = PQntuples(results);

	for (int row = 0; row < numrows; row++) {
		Biome *biome = biomemgr->create(biometype);
		if (!biome) {
			logger.warn("Biome creation failed, unknown type ?");
			continue;
		}

		biome->name	= PQgetvalue(results, row, 0);
		biome->depth_top = pg_to_int(results, row, 3);
		biome->depth_filler = pg_to_int(results, row, 5);;
		biome->depth_water_top = pg_to_int(results, row, 8);;
		biome->y_min = pg_to_int(results, row, 10);;
		biome->y_max = pg_to_int(results, row, 11);;
		biome->heat_point = pg_to_int(results, row, 12);;
		biome->humidity_point = pg_to_int(results, row, 13);;
		biome->flags = g_settings->get(U16SETTING_MG_VERSION);

		biome->m_nodenames.push_back(PQgetvalue(results, row, 2));
		biome->m_nodenames.push_back(PQgetvalue(results, row, 4));
		biome->m_nodenames.push_back(PQgetvalue(results, row, 6));
		biome->m_nodenames.push_back(PQgetvalue(results, row, 7));
		biome->m_nodenames.push_back(PQgetvalue(results, row, 8));
		biome->m_nodenames.push_back(PQgetvalue(results, row, 9));
		biome->m_nodenames.push_back(PQgetvalue(results, row, 1));

		biome->m_nnlistsizes.push_back(biome->m_nodenames.size() - 1);

		biomes.push_back(biome);
	}

	PQclear(results);
	return true;
}

bool Database_PostgreSQL::loadDecorations(std::vector<Decoration *> &decorations, DecorationManager *decomgr, SchematicManager *schmgr, BiomeManager* biomemgr)
{
	verifyDatabase();

	PGresult *results = execPrepared("load_decorations", 0,
		NULL, false, false);

	int numrows = PQntuples(results);

	for (int row = 0; row < numrows; row++) {
		enum DecorationType decotype = (DecorationType)pg_to_int(results, row, 2);
		Decoration *deco = decomgr->create(decotype);
		if (!deco) {
			logger.crit("register_decoration: decoration placement type %d"
				" not implemented", decotype);
			continue;
		}

		deco->name       = PQgetvalue(results, row, 1);
		deco->fill_ratio = pg_to_float(results, row, 5);
		deco->y_min      = pg_to_int(results, row, 14);
		deco->y_max      = pg_to_int(results, row, 15);
		deco->sidelen    = pg_to_int(results, row, 4);
		if (deco->sidelen <= 0) {
			logger.error("register_decoration: sidelen must be greater than 0");
			continue;
		}
		std::string place_on = PQgetvalue(results, row, 3);
		str_remove_spaces(place_on);
		std::vector<std::string> v_place_on = str_split(place_on, ',');
		for (const auto &placeOn: v_place_on) {
			deco->m_nodenames.push_back(placeOn);
		}
		deco->m_nnlistsizes.push_back(deco->m_nodenames.size());
		if (deco->m_nnlistsizes.size() == 0) {
			logger.error("register_decoration: no place_on nodes defined");
			continue;
		}

		deco->flags = pg_to_uint(results, row, 17);

		// Noise Param
		deco->np.offset = pg_to_float(results, row, 6);
		deco->np.scale = pg_to_float(results, row, 7);
		deco->np.spread = pg_to_v3f(results, row, 8);
		deco->np.seed = pg_to_int(results, row, 11);
		deco->np.octaves = pg_to_int(results, row, 12);
		deco->np.persist = pg_to_float(results, row, 13);
		deco->np.lacunarity = 0;

		if (deco->np.seed != 0)
			deco->flags |= DECO_USE_NOISE;

		std::string value_str = itos(pg_to_int(results, row, 0));
		const char *value[] = {value_str.c_str()};
		PGresult *results2 = execPrepared("load_decorations_into", 1,
			value, false, false);

		int numrows2 = PQntuples(results2);
		for (int row2 = 0; row2 < numrows2; row2++) {
			std::string biome_name = PQgetvalue(results2, row2, 1);
			ObjDef *biomeDef = biomemgr->getByName(biome_name);
			deco->biomes.insert(biomeDef->index);
		}
		PQclear(results2);

		bool success = false;
		switch (decotype) {
			case DECO_SIMPLE:
			{
				DecoSimple *decoSimple = (DecoSimple*)deco;
				decoSimple->deco_height = pg_to_int(results, row, 20);
				if (decoSimple->deco_height <= 0) {
					logger.error("register_decoration: simple decoration height"
						" must be greater than 0 for %s", decoSimple->name.c_str());
					break;
				}
				decoSimple->deco_height_max = pg_to_int(results, row, 21);
				decoSimple->nspawnby = pg_to_int(results, row, 23);

				std::string reg_deco = PQgetvalue(results, row, 19);
				if (reg_deco == "") {
					logger.error("register_decoration: no decoration nodes defined");
					break;
				}
				decoSimple->m_nodenames.push_back(reg_deco);
				decoSimple->m_nnlistsizes.push_back(1);

				std::string reg_spawn_by = PQgetvalue(results, row, 22);
				if (reg_spawn_by == "" && decoSimple->nspawnby != 0) {
					logger.error("register_decoration: no spawn_by nodes defined, but num_spawn_by specified");
					break;
				}
				decoSimple->m_nodenames.push_back(reg_spawn_by);
				decoSimple->m_nnlistsizes.push_back(1);

				success = true;
				break;
			}

			case DECO_SCHEMATIC:
			{
				DecoSchematic *decoSchem = (DecoSchematic*)deco;
				decoSchem->rotation = (Rotation)pg_to_int(results, row, 18);

				std::string pathSchem = porting::path_share + "/games/epixel_game/mods/default/schematics/";
				Schematic *schem = SchematicManager::create(SCHEMATIC_NORMAL);
				if (!schem->loadSchematicFromFile( pathSchem + PQgetvalue(results, row, 16),
						decomgr->getNodeDef())) {
					logger.error("Schematic not found in : %s for %s", pathSchem.c_str(), decoSchem->name.c_str());
					continue;
				}
				schmgr->add(schem);
				decoSchem->schematic = schem;

				success = true;
				break;
			}

			case DECO_LSYSTEM:
				break;
		}
		if (!success) {
			delete deco;
			continue;
		}

		decorations.push_back(deco);
	}

	PQclear(results);
	return true;
}

void Database_PostgreSQL::dbconnect()
{
	m_conn = PQconnectdb(m_connect_string.c_str());

	if (PQstatus(m_conn) != CONNECTION_OK) {
		throw DatabaseException(std::string(
			"PostgreSQL database error: ") +
			PQerrorMessage(m_conn));
	}

	pg_version = PQserverVersion(m_conn);

	if (pg_version < 90500) {
		throw DatabaseException("PostgreSQL database error: "
			"Server version 9.5 or greater required.");
	}

	logger.debug("PostgreSQL Database: Version %d Connection made.", pg_version);
}

void Database_PostgreSQL::verifyDatabase()
{
	if (PQstatus(m_conn) == CONNECTION_OK)
		return;

	PQreset(m_conn);
	ping();
}


bool Database_PostgreSQL::ping()
{
	if (PQping(m_connect_string.c_str()) != PQPING_OK) {
		throw DatabaseException(std::string(
			"PostgreSQL database error: ") +
			PQerrorMessage(m_conn));
	}
	return true;
}


bool Database_PostgreSQL::initialized() const
{
	return (PQstatus(m_conn) == CONNECTION_OK);
}

void Database_PostgreSQL::initStatements()
{
	if (m_name.compare("map") == 0) {
		prepareStatement("read_block",
				"SELECT block_data, b_version, eb_version, b_flags, b_content_width, b_params_width, b_nodes, b_nodes_metas, "
						"cast(extract(epoch from b_time) as integer) FROM blocks "
						"WHERE posX = $1::int AND posY = $2::int AND posZ = $3::int");

		prepareStatement("write_block",
				"INSERT INTO blocks (posX, posY, posZ, block_data, b_version, eb_version, b_flags, b_content_width, b_params_width, "
						"b_nodes, b_nodes_metas, b_time) VALUES "
						"($1::int, $2::int, $3::int, $4::bytea, $5::int, $6::int, $7::int, $8::int, $9::int, $10::bytea, $11::bytea, to_timestamp($12)::timestamp)"
						"ON CONFLICT ON CONSTRAINT blocks_pkey DO UPDATE SET block_data = $4::bytea, b_version = $5::int, eb_version = $6::int, b_flags = $7::int,"
						"b_content_width = $8::int, b_params_width = $9::int, b_nodes = $10::bytea, b_nodes_metas = $11::bytea, b_time = to_timestamp($12)::timestamp");

		prepareStatement("list_all_loadable_blocks",
				"SELECT posX, posY, posZ FROM blocks");

		prepareStatement("delete_env_meta",
						 "DELETE FROM env_meta");

		prepareStatement("save_env_meta",
				"INSERT INTO env_meta (game_time, time_of_day, automapgen_offset, lbm_introduction_times_version, lbm_introduction_times, day_count) "
						"VALUES ($1::int, $2::int, $3::int, $4::int, $5, $6::int)");

		prepareStatement("load_env_meta",
				"SELECT game_time, time_of_day, automapgen_offset, lbm_introduction_times_version, lbm_introduction_times, day_count FROM env_meta");
	}
	else if (m_name.compare("game") == 0) {
		prepareStatement("write_area",
			 "INSERT INTO areas (id, name, parent_id, pos1x, pos1y, pos1z, pos2x, pos2y, pos2z, isopen, pvpflag) "
					"VALUES($1::int, $2, $3::int, $4::int, $5::int, $6::int, $7::int,"
					"$8::int, $9::int, $10::boolean, $11::int) ON CONFLICT ON CONSTRAINT areas_pkey DO UPDATE SET name = $2, parent_id = $3::int,"
					"pos1x = $4::int, pos1y = $5::int, pos1z = $6::int, pos2x = $7::int, pos2y = $8::int, pos2z = $9::int, "
					"isopen = $10::boolean, pvpflag = $11::int");

		prepareStatement("remove_area",
				"DELETE FROM areas WHERE id = $1::int");

		prepareStatement("write_area_owner",
				"INSERT INTO area_owners (area_id, user_id) VALUES ($1::int, $2::int)");

		prepareStatement("remove_area_owners",
				"DELETE FROM area_owners WHERE area_id = $1::int");

		prepareStatement("load_area_owners",
				"SELECT user_id FROM area_owners WHERE area_id = $1::int");

		prepareStatement("write_ban",
				"INSERT INTO bans (playername, bantime, source, reason) VALUES ($1, to_timestamp($2)::timestamp, $3, $4) "
						"ON CONFLICT ON CONSTRAINT bans_pkey DO UPDATE SET bantime = to_timestamp($2)::timestamp, source = $3, reason = $4");

		prepareStatement("write_ban_ip",
				"INSERT INTO bans_ip (ip, bantime, source, reason) VALUES ($1, to_timestamp($2)::timestamp, $3, $4)"
						"ON CONFLICT ON CONSTRAINT bans_ip_pkey DO UPDATE SET bantime = to_timestamp($2)::timestamp, source = $3, reason = $4");

		prepareStatement("remove_ban",
				"DELETE FROM bans WHERE playername = $1");

		prepareStatement("remove_ban_ip",
				"DELETE FROM bans_ip WHERE ip = $1");

		prepareStatement("load_creaturedef_meshes",
				"SELECT mesh FROM creaturedefs_meshes WHERE name = $1");

		prepareStatement("load_creaturedef_textures",
				"SELECT texture FROM creaturedefs_textures WHERE name = $1");

		prepareStatement("load_creaturedef_spells",
				"SELECT spell_name FROM creaturedefs_spells WHERE creature_name = $1");

		prepareStatement("load_creaturedef_abm_nodes",
				"SELECT nodename FROM creaturedefs_spawning_nodes WHERE name = $1");

		prepareStatement("write_user_priv",
				"INSERT INTO user_privs (user_id, priv_name) VALUES ($1::int, $2) ON CONFLICT ON CONSTRAINT user_privs_pkey DO NOTHING");

		prepareStatement("read_user_privs",
				"SELECT priv_name FROM user_privs WHERE user_id = $1::int");

		prepareStatement("remove_user_priv",
				"DELETE FROM user_privs WHERE user_id = $1::int AND priv_name = $2");

		prepareStatement("write_player_extended_attribute",
				"INSERT INTO player_ext_attributes (user_id, attr, value) VALUES ($1::int, $2, $3)"
						"ON CONFLICT ON CONSTRAINT player_ext_attributes_pkey DO UPDATE SET value = $3");

		prepareStatement("load_player_extended_attributes",
				"SELECT attr, value FROM player_ext_attributes WHERE user_id = $1::int");

		prepareStatement("add_player_to_channel",
				"INSERT INTO user_channels (user_id, channel) VALUES ($1::int, $2)"
						"ON CONFLICT ON CONSTRAINT user_channels_pkey DO NOTHING");

		prepareStatement("add_player_ignore",
				"INSERT INTO player_ignored_players (user_id, ignored_player) VALUES ($1::int, $2) "
						"ON CONFLICT ON CONSTRAINT player_ignored_players_pkey DO NOTHING");

		prepareStatement("remove_player_ignore",
				"DELETE FROM player_ignored_players WHERE user_id = $1::int AND ignored_player = $2");

		prepareStatement("load_player_ignores",
				"SELECT ignored_player FROM player_ignored_players WHERE user_id = $1::int");

		prepareStatement("remove_player_from_channel",
				"DELETE FROM user_channels WHERE user_id = $1::int AND channel = $2");

		prepareStatement("load_player_channels",
				"SELECT channel FROM user_channels WHERE user_id = $1");

		prepareStatement("save_player",
				"INSERT INTO player(user_id, pitch, yaw, posX, posY, posZ, hp, breath, money, hunger, mana, xp, rules_accepted) VALUES "
					"($1::int, $2, $3, $4, $5, $6, $7::int, $8::int, $9, $10::int, $11::int, $12::int, $13::boolean)"
					"ON CONFLICT ON CONSTRAINT player_pkey DO UPDATE SET pitch = $2, yaw = $3, posX = $4, posY = $5, posZ = $6, hp = $7::int, "
					"breath = $8::int, money = $9, hunger = $10::int, mana = $11::int, xp = $12::int, rules_accepted = $13::boolean");

		prepareStatement("remove_player_inventories",
				"DELETE FROM player_inventories WHERE user_id = $1::int");

		prepareStatement("remove_player_inventory_items",
				"DELETE FROM player_inventory_items WHERE user_id = $1::int");

		prepareStatement("add_player_inventory",
				"INSERT INTO player_inventories (user_id, inv_id, inv_width, inv_name, inv_size) VALUES "
						"($1::int, $2::int, $3::int, $4, $5::int)");

		prepareStatement("add_player_inventory_item",
				"INSERT INTO player_inventory_items (user_id, inv_id, slot_id, item) VALUES ($1::int, $2::int, $3::int, $4)");

		prepareStatement("load_player_inventories",
				"SELECT inv_id, inv_width, inv_name, inv_size FROM player_inventories WHERE user_id = $1::int ORDER BY inv_id");

		prepareStatement("load_player_inventory_items",
				"SELECT slot_id, item FROM player_inventory_items WHERE user_id = $1::int AND inv_id = $2::int");

		prepareStatement("load_player",
				"SELECT pitch, yaw, posX, posY, posZ, hp, breath, money, hunger, homeX, homeY, homeZ, mana, xp, rules_accepted FROM player "
						"WHERE user_id = $1");

		prepareStatement("update_player_home",
				"UPDATE player SET homeX = $2, homeY = $3, homeZ = $4 WHERE user_id = $1::int");

		prepareStatement("load_tree_abms",
				"SELECT id,axiom,rules_a,rules_b,rules_c,rules_d,trunk,leaves,angle,iterations,random_level,trunk_type,thin_branches,fruit,fruit_chance,"
						"abm_interval,abm_chance FROM abm_tree_scheme");

		prepareStatement("load_tree_abm_saplings",
				"SELECT sapling FROM abm_tree_sapling WHERE id = $1::int");

		prepareStatement("load_nodereplace_abms",
				"SELECT node_name, node_replace_name, interval, chance FROM abm_nodereplace");

		prepareStatement("load_ores",
				"SELECT oredefs.ore_id, ore_name, ore_type, ore_flags, clust_scarcity, clust_num_ores, clust_size, noise_threshhold, noise_params, y_min, y_max, "
						"noise_offset, noise_scale, noise_spread_x, noise_spread_y, noise_spread_z, noise_persist, noise_lacunarity, noise_seed, noise_octaves, noise_flags "
						"FROM oredefs "
						"LEFT OUTER JOIN oredefs_noise ON (oredefs.ore_id = oredefs_noise.ore_id)");

		prepareStatement("load_ore_pop_into",
				"SELECT into_name FROM oredefs_pop_into WHERE ore_id = $1::int");

		prepareStatement("load_itemdefs",
				"SELECT itemdefs.name, description, type, inventory_image, wield_image, wield_scale_x, wield_scale_y, wield_scale_z, "
						"stack_max, item_range, usable, liquids_pointable, node_placement_prediction, eat_amount, replace_with, "
						"type_extended FROM itemdefs "
						"LEFT OUTER JOIN itemdefs_eat ON (itemdefs.name = itemdefs_eat.name)");

		prepareStatement("load_itemdefs_groups",
				"SELECT group_name, group_value FROM itemdefs_groups WHERE item_name = $1");

		prepareStatement("load_itemdefs_sounds",
				"SELECT sound_name, sound_file, sound_gain FROM itemdefs_sounds WHERE item_name = $1");

		prepareStatement("load_itemdefs_tool_capabilities",
				"SELECT full_punch_interval, max_drop_level FROM itemdefs_tool_capabilities WHERE item_name = $1");

		prepareStatement("load_itemdefs_tool_capabilities_damage_groups",
				"SELECT group_name, group_value FROM itemdefs_tool_capabilities_damage_groups WHERE item_name = $1");

		prepareStatement("load_itemdefs_tool_capabilities_groupcap",
				"SELECT groupcap_name, maxlevel, uses FROM itemdefs_tool_capabilities_groupcap WHERE item_name = $1");

		prepareStatement("load_itemdefs_tool_capabilities_groupcap_times",
				"SELECT time_id, time_value FROM itemdefs_tool_capabilities_groupcap_times WHERE item_name = $1 AND groupcap_name = $2");

		prepareStatement("load_itemdefs_nodes",
				"SELECT drawtype,visual_scale,mesh,alpha,param_type,param_type2,is_ground_content,sunlight_propagates,"
						"walkable,pointable,diggable,climbable,buildable_to,rightclickable,liquid_type,liquid_alternative_flowing,"
						"liquid_alternative_source,liquid_viscosity,liquid_range,leveled,drowning,light_source,damage_per_second,"
						"waving,legacy_facedir_simple,legacy_wallmounted,tile1,tile2,tile3,tile4,tile5,tile6,base,dry,wet,max_drops,"
						"droppable,nodebox_type,collisionbox_type,selectionbox_type,rightclick_formspec "
						"FROM itemdefs_nodes "
						"LEFT OUTER JOIN itemdefs_nodes_tiles ON (itemdefs_nodes.item_name = itemdefs_nodes_tiles.item_name) "
						"LEFT OUTER JOIN itemdefs_nodes_soil ON (itemdefs_nodes.item_name = itemdefs_nodes_soil.item_name) "
						"LEFT OUTER JOIN itemdefs_nodeboxes ON (itemdefs_nodes.item_name = itemdefs_nodeboxes.item_name) "
						"WHERE itemdefs_nodes.item_name = $1");

		prepareStatement("load_itemdefs_nodes_drops",
				"SELECT drop_name, drop_rarity, min_items, max_items FROM itemdefs_nodes_drops WHERE item_name = $1");

		prepareStatement("load_itemdefs_nodes_nodeboxes",
				"SELECT nodebox_id, minedge_x,minedge_y,minedge_z,maxedge_x,maxedge_y,maxedge_z FROM itemdefs_nodeboxes_components "
						"WHERE item_name = $1 AND nodebox_id = $2");

		prepareStatement("load_itemdefs_nodes_nodeboxes_multi",
				"SELECT nodebox_id, minedge_x,minedge_y,minedge_z,maxedge_x,maxedge_y,maxedge_z FROM itemdefs_nodeboxes_components "
						"WHERE item_name = $1 AND nodebox_id IN ($2::int,$3::int,$4::int)");

		prepareStatement("load_spells",
				"SELECT name,visual,visual_size_x,visual_size_y,texture,velocity,damages_player,damages_npc,damagegroup,"
						"damages_nodes_radius,max_range,timer,spell_target_type FROM spelldefs");

		prepareStatement("load_achievements",
				"SELECT id, name, image, description, achievement_type, achievement_target, achievement_target_what FROM achievement");

		prepareStatement("load_user_awards",
				"SELECT id_achievement, achievement_progress, unlocked FROM user_awards WHERE id_player = $1");

		prepareStatement("save_user_awards",
				"INSERT INTO user_awards (id_player, id_achievement, achievement_progress, unlocked) VALUES ($1::int, $2::int, $3::int, $4) "
						"ON CONFLICT ON CONSTRAINT user_awards_pkey DO UPDATE SET "
						"achievement_progress = $3::int, unlocked = $4");

		prepareStatement("save_teleporter",
				"INSERT INTO teleporter_location (loc_x, loc_y, loc_z, description) VALUES ($1, $2, $3, $4)"
						"ON CONFLICT ON CONSTRAINT teleporter_location_pkey DO UPDATE SET description = $4");

		prepareStatement("remove_teleporter",
				"DELETE FROM teleporter_location WHERE loc_x = $1 AND loc_y = $2 AND loc_z = $3");

		prepareStatement("load_teleporters",
				"SELECT loc_x, loc_y, loc_z, description FROM teleporter_location");

		prepareStatement("load_biomes",
				"SELECT name, node_dust, node_top, depth_top, node_filler, depth_filler, node_stone, node_water_top, depth_water_top, node_water, y_min, y_max, heat_point, humidity_point FROM biomes  WHERE name IN (SELECT biome_name FROM biomes_mapgen WHERE mapgen = $1)");

		prepareStatement("load_decorations",
				"SELECT id, name, type, place_on, sidelen, fill_ratio, np_offset, np_scale, np_spread_x, np_spread_y, np_spread_z, np_seed, np_octaves, np_persist, y_min, y_max, schematic, flags, rotation, decoration, height, height_max, spawn_by, num_spawn_by FROM decorations");

		prepareStatement("load_decorations_into",
				"SELECT decoration_id, biome_name FROM decoration_biomes WHERE decoration_id = $1");
	}
	else if (m_name.compare("auth") == 0) {
		prepareStatement("write_user",
				"INSERT INTO users (login, password) VALUES ($1, $2)");

		prepareStatement("set_password",
				"UPDATE users SET password = $1 WHERE login = $2");

		prepareStatement("read_user",
				"SELECT id::int, password, lastlogin, account_lock FROM users WHERE login = $1");

		prepareStatement("read_user_without_case",
				"SELECT login FROM users WHERE lower(login) = lower($1)");

		prepareStatement("read_user_id",
				"SELECT login, password, lastlogin, account_lock FROM users WHERE id = $1::int");

		prepareStatement("read_user_login",
				 "SELECT login FROM users WHERE id = $1::int");

		prepareStatement("read_user_lastlogin",
				"SELECT lastlogin::int FROM users WHERE id = $1::int");

		prepareStatement("update_user_lastlogin",
				"UPDATE users SET lastlogin = extract(epoch from NOW()::timestamp with time zone)::integer WHERE login = $1");

		prepareStatement("remove_user",
				"DELETE FROM users WHERE id = $1::int");
	}

	logger.debug("PostgreSQL: SQL statements prepared.");
}


void Database_PostgreSQL::prepareStatement(const std::string &name, const std::string &sql)
{
	resultsCheck(PQprepare(m_conn, name.c_str(), sql.c_str(), 0, NULL));
}


PGresult* Database_PostgreSQL::resultsCheck(PGresult* result, bool clear /*= true*/)
{
	ExecStatusType statusType = PQresultStatus(result);

	if ((statusType != PGRES_COMMAND_OK && statusType != PGRES_TUPLES_OK) ||
		statusType == PGRES_FATAL_ERROR) {
		throw DatabaseException(std::string(
			"PostgreSQL database error: ") +
			PQresultErrorMessage(result));
	}

	if (clear)
		PQclear(result);

	return result;
}

void Database_PostgreSQL::createDatabase()
{
	PGresult *result = resultsCheck(PQexec(m_conn,
		"SELECT relname FROM pg_class WHERE relname='dbversion';"),
		false);

	if (PQntuples(result)) {
		PQclear(result);
		return;
	}

	const std::string dbcreate_sql = "CREATE TABLE IF NOT EXISTS dbversion ("
			 "	dbname VARCHAR(32) NOT NULL,"
			 "	version BIGINT NOT NULL,"
			 "	PRIMARY KEY(dbname)"
			 ");"
			 "INSERT INTO dbversion (dbname,version) VALUES ('" + m_name + "',1);";

	resultsCheck(PQexec(m_conn, dbcreate_sql.c_str()));

	logger.debug("PostgreSQL: %s Database was inited.", m_name.c_str());
}

bool Database_PostgreSQL::incrementDatabase()
{
	u32 current_db_version = 0;
	if (m_name.compare("map") == 0) {
		current_db_version = CURRENT_MAPDB_VERSION;
	}
	else if (m_name.compare("game") == 0) {
		current_db_version = CURRENT_GAMEDB_VERSION;
	}
	else if (m_name.compare("auth") == 0) {
		current_db_version = CURRENT_AUTHDB_VERSION;
	}
	else {
		logger.fatal("Unknown database with name %s", m_name.c_str());
		FATAL_ERROR("Unknown database found, aborting.");
	}

	std::string dbversion_sql = "SELECT version FROM dbversion WHERE dbname = '" + m_name + "'";
	PGresult *results = resultsCheck(PQexec(m_conn,	dbversion_sql.c_str()), false);

	int numrows = PQntuples(results);

	// If no version found, we are on the second connector and database entry doesn't exist, create it
	// It's useful for mutualized databases
	if (!numrows || numrows != 1) {
		const std::string version_sql = "INSERT INTO dbversion (dbname,version) VALUES ('" + m_name + "',1);";
		resultsCheck(PQexec(m_conn, version_sql.c_str()));
	}

	u32 version = pg_to_int(results, 0, 0);
	PQclear(results);

	logger.info("PostgreSQL %s database version %d found.", m_name.c_str(), version);
	if (version == current_db_version) {
		logger.notice("%s database is already up-to-date.", m_name.c_str());
		return false;
	}

	// We need to update, read files by offset over DB_VERSION
	u16 loopbreak = 0;
	while (version < current_db_version && loopbreak < 1000) {
		u32 i = version + 1;
		std::ifstream ifs;
		ifs.open(porting::path_share + "/sql/" + itos(i) + "_" + m_name + "_pgsql.sql");
		if (!ifs.good()) {
			std::stringstream ss;
			ss << "PostgreSQL upgrade error: file "
					<< porting::path_share << "/sql/" << i << std::string("_") << m_name << "_pgsql.sql not found";
			throw DatabaseException(ss.str());
		}
		std::stringstream sqlStream;
		sqlStream << ifs.rdbuf();
		ifs.close();

		logger.notice("Apply SQL %d_%s_pgsql.sql", i, m_name.c_str());
		resultsCheck(PQexec(m_conn,
			sqlStream.str().c_str()),
			false);

		PGresult *results = resultsCheck(PQexec(m_conn, dbversion_sql.c_str()), false);
		if (PQntuples(results)) {
			version = pg_to_int(results, 0, 0);
		}
		PQclear(results);
	}

	if (loopbreak == 1000) {
		throw DatabaseException("PGSQL UPGRADE FAILED. Version was not updated to current version in some SQL file");
	}

	logger.notice("Database %s updated to version %d", m_name.c_str(), current_db_version);
	return true;
}


Database_PostgreSQL::~Database_PostgreSQL()
{
	PQfinish(m_conn);
}
