/*
Minetest
Copyright (C) 2014 jjb, James Dornan <james@catch22.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "database.h"
#include "mg_biome.h"
#include "mg_decoration.h"
#include "settings.h"
#include <string>

#include <libpq-fe.h>

class ServerMap;

class Database_PostgreSQL : public GameDatabase, public MapDatabase, public AuthDatabase
{
public:
	Database_PostgreSQL(const std::string &connect_string, const std::string &name);

	void precheck();
	void init_worker();

	void beginSave();
	void endSave();

	/*
	 * Map
	 */

	bool saveBlock(const v3s16 &pos, const std::string &data, const epixel::MapBlockDB &blockdb_data);
	void loadBlock(const v3s16 &pos, std::string& data, epixel::MapBlockDB &blockdb_data);

	void listAllLoadableBlocks(std::vector<v3s16> &dst);

	/*
	 * Areas
	 */
	bool saveArea(epixel::Area* area);
	bool deleteArea(u32 areaId);
	bool loadAreas(std::unordered_map<u32, epixel::Area*>& areas);

	bool savePlayerBan(const std::string &playername, epixel::Ban* ban);
	bool saveIPBan(const std::string &ip, epixel::Ban* ban);
	bool deletePlayerBan(const std::string &playername);
	bool deleteIPBan(const std::string &ip);
	bool loadPlayerBans(std::unordered_map<std::string, epixel::Ban*> &bans);
	bool loadIPBans(std::unordered_map<std::string, epixel::Ban*> &bans);

	bool loadCrafts(std::vector<CraftDefinition *> &crafts);

	bool loadTips(std::vector<std::wstring> &tips);
	bool loadTreeABM(INodeDefManager* ndef, std::vector<epixel::abm::DBTreeSaplingABM*> &abms);
	bool loadNodeReplaceABM(INodeDefManager* ndef, std::vector<epixel::abm::NodeReplaceABM*> &abms);
	bool loadOres(std::vector<Ore*> &ores, OreManager* oremgr);

	bool loadBiomes(std::vector<Biome *> &biomes, BiomeManager *biomemgr);
	bool loadDecorations(std::vector<Decoration*> &decorations, DecorationManager* decomgr, SchematicManager *schmgr, BiomeManager *biomemgr);

	/*
	 * Items
	 */
	bool loadItemDefinitions(IWritableItemDefManager* idef, IWritableNodeDefManager* ndef, GameScripting* script);

	/*
	 * Creatures
	 */
	bool loadLoots(epixel::CreatureDropMap &creaturedrops, u32 &loot_nb);
	bool loadCreatureNodeReplace(epixel::CreatureNodeReplaceMap &nodereplace, u32 &replace_nb);
	bool loadCreatureDefinitions(epixel::CreatureDefMap &creaturedefs, epixel::CreatureStore* cstore,
			u32 &meshes_nb, u32 &texture_nb, const epixel::SpellMap &spells);
	bool loadCreatureSpawningABM(INodeDefManager *ndef, std::vector<epixel::abm::CreatureSpawningABM*> &abms);
	bool loadSpells(epixel::SpellMap &spells);

	/*
	 * Auth
	 */
	bool createUser(const std::string &login, const std::string &pwdhash);
	bool loadUserAndVerifyPassword(const std::string &login, const std::string &pwdhash);
	bool loadUser(const std::string &login, std::string &dstpassword);
	bool loadUserPrivs(const u32 user_id, std::set<std::string> &privs);
	bool getUsernamesWithoutCase(const std::string &login, std::vector<std::string> &names);
	bool removeUser(u32 id);
	u32 userExists(const std::string &login);
	bool userExists(u32 id);
	bool getUserListByIds(const std::vector<u32> &user_ids, std::vector<std::string> &res);
	u32 getLastLogin(const u32 id);
	bool setLastLogin(const std::string &login);
	bool setPassword(const std::string &login, const std::string &pwdhash);

	// Ingame player
	bool loadPlayerExtendedAttributes(PlayerSAO* sao);
	bool savePlayerExtendedAttributes(const u32 user_id, const std::unordered_map<std::string, std::string> &attributes);
	bool loadPlayerIgnores(PlayerSAO* sao);
	bool addPlayerIgnore(const u32 user_id, const std::string &ignorename);
	bool removePlayerIgnore(const u32 user_id, const std::string &ignorename);

	// Channels
	bool addPlayerToChannel(const u32 user_id, const std::string &channel);
	bool removePlayerFromChannel(const u32 user_id, const std::string &channel);
	bool loadPlayerChannels(const u32 user_id, std::vector<std::string> &channels);

	// Privileges
	bool addPrivs(u32 id, const std::vector<std::string> &privs);
	bool removePrivs(u32 id, const std::vector<std::string> &privs);

	// Player
	bool savePlayer(RemotePlayer *player);
	bool loadPlayer(RemotePlayer* player, PlayerSAO* sao);
	bool playerDataExists(const u32 user_id);
	bool setPlayerHome(RemotePlayer* player);

	// Teleporters
	bool addTeleportLocation(const epixel::TeleportLocation& tl);
	bool removeTeleportLocation(const epixel::TeleportLocation& tl);
	bool loadTeleportLocations(epixel::TeleportMgr* tmgr);

	// Env Meta
	bool saveEnvMeta(u32 g_time, u32 time_of_day, s16 automapgen_offset, const u64 &lbm_version,
			const std::string &lbm_introtimes_str, const u32 day_count);
	bool loadEnvMeta(ServerEnvironment* s_env);

	// Awards
	bool loadAchievements(std::unordered_map<u32, epixel::Achievement> &achievements);

	bool initialized() const;

	~Database_PostgreSQL();

private:
	std::string m_name = "";
	PGconn* m_conn = nullptr;
	int pg_version;
	std::string m_connect_string = "host=localhost port=5432 user=epixel password=nothing dbname=epixelrandom";
	void dbconnect();
	void initStatements();
	bool ping();
	void verifyDatabase();
	void createDatabase();
	bool incrementDatabase();
	void prepareStatement(const std::string &name, const std::string &sql);
	PGresult* resultsCheck(PGresult* res, bool clear = true);

	inline const int pg_to_int(PGresult *res, int row, int col) {
		return atoi(PQgetvalue(res, row, col));
	}

	inline const u32 pg_to_uint(PGresult *res, int row, int col) {
		return (const u32) atoi(PQgetvalue(res, row, col));
	}

	inline const u64 pg_to_u64(PGresult *res, int row, int col) {
		return (const u64) std::atoll(PQgetvalue(res, row, col));
	}

	inline const float pg_to_float(PGresult *res, int row, int col) {
		return std::atof(PQgetvalue(res, row, col));
	}

	inline const v2f pg_to_v2f(PGresult *res, int row, int col) {
		return v2f(
			pg_to_float(res, row, col),
			pg_to_float(res, row, col + 1)
		);
	}

	inline const v3f pg_to_v3f(PGresult *res, int row, int col) {
		return v3f(
			pg_to_float(res, row, col),
			pg_to_float(res, row, col + 1),
			pg_to_float(res, row, col + 2)
		);
	}

	inline const v3s16 pg_to_v3s16(PGresult *res, int row, int col) {
		return v3s16(
			pg_to_int(res, row, col),
			pg_to_int(res, row, col + 1),
			pg_to_int(res, row, col + 2)
		);
	}

	inline const aabb3f pg_to_aabb3f(PGresult *res, int row, int col) {
		return aabb3f(
			pg_to_float(res, row, col),
			pg_to_float(res, row, col + 1),
			pg_to_float(res, row, col + 2),
			pg_to_float(res, row, col + 3),
			pg_to_float(res, row, col + 4),
			pg_to_float(res, row, col + 5)
		);
	}

	inline const bool pg_to_bool(PGresult *res, int row, int col) {
		return (strcmp(PQgetvalue(res, row, col), "t") == 0);
	}

	inline PGresult* execPrepared(const char* stmtName, const int nbParams,
			const char** params, bool clear = true, bool nobinary = true)
	{
		return resultsCheck(PQexecPrepared(m_conn, stmtName, nbParams,
			params, NULL, NULL, nobinary ? 1 : 0), clear);
	}
};
