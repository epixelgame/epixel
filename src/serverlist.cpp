/*
Minetest
Copyright (C) 2013 celeron55, Perttu Ahola <celeron55@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <fstream>
#include <iostream>
#include <sstream>
#include <algorithm>

#include "version.h"
#include "settings.h"
#include "serverlist.h"
#include "filesys.h"
#include "porting.h"
#include "log.h"
#include "network/networkprotocol.h"
#include "convert_json.h"
#include "httpfetch.h"

namespace ServerList
{

std::string getFilePath()
{
	std::string serverlist_file = g_settings->get("serverlist_file");

	std::string dir_path = "client" DIR_DELIM "serverlist" DIR_DELIM;
	fs::CreateDir(porting::path_user + DIR_DELIM  "client");
	fs::CreateDir(porting::path_user + DIR_DELIM + dir_path);
	return porting::path_user + DIR_DELIM + dir_path + serverlist_file;
}


std::vector<ServerListSpec> getLocal()
{
	std::string path = ServerList::getFilePath();
	std::string liststring;
	if (fs::PathExists(path)) {
		std::ifstream istream(path.c_str());
		if (istream.is_open()) {
			std::ostringstream ostream;
			ostream << istream.rdbuf();
			liststring = ostream.str();
			istream.close();
		}
	}

	return deSerialize(liststring);
}


std::vector<ServerListSpec> getOnline()
{
	std::ostringstream geturl;
	geturl << g_settings->get("serverlist_url") <<
		"/list?proto_version_min=" << CLIENT_PROTOCOL_VERSION_MIN <<
		"&proto_version_max=" << CLIENT_PROTOCOL_VERSION_MAX;
	Json::Value root = fetchJsonValue(geturl.str(), NULL);

	std::vector<ServerListSpec> server_list;

	if (!root.isObject()) {
		return server_list;
	}

	root = root["list"];
	if (!root.isArray()) {
		return server_list;
	}

	for (unsigned int i = 0; i < root.size(); i++) {
		if (root[i].isObject()) {
			server_list.push_back(root[i]);
		}
	}

	return server_list;
}


// Delete a server from the local favorites list
bool deleteEntry(const ServerListSpec &server)
{
	std::vector<ServerListSpec> serverlist = ServerList::getLocal();
	for (std::vector<ServerListSpec>::iterator it = serverlist.begin();
			it != serverlist.end();) {
		if ((*it)["address"] == server["address"] &&
				(*it)["port"] == server["port"]) {
			it = serverlist.erase(it);
		} else {
			++it;
		}
	}

	std::string path = ServerList::getFilePath();
	std::ostringstream ss(std::ios_base::binary);
	ss << ServerList::serialize(serverlist);
	return fs::safeWriteToFile(path, ss.str());
}

// Insert a server to the local favorites list
bool insert(const ServerListSpec &server)
{
	// Remove duplicates
	ServerList::deleteEntry(server);

	std::vector<ServerListSpec> serverlist = ServerList::getLocal();

	// Insert new server at the top of the list
	serverlist.insert(serverlist.begin(), server);

	std::string path = ServerList::getFilePath();
	std::ostringstream ss(std::ios_base::binary);
	ss << ServerList::serialize(serverlist);
	if (!fs::safeWriteToFile(path, ss.str()))
		return false;

	return true;
}

std::vector<ServerListSpec> deSerialize(const std::string &liststring)
{
	std::vector<ServerListSpec> serverlist;
	std::istringstream stream(liststring);
	std::string line, tmp;
	while (std::getline(stream, line)) {
		std::transform(line.begin(), line.end(), line.begin(), ::toupper);
		if (line == "[SERVER]") {
			ServerListSpec server;
			std::getline(stream, tmp);
			server["name"] = tmp;
			std::getline(stream, tmp);
			server["address"] = tmp;
			std::getline(stream, tmp);
			server["port"] = tmp;
			std::getline(stream, tmp);
			server["description"] = tmp;
			serverlist.push_back(server);
		}
	}
	return serverlist;
}

const std::string serialize(const std::vector<ServerListSpec> &serverlist)
{
	std::string liststring;
	for (auto &server: serverlist) {
		liststring += "[server]\n";
		liststring += (server)["name"].asString() + '\n';
		liststring += (server)["address"].asString() + '\n';
		liststring += (server)["port"].asString() + '\n';
		liststring += (server)["description"].asString() + '\n';
		liststring += '\n';
	}
	return liststring;
}

void sendAnnounce(const std::vector<std::string> &clients_names, const float lag, const std::string &gameid,
		const std::string &mg_name, const std::vector<ModSpec> &mods)
{
	Json::Value server;
	server["port"] = g_settings->get(U16SETTING_EPIXEL_PORT);
	server["address"] = g_settings->get("server_address");

	bool strict_checking = g_settings->get(BOOLSETTING_STRICT_PROTOCOL_VERSION);
	server["name"]         = g_settings->get("server_name");
	server["description"]  = g_settings->get("server_description");
	server["version"]      = g_version_string;
	server["proto_min"]    = strict_checking ? LATEST_PROTOCOL_VERSION : SERVER_PROTOCOL_VERSION_MIN;
	server["proto_max"]    = strict_checking ? LATEST_PROTOCOL_VERSION : SERVER_PROTOCOL_VERSION_MAX;
	server["url"]          = g_settings->get("server_url");
	server["creative"]     = g_settings->get(BOOLSETTING_CREATIVE_MODE);
	server["damage"]       = g_settings->get(BOOLSETTING_ENABLE_DAMAGE);
	server["password"]     = true;
	server["pvp"]          = g_settings->get(BOOLSETTING_ENABLE_PVP);
	server["clients_max"]  = g_settings->get(U16SETTING_MAX_USERS);
	server["clients_list"] = Json::Value(Json::arrayValue);
	for (const auto &client_name: clients_names) {
		server["clients_list"].append(client_name);
	}

	server["gameid"] = gameid;
	server["lag"] = lag;
	server["mapgen"]            = mg_name;

	server["mods"]              = Json::Value(Json::arrayValue);
	for (auto &mod: mods) {
		server["mods"].append(mod.name);
	}

	Json::FastWriter writer;
	HTTPFetchRequest fetch_request;
	fetch_request.url = "http://servers.epixel-game.net/api/v1/announce";
	fetch_request.post_fields["json"] = writer.write(server);
	httpfetch_async(fetch_request);
}

void sendLegacyAnnounce(const std::string &action,
		const u16 port,
		const std::vector<std::string> &clients_names,
		const double uptime,
		const u32 game_time,
		const float lag,
		const std::string &gameid,
		const std::string &mg_name,
		const std::vector<ModSpec> &mods)
{
	Json::Value server;
	server["action"] = action;
	server["port"] = port;
	if (g_settings->exists("server_address")) {
		server["address"] = g_settings->get("server_address");
	}

	if (action != "delete") {
		bool strict_checking = g_settings->get(BOOLSETTING_STRICT_PROTOCOL_VERSION);
		server["name"]         = g_settings->get("server_name");
		server["description"]  = g_settings->get("server_description");
		server["version"]      = g_version_string;
		server["proto_min"]    = strict_checking ? LATEST_PROTOCOL_VERSION : SERVER_PROTOCOL_VERSION_MIN;
		server["proto_max"]    = strict_checking ? LATEST_PROTOCOL_VERSION : SERVER_PROTOCOL_VERSION_MAX;
		server["url"]          = g_settings->get("server_url");
		server["creative"]     = g_settings->get(BOOLSETTING_CREATIVE_MODE);
		server["damage"]       = g_settings->get(BOOLSETTING_ENABLE_DAMAGE);
		server["password"]     = true;
		server["pvp"]          = g_settings->get(BOOLSETTING_ENABLE_PVP);
		server["uptime"]       = (int) uptime;
		server["game_time"]    = game_time;
		server["clients"]      = (int) clients_names.size();
		server["clients_max"]  = g_settings->get(U16SETTING_MAX_USERS);
		server["clients_list"] = Json::Value(Json::arrayValue);
		for (auto &client_name: clients_names) {
			server["clients_list"].append(client_name);
		}
		if (gameid != "") server["gameid"] = gameid;
	}

	if (action == "start") {
		server["dedicated"]         = g_settings->get(BOOLSETTING_SERVER_DEDICATED);
		server["rollback"]          = false;
		server["mapgen"]            = mg_name;
		server["privs"]             = g_settings->get("default_privs");
		server["can_see_far_names"] = g_settings->get(S16SETTING_PLAYER_TRANSFER_DISTANCE) <= 0;
		server["mods"]              = Json::Value(Json::arrayValue);
		for (auto &mod: mods) {
			server["mods"].append(mod.name);
		}
		server["mods"].append("epixel_areas");
		server["mods"].append("epixel_ban");
		server["mods"].append("epixel_channels");
		server["mods"].append("epixel_creatures");
		server["mods"].append("epixel_local_pvp");
		server["mods"].append("epixel_item_drop");
		server["mods"].append("epixel_mana");
		server["mods"].append("epixel_home");
		server["mods"].append("epixel_xp");
		logger.notice("Announcing to %s", g_settings->get("serverlist_url").c_str());
	} else {
		if (lag)
			server["lag"] = lag;
	}

	Json::FastWriter writer;
	HTTPFetchRequest fetch_request;
	fetch_request.url = g_settings->get("serverlist_url") + std::string("/announce");
	fetch_request.post_fields["json"] = writer.write(server);
	httpfetch_async(fetch_request);
}

} //namespace ServerList
