/*
Minetest
Copyright (C) 2010-2013 celeron55, Perttu Ahola <celeron55@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "serverobject.h"
#include <fstream>
#include "util/serialize.h"
#include "environment.h"
#include "genericobject.h"
#include "server.h"

ServerActiveObject::ServerActiveObject(ServerEnvironment *env, const v3f& pos):
	ActiveObject(0),
	m_known_by_count(0),
	m_removed(false),
	m_pending_deactivation(false),
	m_static_exists(false),
	m_static_block(1337,1337,1337),
	m_env(env),
	m_base_position(pos)
{
}

ServerActiveObject::~ServerActiveObject()
{
}

ServerActiveObject* ServerActiveObject::create(ActiveObjectType type,
		ServerEnvironment *env, u16 id, v3f pos,
		const std::string &data)
{
	// Find factory function
	std::unordered_map<u16, Factory>::iterator n = m_types.find(type);
	if(n == m_types.end()) {
		// If factory is not found, just return.
		logger.warn("ServerActiveObject: No factory for type=%d", type);
		return NULL;
	}

	Factory f = n->second;
	ServerActiveObject *object = (*f)(env, pos, data);
	return object;
}

void ServerActiveObject::registerType(u16 type, Factory f)
{
	std::unordered_map<u16, Factory>::iterator n = m_types.find(type);
	if(n != m_types.end())
		return;
	m_types[type] = f;
}

ItemStack ServerActiveObject::getWieldedItem() const
{
	const Inventory *inv = getInventory();
	if(inv)
	{
		const InventoryList *list = inv->getList(getWieldList());
		if(list && (getWieldIndex() < (s32)list->getSize()))
			return list->getItem(getWieldIndex());
	}
	return ItemStack();
}

bool ServerActiveObject::setWieldedItem(const ItemStack &item)
{
	if(Inventory *inv = getInventory()) {
		if (InventoryList *list = inv->getList(getWieldList())) {
			list->changeItem(getWieldIndex(), item);
			if (getType() == ACTIVEOBJECT_TYPE_PLAYER) {
				m_env->getGameDef()->SendInventory(((PlayerSAO*)this));
			}
			return true;
		}
	}
	return false;
}

std::string ServerActiveObject::getPropertyPacket()
{
	std::ostringstream os(std::ios::binary);
	writeU8(os, GENERIC_CMD_SET_PROPERTIES);
	m_prop.serialize(os);
	return os.str();
}
