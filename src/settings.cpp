/*
Minetest
Copyright (C) 2010-2013 celeron55, Perttu Ahola <celeron55@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "settings.h"
#include <iostream>
#include <fstream>
#include <regex>
#include "constants.h"
#include "log.h"
#include "util/serialize.h"
#include "filesys.h"
#include "noise.h"
#ifdef __ANDROID__
#include "porting.h"
#endif
std::string g_settings_path;

Settings::Settings()
{
#ifndef SERVER
	m_current_screensize = core::dimension2d<u32>(800,600);
#endif
	initSettings();
}

Settings::~Settings()
{
	clear();
}


Settings & Settings::operator += (const Settings &other)
{
	update(other);
	return *this;
}


Settings & Settings::operator = (const Settings &other)
{
	if (&other == this)
		return *this;

	std::lock_guard<std::mutex> lock(m_mutex);
	std::lock_guard<std::mutex> lock2(other.m_mutex);

	clearNoLock();
	updateNoLock(other);

	return *this;
}


bool Settings::checkNameValid(const std::string &name)
{
	bool valid = name.find_first_of("=\"{}#") == std::string::npos;
	if (valid) valid = trim(name) == name;
	if (!valid) {
		logger.error("Invalid setting name \"%s\"", name.c_str());
		return false;
	}
	return true;
}


bool Settings::checkValueValid(const std::string &value)
{
	if (value.substr(0, 3) == "\"\"\"" ||
		value.find("\n\"\"\"") != std::string::npos) {
		logger.error("Invalid character sequence '\"\"\"' found in setting value!");
		return false;
	}
	return true;
}


std::string Settings::getMultiline(std::istream &is, size_t *num_lines)
{
	size_t lines = 1;
	std::string value;
	std::string line;

	while (is.good()) {
		lines++;
		std::getline(is, line);
		if (line == "\"\"\"")
			break;
		value += line;
		value.push_back('\n');
	}

	size_t len = value.size();
	if (len)
		value.erase(len - 1);

	if (num_lines)
		*num_lines = lines;

	return value;
}


bool Settings::readConfigFile(const char *filename)
{
	std::ifstream is(filename);
	if (!is.good())
		return false;

	return parseConfigLines(is, "");
}


bool Settings::parseConfigLines(std::istream &is, const std::string &end)
{
	std::lock_guard<std::mutex> lock(m_mutex);

	std::string line, name, value;

	while (is.good()) {
		std::getline(is, line);
		SettingsParseEvent event = parseConfigObject(line, end, name, value);

		switch (event) {
		case SPE_NONE:
		case SPE_INVALID:
		case SPE_COMMENT:
			break;
		case SPE_KVPAIR: {
			std::map<std::string, BoolSetting>::const_iterator it = m_bool_mapper.find(name);
			if (it != m_bool_mapper.end()) {
				if (value.compare("true") == 0 || value.compare("yes") == 0) {
					m_bool_settings[it->second] = true;
				}
				else if (value.compare("false") == 0 || value.compare("no") == 0) {
					m_bool_settings[it->second] = false;
				}
				break;
			}

			std::map<std::string, U16Setting>::const_iterator it2 = m_u16_mapper.find(name);
			if (it2 != m_u16_mapper.end()) {
				s32 val = std::atoi(value.c_str());
				if (val < 0) val = 0;
				else if (val > 65535) val = 65535;
				m_u16_settings[it2->second] = val;
				break;
			}

			std::map<std::string, S16Setting>::const_iterator it3 = m_s16_mapper.find(name);
			if (it3 != m_s16_mapper.end()) {
				s32 val = std::atoi(value.c_str());
				if (val < -32768) val = -32768;
				else if (val > 32767) val = 32767;
				m_s16_settings[it3->second] = val;
				break;
			}

			std::map<std::string, FloatSetting>::const_iterator it4 = m_float_mapper.find(name);
			if (it4 != m_float_mapper.end()) {
				m_float_settings[it4->second] = mystof(value);
				break;
			}

			m_settings[name] = SettingsEntry(value);
			break;
		}
		case SPE_END:
			return true;
		case SPE_GROUP: {
			Settings *group = new Settings;
			if (!group->parseConfigLines(is, "}")) {
				delete group;
				return false;
			}
			m_settings[name] = SettingsEntry(group);
			break;
		}
		case SPE_MULTILINE:
			m_settings[name] = SettingsEntry(getMultiline(is));
			break;
		}
	}

	return end.empty();
}


void Settings::writeLines(std::ostream &os, u32 tab_depth) const
{
	std::lock_guard<std::mutex> lock(m_mutex);

	for (std::map<std::string, SettingsEntry>::const_iterator
			it = m_settings.begin();
			it != m_settings.end(); ++it)
		printEntry(os, it->first, it->second, tab_depth);
}


void Settings::printEntry(std::ostream &os, const std::string &name,
	const SettingsEntry &entry, u32 tab_depth)
{
	for (u32 i = 0; i != tab_depth; i++)
		os << "\t";

	if (entry.is_group) {
		os << name << " = {\n";

		entry.group->writeLines(os, tab_depth + 1);

		for (u32 i = 0; i != tab_depth; i++)
			os << "\t";
		os << "}\n";
	} else {
		os << name << " = ";

		if (entry.value.find('\n') != std::string::npos)
			os << "\"\"\"\n" << entry.value << "\n\"\"\"\n";
		else
			os << entry.value << "\n";
	}
}


bool Settings::updateConfigObject(std::istream &is, std::ostream &os,
	const std::string &end, u32 tab_depth, bool mainconfig)
{
	std::map<std::string, SettingsEntry>::const_iterator it;
	std::map<std::string, BoolSetting>::const_iterator it_bool;
	std::map<std::string, U16Setting>::const_iterator it_u16;
	std::map<std::string, S16Setting>::const_iterator it_s16;
	std::map<std::string, S32Setting>::const_iterator it_s32;
	std::map<std::string, FloatSetting>::const_iterator it_float;
	std::set<std::string> present_entries;
	std::string line, name, value;
	bool was_modified = false;
	bool end_found = false;

	// Add any settings that exist in the config file with the current value
	// in the object if existing
	while (is.good() && !end_found) {
		std::getline(is, line);
		SettingsParseEvent event = parseConfigObject(line, end, name, value);

		switch (event) {
		case SPE_END:
			os << line << (is.eof() ? "" : "\n");
			end_found = true;
			break;
		case SPE_MULTILINE:
			value = getMultiline(is);
			/* FALLTHROUGH */
		case SPE_KVPAIR:
			it = m_settings.find(name);
			if (it != m_settings.end() &&
				(it->second.is_group || it->second.value != value)) {
				printEntry(os, name, it->second, tab_depth);
				was_modified = true;
			// Check in the bool store
			} else if (mainconfig && (it_bool = m_bool_mapper.find(name)) != m_bool_mapper.end()) {
				os << it_bool->first << " = " << (m_bool_settings[it_bool->second] ? "true" : "false") << std::endl;
				was_modified = true;
			// Check in the u16 store
			} else if (mainconfig && (it_u16 = m_u16_mapper.find(name)) != m_u16_mapper.end()) {
				os << it_u16->first << " = " << itos(m_u16_settings[it_u16->second]) << std::endl;
				was_modified = true;
			// Check in the s16 store
			} else if (mainconfig && (it_s16 = m_s16_mapper.find(name)) != m_s16_mapper.end()) {
				os << it_s16->first << " = " << itos(m_s16_settings[it_s16->second]) << std::endl;
				was_modified = true;
			// Check in the s32 store
			} else if (mainconfig && (it_s32 = m_s32_mapper.find(name)) != m_s32_mapper.end()) {
				os << it_s32->first << " = " << itos(m_s32_settings[it_s32->second]) << std::endl;
				was_modified = true;
			// Check in the float store
			} else if (mainconfig && (it_float = m_float_mapper.find(name)) != m_float_mapper.end()) {
				os << it_float->first << " = " << ftos(m_float_settings[it_float->second]) << std::endl;
				was_modified = true;
			} else {
				os << line << "\n";
				if (event == SPE_MULTILINE)
					os << value << "\n\"\"\"\n";
			}
			present_entries.insert(name);
			break;
		case SPE_GROUP:
			it = m_settings.find(name);
			if (it != m_settings.end() && it->second.is_group) {
				os << line << "\n";
				sanity_check(it->second.group != NULL);
				was_modified |= it->second.group->updateConfigObject(is, os,
					"}", tab_depth + 1);
			} else {
				printEntry(os, name, it->second, tab_depth);
				was_modified = true;
			}
			present_entries.insert(name);
			break;
		default:
			os << line << (is.eof() ? "" : "\n");
			break;
		}
	}

	// Add any settings in the object that don't exist in the config file yet
	for (it = m_settings.begin(); it != m_settings.end(); ++it) {
		if (present_entries.find(it->first) != present_entries.end())
			continue;

		printEntry(os, it->first, it->second, tab_depth);
		was_modified = true;
	}

	// If it's the main config, print all parameters
	if (mainconfig) {
		for (auto &bm: m_bool_rmapper) {
			if (present_entries.find(bm.second) != present_entries.end())
				continue;

			os << bm.second << " = " << (m_bool_settings[bm.first] ? "true" : "false") << std::endl;
			was_modified = true;
		}

		for (auto &um: m_u16_rmapper) {
			if (present_entries.find(um.second) != present_entries.end())
				continue;

			os << um.second << " = " << itos(m_u16_settings[um.first]) << std::endl;
			was_modified = true;
		}

		for (auto &um: m_s16_rmapper) {
			if (present_entries.find(um.second) != present_entries.end())
				continue;

			os << um.second << " = " << itos(m_s16_settings[um.first]) << std::endl;
			was_modified = true;
		}

		for (auto &um: m_s32_rmapper) {
			if (present_entries.find(um.second) != present_entries.end())
				continue;

			os << um.second << " = " << itos(m_s32_settings[um.first]) << std::endl;
			was_modified = true;
		}

		for (auto &um: m_float_rmapper) {
			if (present_entries.find(um.second) != present_entries.end())
				continue;

			os << um.second << " = " << ftos(m_float_settings[um.first]) << std::endl;
			was_modified = true;
		}
	}
	return was_modified;
}


bool Settings::updateConfigFile(const char *filename, bool mainconfig)
{
	std::lock_guard<std::mutex> lock(m_mutex);

	std::ifstream is(filename);
	std::ostringstream os(std::ios_base::binary);

	bool was_modified = updateConfigObject(is, os, "", 0, true);
	is.close();

	if (!was_modified)
		return true;

	if (!fs::safeWriteToFile(filename, os.str())) {
		logger.crit("Error writing configuration file: \"%s\"", filename);
		return false;
	}

	return true;
}

bool Settings::parseCommandLine(int argc, char **argv,
		const struct optparse_long *longopts)
{
	int option;
	struct optparse options;
	optparse_init(&options, argv);

	while ((option = optparse_long(&options, longopts, NULL)) != -1) {
		switch ((char) option) {
			case 'h': set("help", "true"); break;
			case 'c': set("config", options.optarg); break;
			case 'g': set("gameid", options.optarg); break;
			case 'i': set("info", "true"); break;
			case 'l': set("logfile", options.optarg); break;
			case 'L': set("log4cpp_props", options.optarg); break;
			case 'm': set("migrate", "true"); break;
			case 'p': set("port", options.optarg); break;
			case 'q': set("quiet", "true"); break;
			case 'u': set("run-unittests", "true"); break;
			case 't': set("trace", "true"); break;
			case 'v': set("verbose", "true"); break;
			case 'V': set("version", "true"); break;
			case 'w': set("world", options.optarg); break;
			case 'W': set("worldname", options.optarg); break;
#ifndef SERVER
			case 'a': set("address", options.optarg); break;
			case 'G': set("go", "true"); break;
			case 'P': set("password", options.optarg); break;
			case 'N': set("name", options.optarg); break;
			case 's': set("server", "true"); break;
			case 'T': set("speedtests", "true"); break;
			case 'x': set("videomodes", "true"); break;
#endif
			default: return false; break;
		}
	}

	return true;
}

/***********
 * Getters *
 ***********/


const SettingsEntry &Settings::getEntry(const std::string &name) const
{
	std::lock_guard<std::mutex> lock(m_mutex);

	std::map<std::string, SettingsEntry>::const_iterator n;
	if ((n = m_settings.find(name)) == m_settings.end()) {
		if ((n = m_defaults.find(name)) == m_defaults.end())
			throw SettingNotFoundException("Setting [" + name + "] not found.");
	}
	return n->second;
}


Settings *Settings::getGroup(const std::string &name) const
{
	const SettingsEntry &entry = getEntry(name);
	if (!entry.is_group)
		throw SettingNotFoundException("Setting [" + name + "] is not a group.");
	return entry.group;
}


std::string Settings::get(const std::string &name) const
{
	std::map<std::string,BoolSetting>::const_iterator it =  m_bool_mapper.find(name);
	if (it != m_bool_mapper.end()) {
		return m_bool_settings[it->second] ? "true" : "false";
	}

	std::map<std::string,U16Setting>::const_iterator it2 =  m_u16_mapper.find(name);
	if (it2 != m_u16_mapper.end()) {
		return itos(m_u16_settings[it2->second]);
	}

	std::map<std::string,S16Setting>::const_iterator it3 =  m_s16_mapper.find(name);
	if (it3 != m_s16_mapper.end()) {
		return itos(m_s16_settings[it3->second]);
	}

	std::map<std::string,FloatSetting>::const_iterator it4 =  m_float_mapper.find(name);
	if (it4 != m_float_mapper.end()) {
		return ftos(m_float_settings[it4->second]);
	}

	std::map<std::string,S32Setting>::const_iterator it5 =  m_s32_mapper.find(name);
	if (it5 != m_s32_mapper.end()) {
		return itos(m_s32_settings[it5->second]);
	}

	const SettingsEntry &entry = getEntry(name);
	if (entry.is_group)
		throw SettingNotFoundException("Setting [" + name + "] is a group.");
	return entry.value;
}

bool Settings::getBool(const std::string &name) const
{
	std::map<std::string, BoolSetting>::const_iterator it = m_bool_mapper.find(name);
	if (it == m_bool_mapper.end()) {
		return is_yes(get(name));
	}
	assert(it->second < BOOLSETTING_COUNT);
	return m_bool_settings[it->second];
}

s32 Settings::getS32(const std::string &name) const
{
	return std::atoi(get(name).c_str());
}


float Settings::getFloat(const std::string &name) const
{
	return std::atof(get(name).c_str());
}


u64 Settings::getU64(const std::string &name) const
{
	u64 value = 0;
	std::string s = get(name);
	std::istringstream ss(s);
	ss >> value;
	return value;
}

v3f Settings::getV3F(const std::string &name) const
{
	v3f value;
	const std::string str_val = get(name);
	static const std::regex re("[(](.+)[,](.+)[,](.+)[)]");
	std::smatch rem;
	std::regex_search(str_val, rem, re);

	try {
		value.X = mystof(rem.str(1));
		value.Y = mystof(rem.str(2));
		value.Z = mystof(rem.str(3));
	} catch (std::invalid_argument &) {
		logger.fatal("Invalid parameter found for config key '%s', aborting.", name.c_str());
		exit(1);
	}
	return value;
}

u32 Settings::getFlagStr(const std::string &name, const FlagDesc *flagdesc,
	u32 *flagmask) const
{
	std::string val = get(name);
	return std::isdigit(val[0])
		? std::atoi(val.c_str())
		: readFlagString(val, flagdesc, flagmask);
}

bool Settings::getNoiseParams(const std::string &name, NoiseParams &np) const
{
	return getNoiseParamsFromGroup(name, np) || getNoiseParamsFromValue(name, np);
}

bool Settings::getNoiseParamsFromValue(const std::string &name,
	NoiseParams &np) const
{
	std::string value;

	if (!getNoEx(name, value))
		return false;

	static const std::regex re("^(.+)[,][ ]*(.+)[,][ ]*[(](.+)[,][ ]*(.+)[,][ ]*(.+)[)][,][ ]*(.+)[,][ ]*(.+)[,][ ]*(.+)[,][ ]*(.+)$");
	std::smatch rem;
	std::regex_search(value, rem, re);

	if (rem.size() < 10) {
		logger.fatal("Invalid parameter found for config key '%s', aborting.", name.c_str());
		exit(1);
	}

	try {
		np.offset   = mystof(rem.str(1));
		np.scale    = mystof(rem.str(2));
		np.spread.X = mystof(rem.str(3));
		np.spread.Y = mystof(rem.str(4));
		np.spread.Z = mystof(rem.str(5));
		np.seed     = mystoi(rem.str(6));
		np.octaves  = mystoi(rem.str(7));
		np.persist  = mystof(rem.str(8));

		std::string optional_params = rem.str(9);

		if (!optional_params.empty())
			np.lacunarity = std::atof(optional_params.c_str());
	}
	catch (std::invalid_argument &) {
		logger.fatal("Invalid parameter found for config key '%s', aborting.", name.c_str());
		exit(1);
	}

	return true;
}

bool Settings::getNoiseParamsFromGroup(const std::string &name,
	NoiseParams &np) const
{
	Settings *group = NULL;

	if (!getGroupNoEx(name, group))
		return false;

	group->getFloatNoEx("offset",      np.offset);
	group->getFloatNoEx("scale",       np.scale);
	group->getV3FNoEx("spread",        np.spread);
	group->getS32NoEx("seed",          np.seed);
	group->getU16NoEx("octaves",       np.octaves);
	group->getFloatNoEx("persistence", np.persist);
	group->getFloatNoEx("lacunarity",  np.lacunarity);

	np.flags = 0;
	if (!group->getFlagStrNoEx("flags", np.flags, flagdesc_noiseparams))
		np.flags = NOISE_FLAG_DEFAULTS;

	return true;
}

bool Settings::exists(const std::string &name) const
{
	return (m_settings.find(name) != m_settings.end() ||
		m_defaults.find(name) != m_defaults.end());
}

bool Settings::boolExists(const std::string &name) const
{
	return (m_bool_mapper.find(name) != m_bool_mapper.end());
}

void Settings::initSetting(BoolSetting b, const std::string &key, bool value)
{
	set(b, value);
	m_bool_mapper[key] = b;
	m_bool_rmapper[b] = key;
}

void Settings::initSetting(U16Setting u, const std::string &key, u16 value)
{
	set(u, value);
	m_u16_mapper[key] = u;
	m_u16_rmapper[u] = key;
}

void Settings::initSetting(S16Setting u, const std::string &key, s16 value)
{
	set(u, value);
	m_s16_mapper[key] = u;
	m_s16_rmapper[u] = key;
}

void Settings::initSetting(FloatSetting u, const std::string &key, float value)
{
	set(u, value);
	m_float_mapper[key] = u;
	m_float_rmapper[u] = key;
}

void Settings::initSetting(S32Setting u, const std::string &key, s32 value)
{
	set(u, value);
	m_s32_mapper[key] = u;
	m_s32_rmapper[u] = key;
}

void Settings::initSettings()
{
#ifndef SERVER
	initSetting(BOOLSETTING_ANISTROPIC_FILTER, "anisotropic_filter", false);
	initSetting(BOOLSETTING_AUX1_DESCENDS, "aux1_descends", false);
	initSetting(BOOLSETTING_BILINEAR_FILTER, "bilinear_filter", false);
	initSetting(BOOLSETTING_CONNECTED_GLASS, "connected_glass", false);
	initSetting(BOOLSETTING_CONTINOUS_FORWARD, "continuous_forward", false);
	initSetting(BOOLSETTING_CRAFTRESULT_IS_PREVIEW, "craftresult_is_preview", false); // @ TODO export to player dedicated settings
	initSetting(BOOLSETTING_DESYNCHRONIZE_MAPBLOCK_TEXTURE_ANIMATION, "desynchronize_mapblock_texture_animation", true);
	initSetting(BOOLSETTING_DIRECTIONAL_COLORED_FLAG, "directional_colored_fog", true);
	initSetting(BOOLSETTING_ENABLE_3D_CLOUDS, "enable_3d_clouds", true);
	initSetting(BOOLSETTING_ENABLE_BUILD_WHERE_YOU_STAND, "enable_build_where_you_stand", false);
	initSetting(BOOLSETTING_ENABLE_BUMPMAPPING, "enable_bumpmapping", false);
	initSetting(BOOLSETTING_ENABLE_CLOUDS, "enable_clouds", true);
	initSetting(BOOLSETTING_ENABLE_FOG, "enable_fog", true);
	initSetting(BOOLSETTING_ENABLE_LOCALMAP_SAVING, "enable_local_map_saving", false);
	initSetting(BOOLSETTING_ENABLE_MENU_MUSIC, "enable_menu_music", true);
	initSetting(BOOLSETTING_ENABLE_MESHCACHE, "enable_mesh_cache", false);
	initSetting(BOOLSETTING_ENABLE_NODE_HIGHLIGHTING, "enable_node_nighlighting", false);
	initSetting(BOOLSETTING_ENABLE_PARTICLES, "enable_particles", true);
	initSetting(BOOLSETTING_ENABLE_REMOTEMEDIA_SERVER, "enable_remote_media_server", true);
	initSetting(BOOLSETTING_ENABLE_SHADERS, "enable_shaders", true);
	initSetting(BOOLSETTING_ENABLE_WAVING_LEAVES, "enable_waving_leaves", false);
	initSetting(BOOLSETTING_ENABLE_WAVING_PLANTS, "enable_waving_plants", false);
	initSetting(BOOLSETTING_ENABLE_WAVING_WATER, "enable_waving_water", false);
	initSetting(BOOLSETTING_FULLSCREEN, "fullscreen", false);
	initSetting(BOOLSETTING_GENERATE_NORMALMAPS, "generate_normalmaps", false);
	initSetting(BOOLSETTING_GUI_SCALING_FILTER, "gui_scaling_filter", false);
	initSetting(BOOLSETTING_GUI_SCALING_FILTER_TXR2IMG, "gui_scaling_filter_txr2img", true);
	initSetting(BOOLSETTING_HIGH_PRECISION_FPU, "high_precision_fpu", true);
	initSetting(BOOLSETTING_INVERT_MOUSE, "invert_mouse", false);
	initSetting(BOOLSETTING_MINIMAP_DOUBLE_SCAN_HEIGHT, "minimap_double_scan_height", true);
	initSetting(BOOLSETTING_MIP_MAP, "mip_map", true);
	initSetting(BOOLSETTING_MENU_CLOUDS, "menu_clouds", true);
	initSetting(BOOLSETTING_MODSTORE_DISABLE_SPECIAL_HTTP_HEADER, "modstore_disable_special_http_header", false);
	initSetting(BOOLSETTING_OPAQUE_WATER, "opaque_water", false);
	initSetting(BOOLSETTING_ENABLE_PARALLAX_OCCLUSION, "enable_parallax_occlusion", false);
	initSetting(BOOLSETTING_RANDOM_INPUT, "random_input", false);
	initSetting(BOOLSETTING_SHOW_DEBUG, "show_debug", false);
	initSetting(BOOLSETTING_SMOOTH_LIGHTING, "smooth_lighting", true);
	initSetting(BOOLSETTING_ENABLE_SOUND, "enable_sound", true);
	initSetting(BOOLSETTING_TEXTURE_FILTER_TRANSPARENT, "texture_clean_transparent", false);
	initSetting(BOOLSETTING_TOUCHTARGET, "touchtarget", false);
	initSetting(BOOLSETTING_TRILINEAR_FILTER, "trilinear_filter", false);
	initSetting(BOOLSETTING_VIEW_BOBBING, "view_bobbing", true);
	initSetting(BOOLSETTING_VSYNC, "vsync", false);
	initSetting(BOOLSETTING_ENABLE_INVENTORYITEMS_ANIMATIONS, "inventory_items_animations", false);
	initSetting(BOOLSETTING_TONE_MAPPING, "tone_mapping", false);
	initSetting(BOOLSETTING_SHOW_ENTITY_SELECTIONBOX, "show_entity_selectionbox", true);

	initSetting(U16SETTING_CLOUD_RADIUS, "cloud_radius", 12);
	initSetting(U16SETTING_CROSSHAIR_ALPHA, "crosshair_alpha", 255);
	initSetting(U16SETTING_FSAA, "fsaa", 0);
	initSetting(U16SETTING_FULLSCREEN_BPP, "fullscreen_bpp", 24);
	initSetting(U16SETTING_SCREEN_HEIGHT, "screenH", 600);
	initSetting(U16SETTING_SCREEN_WIDTH, "screenW", 800);
	initSetting(U16SETTING_SERVERMAP_SAVE_INTERVAL, "server_map_save_interval", 5);
	initSetting(U16SETTING_3D_MODE, "3d_mode", 0);

	initSetting(S16SETTING_CLOUD_HEIGHT, "cloud_height", 120);
	initSetting(S16SETTING_SELECTIONBOX_WIDTH, "selectionbox_width", 2);
	initSetting(S16SETTING_VIEWING_RANGE, "viewing_range", 100);

	initSetting(FSETTING_CAMERA_SMOOTHING, "camera_smoothing", 0.0f);
	initSetting(FSETTING_CINEMATIC_CAMERA_SMOOTHING, "cinematic_camera_smoothing", 0.7f);
	initSetting(FSETTING_CLIENT_UNLOAD_DATA_TIMEOUT, "client_unload_unused_data_timeout", 600.0f);
	initSetting(FSETTING_DISPLAY_GAMMA, "display_gamma", 1.8f);
	initSetting(FSETTING_FALL_BOBBING_AMOUNT, "fall_bobbing_amount", 0.0f);
	initSetting(FSETTING_FOV, "fov", 72.0f);
	initSetting(FSETTING_FPS_MAX, "fps_max", 60.0f);
	initSetting(FSETTING_GUI_SCALING, "gui_scaling", 1.0f);
	initSetting(FSETTING_HUD_HOTBAR_MAX_WIDTH, "hud_hotbar_max_width",1.0f);
	initSetting(FSETTING_HUD_SCALING, "hud_scaling", 1.0f);
	initSetting(FSETTING_MOUSE_SENSITIVITY, "mouse_sensitivity", 0.2f);
	initSetting(FSETTING_NORMALMAPS_SMOOTH, "normalmaps_smooth", 1.0f);
	initSetting(FSETTING_NORMALMAPS_STRENGTH, "normalmaps_strength", 0.6f);
	initSetting(FSETTING_PARALLAX_OCCLUSION_BIAS, "parallax_occlusion_bias", 0.04f);
	initSetting(FSETTING_PARALLAX_OCCLUSION_ITERATIONS, "parallax_occlusion_iterations", 4.0f);
	initSetting(FSETTING_PARALLAX_OCCLUSION_MODE, "parallax_occlusion_mode", 1.0f);
	initSetting(FSETTING_PARALLAX_OCCLUSION_SCALE, "parallax_occlusion_scale", 0.08f);
	initSetting(FSETTING_PAUSE_FPS_MAX, "pause_fps_max", 20.0f);
	initSetting(FSETTING_REPEAT_RIGHTCLICK_TIME, "repeat_rightclick_time", 0.25f);
	initSetting(FSETTING_SOUND_VOLUME, "sound_volume", 0.8f);
	initSetting(FSETTING_VIEW_BOBBING_AMOUNT, "view_bobbing_amount", 1.0f);
	initSetting(FSETTING_WANTED_FPS, "wanted_fps", 30.0f);
	initSetting(FSETTING_WATER_WAVE_HEIGHT, "water_wave_height", 1.0f);
	initSetting(FSETTING_WATER_WAVE_LENGTH, "water_wave_length", 20.0f);
	initSetting(FSETTING_WATER_WAVE_SPEED, "water_wave_speed", 5.0f);
	initSetting(FSETTING_3D_PARALAX_STRENGTH, "3d_paralax_strength", 0.025f);

	initSetting(S32SETTING_CLIENT_MAPBLOCK_LIMIT, "client_mapblock_limit", 5000);
	initSetting(S32SETTING_CONSOLE_ALPHA, "console_alpha", 200);
	initSetting(S32SETTING_TEXTURE_MIN_SIZE, "texture_min_size", 64);
	initSetting(S32SETTING_TOOLTIP_SHOW_DELAY, "tooltip_show_delay", 400);
#ifdef __ANDROID__
	initSetting(BOOLSETTING_CURL_VERIFY_CERT, "curl_verify_cert", false);
	initSetting(BOOLSETTING_ENABLE_PARTICLES, "enable_particles", false);
	initSetting(BOOLSETTING_ENABLE_SHADERS, "enable_shaders", false);
	initSetting(BOOLSETTING_FULLSCREEN, "fullscreen", true);
	initSetting(BOOLSETTING_INVENTORY_IMAGE_HACK, "inventory_image_hack", false);
	initSetting(BOOLSETTING_SMOOTH_LIGHTING, "smooth_lighting", false);
	initSetting(BOOLSETTING_TOUCHTARGET, "touchtarget", true);

	initSetting(U16SETTING_SCREEN_HEIGHT, "screenH", 0);
	initSetting(U16SETTING_SCREEN_WIDTH, "screenW", 0);
	initSetting(U16SETTING_TOUCHSCREEN_THRESHOLD, "touchscreen_threshold", 20);

	initSetting(S16SETTING_VIEWING_RANGE, "viewing_range", 50);

	//check for device with small screen
	float x_inches = ((double) porting::getDisplaySize().X /
			(160 * porting::getDisplayDensity()));
	if (x_inches  < 3.5) {
		initSetting(FSETTING_HUD_SCALING, "hud_scaling", 0.6f);
	}
	else if (x_inches < 4.5) {
		initSetting(FSETTING_HUD_SCALING, "hud_scaling", 0.7f);
	}
#else
	initSetting(FSETTING_SCREEN_DPI, "screen_dpi", 72.0f);
#endif // END ANDROID
#endif // END !SERVER
	initSetting(U16SETTING_FONT_SIZE, "font_size", TTF_DEFAULT_FONT_SIZE);
	initSetting(U16SETTING_MONO_FONT_SIZE, "mono_font_size", TTF_DEFAULT_FONT_SIZE);
	initSetting(U16SETTING_FALLBACK_FONT_SIZE, "fallback_font_size", TTF_DEFAULT_FONT_SIZE);

	initSetting(BOOLSETTING_ASK_RECONNECT_ON_CRASH, "ask_reconnect_on_crash", false);
	initSetting(BOOLSETTING_CREATIVE_MODE, "creative_mode", false);
	initSetting(BOOLSETTING_CURL_VERIFY_CERT, "curl_verify_cert", true);
	initSetting(BOOLSETTING_DISABLE_ANTICHEAT, "disable_anticheat", false);
	initSetting(BOOLSETTING_DISALLOW_GUEST_CONNECTION, "disallow_guest_connection", false);
	initSetting(BOOLSETTING_ENABLE_MULTICASE_USERNAMES, "enable_multicase_usernames", false);
	initSetting(BOOLSETTING_ENABLE_DAMAGE, "enable_damage", true);
	initSetting(BOOLSETTING_ENABLE_IPV6, "enable_ipv6", true);
	initSetting(BOOLSETTING_ENABLE_PVP, "enable_pvp", true);
	initSetting(BOOLSETTING_ENABLE_PVP_PLAYER_CAN_PER_AREA, "enable_pvp_per_area_for_players", true);
	initSetting(BOOLSETTING_ENABLE_TIPS, "enable_tips", true);
	initSetting(BOOLSETTING_IGNORE_WORLD_LOAD_ERRORS, "ignore_world_load_errors", false);
	initSetting(BOOLSETTING_IPV6_SERVER, "ipv6_server", false);
	initSetting(BOOLSETTING_SECURE_ENABLE_SECURITY, "secure.enable_security", false);
	initSetting(BOOLSETTING_SERVER_ANNOUNCE, "server_announce", false);
	initSetting(BOOLSETTING_STRICT_PROTOCOL_VERSION, "strict_protocol_version_checking", false);
	initSetting(BOOLSETTING_UNLIMITED_PLAYER_TRANSFER_DISTANCE, "unlimited_player_transfer_distance", true);
	initSetting(BOOLSETTING_ENABLE_CONSOLE, "enable_console", true);
	initSetting(BOOLSETTING_ENABLE_INVENTORY_WAYPOINTS, "enable_inventory_waypoints", true);
	initSetting(BOOLSETTING_ENABLE_FIRE, "enable_fire", true);
	initSetting(BOOLSETTING_ENABLE_MOB_SPAWNING_IN_PROTECTED_AREAS, "enable_mob_spawning_in_protected_areas", false);
	initSetting(BOOLSETTING_ENABLE_AUTOGENERATING_MAP_WHEN_INACTIVE, "enable_autogenerating_map_when_inactive", true);
	initSetting(BOOLSETTING_ENABLE_EXPLOSION_PROTECTION_TARGETOWNER, "enable_explosion_protection_targetowner", true);
	initSetting(BOOLSETTING_ENABLE_MAINTENANCE_MODE, "enable_maintenance_mode", false);
	initSetting(BOOLSETTING_ENABLE_RULES_FORM, "enable_rules_form", false);
	initSetting(BOOLSETTING_ENABLE_RULES_AUTOPRIVS, "enable_rules_autoprivs", false);
	initSetting(BOOLSETTING_ENABLE_INTERSERVER_CHAT_CLIENT, "enable_interserver_chat_client", false);
	initSetting(BOOLSETTING_ENABLE_STATSERVER, "enable_stats_server", false);

	// Mods
	initSetting(BOOLSETTING_ENABLE_MOD_SETHOME, "enable_mod_sethome", true);
	initSetting(BOOLSETTING_ENABLE_MOD_MANA, "enable_mod_mana", true);
	initSetting(BOOLSETTING_ENABLE_MOD_XP, "enable_mod_xp", true);

	initSetting(U16SETTING_DEBUG_LOG_LEVEL, "debug_log_level", 2);
	initSetting(U16SETTING_EMERGEQUEUE_LIMIT_DISKONLY, "emergequeue_limit_diskonly", 32);
	initSetting(U16SETTING_EMERGEQUEUE_LIMIT_GENERATE, "emergequeue_limit_generate", 32);
	initSetting(U16SETTING_EMERGEQUEUE_LIMIT_TOTAL, "emergequeue_limit_total", 256);
	initSetting(U16SETTING_LIQUID_QUEUE_PURGE_TIME, "liquid_queue_purge_time", 0);
	initSetting(U16SETTING_MAX_CHATMESSAGE_LENGTH, "max_chatmessage_length", 500);
	initSetting(U16SETTING_MAP_GENERATION_LIMIT, "map_generation_limit", 31000);
	initSetting(U16SETTING_MAX_OBJECTS_PER_BLOCK, "max_objects_per_block", 49);
	initSetting(U16SETTING_MAX_PACKETS_PER_ITERATION,  "max_packets_per_iteration", 1024);
	initSetting(U16SETTING_MAX_SIMULATENOUS_BLOCK_SENDS_PER_CLIENT, "max_simultaneous_block_sends_per_client", 10);
	initSetting(U16SETTING_MAX_USERS, "max_users", 15);
	initSetting(U16SETTING_MIN_PLAYERNAME_SIZE, "min_playername_size", 3);
	initSetting(U16SETTING_NUM_EMERGE_THREADS, "num_emerge_threads", 1);
	initSetting(U16SETTING_PORT, "port", 30000);
	initSetting(U16SETTING_EPIXEL_PORT, "epixel_port", 30008);
	initSetting(U16SETTING_REDIS_PORT, "redis_port", 6379);
	initSetting(U16SETTING_SQLITE_SYNCHRONOUS, "sqlite_synchronous", 2);
	initSetting(U16SETTING_WORKAROUND_WINDOW_SIZE, "workaround_window_size", 5);
	initSetting(U16SETTING_MINTIME_BETWEEN_TIPS, "minimum_time_between_tips", 200);
	initSetting(U16SETTING_MANA_MAX, "mana_max", 200);
	initSetting(U16SETTING_MANA_DEFAULT_REGEN, "mana_default_regen", 1);
	initSetting(U16SETTING_MANA_COST_HOME, "mana_cost_home", 50);
	initSetting(U16SETTING_SAVE_META_INTERVAL, "save_meta_interval", 12);
	initSetting(U16SETTING_MAPDB_POOL_SIZE, "mapdb_pool_size", 3);
	initSetting(U16SETTING_GAMEDB_POOL_SIZE, "gamedb_pool_size", 3);
	initSetting(U16SETTING_MG_VALLEYS_ALTITUDE_CHILL, "mg_valleys_altitude_chill", 90);
	initSetting(U16SETTING_MG_VALLEYS_CAVE_WATER_MAX_HEIGHT, "mg_valleys_cave_water_max_height", 31000);
	initSetting(U16SETTING_MG_VALLEYS_HUMIDITY, "mg_valleys_humidity", 50);
	initSetting(U16SETTING_MG_VALLEYS_HUMIDITY_BREAK_POINT, "mg_valleys_humidity_break_point", 65);
	initSetting(U16SETTING_MG_VALLEYS_LAVA_MAX_HEIGHT, "mg_valleys_lava_max_height", 0);
	initSetting(U16SETTING_MG_VALLEYS_RIVER_DEPTH, "mg_valleys_river_depth", 4);
	initSetting(U16SETTING_MG_VALLEYS_RIVER_SIZE, "mg_valleys_river_size", 5);
	initSetting(U16SETTING_MG_VALLEYS_TEMPERATURE, "mg_valleys_temperature", 50);
	initSetting(U16SETTING_MG_VALLEYS_WATER_FEATURE, "mg_valleys_water_features", 3);
	initSetting(U16SETTING_INTERSERVER_CHAT_PORT, "interserver_chat_port", 30050);
	initSetting(U16SETTING_INTERSERVER_CHAT_TIMEOUT, "interserver_chat_timeout", 30);
	initSetting(U16SETTING_MESSAGE_RATE_KICK, "message_rate_kick", 30);
	initSetting(U16SETTING_MG_VERSION, "mg_version", 7); // Voir mg_name (doublon ???)

	initSetting(S16SETTING_ACTIVE_BLOCK_RANGE, "active_block_range", 2);
	initSetting(S16SETTING_ACTIVEOBJECT_SEND_RANGE_BLOCKS, "active_object_send_range_blocks", 3);
	initSetting(S16SETTING_CHUNK_SIZE, "chunksize", 5);
	initSetting(S16SETTING_CURL_PARALLEL_LIMIT, "curl_parallel_limit", 8);
	initSetting(S16SETTING_MAX_BLOCK_SEND_DISTANCE, "max_block_send_distance", 9);
	initSetting(S16SETTING_MAX_BLOCK_GENERATE_DISTANCE, "max_block_generate_distance", 7);
	initSetting(S16SETTING_PLAYER_TRANSFER_DISTANCE, "player_transfer_distance", 0);
	initSetting(S16SETTING_VERTICAL_SPAWN_RANGE, "vertical_spawn_range", 16);
	initSetting(S16SETTING_WATER_LEVEL, "water_level", 1);

	initSetting(FSETTING_AMBIENT_OCCLUSION_GAMMA, "ambient_occlusion_gamma", 2.2f);
	initSetting(FSETTING_DEDICATED_SERVER_STEP, "dedicated_server_step", 0.025f);
	initSetting(FSETTING_FULL_BLOCK_SEND_ENABLE_MIN_TIME_FROM_BUILDING, "full_block_send_enable_min_time_from_building", 2.0f);
	initSetting(FSETTING_LIQUID_UPDATE, "liquid_update", 1.0f);
	initSetting(FSETTING_MANA_REGEN_TIMER, "mana_regen_timer", 5.0f);
	initSetting(FSETTING_MOVEMENT_ACCELERATION_DEFAULT, "movement_acceleration_default", 3.0f);
	initSetting(FSETTING_MOVEMENT_ACCELERATION_AIR, "movement_acceleration_air", 2.0f);
	initSetting(FSETTING_MOVEMENT_ACCELERATION_FAST, "movement_acceleration_fast", 10.0f);
	initSetting(FSETTING_MOVEMENT_GRAVITY, "movement_gravity", 9.81f);
	initSetting(FSETTING_MOVEMENT_LIQUID_FLUIDITY, "movement_liquid_fluidity", 1.0f);
	initSetting(FSETTING_MOVEMENT_LIQUID_FLUIDITY_SMOOTH, "movement_liquid_fluidity_smooth", 0.5f);
	initSetting(FSETTING_MOVEMENT_LIQUID_SINK, "movement_liquid_sink", 10.0f);
	initSetting(FSETTING_MOVEMENT_SPEED_CLIMB, "movement_speed_climb", 2.0f);
	initSetting(FSETTING_MOVEMENT_SPEED_CROUCH, "movement_speed_crouch", 1.35f);
	initSetting(FSETTING_MOVEMENT_SPEED_FAST, "movement_speed_fast", 20.0f);
	initSetting(FSETTING_MOVEMENT_SPEED_JUMP, "movement_speed_jump", 6.5f);
	initSetting(FSETTING_MOVEMENT_SPEED_WALK, "movement_speed_walk", 4.0f);
	initSetting(FSETTING_PLAYER_HOME_INTERVAL, "time_player_home_interval", 30.0f);
	initSetting(FSETTING_PLAYER_EXHAUS_ONDIG, "player_exhaus_on_dig", 3.0f);
	initSetting(FSETTING_PLAYER_EXHAUS_ONMOVE, "player_exhaus_on_move", 0.3f);
	initSetting(FSETTING_PLAYER_EXHAUS_ONPLACE, "player_exhaus_on_place", 1.0f);
	initSetting(FSETTING_PLAYER_HUNGER_TICK, "player_hunger_tick", 160.0f);
	initSetting(FSETTING_SERVER_MAP_SAVE_INTERVAL, "server_map_save_interval", 5.3f);
	initSetting(FSETTING_SERVER_UNLOAD_UNUSED_DATA_TIMEOUT, "server_unload_unused_data_timeout", 29.0f);
	initSetting(FSETTING_TIME_SPEED, "time_speed", 18.0f);
	initSetting(FSETTING_MOD_XP_LOSS_DEAD, "xp_loss_dead", 0.3f);
	initSetting(FSETTING_MOD_XP_WIN_PVP, "xp_win_pvp", 0.3f);
	initSetting(FSETTING_TIME_SEND_INTERVAL, "time_send_interval", 5.0f);
	initSetting(FSETTING_PLAYER_HOME_INTERVAL, "time_player_home_interval", 30.0f);
	initSetting(FSETTING_MANA_REGEN_TIMER, "mana_regen_timer", 0.7f);
	initSetting(FSETTING_RULES_ACCEPTANCE_TIMEOUT, "rules_acceptance_timeout", 180.0f);
	initSetting(FSETTING_MAX_MESSAGES_PER_10SEC, "max_messages_per_10sec", 8);
	initSetting(FSETTING_ACTIVE_BLOCK_MGMT_INTERVAL, "active_block_mgmt_interval", 2.0f);
	initSetting(FSETTING_NODETIMER_INTERVAL, "nodetimer_interval", 1.0f);
	initSetting(FSETTING_ABM_INTERVAL, "abm_interval", 1.0f);
	initSetting(FSETTING_DROWNING_INTERVAL, "drowning_interval", 2.0f);
	initSetting(FSETTING_BREATH_INTERVAL, "breath_interval", 0.5f);

	initSetting(S32SETTING_CURL_TIMEOUT, "curl_timeout", 5000);
	initSetting(S32SETTING_LIQUID_LOOP_MAX, "liquid_loop_max", 100000);
	initSetting(S32SETTING_MAX_CLEAROBJECTS_EXTRA_LOADED_BLOCKS, "max_clearobjects_extra_loaded_blocks", 4096);
	initSetting(S32SETTING_MAX_SIMULTANEOUS_BLOCK_SENDS_SERVER_TOTAL, "max_simultaneous_block_sends_server_total", 40);
}


std::vector<std::string> Settings::getNames() const
{
	std::vector<std::string> names;
	for (std::map<std::string, SettingsEntry>::const_iterator
			i = m_settings.begin();
			i != m_settings.end(); ++i) {
		names.push_back(i->first);
	}
	return names;
}



/***************************************
 * Getters that don't throw exceptions *
 ***************************************/


bool Settings::getGroupNoEx(const std::string &name, Settings *&val) const
{
	try {
		val = getGroup(name);
		return true;
	} catch (SettingNotFoundException &e) {
		return false;
	}
}


bool Settings::getNoEx(const std::string &name, std::string &val) const
{
	try {
		val = get(name);
		return true;
	} catch (SettingNotFoundException &e) {
		return false;
	}
}


bool Settings::getFlag(const std::string &name) const
{
	try {
		return getBool(name);
	} catch(SettingNotFoundException &e) {
		return false;
	}
}


bool Settings::getFloatNoEx(const std::string &name, float &val) const
{
	try {
		std::map<std::string, FloatSetting>::const_iterator it = m_float_mapper.find(name);
		if (it == m_float_mapper.end()) {
			val = std::atof(get(name).c_str());
			return true;
		}
		assert(it->second < FSETTING_COUNT);
		val = m_float_settings[it->second];
		return true;
		return true;
	} catch (SettingNotFoundException &e) {
		return false;
	}
}

bool Settings::getU16NoEx(const std::string &name, u16 &val) const
{
	try {
		std::map<std::string, U16Setting>::const_iterator it = m_u16_mapper.find(name);
		if (it == m_u16_mapper.end()) {
			s32 tmpval = std::atoi(get(name).c_str());
			if (tmpval < 0) tmpval = 0;
			if (tmpval > 65535) tmpval = 65535;
			val = tmpval;
			return true;
		}
		assert(it->second < U16SETTING_COUNT);
		val = m_u16_settings[it->second];
		return true;
	} catch (SettingNotFoundException &) {
		return false;
	}
}

bool Settings::getS32NoEx(const std::string &name, s32 &val) const
{
	try {
		std::map<std::string, S32Setting>::const_iterator it = m_s32_mapper.find(name);
		if (it == m_s32_mapper.end()) {
			val = std::atoi(get(name).c_str());
			return true;
		}
		assert(it->second < S32SETTING_COUNT);
		val = m_s32_settings[it->second];
		return true;
	} catch (SettingNotFoundException &e) {
		return false;
	}
}

bool Settings::getV3FNoEx(const std::string &name, v3f &val) const
{
	try {
		val = getV3F(name);
		return true;
	} catch (SettingNotFoundException &e) {
		return false;
	}
}


// N.B. getFlagStrNoEx() does not set val, but merely modifies it.  Thus,
// val must be initialized before using getFlagStrNoEx().  The intention of
// this is to simplify modifying a flags field from a default value.
bool Settings::getFlagStrNoEx(const std::string &name, u32 &val,
	FlagDesc *flagdesc) const
{
	try {
		u32 flags, flagmask;

		flags = getFlagStr(name, flagdesc, &flagmask);

		val &= ~flagmask;
		val |=  flags;

		return true;
	} catch (SettingNotFoundException &e) {
		return false;
	}
}


/***********
 * Setters *
 ***********/

bool Settings::setEntry(const std::string &name, const void *data,
	bool set_group, bool set_default)
{
	Settings *old_group = NULL;

	if (!checkNameValid(name))
		return false;
	if (!set_group && !checkValueValid(*(const std::string *)data))
		return false;

	{
		std::lock_guard<std::mutex> lock(m_mutex);

		SettingsEntry &entry = set_default ? m_defaults[name] : m_settings[name];
		old_group = entry.group;

		entry.value    = set_group ? "" : *(const std::string *)data;
		entry.group    = set_group ? *(Settings **)data : NULL;
		entry.is_group = set_group;
	}

	delete old_group;

	return true;
}


bool Settings::set(const std::string &name, const std::string &value)
{
	std::map<std::string,BoolSetting>::const_iterator it =  m_bool_mapper.find(name);
	if (it != m_bool_mapper.end()) {
		m_bool_settings[it->second] = is_yes(value);
		return true;
	}

	std::map<std::string,U16Setting>::const_iterator it2 =  m_u16_mapper.find(name);
	if (it2 != m_u16_mapper.end()) {
		m_u16_settings[it2->second] = std::atoi(value.c_str());
		return true;
	}

	std::map<std::string,S16Setting>::const_iterator it3 =  m_s16_mapper.find(name);
	if (it3 != m_s16_mapper.end()) {
		m_s16_settings[it3->second] = std::atoi(value.c_str());
		return true;
	}

	std::map<std::string,FloatSetting>::const_iterator it4 =  m_float_mapper.find(name);
	if (it4 != m_float_mapper.end()) {
		m_float_settings[it4->second] = std::atof(value.c_str());
		return true;
	}

	std::map<std::string,S32Setting>::const_iterator it5 =  m_s32_mapper.find(name);
	if (it5 != m_s32_mapper.end()) {
		m_s32_settings[it5->second] = std::atoi(value.c_str());
		return true;
	}

	if (!setEntry(name, &value, false, false))
		return false;

	doCallbacks(name);
	return true;
}


bool Settings::setDefault(const std::string &name, const std::string &value)
{
	return setEntry(name, &value, false, true);
}

void Settings::set(const BoolSetting b, bool value)
{
	assert(b < BOOLSETTING_COUNT);
	m_bool_settings[b] = value;
}

void Settings::set(const U16Setting u, u16 value)
{
	assert(u < U16SETTING_COUNT);
	m_u16_settings[u] = value;
}

void Settings::set(const S16Setting u, s16 value)
{
	assert(u < S16SETTING_COUNT);
	m_s16_settings[u] = value;
}

void Settings::set(const FloatSetting u, float value)
{
	assert(u < FSETTING_COUNT);
	m_float_settings[u] = value;
}

void Settings::set(const S32Setting u, s32 value)
{
	assert(u < S32SETTING_COUNT);
	m_s32_settings[u] = value;
}

bool Settings::setBool(const std::string &name, bool value)
{
	std::map<std::string,BoolSetting>::const_iterator it =  m_bool_mapper.find(name);
	if (it != m_bool_mapper.end()) {
		m_bool_settings[it->second] = value;
		return true;
	}
	return set(name, value ? "true" : "false");
}

bool Settings::setU16(const std::string &name, u16 value)
{
	return set(name, itos(value));
}


bool Settings::setS32(const std::string &name, s32 value)
{
	return set(name, itos(value));
}


bool Settings::setU64(const std::string &name, u64 value)
{
	std::ostringstream os;
	os << value;
	return set(name, os.str());
}


bool Settings::setFloat(const std::string &name, float value)
{
	return set(name, ftos(value));
}


bool Settings::setV3F(const std::string &name, v3f value)
{
	std::ostringstream os;
	os << "(" << value.X << "," << value.Y << "," << value.Z << ")";
	return set(name, os.str());
}


bool Settings::setFlagStr(const std::string &name, u32 flags,
	const FlagDesc *flagdesc, u32 flagmask)
{
	return set(name, writeFlagString(flags, flagdesc, flagmask));
}

bool Settings::setNoiseParams(const std::string &name,
	const NoiseParams &np, bool set_default)
{
	Settings *group = new Settings;

	group->setFloat("offset",      np.offset);
	group->setFloat("scale",       np.scale);
	group->setV3F("spread",        np.spread);
	group->setS32("seed",          np.seed);
	group->setU16("octaves",       np.octaves);
	group->setFloat("persistence", np.persist);
	group->setFloat("lacunarity",  np.lacunarity);
	group->setFlagStr("flags",     np.flags, flagdesc_noiseparams, np.flags);

	return setEntry(name, &group, true, set_default);
}

void Settings::clear()
{
	std::lock_guard<std::mutex> lock(m_mutex);
	clearNoLock();
}

void Settings::clearDefaults()
{
	std::lock_guard<std::mutex> lock(m_mutex);
	clearDefaultsNoLock();
}


void Settings::update(const Settings &other)
{
	if (&other == this)
		return;

	std::lock_guard<std::mutex> lock(m_mutex);
	std::lock_guard<std::mutex> lock2(other.m_mutex);

	updateNoLock(other);
}


SettingsParseEvent Settings::parseConfigObject(const std::string &line,
	const std::string &end, std::string &name, std::string &value)
{
	std::string trimmed_line = trim(line);

	if (trimmed_line.empty())
		return SPE_NONE;
	if (trimmed_line[0] == '#')
		return SPE_COMMENT;
	if (trimmed_line == end)
		return SPE_END;

	size_t pos = trimmed_line.find('=');
	if (pos == std::string::npos)
		return SPE_INVALID;

	name  = trim(trimmed_line.substr(0, pos));
	value = trim(trimmed_line.substr(pos + 1));

	if (value == "{")
		return SPE_GROUP;
	if (value == "\"\"\"")
		return SPE_MULTILINE;

	return SPE_KVPAIR;
}


void Settings::updateNoLock(const Settings &other)
{
	m_settings.insert(other.m_settings.begin(), other.m_settings.end());
	m_defaults.insert(other.m_defaults.begin(), other.m_defaults.end());
}


void Settings::clearNoLock()
{
	std::map<std::string, SettingsEntry>::const_iterator it;
	for (it = m_settings.begin(); it != m_settings.end(); ++it)
		delete it->second.group;
	m_settings.clear();

	clearDefaultsNoLock();
}

void Settings::clearDefaultsNoLock()
{
	std::map<std::string, SettingsEntry>::const_iterator it;
	for (it = m_defaults.begin(); it != m_defaults.end(); ++it)
		delete it->second.group;
	m_defaults.clear();
}


void Settings::registerChangedCallback(std::string name,
	setting_changed_callback cbf, void *userdata)
{
	std::lock_guard<std::mutex> lock(m_callbackMutex);
	m_callbacks[name].push_back(std::make_pair(cbf, userdata));
}

void Settings::deregisterChangedCallback(std::string name, setting_changed_callback cbf, void *userdata)
{
	std::lock_guard<std::mutex> lock(m_callbackMutex);
	std::map<std::string, std::vector<std::pair<setting_changed_callback, void*> > >::iterator iterToVector = m_callbacks.find(name);
	if (iterToVector != m_callbacks.end())
	{
		std::vector<std::pair<setting_changed_callback, void*> > &vector = iterToVector->second;

		std::vector<std::pair<setting_changed_callback, void*> >::iterator position =
			std::find(vector.begin(), vector.end(), std::make_pair(cbf, userdata));

		if (position != vector.end())
			vector.erase(position);
	}
}

void Settings::doCallbacks(const std::string name)
{
	std::lock_guard<std::mutex> lock(m_callbackMutex);
	std::map<std::string, std::vector<std::pair<setting_changed_callback, void*> > >::iterator iterToVector = m_callbacks.find(name);
	if (iterToVector != m_callbacks.end())
	{
		std::vector<std::pair<setting_changed_callback, void*> >::iterator iter;
		for (auto &it: iterToVector->second) {
			(it.first)(name, it.second);
		}
	}
}
