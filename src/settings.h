/*
Minetest
Copyright (C) 2010-2013 celeron55, Perttu Ahola <celeron55@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SETTINGS_HEADER
#define SETTINGS_HEADER

#include "irrlichttypes_bloated.h"
#include "util/string.h"
#include <mutex>
#include "config.h"
#include <assert.h>
#include <string>
#include <map>
#include <list>
#include <set>
#include "contrib/util/optparse.h"

class Settings;
struct NoiseParams;

// Global objects
extern Settings *g_settings;
extern std::string g_settings_path;

/** function type to register a changed callback */
typedef void (*setting_changed_callback)(const std::string &name, void *data);

enum SettingsParseEvent {
	SPE_NONE,
	SPE_INVALID,
	SPE_COMMENT,
	SPE_KVPAIR,
	SPE_END,
	SPE_GROUP,
	SPE_MULTILINE,
};

struct SettingsEntry {
	SettingsEntry()
	{
		group    = NULL;
		is_group = false;
	}

	SettingsEntry(const std::string &value_)
	{
		value    = value_;
		group    = NULL;
		is_group = false;
	}

	SettingsEntry(Settings *group_)
	{
		group    = group_;
		is_group = true;
	}

	std::string value;
	Settings *group;
	bool is_group;
};

enum BoolSetting {
#ifndef SERVER
	BOOLSETTING_ANISTROPIC_FILTER,
	BOOLSETTING_AUX1_DESCENDS,
	BOOLSETTING_BILINEAR_FILTER,
	BOOLSETTING_CONNECTED_GLASS,
	BOOLSETTING_CONTINOUS_FORWARD,
	BOOLSETTING_DESYNCHRONIZE_MAPBLOCK_TEXTURE_ANIMATION,
	BOOLSETTING_DIRECTIONAL_COLORED_FLAG,
	BOOLSETTING_ENABLE_BUMPMAPPING,
	BOOLSETTING_ENABLE_CLOUDS,
	BOOLSETTING_ENABLE_3D_CLOUDS,
	BOOLSETTING_ENABLE_BUILD_WHERE_YOU_STAND,
	BOOLSETTING_ENABLE_FOG,
	BOOLSETTING_ENABLE_LOCALMAP_SAVING,
	BOOLSETTING_ENABLE_MENU_MUSIC,
	BOOLSETTING_ENABLE_MESHCACHE,
	BOOLSETTING_ENABLE_NODE_HIGHLIGHTING,
	BOOLSETTING_ENABLE_PARALLAX_OCCLUSION,
	BOOLSETTING_ENABLE_PARTICLES,
	BOOLSETTING_ENABLE_REMOTEMEDIA_SERVER,
	BOOLSETTING_ENABLE_SOUND,
	BOOLSETTING_ENABLE_WAVING_LEAVES,
	BOOLSETTING_ENABLE_WAVING_PLANTS,
	BOOLSETTING_ENABLE_WAVING_WATER,
	BOOLSETTING_FULLSCREEN,
	BOOLSETTING_GENERATE_NORMALMAPS,
	BOOLSETTING_GUI_SCALING_FILTER,
	BOOLSETTING_GUI_SCALING_FILTER_TXR2IMG,
	BOOLSETTING_HIGH_PRECISION_FPU,
	BOOLSETTING_INVENTORY_IMAGE_HACK,
	BOOLSETTING_INVERT_MOUSE,
	BOOLSETTING_MENU_CLOUDS,
	BOOLSETTING_MINIMAP_DOUBLE_SCAN_HEIGHT,
	BOOLSETTING_MIP_MAP,
	BOOLSETTING_OPAQUE_WATER,
	BOOLSETTING_RANDOM_INPUT,
	BOOLSETTING_SHOW_DEBUG,
	BOOLSETTING_SMOOTH_LIGHTING,
	BOOLSETTING_TEXTURE_FILTER_TRANSPARENT,
	BOOLSETTING_TOUCHTARGET,
	BOOLSETTING_TRILINEAR_FILTER,
	BOOLSETTING_VIEW_BOBBING,
	BOOLSETTING_VSYNC,
	BOOLSETTING_ENABLE_INVENTORYITEMS_ANIMATIONS,
	BOOLSETTING_TONE_MAPPING,
	BOOLSETTING_SHOW_ENTITY_SELECTIONBOX,
#endif
	BOOLSETTING_ASK_RECONNECT_ON_CRASH,
	BOOLSETTING_CREATIVE_MODE,
	BOOLSETTING_CURL_VERIFY_CERT,
	BOOLSETTING_DISABLE_ANTICHEAT,
	BOOLSETTING_DISALLOW_GUEST_CONNECTION,
	BOOLSETTING_ENABLE_MULTICASE_USERNAMES,
	BOOLSETTING_ENABLE_DAMAGE,
	BOOLSETTING_ENABLE_PVP,
	BOOLSETTING_ENABLE_PVP_PLAYER_CAN_PER_AREA,
	BOOLSETTING_ENABLE_SHADERS,
	BOOLSETTING_ENABLE_IPV6,
	BOOLSETTING_IGNORE_WORLD_LOAD_ERRORS,
	BOOLSETTING_IPV6_SERVER,
	BOOLSETTING_MODSTORE_DISABLE_SPECIAL_HTTP_HEADER,
	BOOLSETTING_SERVER_ANNOUNCE,
	BOOLSETTING_SECURE_ENABLE_SECURITY,
	BOOLSETTING_SERVER_DEDICATED,
	BOOLSETTING_STRICT_PROTOCOL_VERSION,
	BOOLSETTING_UNLIMITED_PLAYER_TRANSFER_DISTANCE,
	BOOLSETTING_CRAFTRESULT_IS_PREVIEW, // Player only
	BOOLSETTING_ENABLE_TIPS,
	BOOLSETTING_ENABLE_CONSOLE,
	BOOLSETTING_ENABLE_INVENTORY_WAYPOINTS,
	BOOLSETTING_ENABLE_MOD_SETHOME,
	BOOLSETTING_ENABLE_MOD_MANA,
	BOOLSETTING_ENABLE_MOD_XP,
	BOOLSETTING_ENABLE_FIRE,
	BOOLSETTING_ENABLE_MOB_SPAWNING_IN_PROTECTED_AREAS,
	BOOLSETTING_ENABLE_AUTOGENERATING_MAP_WHEN_INACTIVE,
	BOOLSETTING_ENABLE_EXPLOSION_PROTECTION_TARGETOWNER,
	BOOLSETTING_ENABLE_MAINTENANCE_MODE,
	BOOLSETTING_ENABLE_RULES_FORM,
	BOOLSETTING_ENABLE_RULES_AUTOPRIVS,
	BOOLSETTING_ENABLE_INTERSERVER_CHAT_CLIENT,
	BOOLSETTING_ENABLE_STATSERVER,

	BOOLSETTING_COUNT,
};

enum U16Setting {
#ifndef SERVER
	U16SETTING_CLOUD_RADIUS,
	U16SETTING_CROSSHAIR_ALPHA,
	U16SETTING_FSAA,
	U16SETTING_FULLSCREEN_BPP,
	U16SETTING_SCREEN_HEIGHT,
	U16SETTING_SCREEN_WIDTH,
	U16SETTING_SERVERMAP_SAVE_INTERVAL,
	U16SETTING_TOUCHSCREEN_THRESHOLD,
	U16SETTING_3D_MODE,
#endif
	U16SETTING_DEBUG_LOG_LEVEL,
	U16SETTING_FALLBACK_FONT_SIZE,
	U16SETTING_FONT_SIZE,
	U16SETTING_MONO_FONT_SIZE,
	U16SETTING_EMERGEQUEUE_LIMIT_DISKONLY,
	U16SETTING_EMERGEQUEUE_LIMIT_GENERATE,
	U16SETTING_EMERGEQUEUE_LIMIT_TOTAL,
	U16SETTING_LIQUID_QUEUE_PURGE_TIME,
	U16SETTING_MAX_CHATMESSAGE_LENGTH,
	U16SETTING_MAP_GENERATION_LIMIT,
	U16SETTING_MAX_OBJECTS_PER_BLOCK,
	U16SETTING_MAX_PACKETS_PER_ITERATION,
	U16SETTING_MAX_SIMULATENOUS_BLOCK_SENDS_PER_CLIENT,
	U16SETTING_MAX_USERS,
	U16SETTING_MIN_PLAYERNAME_SIZE,
	U16SETTING_NUM_EMERGE_THREADS,
	U16SETTING_PORT,
	U16SETTING_EPIXEL_PORT,
	U16SETTING_REDIS_PORT,
	U16SETTING_SQLITE_SYNCHRONOUS,
	U16SETTING_WORKAROUND_WINDOW_SIZE,
	U16SETTING_MINTIME_BETWEEN_TIPS,
	U16SETTING_MANA_MAX,
	U16SETTING_MANA_DEFAULT_REGEN,
	U16SETTING_MANA_COST_HOME,
	U16SETTING_SAVE_META_INTERVAL,
	U16SETTING_MAPDB_POOL_SIZE,
	U16SETTING_GAMEDB_POOL_SIZE,
	U16SETTING_MG_VALLEYS_ALTITUDE_CHILL,
	U16SETTING_MG_VALLEYS_CAVE_WATER_MAX_HEIGHT,
	U16SETTING_MG_VALLEYS_HUMIDITY,
	U16SETTING_MG_VALLEYS_HUMIDITY_BREAK_POINT,
	U16SETTING_MG_VALLEYS_LAVA_MAX_HEIGHT,
	U16SETTING_MG_VALLEYS_RIVER_DEPTH,
	U16SETTING_MG_VALLEYS_RIVER_SIZE,
	U16SETTING_MG_VALLEYS_TEMPERATURE,
	U16SETTING_MG_VALLEYS_WATER_FEATURE,
	U16SETTING_INTERSERVER_CHAT_PORT,
	U16SETTING_INTERSERVER_CHAT_TIMEOUT,
	U16SETTING_MESSAGE_RATE_KICK,
	U16SETTING_MG_VERSION,

	U16SETTING_COUNT,
};

enum S16Setting {
#ifndef SERVER
	S16SETTING_CLOUD_HEIGHT,
	S16SETTING_SELECTIONBOX_WIDTH,
	S16SETTING_VIEWING_RANGE,
#endif
	S16SETTING_ACTIVE_BLOCK_RANGE,
	S16SETTING_ACTIVEOBJECT_SEND_RANGE_BLOCKS,
	S16SETTING_CHUNK_SIZE,
	S16SETTING_CURL_PARALLEL_LIMIT,
	S16SETTING_MAX_BLOCK_GENERATE_DISTANCE,
	S16SETTING_MAX_BLOCK_SEND_DISTANCE,
	S16SETTING_PLAYER_TRANSFER_DISTANCE,
	S16SETTING_VERTICAL_SPAWN_RANGE,
	S16SETTING_WATER_LEVEL,

	S16SETTING_COUNT,
};

enum FloatSetting {
#ifndef SERVER
	FSETTING_CAMERA_SMOOTHING,
	FSETTING_CINEMATIC_CAMERA_SMOOTHING,
	FSETTING_CLIENT_UNLOAD_DATA_TIMEOUT,
	FSETTING_DISPLAY_GAMMA,
	FSETTING_FALL_BOBBING_AMOUNT,
	FSETTING_FOV,
	FSETTING_FPS_MAX,
	FSETTING_GUI_SCALING,
	FSETTING_HUD_HOTBAR_MAX_WIDTH,
	FSETTING_HUD_SCALING,
	FSETTING_MOUSE_SENSITIVITY,
	FSETTING_NORMALMAPS_SMOOTH,
	FSETTING_NORMALMAPS_STRENGTH,
	FSETTING_PARALLAX_OCCLUSION_BIAS,
	FSETTING_PARALLAX_OCCLUSION_ITERATIONS,
	FSETTING_PARALLAX_OCCLUSION_MODE,
	FSETTING_PARALLAX_OCCLUSION_SCALE,
	FSETTING_PAUSE_FPS_MAX,
	FSETTING_REPEAT_RIGHTCLICK_TIME,
	FSETTING_SCREEN_DPI,
	FSETTING_SOUND_VOLUME,
	FSETTING_VIEW_BOBBING_AMOUNT,
	FSETTING_WANTED_FPS,
	FSETTING_WATER_WAVE_HEIGHT,
	FSETTING_WATER_WAVE_LENGTH,
	FSETTING_WATER_WAVE_SPEED,
	FSETTING_3D_PARALAX_STRENGTH,
#endif
	FSETTING_AMBIENT_OCCLUSION_GAMMA,
	FSETTING_DEDICATED_SERVER_STEP,
	FSETTING_FULL_BLOCK_SEND_ENABLE_MIN_TIME_FROM_BUILDING,
	FSETTING_LIQUID_UPDATE,
	FSETTING_MOVEMENT_ACCELERATION_DEFAULT,
	FSETTING_MOVEMENT_ACCELERATION_AIR,
	FSETTING_MOVEMENT_ACCELERATION_FAST,
	FSETTING_MOVEMENT_GRAVITY,
	FSETTING_MOVEMENT_LIQUID_FLUIDITY,
	FSETTING_MOVEMENT_LIQUID_FLUIDITY_SMOOTH,
	FSETTING_MOVEMENT_LIQUID_SINK,
	FSETTING_MOVEMENT_SPEED_CLIMB,
	FSETTING_MOVEMENT_SPEED_CROUCH,
	FSETTING_MOVEMENT_SPEED_FAST,
	FSETTING_MOVEMENT_SPEED_JUMP,
	FSETTING_MOVEMENT_SPEED_WALK,
	FSETTING_PLAYER_HUNGER_TICK,
	FSETTING_PLAYER_EXHAUS_ONDIG,
	FSETTING_PLAYER_EXHAUS_ONMOVE,
	FSETTING_PLAYER_EXHAUS_ONPLACE,
	FSETTING_SERVER_MAP_SAVE_INTERVAL,
	FSETTING_SERVER_UNLOAD_UNUSED_DATA_TIMEOUT,
	FSETTING_TIME_SPEED,
	FSETTING_TIME_SEND_INTERVAL,
	FSETTING_PLAYER_HOME_INTERVAL,
	FSETTING_MANA_REGEN_TIMER,
	FSETTING_MOD_XP_LOSS_DEAD,
	FSETTING_MOD_XP_WIN_PVP,
	FSETTING_RULES_ACCEPTANCE_TIMEOUT,
	FSETTING_MAX_MESSAGES_PER_10SEC,
	FSETTING_ACTIVE_BLOCK_MGMT_INTERVAL,
	FSETTING_NODETIMER_INTERVAL,
	FSETTING_ABM_INTERVAL,
	FSETTING_DROWNING_INTERVAL,
	FSETTING_BREATH_INTERVAL,

	FSETTING_COUNT,
};

enum S32Setting
{
#ifndef SERVER
	S32SETTING_CLIENT_MAPBLOCK_LIMIT,
	S32SETTING_CONSOLE_ALPHA,
	S32SETTING_TEXTURE_MIN_SIZE,
	S32SETTING_TOOLTIP_SHOW_DELAY,
#endif
	S32SETTING_CURL_TIMEOUT,
	S32SETTING_LIQUID_LOOP_MAX, // @TODO refactor u32
	S32SETTING_MAX_CLEAROBJECTS_EXTRA_LOADED_BLOCKS,
	S32SETTING_MAX_SIMULTANEOUS_BLOCK_SENDS_SERVER_TOTAL,
	S32SETTING_COUNT,
};

// @TODO, overload with WorldSettings & PlayerSettings
class Settings {
public:
	Settings();
	~Settings();

	Settings & operator += (const Settings &other);
	Settings & operator = (const Settings &other);

	/***********************
	 * Reading and writing *
	 ***********************/

	// Read configuration file.  Returns success.
	bool readConfigFile(const char *filename);
	//Updates configuration file.  Returns success.
	bool updateConfigFile(const char *filename, bool mainconfig = false);
	// NOTE: Types of allowed_options are ignored.  Returns success.
	bool parseCommandLine(int argc, char **argv,
						  const struct optparse_long *longopts);
	bool parseConfigLines(std::istream &is, const std::string &end = "");
	void writeLines(std::ostream &os, u32 tab_depth=0) const;

	SettingsParseEvent parseConfigObject(const std::string &line,
		const std::string &end, std::string &name, std::string &value);
	bool updateConfigObject(std::istream &is, std::ostream &os,
		const std::string &end, u32 tab_depth=0, bool mainconfig = false);

	static bool checkNameValid(const std::string &name);
	static bool checkValueValid(const std::string &value);

	static std::string getMultiline(std::istream &is, size_t *num_lines=NULL);
	static void printEntry(std::ostream &os, const std::string &name,
		const SettingsEntry &entry, u32 tab_depth=0);

	/***********
	 * Getters *
	 ***********/

	const SettingsEntry &getEntry(const std::string &name) const;
	Settings *getGroup(const std::string &name) const;
	std::string get(const std::string &name) const;
	inline bool get(const BoolSetting index) const
	{
		assert(index < BOOLSETTING_COUNT);
		return m_bool_settings[index];
	}

	inline u16 get(const U16Setting index) const
	{
		assert(index < U16SETTING_COUNT);
		return m_u16_settings[index];
	}

	inline s16 get(const S16Setting index) const
	{
		assert(index < S16SETTING_COUNT);
		return m_s16_settings[index];
	}

	inline s32 get(const S32Setting index) const
	{
		assert(index < S32SETTING_COUNT);
		return m_s32_settings[index];
	}

	inline float get(const FloatSetting index) const
	{
		assert(index < FSETTING_COUNT);
		return m_float_settings[index];
	}

	bool getBool(const std::string &name) const;
	s32 getS32(const std::string &name) const;
	u64 getU64(const std::string &name) const;
	float getFloat(const std::string &name) const;
	v3f getV3F(const std::string &name) const;
	u32 getFlagStr(const std::string &name, const FlagDesc *flagdesc,
			u32 *flagmask) const;
	bool getNoiseParams(const std::string &name, NoiseParams &np) const;
	bool getNoiseParamsFromValue(const std::string &name, NoiseParams &np) const;
	bool getNoiseParamsFromGroup(const std::string &name, NoiseParams &np) const;

	// return all keys used
	std::vector<std::string> getNames() const;
	bool exists(const std::string &name) const;
	bool boolExists(const std::string &name) const;


	/***************************************
	 * Getters that don't throw exceptions *
	 ***************************************/

	bool getGroupNoEx(const std::string &name, Settings *&val) const;
	bool getNoEx(const std::string &name, std::string &val) const;
	bool getFlag(const std::string &name) const;
	bool getU16NoEx(const std::string &name, u16 &val) const;
	bool getS32NoEx(const std::string &name, s32 &val) const;
	bool getFloatNoEx(const std::string &name, float &val) const;
	bool getV3FNoEx(const std::string &name, v3f &val) const;
	// N.B. getFlagStrNoEx() does not set val, but merely modifies it.  Thus,
	// val must be initialized before using getFlagStrNoEx().  The intention of
	// this is to simplify modifying a flags field from a default value.
	bool getFlagStrNoEx(const std::string &name, u32 &val, FlagDesc *flagdesc) const;


	/***********
	 * Setters *
	 ***********/

	// N.B. Groups not allocated with new must be set to NULL in the settings
	// tree before object destruction.
	bool setEntry(const std::string &name, const void *entry,
		bool set_group, bool set_default);
	bool set(const std::string &name, const std::string &value);
	bool setDefault(const std::string &name, const std::string &value);
	void set(const BoolSetting b, bool value);
	void set(const U16Setting u, u16 value);
	void set(const S16Setting u, s16 value);
	void set(const FloatSetting u, float value);
	void set(const S32Setting u, s32 value);
	bool setBool(const std::string &name, bool value);
	bool setU16(const std::string &name, u16 value);
	bool setS32(const std::string &name, s32 value);
	bool setU64(const std::string &name, u64 value);
	bool setFloat(const std::string &name, float value);
	bool setV3F(const std::string &name, v3f value);
	bool setFlagStr(const std::string &name, u32 flags,
		const FlagDesc *flagdesc, u32 flagmask);
	bool setNoiseParams(const std::string &name, const NoiseParams &np,
		bool set_default=false);

	void clear();
	void clearDefaults();
	void update(const Settings &other);
	void registerChangedCallback(std::string name, setting_changed_callback cbf, void *userdata = NULL);
	void deregisterChangedCallback(std::string name, setting_changed_callback cbf, void *userdata = NULL);

#ifndef SERVER
	inline void updateScreenSizeIfNeeded(core::dimension2d<u32> irr_screensize)
	{
		if (irr_screensize != m_current_screensize) {
			m_current_screensize = irr_screensize;
			set(U16SETTING_SCREEN_WIDTH, m_current_screensize.Width);
			set(U16SETTING_SCREEN_HEIGHT, m_current_screensize.Height);
		}
	}
#endif
private:

	void updateNoLock(const Settings &other);
	void clearNoLock();
	void clearDefaultsNoLock();

	void doCallbacks(std::string name);

	std::map<std::string, SettingsEntry> m_settings;
	std::map<std::string, SettingsEntry> m_defaults;

	std::map<std::string, std::vector<std::pair<setting_changed_callback,void*> > > m_callbacks;

#ifndef SERVER
	core::dimension2d<u32> m_current_screensize;
#endif

	void initSettings();
	void initSetting(BoolSetting b, const std::string &key, bool value);
	void initSetting(U16Setting u, const std::string &key, u16 value);
	void initSetting(S16Setting u, const std::string &key, s16 value);
	void initSetting(FloatSetting u, const std::string &key, float value);
	void initSetting(S32Setting u, const std::string &key, s32 value);

	bool m_bool_settings[BOOLSETTING_COUNT];
	u16 m_u16_settings[U16SETTING_COUNT];
	s16 m_s16_settings[S16SETTING_COUNT];
	s32 m_s32_settings[S32SETTING_COUNT];
	float m_float_settings[FSETTING_COUNT];
	// Mappers
	std::map<std::string, BoolSetting> m_bool_mapper;
	std::map<std::string, U16Setting> m_u16_mapper;
	std::map<std::string, S16Setting> m_s16_mapper;
	std::map<std::string, S32Setting> m_s32_mapper;
	std::map<std::string, FloatSetting> m_float_mapper;
	// Reverse mappers
	std::map<BoolSetting, std::string> m_bool_rmapper;
	std::map<U16Setting, std::string> m_u16_rmapper;
	std::map<S16Setting, std::string> m_s16_rmapper;
	std::map<S32Setting, std::string> m_s32_rmapper;
	std::map<FloatSetting, std::string> m_float_rmapper;

	mutable std::mutex m_callbackMutex;
	mutable std::mutex m_mutex; // All methods that access m_settings/m_defaults directly should lock this.

};

#endif

