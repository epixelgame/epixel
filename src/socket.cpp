/*
Minetest
Copyright (C) 2013 celeron55, Perttu Ahola <celeron55@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "socket.h"

#include <iostream>
#include <sstream>
#include <iomanip>
#include "util/string.h"
#include "util/numeric.h"
#include "settings.h"
#include "log.h"

#ifdef _WIN32
	typedef int socklen_t;
#else
	#include <unistd.h>
	#include <arpa/inet.h>
#endif

/*
	Address
*/

Address::Address()
{
	m_addr_family = 0;
	memset(&m_address, 0, sizeof(m_address));
	m_port = 0;
}

Address::Address(u32 address, u16 port)
{
	memset(&m_address, 0, sizeof(m_address));
	setAddress(address);
	setPort(port);
}

Address::Address(u8 a, u8 b, u8 c, u8 d, u16 port)
{
	memset(&m_address, 0, sizeof(m_address));
	setAddress(a, b, c, d);
	setPort(port);
}

// Equality (address family, address and port must be equal)
bool Address::operator==(const Address &address)
{
	if(address.m_addr_family != m_addr_family || address.m_port != m_port)
		return false;
	else if(m_addr_family == AF_INET)
	{
		return m_address.ipv4.sin_addr.s_addr ==
			   address.m_address.ipv4.sin_addr.s_addr;
	}
	else
		return false;
}

bool Address::operator!=(const Address &address)
{
	return !(*this == address);
}

void Address::Resolve(const std::string &name)
{
	if (name.empty()) {
		if (m_addr_family == AF_INET) {
			setAddress((u32) 0);
		}
		return;
	}

	struct sockaddr_in chost;
	if (epixel::Socket::resolve(name, &(chost.sin_addr))) {
		m_addr_family = AF_INET;
		m_address.ipv4 = chost;
	}
	else {
		throw ResolveError("Resolution error");
	}
}

// IP address -> textual representation
const std::string Address::serializeString() const
{
// windows XP doesnt have inet_ntop, maybe use better func
#ifdef _WIN32
	if(m_addr_family == AF_INET)
	{
		u8 a, b, c, d;
		u32 addr;
		addr = ntohl(m_address.ipv4.sin_addr.s_addr);
		a = (addr & 0xFF000000) >> 24;
		b = (addr & 0x00FF0000) >> 16;
		c = (addr & 0x0000FF00) >> 8;
		d = (addr & 0x000000FF);
		return itos(a) + "." + itos(b) + "." + itos(c) + "." + itos(d);
	}
	else
		return std::string("");
#else
	if (m_addr_family != AF_INET) {
		return "";
	}
	char str[INET6_ADDRSTRLEN];
	if (inet_ntop(m_addr_family, (void*)&(m_address.ipv4.sin_addr), str, INET6_ADDRSTRLEN) == NULL) {
		return std::string("");
	}
	return std::string(str);
#endif
}

u16 Address::getPort() const
{
	return m_port;
}

int Address::getFamily() const
{
	return m_addr_family;
}

bool Address::isIPv6() const
{
	return false;
}

bool Address::isZero() const
{
	if (m_addr_family == AF_INET) {
		return m_address.ipv4.sin_addr.s_addr == 0;
	}
	return false;
}

void Address::setAddress(u32 address)
{
	m_addr_family = AF_INET;
	m_address.ipv4.sin_family = AF_INET;
	m_address.ipv4.sin_addr.s_addr = htonl(address);
}

void Address::setAddress(u8 a, u8 b, u8 c, u8 d)
{
	m_addr_family = AF_INET;
	m_address.ipv4.sin_family = AF_INET;
	u32 addr = htonl((a << 24) | (b << 16) | (c << 8) | d);
	m_address.ipv4.sin_addr.s_addr = addr;
}

void Address::setPort(u16 port)
{
	m_port = port;
}

/*
	UDPSocket
*/

void UDPSocket::Bind(const Address &addr) {
	struct sockaddr_in address;
	memset(&address, 0, sizeof(address));

	address = addr.getAddress();
	address.sin_family = AF_INET;
	address.sin_port = htons(addr.getPort());

	if (bind(m_sock, (const struct sockaddr *) &address,
			 sizeof(struct sockaddr_in)) < 0) {
		throw SocketException("Failed to bind socket");
	}

	setNonBlocking();
	setSocketTimeout(0);

	static const int reuse_ok = 1;
#ifdef WIN32
	if (setsockopt(m_sock, SOL_SOCKET, SO_REUSEADDR, (char*)&reuse_ok,
#else
	if (setsockopt(m_sock, SOL_SOCKET, SO_REUSEADDR, &reuse_ok,
#endif
				   sizeof(int)) < 0) {

		throw ("setsockopt failed for SO_REUSEADDR");
	}
}

void UDPSocket::Send(const Address &destination, const void *data, size_t size)
{
	setNonBlocking();

	setSocketTimeout(0);

	struct sockaddr_in address = destination.getAddress();
	address.sin_port = htons(destination.getPort());
	size_t sent = (size_t)sendto(m_sock, (const char *)data, size, 0, (struct sockaddr *)&address, sizeof(struct sockaddr_in));

	if (sent != size)
		throw SendFailedException("Failed to send packet: wrong size");
}

int UDPSocket::Receive(Address &sender, void *data, size_t size)
{
	setNonBlocking();
	setSocketTimeout(5);

	fd_set readset;
	int result;
	FD_ZERO(&readset);
	FD_SET(m_sock, &readset);

	// Initialize time out struct
	struct timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = 3 * 1000;

	// select()
	result = select(m_sock+1, &readset, NULL, NULL, &tv);
	if (result <= 0 || !FD_ISSET(m_sock, &readset))
		return 0;

	struct sockaddr_in address;
	memset(&address, 0, sizeof(address));
	socklen_t address_len = sizeof(address);

	int received = recvfrom(m_sock, (char *)data, size, 0, (struct sockaddr *)&address, &address_len);
	if (received <= 0)
		return 0;

	u32 address_ip = ntohl(address.sin_addr.s_addr);
	u16 address_port = ntohs(address.sin_port);
	sender = Address(address_ip, address_port);

	return received;
}
