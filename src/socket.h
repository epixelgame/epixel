/*
Minetest
Copyright (C) 2013 celeron55, Perttu Ahola <celeron55@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef __MTSOCKET_H__
#define __MTSOCKET_H__

#include <ostream>
#include <string.h>
#include "contrib/util/socket.h"
#include "irrlichttypes.h"
#include "exceptions.h"

class ResolveError : public BaseException
{
public:
	ResolveError(const std::string &s):
		BaseException(s)
	{
	}
};

class SendFailedException : public BaseException
{
public:
	SendFailedException(const std::string &s):
		BaseException(s)
	{
	}
};

struct IPv6AddressBytes
{
public:
	u8 bytes[16] = {0};
};

class Address
{
public:
	Address();
	Address(u32 address, u16 port);
	Address(u8 a, u8 b, u8 c, u8 d, u16 port);
	bool operator==(const Address &address);
	bool operator!=(const Address &address);
	// Resolve() may throw ResolveError (address is unchanged in this case)
	void Resolve(const std::string &name);
	unsigned short getPort() const;
	void setAddress(u32 address);
	void setAddress(u8 a, u8 b, u8 c, u8 d);
	int getFamily() const;
	bool isIPv6() const;
	bool isZero() const;
	void setPort(unsigned short port);
	const std::string serializeString() const;
	struct sockaddr_in getAddress() const { return m_address.ipv4; }
private:
	unsigned int m_addr_family;
	union
	{
		struct sockaddr_in ipv4;
	} m_address;
	u16 m_port; // Port is separate from sockaddr structures
};

class UDPSocket: public epixel::Socket
{
public:
	UDPSocket(): Socket(IPPROTO_UDP) {};
	~UDPSocket() {};
	void Bind(const Address &addr);

	void Send(const Address &destination, const void *data, size_t size);
	// Returns -1 if there is no data
	int Receive(Address &sender, void *data, size_t size);
};

#endif