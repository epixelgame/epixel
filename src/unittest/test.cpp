/*
Minetest
Copyright (C) 2013 celeron55, Perttu Ahola <celeron55@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "nodedef.h"
#include "itemdef.h"
#include "gamedef.h"
#include "porting.h"
#include "filesys.h"
#include "test.h"

content_t t_CONTENT_STONE;
content_t t_CONTENT_GRASS;
content_t t_CONTENT_TORCH;
content_t t_CONTENT_WATER;
content_t t_CONTENT_LAVA;
content_t t_CONTENT_BRICK;

////////////////////////////////////////////////////////////////////////////////

////
//// TestGameDef
////

class TestGameDef : public IGameDef {
public:
	TestGameDef();
	~TestGameDef();

	IItemDefManager *getItemDefManager() { return m_itemdef; }
	INodeDefManager *getNodeDefManager() { return m_nodedef; }
	ICraftDefManager *getCraftDefManager() { return m_craftdef; }
	ITextureSource *getTextureSource() { return m_texturesrc; }
	IShaderSource *getShaderSource() { return m_shadersrc; }
	ISoundManager *getSoundManager() { return m_soundmgr; }
	MtEventManager *getEventManager() { return m_eventmgr; }
	scene::ISceneManager *getSceneManager() { return m_scenemgr; }
	IRollbackManager *getRollbackManager() { return m_rollbackmgr; }
	EmergeManager *getEmergeManager() { return m_emergemgr; }

	scene::IAnimatedMesh *getMesh(const std::string &filename) { return NULL; }
	bool checkLocalPrivilege(const std::string &priv) { return false; }
	u16 allocateUnknownNodeId(const std::string &name) { return 0; }

	void defineSomeNodes();

private:
	IItemDefManager *m_itemdef;
	INodeDefManager *m_nodedef;
	ICraftDefManager *m_craftdef;
	ITextureSource *m_texturesrc;
	IShaderSource *m_shadersrc;
	ISoundManager *m_soundmgr;
	MtEventManager *m_eventmgr;
	scene::ISceneManager *m_scenemgr;
	IRollbackManager *m_rollbackmgr;
	EmergeManager *m_emergemgr;
};


TestGameDef::TestGameDef()
{
	m_itemdef = createItemDefManager();
	m_nodedef = createNodeDefManager();

	defineSomeNodes();
}


TestGameDef::~TestGameDef()
{
	delete m_itemdef;
	delete m_nodedef;
}


void TestGameDef::defineSomeNodes()
{
	IWritableItemDefManager *idef = (IWritableItemDefManager *)m_itemdef;
	IWritableNodeDefManager *ndef = (IWritableNodeDefManager *)m_nodedef;

	ItemDefinition itemdef;
	ContentFeatures f;

	//// Stone
	itemdef = ItemDefinition();
	itemdef.type = ITEM_NODE;
	itemdef.name = "default:stone";
	itemdef.description = "Stone";
	itemdef.groups["cracky"] = 3;
	itemdef.inventory_image = "[inventorycube"
		"{default_stone.png"
		"{default_stone.png"
		"{default_stone.png";
	f = ContentFeatures();
	f.name = itemdef.name;
	for(int i = 0; i < 6; i++)
		f.tiledef[i].name = "default_stone.png";
	f.is_ground_content = true;
	idef->registerItem(itemdef);
	t_CONTENT_STONE = ndef->set(f.name, f);

	//// Grass
	itemdef = ItemDefinition();
	itemdef.type = ITEM_NODE;
	itemdef.name = "default:dirt_with_grass";
	itemdef.description = "Dirt with grass";
	itemdef.groups["crumbly"] = 3;
	itemdef.inventory_image = "[inventorycube"
		"{default_grass.png"
		"{default_dirt.png&default_grass_side.png"
		"{default_dirt.png&default_grass_side.png";
	f = ContentFeatures();
	f.name = itemdef.name;
	f.tiledef[0].name = "default_grass.png";
	f.tiledef[1].name = "default_dirt.png";
	for(int i = 2; i < 6; i++)
		f.tiledef[i].name = "default_dirt.png^default_grass_side.png";
	f.is_ground_content = true;
	idef->registerItem(itemdef);
	t_CONTENT_GRASS = ndef->set(f.name, f);

	//// Torch (minimal definition for lighting tests)
	itemdef = ItemDefinition();
	itemdef.type = ITEM_NODE;
	itemdef.name = "default:torch";
	f = ContentFeatures();
	f.name = itemdef.name;
	f.param_type = CPT_LIGHT;
	f.light_propagates = true;
	f.sunlight_propagates = true;
	f.light_source = LIGHT_MAX-1;
	idef->registerItem(itemdef);
	t_CONTENT_TORCH = ndef->set(f.name, f);

	//// Water
	itemdef = ItemDefinition();
	itemdef.type = ITEM_NODE;
	itemdef.name = "default:water";
	itemdef.description = "Water";
	itemdef.inventory_image = "[inventorycube"
		"{default_water.png"
		"{default_water.png"
		"{default_water.png";
	f = ContentFeatures();
	f.name = itemdef.name;
	f.alpha = 128;
	f.liquid_type = LIQUID_SOURCE;
	f.liquid_viscosity = 4;
	f.is_ground_content = true;
	f.groups["liquids"] = 3;
	for(int i = 0; i < 6; i++)
		f.tiledef[i].name = "default_water.png";
	idef->registerItem(itemdef);
	t_CONTENT_WATER = ndef->set(f.name, f);

	//// Lava
	itemdef = ItemDefinition();
	itemdef.type = ITEM_NODE;
	itemdef.name = "default:lava";
	itemdef.description = "Lava";
	itemdef.inventory_image = "[inventorycube"
		"{default_lava.png"
		"{default_lava.png"
		"{default_lava.png";
	f = ContentFeatures();
	f.name = itemdef.name;
	f.alpha = 128;
	f.liquid_type = LIQUID_SOURCE;
	f.liquid_viscosity = 7;
	f.light_source = LIGHT_MAX-1;
	f.is_ground_content = true;
	f.groups["liquids"] = 3;
	for(int i = 0; i < 6; i++)
		f.tiledef[i].name = "default_lava.png";
	idef->registerItem(itemdef);
	t_CONTENT_LAVA = ndef->set(f.name, f);


	//// Brick
	itemdef = ItemDefinition();
	itemdef.type = ITEM_NODE;
	itemdef.name = "default:brick";
	itemdef.description = "Brick";
	itemdef.groups["cracky"] = 3;
	itemdef.inventory_image = "[inventorycube"
		"{default_brick.png"
		"{default_brick.png"
		"{default_brick.png";
	f = ContentFeatures();
	f.name = itemdef.name;
	for(int i = 0; i < 6; i++)
		f.tiledef[i].name = "default_brick.png";
	f.is_ground_content = true;
	idef->registerItem(itemdef);
	t_CONTENT_BRICK = ndef->set(f.name, f);
}

////
//// run_tests
////

bool run_tests()
{
	u32 t1 = porting::getTime(PRECISION_MILLI);
	TestGameDef gamedef;

	u32 num_modules_failed     = 0;
	u32 num_total_tests_failed = 0;
	u32 num_total_tests_run    = 0;
	std::vector<TestBase *> &testmods = TestManager::getTestModules();
	for (size_t i = 0; i != testmods.size(); i++) {
		if (!testmods[i]->testModule(&gamedef))
			num_modules_failed++;

		num_total_tests_failed += testmods[i]->num_tests_failed;
		num_total_tests_run += testmods[i]->num_tests_run;
	}

	u32 tdiff = porting::getTime(PRECISION_MILLI) - t1;

	const char *overall_status = (num_modules_failed == 0) ? "PASSED" : "FAILED";

	logger.debugStream()
		<< "++++++++++++++++++++++++++++++++++++++++"
		<< "++++++++++++++++++++++++++++++++++++++++\n"
		<< "Unit Test Results: " << overall_status << "\n"
		<< "    " << num_modules_failed << " / " << testmods.size()
		<< " failed modules (" << num_total_tests_failed << " / "
		<< num_total_tests_run << " failed individual tests).\n"
		<< "    Testing took " << tdiff << "ms total.\n"
		<< "++++++++++++++++++++++++++++++++++++++++"
		<< "++++++++++++++++++++++++++++++++++++++++\n";

	return num_modules_failed;
}

////
//// TestBase
////

bool TestBase::testModule(IGameDef *gamedef)
{
	logger.debug("======== Testing module %s", getName());
	u32 t1 = porting::getTime(PRECISION_MILLI);


	runTests(gamedef);

	u32 tdiff = porting::getTime(PRECISION_MILLI) - t1;
	logger.debugStream() << "======== Module " << getName() << " "
		<< (num_tests_failed ? "failed" : "passed") << " (" << num_tests_failed
		<< " failures / " << num_tests_run << " tests) - " << tdiff
		<< "ms\n";

	if (!m_test_dir.empty())
		fs::RecursiveDelete(m_test_dir);

	return num_tests_failed == 0;
}

std::string TestBase::getTestTempDirectory()
{
	if (!m_test_dir.empty())
		return m_test_dir;

	char buf[32];
	snprintf(buf, sizeof(buf), "%08X", myrand());

	m_test_dir = fs::TempPath() + DIR_DELIM "mttest_" + buf;
	if (!fs::CreateDir(m_test_dir))
		throw TestFailedException();

	return m_test_dir;
}

std::string TestBase::getTestTempFile()
{
	char buf[32];
	snprintf(buf, sizeof(buf), "%08X", myrand());

	return getTestTempDirectory() + DIR_DELIM + buf + ".tmp";
}
