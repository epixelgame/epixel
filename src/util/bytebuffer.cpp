/*
Minetest
Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "bytebuffer.h"
#include "exceptions.h"
#include "util/serialize.h"
#include <sstream>

void ByteBuffer::checkReadOffset(u32 from_offset)
{
	if (from_offset >= m_datasize) {
		std::stringstream ss;
		ss << "Reading outside packet (offset: " <<
				from_offset << ", packet size: " << getSize() << ")";
		throw PacketError(ss.str());
	}
}

u8* ByteBuffer::getU8Ptr(u32 from_offset)
{
	if (m_datasize == 0) {
		return NULL;
	}

	checkReadOffset(from_offset);

	return (u8*)&m_data[from_offset];
}

ByteBuffer& ByteBuffer::operator>>(std::string& dst)
{
	u16 strLen = readU16(&m_data[m_read_offset]);
	m_read_offset += sizeof(u16);

	dst.clear();

	if (strLen == 0) {
		return *this;
	}

	dst.reserve(strLen);
	dst.append((char*)&m_data[m_read_offset], strLen);

	m_read_offset += strLen;
	return *this;
}

ByteBuffer& ByteBuffer::operator<<(std::string src)
{
	u16 msgsize = src.size();
	if (msgsize > 0xFFFF) {
		msgsize = 0xFFFF;
	}

	*this << msgsize;

	putRawString(src.c_str(), (u32) msgsize);
	return *this;
}

void ByteBuffer::putLongString(const std::string &src)
{
	u32 msgsize = src.size();
	if (msgsize > 0xFFFFFFFF) {
		msgsize = 0xFFFFFFFF;
	}

	*this << msgsize;
	putRawString(src.c_str(), msgsize);
}

ByteBuffer& ByteBuffer::operator<<(bool src)
{
	checkDataSize<u8>();

	writeU8(&m_data[m_read_offset], src);

	incrOffset<u8>();
	return *this;
}

ByteBuffer& ByteBuffer::operator>>(bool& dst)
{
	checkReadOffset(m_read_offset);

	dst = readU8(&m_data[m_read_offset]);

	incrOffset<u8>();
	return *this;
}

ByteBuffer& ByteBuffer::operator<<(u32 src)
{
	checkDataSize<u32>();

	writeU32(&m_data[m_read_offset], src);

	incrOffset<u32>();
	return *this;
}

ByteBuffer& ByteBuffer::operator>>(u32& dst)
{
	checkReadOffset(m_read_offset);

	dst = readU32(&m_data[m_read_offset]);

	incrOffset<u32>();
	return *this;
}

ByteBuffer& ByteBuffer::operator<<(s32 src)
{
	*this << (u32) src;
	return *this;
}

ByteBuffer& ByteBuffer::operator>>(s32& dst)
{
	checkReadOffset(m_read_offset);

	dst = readS32(&m_data[m_read_offset]);

	incrOffset<s32>();
	return *this;
}

ByteBuffer& ByteBuffer::operator<<(v2s32 src)
{
	*this << (s32) src.X;
	*this << (s32) src.Y;
	return *this;
}

ByteBuffer& ByteBuffer::operator>>(v2s32& dst)
{
	dst = readV2S32(&m_data[m_read_offset]);

	incrOffset<v2s32>();
	return *this;
}

ByteBuffer& ByteBuffer::operator<<(v3s32 src)
{
	*this << (s32) src.X;
	*this << (s32) src.Y;
	*this << (s32) src.Z;
	return *this;
}

ByteBuffer& ByteBuffer::operator>>(v3s32& dst)
{
	checkReadOffset(m_read_offset);

	dst = readV3S32(&m_data[m_read_offset]);

	incrOffset<v3s32>();
	return *this;
}

ByteBuffer& ByteBuffer::operator<<(v2f src)
{
	*this << (float) src.X;
	*this << (float) src.Y;
	return *this;
}

ByteBuffer& ByteBuffer::operator>>(v2f& dst)
{
	checkReadOffset(m_read_offset);

	dst = readV2F1000(&m_data[m_read_offset]);

	incrOffset<v2f>();
	return *this;
}

ByteBuffer& ByteBuffer::operator>>(v3f& dst)
{
	checkReadOffset(m_read_offset);

	dst = readV3F1000(&m_data[m_read_offset]);

	incrOffset<v3f>();
	return *this;
}

ByteBuffer& ByteBuffer::operator<<(v3f src)
{
	*this << (float) src.X;
	*this << (float) src.Y;
	*this << (float) src.Z;
	return *this;
}

ByteBuffer& ByteBuffer::operator>>(float& dst)
{
	checkReadOffset(m_read_offset);

	dst = readF1000(&m_data[m_read_offset]);

	incrOffset<float>();
	return *this;
}

ByteBuffer& ByteBuffer::operator<<(float src)
{
	checkDataSize<float>();

	writeF1000(&m_data[m_read_offset], src);

	incrOffset<float>();
	return *this;
}
char* ByteBuffer::getString(u32 from_offset)
{
	checkReadOffset(from_offset);

	return (char*)&m_data[from_offset];
}

void ByteBuffer::putRawString(const char* src, u32 len)
{
	if (len == 0)
		return;

	if (m_read_offset + len >= m_datasize) {
		m_datasize = m_read_offset + len;
		m_data.resize(m_datasize);
	}

	memcpy(&m_data[m_read_offset], src, len);
	m_read_offset += len;
}

ByteBuffer& ByteBuffer::operator>>(std::wstring& dst)
{
	u16 strLen = readU16(&m_data[m_read_offset]);
	m_read_offset += sizeof(u16);

	dst.clear();

	if (strLen == 0) {
		return *this;
	}

	dst.reserve(strLen);
	for(u16 i=0; i<strLen; i++) {
		wchar_t c16 = readU16(&m_data[m_read_offset]);
		dst.append(&c16, 1);
		m_read_offset += sizeof(u16);
	}

	return *this;
}

ByteBuffer& ByteBuffer::operator<<(std::wstring src)
{
	u16 msgsize = src.size();
	if (msgsize > 0xFFFF) {
		msgsize = 0xFFFF;
	}

	*this << msgsize;

	// Write string
	for (u16 i=0; i<msgsize; i++) {
		*this << (u16) src[i];
	}

	return *this;
}

std::string ByteBuffer::readLongString()
{
	u32 strLen = readU32(&m_data[m_read_offset]);
	m_read_offset += sizeof(u32);

	if (strLen == 0) {
		return "";
	}

	std::string dst;

	dst.reserve(strLen);
	dst.append((char*)&m_data[m_read_offset], strLen);

	m_read_offset += strLen;

	return dst;
}

ByteBuffer& ByteBuffer::operator>>(char& dst)
{
	checkReadOffset(m_read_offset);

	dst = readU8(&m_data[m_read_offset]);

	incrOffset<char>();
	return *this;
}

char ByteBuffer::getChar(u32 offset)
{
	checkReadOffset(offset);

	return readU8(&m_data[offset]);
}

ByteBuffer& ByteBuffer::operator<<(char src)
{
	checkDataSize<u8>();

	writeU8(&m_data[m_read_offset], src);

	incrOffset<char>();
	return *this;
}

ByteBuffer& ByteBuffer::operator<<(u8 src)
{
	checkDataSize<u8>();

	writeU8(&m_data[m_read_offset], src);

	incrOffset<u8>();
	return *this;
}

ByteBuffer& ByteBuffer::operator<<(u16 src)
{
	checkDataSize<u16>();

	writeU16(&m_data[m_read_offset], src);

	incrOffset<u16>();
	return *this;
}

ByteBuffer& ByteBuffer::operator<<(u64 src)
{
	checkDataSize<u64>();

	writeU64(&m_data[m_read_offset], src);

	incrOffset<u64>();
	return *this;
}

ByteBuffer& ByteBuffer::operator>>(u8& dst)
{
	checkReadOffset(m_read_offset);

	dst = readU8(&m_data[m_read_offset]);

	incrOffset<u8>();
	return *this;
}

u8 ByteBuffer::getU8(u32 offset)
{
	checkReadOffset(offset);

	return readU8(&m_data[offset]);
}

ByteBuffer& ByteBuffer::operator>>(u16& dst)
{
	checkReadOffset(m_read_offset);

	dst = readU16(&m_data[m_read_offset]);

	incrOffset<u16>();
	return *this;
}

u16 ByteBuffer::getU16(u32 from_offset)
{
	checkReadOffset(from_offset);

	return readU16(&m_data[from_offset]);
}

ByteBuffer& ByteBuffer::operator>>(u64& dst)
{
	checkReadOffset(m_read_offset);

	dst = readU64(&m_data[m_read_offset]);

	incrOffset<u64>();
	return *this;
}

ByteBuffer& ByteBuffer::operator>>(s16& dst)
{
	checkReadOffset(m_read_offset);

	dst = readS16(&m_data[m_read_offset]);

	incrOffset<s16>();
	return *this;
}

ByteBuffer& ByteBuffer::operator<<(s16 src)
{
	*this << (u16) src;
	return *this;
}

ByteBuffer& ByteBuffer::operator>>(v3s16& dst)
{
	checkReadOffset(m_read_offset);

	dst = readV3S16(&m_data[m_read_offset]);

	incrOffset<v3s16>();
	return *this;
}

ByteBuffer& ByteBuffer::operator<<(v3s16 src)
{
	*this << (s16) src.X;
	*this << (s16) src.Y;
	*this << (s16) src.Z;
	return *this;
}

ByteBuffer& ByteBuffer::operator>>(video::SColor& dst)
{
	checkReadOffset(m_read_offset);

	dst = readARGB8(&m_data[m_read_offset]);

	incrOffset<u32>();
	return *this;
}

ByteBuffer& ByteBuffer::operator<<(video::SColor src)
{
	checkDataSize<u32>();

	writeU32(&m_data[m_read_offset], src.color);

	incrOffset<u32>();
	return *this;
}

