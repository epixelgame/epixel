/*
Minetest
Copyright (C) 2015-2016 nerzhul, Loic Blot <loic.blot@unix-experience.fr>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef UTIL_BYTEBUFFER_H_
#define UTIL_BYTEBUFFER_H_

#include "util/pointer.h"
#include "util/numeric.h"
#include "SColor.h"

class ByteBuffer
{
public:
	ByteBuffer(): m_datasize(0), m_read_offset(0) {}
	virtual ~ByteBuffer() {}
	u32 getSize() { return m_datasize; }

	void reinitReadOffset() { m_read_offset = 0; }

	u8* getU8Ptr(u32 offset);

	void putLongString(const std::string &src);

	ByteBuffer& operator>>(std::string& dst);
	ByteBuffer& operator<<(std::string src);

	ByteBuffer& operator>>(bool& dst);
	ByteBuffer& operator<<(bool src);

	ByteBuffer& operator>>(float& dst);
	ByteBuffer& operator<<(float src);

	ByteBuffer& operator>>(u32& dst);
	ByteBuffer& operator<<(u32 src);

	ByteBuffer& operator>>(s32& dst);
	ByteBuffer& operator<<(s32 src);

	ByteBuffer& operator>>(v2s32& dst);
	ByteBuffer& operator<<(v2s32 src);

	ByteBuffer& operator>>(v3s32& dst);
	ByteBuffer& operator<<(v3s32 src);

	ByteBuffer& operator>>(v2f& dst);
	ByteBuffer& operator<<(v2f src);

	ByteBuffer& operator>>(v3f& dst);
	ByteBuffer& operator<<(v3f src);

	// Data extractors
	char* getString(u32 from_offset);
	void putRawString(const char* src, u32 len);

	ByteBuffer& operator>>(std::wstring& dst);
	ByteBuffer& operator<<(std::wstring src);

	std::string readLongString();

	char getChar(u32 offset);
	ByteBuffer& operator>>(char& dst);
	ByteBuffer& operator<<(char src);

	u8 getU8(u32 offset);

	ByteBuffer& operator>>(u8& dst);
	ByteBuffer& operator<<(u8 src);

	u16 getU16(u32 from_offset);
	ByteBuffer& operator>>(u16& dst);
	ByteBuffer& operator<<(u16 src);

	ByteBuffer& operator>>(u64& dst);
	ByteBuffer& operator<<(u64 src);

	ByteBuffer& operator>>(s16& dst);
	ByteBuffer& operator<<(s16 src);

	ByteBuffer& operator>>(v3s16& dst);
	ByteBuffer& operator<<(v3s16 src);

	ByteBuffer& operator>>(video::SColor& dst);
	ByteBuffer& operator<<(video::SColor src);

protected:
	void checkReadOffset(u32 from_offset);

	template<typename T> void checkDataSize()
	{
		if (m_read_offset + sizeof(T) > m_datasize) {
			m_datasize = m_read_offset + sizeof(T);
			m_data.resize(m_datasize);
		}
	}

	template<typename T> void incrOffset()
	{
		m_read_offset += sizeof(T);
	}

	std::vector<u8> m_data;
	u32 m_datasize;
	u32 m_read_offset;
};



#endif /* UTIL_BYTEBUFFER_H_ */
