#!/bin/sh
#Options
EPIXELDIR=../
GAMENAME=epixel_game
MEDIADIR=../../epixel_static_assets/game/

function die {
	echo "$1"
	exit 1
}

if [ ! -d $EPIXELDIR ]; then
die "Please specify a valid Minetest directory"
fi

which openssl &> /dev/null || die "Install the OpenSSL commandline tool!"
openssl -h 2>&1 | grep sha1 &> /dev/null || die "OpenSSL without sha1 won't work"
which awk &> /dev/null || die "Install (g)awk!"

mkdir -p $MEDIADIR

if [ ! $GAMENAME == none ]; then
find $EPIXELDIR/games/$GAMENAME/mods/default -type f -name "*.png" -o -name "*.ogg" -o -name "*.x" | while read f; do
basename "$f"
cp "$f" $MEDIADIR/`cat "$f" | openssl dgst -sha1 | awk '{print $2}'`
done
fi

echo -n "Creating index.mth..."
echo -en "MTHS\x00\x01" > $MEDIADIR/index.mth
find $MEDIADIR -type f ! -name index.mth | while read f; do
cat "$f" | openssl dgst -binary -sha1 >> $MEDIADIR/index.mth
done
echo "done"


