#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# To use this script you need:
# * Python3
# * python-configobj module
# * python-psycopg2 module
# * python-redis module

import getopt,sys,re,os
from os.path import isfile, join
from os import listdir
from configobj import ConfigObj
import psycopg2,redis

def usage():
	print("""Usage:
python minetest_to_epixel.py -c <epixel.conf> (...)

Available options:
-a <file>: minetest auth.txt file. This file is generally in world directory.
-b <file>: minetest ipban.txt file. This file is generally in world directory.
-c <file>: epixel epixel.conf file. This file is generally in /etc or /usr/local/etc directory.
-e <file>: minetest env_meta.txt file. This file is generally in world directory.
-o <file>: unified_inventory home file. This file is generally in world directory.
-p <dir>: old minetest players directory. This directory is generally into /var/lib/minetest, /home/minetest or /var/db/minetest
-r <redis_host>: Redis host IP. Host and hash are inclusive.
-H <redis_hash>: Redis hash. Host and hash are inclusive.
""")

def migrate_authfile(authfile, con):
	# Read authfile and import users
	cur = con.cursor()
	userprivs = {}
	print("Reading authfile...")
	with open(authfile) as f:
		for line in f:
			splline = re.split(":",line.rstrip())
			if len(splline) != 4:
				print("FATAL: auth.txt line invalid:\n----\n%s\n----" % line.rstrip())
				con.close()
				sys.exit(6)
			cur.execute("INSERT INTO users(login,password,lastlogin) VALUES ('%s','%s','%s')" % (splline[0],splline[1],splline[3]))
			userprivs[splline[0]] = re.split(",",splline[2])
	con.commit()

	# Retrieve user ids and import privs
	cur = con.cursor()
	cur.execute("SELECT id,login FROM users")
	rows = cur.fetchall()

	cur = con.cursor()
	for row in rows:
		if row[1] in userprivs:
			for priv in userprivs[row[1]]:
				cur.execute("INSERT INTO user_privs(user_id,priv_name) VALUES('%s','%s')" % (row[0],priv))
	con.commit()
	print("%d users imported." % len(userprivs))

def migrate_banfile(banfile, con):
	# Read authfile and import users
	cur = con.cursor()
	bancount = 0
	print("Reading banfile...")
	with open(banfile) as f:
		for line in f:
			splline = re.split("\|",line.rstrip())
			if len(splline) != 2:
				print("FATAL: ipban.txt line invalid:\n----\n%s\n----" % line.rstrip())
				con.close()
				sys.exit(6)
			cur.execute("INSERT INTO bans_ip(ip,source,reason,bantime) VALUES ('%s','None','%s',to_timestamp(3599)::timestamp)" % (splline[0],splline[1]))
			bancount = bancount + 1
	con.commit()
	print("%d bans imported." % bancount)

def migrate_players(playerpath, inventoryOnly, con):
	files = [ f for f in listdir(playerpath) if isfile(join(playerpath,f)) ]
	cur = con.cursor()
	importCount = 0
	importItemCount = 0
	for fn in files:
		print("Reading playerfile for %s" % fn)
		with open("%s/%s" % (playerpath, fn)) as f:
			readInventory = False

			hp = 20
			breath = 11
			hunger = 20
			money = 0
			name = fn
			pitch = 0.0
			position = [0,0,0]
			yaw = 0.0

			invName = ""
			invSize = 0
			invWidth = 0
			invId = 0
			slotId = 0

			content = f.readlines()
			for l in content:
				l = re.sub("\n","",l)
				if readInventory == False:
					s = re.search("name = (.*)",l)
					if s != None:
						name = s.group(1)
						continue

					s = re.search("hp = (.*)",l)
					if s != None:
						hp = int(s.group(1))
						continue

					s = re.search("breath = (.*)",l)
					if s != None:
						breath = int(s.group(1))
						continue

					s = re.search("hunger = (.*)",l)
					if s != None:
						hunger = int(s.group(1))
						continue

					s = re.search("money = (.*)",l)
					if s != None:
						money = int(s.group(1))
						continue

					s = re.search("pitch = (.*)",l)
					if s != None:
						pitch = float(s.group(1))
						continue

					s = re.search("yaw = (.*)",l)
					if s != None:
						yaw = s.group(1)
						continue

					s = re.search("position = \((.*),(.*),(.*)\)",l)
					if s != None:
						position[0] = float(s.group(1))
						position[1] = float(s.group(2))
						position[2] = float(s.group(3))

					s = re.search("List (.*) (.*)",l)
					if s != None:
						invName = s.group(1)
						invSize = s.group(2)
						# After reading stats, if we found a list, we are reading inventory
						readInventory = True

				else:
					s = re.search("List (.*) (.*)",l)
					if s != None:
						# Increment only when found the second List
						invId = invId + 1
						invName = s.group(1)
						invSize = s.group(2)

					s = re.search("Width (.*)",l)
					if s != None:
						invWidth = s.group(1)
						cur.execute("INSERT INTO player_inventories(user_id,inv_id,inv_width,inv_name,inv_size) VALUES ((SELECT id FROM users WHERE login = '%s'),%s,%s,'%s',%s)" %
							(name, invId, invWidth, invName, invSize))

					s = re.search("Item (.*)",l)
					if s != None:
						curItem = s.group(1)
						cur.execute("INSERT INTO player_inventory_items(user_id,inv_id,slot_id,item) VALUES ((SELECT id FROM users WHERE login = '%s'),%s,%s,'%s')" %
							(name, invId, slotId, curItem))
						slotId = slotId + 1
						importItemCount = importItemCount + 1

					s = re.search("Empty",l)
					if s != None:
						cur.execute("INSERT INTO player_inventory_items(user_id,inv_id,slot_id,item) VALUES ((SELECT id FROM users WHERE login = '%s'),%s,%s,'')" %
							(name, invId, slotId))
						slotId = slotId + 1
						importItemCount = importItemCount + 1

			if inventoryOnly == False:
				cur.execute("INSERT INTO player(user_id,pitch,yaw,posx,posy,posz,hp,breath,money,hunger) VALUES ((SELECT id FROM users WHERE login = '%s'),%s,%s,%s,%s,%s,%s,%s,%s,%s)" %
					(name, pitch, yaw, position[0], position[1], position[2], hp, breath, money, hunger))
			importCount = importCount + 1

	con.commit()
	print("%d players imported and %d inventory items imported." % (importCount,importItemCount))

def migrate_homes(homepath, con):
	cur = con.cursor()
	importCount = 0
	with open(homepath) as f:
		for line in f:
			splline = re.split(" ",line.rstrip())
			if len(splline) != 4:
				print("FATAL: home line invalid:\n----\n%s\n----" % line.rstrip())
				con.close()
				sys.exit(6)
			cur.execute("UPDATE player SET homex = '%s', homey = '%s', homez = '%s' WHERE user_id = (SELECT id FROM users WHERE login = '%s')" % (splline[0],splline[1],splline[2],splline[3]))
			importCount = importCount + 1
	con.commit()
	print("%s player homes imported" % importCount)

def migrate_envmeta(envmetafile, con):
	# Read env_meta
	cur = con.cursor()
	print("Reading envmetafile... ")
	with open(envmetafile) as f:
		content = f.readlines()
		for l in content:
			l = re.sub("\n","",l)
			s = re.search("game_time = (.*)",l)
			if s != None:
				game_time = s.group(1)
				continue

			s = re.search("time_of_day = (.*)",l)
			if s != None:
				time_of_day = int(s.group(1))
				continue

	cur.execute("INSERT INTO env_meta(game_time, time_of_day) VALUES ('%s','%s')" % (game_time, time_of_day))
	con.commit()
	print("Env_meta has been imported to bdd")

def shitty_mt_conversion(i, max_positive):
	if i < max_positive:
		return i
	else:
		return i - (max_positive * 2)

def migrate_redisdb(redisHost, redisHash, con):
	try:
		r = redis.StrictRedis(host=redisHost, port=6379)
		blockList = r.hkeys(redisHash)
		bLen = len(blockList)
		if bLen == 0:
			print("No block to migrate, ensure you give the correct hash")
			sys.exit(2)
		cur = con.cursor()
		print("We will migrate %d blocks, be patient..." % bLen)
		bulkLimit = 0
		bulkBlocks = 0
		for b in blockList:
			i = int(b)
			x = shitty_mt_conversion(i % 4096, 2048)
			i = (i - x) / 4096
			y = shitty_mt_conversion(i % 4096, 2048)
			i = (i - y) / 4096
			z = shitty_mt_conversion(i % 4096, 2048)

			blockData = r.hget(redisHash, b)

			cur.execute("INSERT INTO blocks(posX,posY,posZ,block_data) VALUES (%d,%d,%d,%s)" %
				(int(x),int(y),int(z), psycopg2.Binary(blockData)))
			bulkLimit += 1
			if bulkLimit == 10000:
				bulkLimit = 0
				bulkBlocks += 1
				con.commit()
				print("%d blocks migrated" % int(bulkBlocks * 10000))
		# Commit the last bulk
		con.commit()
		print("%d blocks migrated" % bLen)
	except redis.exceptions.ConnectionError as err:
		print(str(err))
		sys.exit(1)

	print("Redis DB migrated")

def main():
	try:
		opts, args = getopt.getopt(sys.argv[1:], "h:a:c:b:p:o:i:e:r:H:", ["help", "authfile", "config", "banfile", "players", "homefile", "inventoryonly", "envmetapath", "redishost", "redishash"])
	except getopt.GetoptError as err:
		print(str(err))
		usage()
		sys.exit(1)

	authfile = None
	configfile = None
	banfile = None
	playerpath = None
	homepath = None
	inventoryOnly = False
	envmetafile = None
	redisHost = None
	redisHash = None
	for o, a in opts:
		if o in ("-h", "help"):
			usage()
			sys.exit()
		elif o in ("-a", "authfile"):
			authfile = a
		elif o in ("-c", "config"):
			configfile = a
		elif o in ("-b", "banfile"):
			banfile = a
		elif o in ("-o", "homefile"):
			homepath = a
		elif o in ("-p", "players"):
			playerpath = a
		elif o in ("-i", "inventoryonly"):
			inventoryOnly = True
		elif o in ("-e", "envmetapath"):
			envmetafile = a
		elif o in ("-r", "redishost"):
			redisHost = a
		elif o in ("-H", "redishash"):
			redisHash = a

	if authfile == None and banfile == None and playerpath == None and homepath == None and envmetafile == None and redisHost == None or configfile == None:
		usage()
		sys.exit(2)

	# Redis host & hash are inclusive
	if redisHost != None and redisHash == None:
		usage()
		sys.exit(2)

	config = ConfigObj(configfile)
	for configval in ("pg_host","pg_user","pg_password","pg_dbname"):
		if config.get(configval) == None:
			print("Option %s is missing in %s. Please set it" % (configval, configfile))
			sys.exit(3)

	con = None

	try:
		con = psycopg2.connect(database=config.get("pg_dbname"), user=config.get("pg_user"), host=config.get("pg_host"), password=config.get("pg_password"))
		cur = con.cursor()
		cur.execute('SELECT version FROM dbversion')
		dbver = cur.fetchone()
		if dbver[0] < 19:
			print("Database version not supported. Please upgrade to v14 or greater")
			sys.exit(5)

		if authfile != None:
			migrate_authfile(authfile, con)
		if banfile != None:
			migrate_banfile(banfile, con)
		if playerpath != None:
			migrate_players(playerpath, inventoryOnly, con)
		if homepath != None:
			migrate_homes(homepath, con)
		if envmetafile != None:
			migrate_envmeta(envmetafile, con)
		if redisHost != None:
			migrate_redisdb(redisHost, redisHash, con)
	except psycopg2.OperationalError as err:
		print("Unable to connect to PostgreSQL database:\n%s" % str(err))
		sys.exit(4)
	except psycopg2.IntegrityError as err:
		print("PostgreSQL query error:\n%s" % str(err))
		sys.exit(4)

	if con:
		con.close()

if __name__ == "__main__":
	main()
