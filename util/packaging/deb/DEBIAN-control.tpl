Package: epixel
Version: @@VERSION@@
Section: games
Priority: optional
Architecture: amd64
Homepage: http://www.epixel-game.net
Maintainer: Loic Blot <loic.blot@unix-experience.fr>
Build-Depends: cmake, debhelper (>= 9), dh-systemd (>= 1.5), gettext, imagemagick, libbz2-dev, libcurl4-gnutls-dev, libfreetype6-dev, libglu1-mesa-dev, libjpeg-dev, libogg-dev, libopenal-dev, libpng-dev, libvorbis-dev, libx11-dev, zlib1g-dev, liblz4-dev, libapr1-dev, g++ (>= 5.2)
Depends: libvorbis, liblz4, libapr1, libbz2, gettext, libcurl4-gnutls, libfreetype6, libglu1-mesa, libjpeg, libogg, libopenal, libpng, libx11, zlib1g
Description: Multiplayer infinite-world block sandbox
